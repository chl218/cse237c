// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

(* CORE_GENERATION_INFO="fir,hls_ip_2015_4,{HLS_INPUT_TYPE=c,HLS_INPUT_FLOAT=0,HLS_INPUT_FIXED=0,HLS_INPUT_PART=xc7k160tfbg484-2,HLS_INPUT_CLOCK=10.000000,HLS_INPUT_ARCH=others,HLS_SYN_CLOCK=8.430000,HLS_SYN_LAT=15,HLS_SYN_TPT=none,HLS_SYN_MEM=0,HLS_SYN_DSP=44,HLS_SYN_FF=977,HLS_SYN_LUT=253}" *)

module fir (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_idle,
        ap_ready,
        y,
        y_ap_vld,
        c_address0,
        c_ce0,
        c_q0,
        x,
        x_ap_vld
);

parameter    ap_const_logic_1 = 1'b1;
parameter    ap_const_logic_0 = 1'b0;
parameter    ap_ST_st1_fsm_0 = 16'b1;
parameter    ap_ST_st2_fsm_1 = 16'b10;
parameter    ap_ST_st3_fsm_2 = 16'b100;
parameter    ap_ST_st4_fsm_3 = 16'b1000;
parameter    ap_ST_st5_fsm_4 = 16'b10000;
parameter    ap_ST_st6_fsm_5 = 16'b100000;
parameter    ap_ST_st7_fsm_6 = 16'b1000000;
parameter    ap_ST_st8_fsm_7 = 16'b10000000;
parameter    ap_ST_st9_fsm_8 = 16'b100000000;
parameter    ap_ST_st10_fsm_9 = 16'b1000000000;
parameter    ap_ST_st11_fsm_10 = 16'b10000000000;
parameter    ap_ST_st12_fsm_11 = 16'b100000000000;
parameter    ap_ST_st13_fsm_12 = 16'b1000000000000;
parameter    ap_ST_st14_fsm_13 = 16'b10000000000000;
parameter    ap_ST_st15_fsm_14 = 16'b100000000000000;
parameter    ap_ST_st16_fsm_15 = 16'b1000000000000000;
parameter    ap_const_lv32_0 = 32'b00000000000000000000000000000000;
parameter    ap_const_lv1_1 = 1'b1;
parameter    ap_const_lv32_1 = 32'b1;
parameter    ap_const_lv32_5 = 32'b101;
parameter    ap_const_lv32_8 = 32'b1000;
parameter    ap_const_lv32_B = 32'b1011;
parameter    ap_const_lv32_2 = 32'b10;
parameter    ap_const_lv32_6 = 32'b110;
parameter    ap_const_lv32_3 = 32'b11;
parameter    ap_const_lv32_9 = 32'b1001;
parameter    ap_const_lv32_4 = 32'b100;
parameter    ap_const_lv32_A = 32'b1010;
parameter    ap_const_lv32_7 = 32'b111;
parameter    ap_const_lv32_C = 32'b1100;
parameter    ap_const_lv32_E = 32'b1110;
parameter    ap_const_lv64_A = 64'b1010;
parameter    ap_const_lv64_9 = 64'b1001;
parameter    ap_const_lv64_8 = 64'b1000;
parameter    ap_const_lv64_7 = 64'b111;
parameter    ap_const_lv64_6 = 64'b110;
parameter    ap_const_lv64_5 = 64'b101;
parameter    ap_const_lv64_4 = 64'b100;
parameter    ap_const_lv64_3 = 64'b11;
parameter    ap_const_lv64_2 = 64'b10;
parameter    ap_const_lv64_1 = 64'b1;
parameter    ap_const_lv64_0 = 64'b0000000000000000000000000000000000000000000000000000000000000000;
parameter    ap_const_lv32_F = 32'b1111;
parameter    ap_const_lv32_D = 32'b1101;
parameter    ap_true = 1'b1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
output   ap_idle;
output   ap_ready;
output  [31:0] y;
output   y_ap_vld;
output  [3:0] c_address0;
output   c_ce0;
input  [31:0] c_q0;
input  [31:0] x;
input   x_ap_vld;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg y_ap_vld;
reg[3:0] c_address0;
reg c_ce0;
(* fsm_encoding = "none" *) reg   [15:0] ap_CS_fsm = 16'b1;
reg    ap_sig_cseq_ST_st1_fsm_0;
reg    ap_sig_bdd_32;
reg  signed [31:0] x_in_sig;
reg   [31:0] x_preg = 32'b00000000000000000000000000000000;
reg    x_ap_vld_in_sig;
reg    x_ap_vld_preg = 1'b0;
reg  signed [31:0] shift_reg_9 = 32'b00000000000000000000000000000000;
reg  signed [31:0] shift_reg_8 = 32'b00000000000000000000000000000000;
reg  signed [31:0] shift_reg_7 = 32'b00000000000000000000000000000000;
reg  signed [31:0] shift_reg_6 = 32'b00000000000000000000000000000000;
reg  signed [31:0] shift_reg_5 = 32'b00000000000000000000000000000000;
reg  signed [31:0] shift_reg_4 = 32'b00000000000000000000000000000000;
reg  signed [31:0] shift_reg_3 = 32'b00000000000000000000000000000000;
reg  signed [31:0] shift_reg_2 = 32'b00000000000000000000000000000000;
reg  signed [31:0] shift_reg_1 = 32'b00000000000000000000000000000000;
reg  signed [31:0] shift_reg_0 = 32'b00000000000000000000000000000000;
reg  signed [31:0] reg_188;
reg    ap_sig_cseq_ST_st2_fsm_1;
reg    ap_sig_bdd_73;
reg    ap_sig_cseq_ST_st6_fsm_5;
reg    ap_sig_bdd_80;
reg    ap_sig_cseq_ST_st9_fsm_8;
reg    ap_sig_bdd_88;
reg    ap_sig_cseq_ST_st12_fsm_11;
reg    ap_sig_bdd_96;
reg  signed [31:0] reg_192;
reg    ap_sig_cseq_ST_st3_fsm_2;
reg    ap_sig_bdd_105;
reg    ap_sig_cseq_ST_st7_fsm_6;
reg    ap_sig_bdd_112;
reg  signed [31:0] reg_196;
reg    ap_sig_cseq_ST_st4_fsm_3;
reg    ap_sig_bdd_121;
reg    ap_sig_cseq_ST_st10_fsm_9;
reg    ap_sig_bdd_128;
reg  signed [31:0] reg_200;
reg    ap_sig_cseq_ST_st5_fsm_4;
reg    ap_sig_bdd_137;
reg    ap_sig_cseq_ST_st11_fsm_10;
reg    ap_sig_bdd_144;
wire   [31:0] grp_fu_208_p2;
reg   [31:0] tmp_6_reg_451;
wire   [31:0] grp_fu_224_p2;
reg   [31:0] tmp_6_1_reg_456;
wire   [31:0] tmp1_fu_278_p2;
reg   [31:0] tmp1_reg_486;
reg  signed [31:0] c_load_6_reg_491;
reg    ap_sig_cseq_ST_st8_fsm_7;
reg    ap_sig_bdd_186;
wire   [31:0] grp_fu_240_p2;
reg   [31:0] tmp_6_2_reg_501;
wire   [31:0] grp_fu_256_p2;
reg   [31:0] tmp_6_3_reg_506;
wire   [31:0] grp_fu_272_p2;
reg   [31:0] tmp_6_4_reg_511;
wire   [31:0] tmp2_fu_333_p2;
reg   [31:0] tmp2_reg_541;
wire   [31:0] grp_fu_292_p2;
reg   [31:0] tmp_6_5_reg_551;
wire   [31:0] grp_fu_308_p2;
reg   [31:0] tmp_6_6_reg_556;
wire   [31:0] grp_fu_323_p2;
reg   [31:0] tmp_6_7_reg_561;
reg    ap_sig_cseq_ST_st13_fsm_12;
reg    ap_sig_bdd_223;
wire   [31:0] tmp6_fu_382_p2;
reg   [31:0] tmp6_reg_581;
wire   [31:0] grp_fu_348_p2;
reg   [31:0] tmp_6_8_reg_586;
reg    ap_sig_cseq_ST_st15_fsm_14;
reg    ap_sig_bdd_241;
wire   [31:0] grp_fu_364_p2;
reg   [31:0] tmp_6_9_reg_591;
wire   [31:0] grp_fu_376_p2;
reg   [31:0] tmp_6_s_reg_596;
reg    ap_sig_cseq_ST_st16_fsm_15;
reg    ap_sig_bdd_274;
wire   [31:0] tmp3_fu_329_p2;
wire   [31:0] tmp8_fu_394_p2;
wire   [31:0] tmp7_fu_398_p2;
wire   [31:0] tmp5_fu_390_p2;
wire   [31:0] tmp4_fu_403_p2;
wire   [31:0] tmp_fu_386_p2;
wire    grp_fu_208_ce;
wire    grp_fu_224_ce;
wire    grp_fu_240_ce;
wire    grp_fu_256_ce;
wire    grp_fu_272_ce;
wire    grp_fu_292_ce;
wire    grp_fu_308_ce;
wire    grp_fu_323_ce;
reg    grp_fu_348_ce;
reg    ap_sig_cseq_ST_st14_fsm_13;
reg    ap_sig_bdd_361;
reg    grp_fu_364_ce;
reg    grp_fu_376_ce;
reg   [15:0] ap_NS_fsm;


fir_mul_32s_32s_32_3 #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fir_mul_32s_32s_32_3_U0(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( shift_reg_9 ),
    .din1( reg_188 ),
    .ce( grp_fu_208_ce ),
    .dout( grp_fu_208_p2 )
);

fir_mul_32s_32s_32_3 #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fir_mul_32s_32s_32_3_U1(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( shift_reg_8 ),
    .din1( reg_192 ),
    .ce( grp_fu_224_ce ),
    .dout( grp_fu_224_p2 )
);

fir_mul_32s_32s_32_3 #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fir_mul_32s_32s_32_3_U2(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( shift_reg_7 ),
    .din1( reg_196 ),
    .ce( grp_fu_240_ce ),
    .dout( grp_fu_240_p2 )
);

fir_mul_32s_32s_32_3 #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fir_mul_32s_32s_32_3_U3(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( shift_reg_6 ),
    .din1( reg_200 ),
    .ce( grp_fu_256_ce ),
    .dout( grp_fu_256_p2 )
);

fir_mul_32s_32s_32_3 #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fir_mul_32s_32s_32_3_U4(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( shift_reg_5 ),
    .din1( reg_188 ),
    .ce( grp_fu_272_ce ),
    .dout( grp_fu_272_p2 )
);

fir_mul_32s_32s_32_3 #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fir_mul_32s_32s_32_3_U5(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( shift_reg_4 ),
    .din1( reg_192 ),
    .ce( grp_fu_292_ce ),
    .dout( grp_fu_292_p2 )
);

fir_mul_32s_32s_32_3 #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fir_mul_32s_32s_32_3_U6(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( shift_reg_3 ),
    .din1( c_load_6_reg_491 ),
    .ce( grp_fu_308_ce ),
    .dout( grp_fu_308_p2 )
);

fir_mul_32s_32s_32_3 #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fir_mul_32s_32s_32_3_U7(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( shift_reg_2 ),
    .din1( reg_188 ),
    .ce( grp_fu_323_ce ),
    .dout( grp_fu_323_p2 )
);

fir_mul_32s_32s_32_3 #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fir_mul_32s_32s_32_3_U8(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( shift_reg_1 ),
    .din1( reg_196 ),
    .ce( grp_fu_348_ce ),
    .dout( grp_fu_348_p2 )
);

fir_mul_32s_32s_32_3 #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fir_mul_32s_32s_32_3_U9(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( shift_reg_0 ),
    .din1( reg_200 ),
    .ce( grp_fu_364_ce ),
    .dout( grp_fu_364_p2 )
);

fir_mul_32s_32s_32_3 #(
    .ID( 1 ),
    .NUM_STAGE( 3 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fir_mul_32s_32s_32_3_U10(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( reg_188 ),
    .din1( x_in_sig ),
    .ce( grp_fu_376_ce ),
    .dout( grp_fu_376_p2 )
);



always @ (posedge ap_clk) begin : ap_ret_ap_CS_fsm
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_st1_fsm_0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin : ap_ret_x_ap_vld_preg
    if (ap_rst == 1'b1) begin
        x_ap_vld_preg <= ap_const_logic_0;
    end else begin
        if ((ap_const_logic_1 == ap_sig_cseq_ST_st16_fsm_15)) begin
            x_ap_vld_preg <= ap_const_logic_0;
        end else if (((ap_const_logic_1 == x_ap_vld) & ~((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & (ap_const_logic_0 == ap_start)))) begin
            x_ap_vld_preg <= x_ap_vld;
        end
    end
end

always @ (posedge ap_clk) begin : ap_ret_x_preg
    if (ap_rst == 1'b1) begin
        x_preg <= ap_const_lv32_0;
    end else begin
        if (((ap_const_logic_1 == x_ap_vld) & ~((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & (ap_const_logic_0 == ap_start)))) begin
            x_preg <= x;
        end
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7)) begin
        c_load_6_reg_491 <= c_q0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) | (ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5) | (ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8) | (ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11))) begin
        reg_188 <= c_q0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2) | (ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6))) begin
        reg_192 <= c_q0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) | (ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9))) begin
        reg_196 <= c_q0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4) | (ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10))) begin
        reg_200 <= c_q0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st13_fsm_12) & ~(x_ap_vld_in_sig == ap_const_logic_0))) begin
        shift_reg_0 <= x_in_sig;
        shift_reg_1 <= shift_reg_0;
        shift_reg_2 <= shift_reg_1;
        tmp6_reg_581 <= tmp6_fu_382_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9)) begin
        shift_reg_3 <= shift_reg_2;
        shift_reg_4 <= shift_reg_3;
        shift_reg_5 <= shift_reg_4;
        tmp2_reg_541 <= tmp2_fu_333_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        shift_reg_6 <= shift_reg_5;
        shift_reg_7 <= shift_reg_6;
        shift_reg_8 <= shift_reg_7;
        tmp1_reg_486 <= tmp1_fu_278_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3)) begin
        shift_reg_9 <= shift_reg_8;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5)) begin
        tmp_6_1_reg_456 <= grp_fu_224_p2;
        tmp_6_reg_451 <= grp_fu_208_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8)) begin
        tmp_6_2_reg_501 <= grp_fu_240_p2;
        tmp_6_3_reg_506 <= grp_fu_256_p2;
        tmp_6_4_reg_511 <= grp_fu_272_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11)) begin
        tmp_6_5_reg_551 <= grp_fu_292_p2;
        tmp_6_6_reg_556 <= grp_fu_308_p2;
        tmp_6_7_reg_561 <= grp_fu_323_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st15_fsm_14)) begin
        tmp_6_8_reg_586 <= grp_fu_348_p2;
        tmp_6_9_reg_591 <= grp_fu_364_p2;
        tmp_6_s_reg_596 <= grp_fu_376_p2;
    end
end

always @ (ap_sig_cseq_ST_st16_fsm_15) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st16_fsm_15)) begin
        ap_done = ap_const_logic_1;
    end else begin
        ap_done = ap_const_logic_0;
    end
end

always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if ((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0))) begin
        ap_idle = ap_const_logic_1;
    end else begin
        ap_idle = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st16_fsm_15) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st16_fsm_15)) begin
        ap_ready = ap_const_logic_1;
    end else begin
        ap_ready = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_128) begin
    if (ap_sig_bdd_128) begin
        ap_sig_cseq_ST_st10_fsm_9 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st10_fsm_9 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_144) begin
    if (ap_sig_bdd_144) begin
        ap_sig_cseq_ST_st11_fsm_10 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st11_fsm_10 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_96) begin
    if (ap_sig_bdd_96) begin
        ap_sig_cseq_ST_st12_fsm_11 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st12_fsm_11 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_223) begin
    if (ap_sig_bdd_223) begin
        ap_sig_cseq_ST_st13_fsm_12 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st13_fsm_12 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_361) begin
    if (ap_sig_bdd_361) begin
        ap_sig_cseq_ST_st14_fsm_13 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st14_fsm_13 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_241) begin
    if (ap_sig_bdd_241) begin
        ap_sig_cseq_ST_st15_fsm_14 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st15_fsm_14 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_274) begin
    if (ap_sig_bdd_274) begin
        ap_sig_cseq_ST_st16_fsm_15 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st16_fsm_15 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_32) begin
    if (ap_sig_bdd_32) begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_73) begin
    if (ap_sig_bdd_73) begin
        ap_sig_cseq_ST_st2_fsm_1 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st2_fsm_1 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_105) begin
    if (ap_sig_bdd_105) begin
        ap_sig_cseq_ST_st3_fsm_2 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st3_fsm_2 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_121) begin
    if (ap_sig_bdd_121) begin
        ap_sig_cseq_ST_st4_fsm_3 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st4_fsm_3 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_137) begin
    if (ap_sig_bdd_137) begin
        ap_sig_cseq_ST_st5_fsm_4 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st5_fsm_4 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_80) begin
    if (ap_sig_bdd_80) begin
        ap_sig_cseq_ST_st6_fsm_5 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st6_fsm_5 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_112) begin
    if (ap_sig_bdd_112) begin
        ap_sig_cseq_ST_st7_fsm_6 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st7_fsm_6 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_186) begin
    if (ap_sig_bdd_186) begin
        ap_sig_cseq_ST_st8_fsm_7 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st8_fsm_7 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_88) begin
    if (ap_sig_bdd_88) begin
        ap_sig_cseq_ST_st9_fsm_8 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st9_fsm_8 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st1_fsm_0 or ap_sig_cseq_ST_st2_fsm_1 or ap_sig_cseq_ST_st6_fsm_5 or ap_sig_cseq_ST_st9_fsm_8 or ap_sig_cseq_ST_st3_fsm_2 or ap_sig_cseq_ST_st7_fsm_6 or ap_sig_cseq_ST_st4_fsm_3 or ap_sig_cseq_ST_st10_fsm_9 or ap_sig_cseq_ST_st5_fsm_4 or ap_sig_cseq_ST_st11_fsm_10 or ap_sig_cseq_ST_st8_fsm_7) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10)) begin
        c_address0 = ap_const_lv64_0;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9)) begin
        c_address0 = ap_const_lv64_1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8)) begin
        c_address0 = ap_const_lv64_2;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7)) begin
        c_address0 = ap_const_lv64_3;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        c_address0 = ap_const_lv64_4;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5)) begin
        c_address0 = ap_const_lv64_5;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4)) begin
        c_address0 = ap_const_lv64_6;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3)) begin
        c_address0 = ap_const_lv64_7;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        c_address0 = ap_const_lv64_8;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        c_address0 = ap_const_lv64_9;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0)) begin
        c_address0 = ap_const_lv64_A;
    end else begin
        c_address0 = 'bx;
    end
end

always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0 or ap_sig_cseq_ST_st2_fsm_1 or ap_sig_cseq_ST_st6_fsm_5 or ap_sig_cseq_ST_st9_fsm_8 or ap_sig_cseq_ST_st3_fsm_2 or ap_sig_cseq_ST_st7_fsm_6 or ap_sig_cseq_ST_st4_fsm_3 or ap_sig_cseq_ST_st10_fsm_9 or ap_sig_cseq_ST_st5_fsm_4 or ap_sig_cseq_ST_st11_fsm_10 or ap_sig_cseq_ST_st8_fsm_7) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) | (ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_5) | (ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8) | (ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2) | (ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6) | (ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) | (ap_const_logic_1 == ap_sig_cseq_ST_st10_fsm_9) | (ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4) | (ap_const_logic_1 == ap_sig_cseq_ST_st11_fsm_10) | ((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0)) | (ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7))) begin
        c_ce0 = ap_const_logic_1;
    end else begin
        c_ce0 = ap_const_logic_0;
    end
end

always @ (x_ap_vld_in_sig or ap_sig_cseq_ST_st13_fsm_12 or ap_sig_cseq_ST_st15_fsm_14 or ap_sig_cseq_ST_st14_fsm_13) begin
    if ((((ap_const_logic_1 == ap_sig_cseq_ST_st13_fsm_12) & ~(x_ap_vld_in_sig == ap_const_logic_0)) | (ap_const_logic_1 == ap_sig_cseq_ST_st15_fsm_14) | (ap_const_logic_1 == ap_sig_cseq_ST_st14_fsm_13))) begin
        grp_fu_348_ce = ap_const_logic_1;
    end else begin
        grp_fu_348_ce = ap_const_logic_0;
    end
end

always @ (x_ap_vld_in_sig or ap_sig_cseq_ST_st13_fsm_12 or ap_sig_cseq_ST_st15_fsm_14 or ap_sig_cseq_ST_st14_fsm_13) begin
    if ((((ap_const_logic_1 == ap_sig_cseq_ST_st13_fsm_12) & ~(x_ap_vld_in_sig == ap_const_logic_0)) | (ap_const_logic_1 == ap_sig_cseq_ST_st15_fsm_14) | (ap_const_logic_1 == ap_sig_cseq_ST_st14_fsm_13))) begin
        grp_fu_364_ce = ap_const_logic_1;
    end else begin
        grp_fu_364_ce = ap_const_logic_0;
    end
end

always @ (x_ap_vld_in_sig or ap_sig_cseq_ST_st13_fsm_12 or ap_sig_cseq_ST_st15_fsm_14 or ap_sig_cseq_ST_st14_fsm_13) begin
    if ((((ap_const_logic_1 == ap_sig_cseq_ST_st13_fsm_12) & ~(x_ap_vld_in_sig == ap_const_logic_0)) | (ap_const_logic_1 == ap_sig_cseq_ST_st15_fsm_14) | (ap_const_logic_1 == ap_sig_cseq_ST_st14_fsm_13))) begin
        grp_fu_376_ce = ap_const_logic_1;
    end else begin
        grp_fu_376_ce = ap_const_logic_0;
    end
end

always @ (x_ap_vld or x_ap_vld_preg) begin
    if ((ap_const_logic_1 == x_ap_vld)) begin
        x_ap_vld_in_sig = x_ap_vld;
    end else begin
        x_ap_vld_in_sig = x_ap_vld_preg;
    end
end

always @ (x or x_preg or x_ap_vld) begin
    if ((ap_const_logic_1 == x_ap_vld)) begin
        x_in_sig = x;
    end else begin
        x_in_sig = x_preg;
    end
end

always @ (ap_sig_cseq_ST_st16_fsm_15) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st16_fsm_15)) begin
        y_ap_vld = ap_const_logic_1;
    end else begin
        y_ap_vld = ap_const_logic_0;
    end
end
always @ (ap_start or ap_CS_fsm or x_ap_vld_in_sig) begin
    case (ap_CS_fsm)
        ap_ST_st1_fsm_0 : 
        begin
            if (~(ap_start == ap_const_logic_0)) begin
                ap_NS_fsm = ap_ST_st2_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end
        end
        ap_ST_st2_fsm_1 : 
        begin
            ap_NS_fsm = ap_ST_st3_fsm_2;
        end
        ap_ST_st3_fsm_2 : 
        begin
            ap_NS_fsm = ap_ST_st4_fsm_3;
        end
        ap_ST_st4_fsm_3 : 
        begin
            ap_NS_fsm = ap_ST_st5_fsm_4;
        end
        ap_ST_st5_fsm_4 : 
        begin
            ap_NS_fsm = ap_ST_st6_fsm_5;
        end
        ap_ST_st6_fsm_5 : 
        begin
            ap_NS_fsm = ap_ST_st7_fsm_6;
        end
        ap_ST_st7_fsm_6 : 
        begin
            ap_NS_fsm = ap_ST_st8_fsm_7;
        end
        ap_ST_st8_fsm_7 : 
        begin
            ap_NS_fsm = ap_ST_st9_fsm_8;
        end
        ap_ST_st9_fsm_8 : 
        begin
            ap_NS_fsm = ap_ST_st10_fsm_9;
        end
        ap_ST_st10_fsm_9 : 
        begin
            ap_NS_fsm = ap_ST_st11_fsm_10;
        end
        ap_ST_st11_fsm_10 : 
        begin
            ap_NS_fsm = ap_ST_st12_fsm_11;
        end
        ap_ST_st12_fsm_11 : 
        begin
            ap_NS_fsm = ap_ST_st13_fsm_12;
        end
        ap_ST_st13_fsm_12 : 
        begin
            if (~(x_ap_vld_in_sig == ap_const_logic_0)) begin
                ap_NS_fsm = ap_ST_st14_fsm_13;
            end else begin
                ap_NS_fsm = ap_ST_st13_fsm_12;
            end
        end
        ap_ST_st14_fsm_13 : 
        begin
            ap_NS_fsm = ap_ST_st15_fsm_14;
        end
        ap_ST_st15_fsm_14 : 
        begin
            ap_NS_fsm = ap_ST_st16_fsm_15;
        end
        ap_ST_st16_fsm_15 : 
        begin
            ap_NS_fsm = ap_ST_st1_fsm_0;
        end
        default : 
        begin
            ap_NS_fsm = 'bx;
        end
    endcase
end



always @ (ap_CS_fsm) begin
    ap_sig_bdd_105 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_2]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_112 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_6]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_121 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_3]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_128 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_9]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_137 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_4]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_144 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_A]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_186 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_7]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_223 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_C]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_241 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_E]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_274 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_F]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_32 = (ap_CS_fsm[ap_const_lv32_0] == ap_const_lv1_1);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_361 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_D]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_73 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_1]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_80 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_5]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_88 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_8]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_96 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_B]);
end

assign grp_fu_208_ce = ap_const_logic_1;

assign grp_fu_224_ce = ap_const_logic_1;

assign grp_fu_240_ce = ap_const_logic_1;

assign grp_fu_256_ce = ap_const_logic_1;

assign grp_fu_272_ce = ap_const_logic_1;

assign grp_fu_292_ce = ap_const_logic_1;

assign grp_fu_308_ce = ap_const_logic_1;

assign grp_fu_323_ce = ap_const_logic_1;

assign tmp1_fu_278_p2 = (tmp_6_reg_451 + tmp_6_1_reg_456);

assign tmp2_fu_333_p2 = (tmp3_fu_329_p2 + tmp_6_2_reg_501);

assign tmp3_fu_329_p2 = (tmp_6_3_reg_506 + tmp_6_4_reg_511);

assign tmp4_fu_403_p2 = (tmp7_fu_398_p2 + tmp5_fu_390_p2);

assign tmp5_fu_390_p2 = (tmp6_reg_581 + tmp_6_5_reg_551);

assign tmp6_fu_382_p2 = (tmp_6_6_reg_556 + tmp_6_7_reg_561);

assign tmp7_fu_398_p2 = (tmp8_fu_394_p2 + tmp_6_8_reg_586);

assign tmp8_fu_394_p2 = (tmp_6_9_reg_591 + tmp_6_s_reg_596);

assign tmp_fu_386_p2 = (tmp2_reg_541 + tmp1_reg_486);

assign y = (tmp4_fu_403_p2 + tmp_fu_386_p2);


endmodule //fir

