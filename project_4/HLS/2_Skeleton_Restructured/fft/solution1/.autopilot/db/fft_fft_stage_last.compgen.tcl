# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 206 \
    name real_i_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_0 \
    op interface \
    ports { real_i_0_address0 { O 8 vector } real_i_0_ce0 { O 1 bit } real_i_0_q0 { I 32 vector } real_i_0_address1 { O 8 vector } real_i_0_ce1 { O 1 bit } real_i_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 207 \
    name real_i_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_1 \
    op interface \
    ports { real_i_1_address0 { O 8 vector } real_i_1_ce0 { O 1 bit } real_i_1_q0 { I 32 vector } real_i_1_address1 { O 8 vector } real_i_1_ce1 { O 1 bit } real_i_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 208 \
    name real_i_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_2 \
    op interface \
    ports { real_i_2_address0 { O 8 vector } real_i_2_ce0 { O 1 bit } real_i_2_q0 { I 32 vector } real_i_2_address1 { O 8 vector } real_i_2_ce1 { O 1 bit } real_i_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 209 \
    name real_i_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_3 \
    op interface \
    ports { real_i_3_address0 { O 8 vector } real_i_3_ce0 { O 1 bit } real_i_3_q0 { I 32 vector } real_i_3_address1 { O 8 vector } real_i_3_ce1 { O 1 bit } real_i_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 210 \
    name imag_i \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i \
    op interface \
    ports { imag_i_address0 { O 10 vector } imag_i_ce0 { O 1 bit } imag_i_q0 { I 32 vector } imag_i_address1 { O 10 vector } imag_i_ce1 { O 1 bit } imag_i_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 211 \
    name real_o_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o_0 \
    op interface \
    ports { real_o_0_address0 { O 7 vector } real_o_0_ce0 { O 1 bit } real_o_0_we0 { O 1 bit } real_o_0_d0 { O 32 vector } real_o_0_address1 { O 7 vector } real_o_0_ce1 { O 1 bit } real_o_0_we1 { O 1 bit } real_o_0_d1 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 212 \
    name real_o_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o_1 \
    op interface \
    ports { real_o_1_address0 { O 7 vector } real_o_1_ce0 { O 1 bit } real_o_1_we0 { O 1 bit } real_o_1_d0 { O 32 vector } real_o_1_address1 { O 7 vector } real_o_1_ce1 { O 1 bit } real_o_1_we1 { O 1 bit } real_o_1_d1 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 213 \
    name real_o_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o_2 \
    op interface \
    ports { real_o_2_address0 { O 7 vector } real_o_2_ce0 { O 1 bit } real_o_2_we0 { O 1 bit } real_o_2_d0 { O 32 vector } real_o_2_address1 { O 7 vector } real_o_2_ce1 { O 1 bit } real_o_2_we1 { O 1 bit } real_o_2_d1 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 214 \
    name real_o_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o_3 \
    op interface \
    ports { real_o_3_address0 { O 7 vector } real_o_3_ce0 { O 1 bit } real_o_3_we0 { O 1 bit } real_o_3_d0 { O 32 vector } real_o_3_address1 { O 7 vector } real_o_3_ce1 { O 1 bit } real_o_3_we1 { O 1 bit } real_o_3_d1 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 215 \
    name real_o_4 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o_4 \
    op interface \
    ports { real_o_4_address0 { O 7 vector } real_o_4_ce0 { O 1 bit } real_o_4_we0 { O 1 bit } real_o_4_d0 { O 32 vector } real_o_4_address1 { O 7 vector } real_o_4_ce1 { O 1 bit } real_o_4_we1 { O 1 bit } real_o_4_d1 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o_4'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 216 \
    name real_o_5 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o_5 \
    op interface \
    ports { real_o_5_address0 { O 7 vector } real_o_5_ce0 { O 1 bit } real_o_5_we0 { O 1 bit } real_o_5_d0 { O 32 vector } real_o_5_address1 { O 7 vector } real_o_5_ce1 { O 1 bit } real_o_5_we1 { O 1 bit } real_o_5_d1 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o_5'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 217 \
    name real_o_6 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o_6 \
    op interface \
    ports { real_o_6_address0 { O 7 vector } real_o_6_ce0 { O 1 bit } real_o_6_we0 { O 1 bit } real_o_6_d0 { O 32 vector } real_o_6_address1 { O 7 vector } real_o_6_ce1 { O 1 bit } real_o_6_we1 { O 1 bit } real_o_6_d1 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o_6'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 218 \
    name real_o_7 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o_7 \
    op interface \
    ports { real_o_7_address0 { O 7 vector } real_o_7_ce0 { O 1 bit } real_o_7_we0 { O 1 bit } real_o_7_d0 { O 32 vector } real_o_7_address1 { O 7 vector } real_o_7_ce1 { O 1 bit } real_o_7_we1 { O 1 bit } real_o_7_d1 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o_7'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 219 \
    name imag_o_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o_0 \
    op interface \
    ports { imag_o_0_address0 { O 7 vector } imag_o_0_ce0 { O 1 bit } imag_o_0_we0 { O 1 bit } imag_o_0_d0 { O 32 vector } imag_o_0_address1 { O 7 vector } imag_o_0_ce1 { O 1 bit } imag_o_0_we1 { O 1 bit } imag_o_0_d1 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 220 \
    name imag_o_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o_1 \
    op interface \
    ports { imag_o_1_address0 { O 7 vector } imag_o_1_ce0 { O 1 bit } imag_o_1_we0 { O 1 bit } imag_o_1_d0 { O 32 vector } imag_o_1_address1 { O 7 vector } imag_o_1_ce1 { O 1 bit } imag_o_1_we1 { O 1 bit } imag_o_1_d1 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 221 \
    name imag_o_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o_2 \
    op interface \
    ports { imag_o_2_address0 { O 7 vector } imag_o_2_ce0 { O 1 bit } imag_o_2_we0 { O 1 bit } imag_o_2_d0 { O 32 vector } imag_o_2_address1 { O 7 vector } imag_o_2_ce1 { O 1 bit } imag_o_2_we1 { O 1 bit } imag_o_2_d1 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 222 \
    name imag_o_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o_3 \
    op interface \
    ports { imag_o_3_address0 { O 7 vector } imag_o_3_ce0 { O 1 bit } imag_o_3_we0 { O 1 bit } imag_o_3_d0 { O 32 vector } imag_o_3_address1 { O 7 vector } imag_o_3_ce1 { O 1 bit } imag_o_3_we1 { O 1 bit } imag_o_3_d1 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 223 \
    name imag_o_4 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o_4 \
    op interface \
    ports { imag_o_4_address0 { O 7 vector } imag_o_4_ce0 { O 1 bit } imag_o_4_we0 { O 1 bit } imag_o_4_d0 { O 32 vector } imag_o_4_address1 { O 7 vector } imag_o_4_ce1 { O 1 bit } imag_o_4_we1 { O 1 bit } imag_o_4_d1 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o_4'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 224 \
    name imag_o_5 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o_5 \
    op interface \
    ports { imag_o_5_address0 { O 7 vector } imag_o_5_ce0 { O 1 bit } imag_o_5_we0 { O 1 bit } imag_o_5_d0 { O 32 vector } imag_o_5_address1 { O 7 vector } imag_o_5_ce1 { O 1 bit } imag_o_5_we1 { O 1 bit } imag_o_5_d1 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o_5'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 225 \
    name imag_o_6 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o_6 \
    op interface \
    ports { imag_o_6_address0 { O 7 vector } imag_o_6_ce0 { O 1 bit } imag_o_6_we0 { O 1 bit } imag_o_6_d0 { O 32 vector } imag_o_6_address1 { O 7 vector } imag_o_6_ce1 { O 1 bit } imag_o_6_we1 { O 1 bit } imag_o_6_d1 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o_6'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 226 \
    name imag_o_7 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o_7 \
    op interface \
    ports { imag_o_7_address0 { O 7 vector } imag_o_7_ce0 { O 1 bit } imag_o_7_we0 { O 1 bit } imag_o_7_d0 { O 32 vector } imag_o_7_address1 { O 7 vector } imag_o_7_ce1 { O 1 bit } imag_o_7_we1 { O 1 bit } imag_o_7_d1 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o_7'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


