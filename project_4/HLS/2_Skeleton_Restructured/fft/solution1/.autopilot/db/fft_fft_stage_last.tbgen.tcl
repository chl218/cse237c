set moduleName fft_fft_stage_last
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {fft_fft_stage_last}
set C_modelType { void 0 }
set C_modelArgList { 
	{ real_i_0 float 32 regular {array 256 { 1 1 } 1 1 }  }
	{ real_i_1 float 32 regular {array 256 { 1 1 } 1 1 }  }
	{ real_i_2 float 32 regular {array 256 { 1 1 } 1 1 }  }
	{ real_i_3 float 32 regular {array 256 { 1 1 } 1 1 }  }
	{ imag_i float 32 regular {array 1024 { 1 1 } 1 1 }  }
	{ real_o_0 float 32 regular {array 128 { 0 0 } 0 1 }  }
	{ real_o_1 float 32 regular {array 128 { 0 0 } 0 1 }  }
	{ real_o_2 float 32 regular {array 128 { 0 0 } 0 1 }  }
	{ real_o_3 float 32 regular {array 128 { 0 0 } 0 1 }  }
	{ real_o_4 float 32 regular {array 128 { 0 0 } 0 1 }  }
	{ real_o_5 float 32 regular {array 128 { 0 0 } 0 1 }  }
	{ real_o_6 float 32 regular {array 128 { 0 0 } 0 1 }  }
	{ real_o_7 float 32 regular {array 128 { 0 0 } 0 1 }  }
	{ imag_o_0 float 32 regular {array 128 { 0 0 } 0 1 }  }
	{ imag_o_1 float 32 regular {array 128 { 0 0 } 0 1 }  }
	{ imag_o_2 float 32 regular {array 128 { 0 0 } 0 1 }  }
	{ imag_o_3 float 32 regular {array 128 { 0 0 } 0 1 }  }
	{ imag_o_4 float 32 regular {array 128 { 0 0 } 0 1 }  }
	{ imag_o_5 float 32 regular {array 128 { 0 0 } 0 1 }  }
	{ imag_o_6 float 32 regular {array 128 { 0 0 } 0 1 }  }
	{ imag_o_7 float 32 regular {array 128 { 0 0 } 0 1 }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "real_i_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_o_0", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "real_o_1", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "real_o_2", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "real_o_3", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "real_o_4", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "real_o_5", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "real_o_6", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "real_o_7", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "imag_o_0", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "imag_o_1", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "imag_o_2", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "imag_o_3", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "imag_o_4", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "imag_o_5", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "imag_o_6", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "imag_o_7", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} ]}
# RTL Port declarations: 
set portNum 165
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ real_i_0_address0 sc_out sc_lv 8 signal 0 } 
	{ real_i_0_ce0 sc_out sc_logic 1 signal 0 } 
	{ real_i_0_q0 sc_in sc_lv 32 signal 0 } 
	{ real_i_0_address1 sc_out sc_lv 8 signal 0 } 
	{ real_i_0_ce1 sc_out sc_logic 1 signal 0 } 
	{ real_i_0_q1 sc_in sc_lv 32 signal 0 } 
	{ real_i_1_address0 sc_out sc_lv 8 signal 1 } 
	{ real_i_1_ce0 sc_out sc_logic 1 signal 1 } 
	{ real_i_1_q0 sc_in sc_lv 32 signal 1 } 
	{ real_i_1_address1 sc_out sc_lv 8 signal 1 } 
	{ real_i_1_ce1 sc_out sc_logic 1 signal 1 } 
	{ real_i_1_q1 sc_in sc_lv 32 signal 1 } 
	{ real_i_2_address0 sc_out sc_lv 8 signal 2 } 
	{ real_i_2_ce0 sc_out sc_logic 1 signal 2 } 
	{ real_i_2_q0 sc_in sc_lv 32 signal 2 } 
	{ real_i_2_address1 sc_out sc_lv 8 signal 2 } 
	{ real_i_2_ce1 sc_out sc_logic 1 signal 2 } 
	{ real_i_2_q1 sc_in sc_lv 32 signal 2 } 
	{ real_i_3_address0 sc_out sc_lv 8 signal 3 } 
	{ real_i_3_ce0 sc_out sc_logic 1 signal 3 } 
	{ real_i_3_q0 sc_in sc_lv 32 signal 3 } 
	{ real_i_3_address1 sc_out sc_lv 8 signal 3 } 
	{ real_i_3_ce1 sc_out sc_logic 1 signal 3 } 
	{ real_i_3_q1 sc_in sc_lv 32 signal 3 } 
	{ imag_i_address0 sc_out sc_lv 10 signal 4 } 
	{ imag_i_ce0 sc_out sc_logic 1 signal 4 } 
	{ imag_i_q0 sc_in sc_lv 32 signal 4 } 
	{ imag_i_address1 sc_out sc_lv 10 signal 4 } 
	{ imag_i_ce1 sc_out sc_logic 1 signal 4 } 
	{ imag_i_q1 sc_in sc_lv 32 signal 4 } 
	{ real_o_0_address0 sc_out sc_lv 7 signal 5 } 
	{ real_o_0_ce0 sc_out sc_logic 1 signal 5 } 
	{ real_o_0_we0 sc_out sc_logic 1 signal 5 } 
	{ real_o_0_d0 sc_out sc_lv 32 signal 5 } 
	{ real_o_0_address1 sc_out sc_lv 7 signal 5 } 
	{ real_o_0_ce1 sc_out sc_logic 1 signal 5 } 
	{ real_o_0_we1 sc_out sc_logic 1 signal 5 } 
	{ real_o_0_d1 sc_out sc_lv 32 signal 5 } 
	{ real_o_1_address0 sc_out sc_lv 7 signal 6 } 
	{ real_o_1_ce0 sc_out sc_logic 1 signal 6 } 
	{ real_o_1_we0 sc_out sc_logic 1 signal 6 } 
	{ real_o_1_d0 sc_out sc_lv 32 signal 6 } 
	{ real_o_1_address1 sc_out sc_lv 7 signal 6 } 
	{ real_o_1_ce1 sc_out sc_logic 1 signal 6 } 
	{ real_o_1_we1 sc_out sc_logic 1 signal 6 } 
	{ real_o_1_d1 sc_out sc_lv 32 signal 6 } 
	{ real_o_2_address0 sc_out sc_lv 7 signal 7 } 
	{ real_o_2_ce0 sc_out sc_logic 1 signal 7 } 
	{ real_o_2_we0 sc_out sc_logic 1 signal 7 } 
	{ real_o_2_d0 sc_out sc_lv 32 signal 7 } 
	{ real_o_2_address1 sc_out sc_lv 7 signal 7 } 
	{ real_o_2_ce1 sc_out sc_logic 1 signal 7 } 
	{ real_o_2_we1 sc_out sc_logic 1 signal 7 } 
	{ real_o_2_d1 sc_out sc_lv 32 signal 7 } 
	{ real_o_3_address0 sc_out sc_lv 7 signal 8 } 
	{ real_o_3_ce0 sc_out sc_logic 1 signal 8 } 
	{ real_o_3_we0 sc_out sc_logic 1 signal 8 } 
	{ real_o_3_d0 sc_out sc_lv 32 signal 8 } 
	{ real_o_3_address1 sc_out sc_lv 7 signal 8 } 
	{ real_o_3_ce1 sc_out sc_logic 1 signal 8 } 
	{ real_o_3_we1 sc_out sc_logic 1 signal 8 } 
	{ real_o_3_d1 sc_out sc_lv 32 signal 8 } 
	{ real_o_4_address0 sc_out sc_lv 7 signal 9 } 
	{ real_o_4_ce0 sc_out sc_logic 1 signal 9 } 
	{ real_o_4_we0 sc_out sc_logic 1 signal 9 } 
	{ real_o_4_d0 sc_out sc_lv 32 signal 9 } 
	{ real_o_4_address1 sc_out sc_lv 7 signal 9 } 
	{ real_o_4_ce1 sc_out sc_logic 1 signal 9 } 
	{ real_o_4_we1 sc_out sc_logic 1 signal 9 } 
	{ real_o_4_d1 sc_out sc_lv 32 signal 9 } 
	{ real_o_5_address0 sc_out sc_lv 7 signal 10 } 
	{ real_o_5_ce0 sc_out sc_logic 1 signal 10 } 
	{ real_o_5_we0 sc_out sc_logic 1 signal 10 } 
	{ real_o_5_d0 sc_out sc_lv 32 signal 10 } 
	{ real_o_5_address1 sc_out sc_lv 7 signal 10 } 
	{ real_o_5_ce1 sc_out sc_logic 1 signal 10 } 
	{ real_o_5_we1 sc_out sc_logic 1 signal 10 } 
	{ real_o_5_d1 sc_out sc_lv 32 signal 10 } 
	{ real_o_6_address0 sc_out sc_lv 7 signal 11 } 
	{ real_o_6_ce0 sc_out sc_logic 1 signal 11 } 
	{ real_o_6_we0 sc_out sc_logic 1 signal 11 } 
	{ real_o_6_d0 sc_out sc_lv 32 signal 11 } 
	{ real_o_6_address1 sc_out sc_lv 7 signal 11 } 
	{ real_o_6_ce1 sc_out sc_logic 1 signal 11 } 
	{ real_o_6_we1 sc_out sc_logic 1 signal 11 } 
	{ real_o_6_d1 sc_out sc_lv 32 signal 11 } 
	{ real_o_7_address0 sc_out sc_lv 7 signal 12 } 
	{ real_o_7_ce0 sc_out sc_logic 1 signal 12 } 
	{ real_o_7_we0 sc_out sc_logic 1 signal 12 } 
	{ real_o_7_d0 sc_out sc_lv 32 signal 12 } 
	{ real_o_7_address1 sc_out sc_lv 7 signal 12 } 
	{ real_o_7_ce1 sc_out sc_logic 1 signal 12 } 
	{ real_o_7_we1 sc_out sc_logic 1 signal 12 } 
	{ real_o_7_d1 sc_out sc_lv 32 signal 12 } 
	{ imag_o_0_address0 sc_out sc_lv 7 signal 13 } 
	{ imag_o_0_ce0 sc_out sc_logic 1 signal 13 } 
	{ imag_o_0_we0 sc_out sc_logic 1 signal 13 } 
	{ imag_o_0_d0 sc_out sc_lv 32 signal 13 } 
	{ imag_o_0_address1 sc_out sc_lv 7 signal 13 } 
	{ imag_o_0_ce1 sc_out sc_logic 1 signal 13 } 
	{ imag_o_0_we1 sc_out sc_logic 1 signal 13 } 
	{ imag_o_0_d1 sc_out sc_lv 32 signal 13 } 
	{ imag_o_1_address0 sc_out sc_lv 7 signal 14 } 
	{ imag_o_1_ce0 sc_out sc_logic 1 signal 14 } 
	{ imag_o_1_we0 sc_out sc_logic 1 signal 14 } 
	{ imag_o_1_d0 sc_out sc_lv 32 signal 14 } 
	{ imag_o_1_address1 sc_out sc_lv 7 signal 14 } 
	{ imag_o_1_ce1 sc_out sc_logic 1 signal 14 } 
	{ imag_o_1_we1 sc_out sc_logic 1 signal 14 } 
	{ imag_o_1_d1 sc_out sc_lv 32 signal 14 } 
	{ imag_o_2_address0 sc_out sc_lv 7 signal 15 } 
	{ imag_o_2_ce0 sc_out sc_logic 1 signal 15 } 
	{ imag_o_2_we0 sc_out sc_logic 1 signal 15 } 
	{ imag_o_2_d0 sc_out sc_lv 32 signal 15 } 
	{ imag_o_2_address1 sc_out sc_lv 7 signal 15 } 
	{ imag_o_2_ce1 sc_out sc_logic 1 signal 15 } 
	{ imag_o_2_we1 sc_out sc_logic 1 signal 15 } 
	{ imag_o_2_d1 sc_out sc_lv 32 signal 15 } 
	{ imag_o_3_address0 sc_out sc_lv 7 signal 16 } 
	{ imag_o_3_ce0 sc_out sc_logic 1 signal 16 } 
	{ imag_o_3_we0 sc_out sc_logic 1 signal 16 } 
	{ imag_o_3_d0 sc_out sc_lv 32 signal 16 } 
	{ imag_o_3_address1 sc_out sc_lv 7 signal 16 } 
	{ imag_o_3_ce1 sc_out sc_logic 1 signal 16 } 
	{ imag_o_3_we1 sc_out sc_logic 1 signal 16 } 
	{ imag_o_3_d1 sc_out sc_lv 32 signal 16 } 
	{ imag_o_4_address0 sc_out sc_lv 7 signal 17 } 
	{ imag_o_4_ce0 sc_out sc_logic 1 signal 17 } 
	{ imag_o_4_we0 sc_out sc_logic 1 signal 17 } 
	{ imag_o_4_d0 sc_out sc_lv 32 signal 17 } 
	{ imag_o_4_address1 sc_out sc_lv 7 signal 17 } 
	{ imag_o_4_ce1 sc_out sc_logic 1 signal 17 } 
	{ imag_o_4_we1 sc_out sc_logic 1 signal 17 } 
	{ imag_o_4_d1 sc_out sc_lv 32 signal 17 } 
	{ imag_o_5_address0 sc_out sc_lv 7 signal 18 } 
	{ imag_o_5_ce0 sc_out sc_logic 1 signal 18 } 
	{ imag_o_5_we0 sc_out sc_logic 1 signal 18 } 
	{ imag_o_5_d0 sc_out sc_lv 32 signal 18 } 
	{ imag_o_5_address1 sc_out sc_lv 7 signal 18 } 
	{ imag_o_5_ce1 sc_out sc_logic 1 signal 18 } 
	{ imag_o_5_we1 sc_out sc_logic 1 signal 18 } 
	{ imag_o_5_d1 sc_out sc_lv 32 signal 18 } 
	{ imag_o_6_address0 sc_out sc_lv 7 signal 19 } 
	{ imag_o_6_ce0 sc_out sc_logic 1 signal 19 } 
	{ imag_o_6_we0 sc_out sc_logic 1 signal 19 } 
	{ imag_o_6_d0 sc_out sc_lv 32 signal 19 } 
	{ imag_o_6_address1 sc_out sc_lv 7 signal 19 } 
	{ imag_o_6_ce1 sc_out sc_logic 1 signal 19 } 
	{ imag_o_6_we1 sc_out sc_logic 1 signal 19 } 
	{ imag_o_6_d1 sc_out sc_lv 32 signal 19 } 
	{ imag_o_7_address0 sc_out sc_lv 7 signal 20 } 
	{ imag_o_7_ce0 sc_out sc_logic 1 signal 20 } 
	{ imag_o_7_we0 sc_out sc_logic 1 signal 20 } 
	{ imag_o_7_d0 sc_out sc_lv 32 signal 20 } 
	{ imag_o_7_address1 sc_out sc_lv 7 signal 20 } 
	{ imag_o_7_ce1 sc_out sc_logic 1 signal 20 } 
	{ imag_o_7_we1 sc_out sc_logic 1 signal 20 } 
	{ imag_o_7_d1 sc_out sc_lv 32 signal 20 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "real_i_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_i_0", "role": "address0" }} , 
 	{ "name": "real_i_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "ce0" }} , 
 	{ "name": "real_i_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "q0" }} , 
 	{ "name": "real_i_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_i_0", "role": "address1" }} , 
 	{ "name": "real_i_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "ce1" }} , 
 	{ "name": "real_i_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "q1" }} , 
 	{ "name": "real_i_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_i_1", "role": "address0" }} , 
 	{ "name": "real_i_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "ce0" }} , 
 	{ "name": "real_i_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "q0" }} , 
 	{ "name": "real_i_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_i_1", "role": "address1" }} , 
 	{ "name": "real_i_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "ce1" }} , 
 	{ "name": "real_i_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "q1" }} , 
 	{ "name": "real_i_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_i_2", "role": "address0" }} , 
 	{ "name": "real_i_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_2", "role": "ce0" }} , 
 	{ "name": "real_i_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_2", "role": "q0" }} , 
 	{ "name": "real_i_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_i_2", "role": "address1" }} , 
 	{ "name": "real_i_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_2", "role": "ce1" }} , 
 	{ "name": "real_i_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_2", "role": "q1" }} , 
 	{ "name": "real_i_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_i_3", "role": "address0" }} , 
 	{ "name": "real_i_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_3", "role": "ce0" }} , 
 	{ "name": "real_i_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_3", "role": "q0" }} , 
 	{ "name": "real_i_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_i_3", "role": "address1" }} , 
 	{ "name": "real_i_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_3", "role": "ce1" }} , 
 	{ "name": "real_i_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_3", "role": "q1" }} , 
 	{ "name": "imag_i_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":10, "type": "signal", "bundle":{"name": "imag_i", "role": "address0" }} , 
 	{ "name": "imag_i_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i", "role": "ce0" }} , 
 	{ "name": "imag_i_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i", "role": "q0" }} , 
 	{ "name": "imag_i_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":10, "type": "signal", "bundle":{"name": "imag_i", "role": "address1" }} , 
 	{ "name": "imag_i_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i", "role": "ce1" }} , 
 	{ "name": "imag_i_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i", "role": "q1" }} , 
 	{ "name": "real_o_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_o_0", "role": "address0" }} , 
 	{ "name": "real_o_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_0", "role": "ce0" }} , 
 	{ "name": "real_o_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_0", "role": "we0" }} , 
 	{ "name": "real_o_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_0", "role": "d0" }} , 
 	{ "name": "real_o_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_o_0", "role": "address1" }} , 
 	{ "name": "real_o_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_0", "role": "ce1" }} , 
 	{ "name": "real_o_0_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_0", "role": "we1" }} , 
 	{ "name": "real_o_0_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_0", "role": "d1" }} , 
 	{ "name": "real_o_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_o_1", "role": "address0" }} , 
 	{ "name": "real_o_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_1", "role": "ce0" }} , 
 	{ "name": "real_o_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_1", "role": "we0" }} , 
 	{ "name": "real_o_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_1", "role": "d0" }} , 
 	{ "name": "real_o_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_o_1", "role": "address1" }} , 
 	{ "name": "real_o_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_1", "role": "ce1" }} , 
 	{ "name": "real_o_1_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_1", "role": "we1" }} , 
 	{ "name": "real_o_1_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_1", "role": "d1" }} , 
 	{ "name": "real_o_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_o_2", "role": "address0" }} , 
 	{ "name": "real_o_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_2", "role": "ce0" }} , 
 	{ "name": "real_o_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_2", "role": "we0" }} , 
 	{ "name": "real_o_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_2", "role": "d0" }} , 
 	{ "name": "real_o_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_o_2", "role": "address1" }} , 
 	{ "name": "real_o_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_2", "role": "ce1" }} , 
 	{ "name": "real_o_2_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_2", "role": "we1" }} , 
 	{ "name": "real_o_2_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_2", "role": "d1" }} , 
 	{ "name": "real_o_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_o_3", "role": "address0" }} , 
 	{ "name": "real_o_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_3", "role": "ce0" }} , 
 	{ "name": "real_o_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_3", "role": "we0" }} , 
 	{ "name": "real_o_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_3", "role": "d0" }} , 
 	{ "name": "real_o_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_o_3", "role": "address1" }} , 
 	{ "name": "real_o_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_3", "role": "ce1" }} , 
 	{ "name": "real_o_3_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_3", "role": "we1" }} , 
 	{ "name": "real_o_3_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_3", "role": "d1" }} , 
 	{ "name": "real_o_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_o_4", "role": "address0" }} , 
 	{ "name": "real_o_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_4", "role": "ce0" }} , 
 	{ "name": "real_o_4_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_4", "role": "we0" }} , 
 	{ "name": "real_o_4_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_4", "role": "d0" }} , 
 	{ "name": "real_o_4_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_o_4", "role": "address1" }} , 
 	{ "name": "real_o_4_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_4", "role": "ce1" }} , 
 	{ "name": "real_o_4_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_4", "role": "we1" }} , 
 	{ "name": "real_o_4_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_4", "role": "d1" }} , 
 	{ "name": "real_o_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_o_5", "role": "address0" }} , 
 	{ "name": "real_o_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_5", "role": "ce0" }} , 
 	{ "name": "real_o_5_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_5", "role": "we0" }} , 
 	{ "name": "real_o_5_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_5", "role": "d0" }} , 
 	{ "name": "real_o_5_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_o_5", "role": "address1" }} , 
 	{ "name": "real_o_5_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_5", "role": "ce1" }} , 
 	{ "name": "real_o_5_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_5", "role": "we1" }} , 
 	{ "name": "real_o_5_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_5", "role": "d1" }} , 
 	{ "name": "real_o_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_o_6", "role": "address0" }} , 
 	{ "name": "real_o_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_6", "role": "ce0" }} , 
 	{ "name": "real_o_6_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_6", "role": "we0" }} , 
 	{ "name": "real_o_6_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_6", "role": "d0" }} , 
 	{ "name": "real_o_6_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_o_6", "role": "address1" }} , 
 	{ "name": "real_o_6_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_6", "role": "ce1" }} , 
 	{ "name": "real_o_6_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_6", "role": "we1" }} , 
 	{ "name": "real_o_6_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_6", "role": "d1" }} , 
 	{ "name": "real_o_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_o_7", "role": "address0" }} , 
 	{ "name": "real_o_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_7", "role": "ce0" }} , 
 	{ "name": "real_o_7_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_7", "role": "we0" }} , 
 	{ "name": "real_o_7_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_7", "role": "d0" }} , 
 	{ "name": "real_o_7_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_o_7", "role": "address1" }} , 
 	{ "name": "real_o_7_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_7", "role": "ce1" }} , 
 	{ "name": "real_o_7_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_7", "role": "we1" }} , 
 	{ "name": "real_o_7_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_7", "role": "d1" }} , 
 	{ "name": "imag_o_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_o_0", "role": "address0" }} , 
 	{ "name": "imag_o_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_0", "role": "ce0" }} , 
 	{ "name": "imag_o_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_0", "role": "we0" }} , 
 	{ "name": "imag_o_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_0", "role": "d0" }} , 
 	{ "name": "imag_o_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_o_0", "role": "address1" }} , 
 	{ "name": "imag_o_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_0", "role": "ce1" }} , 
 	{ "name": "imag_o_0_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_0", "role": "we1" }} , 
 	{ "name": "imag_o_0_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_0", "role": "d1" }} , 
 	{ "name": "imag_o_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_o_1", "role": "address0" }} , 
 	{ "name": "imag_o_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_1", "role": "ce0" }} , 
 	{ "name": "imag_o_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_1", "role": "we0" }} , 
 	{ "name": "imag_o_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_1", "role": "d0" }} , 
 	{ "name": "imag_o_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_o_1", "role": "address1" }} , 
 	{ "name": "imag_o_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_1", "role": "ce1" }} , 
 	{ "name": "imag_o_1_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_1", "role": "we1" }} , 
 	{ "name": "imag_o_1_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_1", "role": "d1" }} , 
 	{ "name": "imag_o_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_o_2", "role": "address0" }} , 
 	{ "name": "imag_o_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_2", "role": "ce0" }} , 
 	{ "name": "imag_o_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_2", "role": "we0" }} , 
 	{ "name": "imag_o_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_2", "role": "d0" }} , 
 	{ "name": "imag_o_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_o_2", "role": "address1" }} , 
 	{ "name": "imag_o_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_2", "role": "ce1" }} , 
 	{ "name": "imag_o_2_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_2", "role": "we1" }} , 
 	{ "name": "imag_o_2_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_2", "role": "d1" }} , 
 	{ "name": "imag_o_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_o_3", "role": "address0" }} , 
 	{ "name": "imag_o_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_3", "role": "ce0" }} , 
 	{ "name": "imag_o_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_3", "role": "we0" }} , 
 	{ "name": "imag_o_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_3", "role": "d0" }} , 
 	{ "name": "imag_o_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_o_3", "role": "address1" }} , 
 	{ "name": "imag_o_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_3", "role": "ce1" }} , 
 	{ "name": "imag_o_3_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_3", "role": "we1" }} , 
 	{ "name": "imag_o_3_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_3", "role": "d1" }} , 
 	{ "name": "imag_o_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_o_4", "role": "address0" }} , 
 	{ "name": "imag_o_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_4", "role": "ce0" }} , 
 	{ "name": "imag_o_4_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_4", "role": "we0" }} , 
 	{ "name": "imag_o_4_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_4", "role": "d0" }} , 
 	{ "name": "imag_o_4_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_o_4", "role": "address1" }} , 
 	{ "name": "imag_o_4_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_4", "role": "ce1" }} , 
 	{ "name": "imag_o_4_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_4", "role": "we1" }} , 
 	{ "name": "imag_o_4_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_4", "role": "d1" }} , 
 	{ "name": "imag_o_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_o_5", "role": "address0" }} , 
 	{ "name": "imag_o_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_5", "role": "ce0" }} , 
 	{ "name": "imag_o_5_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_5", "role": "we0" }} , 
 	{ "name": "imag_o_5_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_5", "role": "d0" }} , 
 	{ "name": "imag_o_5_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_o_5", "role": "address1" }} , 
 	{ "name": "imag_o_5_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_5", "role": "ce1" }} , 
 	{ "name": "imag_o_5_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_5", "role": "we1" }} , 
 	{ "name": "imag_o_5_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_5", "role": "d1" }} , 
 	{ "name": "imag_o_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_o_6", "role": "address0" }} , 
 	{ "name": "imag_o_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_6", "role": "ce0" }} , 
 	{ "name": "imag_o_6_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_6", "role": "we0" }} , 
 	{ "name": "imag_o_6_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_6", "role": "d0" }} , 
 	{ "name": "imag_o_6_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_o_6", "role": "address1" }} , 
 	{ "name": "imag_o_6_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_6", "role": "ce1" }} , 
 	{ "name": "imag_o_6_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_6", "role": "we1" }} , 
 	{ "name": "imag_o_6_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_6", "role": "d1" }} , 
 	{ "name": "imag_o_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_o_7", "role": "address0" }} , 
 	{ "name": "imag_o_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_7", "role": "ce0" }} , 
 	{ "name": "imag_o_7_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_7", "role": "we0" }} , 
 	{ "name": "imag_o_7_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_7", "role": "d0" }} , 
 	{ "name": "imag_o_7_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_o_7", "role": "address1" }} , 
 	{ "name": "imag_o_7_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_7", "role": "ce1" }} , 
 	{ "name": "imag_o_7_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_7", "role": "we1" }} , 
 	{ "name": "imag_o_7_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_7", "role": "d1" }}  ]}
set Spec2ImplPortList { 
	real_i_0 { ap_memory {  { real_i_0_address0 mem_address 1 8 }  { real_i_0_ce0 mem_ce 1 1 }  { real_i_0_q0 mem_dout 0 32 }  { real_i_0_address1 mem_address 1 8 }  { real_i_0_ce1 mem_ce 1 1 }  { real_i_0_q1 mem_dout 0 32 } } }
	real_i_1 { ap_memory {  { real_i_1_address0 mem_address 1 8 }  { real_i_1_ce0 mem_ce 1 1 }  { real_i_1_q0 mem_dout 0 32 }  { real_i_1_address1 mem_address 1 8 }  { real_i_1_ce1 mem_ce 1 1 }  { real_i_1_q1 mem_dout 0 32 } } }
	real_i_2 { ap_memory {  { real_i_2_address0 mem_address 1 8 }  { real_i_2_ce0 mem_ce 1 1 }  { real_i_2_q0 mem_dout 0 32 }  { real_i_2_address1 mem_address 1 8 }  { real_i_2_ce1 mem_ce 1 1 }  { real_i_2_q1 mem_dout 0 32 } } }
	real_i_3 { ap_memory {  { real_i_3_address0 mem_address 1 8 }  { real_i_3_ce0 mem_ce 1 1 }  { real_i_3_q0 mem_dout 0 32 }  { real_i_3_address1 mem_address 1 8 }  { real_i_3_ce1 mem_ce 1 1 }  { real_i_3_q1 mem_dout 0 32 } } }
	imag_i { ap_memory {  { imag_i_address0 mem_address 1 10 }  { imag_i_ce0 mem_ce 1 1 }  { imag_i_q0 mem_dout 0 32 }  { imag_i_address1 mem_address 1 10 }  { imag_i_ce1 mem_ce 1 1 }  { imag_i_q1 mem_dout 0 32 } } }
	real_o_0 { ap_memory {  { real_o_0_address0 mem_address 1 7 }  { real_o_0_ce0 mem_ce 1 1 }  { real_o_0_we0 mem_we 1 1 }  { real_o_0_d0 mem_din 1 32 }  { real_o_0_address1 mem_address 1 7 }  { real_o_0_ce1 mem_ce 1 1 }  { real_o_0_we1 mem_we 1 1 }  { real_o_0_d1 mem_din 1 32 } } }
	real_o_1 { ap_memory {  { real_o_1_address0 mem_address 1 7 }  { real_o_1_ce0 mem_ce 1 1 }  { real_o_1_we0 mem_we 1 1 }  { real_o_1_d0 mem_din 1 32 }  { real_o_1_address1 mem_address 1 7 }  { real_o_1_ce1 mem_ce 1 1 }  { real_o_1_we1 mem_we 1 1 }  { real_o_1_d1 mem_din 1 32 } } }
	real_o_2 { ap_memory {  { real_o_2_address0 mem_address 1 7 }  { real_o_2_ce0 mem_ce 1 1 }  { real_o_2_we0 mem_we 1 1 }  { real_o_2_d0 mem_din 1 32 }  { real_o_2_address1 mem_address 1 7 }  { real_o_2_ce1 mem_ce 1 1 }  { real_o_2_we1 mem_we 1 1 }  { real_o_2_d1 mem_din 1 32 } } }
	real_o_3 { ap_memory {  { real_o_3_address0 mem_address 1 7 }  { real_o_3_ce0 mem_ce 1 1 }  { real_o_3_we0 mem_we 1 1 }  { real_o_3_d0 mem_din 1 32 }  { real_o_3_address1 mem_address 1 7 }  { real_o_3_ce1 mem_ce 1 1 }  { real_o_3_we1 mem_we 1 1 }  { real_o_3_d1 mem_din 1 32 } } }
	real_o_4 { ap_memory {  { real_o_4_address0 mem_address 1 7 }  { real_o_4_ce0 mem_ce 1 1 }  { real_o_4_we0 mem_we 1 1 }  { real_o_4_d0 mem_din 1 32 }  { real_o_4_address1 mem_address 1 7 }  { real_o_4_ce1 mem_ce 1 1 }  { real_o_4_we1 mem_we 1 1 }  { real_o_4_d1 mem_din 1 32 } } }
	real_o_5 { ap_memory {  { real_o_5_address0 mem_address 1 7 }  { real_o_5_ce0 mem_ce 1 1 }  { real_o_5_we0 mem_we 1 1 }  { real_o_5_d0 mem_din 1 32 }  { real_o_5_address1 mem_address 1 7 }  { real_o_5_ce1 mem_ce 1 1 }  { real_o_5_we1 mem_we 1 1 }  { real_o_5_d1 mem_din 1 32 } } }
	real_o_6 { ap_memory {  { real_o_6_address0 mem_address 1 7 }  { real_o_6_ce0 mem_ce 1 1 }  { real_o_6_we0 mem_we 1 1 }  { real_o_6_d0 mem_din 1 32 }  { real_o_6_address1 mem_address 1 7 }  { real_o_6_ce1 mem_ce 1 1 }  { real_o_6_we1 mem_we 1 1 }  { real_o_6_d1 mem_din 1 32 } } }
	real_o_7 { ap_memory {  { real_o_7_address0 mem_address 1 7 }  { real_o_7_ce0 mem_ce 1 1 }  { real_o_7_we0 mem_we 1 1 }  { real_o_7_d0 mem_din 1 32 }  { real_o_7_address1 mem_address 1 7 }  { real_o_7_ce1 mem_ce 1 1 }  { real_o_7_we1 mem_we 1 1 }  { real_o_7_d1 mem_din 1 32 } } }
	imag_o_0 { ap_memory {  { imag_o_0_address0 mem_address 1 7 }  { imag_o_0_ce0 mem_ce 1 1 }  { imag_o_0_we0 mem_we 1 1 }  { imag_o_0_d0 mem_din 1 32 }  { imag_o_0_address1 mem_address 1 7 }  { imag_o_0_ce1 mem_ce 1 1 }  { imag_o_0_we1 mem_we 1 1 }  { imag_o_0_d1 mem_din 1 32 } } }
	imag_o_1 { ap_memory {  { imag_o_1_address0 mem_address 1 7 }  { imag_o_1_ce0 mem_ce 1 1 }  { imag_o_1_we0 mem_we 1 1 }  { imag_o_1_d0 mem_din 1 32 }  { imag_o_1_address1 mem_address 1 7 }  { imag_o_1_ce1 mem_ce 1 1 }  { imag_o_1_we1 mem_we 1 1 }  { imag_o_1_d1 mem_din 1 32 } } }
	imag_o_2 { ap_memory {  { imag_o_2_address0 mem_address 1 7 }  { imag_o_2_ce0 mem_ce 1 1 }  { imag_o_2_we0 mem_we 1 1 }  { imag_o_2_d0 mem_din 1 32 }  { imag_o_2_address1 mem_address 1 7 }  { imag_o_2_ce1 mem_ce 1 1 }  { imag_o_2_we1 mem_we 1 1 }  { imag_o_2_d1 mem_din 1 32 } } }
	imag_o_3 { ap_memory {  { imag_o_3_address0 mem_address 1 7 }  { imag_o_3_ce0 mem_ce 1 1 }  { imag_o_3_we0 mem_we 1 1 }  { imag_o_3_d0 mem_din 1 32 }  { imag_o_3_address1 mem_address 1 7 }  { imag_o_3_ce1 mem_ce 1 1 }  { imag_o_3_we1 mem_we 1 1 }  { imag_o_3_d1 mem_din 1 32 } } }
	imag_o_4 { ap_memory {  { imag_o_4_address0 mem_address 1 7 }  { imag_o_4_ce0 mem_ce 1 1 }  { imag_o_4_we0 mem_we 1 1 }  { imag_o_4_d0 mem_din 1 32 }  { imag_o_4_address1 mem_address 1 7 }  { imag_o_4_ce1 mem_ce 1 1 }  { imag_o_4_we1 mem_we 1 1 }  { imag_o_4_d1 mem_din 1 32 } } }
	imag_o_5 { ap_memory {  { imag_o_5_address0 mem_address 1 7 }  { imag_o_5_ce0 mem_ce 1 1 }  { imag_o_5_we0 mem_we 1 1 }  { imag_o_5_d0 mem_din 1 32 }  { imag_o_5_address1 mem_address 1 7 }  { imag_o_5_ce1 mem_ce 1 1 }  { imag_o_5_we1 mem_we 1 1 }  { imag_o_5_d1 mem_din 1 32 } } }
	imag_o_6 { ap_memory {  { imag_o_6_address0 mem_address 1 7 }  { imag_o_6_ce0 mem_ce 1 1 }  { imag_o_6_we0 mem_we 1 1 }  { imag_o_6_d0 mem_din 1 32 }  { imag_o_6_address1 mem_address 1 7 }  { imag_o_6_ce1 mem_ce 1 1 }  { imag_o_6_we1 mem_we 1 1 }  { imag_o_6_d1 mem_din 1 32 } } }
	imag_o_7 { ap_memory {  { imag_o_7_address0 mem_address 1 7 }  { imag_o_7_ce0 mem_ce 1 1 }  { imag_o_7_we0 mem_we 1 1 }  { imag_o_7_d0 mem_din 1 32 }  { imag_o_7_address1 mem_address 1 7 }  { imag_o_7_ce1 mem_ce 1 1 }  { imag_o_7_we1 mem_we 1 1 }  { imag_o_7_d1 mem_din 1 32 } } }
}
