# This script segment is generated automatically by AutoPilot

# Memory (RAM/ROM)  definition:
set ID 227
set MemName fft_Stage0_R_0_memcore
set CoreName ap_simcore_mem
set PortList { 2 2 }
set DataWd 32
set AddrRange 256
set AddrWd 8
set impl_style block
set TrueReset 0
set HasInitializer 0
set IsROM 0
set ROMData {}
set NumOfStage 2
set MaxLatency -1
set DelayBudget 2.71
set ClkPeriod 10
set RegisteredInput 0
set memSimGenFunc ap_gen_simcore_mem
set memImplGenFunc ::AESL_LIB_VIRTEX::xil_gen_RAM
eval "set memGenArgs  { \
    id ${ID} \
    name ${MemName} \
    corename ${CoreName}  \
    op mem \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage}  \
    registered_input ${RegisteredInput} \
    port_num 2 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    style ${impl_style} \
    true_reset ${TrueReset} \
    delay_budget ${DelayBudget} \
    clk_period ${ClkPeriod} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
 } "

set Depth 2
set CoreName ap_simcore_mem_df_channel
set MemName fft_Stage0_R_0
if {${::AESL::PGuard_autocg_gen} || ${::AESL::PGuard_simmodel_gen}} {
if {[info proc ap_gen_simcore_mem_df_channel] == "ap_gen_simcore_mem_df_channel"} {
    eval "ap_gen_simcore_mem_df_channel { \
    id ${ID} \
    name ${MemName} \
    memcorename ${MemName}_memcore \
    corename ${CoreName} \
    op mem_df_channel \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage} \
    max_latency ${MaxLatency} \
    registered_input ${RegisteredInput} \
    port_num 2 \
    use_pre_full 1 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    true_reset ${TrueReset} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
    depth ${Depth} \
    memSimGenFunc $memSimGenFunc\
    memImplGenFunc $memImplGenFunc\
    memGenArgs \{$memGenArgs\} \
} "
} else {
puts "@W \[IMPL-105\] Cannot find ap_gen_simcore_mem_df_channel, check your platform lib"
}
}


# Memory (RAM/ROM)  definition:
set ID 228
set MemName fft_Stage0_I_memcore
set CoreName ap_simcore_mem
set PortList { 2 2 }
set DataWd 32
set AddrRange 1024
set AddrWd 10
set impl_style block
set TrueReset 0
set HasInitializer 0
set IsROM 0
set ROMData {}
set NumOfStage 2
set MaxLatency -1
set DelayBudget 2.71
set ClkPeriod 10
set RegisteredInput 0
set memSimGenFunc ap_gen_simcore_mem
set memImplGenFunc ::AESL_LIB_VIRTEX::xil_gen_RAM
eval "set memGenArgs  { \
    id ${ID} \
    name ${MemName} \
    corename ${CoreName}  \
    op mem \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage}  \
    registered_input ${RegisteredInput} \
    port_num 2 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    style ${impl_style} \
    true_reset ${TrueReset} \
    delay_budget ${DelayBudget} \
    clk_period ${ClkPeriod} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
 } "

set Depth 2
set CoreName ap_simcore_mem_df_channel
set MemName fft_Stage0_I
if {${::AESL::PGuard_autocg_gen} || ${::AESL::PGuard_simmodel_gen}} {
if {[info proc ap_gen_simcore_mem_df_channel] == "ap_gen_simcore_mem_df_channel"} {
    eval "ap_gen_simcore_mem_df_channel { \
    id ${ID} \
    name ${MemName} \
    memcorename ${MemName}_memcore \
    corename ${CoreName} \
    op mem_df_channel \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage} \
    max_latency ${MaxLatency} \
    registered_input ${RegisteredInput} \
    port_num 2 \
    use_pre_full 1 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    true_reset ${TrueReset} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
    depth ${Depth} \
    memSimGenFunc $memSimGenFunc\
    memImplGenFunc $memImplGenFunc\
    memGenArgs \{$memGenArgs\} \
} "
} else {
puts "@W \[IMPL-105\] Cannot find ap_gen_simcore_mem_df_channel, check your platform lib"
}
}


# Memory (RAM/ROM)  definition:
set ID 229
set MemName fft_Stage1_R_0_memcore
set CoreName ap_simcore_mem
set PortList { 2 1 }
set DataWd 32
set AddrRange 256
set AddrWd 8
set impl_style block
set TrueReset 0
set HasInitializer 0
set IsROM 0
set ROMData {}
set NumOfStage 2
set MaxLatency -1
set DelayBudget 2.71
set ClkPeriod 10
set RegisteredInput 0
set memSimGenFunc ap_gen_simcore_mem
set memImplGenFunc ::AESL_LIB_VIRTEX::xil_gen_RAM
eval "set memGenArgs  { \
    id ${ID} \
    name ${MemName} \
    corename ${CoreName}  \
    op mem \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage}  \
    registered_input ${RegisteredInput} \
    port_num 2 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    style ${impl_style} \
    true_reset ${TrueReset} \
    delay_budget ${DelayBudget} \
    clk_period ${ClkPeriod} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
 } "

set Depth 2
set CoreName ap_simcore_mem_df_channel
set MemName fft_Stage1_R_0
if {${::AESL::PGuard_autocg_gen} || ${::AESL::PGuard_simmodel_gen}} {
if {[info proc ap_gen_simcore_mem_df_channel] == "ap_gen_simcore_mem_df_channel"} {
    eval "ap_gen_simcore_mem_df_channel { \
    id ${ID} \
    name ${MemName} \
    memcorename ${MemName}_memcore \
    corename ${CoreName} \
    op mem_df_channel \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage} \
    max_latency ${MaxLatency} \
    registered_input ${RegisteredInput} \
    port_num 2 \
    use_pre_full 1 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    true_reset ${TrueReset} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
    depth ${Depth} \
    memSimGenFunc $memSimGenFunc\
    memImplGenFunc $memImplGenFunc\
    memGenArgs \{$memGenArgs\} \
} "
} else {
puts "@W \[IMPL-105\] Cannot find ap_gen_simcore_mem_df_channel, check your platform lib"
}
}


# Memory (RAM/ROM)  definition:
set ID 230
set MemName fft_Stage2_R_0_memcore
set CoreName ap_simcore_mem
set PortList { 2 3 }
set DataWd 32
set AddrRange 256
set AddrWd 8
set impl_style block
set TrueReset 0
set HasInitializer 0
set IsROM 0
set ROMData {}
set NumOfStage 2
set MaxLatency -1
set DelayBudget 2.71
set ClkPeriod 10
set RegisteredInput 0
set memSimGenFunc ap_gen_simcore_mem
set memImplGenFunc ::AESL_LIB_VIRTEX::xil_gen_RAM
eval "set memGenArgs  { \
    id ${ID} \
    name ${MemName} \
    corename ${CoreName}  \
    op mem \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage}  \
    registered_input ${RegisteredInput} \
    port_num 2 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    style ${impl_style} \
    true_reset ${TrueReset} \
    delay_budget ${DelayBudget} \
    clk_period ${ClkPeriod} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
 } "

set Depth 2
set CoreName ap_simcore_mem_df_channel
set MemName fft_Stage2_R_0
if {${::AESL::PGuard_autocg_gen} || ${::AESL::PGuard_simmodel_gen}} {
if {[info proc ap_gen_simcore_mem_df_channel] == "ap_gen_simcore_mem_df_channel"} {
    eval "ap_gen_simcore_mem_df_channel { \
    id ${ID} \
    name ${MemName} \
    memcorename ${MemName}_memcore \
    corename ${CoreName} \
    op mem_df_channel \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage} \
    max_latency ${MaxLatency} \
    registered_input ${RegisteredInput} \
    port_num 2 \
    use_pre_full 1 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    true_reset ${TrueReset} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
    depth ${Depth} \
    memSimGenFunc $memSimGenFunc\
    memImplGenFunc $memImplGenFunc\
    memGenArgs \{$memGenArgs\} \
} "
} else {
puts "@W \[IMPL-105\] Cannot find ap_gen_simcore_mem_df_channel, check your platform lib"
}
}


# Memory (RAM/ROM)  definition:
set ID 231
set MemName fft_Stage2_I_memcore
set CoreName ap_simcore_mem
set PortList { 2 0 }
set DataWd 32
set AddrRange 1024
set AddrWd 10
set impl_style block
set TrueReset 0
set HasInitializer 0
set IsROM 0
set ROMData {}
set NumOfStage 2
set MaxLatency -1
set DelayBudget 2.71
set ClkPeriod 10
set RegisteredInput 0
set memSimGenFunc ap_gen_simcore_mem
set memImplGenFunc ::AESL_LIB_VIRTEX::xil_gen_RAM
eval "set memGenArgs  { \
    id ${ID} \
    name ${MemName} \
    corename ${CoreName}  \
    op mem \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage}  \
    registered_input ${RegisteredInput} \
    port_num 2 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    style ${impl_style} \
    true_reset ${TrueReset} \
    delay_budget ${DelayBudget} \
    clk_period ${ClkPeriod} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
 } "

set Depth 2
set CoreName ap_simcore_mem_df_channel
set MemName fft_Stage2_I
if {${::AESL::PGuard_autocg_gen} || ${::AESL::PGuard_simmodel_gen}} {
if {[info proc ap_gen_simcore_mem_df_channel] == "ap_gen_simcore_mem_df_channel"} {
    eval "ap_gen_simcore_mem_df_channel { \
    id ${ID} \
    name ${MemName} \
    memcorename ${MemName}_memcore \
    corename ${CoreName} \
    op mem_df_channel \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage} \
    max_latency ${MaxLatency} \
    registered_input ${RegisteredInput} \
    port_num 2 \
    use_pre_full 1 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    true_reset ${TrueReset} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
    depth ${Depth} \
    memSimGenFunc $memSimGenFunc\
    memImplGenFunc $memImplGenFunc\
    memGenArgs \{$memGenArgs\} \
} "
} else {
puts "@W \[IMPL-105\] Cannot find ap_gen_simcore_mem_df_channel, check your platform lib"
}
}


# Memory (RAM/ROM)  definition:
set ID 232
set MemName fft_Stage3_I_memcore
set CoreName ap_simcore_mem
set PortList { 2 3 }
set DataWd 32
set AddrRange 1024
set AddrWd 10
set impl_style block
set TrueReset 0
set HasInitializer 0
set IsROM 0
set ROMData {}
set NumOfStage 2
set MaxLatency -1
set DelayBudget 2.71
set ClkPeriod 10
set RegisteredInput 0
set memSimGenFunc ap_gen_simcore_mem
set memImplGenFunc ::AESL_LIB_VIRTEX::xil_gen_RAM
eval "set memGenArgs  { \
    id ${ID} \
    name ${MemName} \
    corename ${CoreName}  \
    op mem \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage}  \
    registered_input ${RegisteredInput} \
    port_num 2 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    style ${impl_style} \
    true_reset ${TrueReset} \
    delay_budget ${DelayBudget} \
    clk_period ${ClkPeriod} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
 } "

set Depth 2
set CoreName ap_simcore_mem_df_channel
set MemName fft_Stage3_I
if {${::AESL::PGuard_autocg_gen} || ${::AESL::PGuard_simmodel_gen}} {
if {[info proc ap_gen_simcore_mem_df_channel] == "ap_gen_simcore_mem_df_channel"} {
    eval "ap_gen_simcore_mem_df_channel { \
    id ${ID} \
    name ${MemName} \
    memcorename ${MemName}_memcore \
    corename ${CoreName} \
    op mem_df_channel \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage} \
    max_latency ${MaxLatency} \
    registered_input ${RegisteredInput} \
    port_num 2 \
    use_pre_full 1 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    true_reset ${TrueReset} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
    depth ${Depth} \
    memSimGenFunc $memSimGenFunc\
    memImplGenFunc $memImplGenFunc\
    memGenArgs \{$memGenArgs\} \
} "
} else {
puts "@W \[IMPL-105\] Cannot find ap_gen_simcore_mem_df_channel, check your platform lib"
}
}


# Memory (RAM/ROM)  definition:
set ID 233
set MemName fft_Stage9_I_memcore
set CoreName ap_simcore_mem
set PortList { 2 1 }
set DataWd 32
set AddrRange 1024
set AddrWd 10
set impl_style block
set TrueReset 0
set HasInitializer 0
set IsROM 0
set ROMData {}
set NumOfStage 2
set MaxLatency -1
set DelayBudget 2.71
set ClkPeriod 10
set RegisteredInput 0
set memSimGenFunc ap_gen_simcore_mem
set memImplGenFunc ::AESL_LIB_VIRTEX::xil_gen_RAM
eval "set memGenArgs  { \
    id ${ID} \
    name ${MemName} \
    corename ${CoreName}  \
    op mem \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage}  \
    registered_input ${RegisteredInput} \
    port_num 2 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    style ${impl_style} \
    true_reset ${TrueReset} \
    delay_budget ${DelayBudget} \
    clk_period ${ClkPeriod} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
 } "

set Depth 2
set CoreName ap_simcore_mem_df_channel
set MemName fft_Stage9_I
if {${::AESL::PGuard_autocg_gen} || ${::AESL::PGuard_simmodel_gen}} {
if {[info proc ap_gen_simcore_mem_df_channel] == "ap_gen_simcore_mem_df_channel"} {
    eval "ap_gen_simcore_mem_df_channel { \
    id ${ID} \
    name ${MemName} \
    memcorename ${MemName}_memcore \
    corename ${CoreName} \
    op mem_df_channel \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage} \
    max_latency ${MaxLatency} \
    registered_input ${RegisteredInput} \
    port_num 2 \
    use_pre_full 1 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    true_reset ${TrueReset} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
    depth ${Depth} \
    memSimGenFunc $memSimGenFunc\
    memImplGenFunc $memImplGenFunc\
    memGenArgs \{$memGenArgs\} \
} "
} else {
puts "@W \[IMPL-105\] Cannot find ap_gen_simcore_mem_df_channel, check your platform lib"
}
}


# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 234 \
    name real_i_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_0 \
    op interface \
    ports { real_i_0_address0 { O 7 vector } real_i_0_ce0 { O 1 bit } real_i_0_d0 { O 32 vector } real_i_0_q0 { I 32 vector } real_i_0_we0 { O 1 bit } real_i_0_address1 { O 7 vector } real_i_0_ce1 { O 1 bit } real_i_0_d1 { O 32 vector } real_i_0_q1 { I 32 vector } real_i_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 235 \
    name real_i_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_1 \
    op interface \
    ports { real_i_1_address0 { O 7 vector } real_i_1_ce0 { O 1 bit } real_i_1_d0 { O 32 vector } real_i_1_q0 { I 32 vector } real_i_1_we0 { O 1 bit } real_i_1_address1 { O 7 vector } real_i_1_ce1 { O 1 bit } real_i_1_d1 { O 32 vector } real_i_1_q1 { I 32 vector } real_i_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 236 \
    name real_i_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_2 \
    op interface \
    ports { real_i_2_address0 { O 7 vector } real_i_2_ce0 { O 1 bit } real_i_2_d0 { O 32 vector } real_i_2_q0 { I 32 vector } real_i_2_we0 { O 1 bit } real_i_2_address1 { O 7 vector } real_i_2_ce1 { O 1 bit } real_i_2_d1 { O 32 vector } real_i_2_q1 { I 32 vector } real_i_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 237 \
    name real_i_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_3 \
    op interface \
    ports { real_i_3_address0 { O 7 vector } real_i_3_ce0 { O 1 bit } real_i_3_d0 { O 32 vector } real_i_3_q0 { I 32 vector } real_i_3_we0 { O 1 bit } real_i_3_address1 { O 7 vector } real_i_3_ce1 { O 1 bit } real_i_3_d1 { O 32 vector } real_i_3_q1 { I 32 vector } real_i_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 238 \
    name real_i_4 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_4 \
    op interface \
    ports { real_i_4_address0 { O 7 vector } real_i_4_ce0 { O 1 bit } real_i_4_d0 { O 32 vector } real_i_4_q0 { I 32 vector } real_i_4_we0 { O 1 bit } real_i_4_address1 { O 7 vector } real_i_4_ce1 { O 1 bit } real_i_4_d1 { O 32 vector } real_i_4_q1 { I 32 vector } real_i_4_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_4'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 239 \
    name real_i_5 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_5 \
    op interface \
    ports { real_i_5_address0 { O 7 vector } real_i_5_ce0 { O 1 bit } real_i_5_d0 { O 32 vector } real_i_5_q0 { I 32 vector } real_i_5_we0 { O 1 bit } real_i_5_address1 { O 7 vector } real_i_5_ce1 { O 1 bit } real_i_5_d1 { O 32 vector } real_i_5_q1 { I 32 vector } real_i_5_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_5'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 240 \
    name real_i_6 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_6 \
    op interface \
    ports { real_i_6_address0 { O 7 vector } real_i_6_ce0 { O 1 bit } real_i_6_d0 { O 32 vector } real_i_6_q0 { I 32 vector } real_i_6_we0 { O 1 bit } real_i_6_address1 { O 7 vector } real_i_6_ce1 { O 1 bit } real_i_6_d1 { O 32 vector } real_i_6_q1 { I 32 vector } real_i_6_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_6'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 241 \
    name real_i_7 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_7 \
    op interface \
    ports { real_i_7_address0 { O 7 vector } real_i_7_ce0 { O 1 bit } real_i_7_d0 { O 32 vector } real_i_7_q0 { I 32 vector } real_i_7_we0 { O 1 bit } real_i_7_address1 { O 7 vector } real_i_7_ce1 { O 1 bit } real_i_7_d1 { O 32 vector } real_i_7_q1 { I 32 vector } real_i_7_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_7'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 242 \
    name imag_i_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_0 \
    op interface \
    ports { imag_i_0_address0 { O 7 vector } imag_i_0_ce0 { O 1 bit } imag_i_0_d0 { O 32 vector } imag_i_0_q0 { I 32 vector } imag_i_0_we0 { O 1 bit } imag_i_0_address1 { O 7 vector } imag_i_0_ce1 { O 1 bit } imag_i_0_d1 { O 32 vector } imag_i_0_q1 { I 32 vector } imag_i_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 243 \
    name imag_i_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_1 \
    op interface \
    ports { imag_i_1_address0 { O 7 vector } imag_i_1_ce0 { O 1 bit } imag_i_1_d0 { O 32 vector } imag_i_1_q0 { I 32 vector } imag_i_1_we0 { O 1 bit } imag_i_1_address1 { O 7 vector } imag_i_1_ce1 { O 1 bit } imag_i_1_d1 { O 32 vector } imag_i_1_q1 { I 32 vector } imag_i_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 244 \
    name imag_i_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_2 \
    op interface \
    ports { imag_i_2_address0 { O 7 vector } imag_i_2_ce0 { O 1 bit } imag_i_2_d0 { O 32 vector } imag_i_2_q0 { I 32 vector } imag_i_2_we0 { O 1 bit } imag_i_2_address1 { O 7 vector } imag_i_2_ce1 { O 1 bit } imag_i_2_d1 { O 32 vector } imag_i_2_q1 { I 32 vector } imag_i_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 245 \
    name imag_i_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_3 \
    op interface \
    ports { imag_i_3_address0 { O 7 vector } imag_i_3_ce0 { O 1 bit } imag_i_3_d0 { O 32 vector } imag_i_3_q0 { I 32 vector } imag_i_3_we0 { O 1 bit } imag_i_3_address1 { O 7 vector } imag_i_3_ce1 { O 1 bit } imag_i_3_d1 { O 32 vector } imag_i_3_q1 { I 32 vector } imag_i_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 246 \
    name imag_i_4 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_4 \
    op interface \
    ports { imag_i_4_address0 { O 7 vector } imag_i_4_ce0 { O 1 bit } imag_i_4_d0 { O 32 vector } imag_i_4_q0 { I 32 vector } imag_i_4_we0 { O 1 bit } imag_i_4_address1 { O 7 vector } imag_i_4_ce1 { O 1 bit } imag_i_4_d1 { O 32 vector } imag_i_4_q1 { I 32 vector } imag_i_4_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_4'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 247 \
    name imag_i_5 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_5 \
    op interface \
    ports { imag_i_5_address0 { O 7 vector } imag_i_5_ce0 { O 1 bit } imag_i_5_d0 { O 32 vector } imag_i_5_q0 { I 32 vector } imag_i_5_we0 { O 1 bit } imag_i_5_address1 { O 7 vector } imag_i_5_ce1 { O 1 bit } imag_i_5_d1 { O 32 vector } imag_i_5_q1 { I 32 vector } imag_i_5_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_5'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 248 \
    name imag_i_6 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_6 \
    op interface \
    ports { imag_i_6_address0 { O 7 vector } imag_i_6_ce0 { O 1 bit } imag_i_6_d0 { O 32 vector } imag_i_6_q0 { I 32 vector } imag_i_6_we0 { O 1 bit } imag_i_6_address1 { O 7 vector } imag_i_6_ce1 { O 1 bit } imag_i_6_d1 { O 32 vector } imag_i_6_q1 { I 32 vector } imag_i_6_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_6'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 249 \
    name imag_i_7 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_7 \
    op interface \
    ports { imag_i_7_address0 { O 7 vector } imag_i_7_ce0 { O 1 bit } imag_i_7_d0 { O 32 vector } imag_i_7_q0 { I 32 vector } imag_i_7_we0 { O 1 bit } imag_i_7_address1 { O 7 vector } imag_i_7_ce1 { O 1 bit } imag_i_7_d1 { O 32 vector } imag_i_7_q1 { I 32 vector } imag_i_7_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_7'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 250 \
    name real_o_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o_0 \
    op interface \
    ports { real_o_0_address0 { O 7 vector } real_o_0_ce0 { O 1 bit } real_o_0_d0 { O 32 vector } real_o_0_q0 { I 32 vector } real_o_0_we0 { O 1 bit } real_o_0_address1 { O 7 vector } real_o_0_ce1 { O 1 bit } real_o_0_d1 { O 32 vector } real_o_0_q1 { I 32 vector } real_o_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 251 \
    name real_o_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o_1 \
    op interface \
    ports { real_o_1_address0 { O 7 vector } real_o_1_ce0 { O 1 bit } real_o_1_d0 { O 32 vector } real_o_1_q0 { I 32 vector } real_o_1_we0 { O 1 bit } real_o_1_address1 { O 7 vector } real_o_1_ce1 { O 1 bit } real_o_1_d1 { O 32 vector } real_o_1_q1 { I 32 vector } real_o_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 252 \
    name real_o_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o_2 \
    op interface \
    ports { real_o_2_address0 { O 7 vector } real_o_2_ce0 { O 1 bit } real_o_2_d0 { O 32 vector } real_o_2_q0 { I 32 vector } real_o_2_we0 { O 1 bit } real_o_2_address1 { O 7 vector } real_o_2_ce1 { O 1 bit } real_o_2_d1 { O 32 vector } real_o_2_q1 { I 32 vector } real_o_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 253 \
    name real_o_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o_3 \
    op interface \
    ports { real_o_3_address0 { O 7 vector } real_o_3_ce0 { O 1 bit } real_o_3_d0 { O 32 vector } real_o_3_q0 { I 32 vector } real_o_3_we0 { O 1 bit } real_o_3_address1 { O 7 vector } real_o_3_ce1 { O 1 bit } real_o_3_d1 { O 32 vector } real_o_3_q1 { I 32 vector } real_o_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 254 \
    name real_o_4 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o_4 \
    op interface \
    ports { real_o_4_address0 { O 7 vector } real_o_4_ce0 { O 1 bit } real_o_4_d0 { O 32 vector } real_o_4_q0 { I 32 vector } real_o_4_we0 { O 1 bit } real_o_4_address1 { O 7 vector } real_o_4_ce1 { O 1 bit } real_o_4_d1 { O 32 vector } real_o_4_q1 { I 32 vector } real_o_4_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o_4'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 255 \
    name real_o_5 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o_5 \
    op interface \
    ports { real_o_5_address0 { O 7 vector } real_o_5_ce0 { O 1 bit } real_o_5_d0 { O 32 vector } real_o_5_q0 { I 32 vector } real_o_5_we0 { O 1 bit } real_o_5_address1 { O 7 vector } real_o_5_ce1 { O 1 bit } real_o_5_d1 { O 32 vector } real_o_5_q1 { I 32 vector } real_o_5_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o_5'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 256 \
    name real_o_6 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o_6 \
    op interface \
    ports { real_o_6_address0 { O 7 vector } real_o_6_ce0 { O 1 bit } real_o_6_d0 { O 32 vector } real_o_6_q0 { I 32 vector } real_o_6_we0 { O 1 bit } real_o_6_address1 { O 7 vector } real_o_6_ce1 { O 1 bit } real_o_6_d1 { O 32 vector } real_o_6_q1 { I 32 vector } real_o_6_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o_6'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 257 \
    name real_o_7 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o_7 \
    op interface \
    ports { real_o_7_address0 { O 7 vector } real_o_7_ce0 { O 1 bit } real_o_7_d0 { O 32 vector } real_o_7_q0 { I 32 vector } real_o_7_we0 { O 1 bit } real_o_7_address1 { O 7 vector } real_o_7_ce1 { O 1 bit } real_o_7_d1 { O 32 vector } real_o_7_q1 { I 32 vector } real_o_7_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o_7'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 258 \
    name imag_o_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o_0 \
    op interface \
    ports { imag_o_0_address0 { O 7 vector } imag_o_0_ce0 { O 1 bit } imag_o_0_d0 { O 32 vector } imag_o_0_q0 { I 32 vector } imag_o_0_we0 { O 1 bit } imag_o_0_address1 { O 7 vector } imag_o_0_ce1 { O 1 bit } imag_o_0_d1 { O 32 vector } imag_o_0_q1 { I 32 vector } imag_o_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 259 \
    name imag_o_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o_1 \
    op interface \
    ports { imag_o_1_address0 { O 7 vector } imag_o_1_ce0 { O 1 bit } imag_o_1_d0 { O 32 vector } imag_o_1_q0 { I 32 vector } imag_o_1_we0 { O 1 bit } imag_o_1_address1 { O 7 vector } imag_o_1_ce1 { O 1 bit } imag_o_1_d1 { O 32 vector } imag_o_1_q1 { I 32 vector } imag_o_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 260 \
    name imag_o_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o_2 \
    op interface \
    ports { imag_o_2_address0 { O 7 vector } imag_o_2_ce0 { O 1 bit } imag_o_2_d0 { O 32 vector } imag_o_2_q0 { I 32 vector } imag_o_2_we0 { O 1 bit } imag_o_2_address1 { O 7 vector } imag_o_2_ce1 { O 1 bit } imag_o_2_d1 { O 32 vector } imag_o_2_q1 { I 32 vector } imag_o_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 261 \
    name imag_o_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o_3 \
    op interface \
    ports { imag_o_3_address0 { O 7 vector } imag_o_3_ce0 { O 1 bit } imag_o_3_d0 { O 32 vector } imag_o_3_q0 { I 32 vector } imag_o_3_we0 { O 1 bit } imag_o_3_address1 { O 7 vector } imag_o_3_ce1 { O 1 bit } imag_o_3_d1 { O 32 vector } imag_o_3_q1 { I 32 vector } imag_o_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 262 \
    name imag_o_4 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o_4 \
    op interface \
    ports { imag_o_4_address0 { O 7 vector } imag_o_4_ce0 { O 1 bit } imag_o_4_d0 { O 32 vector } imag_o_4_q0 { I 32 vector } imag_o_4_we0 { O 1 bit } imag_o_4_address1 { O 7 vector } imag_o_4_ce1 { O 1 bit } imag_o_4_d1 { O 32 vector } imag_o_4_q1 { I 32 vector } imag_o_4_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o_4'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 263 \
    name imag_o_5 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o_5 \
    op interface \
    ports { imag_o_5_address0 { O 7 vector } imag_o_5_ce0 { O 1 bit } imag_o_5_d0 { O 32 vector } imag_o_5_q0 { I 32 vector } imag_o_5_we0 { O 1 bit } imag_o_5_address1 { O 7 vector } imag_o_5_ce1 { O 1 bit } imag_o_5_d1 { O 32 vector } imag_o_5_q1 { I 32 vector } imag_o_5_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o_5'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 264 \
    name imag_o_6 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o_6 \
    op interface \
    ports { imag_o_6_address0 { O 7 vector } imag_o_6_ce0 { O 1 bit } imag_o_6_d0 { O 32 vector } imag_o_6_q0 { I 32 vector } imag_o_6_we0 { O 1 bit } imag_o_6_address1 { O 7 vector } imag_o_6_ce1 { O 1 bit } imag_o_6_d1 { O 32 vector } imag_o_6_q1 { I 32 vector } imag_o_6_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o_6'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 265 \
    name imag_o_7 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o_7 \
    op interface \
    ports { imag_o_7_address0 { O 7 vector } imag_o_7_ce0 { O 1 bit } imag_o_7_d0 { O 32 vector } imag_o_7_q0 { I 32 vector } imag_o_7_we0 { O 1 bit } imag_o_7_address1 { O 7 vector } imag_o_7_ce1 { O 1 bit } imag_o_7_d1 { O 32 vector } imag_o_7_q1 { I 32 vector } imag_o_7_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o_7'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


