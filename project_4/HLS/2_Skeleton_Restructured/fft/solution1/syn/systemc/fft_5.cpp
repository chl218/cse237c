#include "fft.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void fft::thread_imag_i_6_we1() {
    imag_i_6_we1 = ap_const_logic_0;
}

void fft::thread_imag_i_7_address0() {
    imag_i_7_address0 = fft_bit_reverse_U0_imag_i_7_address0.read();
}

void fft::thread_imag_i_7_address1() {
    imag_i_7_address1 = fft_bit_reverse_U0_imag_i_7_address1.read();
}

void fft::thread_imag_i_7_ce0() {
    imag_i_7_ce0 = fft_bit_reverse_U0_imag_i_7_ce0.read();
}

void fft::thread_imag_i_7_ce1() {
    imag_i_7_ce1 = fft_bit_reverse_U0_imag_i_7_ce1.read();
}

void fft::thread_imag_i_7_d0() {
    imag_i_7_d0 = ap_const_lv32_0;
}

void fft::thread_imag_i_7_d1() {
    imag_i_7_d1 = ap_const_lv32_0;
}

void fft::thread_imag_i_7_we0() {
    imag_i_7_we0 = ap_const_logic_0;
}

void fft::thread_imag_i_7_we1() {
    imag_i_7_we1 = ap_const_logic_0;
}

void fft::thread_imag_o_0_address0() {
    imag_o_0_address0 = fft_fft_stage_last_U0_imag_o_0_address0.read();
}

void fft::thread_imag_o_0_address1() {
    imag_o_0_address1 = fft_fft_stage_last_U0_imag_o_0_address1.read();
}

void fft::thread_imag_o_0_ce0() {
    imag_o_0_ce0 = fft_fft_stage_last_U0_imag_o_0_ce0.read();
}

void fft::thread_imag_o_0_ce1() {
    imag_o_0_ce1 = fft_fft_stage_last_U0_imag_o_0_ce1.read();
}

void fft::thread_imag_o_0_d0() {
    imag_o_0_d0 = fft_fft_stage_last_U0_imag_o_0_d0.read();
}

void fft::thread_imag_o_0_d1() {
    imag_o_0_d1 = fft_fft_stage_last_U0_imag_o_0_d1.read();
}

void fft::thread_imag_o_0_we0() {
    imag_o_0_we0 = fft_fft_stage_last_U0_imag_o_0_we0.read();
}

void fft::thread_imag_o_0_we1() {
    imag_o_0_we1 = fft_fft_stage_last_U0_imag_o_0_we1.read();
}

void fft::thread_imag_o_1_address0() {
    imag_o_1_address0 = fft_fft_stage_last_U0_imag_o_1_address0.read();
}

void fft::thread_imag_o_1_address1() {
    imag_o_1_address1 = fft_fft_stage_last_U0_imag_o_1_address1.read();
}

void fft::thread_imag_o_1_ce0() {
    imag_o_1_ce0 = fft_fft_stage_last_U0_imag_o_1_ce0.read();
}

void fft::thread_imag_o_1_ce1() {
    imag_o_1_ce1 = fft_fft_stage_last_U0_imag_o_1_ce1.read();
}

void fft::thread_imag_o_1_d0() {
    imag_o_1_d0 = fft_fft_stage_last_U0_imag_o_1_d0.read();
}

void fft::thread_imag_o_1_d1() {
    imag_o_1_d1 = fft_fft_stage_last_U0_imag_o_1_d1.read();
}

void fft::thread_imag_o_1_we0() {
    imag_o_1_we0 = fft_fft_stage_last_U0_imag_o_1_we0.read();
}

void fft::thread_imag_o_1_we1() {
    imag_o_1_we1 = fft_fft_stage_last_U0_imag_o_1_we1.read();
}

void fft::thread_imag_o_2_address0() {
    imag_o_2_address0 = fft_fft_stage_last_U0_imag_o_2_address0.read();
}

void fft::thread_imag_o_2_address1() {
    imag_o_2_address1 = fft_fft_stage_last_U0_imag_o_2_address1.read();
}

void fft::thread_imag_o_2_ce0() {
    imag_o_2_ce0 = fft_fft_stage_last_U0_imag_o_2_ce0.read();
}

void fft::thread_imag_o_2_ce1() {
    imag_o_2_ce1 = fft_fft_stage_last_U0_imag_o_2_ce1.read();
}

void fft::thread_imag_o_2_d0() {
    imag_o_2_d0 = fft_fft_stage_last_U0_imag_o_2_d0.read();
}

void fft::thread_imag_o_2_d1() {
    imag_o_2_d1 = fft_fft_stage_last_U0_imag_o_2_d1.read();
}

void fft::thread_imag_o_2_we0() {
    imag_o_2_we0 = fft_fft_stage_last_U0_imag_o_2_we0.read();
}

void fft::thread_imag_o_2_we1() {
    imag_o_2_we1 = fft_fft_stage_last_U0_imag_o_2_we1.read();
}

void fft::thread_imag_o_3_address0() {
    imag_o_3_address0 = fft_fft_stage_last_U0_imag_o_3_address0.read();
}

void fft::thread_imag_o_3_address1() {
    imag_o_3_address1 = fft_fft_stage_last_U0_imag_o_3_address1.read();
}

void fft::thread_imag_o_3_ce0() {
    imag_o_3_ce0 = fft_fft_stage_last_U0_imag_o_3_ce0.read();
}

void fft::thread_imag_o_3_ce1() {
    imag_o_3_ce1 = fft_fft_stage_last_U0_imag_o_3_ce1.read();
}

void fft::thread_imag_o_3_d0() {
    imag_o_3_d0 = fft_fft_stage_last_U0_imag_o_3_d0.read();
}

void fft::thread_imag_o_3_d1() {
    imag_o_3_d1 = fft_fft_stage_last_U0_imag_o_3_d1.read();
}

void fft::thread_imag_o_3_we0() {
    imag_o_3_we0 = fft_fft_stage_last_U0_imag_o_3_we0.read();
}

void fft::thread_imag_o_3_we1() {
    imag_o_3_we1 = fft_fft_stage_last_U0_imag_o_3_we1.read();
}

void fft::thread_imag_o_4_address0() {
    imag_o_4_address0 = fft_fft_stage_last_U0_imag_o_4_address0.read();
}

void fft::thread_imag_o_4_address1() {
    imag_o_4_address1 = fft_fft_stage_last_U0_imag_o_4_address1.read();
}

void fft::thread_imag_o_4_ce0() {
    imag_o_4_ce0 = fft_fft_stage_last_U0_imag_o_4_ce0.read();
}

void fft::thread_imag_o_4_ce1() {
    imag_o_4_ce1 = fft_fft_stage_last_U0_imag_o_4_ce1.read();
}

void fft::thread_imag_o_4_d0() {
    imag_o_4_d0 = fft_fft_stage_last_U0_imag_o_4_d0.read();
}

void fft::thread_imag_o_4_d1() {
    imag_o_4_d1 = fft_fft_stage_last_U0_imag_o_4_d1.read();
}

void fft::thread_imag_o_4_we0() {
    imag_o_4_we0 = fft_fft_stage_last_U0_imag_o_4_we0.read();
}

void fft::thread_imag_o_4_we1() {
    imag_o_4_we1 = fft_fft_stage_last_U0_imag_o_4_we1.read();
}

void fft::thread_imag_o_5_address0() {
    imag_o_5_address0 = fft_fft_stage_last_U0_imag_o_5_address0.read();
}

void fft::thread_imag_o_5_address1() {
    imag_o_5_address1 = fft_fft_stage_last_U0_imag_o_5_address1.read();
}

void fft::thread_imag_o_5_ce0() {
    imag_o_5_ce0 = fft_fft_stage_last_U0_imag_o_5_ce0.read();
}

void fft::thread_imag_o_5_ce1() {
    imag_o_5_ce1 = fft_fft_stage_last_U0_imag_o_5_ce1.read();
}

void fft::thread_imag_o_5_d0() {
    imag_o_5_d0 = fft_fft_stage_last_U0_imag_o_5_d0.read();
}

void fft::thread_imag_o_5_d1() {
    imag_o_5_d1 = fft_fft_stage_last_U0_imag_o_5_d1.read();
}

void fft::thread_imag_o_5_we0() {
    imag_o_5_we0 = fft_fft_stage_last_U0_imag_o_5_we0.read();
}

void fft::thread_imag_o_5_we1() {
    imag_o_5_we1 = fft_fft_stage_last_U0_imag_o_5_we1.read();
}

void fft::thread_imag_o_6_address0() {
    imag_o_6_address0 = fft_fft_stage_last_U0_imag_o_6_address0.read();
}

void fft::thread_imag_o_6_address1() {
    imag_o_6_address1 = fft_fft_stage_last_U0_imag_o_6_address1.read();
}

void fft::thread_imag_o_6_ce0() {
    imag_o_6_ce0 = fft_fft_stage_last_U0_imag_o_6_ce0.read();
}

void fft::thread_imag_o_6_ce1() {
    imag_o_6_ce1 = fft_fft_stage_last_U0_imag_o_6_ce1.read();
}

void fft::thread_imag_o_6_d0() {
    imag_o_6_d0 = fft_fft_stage_last_U0_imag_o_6_d0.read();
}

void fft::thread_imag_o_6_d1() {
    imag_o_6_d1 = fft_fft_stage_last_U0_imag_o_6_d1.read();
}

void fft::thread_imag_o_6_we0() {
    imag_o_6_we0 = fft_fft_stage_last_U0_imag_o_6_we0.read();
}

void fft::thread_imag_o_6_we1() {
    imag_o_6_we1 = fft_fft_stage_last_U0_imag_o_6_we1.read();
}

void fft::thread_imag_o_7_address0() {
    imag_o_7_address0 = fft_fft_stage_last_U0_imag_o_7_address0.read();
}

void fft::thread_imag_o_7_address1() {
    imag_o_7_address1 = fft_fft_stage_last_U0_imag_o_7_address1.read();
}

void fft::thread_imag_o_7_ce0() {
    imag_o_7_ce0 = fft_fft_stage_last_U0_imag_o_7_ce0.read();
}

void fft::thread_imag_o_7_ce1() {
    imag_o_7_ce1 = fft_fft_stage_last_U0_imag_o_7_ce1.read();
}

void fft::thread_imag_o_7_d0() {
    imag_o_7_d0 = fft_fft_stage_last_U0_imag_o_7_d0.read();
}

void fft::thread_imag_o_7_d1() {
    imag_o_7_d1 = fft_fft_stage_last_U0_imag_o_7_d1.read();
}

void fft::thread_imag_o_7_we0() {
    imag_o_7_we0 = fft_fft_stage_last_U0_imag_o_7_we0.read();
}

void fft::thread_imag_o_7_we1() {
    imag_o_7_we1 = fft_fft_stage_last_U0_imag_o_7_we1.read();
}

void fft::thread_real_i_0_address0() {
    real_i_0_address0 = fft_bit_reverse_U0_real_i_0_address0.read();
}

void fft::thread_real_i_0_address1() {
    real_i_0_address1 = fft_bit_reverse_U0_real_i_0_address1.read();
}

void fft::thread_real_i_0_ce0() {
    real_i_0_ce0 = fft_bit_reverse_U0_real_i_0_ce0.read();
}

void fft::thread_real_i_0_ce1() {
    real_i_0_ce1 = fft_bit_reverse_U0_real_i_0_ce1.read();
}

void fft::thread_real_i_0_d0() {
    real_i_0_d0 = ap_const_lv32_0;
}

void fft::thread_real_i_0_d1() {
    real_i_0_d1 = ap_const_lv32_0;
}

void fft::thread_real_i_0_we0() {
    real_i_0_we0 = ap_const_logic_0;
}

void fft::thread_real_i_0_we1() {
    real_i_0_we1 = ap_const_logic_0;
}

void fft::thread_real_i_1_address0() {
    real_i_1_address0 = fft_bit_reverse_U0_real_i_1_address0.read();
}

void fft::thread_real_i_1_address1() {
    real_i_1_address1 = fft_bit_reverse_U0_real_i_1_address1.read();
}

void fft::thread_real_i_1_ce0() {
    real_i_1_ce0 = fft_bit_reverse_U0_real_i_1_ce0.read();
}

void fft::thread_real_i_1_ce1() {
    real_i_1_ce1 = fft_bit_reverse_U0_real_i_1_ce1.read();
}

void fft::thread_real_i_1_d0() {
    real_i_1_d0 = ap_const_lv32_0;
}

void fft::thread_real_i_1_d1() {
    real_i_1_d1 = ap_const_lv32_0;
}

void fft::thread_real_i_1_we0() {
    real_i_1_we0 = ap_const_logic_0;
}

void fft::thread_real_i_1_we1() {
    real_i_1_we1 = ap_const_logic_0;
}

void fft::thread_real_i_2_address0() {
    real_i_2_address0 = fft_bit_reverse_U0_real_i_2_address0.read();
}

void fft::thread_real_i_2_address1() {
    real_i_2_address1 = fft_bit_reverse_U0_real_i_2_address1.read();
}

void fft::thread_real_i_2_ce0() {
    real_i_2_ce0 = fft_bit_reverse_U0_real_i_2_ce0.read();
}

void fft::thread_real_i_2_ce1() {
    real_i_2_ce1 = fft_bit_reverse_U0_real_i_2_ce1.read();
}

void fft::thread_real_i_2_d0() {
    real_i_2_d0 = ap_const_lv32_0;
}

void fft::thread_real_i_2_d1() {
    real_i_2_d1 = ap_const_lv32_0;
}

void fft::thread_real_i_2_we0() {
    real_i_2_we0 = ap_const_logic_0;
}

void fft::thread_real_i_2_we1() {
    real_i_2_we1 = ap_const_logic_0;
}

void fft::thread_real_i_3_address0() {
    real_i_3_address0 = fft_bit_reverse_U0_real_i_3_address0.read();
}

void fft::thread_real_i_3_address1() {
    real_i_3_address1 = fft_bit_reverse_U0_real_i_3_address1.read();
}

void fft::thread_real_i_3_ce0() {
    real_i_3_ce0 = fft_bit_reverse_U0_real_i_3_ce0.read();
}

void fft::thread_real_i_3_ce1() {
    real_i_3_ce1 = fft_bit_reverse_U0_real_i_3_ce1.read();
}

void fft::thread_real_i_3_d0() {
    real_i_3_d0 = ap_const_lv32_0;
}

void fft::thread_real_i_3_d1() {
    real_i_3_d1 = ap_const_lv32_0;
}

void fft::thread_real_i_3_we0() {
    real_i_3_we0 = ap_const_logic_0;
}

void fft::thread_real_i_3_we1() {
    real_i_3_we1 = ap_const_logic_0;
}

void fft::thread_real_i_4_address0() {
    real_i_4_address0 = fft_bit_reverse_U0_real_i_4_address0.read();
}

void fft::thread_real_i_4_address1() {
    real_i_4_address1 = fft_bit_reverse_U0_real_i_4_address1.read();
}

void fft::thread_real_i_4_ce0() {
    real_i_4_ce0 = fft_bit_reverse_U0_real_i_4_ce0.read();
}

void fft::thread_real_i_4_ce1() {
    real_i_4_ce1 = fft_bit_reverse_U0_real_i_4_ce1.read();
}

void fft::thread_real_i_4_d0() {
    real_i_4_d0 = ap_const_lv32_0;
}

void fft::thread_real_i_4_d1() {
    real_i_4_d1 = ap_const_lv32_0;
}

void fft::thread_real_i_4_we0() {
    real_i_4_we0 = ap_const_logic_0;
}

void fft::thread_real_i_4_we1() {
    real_i_4_we1 = ap_const_logic_0;
}

void fft::thread_real_i_5_address0() {
    real_i_5_address0 = fft_bit_reverse_U0_real_i_5_address0.read();
}

void fft::thread_real_i_5_address1() {
    real_i_5_address1 = fft_bit_reverse_U0_real_i_5_address1.read();
}

void fft::thread_real_i_5_ce0() {
    real_i_5_ce0 = fft_bit_reverse_U0_real_i_5_ce0.read();
}

void fft::thread_real_i_5_ce1() {
    real_i_5_ce1 = fft_bit_reverse_U0_real_i_5_ce1.read();
}

void fft::thread_real_i_5_d0() {
    real_i_5_d0 = ap_const_lv32_0;
}

void fft::thread_real_i_5_d1() {
    real_i_5_d1 = ap_const_lv32_0;
}

void fft::thread_real_i_5_we0() {
    real_i_5_we0 = ap_const_logic_0;
}

void fft::thread_real_i_5_we1() {
    real_i_5_we1 = ap_const_logic_0;
}

void fft::thread_real_i_6_address0() {
    real_i_6_address0 = fft_bit_reverse_U0_real_i_6_address0.read();
}

void fft::thread_real_i_6_address1() {
    real_i_6_address1 = fft_bit_reverse_U0_real_i_6_address1.read();
}

void fft::thread_real_i_6_ce0() {
    real_i_6_ce0 = fft_bit_reverse_U0_real_i_6_ce0.read();
}

void fft::thread_real_i_6_ce1() {
    real_i_6_ce1 = fft_bit_reverse_U0_real_i_6_ce1.read();
}

void fft::thread_real_i_6_d0() {
    real_i_6_d0 = ap_const_lv32_0;
}

void fft::thread_real_i_6_d1() {
    real_i_6_d1 = ap_const_lv32_0;
}

void fft::thread_real_i_6_we0() {
    real_i_6_we0 = ap_const_logic_0;
}

void fft::thread_real_i_6_we1() {
    real_i_6_we1 = ap_const_logic_0;
}

void fft::thread_real_i_7_address0() {
    real_i_7_address0 = fft_bit_reverse_U0_real_i_7_address0.read();
}

void fft::thread_real_i_7_address1() {
    real_i_7_address1 = fft_bit_reverse_U0_real_i_7_address1.read();
}

void fft::thread_real_i_7_ce0() {
    real_i_7_ce0 = fft_bit_reverse_U0_real_i_7_ce0.read();
}

void fft::thread_real_i_7_ce1() {
    real_i_7_ce1 = fft_bit_reverse_U0_real_i_7_ce1.read();
}

void fft::thread_real_i_7_d0() {
    real_i_7_d0 = ap_const_lv32_0;
}

void fft::thread_real_i_7_d1() {
    real_i_7_d1 = ap_const_lv32_0;
}

void fft::thread_real_i_7_we0() {
    real_i_7_we0 = ap_const_logic_0;
}

void fft::thread_real_i_7_we1() {
    real_i_7_we1 = ap_const_logic_0;
}

void fft::thread_real_o_0_address0() {
    real_o_0_address0 = fft_fft_stage_last_U0_real_o_0_address0.read();
}

void fft::thread_real_o_0_address1() {
    real_o_0_address1 = fft_fft_stage_last_U0_real_o_0_address1.read();
}

void fft::thread_real_o_0_ce0() {
    real_o_0_ce0 = fft_fft_stage_last_U0_real_o_0_ce0.read();
}

void fft::thread_real_o_0_ce1() {
    real_o_0_ce1 = fft_fft_stage_last_U0_real_o_0_ce1.read();
}

void fft::thread_real_o_0_d0() {
    real_o_0_d0 = fft_fft_stage_last_U0_real_o_0_d0.read();
}

void fft::thread_real_o_0_d1() {
    real_o_0_d1 = fft_fft_stage_last_U0_real_o_0_d1.read();
}

void fft::thread_real_o_0_we0() {
    real_o_0_we0 = fft_fft_stage_last_U0_real_o_0_we0.read();
}

void fft::thread_real_o_0_we1() {
    real_o_0_we1 = fft_fft_stage_last_U0_real_o_0_we1.read();
}

void fft::thread_real_o_1_address0() {
    real_o_1_address0 = fft_fft_stage_last_U0_real_o_1_address0.read();
}

void fft::thread_real_o_1_address1() {
    real_o_1_address1 = fft_fft_stage_last_U0_real_o_1_address1.read();
}

void fft::thread_real_o_1_ce0() {
    real_o_1_ce0 = fft_fft_stage_last_U0_real_o_1_ce0.read();
}

void fft::thread_real_o_1_ce1() {
    real_o_1_ce1 = fft_fft_stage_last_U0_real_o_1_ce1.read();
}

void fft::thread_real_o_1_d0() {
    real_o_1_d0 = fft_fft_stage_last_U0_real_o_1_d0.read();
}

void fft::thread_real_o_1_d1() {
    real_o_1_d1 = fft_fft_stage_last_U0_real_o_1_d1.read();
}

void fft::thread_real_o_1_we0() {
    real_o_1_we0 = fft_fft_stage_last_U0_real_o_1_we0.read();
}

void fft::thread_real_o_1_we1() {
    real_o_1_we1 = fft_fft_stage_last_U0_real_o_1_we1.read();
}

void fft::thread_real_o_2_address0() {
    real_o_2_address0 = fft_fft_stage_last_U0_real_o_2_address0.read();
}

void fft::thread_real_o_2_address1() {
    real_o_2_address1 = fft_fft_stage_last_U0_real_o_2_address1.read();
}

void fft::thread_real_o_2_ce0() {
    real_o_2_ce0 = fft_fft_stage_last_U0_real_o_2_ce0.read();
}

void fft::thread_real_o_2_ce1() {
    real_o_2_ce1 = fft_fft_stage_last_U0_real_o_2_ce1.read();
}

void fft::thread_real_o_2_d0() {
    real_o_2_d0 = fft_fft_stage_last_U0_real_o_2_d0.read();
}

void fft::thread_real_o_2_d1() {
    real_o_2_d1 = fft_fft_stage_last_U0_real_o_2_d1.read();
}

void fft::thread_real_o_2_we0() {
    real_o_2_we0 = fft_fft_stage_last_U0_real_o_2_we0.read();
}

void fft::thread_real_o_2_we1() {
    real_o_2_we1 = fft_fft_stage_last_U0_real_o_2_we1.read();
}

void fft::thread_real_o_3_address0() {
    real_o_3_address0 = fft_fft_stage_last_U0_real_o_3_address0.read();
}

void fft::thread_real_o_3_address1() {
    real_o_3_address1 = fft_fft_stage_last_U0_real_o_3_address1.read();
}

void fft::thread_real_o_3_ce0() {
    real_o_3_ce0 = fft_fft_stage_last_U0_real_o_3_ce0.read();
}

void fft::thread_real_o_3_ce1() {
    real_o_3_ce1 = fft_fft_stage_last_U0_real_o_3_ce1.read();
}

void fft::thread_real_o_3_d0() {
    real_o_3_d0 = fft_fft_stage_last_U0_real_o_3_d0.read();
}

void fft::thread_real_o_3_d1() {
    real_o_3_d1 = fft_fft_stage_last_U0_real_o_3_d1.read();
}

void fft::thread_real_o_3_we0() {
    real_o_3_we0 = fft_fft_stage_last_U0_real_o_3_we0.read();
}

void fft::thread_real_o_3_we1() {
    real_o_3_we1 = fft_fft_stage_last_U0_real_o_3_we1.read();
}

void fft::thread_real_o_4_address0() {
    real_o_4_address0 = fft_fft_stage_last_U0_real_o_4_address0.read();
}

void fft::thread_real_o_4_address1() {
    real_o_4_address1 = fft_fft_stage_last_U0_real_o_4_address1.read();
}

void fft::thread_real_o_4_ce0() {
    real_o_4_ce0 = fft_fft_stage_last_U0_real_o_4_ce0.read();
}

void fft::thread_real_o_4_ce1() {
    real_o_4_ce1 = fft_fft_stage_last_U0_real_o_4_ce1.read();
}

void fft::thread_real_o_4_d0() {
    real_o_4_d0 = fft_fft_stage_last_U0_real_o_4_d0.read();
}

void fft::thread_real_o_4_d1() {
    real_o_4_d1 = fft_fft_stage_last_U0_real_o_4_d1.read();
}

void fft::thread_real_o_4_we0() {
    real_o_4_we0 = fft_fft_stage_last_U0_real_o_4_we0.read();
}

void fft::thread_real_o_4_we1() {
    real_o_4_we1 = fft_fft_stage_last_U0_real_o_4_we1.read();
}

void fft::thread_real_o_5_address0() {
    real_o_5_address0 = fft_fft_stage_last_U0_real_o_5_address0.read();
}

void fft::thread_real_o_5_address1() {
    real_o_5_address1 = fft_fft_stage_last_U0_real_o_5_address1.read();
}

void fft::thread_real_o_5_ce0() {
    real_o_5_ce0 = fft_fft_stage_last_U0_real_o_5_ce0.read();
}

void fft::thread_real_o_5_ce1() {
    real_o_5_ce1 = fft_fft_stage_last_U0_real_o_5_ce1.read();
}

void fft::thread_real_o_5_d0() {
    real_o_5_d0 = fft_fft_stage_last_U0_real_o_5_d0.read();
}

void fft::thread_real_o_5_d1() {
    real_o_5_d1 = fft_fft_stage_last_U0_real_o_5_d1.read();
}

void fft::thread_real_o_5_we0() {
    real_o_5_we0 = fft_fft_stage_last_U0_real_o_5_we0.read();
}

void fft::thread_real_o_5_we1() {
    real_o_5_we1 = fft_fft_stage_last_U0_real_o_5_we1.read();
}

void fft::thread_real_o_6_address0() {
    real_o_6_address0 = fft_fft_stage_last_U0_real_o_6_address0.read();
}

void fft::thread_real_o_6_address1() {
    real_o_6_address1 = fft_fft_stage_last_U0_real_o_6_address1.read();
}

void fft::thread_real_o_6_ce0() {
    real_o_6_ce0 = fft_fft_stage_last_U0_real_o_6_ce0.read();
}

void fft::thread_real_o_6_ce1() {
    real_o_6_ce1 = fft_fft_stage_last_U0_real_o_6_ce1.read();
}

void fft::thread_real_o_6_d0() {
    real_o_6_d0 = fft_fft_stage_last_U0_real_o_6_d0.read();
}

void fft::thread_real_o_6_d1() {
    real_o_6_d1 = fft_fft_stage_last_U0_real_o_6_d1.read();
}

void fft::thread_real_o_6_we0() {
    real_o_6_we0 = fft_fft_stage_last_U0_real_o_6_we0.read();
}

void fft::thread_real_o_6_we1() {
    real_o_6_we1 = fft_fft_stage_last_U0_real_o_6_we1.read();
}

void fft::thread_real_o_7_address0() {
    real_o_7_address0 = fft_fft_stage_last_U0_real_o_7_address0.read();
}

void fft::thread_real_o_7_address1() {
    real_o_7_address1 = fft_fft_stage_last_U0_real_o_7_address1.read();
}

void fft::thread_real_o_7_ce0() {
    real_o_7_ce0 = fft_fft_stage_last_U0_real_o_7_ce0.read();
}

void fft::thread_real_o_7_ce1() {
    real_o_7_ce1 = fft_fft_stage_last_U0_real_o_7_ce1.read();
}

void fft::thread_real_o_7_d0() {
    real_o_7_d0 = fft_fft_stage_last_U0_real_o_7_d0.read();
}

void fft::thread_real_o_7_d1() {
    real_o_7_d1 = fft_fft_stage_last_U0_real_o_7_d1.read();
}

void fft::thread_real_o_7_we0() {
    real_o_7_we0 = fft_fft_stage_last_U0_real_o_7_we0.read();
}

void fft::thread_real_o_7_we1() {
    real_o_7_we1 = fft_fft_stage_last_U0_real_o_7_we1.read();
}

}

