#include "fft.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void fft::thread_Stage0_I_U_ap_dummy_ce() {
    Stage0_I_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage0_I_i_address0() {
    Stage0_I_i_address0 = fft_bit_reverse_U0_imag_o_address0.read();
}

void fft::thread_Stage0_I_i_address1() {
    Stage0_I_i_address1 = fft_bit_reverse_U0_imag_o_address1.read();
}

void fft::thread_Stage0_I_i_ce0() {
    Stage0_I_i_ce0 = fft_bit_reverse_U0_imag_o_ce0.read();
}

void fft::thread_Stage0_I_i_ce1() {
    Stage0_I_i_ce1 = fft_bit_reverse_U0_imag_o_ce1.read();
}

void fft::thread_Stage0_I_i_d0() {
    Stage0_I_i_d0 = fft_bit_reverse_U0_imag_o_d0.read();
}

void fft::thread_Stage0_I_i_d1() {
    Stage0_I_i_d1 = fft_bit_reverse_U0_imag_o_d1.read();
}

void fft::thread_Stage0_I_i_we0() {
    Stage0_I_i_we0 = fft_bit_reverse_U0_imag_o_we0.read();
}

void fft::thread_Stage0_I_i_we1() {
    Stage0_I_i_we1 = fft_bit_reverse_U0_imag_o_we1.read();
}

void fft::thread_Stage0_I_i_write() {
    Stage0_I_i_write = ap_chn_write_fft_bit_reverse_U0_Stage0_I.read();
}

void fft::thread_Stage0_I_t_address0() {
    Stage0_I_t_address0 = fft_fft_stage_first_U0_imag_i_address0.read();
}

void fft::thread_Stage0_I_t_address1() {
    Stage0_I_t_address1 = fft_fft_stage_first_U0_imag_i_address1.read();
}

void fft::thread_Stage0_I_t_ce0() {
    Stage0_I_t_ce0 = fft_fft_stage_first_U0_imag_i_ce0.read();
}

void fft::thread_Stage0_I_t_ce1() {
    Stage0_I_t_ce1 = fft_fft_stage_first_U0_imag_i_ce1.read();
}

void fft::thread_Stage0_I_t_d0() {
    Stage0_I_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage0_I_t_d1() {
    Stage0_I_t_d1 = ap_const_lv32_0;
}

void fft::thread_Stage0_I_t_read() {
    Stage0_I_t_read = fft_fft_stage_first_U0_ap_ready.read();
}

void fft::thread_Stage0_I_t_we0() {
    Stage0_I_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage0_I_t_we1() {
    Stage0_I_t_we1 = ap_const_logic_0;
}

void fft::thread_Stage0_R_0_U_ap_dummy_ce() {
    Stage0_R_0_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage0_R_0_i_address0() {
    Stage0_R_0_i_address0 = fft_bit_reverse_U0_real_o_0_address0.read();
}

void fft::thread_Stage0_R_0_i_address1() {
    Stage0_R_0_i_address1 = fft_bit_reverse_U0_real_o_0_address1.read();
}

void fft::thread_Stage0_R_0_i_ce0() {
    Stage0_R_0_i_ce0 = fft_bit_reverse_U0_real_o_0_ce0.read();
}

void fft::thread_Stage0_R_0_i_ce1() {
    Stage0_R_0_i_ce1 = fft_bit_reverse_U0_real_o_0_ce1.read();
}

void fft::thread_Stage0_R_0_i_d0() {
    Stage0_R_0_i_d0 = fft_bit_reverse_U0_real_o_0_d0.read();
}

void fft::thread_Stage0_R_0_i_d1() {
    Stage0_R_0_i_d1 = fft_bit_reverse_U0_real_o_0_d1.read();
}

void fft::thread_Stage0_R_0_i_we0() {
    Stage0_R_0_i_we0 = fft_bit_reverse_U0_real_o_0_we0.read();
}

void fft::thread_Stage0_R_0_i_we1() {
    Stage0_R_0_i_we1 = fft_bit_reverse_U0_real_o_0_we1.read();
}

void fft::thread_Stage0_R_0_i_write() {
    Stage0_R_0_i_write = ap_chn_write_fft_bit_reverse_U0_Stage0_R_0.read();
}

void fft::thread_Stage0_R_0_t_address0() {
    Stage0_R_0_t_address0 = fft_fft_stage_first_U0_real_i_0_address0.read();
}

void fft::thread_Stage0_R_0_t_address1() {
    Stage0_R_0_t_address1 = fft_fft_stage_first_U0_real_i_0_address1.read();
}

void fft::thread_Stage0_R_0_t_ce0() {
    Stage0_R_0_t_ce0 = fft_fft_stage_first_U0_real_i_0_ce0.read();
}

void fft::thread_Stage0_R_0_t_ce1() {
    Stage0_R_0_t_ce1 = fft_fft_stage_first_U0_real_i_0_ce1.read();
}

void fft::thread_Stage0_R_0_t_d0() {
    Stage0_R_0_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage0_R_0_t_d1() {
    Stage0_R_0_t_d1 = ap_const_lv32_0;
}

void fft::thread_Stage0_R_0_t_read() {
    Stage0_R_0_t_read = fft_fft_stage_first_U0_ap_ready.read();
}

void fft::thread_Stage0_R_0_t_we0() {
    Stage0_R_0_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage0_R_0_t_we1() {
    Stage0_R_0_t_we1 = ap_const_logic_0;
}

void fft::thread_Stage0_R_1_U_ap_dummy_ce() {
    Stage0_R_1_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage0_R_1_i_address0() {
    Stage0_R_1_i_address0 = fft_bit_reverse_U0_real_o_1_address0.read();
}

void fft::thread_Stage0_R_1_i_address1() {
    Stage0_R_1_i_address1 = fft_bit_reverse_U0_real_o_1_address1.read();
}

void fft::thread_Stage0_R_1_i_ce0() {
    Stage0_R_1_i_ce0 = fft_bit_reverse_U0_real_o_1_ce0.read();
}

void fft::thread_Stage0_R_1_i_ce1() {
    Stage0_R_1_i_ce1 = fft_bit_reverse_U0_real_o_1_ce1.read();
}

void fft::thread_Stage0_R_1_i_d0() {
    Stage0_R_1_i_d0 = fft_bit_reverse_U0_real_o_1_d0.read();
}

void fft::thread_Stage0_R_1_i_d1() {
    Stage0_R_1_i_d1 = fft_bit_reverse_U0_real_o_1_d1.read();
}

void fft::thread_Stage0_R_1_i_we0() {
    Stage0_R_1_i_we0 = fft_bit_reverse_U0_real_o_1_we0.read();
}

void fft::thread_Stage0_R_1_i_we1() {
    Stage0_R_1_i_we1 = fft_bit_reverse_U0_real_o_1_we1.read();
}

void fft::thread_Stage0_R_1_i_write() {
    Stage0_R_1_i_write = ap_chn_write_fft_bit_reverse_U0_Stage0_R_1.read();
}

void fft::thread_Stage0_R_1_t_address0() {
    Stage0_R_1_t_address0 = fft_fft_stage_first_U0_real_i_1_address0.read();
}

void fft::thread_Stage0_R_1_t_address1() {
    Stage0_R_1_t_address1 = fft_fft_stage_first_U0_real_i_1_address1.read();
}

void fft::thread_Stage0_R_1_t_ce0() {
    Stage0_R_1_t_ce0 = fft_fft_stage_first_U0_real_i_1_ce0.read();
}

void fft::thread_Stage0_R_1_t_ce1() {
    Stage0_R_1_t_ce1 = fft_fft_stage_first_U0_real_i_1_ce1.read();
}

void fft::thread_Stage0_R_1_t_d0() {
    Stage0_R_1_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage0_R_1_t_d1() {
    Stage0_R_1_t_d1 = ap_const_lv32_0;
}

void fft::thread_Stage0_R_1_t_read() {
    Stage0_R_1_t_read = fft_fft_stage_first_U0_ap_ready.read();
}

void fft::thread_Stage0_R_1_t_we0() {
    Stage0_R_1_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage0_R_1_t_we1() {
    Stage0_R_1_t_we1 = ap_const_logic_0;
}

void fft::thread_Stage0_R_2_U_ap_dummy_ce() {
    Stage0_R_2_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage0_R_2_i_address0() {
    Stage0_R_2_i_address0 = fft_bit_reverse_U0_real_o_2_address0.read();
}

void fft::thread_Stage0_R_2_i_address1() {
    Stage0_R_2_i_address1 = fft_bit_reverse_U0_real_o_2_address1.read();
}

void fft::thread_Stage0_R_2_i_ce0() {
    Stage0_R_2_i_ce0 = fft_bit_reverse_U0_real_o_2_ce0.read();
}

void fft::thread_Stage0_R_2_i_ce1() {
    Stage0_R_2_i_ce1 = fft_bit_reverse_U0_real_o_2_ce1.read();
}

void fft::thread_Stage0_R_2_i_d0() {
    Stage0_R_2_i_d0 = fft_bit_reverse_U0_real_o_2_d0.read();
}

void fft::thread_Stage0_R_2_i_d1() {
    Stage0_R_2_i_d1 = fft_bit_reverse_U0_real_o_2_d1.read();
}

void fft::thread_Stage0_R_2_i_we0() {
    Stage0_R_2_i_we0 = fft_bit_reverse_U0_real_o_2_we0.read();
}

void fft::thread_Stage0_R_2_i_we1() {
    Stage0_R_2_i_we1 = fft_bit_reverse_U0_real_o_2_we1.read();
}

void fft::thread_Stage0_R_2_i_write() {
    Stage0_R_2_i_write = ap_chn_write_fft_bit_reverse_U0_Stage0_R_2.read();
}

void fft::thread_Stage0_R_2_t_address0() {
    Stage0_R_2_t_address0 = fft_fft_stage_first_U0_real_i_2_address0.read();
}

void fft::thread_Stage0_R_2_t_address1() {
    Stage0_R_2_t_address1 = fft_fft_stage_first_U0_real_i_2_address1.read();
}

void fft::thread_Stage0_R_2_t_ce0() {
    Stage0_R_2_t_ce0 = fft_fft_stage_first_U0_real_i_2_ce0.read();
}

void fft::thread_Stage0_R_2_t_ce1() {
    Stage0_R_2_t_ce1 = fft_fft_stage_first_U0_real_i_2_ce1.read();
}

void fft::thread_Stage0_R_2_t_d0() {
    Stage0_R_2_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage0_R_2_t_d1() {
    Stage0_R_2_t_d1 = ap_const_lv32_0;
}

void fft::thread_Stage0_R_2_t_read() {
    Stage0_R_2_t_read = fft_fft_stage_first_U0_ap_ready.read();
}

void fft::thread_Stage0_R_2_t_we0() {
    Stage0_R_2_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage0_R_2_t_we1() {
    Stage0_R_2_t_we1 = ap_const_logic_0;
}

void fft::thread_Stage0_R_3_U_ap_dummy_ce() {
    Stage0_R_3_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage0_R_3_i_address0() {
    Stage0_R_3_i_address0 = fft_bit_reverse_U0_real_o_3_address0.read();
}

void fft::thread_Stage0_R_3_i_address1() {
    Stage0_R_3_i_address1 = fft_bit_reverse_U0_real_o_3_address1.read();
}

void fft::thread_Stage0_R_3_i_ce0() {
    Stage0_R_3_i_ce0 = fft_bit_reverse_U0_real_o_3_ce0.read();
}

void fft::thread_Stage0_R_3_i_ce1() {
    Stage0_R_3_i_ce1 = fft_bit_reverse_U0_real_o_3_ce1.read();
}

void fft::thread_Stage0_R_3_i_d0() {
    Stage0_R_3_i_d0 = fft_bit_reverse_U0_real_o_3_d0.read();
}

void fft::thread_Stage0_R_3_i_d1() {
    Stage0_R_3_i_d1 = fft_bit_reverse_U0_real_o_3_d1.read();
}

void fft::thread_Stage0_R_3_i_we0() {
    Stage0_R_3_i_we0 = fft_bit_reverse_U0_real_o_3_we0.read();
}

void fft::thread_Stage0_R_3_i_we1() {
    Stage0_R_3_i_we1 = fft_bit_reverse_U0_real_o_3_we1.read();
}

void fft::thread_Stage0_R_3_i_write() {
    Stage0_R_3_i_write = ap_chn_write_fft_bit_reverse_U0_Stage0_R_3.read();
}

void fft::thread_Stage0_R_3_t_address0() {
    Stage0_R_3_t_address0 = fft_fft_stage_first_U0_real_i_3_address0.read();
}

void fft::thread_Stage0_R_3_t_address1() {
    Stage0_R_3_t_address1 = fft_fft_stage_first_U0_real_i_3_address1.read();
}

void fft::thread_Stage0_R_3_t_ce0() {
    Stage0_R_3_t_ce0 = fft_fft_stage_first_U0_real_i_3_ce0.read();
}

void fft::thread_Stage0_R_3_t_ce1() {
    Stage0_R_3_t_ce1 = fft_fft_stage_first_U0_real_i_3_ce1.read();
}

void fft::thread_Stage0_R_3_t_d0() {
    Stage0_R_3_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage0_R_3_t_d1() {
    Stage0_R_3_t_d1 = ap_const_lv32_0;
}

void fft::thread_Stage0_R_3_t_read() {
    Stage0_R_3_t_read = fft_fft_stage_first_U0_ap_ready.read();
}

void fft::thread_Stage0_R_3_t_we0() {
    Stage0_R_3_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage0_R_3_t_we1() {
    Stage0_R_3_t_we1 = ap_const_logic_0;
}

void fft::thread_Stage1_I_U_ap_dummy_ce() {
    Stage1_I_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage1_I_i_address0() {
    Stage1_I_i_address0 = fft_fft_stage_first_U0_imag_o_address0.read();
}

void fft::thread_Stage1_I_i_address1() {
    Stage1_I_i_address1 = fft_fft_stage_first_U0_imag_o_address1.read();
}

void fft::thread_Stage1_I_i_ce0() {
    Stage1_I_i_ce0 = fft_fft_stage_first_U0_imag_o_ce0.read();
}

void fft::thread_Stage1_I_i_ce1() {
    Stage1_I_i_ce1 = fft_fft_stage_first_U0_imag_o_ce1.read();
}

void fft::thread_Stage1_I_i_d0() {
    Stage1_I_i_d0 = fft_fft_stage_first_U0_imag_o_d0.read();
}

void fft::thread_Stage1_I_i_d1() {
    Stage1_I_i_d1 = fft_fft_stage_first_U0_imag_o_d1.read();
}

void fft::thread_Stage1_I_i_we0() {
    Stage1_I_i_we0 = fft_fft_stage_first_U0_imag_o_we0.read();
}

void fft::thread_Stage1_I_i_we1() {
    Stage1_I_i_we1 = fft_fft_stage_first_U0_imag_o_we1.read();
}

void fft::thread_Stage1_I_i_write() {
    Stage1_I_i_write = ap_chn_write_fft_fft_stage_first_U0_Stage1_I.read();
}

void fft::thread_Stage1_I_t_address0() {
    Stage1_I_t_address0 = fft_fft_stages10_U0_imag_i_address0.read();
}

void fft::thread_Stage1_I_t_address1() {
    Stage1_I_t_address1 = fft_fft_stages10_U0_imag_i_address1.read();
}

void fft::thread_Stage1_I_t_ce0() {
    Stage1_I_t_ce0 = fft_fft_stages10_U0_imag_i_ce0.read();
}

void fft::thread_Stage1_I_t_ce1() {
    Stage1_I_t_ce1 = fft_fft_stages10_U0_imag_i_ce1.read();
}

void fft::thread_Stage1_I_t_d0() {
    Stage1_I_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage1_I_t_d1() {
    Stage1_I_t_d1 = ap_const_lv32_0;
}

void fft::thread_Stage1_I_t_read() {
    Stage1_I_t_read = fft_fft_stages10_U0_ap_ready.read();
}

void fft::thread_Stage1_I_t_we0() {
    Stage1_I_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage1_I_t_we1() {
    Stage1_I_t_we1 = ap_const_logic_0;
}

void fft::thread_Stage1_R_0_U_ap_dummy_ce() {
    Stage1_R_0_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage1_R_0_i_address0() {
    Stage1_R_0_i_address0 = fft_fft_stage_first_U0_real_o_0_address0.read();
}

void fft::thread_Stage1_R_0_i_address1() {
    Stage1_R_0_i_address1 = ap_const_lv8_0;
}

void fft::thread_Stage1_R_0_i_ce0() {
    Stage1_R_0_i_ce0 = fft_fft_stage_first_U0_real_o_0_ce0.read();
}

void fft::thread_Stage1_R_0_i_ce1() {
    Stage1_R_0_i_ce1 = ap_const_logic_0;
}

void fft::thread_Stage1_R_0_i_d0() {
    Stage1_R_0_i_d0 = fft_fft_stage_first_U0_real_o_0_d0.read();
}

void fft::thread_Stage1_R_0_i_we0() {
    Stage1_R_0_i_we0 = fft_fft_stage_first_U0_real_o_0_we0.read();
}

void fft::thread_Stage1_R_0_i_write() {
    Stage1_R_0_i_write = ap_chn_write_fft_fft_stage_first_U0_Stage1_R_0.read();
}

void fft::thread_Stage1_R_0_t_address0() {
    Stage1_R_0_t_address0 = fft_fft_stages10_U0_real_i_address0.read();
}

void fft::thread_Stage1_R_0_t_address1() {
    Stage1_R_0_t_address1 = fft_fft_stages10_U0_real_i_address1.read();
}

void fft::thread_Stage1_R_0_t_ce0() {
    Stage1_R_0_t_ce0 = fft_fft_stages10_U0_real_i_ce0.read();
}

void fft::thread_Stage1_R_0_t_ce1() {
    Stage1_R_0_t_ce1 = fft_fft_stages10_U0_real_i_ce1.read();
}

void fft::thread_Stage1_R_0_t_d0() {
    Stage1_R_0_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage1_R_0_t_d1() {
    Stage1_R_0_t_d1 = ap_const_lv32_0;
}

void fft::thread_Stage1_R_0_t_read() {
    Stage1_R_0_t_read = fft_fft_stages10_U0_ap_ready.read();
}

void fft::thread_Stage1_R_0_t_we0() {
    Stage1_R_0_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage1_R_0_t_we1() {
    Stage1_R_0_t_we1 = ap_const_logic_0;
}

void fft::thread_Stage1_R_1_U_ap_dummy_ce() {
    Stage1_R_1_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage1_R_1_i_address0() {
    Stage1_R_1_i_address0 = fft_fft_stage_first_U0_real_o_1_address0.read();
}

void fft::thread_Stage1_R_1_i_address1() {
    Stage1_R_1_i_address1 = ap_const_lv8_0;
}

void fft::thread_Stage1_R_1_i_ce0() {
    Stage1_R_1_i_ce0 = fft_fft_stage_first_U0_real_o_1_ce0.read();
}

void fft::thread_Stage1_R_1_i_ce1() {
    Stage1_R_1_i_ce1 = ap_const_logic_0;
}

void fft::thread_Stage1_R_1_i_d0() {
    Stage1_R_1_i_d0 = fft_fft_stage_first_U0_real_o_1_d0.read();
}

void fft::thread_Stage1_R_1_i_we0() {
    Stage1_R_1_i_we0 = fft_fft_stage_first_U0_real_o_1_we0.read();
}

void fft::thread_Stage1_R_1_i_write() {
    Stage1_R_1_i_write = ap_chn_write_fft_fft_stage_first_U0_Stage1_R_1.read();
}

void fft::thread_Stage1_R_1_t_address0() {
    Stage1_R_1_t_address0 = fft_fft_stages10_U0_real_i1_address0.read();
}

void fft::thread_Stage1_R_1_t_address1() {
    Stage1_R_1_t_address1 = fft_fft_stages10_U0_real_i1_address1.read();
}

void fft::thread_Stage1_R_1_t_ce0() {
    Stage1_R_1_t_ce0 = fft_fft_stages10_U0_real_i1_ce0.read();
}

void fft::thread_Stage1_R_1_t_ce1() {
    Stage1_R_1_t_ce1 = fft_fft_stages10_U0_real_i1_ce1.read();
}

void fft::thread_Stage1_R_1_t_d0() {
    Stage1_R_1_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage1_R_1_t_d1() {
    Stage1_R_1_t_d1 = ap_const_lv32_0;
}

void fft::thread_Stage1_R_1_t_read() {
    Stage1_R_1_t_read = fft_fft_stages10_U0_ap_ready.read();
}

void fft::thread_Stage1_R_1_t_we0() {
    Stage1_R_1_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage1_R_1_t_we1() {
    Stage1_R_1_t_we1 = ap_const_logic_0;
}

void fft::thread_Stage1_R_2_U_ap_dummy_ce() {
    Stage1_R_2_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage1_R_2_i_address0() {
    Stage1_R_2_i_address0 = fft_fft_stage_first_U0_real_o_2_address0.read();
}

void fft::thread_Stage1_R_2_i_address1() {
    Stage1_R_2_i_address1 = ap_const_lv8_0;
}

void fft::thread_Stage1_R_2_i_ce0() {
    Stage1_R_2_i_ce0 = fft_fft_stage_first_U0_real_o_2_ce0.read();
}

void fft::thread_Stage1_R_2_i_ce1() {
    Stage1_R_2_i_ce1 = ap_const_logic_0;
}

void fft::thread_Stage1_R_2_i_d0() {
    Stage1_R_2_i_d0 = fft_fft_stage_first_U0_real_o_2_d0.read();
}

void fft::thread_Stage1_R_2_i_we0() {
    Stage1_R_2_i_we0 = fft_fft_stage_first_U0_real_o_2_we0.read();
}

void fft::thread_Stage1_R_2_i_write() {
    Stage1_R_2_i_write = ap_chn_write_fft_fft_stage_first_U0_Stage1_R_2.read();
}

void fft::thread_Stage1_R_2_t_address0() {
    Stage1_R_2_t_address0 = fft_fft_stages10_U0_real_i2_address0.read();
}

void fft::thread_Stage1_R_2_t_address1() {
    Stage1_R_2_t_address1 = fft_fft_stages10_U0_real_i2_address1.read();
}

void fft::thread_Stage1_R_2_t_ce0() {
    Stage1_R_2_t_ce0 = fft_fft_stages10_U0_real_i2_ce0.read();
}

void fft::thread_Stage1_R_2_t_ce1() {
    Stage1_R_2_t_ce1 = fft_fft_stages10_U0_real_i2_ce1.read();
}

void fft::thread_Stage1_R_2_t_d0() {
    Stage1_R_2_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage1_R_2_t_d1() {
    Stage1_R_2_t_d1 = ap_const_lv32_0;
}

void fft::thread_Stage1_R_2_t_read() {
    Stage1_R_2_t_read = fft_fft_stages10_U0_ap_ready.read();
}

void fft::thread_Stage1_R_2_t_we0() {
    Stage1_R_2_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage1_R_2_t_we1() {
    Stage1_R_2_t_we1 = ap_const_logic_0;
}

void fft::thread_Stage1_R_3_U_ap_dummy_ce() {
    Stage1_R_3_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage1_R_3_i_address0() {
    Stage1_R_3_i_address0 = fft_fft_stage_first_U0_real_o_3_address0.read();
}

void fft::thread_Stage1_R_3_i_address1() {
    Stage1_R_3_i_address1 = ap_const_lv8_0;
}

void fft::thread_Stage1_R_3_i_ce0() {
    Stage1_R_3_i_ce0 = fft_fft_stage_first_U0_real_o_3_ce0.read();
}

void fft::thread_Stage1_R_3_i_ce1() {
    Stage1_R_3_i_ce1 = ap_const_logic_0;
}

void fft::thread_Stage1_R_3_i_d0() {
    Stage1_R_3_i_d0 = fft_fft_stage_first_U0_real_o_3_d0.read();
}

void fft::thread_Stage1_R_3_i_we0() {
    Stage1_R_3_i_we0 = fft_fft_stage_first_U0_real_o_3_we0.read();
}

void fft::thread_Stage1_R_3_i_write() {
    Stage1_R_3_i_write = ap_chn_write_fft_fft_stage_first_U0_Stage1_R_3.read();
}

void fft::thread_Stage1_R_3_t_address0() {
    Stage1_R_3_t_address0 = fft_fft_stages10_U0_real_i3_address0.read();
}

void fft::thread_Stage1_R_3_t_address1() {
    Stage1_R_3_t_address1 = fft_fft_stages10_U0_real_i3_address1.read();
}

void fft::thread_Stage1_R_3_t_ce0() {
    Stage1_R_3_t_ce0 = fft_fft_stages10_U0_real_i3_ce0.read();
}

void fft::thread_Stage1_R_3_t_ce1() {
    Stage1_R_3_t_ce1 = fft_fft_stages10_U0_real_i3_ce1.read();
}

void fft::thread_Stage1_R_3_t_d0() {
    Stage1_R_3_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage1_R_3_t_d1() {
    Stage1_R_3_t_d1 = ap_const_lv32_0;
}

void fft::thread_Stage1_R_3_t_read() {
    Stage1_R_3_t_read = fft_fft_stages10_U0_ap_ready.read();
}

void fft::thread_Stage1_R_3_t_we0() {
    Stage1_R_3_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage1_R_3_t_we1() {
    Stage1_R_3_t_we1 = ap_const_logic_0;
}

void fft::thread_Stage2_I_U_ap_dummy_ce() {
    Stage2_I_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage2_I_i_address0() {
    Stage2_I_i_address0 = fft_fft_stages10_U0_imag_o_address0.read();
}

void fft::thread_Stage2_I_i_address1() {
    Stage2_I_i_address1 = fft_fft_stages10_U0_imag_o_address1.read();
}

void fft::thread_Stage2_I_i_ce0() {
    Stage2_I_i_ce0 = fft_fft_stages10_U0_imag_o_ce0.read();
}

void fft::thread_Stage2_I_i_ce1() {
    Stage2_I_i_ce1 = fft_fft_stages10_U0_imag_o_ce1.read();
}

void fft::thread_Stage2_I_i_d0() {
    Stage2_I_i_d0 = fft_fft_stages10_U0_imag_o_d0.read();
}

void fft::thread_Stage2_I_i_d1() {
    Stage2_I_i_d1 = fft_fft_stages10_U0_imag_o_d1.read();
}

void fft::thread_Stage2_I_i_we0() {
    Stage2_I_i_we0 = fft_fft_stages10_U0_imag_o_we0.read();
}

void fft::thread_Stage2_I_i_we1() {
    Stage2_I_i_we1 = fft_fft_stages10_U0_imag_o_we1.read();
}

void fft::thread_Stage2_I_i_write() {
    Stage2_I_i_write = ap_chn_write_fft_fft_stages10_U0_Stage2_I.read();
}

void fft::thread_Stage2_I_t_address0() {
    Stage2_I_t_address0 = fft_fft_stages11_U0_imag_i_address0.read();
}

void fft::thread_Stage2_I_t_address1() {
    Stage2_I_t_address1 = ap_const_lv10_0;
}

void fft::thread_Stage2_I_t_ce0() {
    Stage2_I_t_ce0 = fft_fft_stages11_U0_imag_i_ce0.read();
}

void fft::thread_Stage2_I_t_ce1() {
    Stage2_I_t_ce1 = ap_const_logic_0;
}

void fft::thread_Stage2_I_t_d0() {
    Stage2_I_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage2_I_t_d1() {
    Stage2_I_t_d1 = ap_const_lv32_0;
}

void fft::thread_Stage2_I_t_read() {
    Stage2_I_t_read = fft_fft_stages11_U0_ap_ready.read();
}

void fft::thread_Stage2_I_t_we0() {
    Stage2_I_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage2_I_t_we1() {
    Stage2_I_t_we1 = ap_const_logic_0;
}

void fft::thread_Stage2_R_0_U_ap_dummy_ce() {
    Stage2_R_0_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage2_R_0_i_address0() {
    Stage2_R_0_i_address0 = fft_fft_stages10_U0_real_o_address0.read();
}

void fft::thread_Stage2_R_0_i_ce0() {
    Stage2_R_0_i_ce0 = fft_fft_stages10_U0_real_o_ce0.read();
}

void fft::thread_Stage2_R_0_i_d0() {
    Stage2_R_0_i_d0 = fft_fft_stages10_U0_real_o_d0.read();
}

void fft::thread_Stage2_R_0_i_we0() {
    Stage2_R_0_i_we0 = fft_fft_stages10_U0_real_o_we0.read();
}

void fft::thread_Stage2_R_0_i_write() {
    Stage2_R_0_i_write = ap_chn_write_fft_fft_stages10_U0_Stage2_R_0.read();
}

void fft::thread_Stage2_R_0_t_address0() {
    Stage2_R_0_t_address0 = fft_fft_stages11_U0_real_i_address0.read();
}

void fft::thread_Stage2_R_0_t_ce0() {
    Stage2_R_0_t_ce0 = fft_fft_stages11_U0_real_i_ce0.read();
}

void fft::thread_Stage2_R_0_t_d0() {
    Stage2_R_0_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage2_R_0_t_read() {
    Stage2_R_0_t_read = fft_fft_stages11_U0_ap_ready.read();
}

void fft::thread_Stage2_R_0_t_we0() {
    Stage2_R_0_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage2_R_1_U_ap_dummy_ce() {
    Stage2_R_1_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage2_R_1_i_address0() {
    Stage2_R_1_i_address0 = fft_fft_stages10_U0_real_o4_address0.read();
}

void fft::thread_Stage2_R_1_i_ce0() {
    Stage2_R_1_i_ce0 = fft_fft_stages10_U0_real_o4_ce0.read();
}

void fft::thread_Stage2_R_1_i_d0() {
    Stage2_R_1_i_d0 = fft_fft_stages10_U0_real_o4_d0.read();
}

void fft::thread_Stage2_R_1_i_we0() {
    Stage2_R_1_i_we0 = fft_fft_stages10_U0_real_o4_we0.read();
}

void fft::thread_Stage2_R_1_i_write() {
    Stage2_R_1_i_write = ap_chn_write_fft_fft_stages10_U0_Stage2_R_1.read();
}

void fft::thread_Stage2_R_1_t_address0() {
    Stage2_R_1_t_address0 = fft_fft_stages11_U0_real_i1_address0.read();
}

void fft::thread_Stage2_R_1_t_ce0() {
    Stage2_R_1_t_ce0 = fft_fft_stages11_U0_real_i1_ce0.read();
}

void fft::thread_Stage2_R_1_t_d0() {
    Stage2_R_1_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage2_R_1_t_read() {
    Stage2_R_1_t_read = fft_fft_stages11_U0_ap_ready.read();
}

void fft::thread_Stage2_R_1_t_we0() {
    Stage2_R_1_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage2_R_2_U_ap_dummy_ce() {
    Stage2_R_2_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage2_R_2_i_address0() {
    Stage2_R_2_i_address0 = fft_fft_stages10_U0_real_o5_address0.read();
}

void fft::thread_Stage2_R_2_i_ce0() {
    Stage2_R_2_i_ce0 = fft_fft_stages10_U0_real_o5_ce0.read();
}

void fft::thread_Stage2_R_2_i_d0() {
    Stage2_R_2_i_d0 = fft_fft_stages10_U0_real_o5_d0.read();
}

void fft::thread_Stage2_R_2_i_we0() {
    Stage2_R_2_i_we0 = fft_fft_stages10_U0_real_o5_we0.read();
}

void fft::thread_Stage2_R_2_i_write() {
    Stage2_R_2_i_write = ap_chn_write_fft_fft_stages10_U0_Stage2_R_2.read();
}

void fft::thread_Stage2_R_2_t_address0() {
    Stage2_R_2_t_address0 = fft_fft_stages11_U0_real_i2_address0.read();
}

void fft::thread_Stage2_R_2_t_ce0() {
    Stage2_R_2_t_ce0 = fft_fft_stages11_U0_real_i2_ce0.read();
}

void fft::thread_Stage2_R_2_t_d0() {
    Stage2_R_2_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage2_R_2_t_read() {
    Stage2_R_2_t_read = fft_fft_stages11_U0_ap_ready.read();
}

void fft::thread_Stage2_R_2_t_we0() {
    Stage2_R_2_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage2_R_3_U_ap_dummy_ce() {
    Stage2_R_3_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage2_R_3_i_address0() {
    Stage2_R_3_i_address0 = fft_fft_stages10_U0_real_o6_address0.read();
}

void fft::thread_Stage2_R_3_i_ce0() {
    Stage2_R_3_i_ce0 = fft_fft_stages10_U0_real_o6_ce0.read();
}

void fft::thread_Stage2_R_3_i_d0() {
    Stage2_R_3_i_d0 = fft_fft_stages10_U0_real_o6_d0.read();
}

void fft::thread_Stage2_R_3_i_we0() {
    Stage2_R_3_i_we0 = fft_fft_stages10_U0_real_o6_we0.read();
}

void fft::thread_Stage2_R_3_i_write() {
    Stage2_R_3_i_write = ap_chn_write_fft_fft_stages10_U0_Stage2_R_3.read();
}

void fft::thread_Stage2_R_3_t_address0() {
    Stage2_R_3_t_address0 = fft_fft_stages11_U0_real_i3_address0.read();
}

void fft::thread_Stage2_R_3_t_ce0() {
    Stage2_R_3_t_ce0 = fft_fft_stages11_U0_real_i3_ce0.read();
}

void fft::thread_Stage2_R_3_t_d0() {
    Stage2_R_3_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage2_R_3_t_read() {
    Stage2_R_3_t_read = fft_fft_stages11_U0_ap_ready.read();
}

void fft::thread_Stage2_R_3_t_we0() {
    Stage2_R_3_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage3_I_U_ap_dummy_ce() {
    Stage3_I_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage3_I_i_address0() {
    Stage3_I_i_address0 = fft_fft_stages11_U0_imag_o_address0.read();
}

void fft::thread_Stage3_I_i_ce0() {
    Stage3_I_i_ce0 = fft_fft_stages11_U0_imag_o_ce0.read();
}

void fft::thread_Stage3_I_i_d0() {
    Stage3_I_i_d0 = fft_fft_stages11_U0_imag_o_d0.read();
}

void fft::thread_Stage3_I_i_we0() {
    Stage3_I_i_we0 = fft_fft_stages11_U0_imag_o_we0.read();
}

void fft::thread_Stage3_I_i_write() {
    Stage3_I_i_write = ap_chn_write_fft_fft_stages11_U0_Stage3_I.read();
}

void fft::thread_Stage3_I_t_address0() {
    Stage3_I_t_address0 = fft_fft_stages12_U0_imag_i_address0.read();
}

void fft::thread_Stage3_I_t_ce0() {
    Stage3_I_t_ce0 = fft_fft_stages12_U0_imag_i_ce0.read();
}

void fft::thread_Stage3_I_t_d0() {
    Stage3_I_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage3_I_t_read() {
    Stage3_I_t_read = fft_fft_stages12_U0_ap_ready.read();
}

void fft::thread_Stage3_I_t_we0() {
    Stage3_I_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage3_R_0_U_ap_dummy_ce() {
    Stage3_R_0_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage3_R_0_i_address0() {
    Stage3_R_0_i_address0 = fft_fft_stages11_U0_real_o_address0.read();
}

void fft::thread_Stage3_R_0_i_ce0() {
    Stage3_R_0_i_ce0 = fft_fft_stages11_U0_real_o_ce0.read();
}

void fft::thread_Stage3_R_0_i_d0() {
    Stage3_R_0_i_d0 = fft_fft_stages11_U0_real_o_d0.read();
}

void fft::thread_Stage3_R_0_i_we0() {
    Stage3_R_0_i_we0 = fft_fft_stages11_U0_real_o_we0.read();
}

void fft::thread_Stage3_R_0_i_write() {
    Stage3_R_0_i_write = ap_chn_write_fft_fft_stages11_U0_Stage3_R_0.read();
}

void fft::thread_Stage3_R_0_t_address0() {
    Stage3_R_0_t_address0 = fft_fft_stages12_U0_real_i_address0.read();
}

void fft::thread_Stage3_R_0_t_ce0() {
    Stage3_R_0_t_ce0 = fft_fft_stages12_U0_real_i_ce0.read();
}

void fft::thread_Stage3_R_0_t_d0() {
    Stage3_R_0_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage3_R_0_t_read() {
    Stage3_R_0_t_read = fft_fft_stages12_U0_ap_ready.read();
}

void fft::thread_Stage3_R_0_t_we0() {
    Stage3_R_0_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage3_R_1_U_ap_dummy_ce() {
    Stage3_R_1_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage3_R_1_i_address0() {
    Stage3_R_1_i_address0 = fft_fft_stages11_U0_real_o4_address0.read();
}

void fft::thread_Stage3_R_1_i_ce0() {
    Stage3_R_1_i_ce0 = fft_fft_stages11_U0_real_o4_ce0.read();
}

void fft::thread_Stage3_R_1_i_d0() {
    Stage3_R_1_i_d0 = fft_fft_stages11_U0_real_o4_d0.read();
}

void fft::thread_Stage3_R_1_i_we0() {
    Stage3_R_1_i_we0 = fft_fft_stages11_U0_real_o4_we0.read();
}

void fft::thread_Stage3_R_1_i_write() {
    Stage3_R_1_i_write = ap_chn_write_fft_fft_stages11_U0_Stage3_R_1.read();
}

void fft::thread_Stage3_R_1_t_address0() {
    Stage3_R_1_t_address0 = fft_fft_stages12_U0_real_i1_address0.read();
}

void fft::thread_Stage3_R_1_t_ce0() {
    Stage3_R_1_t_ce0 = fft_fft_stages12_U0_real_i1_ce0.read();
}

void fft::thread_Stage3_R_1_t_d0() {
    Stage3_R_1_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage3_R_1_t_read() {
    Stage3_R_1_t_read = fft_fft_stages12_U0_ap_ready.read();
}

void fft::thread_Stage3_R_1_t_we0() {
    Stage3_R_1_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage3_R_2_U_ap_dummy_ce() {
    Stage3_R_2_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage3_R_2_i_address0() {
    Stage3_R_2_i_address0 = fft_fft_stages11_U0_real_o5_address0.read();
}

void fft::thread_Stage3_R_2_i_ce0() {
    Stage3_R_2_i_ce0 = fft_fft_stages11_U0_real_o5_ce0.read();
}

void fft::thread_Stage3_R_2_i_d0() {
    Stage3_R_2_i_d0 = fft_fft_stages11_U0_real_o5_d0.read();
}

void fft::thread_Stage3_R_2_i_we0() {
    Stage3_R_2_i_we0 = fft_fft_stages11_U0_real_o5_we0.read();
}

void fft::thread_Stage3_R_2_i_write() {
    Stage3_R_2_i_write = ap_chn_write_fft_fft_stages11_U0_Stage3_R_2.read();
}

void fft::thread_Stage3_R_2_t_address0() {
    Stage3_R_2_t_address0 = fft_fft_stages12_U0_real_i2_address0.read();
}

void fft::thread_Stage3_R_2_t_ce0() {
    Stage3_R_2_t_ce0 = fft_fft_stages12_U0_real_i2_ce0.read();
}

void fft::thread_Stage3_R_2_t_d0() {
    Stage3_R_2_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage3_R_2_t_read() {
    Stage3_R_2_t_read = fft_fft_stages12_U0_ap_ready.read();
}

void fft::thread_Stage3_R_2_t_we0() {
    Stage3_R_2_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage3_R_3_U_ap_dummy_ce() {
    Stage3_R_3_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage3_R_3_i_address0() {
    Stage3_R_3_i_address0 = fft_fft_stages11_U0_real_o6_address0.read();
}

void fft::thread_Stage3_R_3_i_ce0() {
    Stage3_R_3_i_ce0 = fft_fft_stages11_U0_real_o6_ce0.read();
}

void fft::thread_Stage3_R_3_i_d0() {
    Stage3_R_3_i_d0 = fft_fft_stages11_U0_real_o6_d0.read();
}

void fft::thread_Stage3_R_3_i_we0() {
    Stage3_R_3_i_we0 = fft_fft_stages11_U0_real_o6_we0.read();
}

void fft::thread_Stage3_R_3_i_write() {
    Stage3_R_3_i_write = ap_chn_write_fft_fft_stages11_U0_Stage3_R_3.read();
}

void fft::thread_Stage3_R_3_t_address0() {
    Stage3_R_3_t_address0 = fft_fft_stages12_U0_real_i3_address0.read();
}

void fft::thread_Stage3_R_3_t_ce0() {
    Stage3_R_3_t_ce0 = fft_fft_stages12_U0_real_i3_ce0.read();
}

void fft::thread_Stage3_R_3_t_d0() {
    Stage3_R_3_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage3_R_3_t_read() {
    Stage3_R_3_t_read = fft_fft_stages12_U0_ap_ready.read();
}

void fft::thread_Stage3_R_3_t_we0() {
    Stage3_R_3_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage4_I_U_ap_dummy_ce() {
    Stage4_I_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage4_I_i_address0() {
    Stage4_I_i_address0 = fft_fft_stages12_U0_imag_o_address0.read();
}

void fft::thread_Stage4_I_i_ce0() {
    Stage4_I_i_ce0 = fft_fft_stages12_U0_imag_o_ce0.read();
}

void fft::thread_Stage4_I_i_d0() {
    Stage4_I_i_d0 = fft_fft_stages12_U0_imag_o_d0.read();
}

void fft::thread_Stage4_I_i_we0() {
    Stage4_I_i_we0 = fft_fft_stages12_U0_imag_o_we0.read();
}

void fft::thread_Stage4_I_i_write() {
    Stage4_I_i_write = ap_chn_write_fft_fft_stages12_U0_Stage4_I.read();
}

void fft::thread_Stage4_I_t_address0() {
    Stage4_I_t_address0 = fft_fft_stages13_U0_imag_i_address0.read();
}

void fft::thread_Stage4_I_t_ce0() {
    Stage4_I_t_ce0 = fft_fft_stages13_U0_imag_i_ce0.read();
}

void fft::thread_Stage4_I_t_d0() {
    Stage4_I_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage4_I_t_read() {
    Stage4_I_t_read = fft_fft_stages13_U0_ap_ready.read();
}

void fft::thread_Stage4_I_t_we0() {
    Stage4_I_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage4_R_0_U_ap_dummy_ce() {
    Stage4_R_0_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage4_R_0_i_address0() {
    Stage4_R_0_i_address0 = fft_fft_stages12_U0_real_o_address0.read();
}

void fft::thread_Stage4_R_0_i_ce0() {
    Stage4_R_0_i_ce0 = fft_fft_stages12_U0_real_o_ce0.read();
}

void fft::thread_Stage4_R_0_i_d0() {
    Stage4_R_0_i_d0 = fft_fft_stages12_U0_real_o_d0.read();
}

void fft::thread_Stage4_R_0_i_we0() {
    Stage4_R_0_i_we0 = fft_fft_stages12_U0_real_o_we0.read();
}

void fft::thread_Stage4_R_0_i_write() {
    Stage4_R_0_i_write = ap_chn_write_fft_fft_stages12_U0_Stage4_R_0.read();
}

void fft::thread_Stage4_R_0_t_address0() {
    Stage4_R_0_t_address0 = fft_fft_stages13_U0_real_i_address0.read();
}

void fft::thread_Stage4_R_0_t_ce0() {
    Stage4_R_0_t_ce0 = fft_fft_stages13_U0_real_i_ce0.read();
}

void fft::thread_Stage4_R_0_t_d0() {
    Stage4_R_0_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage4_R_0_t_read() {
    Stage4_R_0_t_read = fft_fft_stages13_U0_ap_ready.read();
}

void fft::thread_Stage4_R_0_t_we0() {
    Stage4_R_0_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage4_R_1_U_ap_dummy_ce() {
    Stage4_R_1_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage4_R_1_i_address0() {
    Stage4_R_1_i_address0 = fft_fft_stages12_U0_real_o4_address0.read();
}

void fft::thread_Stage4_R_1_i_ce0() {
    Stage4_R_1_i_ce0 = fft_fft_stages12_U0_real_o4_ce0.read();
}

void fft::thread_Stage4_R_1_i_d0() {
    Stage4_R_1_i_d0 = fft_fft_stages12_U0_real_o4_d0.read();
}

void fft::thread_Stage4_R_1_i_we0() {
    Stage4_R_1_i_we0 = fft_fft_stages12_U0_real_o4_we0.read();
}

void fft::thread_Stage4_R_1_i_write() {
    Stage4_R_1_i_write = ap_chn_write_fft_fft_stages12_U0_Stage4_R_1.read();
}

void fft::thread_Stage4_R_1_t_address0() {
    Stage4_R_1_t_address0 = fft_fft_stages13_U0_real_i1_address0.read();
}

void fft::thread_Stage4_R_1_t_ce0() {
    Stage4_R_1_t_ce0 = fft_fft_stages13_U0_real_i1_ce0.read();
}

void fft::thread_Stage4_R_1_t_d0() {
    Stage4_R_1_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage4_R_1_t_read() {
    Stage4_R_1_t_read = fft_fft_stages13_U0_ap_ready.read();
}

void fft::thread_Stage4_R_1_t_we0() {
    Stage4_R_1_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage4_R_2_U_ap_dummy_ce() {
    Stage4_R_2_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage4_R_2_i_address0() {
    Stage4_R_2_i_address0 = fft_fft_stages12_U0_real_o5_address0.read();
}

void fft::thread_Stage4_R_2_i_ce0() {
    Stage4_R_2_i_ce0 = fft_fft_stages12_U0_real_o5_ce0.read();
}

void fft::thread_Stage4_R_2_i_d0() {
    Stage4_R_2_i_d0 = fft_fft_stages12_U0_real_o5_d0.read();
}

void fft::thread_Stage4_R_2_i_we0() {
    Stage4_R_2_i_we0 = fft_fft_stages12_U0_real_o5_we0.read();
}

void fft::thread_Stage4_R_2_i_write() {
    Stage4_R_2_i_write = ap_chn_write_fft_fft_stages12_U0_Stage4_R_2.read();
}

void fft::thread_Stage4_R_2_t_address0() {
    Stage4_R_2_t_address0 = fft_fft_stages13_U0_real_i2_address0.read();
}

void fft::thread_Stage4_R_2_t_ce0() {
    Stage4_R_2_t_ce0 = fft_fft_stages13_U0_real_i2_ce0.read();
}

void fft::thread_Stage4_R_2_t_d0() {
    Stage4_R_2_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage4_R_2_t_read() {
    Stage4_R_2_t_read = fft_fft_stages13_U0_ap_ready.read();
}

void fft::thread_Stage4_R_2_t_we0() {
    Stage4_R_2_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage4_R_3_U_ap_dummy_ce() {
    Stage4_R_3_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage4_R_3_i_address0() {
    Stage4_R_3_i_address0 = fft_fft_stages12_U0_real_o6_address0.read();
}

void fft::thread_Stage4_R_3_i_ce0() {
    Stage4_R_3_i_ce0 = fft_fft_stages12_U0_real_o6_ce0.read();
}

void fft::thread_Stage4_R_3_i_d0() {
    Stage4_R_3_i_d0 = fft_fft_stages12_U0_real_o6_d0.read();
}

void fft::thread_Stage4_R_3_i_we0() {
    Stage4_R_3_i_we0 = fft_fft_stages12_U0_real_o6_we0.read();
}

void fft::thread_Stage4_R_3_i_write() {
    Stage4_R_3_i_write = ap_chn_write_fft_fft_stages12_U0_Stage4_R_3.read();
}

void fft::thread_Stage4_R_3_t_address0() {
    Stage4_R_3_t_address0 = fft_fft_stages13_U0_real_i3_address0.read();
}

void fft::thread_Stage4_R_3_t_ce0() {
    Stage4_R_3_t_ce0 = fft_fft_stages13_U0_real_i3_ce0.read();
}

void fft::thread_Stage4_R_3_t_d0() {
    Stage4_R_3_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage4_R_3_t_read() {
    Stage4_R_3_t_read = fft_fft_stages13_U0_ap_ready.read();
}

void fft::thread_Stage4_R_3_t_we0() {
    Stage4_R_3_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage5_I_U_ap_dummy_ce() {
    Stage5_I_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage5_I_i_address0() {
    Stage5_I_i_address0 = fft_fft_stages13_U0_imag_o_address0.read();
}

void fft::thread_Stage5_I_i_ce0() {
    Stage5_I_i_ce0 = fft_fft_stages13_U0_imag_o_ce0.read();
}

void fft::thread_Stage5_I_i_d0() {
    Stage5_I_i_d0 = fft_fft_stages13_U0_imag_o_d0.read();
}

void fft::thread_Stage5_I_i_we0() {
    Stage5_I_i_we0 = fft_fft_stages13_U0_imag_o_we0.read();
}

void fft::thread_Stage5_I_i_write() {
    Stage5_I_i_write = ap_chn_write_fft_fft_stages13_U0_Stage5_I.read();
}

void fft::thread_Stage5_I_t_address0() {
    Stage5_I_t_address0 = fft_fft_stages14_U0_imag_i_address0.read();
}

void fft::thread_Stage5_I_t_ce0() {
    Stage5_I_t_ce0 = fft_fft_stages14_U0_imag_i_ce0.read();
}

void fft::thread_Stage5_I_t_d0() {
    Stage5_I_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage5_I_t_read() {
    Stage5_I_t_read = fft_fft_stages14_U0_ap_ready.read();
}

void fft::thread_Stage5_I_t_we0() {
    Stage5_I_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage5_R_0_U_ap_dummy_ce() {
    Stage5_R_0_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage5_R_0_i_address0() {
    Stage5_R_0_i_address0 = fft_fft_stages13_U0_real_o_address0.read();
}

void fft::thread_Stage5_R_0_i_ce0() {
    Stage5_R_0_i_ce0 = fft_fft_stages13_U0_real_o_ce0.read();
}

void fft::thread_Stage5_R_0_i_d0() {
    Stage5_R_0_i_d0 = fft_fft_stages13_U0_real_o_d0.read();
}

void fft::thread_Stage5_R_0_i_we0() {
    Stage5_R_0_i_we0 = fft_fft_stages13_U0_real_o_we0.read();
}

void fft::thread_Stage5_R_0_i_write() {
    Stage5_R_0_i_write = ap_chn_write_fft_fft_stages13_U0_Stage5_R_0.read();
}

void fft::thread_Stage5_R_0_t_address0() {
    Stage5_R_0_t_address0 = fft_fft_stages14_U0_real_i_address0.read();
}

void fft::thread_Stage5_R_0_t_ce0() {
    Stage5_R_0_t_ce0 = fft_fft_stages14_U0_real_i_ce0.read();
}

void fft::thread_Stage5_R_0_t_d0() {
    Stage5_R_0_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage5_R_0_t_read() {
    Stage5_R_0_t_read = fft_fft_stages14_U0_ap_ready.read();
}

void fft::thread_Stage5_R_0_t_we0() {
    Stage5_R_0_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage5_R_1_U_ap_dummy_ce() {
    Stage5_R_1_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage5_R_1_i_address0() {
    Stage5_R_1_i_address0 = fft_fft_stages13_U0_real_o4_address0.read();
}

void fft::thread_Stage5_R_1_i_ce0() {
    Stage5_R_1_i_ce0 = fft_fft_stages13_U0_real_o4_ce0.read();
}

void fft::thread_Stage5_R_1_i_d0() {
    Stage5_R_1_i_d0 = fft_fft_stages13_U0_real_o4_d0.read();
}

void fft::thread_Stage5_R_1_i_we0() {
    Stage5_R_1_i_we0 = fft_fft_stages13_U0_real_o4_we0.read();
}

void fft::thread_Stage5_R_1_i_write() {
    Stage5_R_1_i_write = ap_chn_write_fft_fft_stages13_U0_Stage5_R_1.read();
}

void fft::thread_Stage5_R_1_t_address0() {
    Stage5_R_1_t_address0 = fft_fft_stages14_U0_real_i1_address0.read();
}

void fft::thread_Stage5_R_1_t_ce0() {
    Stage5_R_1_t_ce0 = fft_fft_stages14_U0_real_i1_ce0.read();
}

void fft::thread_Stage5_R_1_t_d0() {
    Stage5_R_1_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage5_R_1_t_read() {
    Stage5_R_1_t_read = fft_fft_stages14_U0_ap_ready.read();
}

void fft::thread_Stage5_R_1_t_we0() {
    Stage5_R_1_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage5_R_2_U_ap_dummy_ce() {
    Stage5_R_2_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage5_R_2_i_address0() {
    Stage5_R_2_i_address0 = fft_fft_stages13_U0_real_o5_address0.read();
}

void fft::thread_Stage5_R_2_i_ce0() {
    Stage5_R_2_i_ce0 = fft_fft_stages13_U0_real_o5_ce0.read();
}

void fft::thread_Stage5_R_2_i_d0() {
    Stage5_R_2_i_d0 = fft_fft_stages13_U0_real_o5_d0.read();
}

void fft::thread_Stage5_R_2_i_we0() {
    Stage5_R_2_i_we0 = fft_fft_stages13_U0_real_o5_we0.read();
}

void fft::thread_Stage5_R_2_i_write() {
    Stage5_R_2_i_write = ap_chn_write_fft_fft_stages13_U0_Stage5_R_2.read();
}

void fft::thread_Stage5_R_2_t_address0() {
    Stage5_R_2_t_address0 = fft_fft_stages14_U0_real_i2_address0.read();
}

void fft::thread_Stage5_R_2_t_ce0() {
    Stage5_R_2_t_ce0 = fft_fft_stages14_U0_real_i2_ce0.read();
}

void fft::thread_Stage5_R_2_t_d0() {
    Stage5_R_2_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage5_R_2_t_read() {
    Stage5_R_2_t_read = fft_fft_stages14_U0_ap_ready.read();
}

void fft::thread_Stage5_R_2_t_we0() {
    Stage5_R_2_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage5_R_3_U_ap_dummy_ce() {
    Stage5_R_3_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage5_R_3_i_address0() {
    Stage5_R_3_i_address0 = fft_fft_stages13_U0_real_o6_address0.read();
}

void fft::thread_Stage5_R_3_i_ce0() {
    Stage5_R_3_i_ce0 = fft_fft_stages13_U0_real_o6_ce0.read();
}

void fft::thread_Stage5_R_3_i_d0() {
    Stage5_R_3_i_d0 = fft_fft_stages13_U0_real_o6_d0.read();
}

void fft::thread_Stage5_R_3_i_we0() {
    Stage5_R_3_i_we0 = fft_fft_stages13_U0_real_o6_we0.read();
}

void fft::thread_Stage5_R_3_i_write() {
    Stage5_R_3_i_write = ap_chn_write_fft_fft_stages13_U0_Stage5_R_3.read();
}

void fft::thread_Stage5_R_3_t_address0() {
    Stage5_R_3_t_address0 = fft_fft_stages14_U0_real_i3_address0.read();
}

void fft::thread_Stage5_R_3_t_ce0() {
    Stage5_R_3_t_ce0 = fft_fft_stages14_U0_real_i3_ce0.read();
}

void fft::thread_Stage5_R_3_t_d0() {
    Stage5_R_3_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage5_R_3_t_read() {
    Stage5_R_3_t_read = fft_fft_stages14_U0_ap_ready.read();
}

void fft::thread_Stage5_R_3_t_we0() {
    Stage5_R_3_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage6_I_U_ap_dummy_ce() {
    Stage6_I_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage6_I_i_address0() {
    Stage6_I_i_address0 = fft_fft_stages14_U0_imag_o_address0.read();
}

void fft::thread_Stage6_I_i_ce0() {
    Stage6_I_i_ce0 = fft_fft_stages14_U0_imag_o_ce0.read();
}

void fft::thread_Stage6_I_i_d0() {
    Stage6_I_i_d0 = fft_fft_stages14_U0_imag_o_d0.read();
}

void fft::thread_Stage6_I_i_we0() {
    Stage6_I_i_we0 = fft_fft_stages14_U0_imag_o_we0.read();
}

void fft::thread_Stage6_I_i_write() {
    Stage6_I_i_write = ap_chn_write_fft_fft_stages14_U0_Stage6_I.read();
}

void fft::thread_Stage6_I_t_address0() {
    Stage6_I_t_address0 = fft_fft_stages15_U0_imag_i_address0.read();
}

void fft::thread_Stage6_I_t_ce0() {
    Stage6_I_t_ce0 = fft_fft_stages15_U0_imag_i_ce0.read();
}

void fft::thread_Stage6_I_t_d0() {
    Stage6_I_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage6_I_t_read() {
    Stage6_I_t_read = fft_fft_stages15_U0_ap_ready.read();
}

void fft::thread_Stage6_I_t_we0() {
    Stage6_I_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage6_R_0_U_ap_dummy_ce() {
    Stage6_R_0_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage6_R_0_i_address0() {
    Stage6_R_0_i_address0 = fft_fft_stages14_U0_real_o_address0.read();
}

void fft::thread_Stage6_R_0_i_ce0() {
    Stage6_R_0_i_ce0 = fft_fft_stages14_U0_real_o_ce0.read();
}

void fft::thread_Stage6_R_0_i_d0() {
    Stage6_R_0_i_d0 = fft_fft_stages14_U0_real_o_d0.read();
}

void fft::thread_Stage6_R_0_i_we0() {
    Stage6_R_0_i_we0 = fft_fft_stages14_U0_real_o_we0.read();
}

void fft::thread_Stage6_R_0_i_write() {
    Stage6_R_0_i_write = ap_chn_write_fft_fft_stages14_U0_Stage6_R_0.read();
}

void fft::thread_Stage6_R_0_t_address0() {
    Stage6_R_0_t_address0 = fft_fft_stages15_U0_real_i_address0.read();
}

void fft::thread_Stage6_R_0_t_ce0() {
    Stage6_R_0_t_ce0 = fft_fft_stages15_U0_real_i_ce0.read();
}

void fft::thread_Stage6_R_0_t_d0() {
    Stage6_R_0_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage6_R_0_t_read() {
    Stage6_R_0_t_read = fft_fft_stages15_U0_ap_ready.read();
}

void fft::thread_Stage6_R_0_t_we0() {
    Stage6_R_0_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage6_R_1_U_ap_dummy_ce() {
    Stage6_R_1_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage6_R_1_i_address0() {
    Stage6_R_1_i_address0 = fft_fft_stages14_U0_real_o4_address0.read();
}

void fft::thread_Stage6_R_1_i_ce0() {
    Stage6_R_1_i_ce0 = fft_fft_stages14_U0_real_o4_ce0.read();
}

void fft::thread_Stage6_R_1_i_d0() {
    Stage6_R_1_i_d0 = fft_fft_stages14_U0_real_o4_d0.read();
}

void fft::thread_Stage6_R_1_i_we0() {
    Stage6_R_1_i_we0 = fft_fft_stages14_U0_real_o4_we0.read();
}

void fft::thread_Stage6_R_1_i_write() {
    Stage6_R_1_i_write = ap_chn_write_fft_fft_stages14_U0_Stage6_R_1.read();
}

void fft::thread_Stage6_R_1_t_address0() {
    Stage6_R_1_t_address0 = fft_fft_stages15_U0_real_i1_address0.read();
}

void fft::thread_Stage6_R_1_t_ce0() {
    Stage6_R_1_t_ce0 = fft_fft_stages15_U0_real_i1_ce0.read();
}

void fft::thread_Stage6_R_1_t_d0() {
    Stage6_R_1_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage6_R_1_t_read() {
    Stage6_R_1_t_read = fft_fft_stages15_U0_ap_ready.read();
}

void fft::thread_Stage6_R_1_t_we0() {
    Stage6_R_1_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage6_R_2_U_ap_dummy_ce() {
    Stage6_R_2_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage6_R_2_i_address0() {
    Stage6_R_2_i_address0 = fft_fft_stages14_U0_real_o5_address0.read();
}

void fft::thread_Stage6_R_2_i_ce0() {
    Stage6_R_2_i_ce0 = fft_fft_stages14_U0_real_o5_ce0.read();
}

void fft::thread_Stage6_R_2_i_d0() {
    Stage6_R_2_i_d0 = fft_fft_stages14_U0_real_o5_d0.read();
}

void fft::thread_Stage6_R_2_i_we0() {
    Stage6_R_2_i_we0 = fft_fft_stages14_U0_real_o5_we0.read();
}

void fft::thread_Stage6_R_2_i_write() {
    Stage6_R_2_i_write = ap_chn_write_fft_fft_stages14_U0_Stage6_R_2.read();
}

void fft::thread_Stage6_R_2_t_address0() {
    Stage6_R_2_t_address0 = fft_fft_stages15_U0_real_i2_address0.read();
}

void fft::thread_Stage6_R_2_t_ce0() {
    Stage6_R_2_t_ce0 = fft_fft_stages15_U0_real_i2_ce0.read();
}

void fft::thread_Stage6_R_2_t_d0() {
    Stage6_R_2_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage6_R_2_t_read() {
    Stage6_R_2_t_read = fft_fft_stages15_U0_ap_ready.read();
}

void fft::thread_Stage6_R_2_t_we0() {
    Stage6_R_2_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage6_R_3_U_ap_dummy_ce() {
    Stage6_R_3_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage6_R_3_i_address0() {
    Stage6_R_3_i_address0 = fft_fft_stages14_U0_real_o6_address0.read();
}

void fft::thread_Stage6_R_3_i_ce0() {
    Stage6_R_3_i_ce0 = fft_fft_stages14_U0_real_o6_ce0.read();
}

void fft::thread_Stage6_R_3_i_d0() {
    Stage6_R_3_i_d0 = fft_fft_stages14_U0_real_o6_d0.read();
}

void fft::thread_Stage6_R_3_i_we0() {
    Stage6_R_3_i_we0 = fft_fft_stages14_U0_real_o6_we0.read();
}

void fft::thread_Stage6_R_3_i_write() {
    Stage6_R_3_i_write = ap_chn_write_fft_fft_stages14_U0_Stage6_R_3.read();
}

void fft::thread_Stage6_R_3_t_address0() {
    Stage6_R_3_t_address0 = fft_fft_stages15_U0_real_i3_address0.read();
}

void fft::thread_Stage6_R_3_t_ce0() {
    Stage6_R_3_t_ce0 = fft_fft_stages15_U0_real_i3_ce0.read();
}

void fft::thread_Stage6_R_3_t_d0() {
    Stage6_R_3_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage6_R_3_t_read() {
    Stage6_R_3_t_read = fft_fft_stages15_U0_ap_ready.read();
}

void fft::thread_Stage6_R_3_t_we0() {
    Stage6_R_3_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage7_I_U_ap_dummy_ce() {
    Stage7_I_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage7_I_i_address0() {
    Stage7_I_i_address0 = fft_fft_stages15_U0_imag_o_address0.read();
}

void fft::thread_Stage7_I_i_ce0() {
    Stage7_I_i_ce0 = fft_fft_stages15_U0_imag_o_ce0.read();
}

void fft::thread_Stage7_I_i_d0() {
    Stage7_I_i_d0 = fft_fft_stages15_U0_imag_o_d0.read();
}

void fft::thread_Stage7_I_i_we0() {
    Stage7_I_i_we0 = fft_fft_stages15_U0_imag_o_we0.read();
}

void fft::thread_Stage7_I_i_write() {
    Stage7_I_i_write = ap_chn_write_fft_fft_stages15_U0_Stage7_I.read();
}

void fft::thread_Stage7_I_t_address0() {
    Stage7_I_t_address0 = fft_fft_stages16_U0_imag_i_address0.read();
}

void fft::thread_Stage7_I_t_ce0() {
    Stage7_I_t_ce0 = fft_fft_stages16_U0_imag_i_ce0.read();
}

void fft::thread_Stage7_I_t_d0() {
    Stage7_I_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage7_I_t_read() {
    Stage7_I_t_read = fft_fft_stages16_U0_ap_ready.read();
}

void fft::thread_Stage7_I_t_we0() {
    Stage7_I_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage7_R_0_U_ap_dummy_ce() {
    Stage7_R_0_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage7_R_0_i_address0() {
    Stage7_R_0_i_address0 = fft_fft_stages15_U0_real_o_address0.read();
}

void fft::thread_Stage7_R_0_i_ce0() {
    Stage7_R_0_i_ce0 = fft_fft_stages15_U0_real_o_ce0.read();
}

void fft::thread_Stage7_R_0_i_d0() {
    Stage7_R_0_i_d0 = fft_fft_stages15_U0_real_o_d0.read();
}

void fft::thread_Stage7_R_0_i_we0() {
    Stage7_R_0_i_we0 = fft_fft_stages15_U0_real_o_we0.read();
}

void fft::thread_Stage7_R_0_i_write() {
    Stage7_R_0_i_write = ap_chn_write_fft_fft_stages15_U0_Stage7_R_0.read();
}

void fft::thread_Stage7_R_0_t_address0() {
    Stage7_R_0_t_address0 = fft_fft_stages16_U0_real_i_address0.read();
}

void fft::thread_Stage7_R_0_t_ce0() {
    Stage7_R_0_t_ce0 = fft_fft_stages16_U0_real_i_ce0.read();
}

void fft::thread_Stage7_R_0_t_d0() {
    Stage7_R_0_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage7_R_0_t_read() {
    Stage7_R_0_t_read = fft_fft_stages16_U0_ap_ready.read();
}

void fft::thread_Stage7_R_0_t_we0() {
    Stage7_R_0_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage7_R_1_U_ap_dummy_ce() {
    Stage7_R_1_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage7_R_1_i_address0() {
    Stage7_R_1_i_address0 = fft_fft_stages15_U0_real_o4_address0.read();
}

void fft::thread_Stage7_R_1_i_ce0() {
    Stage7_R_1_i_ce0 = fft_fft_stages15_U0_real_o4_ce0.read();
}

void fft::thread_Stage7_R_1_i_d0() {
    Stage7_R_1_i_d0 = fft_fft_stages15_U0_real_o4_d0.read();
}

void fft::thread_Stage7_R_1_i_we0() {
    Stage7_R_1_i_we0 = fft_fft_stages15_U0_real_o4_we0.read();
}

void fft::thread_Stage7_R_1_i_write() {
    Stage7_R_1_i_write = ap_chn_write_fft_fft_stages15_U0_Stage7_R_1.read();
}

void fft::thread_Stage7_R_1_t_address0() {
    Stage7_R_1_t_address0 = fft_fft_stages16_U0_real_i1_address0.read();
}

void fft::thread_Stage7_R_1_t_ce0() {
    Stage7_R_1_t_ce0 = fft_fft_stages16_U0_real_i1_ce0.read();
}

void fft::thread_Stage7_R_1_t_d0() {
    Stage7_R_1_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage7_R_1_t_read() {
    Stage7_R_1_t_read = fft_fft_stages16_U0_ap_ready.read();
}

void fft::thread_Stage7_R_1_t_we0() {
    Stage7_R_1_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage7_R_2_U_ap_dummy_ce() {
    Stage7_R_2_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage7_R_2_i_address0() {
    Stage7_R_2_i_address0 = fft_fft_stages15_U0_real_o5_address0.read();
}

void fft::thread_Stage7_R_2_i_ce0() {
    Stage7_R_2_i_ce0 = fft_fft_stages15_U0_real_o5_ce0.read();
}

void fft::thread_Stage7_R_2_i_d0() {
    Stage7_R_2_i_d0 = fft_fft_stages15_U0_real_o5_d0.read();
}

void fft::thread_Stage7_R_2_i_we0() {
    Stage7_R_2_i_we0 = fft_fft_stages15_U0_real_o5_we0.read();
}

void fft::thread_Stage7_R_2_i_write() {
    Stage7_R_2_i_write = ap_chn_write_fft_fft_stages15_U0_Stage7_R_2.read();
}

void fft::thread_Stage7_R_2_t_address0() {
    Stage7_R_2_t_address0 = fft_fft_stages16_U0_real_i2_address0.read();
}

void fft::thread_Stage7_R_2_t_ce0() {
    Stage7_R_2_t_ce0 = fft_fft_stages16_U0_real_i2_ce0.read();
}

void fft::thread_Stage7_R_2_t_d0() {
    Stage7_R_2_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage7_R_2_t_read() {
    Stage7_R_2_t_read = fft_fft_stages16_U0_ap_ready.read();
}

void fft::thread_Stage7_R_2_t_we0() {
    Stage7_R_2_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage7_R_3_U_ap_dummy_ce() {
    Stage7_R_3_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage7_R_3_i_address0() {
    Stage7_R_3_i_address0 = fft_fft_stages15_U0_real_o6_address0.read();
}

void fft::thread_Stage7_R_3_i_ce0() {
    Stage7_R_3_i_ce0 = fft_fft_stages15_U0_real_o6_ce0.read();
}

void fft::thread_Stage7_R_3_i_d0() {
    Stage7_R_3_i_d0 = fft_fft_stages15_U0_real_o6_d0.read();
}

void fft::thread_Stage7_R_3_i_we0() {
    Stage7_R_3_i_we0 = fft_fft_stages15_U0_real_o6_we0.read();
}

void fft::thread_Stage7_R_3_i_write() {
    Stage7_R_3_i_write = ap_chn_write_fft_fft_stages15_U0_Stage7_R_3.read();
}

void fft::thread_Stage7_R_3_t_address0() {
    Stage7_R_3_t_address0 = fft_fft_stages16_U0_real_i3_address0.read();
}

void fft::thread_Stage7_R_3_t_ce0() {
    Stage7_R_3_t_ce0 = fft_fft_stages16_U0_real_i3_ce0.read();
}

void fft::thread_Stage7_R_3_t_d0() {
    Stage7_R_3_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage7_R_3_t_read() {
    Stage7_R_3_t_read = fft_fft_stages16_U0_ap_ready.read();
}

void fft::thread_Stage7_R_3_t_we0() {
    Stage7_R_3_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage8_I_U_ap_dummy_ce() {
    Stage8_I_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage8_I_i_address0() {
    Stage8_I_i_address0 = fft_fft_stages16_U0_imag_o_address0.read();
}

void fft::thread_Stage8_I_i_ce0() {
    Stage8_I_i_ce0 = fft_fft_stages16_U0_imag_o_ce0.read();
}

void fft::thread_Stage8_I_i_d0() {
    Stage8_I_i_d0 = fft_fft_stages16_U0_imag_o_d0.read();
}

void fft::thread_Stage8_I_i_we0() {
    Stage8_I_i_we0 = fft_fft_stages16_U0_imag_o_we0.read();
}

void fft::thread_Stage8_I_i_write() {
    Stage8_I_i_write = ap_chn_write_fft_fft_stages16_U0_Stage8_I.read();
}

void fft::thread_Stage8_I_t_address0() {
    Stage8_I_t_address0 = fft_fft_stages17_U0_imag_i_address0.read();
}

void fft::thread_Stage8_I_t_ce0() {
    Stage8_I_t_ce0 = fft_fft_stages17_U0_imag_i_ce0.read();
}

void fft::thread_Stage8_I_t_d0() {
    Stage8_I_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage8_I_t_read() {
    Stage8_I_t_read = fft_fft_stages17_U0_ap_ready.read();
}

void fft::thread_Stage8_I_t_we0() {
    Stage8_I_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage8_R_0_U_ap_dummy_ce() {
    Stage8_R_0_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage8_R_0_i_address0() {
    Stage8_R_0_i_address0 = fft_fft_stages16_U0_real_o_address0.read();
}

void fft::thread_Stage8_R_0_i_ce0() {
    Stage8_R_0_i_ce0 = fft_fft_stages16_U0_real_o_ce0.read();
}

void fft::thread_Stage8_R_0_i_d0() {
    Stage8_R_0_i_d0 = fft_fft_stages16_U0_real_o_d0.read();
}

void fft::thread_Stage8_R_0_i_we0() {
    Stage8_R_0_i_we0 = fft_fft_stages16_U0_real_o_we0.read();
}

void fft::thread_Stage8_R_0_i_write() {
    Stage8_R_0_i_write = ap_chn_write_fft_fft_stages16_U0_Stage8_R_0.read();
}

void fft::thread_Stage8_R_0_t_address0() {
    Stage8_R_0_t_address0 = fft_fft_stages17_U0_real_i_address0.read();
}

void fft::thread_Stage8_R_0_t_ce0() {
    Stage8_R_0_t_ce0 = fft_fft_stages17_U0_real_i_ce0.read();
}

void fft::thread_Stage8_R_0_t_d0() {
    Stage8_R_0_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage8_R_0_t_read() {
    Stage8_R_0_t_read = fft_fft_stages17_U0_ap_ready.read();
}

void fft::thread_Stage8_R_0_t_we0() {
    Stage8_R_0_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage8_R_1_U_ap_dummy_ce() {
    Stage8_R_1_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage8_R_1_i_address0() {
    Stage8_R_1_i_address0 = fft_fft_stages16_U0_real_o4_address0.read();
}

void fft::thread_Stage8_R_1_i_ce0() {
    Stage8_R_1_i_ce0 = fft_fft_stages16_U0_real_o4_ce0.read();
}

void fft::thread_Stage8_R_1_i_d0() {
    Stage8_R_1_i_d0 = fft_fft_stages16_U0_real_o4_d0.read();
}

void fft::thread_Stage8_R_1_i_we0() {
    Stage8_R_1_i_we0 = fft_fft_stages16_U0_real_o4_we0.read();
}

void fft::thread_Stage8_R_1_i_write() {
    Stage8_R_1_i_write = ap_chn_write_fft_fft_stages16_U0_Stage8_R_1.read();
}

void fft::thread_Stage8_R_1_t_address0() {
    Stage8_R_1_t_address0 = fft_fft_stages17_U0_real_i1_address0.read();
}

void fft::thread_Stage8_R_1_t_ce0() {
    Stage8_R_1_t_ce0 = fft_fft_stages17_U0_real_i1_ce0.read();
}

void fft::thread_Stage8_R_1_t_d0() {
    Stage8_R_1_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage8_R_1_t_read() {
    Stage8_R_1_t_read = fft_fft_stages17_U0_ap_ready.read();
}

void fft::thread_Stage8_R_1_t_we0() {
    Stage8_R_1_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage8_R_2_U_ap_dummy_ce() {
    Stage8_R_2_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage8_R_2_i_address0() {
    Stage8_R_2_i_address0 = fft_fft_stages16_U0_real_o5_address0.read();
}

void fft::thread_Stage8_R_2_i_ce0() {
    Stage8_R_2_i_ce0 = fft_fft_stages16_U0_real_o5_ce0.read();
}

void fft::thread_Stage8_R_2_i_d0() {
    Stage8_R_2_i_d0 = fft_fft_stages16_U0_real_o5_d0.read();
}

void fft::thread_Stage8_R_2_i_we0() {
    Stage8_R_2_i_we0 = fft_fft_stages16_U0_real_o5_we0.read();
}

void fft::thread_Stage8_R_2_i_write() {
    Stage8_R_2_i_write = ap_chn_write_fft_fft_stages16_U0_Stage8_R_2.read();
}

void fft::thread_Stage8_R_2_t_address0() {
    Stage8_R_2_t_address0 = fft_fft_stages17_U0_real_i2_address0.read();
}

void fft::thread_Stage8_R_2_t_ce0() {
    Stage8_R_2_t_ce0 = fft_fft_stages17_U0_real_i2_ce0.read();
}

void fft::thread_Stage8_R_2_t_d0() {
    Stage8_R_2_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage8_R_2_t_read() {
    Stage8_R_2_t_read = fft_fft_stages17_U0_ap_ready.read();
}

void fft::thread_Stage8_R_2_t_we0() {
    Stage8_R_2_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage8_R_3_U_ap_dummy_ce() {
    Stage8_R_3_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage8_R_3_i_address0() {
    Stage8_R_3_i_address0 = fft_fft_stages16_U0_real_o6_address0.read();
}

void fft::thread_Stage8_R_3_i_ce0() {
    Stage8_R_3_i_ce0 = fft_fft_stages16_U0_real_o6_ce0.read();
}

void fft::thread_Stage8_R_3_i_d0() {
    Stage8_R_3_i_d0 = fft_fft_stages16_U0_real_o6_d0.read();
}

void fft::thread_Stage8_R_3_i_we0() {
    Stage8_R_3_i_we0 = fft_fft_stages16_U0_real_o6_we0.read();
}

void fft::thread_Stage8_R_3_i_write() {
    Stage8_R_3_i_write = ap_chn_write_fft_fft_stages16_U0_Stage8_R_3.read();
}

void fft::thread_Stage8_R_3_t_address0() {
    Stage8_R_3_t_address0 = fft_fft_stages17_U0_real_i3_address0.read();
}

void fft::thread_Stage8_R_3_t_ce0() {
    Stage8_R_3_t_ce0 = fft_fft_stages17_U0_real_i3_ce0.read();
}

void fft::thread_Stage8_R_3_t_d0() {
    Stage8_R_3_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage8_R_3_t_read() {
    Stage8_R_3_t_read = fft_fft_stages17_U0_ap_ready.read();
}

void fft::thread_Stage8_R_3_t_we0() {
    Stage8_R_3_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage9_I_U_ap_dummy_ce() {
    Stage9_I_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage9_I_i_address0() {
    Stage9_I_i_address0 = fft_fft_stages17_U0_imag_o_address0.read();
}

void fft::thread_Stage9_I_i_address1() {
    Stage9_I_i_address1 = ap_const_lv10_0;
}

void fft::thread_Stage9_I_i_ce0() {
    Stage9_I_i_ce0 = fft_fft_stages17_U0_imag_o_ce0.read();
}

void fft::thread_Stage9_I_i_ce1() {
    Stage9_I_i_ce1 = ap_const_logic_0;
}

void fft::thread_Stage9_I_i_d0() {
    Stage9_I_i_d0 = fft_fft_stages17_U0_imag_o_d0.read();
}

void fft::thread_Stage9_I_i_we0() {
    Stage9_I_i_we0 = fft_fft_stages17_U0_imag_o_we0.read();
}

void fft::thread_Stage9_I_i_write() {
    Stage9_I_i_write = ap_chn_write_fft_fft_stages17_U0_Stage9_I.read();
}

void fft::thread_Stage9_I_t_address0() {
    Stage9_I_t_address0 = fft_fft_stage_last_U0_imag_i_address0.read();
}

void fft::thread_Stage9_I_t_address1() {
    Stage9_I_t_address1 = fft_fft_stage_last_U0_imag_i_address1.read();
}

void fft::thread_Stage9_I_t_ce0() {
    Stage9_I_t_ce0 = fft_fft_stage_last_U0_imag_i_ce0.read();
}

void fft::thread_Stage9_I_t_ce1() {
    Stage9_I_t_ce1 = fft_fft_stage_last_U0_imag_i_ce1.read();
}

void fft::thread_Stage9_I_t_d0() {
    Stage9_I_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage9_I_t_d1() {
    Stage9_I_t_d1 = ap_const_lv32_0;
}

void fft::thread_Stage9_I_t_read() {
    Stage9_I_t_read = fft_fft_stage_last_U0_ap_ready.read();
}

void fft::thread_Stage9_I_t_we0() {
    Stage9_I_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage9_I_t_we1() {
    Stage9_I_t_we1 = ap_const_logic_0;
}

void fft::thread_Stage9_R_0_U_ap_dummy_ce() {
    Stage9_R_0_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage9_R_0_i_address0() {
    Stage9_R_0_i_address0 = fft_fft_stages17_U0_real_o_address0.read();
}

void fft::thread_Stage9_R_0_i_address1() {
    Stage9_R_0_i_address1 = ap_const_lv8_0;
}

void fft::thread_Stage9_R_0_i_ce0() {
    Stage9_R_0_i_ce0 = fft_fft_stages17_U0_real_o_ce0.read();
}

void fft::thread_Stage9_R_0_i_ce1() {
    Stage9_R_0_i_ce1 = ap_const_logic_0;
}

void fft::thread_Stage9_R_0_i_d0() {
    Stage9_R_0_i_d0 = fft_fft_stages17_U0_real_o_d0.read();
}

void fft::thread_Stage9_R_0_i_we0() {
    Stage9_R_0_i_we0 = fft_fft_stages17_U0_real_o_we0.read();
}

void fft::thread_Stage9_R_0_i_write() {
    Stage9_R_0_i_write = ap_chn_write_fft_fft_stages17_U0_Stage9_R_0.read();
}

void fft::thread_Stage9_R_0_t_address0() {
    Stage9_R_0_t_address0 = fft_fft_stage_last_U0_real_i_0_address0.read();
}

void fft::thread_Stage9_R_0_t_address1() {
    Stage9_R_0_t_address1 = fft_fft_stage_last_U0_real_i_0_address1.read();
}

void fft::thread_Stage9_R_0_t_ce0() {
    Stage9_R_0_t_ce0 = fft_fft_stage_last_U0_real_i_0_ce0.read();
}

void fft::thread_Stage9_R_0_t_ce1() {
    Stage9_R_0_t_ce1 = fft_fft_stage_last_U0_real_i_0_ce1.read();
}

void fft::thread_Stage9_R_0_t_d0() {
    Stage9_R_0_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage9_R_0_t_d1() {
    Stage9_R_0_t_d1 = ap_const_lv32_0;
}

void fft::thread_Stage9_R_0_t_read() {
    Stage9_R_0_t_read = fft_fft_stage_last_U0_ap_ready.read();
}

void fft::thread_Stage9_R_0_t_we0() {
    Stage9_R_0_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage9_R_0_t_we1() {
    Stage9_R_0_t_we1 = ap_const_logic_0;
}

void fft::thread_Stage9_R_1_U_ap_dummy_ce() {
    Stage9_R_1_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage9_R_1_i_address0() {
    Stage9_R_1_i_address0 = fft_fft_stages17_U0_real_o4_address0.read();
}

void fft::thread_Stage9_R_1_i_address1() {
    Stage9_R_1_i_address1 = ap_const_lv8_0;
}

void fft::thread_Stage9_R_1_i_ce0() {
    Stage9_R_1_i_ce0 = fft_fft_stages17_U0_real_o4_ce0.read();
}

void fft::thread_Stage9_R_1_i_ce1() {
    Stage9_R_1_i_ce1 = ap_const_logic_0;
}

void fft::thread_Stage9_R_1_i_d0() {
    Stage9_R_1_i_d0 = fft_fft_stages17_U0_real_o4_d0.read();
}

void fft::thread_Stage9_R_1_i_we0() {
    Stage9_R_1_i_we0 = fft_fft_stages17_U0_real_o4_we0.read();
}

void fft::thread_Stage9_R_1_i_write() {
    Stage9_R_1_i_write = ap_chn_write_fft_fft_stages17_U0_Stage9_R_1.read();
}

void fft::thread_Stage9_R_1_t_address0() {
    Stage9_R_1_t_address0 = fft_fft_stage_last_U0_real_i_1_address0.read();
}

void fft::thread_Stage9_R_1_t_address1() {
    Stage9_R_1_t_address1 = fft_fft_stage_last_U0_real_i_1_address1.read();
}

void fft::thread_Stage9_R_1_t_ce0() {
    Stage9_R_1_t_ce0 = fft_fft_stage_last_U0_real_i_1_ce0.read();
}

void fft::thread_Stage9_R_1_t_ce1() {
    Stage9_R_1_t_ce1 = fft_fft_stage_last_U0_real_i_1_ce1.read();
}

void fft::thread_Stage9_R_1_t_d0() {
    Stage9_R_1_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage9_R_1_t_d1() {
    Stage9_R_1_t_d1 = ap_const_lv32_0;
}

void fft::thread_Stage9_R_1_t_read() {
    Stage9_R_1_t_read = fft_fft_stage_last_U0_ap_ready.read();
}

void fft::thread_Stage9_R_1_t_we0() {
    Stage9_R_1_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage9_R_1_t_we1() {
    Stage9_R_1_t_we1 = ap_const_logic_0;
}

void fft::thread_Stage9_R_2_U_ap_dummy_ce() {
    Stage9_R_2_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage9_R_2_i_address0() {
    Stage9_R_2_i_address0 = fft_fft_stages17_U0_real_o5_address0.read();
}

void fft::thread_Stage9_R_2_i_address1() {
    Stage9_R_2_i_address1 = ap_const_lv8_0;
}

void fft::thread_Stage9_R_2_i_ce0() {
    Stage9_R_2_i_ce0 = fft_fft_stages17_U0_real_o5_ce0.read();
}

void fft::thread_Stage9_R_2_i_ce1() {
    Stage9_R_2_i_ce1 = ap_const_logic_0;
}

void fft::thread_Stage9_R_2_i_d0() {
    Stage9_R_2_i_d0 = fft_fft_stages17_U0_real_o5_d0.read();
}

void fft::thread_Stage9_R_2_i_we0() {
    Stage9_R_2_i_we0 = fft_fft_stages17_U0_real_o5_we0.read();
}

void fft::thread_Stage9_R_2_i_write() {
    Stage9_R_2_i_write = ap_chn_write_fft_fft_stages17_U0_Stage9_R_2.read();
}

void fft::thread_Stage9_R_2_t_address0() {
    Stage9_R_2_t_address0 = fft_fft_stage_last_U0_real_i_2_address0.read();
}

void fft::thread_Stage9_R_2_t_address1() {
    Stage9_R_2_t_address1 = fft_fft_stage_last_U0_real_i_2_address1.read();
}

void fft::thread_Stage9_R_2_t_ce0() {
    Stage9_R_2_t_ce0 = fft_fft_stage_last_U0_real_i_2_ce0.read();
}

void fft::thread_Stage9_R_2_t_ce1() {
    Stage9_R_2_t_ce1 = fft_fft_stage_last_U0_real_i_2_ce1.read();
}

void fft::thread_Stage9_R_2_t_d0() {
    Stage9_R_2_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage9_R_2_t_d1() {
    Stage9_R_2_t_d1 = ap_const_lv32_0;
}

void fft::thread_Stage9_R_2_t_read() {
    Stage9_R_2_t_read = fft_fft_stage_last_U0_ap_ready.read();
}

void fft::thread_Stage9_R_2_t_we0() {
    Stage9_R_2_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage9_R_2_t_we1() {
    Stage9_R_2_t_we1 = ap_const_logic_0;
}

void fft::thread_Stage9_R_3_U_ap_dummy_ce() {
    Stage9_R_3_U_ap_dummy_ce = ap_const_logic_1;
}

void fft::thread_Stage9_R_3_i_address0() {
    Stage9_R_3_i_address0 = fft_fft_stages17_U0_real_o6_address0.read();
}

void fft::thread_Stage9_R_3_i_address1() {
    Stage9_R_3_i_address1 = ap_const_lv8_0;
}

void fft::thread_Stage9_R_3_i_ce0() {
    Stage9_R_3_i_ce0 = fft_fft_stages17_U0_real_o6_ce0.read();
}

void fft::thread_Stage9_R_3_i_ce1() {
    Stage9_R_3_i_ce1 = ap_const_logic_0;
}

void fft::thread_Stage9_R_3_i_d0() {
    Stage9_R_3_i_d0 = fft_fft_stages17_U0_real_o6_d0.read();
}

void fft::thread_Stage9_R_3_i_we0() {
    Stage9_R_3_i_we0 = fft_fft_stages17_U0_real_o6_we0.read();
}

void fft::thread_Stage9_R_3_i_write() {
    Stage9_R_3_i_write = ap_chn_write_fft_fft_stages17_U0_Stage9_R_3.read();
}

void fft::thread_Stage9_R_3_t_address0() {
    Stage9_R_3_t_address0 = fft_fft_stage_last_U0_real_i_3_address0.read();
}

void fft::thread_Stage9_R_3_t_address1() {
    Stage9_R_3_t_address1 = fft_fft_stage_last_U0_real_i_3_address1.read();
}

void fft::thread_Stage9_R_3_t_ce0() {
    Stage9_R_3_t_ce0 = fft_fft_stage_last_U0_real_i_3_ce0.read();
}

void fft::thread_Stage9_R_3_t_ce1() {
    Stage9_R_3_t_ce1 = fft_fft_stage_last_U0_real_i_3_ce1.read();
}

void fft::thread_Stage9_R_3_t_d0() {
    Stage9_R_3_t_d0 = ap_const_lv32_0;
}

void fft::thread_Stage9_R_3_t_d1() {
    Stage9_R_3_t_d1 = ap_const_lv32_0;
}

void fft::thread_Stage9_R_3_t_read() {
    Stage9_R_3_t_read = fft_fft_stage_last_U0_ap_ready.read();
}

void fft::thread_Stage9_R_3_t_we0() {
    Stage9_R_3_t_we0 = ap_const_logic_0;
}

void fft::thread_Stage9_R_3_t_we1() {
    Stage9_R_3_t_we1 = ap_const_logic_0;
}

void fft::thread_ap_chn_write_fft_bit_reverse_U0_Stage0_I() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_bit_reverse_U0_imag_o_pipo_status.read())) {
        ap_chn_write_fft_bit_reverse_U0_Stage0_I = ap_const_logic_0;
    } else {
        ap_chn_write_fft_bit_reverse_U0_Stage0_I = fft_bit_reverse_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_bit_reverse_U0_Stage0_R_0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_bit_reverse_U0_real_o_0_pipo_status.read())) {
        ap_chn_write_fft_bit_reverse_U0_Stage0_R_0 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_bit_reverse_U0_Stage0_R_0 = fft_bit_reverse_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_bit_reverse_U0_Stage0_R_1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_bit_reverse_U0_real_o_1_pipo_status.read())) {
        ap_chn_write_fft_bit_reverse_U0_Stage0_R_1 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_bit_reverse_U0_Stage0_R_1 = fft_bit_reverse_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_bit_reverse_U0_Stage0_R_2() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_bit_reverse_U0_real_o_2_pipo_status.read())) {
        ap_chn_write_fft_bit_reverse_U0_Stage0_R_2 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_bit_reverse_U0_Stage0_R_2 = fft_bit_reverse_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_bit_reverse_U0_Stage0_R_3() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_bit_reverse_U0_real_o_3_pipo_status.read())) {
        ap_chn_write_fft_bit_reverse_U0_Stage0_R_3 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_bit_reverse_U0_Stage0_R_3 = fft_bit_reverse_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stage_first_U0_Stage1_I() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stage_first_U0_imag_o_pipo_status.read())) {
        ap_chn_write_fft_fft_stage_first_U0_Stage1_I = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stage_first_U0_Stage1_I = fft_fft_stage_first_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stage_first_U0_Stage1_R_0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stage_first_U0_real_o_0_pipo_status.read())) {
        ap_chn_write_fft_fft_stage_first_U0_Stage1_R_0 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stage_first_U0_Stage1_R_0 = fft_fft_stage_first_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stage_first_U0_Stage1_R_1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stage_first_U0_real_o_1_pipo_status.read())) {
        ap_chn_write_fft_fft_stage_first_U0_Stage1_R_1 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stage_first_U0_Stage1_R_1 = fft_fft_stage_first_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stage_first_U0_Stage1_R_2() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stage_first_U0_real_o_2_pipo_status.read())) {
        ap_chn_write_fft_fft_stage_first_U0_Stage1_R_2 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stage_first_U0_Stage1_R_2 = fft_fft_stage_first_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stage_first_U0_Stage1_R_3() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stage_first_U0_real_o_3_pipo_status.read())) {
        ap_chn_write_fft_fft_stage_first_U0_Stage1_R_3 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stage_first_U0_Stage1_R_3 = fft_fft_stage_first_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages10_U0_Stage2_I() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages10_U0_imag_o_pipo_status.read())) {
        ap_chn_write_fft_fft_stages10_U0_Stage2_I = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages10_U0_Stage2_I = fft_fft_stages10_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages10_U0_Stage2_R_0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages10_U0_real_o_pipo_status.read())) {
        ap_chn_write_fft_fft_stages10_U0_Stage2_R_0 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages10_U0_Stage2_R_0 = fft_fft_stages10_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages10_U0_Stage2_R_1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages10_U0_real_o4_pipo_status.read())) {
        ap_chn_write_fft_fft_stages10_U0_Stage2_R_1 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages10_U0_Stage2_R_1 = fft_fft_stages10_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages10_U0_Stage2_R_2() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages10_U0_real_o5_pipo_status.read())) {
        ap_chn_write_fft_fft_stages10_U0_Stage2_R_2 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages10_U0_Stage2_R_2 = fft_fft_stages10_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages10_U0_Stage2_R_3() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages10_U0_real_o6_pipo_status.read())) {
        ap_chn_write_fft_fft_stages10_U0_Stage2_R_3 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages10_U0_Stage2_R_3 = fft_fft_stages10_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages11_U0_Stage3_I() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages11_U0_imag_o_pipo_status.read())) {
        ap_chn_write_fft_fft_stages11_U0_Stage3_I = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages11_U0_Stage3_I = fft_fft_stages11_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages11_U0_Stage3_R_0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages11_U0_real_o_pipo_status.read())) {
        ap_chn_write_fft_fft_stages11_U0_Stage3_R_0 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages11_U0_Stage3_R_0 = fft_fft_stages11_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages11_U0_Stage3_R_1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages11_U0_real_o4_pipo_status.read())) {
        ap_chn_write_fft_fft_stages11_U0_Stage3_R_1 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages11_U0_Stage3_R_1 = fft_fft_stages11_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages11_U0_Stage3_R_2() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages11_U0_real_o5_pipo_status.read())) {
        ap_chn_write_fft_fft_stages11_U0_Stage3_R_2 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages11_U0_Stage3_R_2 = fft_fft_stages11_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages11_U0_Stage3_R_3() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages11_U0_real_o6_pipo_status.read())) {
        ap_chn_write_fft_fft_stages11_U0_Stage3_R_3 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages11_U0_Stage3_R_3 = fft_fft_stages11_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages12_U0_Stage4_I() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages12_U0_imag_o_pipo_status.read())) {
        ap_chn_write_fft_fft_stages12_U0_Stage4_I = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages12_U0_Stage4_I = fft_fft_stages12_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages12_U0_Stage4_R_0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages12_U0_real_o_pipo_status.read())) {
        ap_chn_write_fft_fft_stages12_U0_Stage4_R_0 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages12_U0_Stage4_R_0 = fft_fft_stages12_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages12_U0_Stage4_R_1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages12_U0_real_o4_pipo_status.read())) {
        ap_chn_write_fft_fft_stages12_U0_Stage4_R_1 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages12_U0_Stage4_R_1 = fft_fft_stages12_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages12_U0_Stage4_R_2() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages12_U0_real_o5_pipo_status.read())) {
        ap_chn_write_fft_fft_stages12_U0_Stage4_R_2 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages12_U0_Stage4_R_2 = fft_fft_stages12_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages12_U0_Stage4_R_3() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages12_U0_real_o6_pipo_status.read())) {
        ap_chn_write_fft_fft_stages12_U0_Stage4_R_3 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages12_U0_Stage4_R_3 = fft_fft_stages12_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages13_U0_Stage5_I() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages13_U0_imag_o_pipo_status.read())) {
        ap_chn_write_fft_fft_stages13_U0_Stage5_I = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages13_U0_Stage5_I = fft_fft_stages13_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages13_U0_Stage5_R_0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages13_U0_real_o_pipo_status.read())) {
        ap_chn_write_fft_fft_stages13_U0_Stage5_R_0 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages13_U0_Stage5_R_0 = fft_fft_stages13_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages13_U0_Stage5_R_1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages13_U0_real_o4_pipo_status.read())) {
        ap_chn_write_fft_fft_stages13_U0_Stage5_R_1 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages13_U0_Stage5_R_1 = fft_fft_stages13_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages13_U0_Stage5_R_2() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages13_U0_real_o5_pipo_status.read())) {
        ap_chn_write_fft_fft_stages13_U0_Stage5_R_2 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages13_U0_Stage5_R_2 = fft_fft_stages13_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages13_U0_Stage5_R_3() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages13_U0_real_o6_pipo_status.read())) {
        ap_chn_write_fft_fft_stages13_U0_Stage5_R_3 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages13_U0_Stage5_R_3 = fft_fft_stages13_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages14_U0_Stage6_I() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages14_U0_imag_o_pipo_status.read())) {
        ap_chn_write_fft_fft_stages14_U0_Stage6_I = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages14_U0_Stage6_I = fft_fft_stages14_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages14_U0_Stage6_R_0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages14_U0_real_o_pipo_status.read())) {
        ap_chn_write_fft_fft_stages14_U0_Stage6_R_0 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages14_U0_Stage6_R_0 = fft_fft_stages14_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages14_U0_Stage6_R_1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages14_U0_real_o4_pipo_status.read())) {
        ap_chn_write_fft_fft_stages14_U0_Stage6_R_1 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages14_U0_Stage6_R_1 = fft_fft_stages14_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages14_U0_Stage6_R_2() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages14_U0_real_o5_pipo_status.read())) {
        ap_chn_write_fft_fft_stages14_U0_Stage6_R_2 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages14_U0_Stage6_R_2 = fft_fft_stages14_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages14_U0_Stage6_R_3() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages14_U0_real_o6_pipo_status.read())) {
        ap_chn_write_fft_fft_stages14_U0_Stage6_R_3 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages14_U0_Stage6_R_3 = fft_fft_stages14_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages15_U0_Stage7_I() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages15_U0_imag_o_pipo_status.read())) {
        ap_chn_write_fft_fft_stages15_U0_Stage7_I = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages15_U0_Stage7_I = fft_fft_stages15_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages15_U0_Stage7_R_0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages15_U0_real_o_pipo_status.read())) {
        ap_chn_write_fft_fft_stages15_U0_Stage7_R_0 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages15_U0_Stage7_R_0 = fft_fft_stages15_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages15_U0_Stage7_R_1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages15_U0_real_o4_pipo_status.read())) {
        ap_chn_write_fft_fft_stages15_U0_Stage7_R_1 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages15_U0_Stage7_R_1 = fft_fft_stages15_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages15_U0_Stage7_R_2() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages15_U0_real_o5_pipo_status.read())) {
        ap_chn_write_fft_fft_stages15_U0_Stage7_R_2 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages15_U0_Stage7_R_2 = fft_fft_stages15_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages15_U0_Stage7_R_3() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages15_U0_real_o6_pipo_status.read())) {
        ap_chn_write_fft_fft_stages15_U0_Stage7_R_3 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages15_U0_Stage7_R_3 = fft_fft_stages15_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages16_U0_Stage8_I() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages16_U0_imag_o_pipo_status.read())) {
        ap_chn_write_fft_fft_stages16_U0_Stage8_I = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages16_U0_Stage8_I = fft_fft_stages16_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages16_U0_Stage8_R_0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages16_U0_real_o_pipo_status.read())) {
        ap_chn_write_fft_fft_stages16_U0_Stage8_R_0 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages16_U0_Stage8_R_0 = fft_fft_stages16_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages16_U0_Stage8_R_1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages16_U0_real_o4_pipo_status.read())) {
        ap_chn_write_fft_fft_stages16_U0_Stage8_R_1 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages16_U0_Stage8_R_1 = fft_fft_stages16_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages16_U0_Stage8_R_2() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages16_U0_real_o5_pipo_status.read())) {
        ap_chn_write_fft_fft_stages16_U0_Stage8_R_2 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages16_U0_Stage8_R_2 = fft_fft_stages16_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages16_U0_Stage8_R_3() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages16_U0_real_o6_pipo_status.read())) {
        ap_chn_write_fft_fft_stages16_U0_Stage8_R_3 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages16_U0_Stage8_R_3 = fft_fft_stages16_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages17_U0_Stage9_I() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages17_U0_imag_o_pipo_status.read())) {
        ap_chn_write_fft_fft_stages17_U0_Stage9_I = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages17_U0_Stage9_I = fft_fft_stages17_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages17_U0_Stage9_R_0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages17_U0_real_o_pipo_status.read())) {
        ap_chn_write_fft_fft_stages17_U0_Stage9_R_0 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages17_U0_Stage9_R_0 = fft_fft_stages17_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages17_U0_Stage9_R_1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages17_U0_real_o4_pipo_status.read())) {
        ap_chn_write_fft_fft_stages17_U0_Stage9_R_1 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages17_U0_Stage9_R_1 = fft_fft_stages17_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages17_U0_Stage9_R_2() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages17_U0_real_o5_pipo_status.read())) {
        ap_chn_write_fft_fft_stages17_U0_Stage9_R_2 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages17_U0_Stage9_R_2 = fft_fft_stages17_U0_ap_done.read();
    }
}

void fft::thread_ap_chn_write_fft_fft_stages17_U0_Stage9_R_3() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ready_fft_fft_stages17_U0_real_o6_pipo_status.read())) {
        ap_chn_write_fft_fft_stages17_U0_Stage9_R_3 = ap_const_logic_0;
    } else {
        ap_chn_write_fft_fft_stages17_U0_Stage9_R_3 = fft_fft_stages17_U0_ap_done.read();
    }
}

void fft::thread_ap_done() {
    ap_done = ap_sig_hs_done.read();
}

void fft::thread_ap_idle() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_ap_idle.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_ap_idle.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_ap_idle.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_ap_idle.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_ap_idle.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_ap_idle.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_ap_idle.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_ap_idle.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_ap_idle.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_ap_idle.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_last_U0_ap_idle.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage0_R_0_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage0_R_1_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage0_R_2_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage0_R_3_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage0_I_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage1_R_0_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage1_R_1_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage1_R_2_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage1_R_3_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage1_I_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage2_R_0_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage2_R_1_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage2_R_2_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage2_R_3_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage2_I_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage3_R_0_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage3_R_1_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage3_R_2_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage3_R_3_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage3_I_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage4_R_0_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage4_R_1_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage4_R_2_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage4_R_3_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage4_I_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage5_R_0_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage5_R_1_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage5_R_2_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage5_R_3_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage5_I_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage6_R_0_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage6_R_1_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage6_R_2_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage6_R_3_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage6_I_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage7_R_0_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage7_R_1_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage7_R_2_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage7_R_3_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage7_I_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage8_R_0_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage8_R_1_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage8_R_2_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage8_R_3_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage8_I_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage9_R_0_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage9_R_1_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage9_R_2_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage9_R_3_t_empty_n.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_0, Stage9_I_t_empty_n.read()))) {
        ap_idle = ap_const_logic_1;
    } else {
        ap_idle = ap_const_logic_0;
    }
}

void fft::thread_ap_ready() {
    ap_ready = ap_sig_top_allready.read();
}

void fft::thread_ap_sig_hs_continue() {
    ap_sig_hs_continue = ap_const_logic_1;
}

void fft::thread_ap_sig_hs_done() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_last_U0_ap_done.read())) {
        ap_sig_hs_done = ap_const_logic_1;
    } else {
        ap_sig_hs_done = ap_const_logic_0;
    }
}

void fft::thread_ap_sig_ready_fft_bit_reverse_U0_imag_o_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_bit_reverse_U0_imag_o_pipo_status.read())) {
        ap_sig_ready_fft_bit_reverse_U0_imag_o_pipo_status = fft_bit_reverse_U0_imag_o_pipo_status.read();
    } else {
        ap_sig_ready_fft_bit_reverse_U0_imag_o_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_bit_reverse_U0_real_o_0_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_bit_reverse_U0_real_o_0_pipo_status.read())) {
        ap_sig_ready_fft_bit_reverse_U0_real_o_0_pipo_status = fft_bit_reverse_U0_real_o_0_pipo_status.read();
    } else {
        ap_sig_ready_fft_bit_reverse_U0_real_o_0_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_bit_reverse_U0_real_o_1_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_bit_reverse_U0_real_o_1_pipo_status.read())) {
        ap_sig_ready_fft_bit_reverse_U0_real_o_1_pipo_status = fft_bit_reverse_U0_real_o_1_pipo_status.read();
    } else {
        ap_sig_ready_fft_bit_reverse_U0_real_o_1_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_bit_reverse_U0_real_o_2_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_bit_reverse_U0_real_o_2_pipo_status.read())) {
        ap_sig_ready_fft_bit_reverse_U0_real_o_2_pipo_status = fft_bit_reverse_U0_real_o_2_pipo_status.read();
    } else {
        ap_sig_ready_fft_bit_reverse_U0_real_o_2_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_bit_reverse_U0_real_o_3_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_bit_reverse_U0_real_o_3_pipo_status.read())) {
        ap_sig_ready_fft_bit_reverse_U0_real_o_3_pipo_status = fft_bit_reverse_U0_real_o_3_pipo_status.read();
    } else {
        ap_sig_ready_fft_bit_reverse_U0_real_o_3_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stage_first_U0_imag_o_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stage_first_U0_imag_o_pipo_status.read())) {
        ap_sig_ready_fft_fft_stage_first_U0_imag_o_pipo_status = fft_fft_stage_first_U0_imag_o_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stage_first_U0_imag_o_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stage_first_U0_real_o_0_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stage_first_U0_real_o_0_pipo_status.read())) {
        ap_sig_ready_fft_fft_stage_first_U0_real_o_0_pipo_status = fft_fft_stage_first_U0_real_o_0_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stage_first_U0_real_o_0_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stage_first_U0_real_o_1_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stage_first_U0_real_o_1_pipo_status.read())) {
        ap_sig_ready_fft_fft_stage_first_U0_real_o_1_pipo_status = fft_fft_stage_first_U0_real_o_1_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stage_first_U0_real_o_1_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stage_first_U0_real_o_2_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stage_first_U0_real_o_2_pipo_status.read())) {
        ap_sig_ready_fft_fft_stage_first_U0_real_o_2_pipo_status = fft_fft_stage_first_U0_real_o_2_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stage_first_U0_real_o_2_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stage_first_U0_real_o_3_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stage_first_U0_real_o_3_pipo_status.read())) {
        ap_sig_ready_fft_fft_stage_first_U0_real_o_3_pipo_status = fft_fft_stage_first_U0_real_o_3_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stage_first_U0_real_o_3_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages10_U0_imag_o_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages10_U0_imag_o_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages10_U0_imag_o_pipo_status = fft_fft_stages10_U0_imag_o_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages10_U0_imag_o_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages10_U0_real_o4_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages10_U0_real_o4_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages10_U0_real_o4_pipo_status = fft_fft_stages10_U0_real_o4_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages10_U0_real_o4_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages10_U0_real_o5_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages10_U0_real_o5_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages10_U0_real_o5_pipo_status = fft_fft_stages10_U0_real_o5_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages10_U0_real_o5_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages10_U0_real_o6_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages10_U0_real_o6_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages10_U0_real_o6_pipo_status = fft_fft_stages10_U0_real_o6_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages10_U0_real_o6_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages10_U0_real_o_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages10_U0_real_o_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages10_U0_real_o_pipo_status = fft_fft_stages10_U0_real_o_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages10_U0_real_o_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages11_U0_imag_o_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages11_U0_imag_o_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages11_U0_imag_o_pipo_status = fft_fft_stages11_U0_imag_o_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages11_U0_imag_o_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages11_U0_real_o4_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages11_U0_real_o4_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages11_U0_real_o4_pipo_status = fft_fft_stages11_U0_real_o4_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages11_U0_real_o4_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages11_U0_real_o5_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages11_U0_real_o5_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages11_U0_real_o5_pipo_status = fft_fft_stages11_U0_real_o5_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages11_U0_real_o5_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages11_U0_real_o6_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages11_U0_real_o6_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages11_U0_real_o6_pipo_status = fft_fft_stages11_U0_real_o6_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages11_U0_real_o6_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages11_U0_real_o_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages11_U0_real_o_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages11_U0_real_o_pipo_status = fft_fft_stages11_U0_real_o_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages11_U0_real_o_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages12_U0_imag_o_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages12_U0_imag_o_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages12_U0_imag_o_pipo_status = fft_fft_stages12_U0_imag_o_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages12_U0_imag_o_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages12_U0_real_o4_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages12_U0_real_o4_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages12_U0_real_o4_pipo_status = fft_fft_stages12_U0_real_o4_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages12_U0_real_o4_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages12_U0_real_o5_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages12_U0_real_o5_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages12_U0_real_o5_pipo_status = fft_fft_stages12_U0_real_o5_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages12_U0_real_o5_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages12_U0_real_o6_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages12_U0_real_o6_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages12_U0_real_o6_pipo_status = fft_fft_stages12_U0_real_o6_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages12_U0_real_o6_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages12_U0_real_o_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages12_U0_real_o_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages12_U0_real_o_pipo_status = fft_fft_stages12_U0_real_o_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages12_U0_real_o_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages13_U0_imag_o_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages13_U0_imag_o_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages13_U0_imag_o_pipo_status = fft_fft_stages13_U0_imag_o_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages13_U0_imag_o_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages13_U0_real_o4_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages13_U0_real_o4_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages13_U0_real_o4_pipo_status = fft_fft_stages13_U0_real_o4_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages13_U0_real_o4_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages13_U0_real_o5_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages13_U0_real_o5_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages13_U0_real_o5_pipo_status = fft_fft_stages13_U0_real_o5_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages13_U0_real_o5_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages13_U0_real_o6_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages13_U0_real_o6_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages13_U0_real_o6_pipo_status = fft_fft_stages13_U0_real_o6_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages13_U0_real_o6_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages13_U0_real_o_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages13_U0_real_o_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages13_U0_real_o_pipo_status = fft_fft_stages13_U0_real_o_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages13_U0_real_o_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages14_U0_imag_o_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages14_U0_imag_o_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages14_U0_imag_o_pipo_status = fft_fft_stages14_U0_imag_o_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages14_U0_imag_o_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages14_U0_real_o4_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages14_U0_real_o4_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages14_U0_real_o4_pipo_status = fft_fft_stages14_U0_real_o4_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages14_U0_real_o4_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages14_U0_real_o5_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages14_U0_real_o5_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages14_U0_real_o5_pipo_status = fft_fft_stages14_U0_real_o5_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages14_U0_real_o5_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages14_U0_real_o6_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages14_U0_real_o6_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages14_U0_real_o6_pipo_status = fft_fft_stages14_U0_real_o6_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages14_U0_real_o6_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages14_U0_real_o_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages14_U0_real_o_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages14_U0_real_o_pipo_status = fft_fft_stages14_U0_real_o_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages14_U0_real_o_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages15_U0_imag_o_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages15_U0_imag_o_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages15_U0_imag_o_pipo_status = fft_fft_stages15_U0_imag_o_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages15_U0_imag_o_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages15_U0_real_o4_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages15_U0_real_o4_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages15_U0_real_o4_pipo_status = fft_fft_stages15_U0_real_o4_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages15_U0_real_o4_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages15_U0_real_o5_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages15_U0_real_o5_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages15_U0_real_o5_pipo_status = fft_fft_stages15_U0_real_o5_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages15_U0_real_o5_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages15_U0_real_o6_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages15_U0_real_o6_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages15_U0_real_o6_pipo_status = fft_fft_stages15_U0_real_o6_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages15_U0_real_o6_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages15_U0_real_o_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages15_U0_real_o_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages15_U0_real_o_pipo_status = fft_fft_stages15_U0_real_o_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages15_U0_real_o_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages16_U0_imag_o_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages16_U0_imag_o_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages16_U0_imag_o_pipo_status = fft_fft_stages16_U0_imag_o_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages16_U0_imag_o_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages16_U0_real_o4_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages16_U0_real_o4_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages16_U0_real_o4_pipo_status = fft_fft_stages16_U0_real_o4_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages16_U0_real_o4_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages16_U0_real_o5_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages16_U0_real_o5_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages16_U0_real_o5_pipo_status = fft_fft_stages16_U0_real_o5_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages16_U0_real_o5_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages16_U0_real_o6_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages16_U0_real_o6_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages16_U0_real_o6_pipo_status = fft_fft_stages16_U0_real_o6_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages16_U0_real_o6_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages16_U0_real_o_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages16_U0_real_o_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages16_U0_real_o_pipo_status = fft_fft_stages16_U0_real_o_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages16_U0_real_o_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages17_U0_imag_o_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages17_U0_imag_o_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages17_U0_imag_o_pipo_status = fft_fft_stages17_U0_imag_o_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages17_U0_imag_o_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages17_U0_real_o4_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages17_U0_real_o4_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages17_U0_real_o4_pipo_status = fft_fft_stages17_U0_real_o4_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages17_U0_real_o4_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages17_U0_real_o5_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages17_U0_real_o5_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages17_U0_real_o5_pipo_status = fft_fft_stages17_U0_real_o5_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages17_U0_real_o5_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages17_U0_real_o6_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages17_U0_real_o6_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages17_U0_real_o6_pipo_status = fft_fft_stages17_U0_real_o6_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages17_U0_real_o6_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_ready_fft_fft_stages17_U0_real_o_pipo_status() {
    if (esl_seteq<1,1,1>(ap_const_logic_0, ap_reg_ready_fft_fft_stages17_U0_real_o_pipo_status.read())) {
        ap_sig_ready_fft_fft_stages17_U0_real_o_pipo_status = fft_fft_stages17_U0_real_o_pipo_status.read();
    } else {
        ap_sig_ready_fft_fft_stages17_U0_real_o_pipo_status = ap_const_logic_1;
    }
}

void fft::thread_ap_sig_top_allready() {
    ap_sig_top_allready = fft_bit_reverse_U0_ap_ready.read();
}

void fft::thread_fft_bit_reverse_U0_ap_continue() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_bit_reverse_U0_real_o_2_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_bit_reverse_U0_real_o_3_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_bit_reverse_U0_imag_o_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_bit_reverse_U0_real_o_0_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_bit_reverse_U0_real_o_1_pipo_status.read()))) {
        fft_bit_reverse_U0_ap_continue = ap_const_logic_1;
    } else {
        fft_bit_reverse_U0_ap_continue = ap_const_logic_0;
    }
}

void fft::thread_fft_bit_reverse_U0_ap_start() {
    fft_bit_reverse_U0_ap_start = ap_start.read();
}

void fft::thread_fft_bit_reverse_U0_imag_i_0_q0() {
    fft_bit_reverse_U0_imag_i_0_q0 = imag_i_0_q0.read();
}

void fft::thread_fft_bit_reverse_U0_imag_i_0_q1() {
    fft_bit_reverse_U0_imag_i_0_q1 = imag_i_0_q1.read();
}

void fft::thread_fft_bit_reverse_U0_imag_i_1_q0() {
    fft_bit_reverse_U0_imag_i_1_q0 = imag_i_1_q0.read();
}

void fft::thread_fft_bit_reverse_U0_imag_i_1_q1() {
    fft_bit_reverse_U0_imag_i_1_q1 = imag_i_1_q1.read();
}

void fft::thread_fft_bit_reverse_U0_imag_i_2_q0() {
    fft_bit_reverse_U0_imag_i_2_q0 = imag_i_2_q0.read();
}

void fft::thread_fft_bit_reverse_U0_imag_i_2_q1() {
    fft_bit_reverse_U0_imag_i_2_q1 = imag_i_2_q1.read();
}

void fft::thread_fft_bit_reverse_U0_imag_i_3_q0() {
    fft_bit_reverse_U0_imag_i_3_q0 = imag_i_3_q0.read();
}

void fft::thread_fft_bit_reverse_U0_imag_i_3_q1() {
    fft_bit_reverse_U0_imag_i_3_q1 = imag_i_3_q1.read();
}

void fft::thread_fft_bit_reverse_U0_imag_i_4_q0() {
    fft_bit_reverse_U0_imag_i_4_q0 = imag_i_4_q0.read();
}

void fft::thread_fft_bit_reverse_U0_imag_i_4_q1() {
    fft_bit_reverse_U0_imag_i_4_q1 = imag_i_4_q1.read();
}

void fft::thread_fft_bit_reverse_U0_imag_i_5_q0() {
    fft_bit_reverse_U0_imag_i_5_q0 = imag_i_5_q0.read();
}

void fft::thread_fft_bit_reverse_U0_imag_i_5_q1() {
    fft_bit_reverse_U0_imag_i_5_q1 = imag_i_5_q1.read();
}

void fft::thread_fft_bit_reverse_U0_imag_i_6_q0() {
    fft_bit_reverse_U0_imag_i_6_q0 = imag_i_6_q0.read();
}

void fft::thread_fft_bit_reverse_U0_imag_i_6_q1() {
    fft_bit_reverse_U0_imag_i_6_q1 = imag_i_6_q1.read();
}

void fft::thread_fft_bit_reverse_U0_imag_i_7_q0() {
    fft_bit_reverse_U0_imag_i_7_q0 = imag_i_7_q0.read();
}

void fft::thread_fft_bit_reverse_U0_imag_i_7_q1() {
    fft_bit_reverse_U0_imag_i_7_q1 = imag_i_7_q1.read();
}

void fft::thread_fft_bit_reverse_U0_imag_o_pipo_status() {
    fft_bit_reverse_U0_imag_o_pipo_status = Stage0_I_i_full_n.read();
}

void fft::thread_fft_bit_reverse_U0_imag_o_q0() {
    fft_bit_reverse_U0_imag_o_q0 = ap_const_lv32_0;
}

void fft::thread_fft_bit_reverse_U0_imag_o_q1() {
    fft_bit_reverse_U0_imag_o_q1 = ap_const_lv32_0;
}

void fft::thread_fft_bit_reverse_U0_real_i_0_q0() {
    fft_bit_reverse_U0_real_i_0_q0 = real_i_0_q0.read();
}

void fft::thread_fft_bit_reverse_U0_real_i_0_q1() {
    fft_bit_reverse_U0_real_i_0_q1 = real_i_0_q1.read();
}

void fft::thread_fft_bit_reverse_U0_real_i_1_q0() {
    fft_bit_reverse_U0_real_i_1_q0 = real_i_1_q0.read();
}

void fft::thread_fft_bit_reverse_U0_real_i_1_q1() {
    fft_bit_reverse_U0_real_i_1_q1 = real_i_1_q1.read();
}

void fft::thread_fft_bit_reverse_U0_real_i_2_q0() {
    fft_bit_reverse_U0_real_i_2_q0 = real_i_2_q0.read();
}

void fft::thread_fft_bit_reverse_U0_real_i_2_q1() {
    fft_bit_reverse_U0_real_i_2_q1 = real_i_2_q1.read();
}

void fft::thread_fft_bit_reverse_U0_real_i_3_q0() {
    fft_bit_reverse_U0_real_i_3_q0 = real_i_3_q0.read();
}

void fft::thread_fft_bit_reverse_U0_real_i_3_q1() {
    fft_bit_reverse_U0_real_i_3_q1 = real_i_3_q1.read();
}

void fft::thread_fft_bit_reverse_U0_real_i_4_q0() {
    fft_bit_reverse_U0_real_i_4_q0 = real_i_4_q0.read();
}

void fft::thread_fft_bit_reverse_U0_real_i_4_q1() {
    fft_bit_reverse_U0_real_i_4_q1 = real_i_4_q1.read();
}

void fft::thread_fft_bit_reverse_U0_real_i_5_q0() {
    fft_bit_reverse_U0_real_i_5_q0 = real_i_5_q0.read();
}

void fft::thread_fft_bit_reverse_U0_real_i_5_q1() {
    fft_bit_reverse_U0_real_i_5_q1 = real_i_5_q1.read();
}

void fft::thread_fft_bit_reverse_U0_real_i_6_q0() {
    fft_bit_reverse_U0_real_i_6_q0 = real_i_6_q0.read();
}

void fft::thread_fft_bit_reverse_U0_real_i_6_q1() {
    fft_bit_reverse_U0_real_i_6_q1 = real_i_6_q1.read();
}

void fft::thread_fft_bit_reverse_U0_real_i_7_q0() {
    fft_bit_reverse_U0_real_i_7_q0 = real_i_7_q0.read();
}

void fft::thread_fft_bit_reverse_U0_real_i_7_q1() {
    fft_bit_reverse_U0_real_i_7_q1 = real_i_7_q1.read();
}

void fft::thread_fft_bit_reverse_U0_real_o_0_pipo_status() {
    fft_bit_reverse_U0_real_o_0_pipo_status = Stage0_R_0_i_full_n.read();
}

void fft::thread_fft_bit_reverse_U0_real_o_0_q0() {
    fft_bit_reverse_U0_real_o_0_q0 = ap_const_lv32_0;
}

void fft::thread_fft_bit_reverse_U0_real_o_0_q1() {
    fft_bit_reverse_U0_real_o_0_q1 = ap_const_lv32_0;
}

void fft::thread_fft_bit_reverse_U0_real_o_1_pipo_status() {
    fft_bit_reverse_U0_real_o_1_pipo_status = Stage0_R_1_i_full_n.read();
}

void fft::thread_fft_bit_reverse_U0_real_o_1_q0() {
    fft_bit_reverse_U0_real_o_1_q0 = ap_const_lv32_0;
}

void fft::thread_fft_bit_reverse_U0_real_o_1_q1() {
    fft_bit_reverse_U0_real_o_1_q1 = ap_const_lv32_0;
}

void fft::thread_fft_bit_reverse_U0_real_o_2_pipo_status() {
    fft_bit_reverse_U0_real_o_2_pipo_status = Stage0_R_2_i_full_n.read();
}

void fft::thread_fft_bit_reverse_U0_real_o_2_q0() {
    fft_bit_reverse_U0_real_o_2_q0 = ap_const_lv32_0;
}

void fft::thread_fft_bit_reverse_U0_real_o_2_q1() {
    fft_bit_reverse_U0_real_o_2_q1 = ap_const_lv32_0;
}

void fft::thread_fft_bit_reverse_U0_real_o_3_pipo_status() {
    fft_bit_reverse_U0_real_o_3_pipo_status = Stage0_R_3_i_full_n.read();
}

void fft::thread_fft_bit_reverse_U0_real_o_3_q0() {
    fft_bit_reverse_U0_real_o_3_q0 = ap_const_lv32_0;
}

void fft::thread_fft_bit_reverse_U0_real_o_3_q1() {
    fft_bit_reverse_U0_real_o_3_q1 = ap_const_lv32_0;
}

void fft::thread_fft_fft_stage_first_U0_ap_continue() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stage_first_U0_real_o_3_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stage_first_U0_imag_o_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stage_first_U0_real_o_2_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stage_first_U0_real_o_0_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stage_first_U0_real_o_1_pipo_status.read()))) {
        fft_fft_stage_first_U0_ap_continue = ap_const_logic_1;
    } else {
        fft_fft_stage_first_U0_ap_continue = ap_const_logic_0;
    }
}

void fft::thread_fft_fft_stage_first_U0_ap_start() {
    fft_fft_stage_first_U0_ap_start = (Stage0_R_0_t_empty_n.read() & Stage0_R_1_t_empty_n.read() & Stage0_R_2_t_empty_n.read() & Stage0_R_3_t_empty_n.read() & Stage0_I_t_empty_n.read());
}

void fft::thread_fft_fft_stage_first_U0_imag_i_q0() {
    fft_fft_stage_first_U0_imag_i_q0 = Stage0_I_t_q0.read();
}

void fft::thread_fft_fft_stage_first_U0_imag_i_q1() {
    fft_fft_stage_first_U0_imag_i_q1 = Stage0_I_t_q1.read();
}

void fft::thread_fft_fft_stage_first_U0_imag_o_pipo_status() {
    fft_fft_stage_first_U0_imag_o_pipo_status = Stage1_I_i_full_n.read();
}

void fft::thread_fft_fft_stage_first_U0_real_i_0_q0() {
    fft_fft_stage_first_U0_real_i_0_q0 = Stage0_R_0_t_q0.read();
}

void fft::thread_fft_fft_stage_first_U0_real_i_0_q1() {
    fft_fft_stage_first_U0_real_i_0_q1 = Stage0_R_0_t_q1.read();
}

void fft::thread_fft_fft_stage_first_U0_real_i_1_q0() {
    fft_fft_stage_first_U0_real_i_1_q0 = Stage0_R_1_t_q0.read();
}

void fft::thread_fft_fft_stage_first_U0_real_i_1_q1() {
    fft_fft_stage_first_U0_real_i_1_q1 = Stage0_R_1_t_q1.read();
}

void fft::thread_fft_fft_stage_first_U0_real_i_2_q0() {
    fft_fft_stage_first_U0_real_i_2_q0 = Stage0_R_2_t_q0.read();
}

void fft::thread_fft_fft_stage_first_U0_real_i_2_q1() {
    fft_fft_stage_first_U0_real_i_2_q1 = Stage0_R_2_t_q1.read();
}

void fft::thread_fft_fft_stage_first_U0_real_i_3_q0() {
    fft_fft_stage_first_U0_real_i_3_q0 = Stage0_R_3_t_q0.read();
}

void fft::thread_fft_fft_stage_first_U0_real_i_3_q1() {
    fft_fft_stage_first_U0_real_i_3_q1 = Stage0_R_3_t_q1.read();
}

void fft::thread_fft_fft_stage_first_U0_real_o_0_pipo_status() {
    fft_fft_stage_first_U0_real_o_0_pipo_status = Stage1_R_0_i_full_n.read();
}

void fft::thread_fft_fft_stage_first_U0_real_o_1_pipo_status() {
    fft_fft_stage_first_U0_real_o_1_pipo_status = Stage1_R_1_i_full_n.read();
}

void fft::thread_fft_fft_stage_first_U0_real_o_2_pipo_status() {
    fft_fft_stage_first_U0_real_o_2_pipo_status = Stage1_R_2_i_full_n.read();
}

void fft::thread_fft_fft_stage_first_U0_real_o_3_pipo_status() {
    fft_fft_stage_first_U0_real_o_3_pipo_status = Stage1_R_3_i_full_n.read();
}

void fft::thread_fft_fft_stage_last_U0_ap_continue() {
    fft_fft_stage_last_U0_ap_continue = ap_sig_hs_continue.read();
}

void fft::thread_fft_fft_stage_last_U0_ap_start() {
    fft_fft_stage_last_U0_ap_start = (Stage9_R_0_t_empty_n.read() & Stage9_R_1_t_empty_n.read() & Stage9_R_2_t_empty_n.read() & Stage9_R_3_t_empty_n.read() & Stage9_I_t_empty_n.read());
}

void fft::thread_fft_fft_stage_last_U0_imag_i_q0() {
    fft_fft_stage_last_U0_imag_i_q0 = Stage9_I_t_q0.read();
}

void fft::thread_fft_fft_stage_last_U0_imag_i_q1() {
    fft_fft_stage_last_U0_imag_i_q1 = Stage9_I_t_q1.read();
}

void fft::thread_fft_fft_stage_last_U0_real_i_0_q0() {
    fft_fft_stage_last_U0_real_i_0_q0 = Stage9_R_0_t_q0.read();
}

void fft::thread_fft_fft_stage_last_U0_real_i_0_q1() {
    fft_fft_stage_last_U0_real_i_0_q1 = Stage9_R_0_t_q1.read();
}

void fft::thread_fft_fft_stage_last_U0_real_i_1_q0() {
    fft_fft_stage_last_U0_real_i_1_q0 = Stage9_R_1_t_q0.read();
}

void fft::thread_fft_fft_stage_last_U0_real_i_1_q1() {
    fft_fft_stage_last_U0_real_i_1_q1 = Stage9_R_1_t_q1.read();
}

void fft::thread_fft_fft_stage_last_U0_real_i_2_q0() {
    fft_fft_stage_last_U0_real_i_2_q0 = Stage9_R_2_t_q0.read();
}

void fft::thread_fft_fft_stage_last_U0_real_i_2_q1() {
    fft_fft_stage_last_U0_real_i_2_q1 = Stage9_R_2_t_q1.read();
}

void fft::thread_fft_fft_stage_last_U0_real_i_3_q0() {
    fft_fft_stage_last_U0_real_i_3_q0 = Stage9_R_3_t_q0.read();
}

void fft::thread_fft_fft_stage_last_U0_real_i_3_q1() {
    fft_fft_stage_last_U0_real_i_3_q1 = Stage9_R_3_t_q1.read();
}

void fft::thread_fft_fft_stages10_U0_ap_continue() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages10_U0_real_o_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages10_U0_real_o6_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages10_U0_real_o5_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages10_U0_imag_o_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages10_U0_real_o4_pipo_status.read()))) {
        fft_fft_stages10_U0_ap_continue = ap_const_logic_1;
    } else {
        fft_fft_stages10_U0_ap_continue = ap_const_logic_0;
    }
}

void fft::thread_fft_fft_stages10_U0_ap_start() {
    fft_fft_stages10_U0_ap_start = (Stage1_R_0_t_empty_n.read() & Stage1_R_1_t_empty_n.read() & Stage1_R_2_t_empty_n.read() & Stage1_R_3_t_empty_n.read() & Stage1_I_t_empty_n.read());
}

void fft::thread_fft_fft_stages10_U0_imag_i_q0() {
    fft_fft_stages10_U0_imag_i_q0 = Stage1_I_t_q0.read();
}

void fft::thread_fft_fft_stages10_U0_imag_i_q1() {
    fft_fft_stages10_U0_imag_i_q1 = Stage1_I_t_q1.read();
}

void fft::thread_fft_fft_stages10_U0_imag_o_pipo_status() {
    fft_fft_stages10_U0_imag_o_pipo_status = Stage2_I_i_full_n.read();
}

void fft::thread_fft_fft_stages10_U0_real_i1_q0() {
    fft_fft_stages10_U0_real_i1_q0 = Stage1_R_1_t_q0.read();
}

void fft::thread_fft_fft_stages10_U0_real_i1_q1() {
    fft_fft_stages10_U0_real_i1_q1 = Stage1_R_1_t_q1.read();
}

void fft::thread_fft_fft_stages10_U0_real_i2_q0() {
    fft_fft_stages10_U0_real_i2_q0 = Stage1_R_2_t_q0.read();
}

void fft::thread_fft_fft_stages10_U0_real_i2_q1() {
    fft_fft_stages10_U0_real_i2_q1 = Stage1_R_2_t_q1.read();
}

void fft::thread_fft_fft_stages10_U0_real_i3_q0() {
    fft_fft_stages10_U0_real_i3_q0 = Stage1_R_3_t_q0.read();
}

void fft::thread_fft_fft_stages10_U0_real_i3_q1() {
    fft_fft_stages10_U0_real_i3_q1 = Stage1_R_3_t_q1.read();
}

void fft::thread_fft_fft_stages10_U0_real_i_q0() {
    fft_fft_stages10_U0_real_i_q0 = Stage1_R_0_t_q0.read();
}

void fft::thread_fft_fft_stages10_U0_real_i_q1() {
    fft_fft_stages10_U0_real_i_q1 = Stage1_R_0_t_q1.read();
}

void fft::thread_fft_fft_stages10_U0_real_o4_pipo_status() {
    fft_fft_stages10_U0_real_o4_pipo_status = Stage2_R_1_i_full_n.read();
}

void fft::thread_fft_fft_stages10_U0_real_o5_pipo_status() {
    fft_fft_stages10_U0_real_o5_pipo_status = Stage2_R_2_i_full_n.read();
}

void fft::thread_fft_fft_stages10_U0_real_o6_pipo_status() {
    fft_fft_stages10_U0_real_o6_pipo_status = Stage2_R_3_i_full_n.read();
}

void fft::thread_fft_fft_stages10_U0_real_o_pipo_status() {
    fft_fft_stages10_U0_real_o_pipo_status = Stage2_R_0_i_full_n.read();
}

void fft::thread_fft_fft_stages11_U0_ap_continue() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages11_U0_real_o_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages11_U0_real_o6_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages11_U0_real_o4_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages11_U0_real_o5_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages11_U0_imag_o_pipo_status.read()))) {
        fft_fft_stages11_U0_ap_continue = ap_const_logic_1;
    } else {
        fft_fft_stages11_U0_ap_continue = ap_const_logic_0;
    }
}

void fft::thread_fft_fft_stages11_U0_ap_start() {
    fft_fft_stages11_U0_ap_start = (Stage2_R_0_t_empty_n.read() & Stage2_R_1_t_empty_n.read() & Stage2_R_2_t_empty_n.read() & Stage2_R_3_t_empty_n.read() & Stage2_I_t_empty_n.read());
}

void fft::thread_fft_fft_stages11_U0_imag_i_q0() {
    fft_fft_stages11_U0_imag_i_q0 = Stage2_I_t_q0.read();
}

void fft::thread_fft_fft_stages11_U0_imag_o_pipo_status() {
    fft_fft_stages11_U0_imag_o_pipo_status = Stage3_I_i_full_n.read();
}

void fft::thread_fft_fft_stages11_U0_real_i1_q0() {
    fft_fft_stages11_U0_real_i1_q0 = Stage2_R_1_t_q0.read();
}

void fft::thread_fft_fft_stages11_U0_real_i2_q0() {
    fft_fft_stages11_U0_real_i2_q0 = Stage2_R_2_t_q0.read();
}

void fft::thread_fft_fft_stages11_U0_real_i3_q0() {
    fft_fft_stages11_U0_real_i3_q0 = Stage2_R_3_t_q0.read();
}

void fft::thread_fft_fft_stages11_U0_real_i_q0() {
    fft_fft_stages11_U0_real_i_q0 = Stage2_R_0_t_q0.read();
}

void fft::thread_fft_fft_stages11_U0_real_o4_pipo_status() {
    fft_fft_stages11_U0_real_o4_pipo_status = Stage3_R_1_i_full_n.read();
}

void fft::thread_fft_fft_stages11_U0_real_o5_pipo_status() {
    fft_fft_stages11_U0_real_o5_pipo_status = Stage3_R_2_i_full_n.read();
}

void fft::thread_fft_fft_stages11_U0_real_o6_pipo_status() {
    fft_fft_stages11_U0_real_o6_pipo_status = Stage3_R_3_i_full_n.read();
}

void fft::thread_fft_fft_stages11_U0_real_o_pipo_status() {
    fft_fft_stages11_U0_real_o_pipo_status = Stage3_R_0_i_full_n.read();
}

void fft::thread_fft_fft_stages12_U0_ap_continue() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages12_U0_imag_o_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages12_U0_real_o6_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages12_U0_real_o4_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages12_U0_real_o5_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages12_U0_real_o_pipo_status.read()))) {
        fft_fft_stages12_U0_ap_continue = ap_const_logic_1;
    } else {
        fft_fft_stages12_U0_ap_continue = ap_const_logic_0;
    }
}

void fft::thread_fft_fft_stages12_U0_ap_start() {
    fft_fft_stages12_U0_ap_start = (Stage3_R_0_t_empty_n.read() & Stage3_R_1_t_empty_n.read() & Stage3_R_2_t_empty_n.read() & Stage3_R_3_t_empty_n.read() & Stage3_I_t_empty_n.read());
}

void fft::thread_fft_fft_stages12_U0_imag_i_q0() {
    fft_fft_stages12_U0_imag_i_q0 = Stage3_I_t_q0.read();
}

void fft::thread_fft_fft_stages12_U0_imag_o_pipo_status() {
    fft_fft_stages12_U0_imag_o_pipo_status = Stage4_I_i_full_n.read();
}

void fft::thread_fft_fft_stages12_U0_real_i1_q0() {
    fft_fft_stages12_U0_real_i1_q0 = Stage3_R_1_t_q0.read();
}

void fft::thread_fft_fft_stages12_U0_real_i2_q0() {
    fft_fft_stages12_U0_real_i2_q0 = Stage3_R_2_t_q0.read();
}

void fft::thread_fft_fft_stages12_U0_real_i3_q0() {
    fft_fft_stages12_U0_real_i3_q0 = Stage3_R_3_t_q0.read();
}

void fft::thread_fft_fft_stages12_U0_real_i_q0() {
    fft_fft_stages12_U0_real_i_q0 = Stage3_R_0_t_q0.read();
}

void fft::thread_fft_fft_stages12_U0_real_o4_pipo_status() {
    fft_fft_stages12_U0_real_o4_pipo_status = Stage4_R_1_i_full_n.read();
}

void fft::thread_fft_fft_stages12_U0_real_o5_pipo_status() {
    fft_fft_stages12_U0_real_o5_pipo_status = Stage4_R_2_i_full_n.read();
}

void fft::thread_fft_fft_stages12_U0_real_o6_pipo_status() {
    fft_fft_stages12_U0_real_o6_pipo_status = Stage4_R_3_i_full_n.read();
}

void fft::thread_fft_fft_stages12_U0_real_o_pipo_status() {
    fft_fft_stages12_U0_real_o_pipo_status = Stage4_R_0_i_full_n.read();
}

void fft::thread_fft_fft_stages13_U0_ap_continue() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages13_U0_real_o4_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages13_U0_real_o_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages13_U0_real_o5_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages13_U0_imag_o_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages13_U0_real_o6_pipo_status.read()))) {
        fft_fft_stages13_U0_ap_continue = ap_const_logic_1;
    } else {
        fft_fft_stages13_U0_ap_continue = ap_const_logic_0;
    }
}

void fft::thread_fft_fft_stages13_U0_ap_start() {
    fft_fft_stages13_U0_ap_start = (Stage4_R_0_t_empty_n.read() & Stage4_R_1_t_empty_n.read() & Stage4_R_2_t_empty_n.read() & Stage4_R_3_t_empty_n.read() & Stage4_I_t_empty_n.read());
}

void fft::thread_fft_fft_stages13_U0_imag_i_q0() {
    fft_fft_stages13_U0_imag_i_q0 = Stage4_I_t_q0.read();
}

void fft::thread_fft_fft_stages13_U0_imag_o_pipo_status() {
    fft_fft_stages13_U0_imag_o_pipo_status = Stage5_I_i_full_n.read();
}

void fft::thread_fft_fft_stages13_U0_real_i1_q0() {
    fft_fft_stages13_U0_real_i1_q0 = Stage4_R_1_t_q0.read();
}

void fft::thread_fft_fft_stages13_U0_real_i2_q0() {
    fft_fft_stages13_U0_real_i2_q0 = Stage4_R_2_t_q0.read();
}

void fft::thread_fft_fft_stages13_U0_real_i3_q0() {
    fft_fft_stages13_U0_real_i3_q0 = Stage4_R_3_t_q0.read();
}

void fft::thread_fft_fft_stages13_U0_real_i_q0() {
    fft_fft_stages13_U0_real_i_q0 = Stage4_R_0_t_q0.read();
}

void fft::thread_fft_fft_stages13_U0_real_o4_pipo_status() {
    fft_fft_stages13_U0_real_o4_pipo_status = Stage5_R_1_i_full_n.read();
}

void fft::thread_fft_fft_stages13_U0_real_o5_pipo_status() {
    fft_fft_stages13_U0_real_o5_pipo_status = Stage5_R_2_i_full_n.read();
}

void fft::thread_fft_fft_stages13_U0_real_o6_pipo_status() {
    fft_fft_stages13_U0_real_o6_pipo_status = Stage5_R_3_i_full_n.read();
}

void fft::thread_fft_fft_stages13_U0_real_o_pipo_status() {
    fft_fft_stages13_U0_real_o_pipo_status = Stage5_R_0_i_full_n.read();
}

void fft::thread_fft_fft_stages14_U0_ap_continue() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages14_U0_real_o6_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages14_U0_imag_o_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages14_U0_real_o5_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages14_U0_real_o4_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages14_U0_real_o_pipo_status.read()))) {
        fft_fft_stages14_U0_ap_continue = ap_const_logic_1;
    } else {
        fft_fft_stages14_U0_ap_continue = ap_const_logic_0;
    }
}

void fft::thread_fft_fft_stages14_U0_ap_start() {
    fft_fft_stages14_U0_ap_start = (Stage5_R_0_t_empty_n.read() & Stage5_R_1_t_empty_n.read() & Stage5_R_2_t_empty_n.read() & Stage5_R_3_t_empty_n.read() & Stage5_I_t_empty_n.read());
}

void fft::thread_fft_fft_stages14_U0_imag_i_q0() {
    fft_fft_stages14_U0_imag_i_q0 = Stage5_I_t_q0.read();
}

void fft::thread_fft_fft_stages14_U0_imag_o_pipo_status() {
    fft_fft_stages14_U0_imag_o_pipo_status = Stage6_I_i_full_n.read();
}

void fft::thread_fft_fft_stages14_U0_real_i1_q0() {
    fft_fft_stages14_U0_real_i1_q0 = Stage5_R_1_t_q0.read();
}

void fft::thread_fft_fft_stages14_U0_real_i2_q0() {
    fft_fft_stages14_U0_real_i2_q0 = Stage5_R_2_t_q0.read();
}

void fft::thread_fft_fft_stages14_U0_real_i3_q0() {
    fft_fft_stages14_U0_real_i3_q0 = Stage5_R_3_t_q0.read();
}

void fft::thread_fft_fft_stages14_U0_real_i_q0() {
    fft_fft_stages14_U0_real_i_q0 = Stage5_R_0_t_q0.read();
}

void fft::thread_fft_fft_stages14_U0_real_o4_pipo_status() {
    fft_fft_stages14_U0_real_o4_pipo_status = Stage6_R_1_i_full_n.read();
}

void fft::thread_fft_fft_stages14_U0_real_o5_pipo_status() {
    fft_fft_stages14_U0_real_o5_pipo_status = Stage6_R_2_i_full_n.read();
}

void fft::thread_fft_fft_stages14_U0_real_o6_pipo_status() {
    fft_fft_stages14_U0_real_o6_pipo_status = Stage6_R_3_i_full_n.read();
}

void fft::thread_fft_fft_stages14_U0_real_o_pipo_status() {
    fft_fft_stages14_U0_real_o_pipo_status = Stage6_R_0_i_full_n.read();
}

void fft::thread_fft_fft_stages15_U0_ap_continue() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages15_U0_real_o6_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages15_U0_imag_o_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages15_U0_real_o4_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages15_U0_real_o_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages15_U0_real_o5_pipo_status.read()))) {
        fft_fft_stages15_U0_ap_continue = ap_const_logic_1;
    } else {
        fft_fft_stages15_U0_ap_continue = ap_const_logic_0;
    }
}

void fft::thread_fft_fft_stages15_U0_ap_start() {
    fft_fft_stages15_U0_ap_start = (Stage6_R_0_t_empty_n.read() & Stage6_R_1_t_empty_n.read() & Stage6_R_2_t_empty_n.read() & Stage6_R_3_t_empty_n.read() & Stage6_I_t_empty_n.read());
}

void fft::thread_fft_fft_stages15_U0_imag_i_q0() {
    fft_fft_stages15_U0_imag_i_q0 = Stage6_I_t_q0.read();
}

void fft::thread_fft_fft_stages15_U0_imag_o_pipo_status() {
    fft_fft_stages15_U0_imag_o_pipo_status = Stage7_I_i_full_n.read();
}

void fft::thread_fft_fft_stages15_U0_real_i1_q0() {
    fft_fft_stages15_U0_real_i1_q0 = Stage6_R_1_t_q0.read();
}

void fft::thread_fft_fft_stages15_U0_real_i2_q0() {
    fft_fft_stages15_U0_real_i2_q0 = Stage6_R_2_t_q0.read();
}

void fft::thread_fft_fft_stages15_U0_real_i3_q0() {
    fft_fft_stages15_U0_real_i3_q0 = Stage6_R_3_t_q0.read();
}

void fft::thread_fft_fft_stages15_U0_real_i_q0() {
    fft_fft_stages15_U0_real_i_q0 = Stage6_R_0_t_q0.read();
}

void fft::thread_fft_fft_stages15_U0_real_o4_pipo_status() {
    fft_fft_stages15_U0_real_o4_pipo_status = Stage7_R_1_i_full_n.read();
}

void fft::thread_fft_fft_stages15_U0_real_o5_pipo_status() {
    fft_fft_stages15_U0_real_o5_pipo_status = Stage7_R_2_i_full_n.read();
}

void fft::thread_fft_fft_stages15_U0_real_o6_pipo_status() {
    fft_fft_stages15_U0_real_o6_pipo_status = Stage7_R_3_i_full_n.read();
}

void fft::thread_fft_fft_stages15_U0_real_o_pipo_status() {
    fft_fft_stages15_U0_real_o_pipo_status = Stage7_R_0_i_full_n.read();
}

void fft::thread_fft_fft_stages16_U0_ap_continue() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages16_U0_real_o5_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages16_U0_real_o6_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages16_U0_real_o_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages16_U0_imag_o_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages16_U0_real_o4_pipo_status.read()))) {
        fft_fft_stages16_U0_ap_continue = ap_const_logic_1;
    } else {
        fft_fft_stages16_U0_ap_continue = ap_const_logic_0;
    }
}

void fft::thread_fft_fft_stages16_U0_ap_start() {
    fft_fft_stages16_U0_ap_start = (Stage7_R_0_t_empty_n.read() & Stage7_R_1_t_empty_n.read() & Stage7_R_2_t_empty_n.read() & Stage7_R_3_t_empty_n.read() & Stage7_I_t_empty_n.read());
}

void fft::thread_fft_fft_stages16_U0_imag_i_q0() {
    fft_fft_stages16_U0_imag_i_q0 = Stage7_I_t_q0.read();
}

void fft::thread_fft_fft_stages16_U0_imag_o_pipo_status() {
    fft_fft_stages16_U0_imag_o_pipo_status = Stage8_I_i_full_n.read();
}

void fft::thread_fft_fft_stages16_U0_real_i1_q0() {
    fft_fft_stages16_U0_real_i1_q0 = Stage7_R_1_t_q0.read();
}

void fft::thread_fft_fft_stages16_U0_real_i2_q0() {
    fft_fft_stages16_U0_real_i2_q0 = Stage7_R_2_t_q0.read();
}

void fft::thread_fft_fft_stages16_U0_real_i3_q0() {
    fft_fft_stages16_U0_real_i3_q0 = Stage7_R_3_t_q0.read();
}

void fft::thread_fft_fft_stages16_U0_real_i_q0() {
    fft_fft_stages16_U0_real_i_q0 = Stage7_R_0_t_q0.read();
}

void fft::thread_fft_fft_stages16_U0_real_o4_pipo_status() {
    fft_fft_stages16_U0_real_o4_pipo_status = Stage8_R_1_i_full_n.read();
}

void fft::thread_fft_fft_stages16_U0_real_o5_pipo_status() {
    fft_fft_stages16_U0_real_o5_pipo_status = Stage8_R_2_i_full_n.read();
}

void fft::thread_fft_fft_stages16_U0_real_o6_pipo_status() {
    fft_fft_stages16_U0_real_o6_pipo_status = Stage8_R_3_i_full_n.read();
}

void fft::thread_fft_fft_stages16_U0_real_o_pipo_status() {
    fft_fft_stages16_U0_real_o_pipo_status = Stage8_R_0_i_full_n.read();
}

void fft::thread_fft_fft_stages17_U0_ap_continue() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages17_U0_real_o_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages17_U0_real_o4_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages17_U0_real_o6_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages17_U0_real_o5_pipo_status.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_ready_fft_fft_stages17_U0_imag_o_pipo_status.read()))) {
        fft_fft_stages17_U0_ap_continue = ap_const_logic_1;
    } else {
        fft_fft_stages17_U0_ap_continue = ap_const_logic_0;
    }
}

void fft::thread_fft_fft_stages17_U0_ap_start() {
    fft_fft_stages17_U0_ap_start = (Stage8_R_0_t_empty_n.read() & Stage8_R_1_t_empty_n.read() & Stage8_R_2_t_empty_n.read() & Stage8_R_3_t_empty_n.read() & Stage8_I_t_empty_n.read());
}

void fft::thread_fft_fft_stages17_U0_imag_i_q0() {
    fft_fft_stages17_U0_imag_i_q0 = Stage8_I_t_q0.read();
}

void fft::thread_fft_fft_stages17_U0_imag_o_pipo_status() {
    fft_fft_stages17_U0_imag_o_pipo_status = Stage9_I_i_full_n.read();
}

void fft::thread_fft_fft_stages17_U0_real_i1_q0() {
    fft_fft_stages17_U0_real_i1_q0 = Stage8_R_1_t_q0.read();
}

void fft::thread_fft_fft_stages17_U0_real_i2_q0() {
    fft_fft_stages17_U0_real_i2_q0 = Stage8_R_2_t_q0.read();
}

void fft::thread_fft_fft_stages17_U0_real_i3_q0() {
    fft_fft_stages17_U0_real_i3_q0 = Stage8_R_3_t_q0.read();
}

void fft::thread_fft_fft_stages17_U0_real_i_q0() {
    fft_fft_stages17_U0_real_i_q0 = Stage8_R_0_t_q0.read();
}

void fft::thread_fft_fft_stages17_U0_real_o4_pipo_status() {
    fft_fft_stages17_U0_real_o4_pipo_status = Stage9_R_1_i_full_n.read();
}

void fft::thread_fft_fft_stages17_U0_real_o5_pipo_status() {
    fft_fft_stages17_U0_real_o5_pipo_status = Stage9_R_2_i_full_n.read();
}

void fft::thread_fft_fft_stages17_U0_real_o6_pipo_status() {
    fft_fft_stages17_U0_real_o6_pipo_status = Stage9_R_3_i_full_n.read();
}

void fft::thread_fft_fft_stages17_U0_real_o_pipo_status() {
    fft_fft_stages17_U0_real_o_pipo_status = Stage9_R_0_i_full_n.read();
}

void fft::thread_imag_i_0_address0() {
    imag_i_0_address0 = fft_bit_reverse_U0_imag_i_0_address0.read();
}

void fft::thread_imag_i_0_address1() {
    imag_i_0_address1 = fft_bit_reverse_U0_imag_i_0_address1.read();
}

void fft::thread_imag_i_0_ce0() {
    imag_i_0_ce0 = fft_bit_reverse_U0_imag_i_0_ce0.read();
}

void fft::thread_imag_i_0_ce1() {
    imag_i_0_ce1 = fft_bit_reverse_U0_imag_i_0_ce1.read();
}

void fft::thread_imag_i_0_d0() {
    imag_i_0_d0 = ap_const_lv32_0;
}

void fft::thread_imag_i_0_d1() {
    imag_i_0_d1 = ap_const_lv32_0;
}

void fft::thread_imag_i_0_we0() {
    imag_i_0_we0 = ap_const_logic_0;
}

void fft::thread_imag_i_0_we1() {
    imag_i_0_we1 = ap_const_logic_0;
}

void fft::thread_imag_i_1_address0() {
    imag_i_1_address0 = fft_bit_reverse_U0_imag_i_1_address0.read();
}

void fft::thread_imag_i_1_address1() {
    imag_i_1_address1 = fft_bit_reverse_U0_imag_i_1_address1.read();
}

void fft::thread_imag_i_1_ce0() {
    imag_i_1_ce0 = fft_bit_reverse_U0_imag_i_1_ce0.read();
}

void fft::thread_imag_i_1_ce1() {
    imag_i_1_ce1 = fft_bit_reverse_U0_imag_i_1_ce1.read();
}

void fft::thread_imag_i_1_d0() {
    imag_i_1_d0 = ap_const_lv32_0;
}

void fft::thread_imag_i_1_d1() {
    imag_i_1_d1 = ap_const_lv32_0;
}

void fft::thread_imag_i_1_we0() {
    imag_i_1_we0 = ap_const_logic_0;
}

void fft::thread_imag_i_1_we1() {
    imag_i_1_we1 = ap_const_logic_0;
}

void fft::thread_imag_i_2_address0() {
    imag_i_2_address0 = fft_bit_reverse_U0_imag_i_2_address0.read();
}

void fft::thread_imag_i_2_address1() {
    imag_i_2_address1 = fft_bit_reverse_U0_imag_i_2_address1.read();
}

void fft::thread_imag_i_2_ce0() {
    imag_i_2_ce0 = fft_bit_reverse_U0_imag_i_2_ce0.read();
}

void fft::thread_imag_i_2_ce1() {
    imag_i_2_ce1 = fft_bit_reverse_U0_imag_i_2_ce1.read();
}

void fft::thread_imag_i_2_d0() {
    imag_i_2_d0 = ap_const_lv32_0;
}

void fft::thread_imag_i_2_d1() {
    imag_i_2_d1 = ap_const_lv32_0;
}

void fft::thread_imag_i_2_we0() {
    imag_i_2_we0 = ap_const_logic_0;
}

void fft::thread_imag_i_2_we1() {
    imag_i_2_we1 = ap_const_logic_0;
}

void fft::thread_imag_i_3_address0() {
    imag_i_3_address0 = fft_bit_reverse_U0_imag_i_3_address0.read();
}

void fft::thread_imag_i_3_address1() {
    imag_i_3_address1 = fft_bit_reverse_U0_imag_i_3_address1.read();
}

void fft::thread_imag_i_3_ce0() {
    imag_i_3_ce0 = fft_bit_reverse_U0_imag_i_3_ce0.read();
}

void fft::thread_imag_i_3_ce1() {
    imag_i_3_ce1 = fft_bit_reverse_U0_imag_i_3_ce1.read();
}

void fft::thread_imag_i_3_d0() {
    imag_i_3_d0 = ap_const_lv32_0;
}

void fft::thread_imag_i_3_d1() {
    imag_i_3_d1 = ap_const_lv32_0;
}

void fft::thread_imag_i_3_we0() {
    imag_i_3_we0 = ap_const_logic_0;
}

void fft::thread_imag_i_3_we1() {
    imag_i_3_we1 = ap_const_logic_0;
}

void fft::thread_imag_i_4_address0() {
    imag_i_4_address0 = fft_bit_reverse_U0_imag_i_4_address0.read();
}

void fft::thread_imag_i_4_address1() {
    imag_i_4_address1 = fft_bit_reverse_U0_imag_i_4_address1.read();
}

void fft::thread_imag_i_4_ce0() {
    imag_i_4_ce0 = fft_bit_reverse_U0_imag_i_4_ce0.read();
}

void fft::thread_imag_i_4_ce1() {
    imag_i_4_ce1 = fft_bit_reverse_U0_imag_i_4_ce1.read();
}

void fft::thread_imag_i_4_d0() {
    imag_i_4_d0 = ap_const_lv32_0;
}

void fft::thread_imag_i_4_d1() {
    imag_i_4_d1 = ap_const_lv32_0;
}

void fft::thread_imag_i_4_we0() {
    imag_i_4_we0 = ap_const_logic_0;
}

void fft::thread_imag_i_4_we1() {
    imag_i_4_we1 = ap_const_logic_0;
}

void fft::thread_imag_i_5_address0() {
    imag_i_5_address0 = fft_bit_reverse_U0_imag_i_5_address0.read();
}

void fft::thread_imag_i_5_address1() {
    imag_i_5_address1 = fft_bit_reverse_U0_imag_i_5_address1.read();
}

void fft::thread_imag_i_5_ce0() {
    imag_i_5_ce0 = fft_bit_reverse_U0_imag_i_5_ce0.read();
}

void fft::thread_imag_i_5_ce1() {
    imag_i_5_ce1 = fft_bit_reverse_U0_imag_i_5_ce1.read();
}

void fft::thread_imag_i_5_d0() {
    imag_i_5_d0 = ap_const_lv32_0;
}

void fft::thread_imag_i_5_d1() {
    imag_i_5_d1 = ap_const_lv32_0;
}

void fft::thread_imag_i_5_we0() {
    imag_i_5_we0 = ap_const_logic_0;
}

void fft::thread_imag_i_5_we1() {
    imag_i_5_we1 = ap_const_logic_0;
}

void fft::thread_imag_i_6_address0() {
    imag_i_6_address0 = fft_bit_reverse_U0_imag_i_6_address0.read();
}

void fft::thread_imag_i_6_address1() {
    imag_i_6_address1 = fft_bit_reverse_U0_imag_i_6_address1.read();
}

void fft::thread_imag_i_6_ce0() {
    imag_i_6_ce0 = fft_bit_reverse_U0_imag_i_6_ce0.read();
}

void fft::thread_imag_i_6_ce1() {
    imag_i_6_ce1 = fft_bit_reverse_U0_imag_i_6_ce1.read();
}

void fft::thread_imag_i_6_d0() {
    imag_i_6_d0 = ap_const_lv32_0;
}

void fft::thread_imag_i_6_d1() {
    imag_i_6_d1 = ap_const_lv32_0;
}

void fft::thread_imag_i_6_we0() {
    imag_i_6_we0 = ap_const_logic_0;
}

}

