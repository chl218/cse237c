#include "fft.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void fft::thread_ap_clk_no_reset_() {
    ap_CS = ap_const_logic_0;
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_procdone_fft_bit_reverse_U0 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_hs_done.read())) {
            ap_reg_procdone_fft_bit_reverse_U0 = ap_const_logic_0;
        } else if (esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_ap_done.read())) {
            ap_reg_procdone_fft_bit_reverse_U0 = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_procdone_fft_fft_stage_first_U0 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_hs_done.read())) {
            ap_reg_procdone_fft_fft_stage_first_U0 = ap_const_logic_0;
        } else if (esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_ap_done.read())) {
            ap_reg_procdone_fft_fft_stage_first_U0 = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_procdone_fft_fft_stage_last_U0 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_hs_done.read())) {
            ap_reg_procdone_fft_fft_stage_last_U0 = ap_const_logic_0;
        } else if (esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_last_U0_ap_done.read())) {
            ap_reg_procdone_fft_fft_stage_last_U0 = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_procdone_fft_fft_stages10_U0 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_hs_done.read())) {
            ap_reg_procdone_fft_fft_stages10_U0 = ap_const_logic_0;
        } else if (esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_ap_done.read())) {
            ap_reg_procdone_fft_fft_stages10_U0 = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_procdone_fft_fft_stages11_U0 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_hs_done.read())) {
            ap_reg_procdone_fft_fft_stages11_U0 = ap_const_logic_0;
        } else if (esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_ap_done.read())) {
            ap_reg_procdone_fft_fft_stages11_U0 = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_procdone_fft_fft_stages12_U0 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_hs_done.read())) {
            ap_reg_procdone_fft_fft_stages12_U0 = ap_const_logic_0;
        } else if (esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_ap_done.read())) {
            ap_reg_procdone_fft_fft_stages12_U0 = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_procdone_fft_fft_stages13_U0 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_hs_done.read())) {
            ap_reg_procdone_fft_fft_stages13_U0 = ap_const_logic_0;
        } else if (esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_ap_done.read())) {
            ap_reg_procdone_fft_fft_stages13_U0 = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_procdone_fft_fft_stages14_U0 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_hs_done.read())) {
            ap_reg_procdone_fft_fft_stages14_U0 = ap_const_logic_0;
        } else if (esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_ap_done.read())) {
            ap_reg_procdone_fft_fft_stages14_U0 = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_procdone_fft_fft_stages15_U0 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_hs_done.read())) {
            ap_reg_procdone_fft_fft_stages15_U0 = ap_const_logic_0;
        } else if (esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_ap_done.read())) {
            ap_reg_procdone_fft_fft_stages15_U0 = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_procdone_fft_fft_stages16_U0 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_hs_done.read())) {
            ap_reg_procdone_fft_fft_stages16_U0 = ap_const_logic_0;
        } else if (esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_ap_done.read())) {
            ap_reg_procdone_fft_fft_stages16_U0 = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_procdone_fft_fft_stages17_U0 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_hs_done.read())) {
            ap_reg_procdone_fft_fft_stages17_U0 = ap_const_logic_0;
        } else if (esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_ap_done.read())) {
            ap_reg_procdone_fft_fft_stages17_U0 = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_bit_reverse_U0_imag_o_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_ap_continue.read()))) {
            ap_reg_ready_fft_bit_reverse_U0_imag_o_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_imag_o_pipo_status.read()))) {
            ap_reg_ready_fft_bit_reverse_U0_imag_o_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_bit_reverse_U0_real_o_0_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_ap_continue.read()))) {
            ap_reg_ready_fft_bit_reverse_U0_real_o_0_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_real_o_0_pipo_status.read()))) {
            ap_reg_ready_fft_bit_reverse_U0_real_o_0_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_bit_reverse_U0_real_o_1_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_ap_continue.read()))) {
            ap_reg_ready_fft_bit_reverse_U0_real_o_1_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_real_o_1_pipo_status.read()))) {
            ap_reg_ready_fft_bit_reverse_U0_real_o_1_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_bit_reverse_U0_real_o_2_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_ap_continue.read()))) {
            ap_reg_ready_fft_bit_reverse_U0_real_o_2_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_real_o_2_pipo_status.read()))) {
            ap_reg_ready_fft_bit_reverse_U0_real_o_2_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_bit_reverse_U0_real_o_3_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_ap_continue.read()))) {
            ap_reg_ready_fft_bit_reverse_U0_real_o_3_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_bit_reverse_U0_real_o_3_pipo_status.read()))) {
            ap_reg_ready_fft_bit_reverse_U0_real_o_3_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stage_first_U0_imag_o_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stage_first_U0_imag_o_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_imag_o_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stage_first_U0_imag_o_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stage_first_U0_real_o_0_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stage_first_U0_real_o_0_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_real_o_0_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stage_first_U0_real_o_0_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stage_first_U0_real_o_1_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stage_first_U0_real_o_1_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_real_o_1_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stage_first_U0_real_o_1_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stage_first_U0_real_o_2_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stage_first_U0_real_o_2_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_real_o_2_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stage_first_U0_real_o_2_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stage_first_U0_real_o_3_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stage_first_U0_real_o_3_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stage_first_U0_real_o_3_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stage_first_U0_real_o_3_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages10_U0_imag_o_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages10_U0_imag_o_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_imag_o_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages10_U0_imag_o_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages10_U0_real_o4_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages10_U0_real_o4_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_real_o4_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages10_U0_real_o4_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages10_U0_real_o5_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages10_U0_real_o5_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_real_o5_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages10_U0_real_o5_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages10_U0_real_o6_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages10_U0_real_o6_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_real_o6_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages10_U0_real_o6_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages10_U0_real_o_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages10_U0_real_o_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages10_U0_real_o_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages10_U0_real_o_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages11_U0_imag_o_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages11_U0_imag_o_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_imag_o_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages11_U0_imag_o_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages11_U0_real_o4_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages11_U0_real_o4_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_real_o4_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages11_U0_real_o4_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages11_U0_real_o5_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages11_U0_real_o5_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_real_o5_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages11_U0_real_o5_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages11_U0_real_o6_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages11_U0_real_o6_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_real_o6_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages11_U0_real_o6_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages11_U0_real_o_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages11_U0_real_o_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages11_U0_real_o_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages11_U0_real_o_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages12_U0_imag_o_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages12_U0_imag_o_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_imag_o_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages12_U0_imag_o_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages12_U0_real_o4_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages12_U0_real_o4_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_real_o4_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages12_U0_real_o4_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages12_U0_real_o5_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages12_U0_real_o5_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_real_o5_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages12_U0_real_o5_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages12_U0_real_o6_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages12_U0_real_o6_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_real_o6_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages12_U0_real_o6_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages12_U0_real_o_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages12_U0_real_o_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages12_U0_real_o_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages12_U0_real_o_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages13_U0_imag_o_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages13_U0_imag_o_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_imag_o_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages13_U0_imag_o_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages13_U0_real_o4_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages13_U0_real_o4_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_real_o4_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages13_U0_real_o4_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages13_U0_real_o5_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages13_U0_real_o5_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_real_o5_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages13_U0_real_o5_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages13_U0_real_o6_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages13_U0_real_o6_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_real_o6_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages13_U0_real_o6_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages13_U0_real_o_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages13_U0_real_o_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages13_U0_real_o_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages13_U0_real_o_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages14_U0_imag_o_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages14_U0_imag_o_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_imag_o_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages14_U0_imag_o_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages14_U0_real_o4_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages14_U0_real_o4_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_real_o4_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages14_U0_real_o4_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages14_U0_real_o5_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages14_U0_real_o5_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_real_o5_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages14_U0_real_o5_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages14_U0_real_o6_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages14_U0_real_o6_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_real_o6_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages14_U0_real_o6_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages14_U0_real_o_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages14_U0_real_o_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages14_U0_real_o_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages14_U0_real_o_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages15_U0_imag_o_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages15_U0_imag_o_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_imag_o_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages15_U0_imag_o_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages15_U0_real_o4_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages15_U0_real_o4_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_real_o4_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages15_U0_real_o4_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages15_U0_real_o5_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages15_U0_real_o5_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_real_o5_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages15_U0_real_o5_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages15_U0_real_o6_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages15_U0_real_o6_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_real_o6_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages15_U0_real_o6_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages15_U0_real_o_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages15_U0_real_o_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages15_U0_real_o_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages15_U0_real_o_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages16_U0_imag_o_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages16_U0_imag_o_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_imag_o_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages16_U0_imag_o_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages16_U0_real_o4_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages16_U0_real_o4_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_real_o4_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages16_U0_real_o4_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages16_U0_real_o5_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages16_U0_real_o5_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_real_o5_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages16_U0_real_o5_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages16_U0_real_o6_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages16_U0_real_o6_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_real_o6_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages16_U0_real_o6_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages16_U0_real_o_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages16_U0_real_o_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages16_U0_real_o_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages16_U0_real_o_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages17_U0_imag_o_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages17_U0_imag_o_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_imag_o_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages17_U0_imag_o_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages17_U0_real_o4_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages17_U0_real_o4_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_real_o4_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages17_U0_real_o4_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages17_U0_real_o5_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages17_U0_real_o5_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_real_o5_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages17_U0_real_o5_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages17_U0_real_o6_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages17_U0_real_o6_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_real_o6_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages17_U0_real_o6_pipo_status = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ready_fft_fft_stages17_U0_real_o_pipo_status = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_ap_done.read()) && 
             esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_ap_continue.read()))) {
            ap_reg_ready_fft_fft_stages17_U0_real_o_pipo_status = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_ap_done.read()) && 
                    esl_seteq<1,1,1>(ap_const_logic_1, fft_fft_stages17_U0_real_o_pipo_status.read()))) {
            ap_reg_ready_fft_fft_stages17_U0_real_o_pipo_status = ap_const_logic_1;
        }
    }
}

}

