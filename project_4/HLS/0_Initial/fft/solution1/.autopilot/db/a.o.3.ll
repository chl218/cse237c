; ModuleID = 'D:/Projects/vivado/project_4/HLS/0_Initial/fft/solution1/.autopilot/db/a.o.3.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a] ; [#uses=0 type=[1 x void ()*]*]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535] ; [#uses=0 type=[1 x i32]*]
@fft_str = internal unnamed_addr constant [4 x i8] c"fft\00" ; [#uses=1 type=[4 x i8]*]
@W_real = internal unnamed_addr constant [512 x float] [float 1.000000e+00, float 0x3FEFFFD820000000, float 0x3FEFFF62C0000000, float 0x3FEFFE9DA0000000, float 0x3FEFFD88C0000000, float 0x3FEFFC2440000000, float 0x3FEFFA7220000000, float 0x3FEFF87260000000, float 0x3FEFF620E0000000, float 0x3FEFF38400000000, float 0x3FEFF09520000000, float 0x3FEFED58C0000000, float 0x3FEFE9CCC0000000, float 0x3FEFE5F300000000, float 0x3FEFE1CBC0000000, float 0x3FEFDD52C0000000, float 0x3FEFD88E40000000, float 0x3FEFD37A00000000, float 0x3FEFCE1600000000, float 0x3FEFC86480000000, float 0x3FEFC26560000000, float 0x3FEFBC16A0000000, float 0x3FEFB57A40000000, float 0x3FEFAE8E20000000, float 0x3FEFA75680000000, float 0x3FEF9FCF40000000, float 0x3FEF97F840000000, float 0x3FEF8FD5C0000000, float 0x3FEF8765C0000000, float 0x3FEF7EA600000000, float 0x3FEF7598A0000000, float 0x3FEF6C3FC0000000, float 0x3FEF629740000000, float 0x3FEF58A320000000, float 0x3FEF4E5F80000000, float 0x3FEF43D040000000, float 0x3FEF38F360000000, float 0x3FEF2DC900000000, float 0x3FEF225320000000, float 0x3FEF168F80000000, float 0x3FEF0A7E80000000, float 0x3FEEFE21E0000000, float 0x3FEEF177A0000000, float 0x3FEEE48200000000, float 0x3FEED740C0000000, float 0x3FEEC9B200000000, float 0x3FEEBBD9C0000000, float 0x3FEEADB1E0000000, float 0x3FEE9F40A0000000, float 0x3FEE9083E0000000, float 0x3FEE817BA0000000, float 0x3FEE7227E0000000, float 0x3FEE628880000000, float 0x3FEE529FE0000000, float 0x3FEE4269A0000000, float 0x3FEE31EA00000000, float 0x3FEE2120E0000000, float 0x3FEE100C60000000, float 0x3FEDFEAE60000000, float 0x3FEDED0700000000, float 0x3FEDDB1420000000, float 0x3FEDC8D7E0000000, float 0x3FEDB65240000000, float 0x3FEDA38320000000, float 0x3FED906CC0000000, float 0x3FED7D0AE0000000, float 0x3FED6961C0000000, float 0x3FED556F40000000, float 0x3FED413560000000, float 0x3FED2CB200000000, float 0x3FED17E780000000, float 0x3FED02D5A0000000, float 0x3FECED7A60000000, float 0x3FECD7D9E0000000, float 0x3FECC1F000000000, float 0x3FECABC0E0000000, float 0x3FEC954A80000000, float 0x3FEC7E8EE0000000, float 0x3FEC678C00000000, float 0x3FEC5041C0000000, float 0x3FEC38B260000000, float 0x3FEC20DDC0000000, float 0x3FEC08C400000000, float 0x3FEBF064E0000000, float 0x3FEBD7C0C0000000, float 0x3FEBBED740000000, float 0x3FEBA5AAC0000000, float 0x3FEB8C3900000000, float 0x3FEB728420000000, float 0x3FEB588A00000000, float 0x3FEB3E4CE0000000, float 0x3FEB23CC80000000, float 0x3FEB090B40000000, float 0x3FEAEE04C0000000, float 0x3FEAD2BD40000000, float 0x3FEAB732A0000000, float 0x3FEA9B6700000000, float 0x3FEA7F5840000000, float 0x3FEA630880000000, float 0x3FEA4677C0000000, float 0x3FEA29A800000000, float 0x3FEA0C9540000000, float 0x3FE9EF4380000000, float 0x3FE9D1B2E0000000, float 0x3FE9B3E140000000, float 0x3FE995CEA0000000, float 0x3FE9777F20000000, float 0x3FE958F0C0000000, float 0x3FE93A2160000000, float 0x3FE91B1740000000, float 0x3FE8FBCC20000000, float 0x3FE8DC4440000000, float 0x3FE8BC7F80000000, float 0x3FE89C7DE0000000, float 0x3FE87C3F80000000, float 0x3FE85BC440000000, float 0x3FE83B0E60000000, float 0x3FE81A1BA0000000, float 0x3FE7F8EC00000000, float 0x3FE7D783E0000000, float 0x3FE7B5DEE0000000, float 0x3FE7940140000000, float 0x3FE771E6C0000000, float 0x3FE74F93C0000000, float 0x3FE72D0800000000, float 0x3FE70A41A0000000, float 0x3FE6E744C0000000, float 0x3FE6C40D00000000, float 0x3FE6A09EE0000000, float 0x3FE67CF800000000, float 0x3FE65918A0000000, float 0x3FE63502A0000000, float 0x3FE610B840000000, float 0x3FE5EC3540000000, float 0x3FE5C77BC0000000, float 0x3FE5A28DC0000000, float 0x3FE57D6940000000, float 0x3FE5581060000000, float 0x3FE5328300000000, float 0x3FE50CC140000000, float 0x3FE4E6CB20000000, float 0x3FE4C0A060000000, float 0x3FE49A45A0000000, float 0x3FE473B420000000, float 0x3FE44CF280000000, float 0x3FE425FEA0000000, float 0x3FE3FED840000000, float 0x3FE3D781C0000000, float 0x3FE3AFFB00000000, float 0x3FE38841E0000000, float 0x3FE36058A0000000, float 0x3FE3383F00000000, float 0x3FE30FF760000000, float 0x3FE2E78180000000, float 0x3FE2BEDB80000000, float 0x3FE2960740000000, float 0x3FE26D04E0000000, float 0x3FE243D680000000, float 0x3FE21A7A00000000, float 0x3FE1F0F160000000, float 0x3FE1C73AC0000000, float 0x3FE19D5A20000000, float 0x3FE1734D60000000, float 0x3FE14914C0000000, float 0x3FE11EB420000000, float 0x3FE0F42780000000, float 0x3FE0C97100000000, float 0x3FE09E9080000000, float 0x3FE0738820000000, float 0x3FE04855E0000000, float 0x3FE01CFBC0000000, float 0x3FDFE2F7C0000000, float 0x3FDF8BA400000000, float 0x3FDF3404E0000000, float 0x3FDEDC1A40000000, float 0x3FDE83E000000000, float 0x3FDE2B5E60000000, float 0x3FDDD28D00000000, float 0x3FDD7978A0000000, float 0x3FDD2018A0000000, float 0x3FDCC66D40000000, float 0x3FDC6C7EA0000000, float 0x3FDC1248E0000000, float 0x3FDBB7CFE0000000, float 0x3FDB5D0FA0000000, float 0x3FDB020C40000000, float 0x3FDAA6CA00000000, float 0x3FDA4B4080000000, float 0x3FD9EF7800000000, float 0x3FD9937080000000, float 0x3FD9372A40000000, float 0x3FD8DAA500000000, float 0x3FD87DE0E0000000, float 0x3FD820E200000000, float 0x3FD7C3A860000000, float 0x3FD7663420000000, float 0x3FD7088500000000, float 0x3FD6AA9B40000000, float 0x3FD64C7F40000000, float 0x3FD5EE2860000000, float 0x3FD58F9B20000000, float 0x3FD530D740000000, float 0x3FD4D1E100000000, float 0x3FD472B880000000, float 0x3FD4135DA0000000, float 0x3FD3B3D080000000, float 0x3FD3541100000000, float 0x3FD2F42360000000, float 0x3FD2940780000000, float 0x3FD233B960000000, float 0x3FD1D34580000000, float 0x3FD1729F60000000, float 0x3FD111D360000000, float 0x3FD0B0D960000000, float 0x3FD04FB980000000, float 0x3FCFDCDF60000000, float 0x3FCF19F800000000, float 0x3FCE56CD60000000, float 0x3FCD934F00000000, float 0x3FCCCF8D80000000, float 0x3FCC0B8060000000, float 0x3FCB473040000000, float 0x3FCA829D00000000, float 0x3FC9BDCF00000000, float 0x3FC8F8B580000000, float 0x3FC83369C0000000, float 0x3FC76DDAC0000000, float 0x3FC6A81120000000, float 0x3FC5E21540000000, float 0x3FC51BDE80000000, float 0x3FC4557580000000, float 0x3FC38EDA20000000, float 0x3FC2C80C80000000, float 0x3FC20114E0000000, float 0x3FC139F340000000, float 0x3FC0729F60000000, float 0x3FBF5653C0000000, float 0x3FBDC71500000000, float 0x3FBC378240000000, float 0x3FBAA7BD40000000, float 0x3FB917A460000000, float 0x3FB7875920000000, float 0x3FB5F6CAC0000000, float 0x3FB4660A20000000, float 0x3FB2D527E0000000, float 0x3FB1440280000000, float 0x3FAF657760000000, float 0x3FAC4284E0000000, float 0x3FA91F70E0000000, float 0x3FA5FBF840000000, float 0x3FA2D85E00000000, float 0x3F9F694460000000, float 0x3F992146A0000000, float 0x3F92D948E0000000, float 0x3F892189C0000000, float 0x3F79221000000000, float -0.000000e+00, float 0xBF79221000000000, float 0xBF89221000000000, float 0xBF92D948E0000000, float 0xBF992146A0000000, float 0xBF9F694460000000, float 0xBFA2D85E00000000, float 0xBFA5FBF840000000, float 0xBFA91F70E0000000, float 0xBFAC4284E0000000, float 0xBFAF657760000000, float 0xBFB1440280000000, float 0xBFB2D527E0000000, float 0xBFB4660A20000000, float 0xBFB5F6CAC0000000, float 0xBFB7875920000000, float 0xBFB917A460000000, float 0xBFBAA7BD40000000, float 0xBFBC378240000000, float 0xBFBDC71500000000, float 0xBFBF5653C0000000, float 0xBFC0729F60000000, float 0xBFC139F340000000, float 0xBFC20114E0000000, float 0xBFC2C814E0000000, float 0xBFC38EDA20000000, float 0xBFC4557580000000, float 0xBFC51BDE80000000, float 0xBFC5E21540000000, float 0xBFC6A81120000000, float 0xBFC76DDAC0000000, float 0xBFC83369C0000000, float 0xBFC8F8B580000000, float 0xBFC9BDCF00000000, float 0xBFCA829D00000000, float 0xBFCB473040000000, float 0xBFCC0B8060000000, float 0xBFCCCF8D80000000, float 0xBFCD934F00000000, float 0xBFCE56CD60000000, float 0xBFCF19F800000000, float 0xBFCFDCDF60000000, float 0xBFD04FB980000000, float 0xBFD0B0D960000000, float 0xBFD111D360000000, float 0xBFD1729F60000000, float 0xBFD1D34580000000, float 0xBFD233BDA0000000, float 0xBFD2940780000000, float 0xBFD2F42360000000, float 0xBFD3541100000000, float 0xBFD3B3D080000000, float 0xBFD4135DA0000000, float 0xBFD472B880000000, float 0xBFD4D1E100000000, float 0xBFD530D740000000, float 0xBFD58F9B20000000, float 0xBFD5EE2860000000, float 0xBFD64C7F40000000, float 0xBFD6AA9F80000000, float 0xBFD7088500000000, float 0xBFD7663420000000, float 0xBFD7C3A860000000, float 0xBFD820E200000000, float 0xBFD87DE0E0000000, float 0xBFD8DAA500000000, float 0xBFD9372A40000000, float 0xBFD9937080000000, float 0xBFD9EF7800000000, float 0xBFDA4B4080000000, float 0xBFDAA6CA00000000, float 0xBFDB020C40000000, float 0xBFDB5D0FA0000000, float 0xBFDBB7CFE0000000, float 0xBFDC1248E0000000, float 0xBFDC6C7EA0000000, float 0xBFDCC66D40000000, float 0xBFDD2018A0000000, float 0xBFDD7978A0000000, float 0xBFDDD29140000000, float 0xBFDE2B5E60000000, float 0xBFDE83E000000000, float 0xBFDEDC1A40000000, float 0xBFDF3404E0000000, float 0xBFDF8BA400000000, float 0xBFDFE2F7C0000000, float 0xBFE01CFBC0000000, float 0xBFE04855E0000000, float 0xBFE0738820000000, float 0xBFE09E9080000000, float 0xBFE0C97100000000, float 0xBFE0F42780000000, float 0xBFE11EB420000000, float 0xBFE14916C0000000, float 0xBFE1734D60000000, float 0xBFE19D5A20000000, float 0xBFE1C73AC0000000, float 0xBFE1F0F160000000, float 0xBFE21A7A00000000, float 0xBFE243D680000000, float 0xBFE26D04E0000000, float 0xBFE2960740000000, float 0xBFE2BEDB80000000, float 0xBFE2E78180000000, float 0xBFE30FF760000000, float 0xBFE3384120000000, float 0xBFE36058A0000000, float 0xBFE38841E0000000, float 0xBFE3AFFB00000000, float 0xBFE3D781C0000000, float 0xBFE3FEDA60000000, float 0xBFE425FEA0000000, float 0xBFE44CF280000000, float 0xBFE473B640000000, float 0xBFE49A45A0000000, float 0xBFE4C0A060000000, float 0xBFE4E6CB20000000, float 0xBFE50CC140000000, float 0xBFE5328300000000, float 0xBFE5581060000000, float 0xBFE57D6940000000, float 0xBFE5A28DC0000000, float 0xBFE5C77BC0000000, float 0xBFE5EC3540000000, float 0xBFE610B840000000, float 0xBFE63504C0000000, float 0xBFE65918A0000000, float 0xBFE67CF800000000, float 0xBFE6A09EE0000000, float 0xBFE6C40D00000000, float 0xBFE6E744C0000000, float 0xBFE70A43C0000000, float 0xBFE72D0800000000, float 0xBFE74F93C0000000, float 0xBFE771E6C0000000, float 0xBFE7940140000000, float 0xBFE7B5DEE0000000, float 0xBFE7D783E0000000, float 0xBFE7F8EC00000000, float 0xBFE81A1BA0000000, float 0xBFE83B0E60000000, float 0xBFE85BC440000000, float 0xBFE87C3F80000000, float 0xBFE89C7DE0000000, float 0xBFE8BC7F80000000, float 0xBFE8DC4440000000, float 0xBFE8FBCC20000000, float 0xBFE91B1740000000, float 0xBFE93A2160000000, float 0xBFE958F0C0000000, float 0xBFE9777F20000000, float 0xBFE995CEA0000000, float 0xBFE9B3E140000000, float 0xBFE9D1B2E0000000, float 0xBFE9EF4380000000, float 0xBFEA0C9540000000, float 0xBFEA29A800000000, float 0xBFEA4679C0000000, float 0xBFEA630880000000, float 0xBFEA7F5840000000, float 0xBFEA9B6700000000, float 0xBFEAB732A0000000, float 0xBFEAD2BD40000000, float 0xBFEAEE04C0000000, float 0xBFEB090B40000000, float 0xBFEB23CC80000000, float 0xBFEB3E4CE0000000, float 0xBFEB588A00000000, float 0xBFEB728420000000, float 0xBFEB8C3900000000, float 0xBFEBA5AAC0000000, float 0xBFEBBED740000000, float 0xBFEBD7C0C0000000, float 0xBFEBF064E0000000, float 0xBFEC08C400000000, float 0xBFEC20DDC0000000, float 0xBFEC38B260000000, float 0xBFEC5041C0000000, float 0xBFEC678C00000000, float 0xBFEC7E8EE0000000, float 0xBFEC954A80000000, float 0xBFECABC0E0000000, float 0xBFECC1F000000000, float 0xBFECD7D9E0000000, float 0xBFECED7A60000000, float 0xBFED02D5A0000000, float 0xBFED17E780000000, float 0xBFED2CB200000000, float 0xBFED413560000000, float 0xBFED556F40000000, float 0xBFED6961C0000000, float 0xBFED7D0AE0000000, float 0xBFED906CC0000000, float 0xBFEDA38320000000, float 0xBFEDB65240000000, float 0xBFEDC8D7E0000000, float 0xBFEDDB1420000000, float 0xBFEDED0700000000, float 0xBFEDFEAE60000000, float 0xBFEE100C60000000, float 0xBFEE2120E0000000, float 0xBFEE31EC00000000, float 0xBFEE4269A0000000, float 0xBFEE529FE0000000, float 0xBFEE628880000000, float 0xBFEE7227E0000000, float 0xBFEE817BA0000000, float 0xBFEE9083E0000000, float 0xBFEE9F40A0000000, float 0xBFEEADB400000000, float 0xBFEEBBD9C0000000, float 0xBFEEC9B200000000, float 0xBFEED740C0000000, float 0xBFEEE48200000000, float 0xBFEEF177A0000000, float 0xBFEEFE21E0000000, float 0xBFEF0A7E80000000, float 0xBFEF168F80000000, float 0xBFEF225320000000, float 0xBFEF2DC900000000, float 0xBFEF38F360000000, float 0xBFEF43D040000000, float 0xBFEF4E5F80000000, float 0xBFEF58A320000000, float 0xBFEF629740000000, float 0xBFEF6C3FC0000000, float 0xBFEF7598A0000000, float 0xBFEF7EA600000000, float 0xBFEF8765C0000000, float 0xBFEF8FD5C0000000, float 0xBFEF97F840000000, float 0xBFEF9FCF40000000, float 0xBFEFA75680000000, float 0xBFEFAE8E20000000, float 0xBFEFB57A40000000, float 0xBFEFBC16A0000000, float 0xBFEFC26560000000, float 0xBFEFC86480000000, float 0xBFEFCE1600000000, float 0xBFEFD37A00000000, float 0xBFEFD88E40000000, float 0xBFEFDD52C0000000, float 0xBFEFE1CBC0000000, float 0xBFEFE5F300000000, float 0xBFEFE9CCC0000000, float 0xBFEFED58C0000000, float 0xBFEFF09520000000, float 0xBFEFF38400000000, float 0xBFEFF620E0000000, float 0xBFEFF87260000000, float 0xBFEFFA7220000000, float 0xBFEFFC2440000000, float 0xBFEFFD88C0000000, float 0xBFEFFE9DA0000000, float 0xBFEFFF62C0000000, float 0xBFEFFFD820000000], align 16 ; [#uses=1 type=[512 x float]*]
@W_imag = internal unnamed_addr constant [512 x float] [float -0.000000e+00, float 0xBF79221000000000, float 0xBF89221000000000, float 0xBF92D948E0000000, float 0xBF992146A0000000, float 0xBF9F694460000000, float 0xBFA2D85E00000000, float 0xBFA5FBF840000000, float 0xBFA91F70E0000000, float 0xBFAC4284E0000000, float 0xBFAF657760000000, float 0xBFB1440280000000, float 0xBFB2D527E0000000, float 0xBFB4660A20000000, float 0xBFB5F6CAC0000000, float 0xBFB7875920000000, float 0xBFB917A460000000, float 0xBFBAA7BD40000000, float 0xBFBC378240000000, float 0xBFBDC71500000000, float 0xBFBF5653C0000000, float 0xBFC0729F60000000, float 0xBFC139F340000000, float 0xBFC20114E0000000, float 0xBFC2C80C80000000, float 0xBFC38EDA20000000, float 0xBFC4557580000000, float 0xBFC51BDE80000000, float 0xBFC5E21540000000, float 0xBFC6A81120000000, float 0xBFC76DDAC0000000, float 0xBFC83369C0000000, float 0xBFC8F8B580000000, float 0xBFC9BDCF00000000, float 0xBFCA829D00000000, float 0xBFCB473040000000, float 0xBFCC0B8060000000, float 0xBFCCCF8D80000000, float 0xBFCD934F00000000, float 0xBFCE56CD60000000, float 0xBFCF19F800000000, float 0xBFCFDCDF60000000, float 0xBFD04FB980000000, float 0xBFD0B0D960000000, float 0xBFD111D360000000, float 0xBFD1729F60000000, float 0xBFD1D34580000000, float 0xBFD233BDA0000000, float 0xBFD2940780000000, float 0xBFD2F42360000000, float 0xBFD3541100000000, float 0xBFD3B3D080000000, float 0xBFD4135DA0000000, float 0xBFD472B880000000, float 0xBFD4D1E100000000, float 0xBFD530D740000000, float 0xBFD58F9B20000000, float 0xBFD5EE2860000000, float 0xBFD64C7F40000000, float 0xBFD6AA9F80000000, float 0xBFD7088500000000, float 0xBFD7663420000000, float 0xBFD7C3A860000000, float 0xBFD820E200000000, float 0xBFD87DE0E0000000, float 0xBFD8DAA500000000, float 0xBFD9372A40000000, float 0xBFD9937080000000, float 0xBFD9EF7800000000, float 0xBFDA4B4080000000, float 0xBFDAA6CA00000000, float 0xBFDB020C40000000, float 0xBFDB5D0FA0000000, float 0xBFDBB7CFE0000000, float 0xBFDC1248E0000000, float 0xBFDC6C7EA0000000, float 0xBFDCC66D40000000, float 0xBFDD2018A0000000, float 0xBFDD7978A0000000, float 0xBFDDD29140000000, float 0xBFDE2B5E60000000, float 0xBFDE83E000000000, float 0xBFDEDC1A40000000, float 0xBFDF3404E0000000, float 0xBFDF8BA400000000, float 0xBFDFE2F7C0000000, float 0xBFE01CFBC0000000, float 0xBFE04855E0000000, float 0xBFE0738820000000, float 0xBFE09E9080000000, float 0xBFE0C97100000000, float 0xBFE0F42780000000, float 0xBFE11EB420000000, float 0xBFE14916C0000000, float 0xBFE1734D60000000, float 0xBFE19D5A20000000, float 0xBFE1C73AC0000000, float 0xBFE1F0F160000000, float 0xBFE21A7A00000000, float 0xBFE243D680000000, float 0xBFE26D04E0000000, float 0xBFE2960740000000, float 0xBFE2BEDB80000000, float 0xBFE2E78180000000, float 0xBFE30FF760000000, float 0xBFE3384120000000, float 0xBFE36058A0000000, float 0xBFE38841E0000000, float 0xBFE3AFFB00000000, float 0xBFE3D781C0000000, float 0xBFE3FEDA60000000, float 0xBFE425FEA0000000, float 0xBFE44CF280000000, float 0xBFE473B420000000, float 0xBFE49A45A0000000, float 0xBFE4C0A060000000, float 0xBFE4E6CB20000000, float 0xBFE50CC140000000, float 0xBFE5328300000000, float 0xBFE5581060000000, float 0xBFE57D6940000000, float 0xBFE5A28DC0000000, float 0xBFE5C77BC0000000, float 0xBFE5EC3540000000, float 0xBFE610B840000000, float 0xBFE63502A0000000, float 0xBFE65918A0000000, float 0xBFE67CF800000000, float 0xBFE6A09EE0000000, float 0xBFE6C40D00000000, float 0xBFE6E744C0000000, float 0xBFE70A43C0000000, float 0xBFE72D0800000000, float 0xBFE74F93C0000000, float 0xBFE771E6C0000000, float 0xBFE7940140000000, float 0xBFE7B5DEE0000000, float 0xBFE7D783E0000000, float 0xBFE7F8EC00000000, float 0xBFE81A1BA0000000, float 0xBFE83B0E60000000, float 0xBFE85BC440000000, float 0xBFE87C3F80000000, float 0xBFE89C7DE0000000, float 0xBFE8BC7F80000000, float 0xBFE8DC4440000000, float 0xBFE8FBCC20000000, float 0xBFE91B1740000000, float 0xBFE93A2160000000, float 0xBFE958F0C0000000, float 0xBFE9777F20000000, float 0xBFE995CEA0000000, float 0xBFE9B3E140000000, float 0xBFE9D1B2E0000000, float 0xBFE9EF4380000000, float 0xBFEA0C9540000000, float 0xBFEA29A800000000, float 0xBFEA4679C0000000, float 0xBFEA630880000000, float 0xBFEA7F5840000000, float 0xBFEA9B6700000000, float 0xBFEAB732A0000000, float 0xBFEAD2BD40000000, float 0xBFEAEE04C0000000, float 0xBFEB090B40000000, float 0xBFEB23CC80000000, float 0xBFEB3E4CE0000000, float 0xBFEB588A00000000, float 0xBFEB728420000000, float 0xBFEB8C3900000000, float 0xBFEBA5AAC0000000, float 0xBFEBBED740000000, float 0xBFEBD7C0C0000000, float 0xBFEBF064E0000000, float 0xBFEC08C400000000, float 0xBFEC20DDC0000000, float 0xBFEC38B260000000, float 0xBFEC5041C0000000, float 0xBFEC678C00000000, float 0xBFEC7E8EE0000000, float 0xBFEC954A80000000, float 0xBFECABC0E0000000, float 0xBFECC1F000000000, float 0xBFECD7D9E0000000, float 0xBFECED7A60000000, float 0xBFED02D5A0000000, float 0xBFED17E780000000, float 0xBFED2CB200000000, float 0xBFED413560000000, float 0xBFED556F40000000, float 0xBFED6961C0000000, float 0xBFED7D0AE0000000, float 0xBFED906CC0000000, float 0xBFEDA38320000000, float 0xBFEDB65240000000, float 0xBFEDC8D7E0000000, float 0xBFEDDB1420000000, float 0xBFEDED0700000000, float 0xBFEDFEAE60000000, float 0xBFEE100C60000000, float 0xBFEE2120E0000000, float 0xBFEE31EA00000000, float 0xBFEE4269A0000000, float 0xBFEE529FE0000000, float 0xBFEE628880000000, float 0xBFEE7227E0000000, float 0xBFEE817BA0000000, float 0xBFEE9083E0000000, float 0xBFEE9F40A0000000, float 0xBFEEADB1E0000000, float 0xBFEEBBD9C0000000, float 0xBFEEC9B200000000, float 0xBFEED740C0000000, float 0xBFEEE48200000000, float 0xBFEEF177A0000000, float 0xBFEEFE21E0000000, float 0xBFEF0A7E80000000, float 0xBFEF168F80000000, float 0xBFEF225320000000, float 0xBFEF2DC900000000, float 0xBFEF38F360000000, float 0xBFEF43D040000000, float 0xBFEF4E5F80000000, float 0xBFEF58A320000000, float 0xBFEF629740000000, float 0xBFEF6C3FC0000000, float 0xBFEF7598A0000000, float 0xBFEF7EA600000000, float 0xBFEF8765C0000000, float 0xBFEF8FD5C0000000, float 0xBFEF97F840000000, float 0xBFEF9FCF40000000, float 0xBFEFA75680000000, float 0xBFEFAE8E20000000, float 0xBFEFB57A40000000, float 0xBFEFBC16A0000000, float 0xBFEFC26560000000, float 0xBFEFC86480000000, float 0xBFEFCE1600000000, float 0xBFEFD37A00000000, float 0xBFEFD88E40000000, float 0xBFEFDD52C0000000, float 0xBFEFE1CBC0000000, float 0xBFEFE5F300000000, float 0xBFEFE9CCC0000000, float 0xBFEFED58C0000000, float 0xBFEFF09520000000, float 0xBFEFF38400000000, float 0xBFEFF620E0000000, float 0xBFEFF87260000000, float 0xBFEFFA7220000000, float 0xBFEFFC2440000000, float 0xBFEFFD88C0000000, float 0xBFEFFE9DA0000000, float 0xBFEFFF62C0000000, float 0xBFEFFFD820000000, float -1.000000e+00, float 0xBFEFFFD820000000, float 0xBFEFFF62C0000000, float 0xBFEFFE9DA0000000, float 0xBFEFFD88C0000000, float 0xBFEFFC2440000000, float 0xBFEFFA7220000000, float 0xBFEFF87260000000, float 0xBFEFF620E0000000, float 0xBFEFF38400000000, float 0xBFEFF09520000000, float 0xBFEFED58C0000000, float 0xBFEFE9CCC0000000, float 0xBFEFE5F300000000, float 0xBFEFE1CBC0000000, float 0xBFEFDD52C0000000, float 0xBFEFD88E40000000, float 0xBFEFD37A00000000, float 0xBFEFCE1600000000, float 0xBFEFC86480000000, float 0xBFEFC26560000000, float 0xBFEFBC16A0000000, float 0xBFEFB57A40000000, float 0xBFEFAE8E20000000, float 0xBFEFA75680000000, float 0xBFEF9FCF40000000, float 0xBFEF97F840000000, float 0xBFEF8FD5C0000000, float 0xBFEF8765C0000000, float 0xBFEF7EA600000000, float 0xBFEF7598A0000000, float 0xBFEF6C3FC0000000, float 0xBFEF629740000000, float 0xBFEF58A320000000, float 0xBFEF4E5F80000000, float 0xBFEF43D040000000, float 0xBFEF38F360000000, float 0xBFEF2DC900000000, float 0xBFEF225320000000, float 0xBFEF168F80000000, float 0xBFEF0A7E80000000, float 0xBFEEFE21E0000000, float 0xBFEEF177A0000000, float 0xBFEEE48200000000, float 0xBFEED740C0000000, float 0xBFEEC9B200000000, float 0xBFEEBBD9C0000000, float 0xBFEEADB1E0000000, float 0xBFEE9F40A0000000, float 0xBFEE9083E0000000, float 0xBFEE817BA0000000, float 0xBFEE7227E0000000, float 0xBFEE628880000000, float 0xBFEE529FE0000000, float 0xBFEE4269A0000000, float 0xBFEE31EA00000000, float 0xBFEE2120E0000000, float 0xBFEE100C60000000, float 0xBFEDFEAE60000000, float 0xBFEDED04E0000000, float 0xBFEDDB1420000000, float 0xBFEDC8D7E0000000, float 0xBFEDB65240000000, float 0xBFEDA38320000000, float 0xBFED906CC0000000, float 0xBFED7D0AE0000000, float 0xBFED6961C0000000, float 0xBFED556F40000000, float 0xBFED413560000000, float 0xBFED2CB200000000, float 0xBFED17E780000000, float 0xBFED02D5A0000000, float 0xBFECED7A60000000, float 0xBFECD7D9E0000000, float 0xBFECC1F000000000, float 0xBFECABC0E0000000, float 0xBFEC954A80000000, float 0xBFEC7E8EE0000000, float 0xBFEC678C00000000, float 0xBFEC5041C0000000, float 0xBFEC38B260000000, float 0xBFEC20DDC0000000, float 0xBFEC08C400000000, float 0xBFEBF064E0000000, float 0xBFEBD7C0C0000000, float 0xBFEBBED740000000, float 0xBFEBA5AAC0000000, float 0xBFEB8C3900000000, float 0xBFEB728420000000, float 0xBFEB588A00000000, float 0xBFEB3E4CE0000000, float 0xBFEB23CC80000000, float 0xBFEB090B40000000, float 0xBFEAEE04C0000000, float 0xBFEAD2BD40000000, float 0xBFEAB732A0000000, float 0xBFEA9B6700000000, float 0xBFEA7F5840000000, float 0xBFEA630880000000, float 0xBFEA4677C0000000, float 0xBFEA29A800000000, float 0xBFEA0C9540000000, float 0xBFE9EF4380000000, float 0xBFE9D1B2E0000000, float 0xBFE9B3E140000000, float 0xBFE995CEA0000000, float 0xBFE9777F20000000, float 0xBFE958F0C0000000, float 0xBFE93A2160000000, float 0xBFE91B1740000000, float 0xBFE8FBCC20000000, float 0xBFE8DC4440000000, float 0xBFE8BC7F80000000, float 0xBFE89C7DE0000000, float 0xBFE87C3F80000000, float 0xBFE85BC440000000, float 0xBFE83B0E60000000, float 0xBFE81A1BA0000000, float 0xBFE7F8EC00000000, float 0xBFE7D783E0000000, float 0xBFE7B5DEE0000000, float 0xBFE7940140000000, float 0xBFE771E6C0000000, float 0xBFE74F93C0000000, float 0xBFE72D0800000000, float 0xBFE70A41A0000000, float 0xBFE6E744C0000000, float 0xBFE6C40D00000000, float 0xBFE6A09EE0000000, float 0xBFE67CF800000000, float 0xBFE65918A0000000, float 0xBFE63502A0000000, float 0xBFE610B840000000, float 0xBFE5EC3540000000, float 0xBFE5C77BC0000000, float 0xBFE5A28DC0000000, float 0xBFE57D6940000000, float 0xBFE5581060000000, float 0xBFE5328300000000, float 0xBFE50CC140000000, float 0xBFE4E6CB20000000, float 0xBFE4C0A060000000, float 0xBFE49A4380000000, float 0xBFE473B420000000, float 0xBFE44CF280000000, float 0xBFE425FEA0000000, float 0xBFE3FED840000000, float 0xBFE3D781C0000000, float 0xBFE3AFFB00000000, float 0xBFE38841E0000000, float 0xBFE36058A0000000, float 0xBFE3383F00000000, float 0xBFE30FF760000000, float 0xBFE2E78180000000, float 0xBFE2BEDB80000000, float 0xBFE2960740000000, float 0xBFE26D04E0000000, float 0xBFE243D680000000, float 0xBFE21A7A00000000, float 0xBFE1F0EF60000000, float 0xBFE1C73AC0000000, float 0xBFE19D5A20000000, float 0xBFE1734D60000000, float 0xBFE14914C0000000, float 0xBFE11EB420000000, float 0xBFE0F42780000000, float 0xBFE0C97100000000, float 0xBFE09E9080000000, float 0xBFE0738820000000, float 0xBFE04855E0000000, float 0xBFE01CFBC0000000, float 0xBFDFE2F7C0000000, float 0xBFDF8BA400000000, float 0xBFDF3404E0000000, float 0xBFDEDC1A40000000, float 0xBFDE83E000000000, float 0xBFDE2B5E60000000, float 0xBFDDD28D00000000, float 0xBFDD7978A0000000, float 0xBFDD2018A0000000, float 0xBFDCC66D40000000, float 0xBFDC6C7EA0000000, float 0xBFDC1248E0000000, float 0xBFDBB7CFE0000000, float 0xBFDB5D0FA0000000, float 0xBFDB020C40000000, float 0xBFDAA6C5E0000000, float 0xBFDA4B4080000000, float 0xBFD9EF7800000000, float 0xBFD9937080000000, float 0xBFD9372A40000000, float 0xBFD8DAA500000000, float 0xBFD87DE0E0000000, float 0xBFD820E200000000, float 0xBFD7C3A860000000, float 0xBFD7663420000000, float 0xBFD7088500000000, float 0xBFD6AA9B40000000, float 0xBFD64C7F40000000, float 0xBFD5EE2860000000, float 0xBFD58F9B20000000, float 0xBFD530D740000000, float 0xBFD4D1E100000000, float 0xBFD472B880000000, float 0xBFD4135DA0000000, float 0xBFD3B3D080000000, float 0xBFD3541100000000, float 0xBFD2F42360000000, float 0xBFD2940780000000, float 0xBFD233B960000000, float 0xBFD1D34580000000, float 0xBFD1729F60000000, float 0xBFD111D360000000, float 0xBFD0B0D960000000, float 0xBFD04FB980000000, float 0xBFCFDCDF60000000, float 0xBFCF19F800000000, float 0xBFCE56CD60000000, float 0xBFCD934F00000000, float 0xBFCCCF8D80000000, float 0xBFCC0B8060000000, float 0xBFCB473040000000, float 0xBFCA829D00000000, float 0xBFC9BDCF00000000, float 0xBFC8F8B580000000, float 0xBFC83369C0000000, float 0xBFC76DDAC0000000, float 0xBFC6A81120000000, float 0xBFC5E21540000000, float 0xBFC51BDE80000000, float 0xBFC4557580000000, float 0xBFC38EDA20000000, float 0xBFC2C80C80000000, float 0xBFC20114E0000000, float 0xBFC139F340000000, float 0xBFC0729F60000000, float 0xBFBF5653C0000000, float 0xBFBDC71500000000, float 0xBFBC378240000000, float 0xBFBAA7BD40000000, float 0xBFB917A460000000, float 0xBFB7875920000000, float 0xBFB5F6CAC0000000, float 0xBFB4660A20000000, float 0xBFB2D51720000000, float 0xBFB1440280000000, float 0xBFAF657760000000, float 0xBFAC4284E0000000, float 0xBFA91F70E0000000, float 0xBFA5FBF840000000, float 0xBFA2D85E00000000, float 0xBF9F694460000000, float 0xBF992146A0000000, float 0xBF92D948E0000000, float 0xBF892189C0000000, float 0xBF79221000000000], align 16 ; [#uses=1 type=[512 x float]*]
@p_str4 = private unnamed_addr constant [7 x i8] c"DFTpts\00", align 1 ; [#uses=3 type=[7 x i8]*]
@p_str3 = private unnamed_addr constant [10 x i8] c"butterfly\00", align 1 ; [#uses=3 type=[10 x i8]*]
@p_str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1 ; [#uses=1 type=[1 x i8]*]
@p_str = private unnamed_addr constant [7 x i8] c"stages\00", align 1 ; [#uses=3 type=[7 x i8]*]

; [#uses=2]
declare i32 @llvm.part.select.i32(i32, i32, i32) nounwind readnone

; [#uses=1]
declare i11 @llvm.part.select.i11(i11, i32, i32) nounwind readnone

; [#uses=1]
declare i10 @llvm.part.select.i10(i10, i32, i32) nounwind readnone

; [#uses=24]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=0]
define void @fft([1024 x float]* %X_R, [1024 x float]* %X_I) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap([1024 x float]* %X_R) nounwind, !map !7
  call void (...)* @_ssdm_op_SpecBitsMap([1024 x float]* %X_I) nounwind, !map !13
  call void (...)* @_ssdm_op_SpecTopModule([4 x i8]* @fft_str) nounwind
  call void @llvm.dbg.value(metadata !{[1024 x float]* %X_R}, i64 0, metadata !17), !dbg !30 ; [debug line = 49:16] [debug variable = X_R]
  call void @llvm.dbg.value(metadata !{[1024 x float]* %X_I}, i64 0, metadata !31), !dbg !32 ; [debug line = 49:51] [debug variable = X_I]
  call fastcc void @fft_bit_reverse([1024 x float]* %X_R, [1024 x float]* %X_I) nounwind, !dbg !33 ; [debug line = 68:2]
  br label %1, !dbg !35                           ; [debug line = 76:13]

; <label>:1                                       ; preds = %8, %0
  %step = phi i32 [ 512, %0 ], [ %step_1, %8 ]    ; [#uses=4 type=i32]
  %stage = phi i4 [ 1, %0 ], [ %stage_1, %8 ]     ; [#uses=3 type=i4]
  %stage_cast2 = zext i4 %stage to i11, !dbg !35  ; [#uses=1 type=i11] [debug line = 76:13]
  %exitcond = icmp eq i4 %stage, -5, !dbg !35     ; [#uses=1 type=i1] [debug line = 76:13]
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 10, i64 10, i64 10)
  br i1 %exitcond, label %9, label %2, !dbg !35   ; [debug line = 76:13]

; <label>:2                                       ; preds = %1
  call void (...)* @_ssdm_op_SpecLoopName([7 x i8]* @p_str) nounwind, !dbg !37 ; [debug line = 77:3]
  %tmp_8 = call i32 (...)* @_ssdm_op_SpecRegionBegin([7 x i8]* @p_str) nounwind, !dbg !37 ; [#uses=1 type=i32] [debug line = 77:3]
  %DFTpts = shl i11 1, %stage_cast2, !dbg !39     ; [#uses=2 type=i11] [debug line = 79:2]
  %DFTpts_cast = zext i11 %DFTpts to i32, !dbg !39 ; [#uses=1 type=i32] [debug line = 79:2]
  call void @llvm.dbg.value(metadata !{i11 %DFTpts}, i64 0, metadata !40), !dbg !39 ; [debug line = 79:2] [debug variable = DFTpts]
  %p_lshr_f_cast = call i10 @_ssdm_op_PartSelect.i10.i11.i32.i32(i11 %DFTpts, i32 1, i32 10), !dbg !42 ; [#uses=2 type=i10] [debug line = 80:3]
  %numBF = zext i10 %p_lshr_f_cast to i32, !dbg !42 ; [#uses=1 type=i32] [debug line = 80:3]
  call void @llvm.dbg.value(metadata !{i32 %numBF}, i64 0, metadata !43), !dbg !42 ; [debug line = 80:3] [debug variable = numBF]
  br label %3, !dbg !44                           ; [debug line = 87:17]

; <label>:3                                       ; preds = %7, %2
  %i = phi i10 [ 0, %2 ], [ %j, %7 ]              ; [#uses=3 type=i10]
  %k = phi i32 [ 0, %2 ], [ %k_1, %7 ]            ; [#uses=2 type=i32]
  %i_cast1 = zext i10 %i to i32                   ; [#uses=1 type=i32]
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 1, i64 512, i64 256)
  %exitcond1 = icmp eq i10 %i, %p_lshr_f_cast, !dbg !44 ; [#uses=1 type=i1] [debug line = 87:17]
  %j = add i10 %i, 1, !dbg !46                    ; [#uses=1 type=i10] [debug line = 87:33]
  br i1 %exitcond1, label %8, label %4, !dbg !44  ; [debug line = 87:17]

; <label>:4                                       ; preds = %3
  call void (...)* @_ssdm_op_SpecLoopName([10 x i8]* @p_str3) nounwind, !dbg !47 ; [debug line = 88:4]
  %tmp_13 = call i32 (...)* @_ssdm_op_SpecRegionBegin([10 x i8]* @p_str3) nounwind, !dbg !47 ; [#uses=1 type=i32] [debug line = 88:4]
  %tmp_5 = sext i32 %k to i64, !dbg !49           ; [#uses=2 type=i64] [debug line = 91:2]
  %W_real_addr = getelementptr inbounds [512 x float]* @W_real, i64 0, i64 %tmp_5, !dbg !49 ; [#uses=1 type=float*] [debug line = 91:2]
  %c = load float* %W_real_addr, align 4, !dbg !49 ; [#uses=2 type=float] [debug line = 91:2]
  call void @llvm.dbg.value(metadata !{float %c}, i64 0, metadata !50), !dbg !49 ; [debug line = 91:2] [debug variable = c]
  %W_imag_addr = getelementptr inbounds [512 x float]* @W_imag, i64 0, i64 %tmp_5, !dbg !51 ; [#uses=1 type=float*] [debug line = 92:4]
  %s = load float* %W_imag_addr, align 4, !dbg !51 ; [#uses=2 type=float] [debug line = 92:4]
  call void @llvm.dbg.value(metadata !{float %s}, i64 0, metadata !52), !dbg !51 ; [debug line = 92:4] [debug variable = s]
  call void @llvm.dbg.value(metadata !{i10 %i}, i64 0, metadata !53), !dbg !54 ; [debug line = 95:22] [debug variable = i]
  br label %5, !dbg !54                           ; [debug line = 95:22]

; <label>:5                                       ; preds = %6, %4
  %i2 = phi i32 [ %i_cast1, %4 ], [ %i_1, %6 ]    ; [#uses=4 type=i32]
  %tmp_16 = call i22 @_ssdm_op_PartSelect.i22.i32.i32.i32(i32 %i2, i32 10, i32 31), !dbg !54 ; [#uses=1 type=i22] [debug line = 95:22]
  %icmp = icmp slt i22 %tmp_16, 1, !dbg !54       ; [#uses=1 type=i1] [debug line = 95:22]
  br i1 %icmp, label %6, label %7, !dbg !54       ; [debug line = 95:22]

; <label>:6                                       ; preds = %5
  call void (...)* @_ssdm_op_SpecLoopName([7 x i8]* @p_str4) nounwind, !dbg !56 ; [debug line = 96:5]
  %tmp_14 = call i32 (...)* @_ssdm_op_SpecRegionBegin([7 x i8]* @p_str4) nounwind, !dbg !56 ; [#uses=1 type=i32] [debug line = 96:5]
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 1024, i32 512, [1 x i8]* @p_str1) nounwind, !dbg !58 ; [debug line = 97:1]
  %i_lower = add nsw i32 %i2, %numBF, !dbg !59    ; [#uses=1 type=i32] [debug line = 100:2]
  call void @llvm.dbg.value(metadata !{i32 %i_lower}, i64 0, metadata !60), !dbg !59 ; [debug line = 100:2] [debug variable = i_lower]
  %tmp_9 = sext i32 %i_lower to i64, !dbg !61     ; [#uses=2 type=i64] [debug line = 101:5]
  %X_R_addr = getelementptr [1024 x float]* %X_R, i64 0, i64 %tmp_9, !dbg !61 ; [#uses=2 type=float*] [debug line = 101:5]
  %X_R_load = load float* %X_R_addr, align 4, !dbg !61 ; [#uses=2 type=float] [debug line = 101:5]
  %tmp_s = fmul float %X_R_load, %c, !dbg !61     ; [#uses=1 type=float] [debug line = 101:5]
  %X_I_addr = getelementptr [1024 x float]* %X_I, i64 0, i64 %tmp_9, !dbg !61 ; [#uses=2 type=float*] [debug line = 101:5]
  %X_I_load = load float* %X_I_addr, align 4, !dbg !61 ; [#uses=2 type=float] [debug line = 101:5]
  %tmp_1 = fmul float %X_I_load, %s, !dbg !61     ; [#uses=1 type=float] [debug line = 101:5]
  %temp_R = fsub float %tmp_s, %tmp_1, !dbg !61   ; [#uses=2 type=float] [debug line = 101:5]
  call void @llvm.dbg.value(metadata !{float %temp_R}, i64 0, metadata !62), !dbg !61 ; [debug line = 101:5] [debug variable = temp_R]
  %tmp_2 = fmul float %X_I_load, %c, !dbg !63     ; [#uses=1 type=float] [debug line = 102:5]
  %tmp_3 = fmul float %X_R_load, %s, !dbg !63     ; [#uses=1 type=float] [debug line = 102:5]
  %temp_I = fadd float %tmp_2, %tmp_3, !dbg !63   ; [#uses=2 type=float] [debug line = 102:5]
  call void @llvm.dbg.value(metadata !{float %temp_I}, i64 0, metadata !64), !dbg !63 ; [debug line = 102:5] [debug variable = temp_I]
  %tmp_4 = sext i32 %i2 to i64, !dbg !65          ; [#uses=2 type=i64] [debug line = 104:5]
  %X_R_addr_1 = getelementptr [1024 x float]* %X_R, i64 0, i64 %tmp_4, !dbg !65 ; [#uses=3 type=float*] [debug line = 104:5]
  %X_R_load_1 = load float* %X_R_addr_1, align 4, !dbg !65 ; [#uses=1 type=float] [debug line = 104:5]
  %tmp_6 = fsub float %X_R_load_1, %temp_R, !dbg !65 ; [#uses=1 type=float] [debug line = 104:5]
  store float %tmp_6, float* %X_R_addr, align 4, !dbg !65 ; [debug line = 104:5]
  %X_I_addr_1 = getelementptr [1024 x float]* %X_I, i64 0, i64 %tmp_4, !dbg !66 ; [#uses=3 type=float*] [debug line = 105:5]
  %X_I_load_1 = load float* %X_I_addr_1, align 4, !dbg !66 ; [#uses=1 type=float] [debug line = 105:5]
  %tmp_7 = fsub float %X_I_load_1, %temp_I, !dbg !66 ; [#uses=1 type=float] [debug line = 105:5]
  store float %tmp_7, float* %X_I_addr, align 4, !dbg !66 ; [debug line = 105:5]
  %X_R_load_2 = load float* %X_R_addr_1, align 4, !dbg !67 ; [#uses=1 type=float] [debug line = 106:5]
  %tmp_10 = fadd float %X_R_load_2, %temp_R, !dbg !67 ; [#uses=1 type=float] [debug line = 106:5]
  store float %tmp_10, float* %X_R_addr_1, align 4, !dbg !67 ; [debug line = 106:5]
  %X_I_load_2 = load float* %X_I_addr_1, align 4, !dbg !68 ; [#uses=1 type=float] [debug line = 107:5]
  %tmp_11 = fadd float %X_I_load_2, %temp_I, !dbg !68 ; [#uses=1 type=float] [debug line = 107:5]
  store float %tmp_11, float* %X_I_addr_1, align 4, !dbg !68 ; [debug line = 107:5]
  %empty = call i32 (...)* @_ssdm_op_SpecRegionEnd([7 x i8]* @p_str4, i32 %tmp_14) nounwind, !dbg !69 ; [#uses=0 type=i32] [debug line = 108:4]
  %i_1 = add nsw i32 %DFTpts_cast, %i2, !dbg !70  ; [#uses=1 type=i32] [debug line = 95:53]
  call void @llvm.dbg.value(metadata !{i32 %i_1}, i64 0, metadata !53), !dbg !70 ; [debug line = 95:53] [debug variable = i]
  br label %5, !dbg !70                           ; [debug line = 95:53]

; <label>:7                                       ; preds = %5
  %k_1 = add nsw i32 %step, %k, !dbg !71          ; [#uses=1 type=i32] [debug line = 109:4]
  call void @llvm.dbg.value(metadata !{i32 %k_1}, i64 0, metadata !72), !dbg !71 ; [debug line = 109:4] [debug variable = k]
  %empty_8 = call i32 (...)* @_ssdm_op_SpecRegionEnd([10 x i8]* @p_str3, i32 %tmp_13) nounwind, !dbg !73 ; [#uses=0 type=i32] [debug line = 110:3]
  call void @llvm.dbg.value(metadata !{i10 %j}, i64 0, metadata !74), !dbg !46 ; [debug line = 87:33] [debug variable = j]
  br label %3, !dbg !46                           ; [debug line = 87:33]

; <label>:8                                       ; preds = %3
  %tmp_12 = call i1 @_ssdm_op_BitSelect.i1.i32.i32(i32 %step, i32 31), !dbg !75 ; [#uses=1 type=i1] [debug line = 111:3]
  %p_neg1 = sub i32 0, %step, !dbg !75            ; [#uses=1 type=i32] [debug line = 111:3]
  %p_lshr1 = call i31 @_ssdm_op_PartSelect.i31.i32.i32.i32(i32 %p_neg1, i32 1, i32 31), !dbg !75 ; [#uses=1 type=i31] [debug line = 111:3]
  %tmp = zext i31 %p_lshr1 to i32, !dbg !75       ; [#uses=1 type=i32] [debug line = 111:3]
  %p_neg_t1 = sub i32 0, %tmp, !dbg !75           ; [#uses=1 type=i32] [debug line = 111:3]
  %p_lshr_f1 = call i31 @_ssdm_op_PartSelect.i31.i32.i32.i32(i32 %step, i32 1, i32 31), !dbg !75 ; [#uses=1 type=i31] [debug line = 111:3]
  %tmp_15 = zext i31 %p_lshr_f1 to i32, !dbg !75  ; [#uses=1 type=i32] [debug line = 111:3]
  %step_1 = select i1 %tmp_12, i32 %p_neg_t1, i32 %tmp_15, !dbg !75 ; [#uses=1 type=i32] [debug line = 111:3]
  call void @llvm.dbg.value(metadata !{i32 %step_1}, i64 0, metadata !76), !dbg !75 ; [debug line = 111:3] [debug variable = step]
  %empty_9 = call i32 (...)* @_ssdm_op_SpecRegionEnd([7 x i8]* @p_str, i32 %tmp_8) nounwind, !dbg !77 ; [#uses=0 type=i32] [debug line = 112:2]
  %stage_1 = add i4 %stage, 1, !dbg !78           ; [#uses=1 type=i4] [debug line = 76:66]
  call void @llvm.dbg.value(metadata !{i4 %stage_1}, i64 0, metadata !79), !dbg !78 ; [debug line = 76:66] [debug variable = stage]
  br label %1, !dbg !78                           ; [debug line = 76:66]

; <label>:9                                       ; preds = %1
  ret void, !dbg !80                              ; [debug line = 113:1]
}

; [#uses=1]
define internal fastcc void @fft_bit_reverse([1024 x float]* nocapture %X_R, [1024 x float]* nocapture %X_I) {
  call void @llvm.dbg.value(metadata !{[1024 x float]* %X_R}, i64 0, metadata !81), !dbg !83 ; [debug line = 26:24] [debug variable = X_R]
  call void @llvm.dbg.value(metadata !{[1024 x float]* %X_I}, i64 0, metadata !84), !dbg !85 ; [debug line = 26:59] [debug variable = X_I]
  br label %1, !dbg !86                           ; [debug line = 31:6]

; <label>:1                                       ; preds = %._crit_edge, %0
  %input_assign = phi i11 [ 0, %0 ], [ %i_2, %._crit_edge ] ; [#uses=5 type=i11]
  %tmp_19 = trunc i11 %input_assign to i10, !dbg !89 ; [#uses=1 type=i10] [debug line = 16:40@32:14]
  %input_assign_cast1 = zext i11 %input_assign to i32, !dbg !89 ; [#uses=1 type=i32] [debug line = 16:40@32:14]
  call void @llvm.dbg.value(metadata !{i11 %input_assign}, i64 0, metadata !96) nounwind, !dbg !89 ; [debug line = 16:40@32:14] [debug variable = input]
  %exitcond = icmp eq i11 %input_assign, -1024, !dbg !86 ; [#uses=1 type=i1] [debug line = 31:6]
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 1024, i64 1024, i64 1024)
  %i_2 = add i11 1, %input_assign, !dbg !97       ; [#uses=1 type=i11] [debug line = 31:41]
  br i1 %exitcond, label %4, label %.preheader, !dbg !86 ; [debug line = 31:6]

.preheader:                                       ; preds = %2, %1
  %reversed = phi i32 [ %rev, %2 ], [ 0, %1 ]     ; [#uses=3 type=i32]
  %i_i = phi i4 [ %i, %2 ], [ 0, %1 ]             ; [#uses=2 type=i4]
  %p_0_i = phi i10 [ %input_assign_1, %2 ], [ %tmp_19, %1 ] ; [#uses=2 type=i10]
  %exitcond_i = icmp eq i4 %i_i, -6, !dbg !98     ; [#uses=1 type=i1] [debug line = 18:6@32:14]
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 10, i64 10, i64 10) nounwind
  %i = add i4 %i_i, 1, !dbg !101                  ; [#uses=1 type=i4] [debug line = 18:52@32:14]
  br i1 %exitcond_i, label %reverse_bits.exit, label %2, !dbg !98 ; [debug line = 18:6@32:14]

; <label>:2                                       ; preds = %.preheader
  %tmp_20 = trunc i32 %reversed to i31            ; [#uses=1 type=i31]
  %tmp_21 = trunc i10 %p_0_i to i1, !dbg !98      ; [#uses=1 type=i1] [debug line = 18:6@32:14]
  %rev = call i32 @_ssdm_op_BitConcatenate.i32.i31.i1(i31 %tmp_20, i1 %tmp_21), !dbg !102 ; [#uses=1 type=i32] [debug line = 19:3@32:14]
  call void @llvm.dbg.value(metadata !{i32 %rev}, i64 0, metadata !104) nounwind, !dbg !102 ; [debug line = 19:3@32:14] [debug variable = rev]
  %tmp_22 = call i9 @_ssdm_op_PartSelect.i9.i10.i32.i32(i10 %p_0_i, i32 1, i32 9), !dbg !105 ; [#uses=1 type=i9] [debug line = 20:3@32:14]
  %input_assign_1 = zext i9 %tmp_22 to i10, !dbg !105 ; [#uses=1 type=i10] [debug line = 20:3@32:14]
  call void @llvm.dbg.value(metadata !{i4 %i}, i64 0, metadata !106) nounwind, !dbg !101 ; [debug line = 18:52@32:14] [debug variable = i]
  br label %.preheader, !dbg !101                 ; [debug line = 18:52@32:14]

reverse_bits.exit:                                ; preds = %.preheader
  call void @llvm.dbg.value(metadata !{i32 %reversed}, i64 0, metadata !107), !dbg !94 ; [debug line = 32:14] [debug variable = reversed]
  %tmp = icmp ult i32 %input_assign_cast1, %reversed, !dbg !108 ; [#uses=1 type=i1] [debug line = 33:3]
  br i1 %tmp, label %3, label %._crit_edge, !dbg !108 ; [debug line = 33:3]

; <label>:3                                       ; preds = %reverse_bits.exit
  %tmp_s = zext i11 %input_assign to i64, !dbg !109 ; [#uses=2 type=i64] [debug line = 34:4]
  %X_R_addr = getelementptr [1024 x float]* %X_R, i64 0, i64 %tmp_s, !dbg !109 ; [#uses=2 type=float*] [debug line = 34:4]
  %tmp_23 = load float* %X_R_addr, align 4, !dbg !109 ; [#uses=1 type=float] [debug line = 34:4]
  call void @llvm.dbg.value(metadata !{float %tmp_23}, i64 0, metadata !111), !dbg !109 ; [debug line = 34:4] [debug variable = tmp]
  %tmp_12 = zext i32 %reversed to i64, !dbg !112  ; [#uses=2 type=i64] [debug line = 35:4]
  %X_R_addr_2 = getelementptr [1024 x float]* %X_R, i64 0, i64 %tmp_12, !dbg !112 ; [#uses=2 type=float*] [debug line = 35:4]
  %X_R_load = load float* %X_R_addr_2, align 4, !dbg !112 ; [#uses=1 type=float] [debug line = 35:4]
  store float %X_R_load, float* %X_R_addr, align 4, !dbg !112 ; [debug line = 35:4]
  store float %tmp_23, float* %X_R_addr_2, align 4, !dbg !113 ; [debug line = 36:4]
  %X_I_addr = getelementptr [1024 x float]* %X_I, i64 0, i64 %tmp_s, !dbg !114 ; [#uses=2 type=float*] [debug line = 38:4]
  %tmp_24 = load float* %X_I_addr, align 4, !dbg !114 ; [#uses=1 type=float] [debug line = 38:4]
  call void @llvm.dbg.value(metadata !{float %tmp_24}, i64 0, metadata !111), !dbg !114 ; [debug line = 38:4] [debug variable = tmp]
  %X_I_addr_2 = getelementptr [1024 x float]* %X_I, i64 0, i64 %tmp_12, !dbg !115 ; [#uses=2 type=float*] [debug line = 39:4]
  %X_I_load = load float* %X_I_addr_2, align 4, !dbg !115 ; [#uses=1 type=float] [debug line = 39:4]
  store float %X_I_load, float* %X_I_addr, align 4, !dbg !115 ; [debug line = 39:4]
  store float %tmp_24, float* %X_I_addr_2, align 4, !dbg !116 ; [debug line = 40:4]
  br label %._crit_edge, !dbg !117                ; [debug line = 41:3]

._crit_edge:                                      ; preds = %3, %reverse_bits.exit
  call void @llvm.dbg.value(metadata !{i11 %i_2}, i64 0, metadata !118), !dbg !97 ; [debug line = 31:41] [debug variable = i]
  br label %1, !dbg !97                           ; [debug line = 31:41]

; <label>:4                                       ; preds = %1
  ret void, !dbg !119                             ; [debug line = 46:1]
}

; [#uses=1]
define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

; [#uses=3]
define weak i32 @_ssdm_op_SpecRegionEnd(...) {
entry:
  ret i32 0
}

; [#uses=3]
define weak i32 @_ssdm_op_SpecRegionBegin(...) {
entry:
  ret i32 0
}

; [#uses=5]
define weak void @_ssdm_op_SpecLoopTripCount(...) nounwind {
entry:
  ret void
}

; [#uses=3]
define weak void @_ssdm_op_SpecLoopName(...) nounwind {
entry:
  ret void
}

; [#uses=2]
define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

; [#uses=1]
define weak i9 @_ssdm_op_PartSelect.i9.i10.i32.i32(i10, i32, i32) nounwind readnone {
entry:
  %empty = call i10 @llvm.part.select.i10(i10 %0, i32 %1, i32 %2) ; [#uses=1 type=i10]
  %empty_10 = trunc i10 %empty to i9              ; [#uses=1 type=i9]
  ret i9 %empty_10
}

; [#uses=2]
define weak i31 @_ssdm_op_PartSelect.i31.i32.i32.i32(i32, i32, i32) nounwind readnone {
entry:
  %empty = call i32 @llvm.part.select.i32(i32 %0, i32 %1, i32 %2) ; [#uses=1 type=i32]
  %empty_11 = trunc i32 %empty to i31             ; [#uses=1 type=i31]
  ret i31 %empty_11
}

; [#uses=1]
define weak i22 @_ssdm_op_PartSelect.i22.i32.i32.i32(i32, i32, i32) nounwind readnone {
entry:
  %empty = call i32 @llvm.part.select.i32(i32 %0, i32 %1, i32 %2) ; [#uses=1 type=i32]
  %empty_12 = trunc i32 %empty to i22             ; [#uses=1 type=i22]
  ret i22 %empty_12
}

; [#uses=1]
define weak i10 @_ssdm_op_PartSelect.i10.i11.i32.i32(i11, i32, i32) nounwind readnone {
entry:
  %empty = call i11 @llvm.part.select.i11(i11 %0, i32 %1, i32 %2) ; [#uses=1 type=i11]
  %empty_13 = trunc i11 %empty to i10             ; [#uses=1 type=i10]
  ret i10 %empty_13
}

; [#uses=0]
declare i1 @_ssdm_op_PartSelect.i1.i10.i32.i32(i10, i32, i32) nounwind readnone

; [#uses=0]
declare i16 @_ssdm_op_HSub(...)

; [#uses=0]
declare i16 @_ssdm_op_HMul(...)

; [#uses=0]
declare i16 @_ssdm_op_HDiv(...)

; [#uses=0]
declare i16 @_ssdm_op_HAdd(...)

; [#uses=1]
define weak i1 @_ssdm_op_BitSelect.i1.i32.i32(i32, i32) nounwind readnone {
entry:
  %empty = shl i32 1, %1                          ; [#uses=1 type=i32]
  %empty_14 = and i32 %0, %empty                  ; [#uses=1 type=i32]
  %empty_15 = icmp ne i32 %empty_14, 0            ; [#uses=1 type=i1]
  ret i1 %empty_15
}

; [#uses=1]
define weak i32 @_ssdm_op_BitConcatenate.i32.i31.i1(i31, i1) nounwind readnone {
entry:
  %empty = zext i31 %0 to i32                     ; [#uses=1 type=i32]
  %empty_16 = zext i1 %1 to i32                   ; [#uses=1 type=i32]
  %empty_17 = shl i32 %empty, 1                   ; [#uses=1 type=i32]
  %empty_18 = or i32 %empty_17, %empty_16         ; [#uses=1 type=i32]
  ret i32 %empty_18
}

; [#uses=1]
declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [1 x i32]* @llvm_global_ctors_0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 0, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 31, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"X_R", metadata !11, metadata !"float", i32 0, i32 31}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 1023, i32 1}
!13 = metadata !{metadata !14}
!14 = metadata !{i32 0, i32 31, metadata !15}
!15 = metadata !{metadata !16}
!16 = metadata !{metadata !"X_I", metadata !11, metadata !"float", i32 0, i32 31}
!17 = metadata !{i32 786689, metadata !18, metadata !"X_R", null, i32 49, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!18 = metadata !{i32 786478, i32 0, metadata !19, metadata !"fft", metadata !"fft", metadata !"_Z3fftPfS_", metadata !19, i32 49, metadata !20, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !25, i32 50} ; [ DW_TAG_subprogram ]
!19 = metadata !{i32 786473, metadata !"fft.cpp", metadata !"d:/Projects/vivado/project_4/HLS/0_Initial", null} ; [ DW_TAG_file_type ]
!20 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !21, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!21 = metadata !{null, metadata !22, metadata !22}
!22 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !23} ; [ DW_TAG_pointer_type ]
!23 = metadata !{i32 786454, null, metadata !"DTYPE", metadata !19, i32 4, i64 0, i64 0, i64 0, i32 0, metadata !24} ; [ DW_TAG_typedef ]
!24 = metadata !{i32 786468, null, metadata !"float", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 4} ; [ DW_TAG_base_type ]
!25 = metadata !{metadata !26}
!26 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!27 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 0, i64 0, i32 0, i32 0, metadata !23, metadata !28, i32 0, i32 0} ; [ DW_TAG_array_type ]
!28 = metadata !{metadata !29}
!29 = metadata !{i32 786465, i64 0, i64 1023}     ; [ DW_TAG_subrange_type ]
!30 = metadata !{i32 49, i32 16, metadata !18, null}
!31 = metadata !{i32 786689, metadata !18, metadata !"X_I", null, i32 49, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!32 = metadata !{i32 49, i32 51, metadata !18, null}
!33 = metadata !{i32 68, i32 2, metadata !34, null}
!34 = metadata !{i32 786443, metadata !18, i32 50, i32 1, metadata !19, i32 7} ; [ DW_TAG_lexical_block ]
!35 = metadata !{i32 76, i32 13, metadata !36, null}
!36 = metadata !{i32 786443, metadata !34, i32 76, i32 9, metadata !19, i32 8} ; [ DW_TAG_lexical_block ]
!37 = metadata !{i32 77, i32 3, metadata !38, null}
!38 = metadata !{i32 786443, metadata !36, i32 77, i32 2, metadata !19, i32 9} ; [ DW_TAG_lexical_block ]
!39 = metadata !{i32 79, i32 2, metadata !38, null}
!40 = metadata !{i32 786688, metadata !34, metadata !"DFTpts", metadata !19, i32 61, metadata !41, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!41 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!42 = metadata !{i32 80, i32 3, metadata !38, null}
!43 = metadata !{i32 786688, metadata !34, metadata !"numBF", metadata !19, i32 62, metadata !41, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!44 = metadata !{i32 87, i32 17, metadata !45, null}
!45 = metadata !{i32 786443, metadata !38, i32 87, i32 13, metadata !19, i32 10} ; [ DW_TAG_lexical_block ]
!46 = metadata !{i32 87, i32 33, metadata !45, null}
!47 = metadata !{i32 88, i32 4, metadata !48, null}
!48 = metadata !{i32 786443, metadata !45, i32 88, i32 3, metadata !19, i32 11} ; [ DW_TAG_lexical_block ]
!49 = metadata !{i32 91, i32 2, metadata !48, null}
!50 = metadata !{i32 786688, metadata !34, metadata !"c", metadata !19, i32 74, metadata !23, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!51 = metadata !{i32 92, i32 4, metadata !48, null}
!52 = metadata !{i32 786688, metadata !34, metadata !"s", metadata !19, i32 74, metadata !23, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!53 = metadata !{i32 786688, metadata !34, metadata !"i", metadata !19, i32 56, metadata !41, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!54 = metadata !{i32 95, i32 22, metadata !55, null}
!55 = metadata !{i32 786443, metadata !48, i32 95, i32 18, metadata !19, i32 12} ; [ DW_TAG_lexical_block ]
!56 = metadata !{i32 96, i32 5, metadata !57, null}
!57 = metadata !{i32 786443, metadata !55, i32 96, i32 4, metadata !19, i32 13} ; [ DW_TAG_lexical_block ]
!58 = metadata !{i32 97, i32 1, metadata !57, null}
!59 = metadata !{i32 100, i32 2, metadata !57, null}
!60 = metadata !{i32 786688, metadata !34, metadata !"i_lower", metadata !19, i32 57, metadata !41, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!61 = metadata !{i32 101, i32 5, metadata !57, null}
!62 = metadata !{i32 786688, metadata !34, metadata !"temp_R", metadata !19, i32 52, metadata !23, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!63 = metadata !{i32 102, i32 5, metadata !57, null}
!64 = metadata !{i32 786688, metadata !34, metadata !"temp_I", metadata !19, i32 53, metadata !23, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!65 = metadata !{i32 104, i32 5, metadata !57, null}
!66 = metadata !{i32 105, i32 5, metadata !57, null}
!67 = metadata !{i32 106, i32 5, metadata !57, null}
!68 = metadata !{i32 107, i32 5, metadata !57, null}
!69 = metadata !{i32 108, i32 4, metadata !57, null}
!70 = metadata !{i32 95, i32 53, metadata !55, null}
!71 = metadata !{i32 109, i32 4, metadata !48, null}
!72 = metadata !{i32 786688, metadata !34, metadata !"k", metadata !19, i32 56, metadata !41, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!73 = metadata !{i32 110, i32 3, metadata !48, null}
!74 = metadata !{i32 786688, metadata !34, metadata !"j", metadata !19, i32 56, metadata !41, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!75 = metadata !{i32 111, i32 3, metadata !38, null}
!76 = metadata !{i32 786688, metadata !34, metadata !"step", metadata !19, i32 58, metadata !41, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!77 = metadata !{i32 112, i32 2, metadata !38, null}
!78 = metadata !{i32 76, i32 66, metadata !36, null}
!79 = metadata !{i32 786688, metadata !34, metadata !"stage", metadata !19, i32 60, metadata !41, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!80 = metadata !{i32 113, i32 1, metadata !34, null}
!81 = metadata !{i32 786689, metadata !82, metadata !"X_R", null, i32 26, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!82 = metadata !{i32 786478, i32 0, metadata !19, metadata !"bit_reverse", metadata !"bit_reverse", metadata !"_Z11bit_reversePfS_", metadata !19, i32 26, metadata !20, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !25, i32 26} ; [ DW_TAG_subprogram ]
!83 = metadata !{i32 26, i32 24, metadata !82, null}
!84 = metadata !{i32 786689, metadata !82, metadata !"X_I", null, i32 26, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!85 = metadata !{i32 26, i32 59, metadata !82, null}
!86 = metadata !{i32 31, i32 6, metadata !87, null}
!87 = metadata !{i32 786443, metadata !88, i32 31, i32 2, metadata !19, i32 4} ; [ DW_TAG_lexical_block ]
!88 = metadata !{i32 786443, metadata !82, i32 26, i32 88, metadata !19, i32 3} ; [ DW_TAG_lexical_block ]
!89 = metadata !{i32 16, i32 40, metadata !90, metadata !94}
!90 = metadata !{i32 786478, i32 0, metadata !19, metadata !"reverse_bits", metadata !"reverse_bits", metadata !"_Z12reverse_bitsj", metadata !19, i32 16, metadata !91, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !25, i32 16} ; [ DW_TAG_subprogram ]
!91 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !92, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!92 = metadata !{metadata !93, metadata !93}
!93 = metadata !{i32 786468, null, metadata !"unsigned int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 7} ; [ DW_TAG_base_type ]
!94 = metadata !{i32 32, i32 14, metadata !95, null}
!95 = metadata !{i32 786443, metadata !87, i32 31, i32 46, metadata !19, i32 5} ; [ DW_TAG_lexical_block ]
!96 = metadata !{i32 786689, metadata !90, metadata !"input", metadata !19, i32 16777232, metadata !93, i32 0, metadata !94} ; [ DW_TAG_arg_variable ]
!97 = metadata !{i32 31, i32 41, metadata !87, null}
!98 = metadata !{i32 18, i32 6, metadata !99, metadata !94}
!99 = metadata !{i32 786443, metadata !100, i32 18, i32 2, metadata !19, i32 1} ; [ DW_TAG_lexical_block ]
!100 = metadata !{i32 786443, metadata !90, i32 16, i32 47, metadata !19, i32 0} ; [ DW_TAG_lexical_block ]
!101 = metadata !{i32 18, i32 52, metadata !99, metadata !94}
!102 = metadata !{i32 19, i32 3, metadata !103, metadata !94}
!103 = metadata !{i32 786443, metadata !99, i32 18, i32 57, metadata !19, i32 2} ; [ DW_TAG_lexical_block ]
!104 = metadata !{i32 786688, metadata !100, metadata !"rev", metadata !19, i32 17, metadata !41, i32 0, metadata !94} ; [ DW_TAG_auto_variable ]
!105 = metadata !{i32 20, i32 3, metadata !103, metadata !94}
!106 = metadata !{i32 786688, metadata !100, metadata !"i", metadata !19, i32 17, metadata !41, i32 0, metadata !94} ; [ DW_TAG_auto_variable ]
!107 = metadata !{i32 786688, metadata !88, metadata !"reversed", metadata !19, i32 27, metadata !93, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!108 = metadata !{i32 33, i32 3, metadata !95, null}
!109 = metadata !{i32 34, i32 4, metadata !110, null}
!110 = metadata !{i32 786443, metadata !95, i32 33, i32 20, metadata !19, i32 6} ; [ DW_TAG_lexical_block ]
!111 = metadata !{i32 786688, metadata !88, metadata !"tmp", metadata !19, i32 29, metadata !23, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!112 = metadata !{i32 35, i32 4, metadata !110, null}
!113 = metadata !{i32 36, i32 4, metadata !110, null}
!114 = metadata !{i32 38, i32 4, metadata !110, null}
!115 = metadata !{i32 39, i32 4, metadata !110, null}
!116 = metadata !{i32 40, i32 4, metadata !110, null}
!117 = metadata !{i32 41, i32 3, metadata !110, null}
!118 = metadata !{i32 786688, metadata !88, metadata !"i", metadata !19, i32 28, metadata !93, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!119 = metadata !{i32 46, i32 1, metadata !88, null}
