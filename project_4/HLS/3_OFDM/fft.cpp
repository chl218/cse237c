/*
This is traditional 2-radix DIT FFT algorithm implementation.
INPUT:
	In_R, In_I[]: Real and Imag parts of Complex signal

OUTPUT:
	real_o, imag_o[]: Real and Imag parts of Complex signal
*/

#include "fft.h"

void bit_reverse(DTYPE real_i[SIZE], DTYPE imag_i[SIZE], DTYPE real_o[SIZE], DTYPE imag_o[SIZE]) {
#pragma HLS DATAFLOW

	rev: for(unsigned int i = 0; i < SIZE; i+=2) {
#pragma HLS UNROLL factor=32
#pragma HLS PIPELINE II=1

		unsigned int r0 = index_table[i];
		unsigned int r1 = index_table[i+1];

		real_o[i] = real_i[r0];
		imag_o[i] = imag_i[r0];

		real_o[i+1] = real_i[r1];
		imag_o[i+1] = imag_i[r1];
	}
}

void fft_stage_first(DTYPE real_i[SIZE], DTYPE imag_i[SIZE], DTYPE real_o[SIZE], DTYPE imag_o[SIZE]) {
	DTYPE c = W_real[0];
	DTYPE s = W_imag[0];

	DFTpts:for(int i = 0; i < SIZE; i += 2) {
#pragma HLS PIPELINE II=1
#pragma HLS UNROLL factor=4
		int i_lower = i + 1;
		DTYPE temp_R = real_i[i_lower]*c - imag_i[i_lower]*s;
		DTYPE temp_I = imag_i[i_lower]*c + real_i[i_lower]*s;

		real_o[i_lower] = real_i[i] - temp_R;
		imag_o[i_lower] = imag_i[i] - temp_I;
		real_o[i] = real_i[i] + temp_R;
		imag_o[i] = imag_i[i] + temp_I;
	}
}

void fft_stages(DTYPE real_i[SIZE], DTYPE imag_i[SIZE], int STAGES, DTYPE real_o[SIZE], DTYPE imag_o[SIZE]){
	int DFTpts 	= 1 << STAGES;			// DFT = 2^stage = points in sub DFT
	int numBF 	= DFTpts >> 1; 			// Butterfly WIDTHS in sub-DFT
	int k 		= 0;
	int step 	= step_table[STAGES];

	butterfly: for(int j = 0; j < numBF; j++) {
#pragma HLS PIPELINE II=1

		DTYPE c = W_real[k];
		DTYPE s = W_imag[k];

		DFTpts:for(int i = j; i < SIZE; i += DFTpts) {
#pragma HLS LOOP_TRIPCOUNT
#pragma HLS PIPELINE II=1
#pragma HLS UNROLL factor=4

			int i_lower = i + numBF;
			DTYPE temp_R = real_i[i_lower]*c - imag_i[i_lower]*s;
			DTYPE temp_I = imag_i[i_lower]*c + real_i[i_lower]*s;

			real_o[i_lower] = real_i[i] - temp_R;
			imag_o[i_lower] = imag_i[i] - temp_I;
			real_o[i] = real_i[i] + temp_R;
			imag_o[i] = imag_i[i] + temp_I;
		}
		k+=step;
	}
}

void fft_stage_last(DTYPE real_i[SIZE], DTYPE imag_i[SIZE], DTYPE real_o[SIZE], DTYPE imag_o[SIZE]) {
	int k 		= 0;
	int DFTpts	= 1024;		// DFT = 2^stage = points in sub DFT
	int numBF 	= 512; 		// Butterfly WIDTHS in sub-DFT

	butterfly: for(int j = 0; j < numBF; j++) {
#pragma HLS LOOP_TRIPCOUNT
#pragma HLS PIPELINE II=1
#pragma HLS UNROLL factor=4
		DTYPE c = W_real[k];
		DTYPE s = W_imag[k];

		int i_lower = j + numBF;
		DTYPE temp_R = real_i[i_lower]*c - imag_i[i_lower]*s;
		DTYPE temp_I = imag_i[i_lower]*c + real_i[i_lower]*s;

		real_o[i_lower] = real_i[j] - temp_R;
		imag_o[i_lower] = imag_i[j] - temp_I;
		real_o[j] = real_i[j] + temp_R;
		imag_o[j] = imag_i[j] + temp_I;
		k++;
	}
}


void fft(DTYPE real_i[SIZE], DTYPE imag_i[SIZE], DTYPE real_o[SIZE], DTYPE imag_o[SIZE]) {
#pragma HLS DATAFLOW
#pragma HLS ARRAY_PARTITION variable=imag_o cyclic factor=2 dim=1
#pragma HLS ARRAY_PARTITION variable=imag_i cyclic factor=2 dim=1
#pragma HLS ARRAY_PARTITION variable=real_o cyclic factor=2 dim=1
#pragma HLS ARRAY_PARTITION variable=real_i cyclic factor=2 dim=1
#pragma HLS ARRAY_PARTITION variable=index_table cyclic factor=2 dim=1
#pragma HLS ARRAY_PARTITION variable=step_table  cyclic factor=2 dim=1

	//Call fft
	DTYPE Stage0_R[SIZE], Stage0_I[SIZE];
	DTYPE Stage1_R[SIZE], Stage1_I[SIZE];
	DTYPE Stage2_R[SIZE], Stage2_I[SIZE];
	DTYPE Stage3_R[SIZE], Stage3_I[SIZE];
	DTYPE Stage4_R[SIZE], Stage4_I[SIZE];
	DTYPE Stage5_R[SIZE], Stage5_I[SIZE];
	DTYPE Stage6_R[SIZE], Stage6_I[SIZE];
	DTYPE Stage7_R[SIZE], Stage7_I[SIZE];
	DTYPE Stage8_R[SIZE], Stage8_I[SIZE];
	DTYPE Stage9_R[SIZE], Stage9_I[SIZE];
	DTYPE Stage10_R[SIZE], Stage10_I[SIZE];

#pragma HLS ARRAY_PARTITION variable=Stage0_R cyclic factor=2 dim=1
#pragma HLS ARRAY_PARTITION variable=Stage1_R cyclic factor=2 dim=1
#pragma HLS ARRAY_PARTITION variable=Stage2_R cyclic factor=2 dim=1
#pragma HLS ARRAY_PARTITION variable=Stage3_R cyclic factor=2 dim=1
#pragma HLS ARRAY_PARTITION variable=Stage4_R cyclic factor=2 dim=1
#pragma HLS ARRAY_PARTITION variable=Stage5_R cyclic factor=2 dim=1
#pragma HLS ARRAY_PARTITION variable=Stage6_R cyclic factor=2 dim=1
#pragma HLS ARRAY_PARTITION variable=Stage7_R cyclic factor=2 dim=1
#pragma HLS ARRAY_PARTITION variable=Stage8_R cyclic factor=2 dim=1
#pragma HLS ARRAY_PARTITION variable=Stage9_R cyclic factor=2 dim=1
#pragma HLS ARRAY_PARTITION variable=Stage10_R cyclic factor=2 dim=1

	bit_reverse(real_i, imag_i, Stage0_R, Stage0_I);
	fft_stage_first(Stage0_R, Stage0_I, Stage1_R, Stage1_I);
	fft_stages(Stage1_R, Stage1_I, 2, Stage2_R, Stage2_I);
	fft_stages(Stage2_R, Stage2_I, 3, Stage3_R, Stage3_I);
	fft_stages(Stage3_R, Stage3_I, 4, Stage4_R, Stage4_I);
	fft_stages(Stage4_R, Stage4_I, 5, Stage5_R, Stage5_I);
	fft_stages(Stage5_R, Stage5_I, 6, Stage6_R, Stage6_I);
	fft_stages(Stage6_R, Stage6_I, 7, Stage7_R, Stage7_I);
	fft_stages(Stage7_R, Stage7_I, 8, Stage8_R, Stage8_I);
	fft_stages(Stage8_R, Stage8_I, 9, Stage9_R, Stage9_I);
	fft_stage_last(Stage9_R, Stage9_I, Stage10_R, Stage10_I);
	bit_reverse(Stage10_R, Stage10_I, real_o, imag_o);
}



