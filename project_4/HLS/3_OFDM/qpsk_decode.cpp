#include "fft.h"
#include <stdio.h>

void qpsk_decode(DTYPE R[SIZE], DTYPE I[SIZE], int D[SIZE]) {

	for(int i = 0; i < SIZE; i++) {
#pragma HLS PIPELINE
		if(R[i] > 0) {
			D[i] = I[i] > 0 ? 0 : 2;
		}
		else {
			D[i] = I[i] > 0 ? 1 : 3;
		}
	}
}

