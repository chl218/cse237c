// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module ofdm_receiver_bit_reverse20 (
        real_i_address0,
        real_i_ce0,
        real_i_d0,
        real_i_q0,
        real_i_we0,
        real_i_address1,
        real_i_ce1,
        real_i_d1,
        real_i_q1,
        real_i_we1,
        real_i1_address0,
        real_i1_ce0,
        real_i1_d0,
        real_i1_q0,
        real_i1_we0,
        real_i1_address1,
        real_i1_ce1,
        real_i1_d1,
        real_i1_q1,
        real_i1_we1,
        imag_i_address0,
        imag_i_ce0,
        imag_i_d0,
        imag_i_q0,
        imag_i_we0,
        imag_i_address1,
        imag_i_ce1,
        imag_i_d1,
        imag_i_q1,
        imag_i_we1,
        real_o_address0,
        real_o_ce0,
        real_o_d0,
        real_o_q0,
        real_o_we0,
        real_o_address1,
        real_o_ce1,
        real_o_d1,
        real_o_q1,
        real_o_we1,
        real_o2_address0,
        real_o2_ce0,
        real_o2_d0,
        real_o2_q0,
        real_o2_we0,
        real_o2_address1,
        real_o2_ce1,
        real_o2_d1,
        real_o2_q1,
        real_o2_we1,
        imag_o_address0,
        imag_o_ce0,
        imag_o_d0,
        imag_o_q0,
        imag_o_we0,
        imag_o_address1,
        imag_o_ce1,
        imag_o_d1,
        imag_o_q1,
        imag_o_we1,
        imag_o3_address0,
        imag_o3_ce0,
        imag_o3_d0,
        imag_o3_q0,
        imag_o3_we0,
        imag_o3_address1,
        imag_o3_ce1,
        imag_o3_d1,
        imag_o3_q1,
        imag_o3_we1,
        ap_clk,
        ap_rst,
        imag_i_pipo_status,
        imag_i_pipo_update,
        real_i_pipo_status,
        real_i_pipo_update,
        real_i1_pipo_status,
        real_i1_pipo_update,
        ap_continue,
        ap_done,
        ap_start,
        ap_idle,
        ap_ready
);

parameter    ap_const_lv9_0 = 9'b000000000;
parameter    ap_const_logic_0 = 1'b0;
parameter    ap_const_lv32_0 = 32'b00000000000000000000000000000000;
parameter    ap_const_lv10_0 = 10'b0000000000;
parameter    ap_const_logic_1 = 1'b1;
parameter    ap_true = 1'b1;

output  [8:0] real_i_address0;
output   real_i_ce0;
output  [31:0] real_i_d0;
input  [31:0] real_i_q0;
output   real_i_we0;
output  [8:0] real_i_address1;
output   real_i_ce1;
output  [31:0] real_i_d1;
input  [31:0] real_i_q1;
output   real_i_we1;
output  [8:0] real_i1_address0;
output   real_i1_ce0;
output  [31:0] real_i1_d0;
input  [31:0] real_i1_q0;
output   real_i1_we0;
output  [8:0] real_i1_address1;
output   real_i1_ce1;
output  [31:0] real_i1_d1;
input  [31:0] real_i1_q1;
output   real_i1_we1;
output  [9:0] imag_i_address0;
output   imag_i_ce0;
output  [31:0] imag_i_d0;
input  [31:0] imag_i_q0;
output   imag_i_we0;
output  [9:0] imag_i_address1;
output   imag_i_ce1;
output  [31:0] imag_i_d1;
input  [31:0] imag_i_q1;
output   imag_i_we1;
output  [8:0] real_o_address0;
output   real_o_ce0;
output  [31:0] real_o_d0;
input  [31:0] real_o_q0;
output   real_o_we0;
output  [8:0] real_o_address1;
output   real_o_ce1;
output  [31:0] real_o_d1;
input  [31:0] real_o_q1;
output   real_o_we1;
output  [8:0] real_o2_address0;
output   real_o2_ce0;
output  [31:0] real_o2_d0;
input  [31:0] real_o2_q0;
output   real_o2_we0;
output  [8:0] real_o2_address1;
output   real_o2_ce1;
output  [31:0] real_o2_d1;
input  [31:0] real_o2_q1;
output   real_o2_we1;
output  [8:0] imag_o_address0;
output   imag_o_ce0;
output  [31:0] imag_o_d0;
input  [31:0] imag_o_q0;
output   imag_o_we0;
output  [8:0] imag_o_address1;
output   imag_o_ce1;
output  [31:0] imag_o_d1;
input  [31:0] imag_o_q1;
output   imag_o_we1;
output  [8:0] imag_o3_address0;
output   imag_o3_ce0;
output  [31:0] imag_o3_d0;
input  [31:0] imag_o3_q0;
output   imag_o3_we0;
output  [8:0] imag_o3_address1;
output   imag_o3_ce1;
output  [31:0] imag_o3_d1;
input  [31:0] imag_o3_q1;
output   imag_o3_we1;
input   ap_clk;
input   ap_rst;
input   imag_i_pipo_status;
output   imag_i_pipo_update;
input   real_i_pipo_status;
output   real_i_pipo_update;
input   real_i1_pipo_status;
output   real_i1_pipo_update;
input   ap_continue;
output   ap_done;
input   ap_start;
output   ap_idle;
output   ap_ready;

reg ap_idle;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_start;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_done;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_continue;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_idle;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_ready;
wire   [8:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o2_address0;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o2_ce0;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o2_we0;
wire   [31:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o2_d0;
wire   [9:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_address0;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_ce0;
wire   [31:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_q0;
wire   [9:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_address1;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_ce1;
wire   [31:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_q1;
wire   [8:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o3_address0;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o3_ce0;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o3_we0;
wire   [31:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o3_d0;
wire   [8:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o_address0;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o_ce0;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o_we0;
wire   [31:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o_d0;
wire   [8:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o_address0;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o_ce0;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o_we0;
wire   [31:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o_d0;
wire   [8:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_address0;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_ce0;
wire   [31:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_q0;
wire   [8:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_address1;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_ce1;
wire   [31:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_q1;
wire   [8:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_address0;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_ce0;
wire   [31:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_q0;
wire   [8:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_address1;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_ce1;
wire   [31:0] ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_q1;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_pipo_status;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_pipo_update;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_pipo_status;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_pipo_update;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_pipo_status;
wire    ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_pipo_update;
wire    ap_sig_hs_continue;
reg    ap_reg_procdone_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0 = 1'b0;
reg    ap_sig_hs_done;
reg    ap_CS;
wire    ap_sig_top_allready;


ofdm_receiver_bit_reverse20_Loop_rev_proc ofdm_receiver_bit_reverse20_Loop_rev_proc_U0(
    .ap_clk( ap_clk ),
    .ap_rst( ap_rst ),
    .ap_start( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_start ),
    .ap_done( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_done ),
    .ap_continue( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_continue ),
    .ap_idle( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_idle ),
    .ap_ready( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_ready ),
    .real_o2_address0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o2_address0 ),
    .real_o2_ce0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o2_ce0 ),
    .real_o2_we0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o2_we0 ),
    .real_o2_d0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o2_d0 ),
    .imag_i_address0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_address0 ),
    .imag_i_ce0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_ce0 ),
    .imag_i_q0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_q0 ),
    .imag_i_address1( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_address1 ),
    .imag_i_ce1( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_ce1 ),
    .imag_i_q1( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_q1 ),
    .imag_o3_address0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o3_address0 ),
    .imag_o3_ce0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o3_ce0 ),
    .imag_o3_we0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o3_we0 ),
    .imag_o3_d0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o3_d0 ),
    .real_o_address0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o_address0 ),
    .real_o_ce0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o_ce0 ),
    .real_o_we0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o_we0 ),
    .real_o_d0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o_d0 ),
    .imag_o_address0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o_address0 ),
    .imag_o_ce0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o_ce0 ),
    .imag_o_we0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o_we0 ),
    .imag_o_d0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o_d0 ),
    .real_i_address0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_address0 ),
    .real_i_ce0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_ce0 ),
    .real_i_q0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_q0 ),
    .real_i_address1( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_address1 ),
    .real_i_ce1( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_ce1 ),
    .real_i_q1( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_q1 ),
    .real_i1_address0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_address0 ),
    .real_i1_ce0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_ce0 ),
    .real_i1_q0( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_q0 ),
    .real_i1_address1( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_address1 ),
    .real_i1_ce1( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_ce1 ),
    .real_i1_q1( ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_q1 )
);



always @ (posedge ap_clk) begin : ap_ret_ap_reg_procdone_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0
    if (ap_rst == 1'b1) begin
        ap_reg_procdone_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0 <= ap_const_logic_0;
    end else begin
        if ((ap_const_logic_1 == ap_sig_hs_done)) begin
            ap_reg_procdone_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0 <= ap_const_logic_0;
        end else if ((ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_done == ap_const_logic_1)) begin
            ap_reg_procdone_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0 <= ap_const_logic_1;
        end
    end
end

always @ (posedge ap_clk) begin
    ap_CS <= ap_const_logic_0;
end

always @ (ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_idle) begin
    if ((ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_idle == ap_const_logic_1)) begin
        ap_idle = ap_const_logic_1;
    end else begin
        ap_idle = ap_const_logic_0;
    end
end

always @ (ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_done) begin
    if ((ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_done == ap_const_logic_1)) begin
        ap_sig_hs_done = ap_const_logic_1;
    end else begin
        ap_sig_hs_done = ap_const_logic_0;
    end
end

assign ap_done = ap_sig_hs_done;

assign ap_ready = ap_sig_top_allready;

assign ap_sig_hs_continue = ap_continue;

assign ap_sig_top_allready = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_ready;

assign imag_i_address0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_address0;

assign imag_i_address1 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_address1;

assign imag_i_ce0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_ce0;

assign imag_i_ce1 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_ce1;

assign imag_i_d0 = ap_const_lv32_0;

assign imag_i_d1 = ap_const_lv32_0;

assign imag_i_pipo_update = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_pipo_update;

assign imag_i_we0 = ap_const_logic_0;

assign imag_i_we1 = ap_const_logic_0;

assign imag_o3_address0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o3_address0;

assign imag_o3_address1 = ap_const_lv9_0;

assign imag_o3_ce0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o3_ce0;

assign imag_o3_ce1 = ap_const_logic_0;

assign imag_o3_d0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o3_d0;

assign imag_o3_d1 = ap_const_lv32_0;

assign imag_o3_we0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o3_we0;

assign imag_o3_we1 = ap_const_logic_0;

assign imag_o_address0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o_address0;

assign imag_o_address1 = ap_const_lv9_0;

assign imag_o_ce0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o_ce0;

assign imag_o_ce1 = ap_const_logic_0;

assign imag_o_d0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o_d0;

assign imag_o_d1 = ap_const_lv32_0;

assign imag_o_we0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o_we0;

assign imag_o_we1 = ap_const_logic_0;

assign ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_continue = ap_sig_hs_continue;

assign ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_start = ap_start;

assign ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_pipo_status = imag_i_pipo_status;

assign ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_pipo_update = ap_const_logic_0;

assign ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_q0 = imag_i_q0;

assign ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_q1 = imag_i_q1;

assign ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_pipo_status = real_i1_pipo_status;

assign ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_pipo_update = ap_const_logic_0;

assign ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_q0 = real_i1_q0;

assign ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_q1 = real_i1_q1;

assign ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_pipo_status = real_i_pipo_status;

assign ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_pipo_update = ap_const_logic_0;

assign ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_q0 = real_i_q0;

assign ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_q1 = real_i_q1;

assign real_i1_address0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_address0;

assign real_i1_address1 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_address1;

assign real_i1_ce0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_ce0;

assign real_i1_ce1 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_ce1;

assign real_i1_d0 = ap_const_lv32_0;

assign real_i1_d1 = ap_const_lv32_0;

assign real_i1_pipo_update = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_pipo_update;

assign real_i1_we0 = ap_const_logic_0;

assign real_i1_we1 = ap_const_logic_0;

assign real_i_address0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_address0;

assign real_i_address1 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_address1;

assign real_i_ce0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_ce0;

assign real_i_ce1 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_ce1;

assign real_i_d0 = ap_const_lv32_0;

assign real_i_d1 = ap_const_lv32_0;

assign real_i_pipo_update = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_pipo_update;

assign real_i_we0 = ap_const_logic_0;

assign real_i_we1 = ap_const_logic_0;

assign real_o2_address0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o2_address0;

assign real_o2_address1 = ap_const_lv9_0;

assign real_o2_ce0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o2_ce0;

assign real_o2_ce1 = ap_const_logic_0;

assign real_o2_d0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o2_d0;

assign real_o2_d1 = ap_const_lv32_0;

assign real_o2_we0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o2_we0;

assign real_o2_we1 = ap_const_logic_0;

assign real_o_address0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o_address0;

assign real_o_address1 = ap_const_lv9_0;

assign real_o_ce0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o_ce0;

assign real_o_ce1 = ap_const_logic_0;

assign real_o_d0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o_d0;

assign real_o_d1 = ap_const_lv32_0;

assign real_o_we0 = ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o_we0;

assign real_o_we1 = ap_const_logic_0;


endmodule //ofdm_receiver_bit_reverse20

