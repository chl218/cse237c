// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

#ifndef _ofdm_receiver_fft_stage_first_HH_
#define _ofdm_receiver_fft_stage_first_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "ofdm_receiver_faddfsub_32ns_32ns_32_5_full_dsp.h"
#include "ofdm_receiver_fsub_32ns_32ns_32_5_full_dsp.h"
#include "ofdm_receiver_fadd_32ns_32ns_32_5_full_dsp.h"
#include "ofdm_receiver_fmul_32ns_32ns_32_4_max_dsp.h"

namespace ap_rtl {

struct ofdm_receiver_fft_stage_first : public sc_module {
    // Port declarations 45
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_out< sc_lv<9> > real_i_0_address0;
    sc_out< sc_logic > real_i_0_ce0;
    sc_in< sc_lv<32> > real_i_0_q0;
    sc_out< sc_lv<9> > real_i_0_address1;
    sc_out< sc_logic > real_i_0_ce1;
    sc_in< sc_lv<32> > real_i_0_q1;
    sc_out< sc_lv<9> > real_i_1_address0;
    sc_out< sc_logic > real_i_1_ce0;
    sc_in< sc_lv<32> > real_i_1_q0;
    sc_out< sc_lv<9> > real_i_1_address1;
    sc_out< sc_logic > real_i_1_ce1;
    sc_in< sc_lv<32> > real_i_1_q1;
    sc_out< sc_lv<10> > imag_i_address0;
    sc_out< sc_logic > imag_i_ce0;
    sc_in< sc_lv<32> > imag_i_q0;
    sc_out< sc_lv<10> > imag_i_address1;
    sc_out< sc_logic > imag_i_ce1;
    sc_in< sc_lv<32> > imag_i_q1;
    sc_out< sc_lv<9> > real_o_0_address0;
    sc_out< sc_logic > real_o_0_ce0;
    sc_out< sc_logic > real_o_0_we0;
    sc_out< sc_lv<32> > real_o_0_d0;
    sc_out< sc_lv<9> > real_o_0_address1;
    sc_out< sc_logic > real_o_0_ce1;
    sc_out< sc_logic > real_o_0_we1;
    sc_out< sc_lv<32> > real_o_0_d1;
    sc_out< sc_lv<9> > real_o_1_address0;
    sc_out< sc_logic > real_o_1_ce0;
    sc_out< sc_logic > real_o_1_we0;
    sc_out< sc_lv<32> > real_o_1_d0;
    sc_out< sc_lv<10> > imag_o_address0;
    sc_out< sc_logic > imag_o_ce0;
    sc_out< sc_logic > imag_o_we0;
    sc_out< sc_lv<32> > imag_o_d0;
    sc_out< sc_lv<10> > imag_o_address1;
    sc_out< sc_logic > imag_o_ce1;
    sc_out< sc_logic > imag_o_we1;
    sc_out< sc_lv<32> > imag_o_d1;


    // Module declarations
    ofdm_receiver_fft_stage_first(sc_module_name name);
    SC_HAS_PROCESS(ofdm_receiver_fft_stage_first);

    ~ofdm_receiver_fft_stage_first();

    sc_trace_file* mVcdFile;

    ofdm_receiver_faddfsub_32ns_32ns_32_5_full_dsp<1,5,32,32,32>* ofdm_receiver_faddfsub_32ns_32ns_32_5_full_dsp_U16;
    ofdm_receiver_faddfsub_32ns_32ns_32_5_full_dsp<1,5,32,32,32>* ofdm_receiver_faddfsub_32ns_32ns_32_5_full_dsp_U17;
    ofdm_receiver_fsub_32ns_32ns_32_5_full_dsp<1,5,32,32,32>* ofdm_receiver_fsub_32ns_32ns_32_5_full_dsp_U18;
    ofdm_receiver_faddfsub_32ns_32ns_32_5_full_dsp<1,5,32,32,32>* ofdm_receiver_faddfsub_32ns_32ns_32_5_full_dsp_U19;
    ofdm_receiver_faddfsub_32ns_32ns_32_5_full_dsp<1,5,32,32,32>* ofdm_receiver_faddfsub_32ns_32ns_32_5_full_dsp_U20;
    ofdm_receiver_fadd_32ns_32ns_32_5_full_dsp<1,5,32,32,32>* ofdm_receiver_fadd_32ns_32ns_32_5_full_dsp_U21;
    ofdm_receiver_fmul_32ns_32ns_32_4_max_dsp<1,4,32,32,32>* ofdm_receiver_fmul_32ns_32ns_32_4_max_dsp_U22;
    ofdm_receiver_fmul_32ns_32ns_32_4_max_dsp<1,4,32,32,32>* ofdm_receiver_fmul_32ns_32ns_32_4_max_dsp_U23;
    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<6> > ap_CS_fsm;
    sc_signal< sc_logic > ap_sig_cseq_ST_st1_fsm_0;
    sc_signal< bool > ap_sig_bdd_25;
    sc_signal< sc_lv<11> > i_reg_359;
    sc_signal< sc_lv<11> > ap_reg_ppstg_i_reg_359_pp0_it1;
    sc_signal< sc_logic > ap_sig_cseq_ST_pp0_stg0_fsm_1;
    sc_signal< bool > ap_sig_bdd_96;
    sc_signal< sc_logic > ap_reg_ppiten_pp0_it0;
    sc_signal< sc_logic > ap_reg_ppiten_pp0_it1;
    sc_signal< sc_logic > ap_reg_ppiten_pp0_it2;
    sc_signal< sc_logic > ap_reg_ppiten_pp0_it3;
    sc_signal< sc_logic > ap_reg_ppiten_pp0_it4;
    sc_signal< sc_lv<32> > grp_fu_379_p2;
    sc_signal< sc_lv<32> > reg_408;
    sc_signal< sc_logic > ap_sig_cseq_ST_pp0_stg2_fsm_3;
    sc_signal< bool > ap_sig_bdd_115;
    sc_signal< sc_lv<1> > tmp_reg_636;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_reg_636_pp0_it3;
    sc_signal< sc_logic > ap_sig_cseq_ST_pp0_stg3_fsm_4;
    sc_signal< bool > ap_sig_bdd_128;
    sc_signal< sc_logic > ap_sig_cseq_ST_pp0_stg1_fsm_2;
    sc_signal< bool > ap_sig_bdd_141;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_reg_636_pp0_it4;
    sc_signal< sc_lv<32> > grp_fu_383_p2;
    sc_signal< sc_lv<32> > reg_414;
    sc_signal< sc_lv<32> > grp_fu_387_p2;
    sc_signal< sc_lv<32> > reg_420;
    sc_signal< sc_lv<32> > grp_fu_391_p2;
    sc_signal< sc_lv<32> > reg_426;
    sc_signal< sc_lv<32> > reg_433;
    sc_signal< sc_lv<1> > tmp_fu_439_p3;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_reg_636_pp0_it1;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_reg_636_pp0_it2;
    sc_signal< sc_lv<10> > tmp_31_fu_447_p1;
    sc_signal< sc_lv<10> > tmp_31_reg_640;
    sc_signal< sc_lv<10> > ap_reg_ppstg_tmp_31_reg_640_pp0_it1;
    sc_signal< sc_lv<10> > ap_reg_ppstg_tmp_31_reg_640_pp0_it2;
    sc_signal< sc_lv<64> > tmp_s_fu_457_p1;
    sc_signal< sc_lv<64> > tmp_s_reg_649;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_s_reg_649_pp0_it1;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_s_reg_649_pp0_it2;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_s_reg_649_pp0_it3;
    sc_signal< sc_lv<64> > newIndex15_fu_472_p1;
    sc_signal< sc_lv<64> > newIndex15_reg_654;
    sc_signal< sc_lv<64> > ap_reg_ppstg_newIndex15_reg_654_pp0_it1;
    sc_signal< sc_lv<64> > ap_reg_ppstg_newIndex15_reg_654_pp0_it2;
    sc_signal< sc_lv<64> > ap_reg_ppstg_newIndex15_reg_654_pp0_it3;
    sc_signal< sc_lv<64> > tmp_1_fu_483_p1;
    sc_signal< sc_lv<64> > tmp_1_reg_671;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_1_reg_671_pp0_it1;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_1_reg_671_pp0_it2;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_1_reg_671_pp0_it3;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_1_reg_671_pp0_it4;
    sc_signal< sc_lv<64> > newIndex18_fu_498_p1;
    sc_signal< sc_lv<64> > newIndex18_reg_676;
    sc_signal< sc_lv<64> > ap_reg_ppstg_newIndex18_reg_676_pp0_it1;
    sc_signal< sc_lv<64> > ap_reg_ppstg_newIndex18_reg_676_pp0_it2;
    sc_signal< sc_lv<64> > ap_reg_ppstg_newIndex18_reg_676_pp0_it3;
    sc_signal< sc_lv<32> > real_i_1_load_reg_691;
    sc_signal< sc_lv<32> > imag_i_load_reg_697;
    sc_signal< sc_lv<32> > ap_reg_ppstg_imag_i_load_reg_697_pp0_it1;
    sc_signal< sc_lv<32> > real_i_1_load_4_reg_703;
    sc_signal< sc_lv<32> > imag_i_load_1_reg_709;
    sc_signal< sc_lv<32> > ap_reg_ppstg_imag_i_load_1_reg_709_pp0_it1;
    sc_signal< sc_lv<64> > tmp_2_fu_508_p1;
    sc_signal< sc_lv<64> > tmp_2_reg_715;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_2_reg_715_pp0_it1;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_2_reg_715_pp0_it2;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_2_reg_715_pp0_it3;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_2_reg_715_pp0_it4;
    sc_signal< sc_lv<64> > newIndex21_fu_523_p1;
    sc_signal< sc_lv<64> > newIndex21_reg_720;
    sc_signal< sc_lv<64> > ap_reg_ppstg_newIndex21_reg_720_pp0_it1;
    sc_signal< sc_lv<64> > ap_reg_ppstg_newIndex21_reg_720_pp0_it2;
    sc_signal< sc_lv<64> > ap_reg_ppstg_newIndex21_reg_720_pp0_it3;
    sc_signal< sc_lv<64> > tmp_3_fu_533_p1;
    sc_signal< sc_lv<64> > tmp_3_reg_735;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_3_reg_735_pp0_it1;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_3_reg_735_pp0_it2;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_3_reg_735_pp0_it3;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_3_reg_735_pp0_it4;
    sc_signal< sc_lv<64> > newIndex23_fu_548_p1;
    sc_signal< sc_lv<64> > newIndex23_reg_740;
    sc_signal< sc_lv<64> > ap_reg_ppstg_newIndex23_reg_740_pp0_it1;
    sc_signal< sc_lv<64> > ap_reg_ppstg_newIndex23_reg_740_pp0_it2;
    sc_signal< sc_lv<64> > ap_reg_ppstg_newIndex23_reg_740_pp0_it3;
    sc_signal< sc_lv<64> > ap_reg_ppstg_newIndex23_reg_740_pp0_it4;
    sc_signal< sc_lv<32> > real_i_1_load_5_reg_755;
    sc_signal< sc_lv<32> > imag_i_load_2_reg_761;
    sc_signal< sc_lv<32> > ap_reg_ppstg_imag_i_load_2_reg_761_pp0_it1;
    sc_signal< sc_lv<32> > real_i_1_load_6_reg_767;
    sc_signal< sc_lv<32> > ap_reg_ppstg_real_i_1_load_6_reg_767_pp0_it1;
    sc_signal< sc_lv<32> > imag_i_load_3_reg_773;
    sc_signal< sc_lv<32> > ap_reg_ppstg_imag_i_load_3_reg_773_pp0_it1;
    sc_signal< sc_lv<11> > i_3_3_fu_553_p2;
    sc_signal< sc_lv<11> > i_3_3_reg_779;
    sc_signal< sc_lv<32> > grp_fu_395_p2;
    sc_signal< sc_lv<32> > tmp_21_reg_784;
    sc_signal< sc_lv<32> > grp_fu_401_p2;
    sc_signal< sc_lv<32> > tmp_21_1_reg_789;
    sc_signal< sc_lv<32> > tmp_22_reg_794;
    sc_signal< sc_lv<32> > tmp_21_2_reg_799;
    sc_signal< sc_lv<64> > tmp_23_fu_559_p1;
    sc_signal< sc_lv<64> > tmp_23_reg_804;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_23_reg_804_pp0_it2;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_23_reg_804_pp0_it3;
    sc_signal< sc_lv<32> > tmp_22_1_reg_814;
    sc_signal< sc_lv<32> > tmp_22_2_reg_819;
    sc_signal< sc_lv<32> > imag_i_load_8_reg_824;
    sc_signal< sc_lv<64> > tmp_23_2_fu_569_p1;
    sc_signal< sc_lv<64> > tmp_23_2_reg_830;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_23_2_reg_830_pp0_it2;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_23_2_reg_830_pp0_it3;
    sc_signal< sc_lv<9> > newIndex11_reg_835;
    sc_signal< sc_lv<32> > tmp_21_3_reg_845;
    sc_signal< sc_lv<32> > tmp_22_3_reg_850;
    sc_signal< sc_lv<10> > i_3_s_fu_584_p2;
    sc_signal< sc_lv<10> > i_3_s_reg_860;
    sc_signal< sc_lv<64> > newIndex20_fu_599_p1;
    sc_signal< sc_lv<64> > newIndex20_reg_865;
    sc_signal< sc_lv<64> > ap_reg_ppstg_newIndex20_reg_865_pp0_it3;
    sc_signal< sc_lv<32> > imag_i_load_10_reg_875;
    sc_signal< sc_lv<32> > grp_fu_371_p2;
    sc_signal< sc_lv<32> > temp_R_reg_881;
    sc_signal< sc_lv<32> > real_i_0_load_reg_887;
    sc_signal< sc_lv<32> > grp_fu_375_p2;
    sc_signal< sc_lv<32> > temp_R_1_reg_893;
    sc_signal< sc_lv<32> > real_i_0_load_4_reg_899;
    sc_signal< sc_lv<64> > newIndex22_fu_604_p1;
    sc_signal< sc_lv<64> > newIndex22_reg_905;
    sc_signal< sc_lv<64> > ap_reg_ppstg_newIndex22_reg_905_pp0_it3;
    sc_signal< sc_lv<10> > i_3_2_fu_608_p2;
    sc_signal< sc_lv<10> > i_3_2_reg_915;
    sc_signal< sc_lv<64> > newIndex3_fu_623_p1;
    sc_signal< sc_lv<64> > newIndex3_reg_920;
    sc_signal< sc_lv<64> > ap_reg_ppstg_newIndex3_reg_920_pp0_it3;
    sc_signal< sc_lv<64> > ap_reg_ppstg_newIndex3_reg_920_pp0_it4;
    sc_signal< sc_lv<32> > temp_I_reg_930;
    sc_signal< sc_lv<64> > tmp_23_1_fu_628_p1;
    sc_signal< sc_lv<64> > tmp_23_1_reg_936;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_23_1_reg_936_pp0_it3;
    sc_signal< sc_lv<32> > temp_R_2_reg_946;
    sc_signal< sc_lv<32> > real_i_0_load_5_reg_952;
    sc_signal< sc_lv<32> > real_i_0_load_6_reg_958;
    sc_signal< sc_lv<32> > temp_I_1_reg_964;
    sc_signal< sc_lv<32> > imag_i_load_9_reg_970;
    sc_signal< sc_lv<32> > temp_I_2_reg_976;
    sc_signal< sc_lv<64> > tmp_23_3_fu_632_p1;
    sc_signal< sc_lv<64> > tmp_23_3_reg_982;
    sc_signal< sc_lv<64> > ap_reg_ppstg_tmp_23_3_reg_982_pp0_it3;
    sc_signal< sc_lv<32> > temp_R_3_reg_992;
    sc_signal< sc_lv<32> > temp_I_3_reg_998;
    sc_signal< sc_lv<32> > imag_i_load_11_reg_1004;
    sc_signal< sc_lv<32> > tmp_25_3_reg_1010;
    sc_signal< sc_lv<32> > tmp_27_3_reg_1015;
    sc_signal< bool > ap_sig_bdd_435;
    sc_signal< sc_lv<11> > i_phi_fu_363_p4;
    sc_signal< sc_lv<32> > grp_fu_371_p0;
    sc_signal< sc_lv<32> > grp_fu_371_p1;
    sc_signal< sc_lv<32> > grp_fu_375_p0;
    sc_signal< sc_lv<32> > grp_fu_375_p1;
    sc_signal< sc_lv<32> > grp_fu_379_p0;
    sc_signal< sc_lv<32> > grp_fu_379_p1;
    sc_signal< sc_lv<32> > grp_fu_383_p0;
    sc_signal< sc_lv<32> > grp_fu_383_p1;
    sc_signal< sc_lv<32> > grp_fu_387_p0;
    sc_signal< sc_lv<32> > grp_fu_387_p1;
    sc_signal< sc_lv<32> > grp_fu_391_p0;
    sc_signal< sc_lv<32> > grp_fu_391_p1;
    sc_signal< sc_lv<32> > grp_fu_395_p0;
    sc_signal< sc_lv<32> > grp_fu_401_p0;
    sc_signal< sc_lv<10> > i_lower_fu_451_p2;
    sc_signal< sc_lv<10> > newIndex14_fu_462_p4;
    sc_signal< sc_lv<10> > i_lower_1_fu_477_p2;
    sc_signal< sc_lv<9> > newIndex17_fu_488_p4;
    sc_signal< sc_lv<10> > i_lower_2_fu_503_p2;
    sc_signal< sc_lv<9> > newIndex16_fu_513_p4;
    sc_signal< sc_lv<10> > i_lower_3_fu_528_p2;
    sc_signal< sc_lv<9> > newIndex6_fu_538_p4;
    sc_signal< sc_lv<10> > i_3_1_fu_564_p2;
    sc_signal< sc_lv<9> > newIndex19_fu_589_p4;
    sc_signal< sc_lv<9> > newIndex_fu_613_p4;
    sc_signal< sc_lv<2> > grp_fu_371_opcode;
    sc_signal< sc_logic > grp_fu_371_ce;
    sc_signal< sc_lv<2> > grp_fu_375_opcode;
    sc_signal< sc_logic > grp_fu_375_ce;
    sc_signal< sc_logic > grp_fu_379_ce;
    sc_signal< sc_lv<2> > grp_fu_383_opcode;
    sc_signal< sc_logic > grp_fu_383_ce;
    sc_signal< sc_lv<2> > grp_fu_387_opcode;
    sc_signal< sc_logic > grp_fu_387_ce;
    sc_signal< sc_logic > grp_fu_391_ce;
    sc_signal< sc_logic > grp_fu_395_ce;
    sc_signal< sc_logic > grp_fu_401_ce;
    sc_signal< sc_logic > ap_sig_cseq_ST_st22_fsm_5;
    sc_signal< bool > ap_sig_bdd_657;
    sc_signal< sc_lv<6> > ap_NS_fsm;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<6> ap_ST_st1_fsm_0;
    static const sc_lv<6> ap_ST_pp0_stg0_fsm_1;
    static const sc_lv<6> ap_ST_pp0_stg1_fsm_2;
    static const sc_lv<6> ap_ST_pp0_stg2_fsm_3;
    static const sc_lv<6> ap_ST_pp0_stg3_fsm_4;
    static const sc_lv<6> ap_ST_st22_fsm_5;
    static const sc_lv<32> ap_const_lv32_0;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<32> ap_const_lv32_1;
    static const sc_lv<32> ap_const_lv32_3;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<32> ap_const_lv32_4;
    static const sc_lv<32> ap_const_lv32_2;
    static const sc_lv<11> ap_const_lv11_0;
    static const sc_lv<32> ap_const_lv32_80000000;
    static const sc_lv<32> ap_const_lv32_A;
    static const sc_lv<10> ap_const_lv10_1;
    static const sc_lv<10> ap_const_lv10_3;
    static const sc_lv<32> ap_const_lv32_9;
    static const sc_lv<10> ap_const_lv10_5;
    static const sc_lv<10> ap_const_lv10_7;
    static const sc_lv<11> ap_const_lv11_8;
    static const sc_lv<10> ap_const_lv10_4;
    static const sc_lv<10> ap_const_lv10_2;
    static const sc_lv<10> ap_const_lv10_6;
    static const sc_lv<2> ap_const_lv2_0;
    static const sc_lv<2> ap_const_lv2_1;
    static const sc_lv<32> ap_const_lv32_5;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_ap_done();
    void thread_ap_idle();
    void thread_ap_ready();
    void thread_ap_sig_bdd_115();
    void thread_ap_sig_bdd_128();
    void thread_ap_sig_bdd_141();
    void thread_ap_sig_bdd_25();
    void thread_ap_sig_bdd_435();
    void thread_ap_sig_bdd_657();
    void thread_ap_sig_bdd_96();
    void thread_ap_sig_cseq_ST_pp0_stg0_fsm_1();
    void thread_ap_sig_cseq_ST_pp0_stg1_fsm_2();
    void thread_ap_sig_cseq_ST_pp0_stg2_fsm_3();
    void thread_ap_sig_cseq_ST_pp0_stg3_fsm_4();
    void thread_ap_sig_cseq_ST_st1_fsm_0();
    void thread_ap_sig_cseq_ST_st22_fsm_5();
    void thread_grp_fu_371_ce();
    void thread_grp_fu_371_opcode();
    void thread_grp_fu_371_p0();
    void thread_grp_fu_371_p1();
    void thread_grp_fu_375_ce();
    void thread_grp_fu_375_opcode();
    void thread_grp_fu_375_p0();
    void thread_grp_fu_375_p1();
    void thread_grp_fu_379_ce();
    void thread_grp_fu_379_p0();
    void thread_grp_fu_379_p1();
    void thread_grp_fu_383_ce();
    void thread_grp_fu_383_opcode();
    void thread_grp_fu_383_p0();
    void thread_grp_fu_383_p1();
    void thread_grp_fu_387_ce();
    void thread_grp_fu_387_opcode();
    void thread_grp_fu_387_p0();
    void thread_grp_fu_387_p1();
    void thread_grp_fu_391_ce();
    void thread_grp_fu_391_p0();
    void thread_grp_fu_391_p1();
    void thread_grp_fu_395_ce();
    void thread_grp_fu_395_p0();
    void thread_grp_fu_401_ce();
    void thread_grp_fu_401_p0();
    void thread_i_3_1_fu_564_p2();
    void thread_i_3_2_fu_608_p2();
    void thread_i_3_3_fu_553_p2();
    void thread_i_3_s_fu_584_p2();
    void thread_i_lower_1_fu_477_p2();
    void thread_i_lower_2_fu_503_p2();
    void thread_i_lower_3_fu_528_p2();
    void thread_i_lower_fu_451_p2();
    void thread_i_phi_fu_363_p4();
    void thread_imag_i_address0();
    void thread_imag_i_address1();
    void thread_imag_i_ce0();
    void thread_imag_i_ce1();
    void thread_imag_o_address0();
    void thread_imag_o_address1();
    void thread_imag_o_ce0();
    void thread_imag_o_ce1();
    void thread_imag_o_d0();
    void thread_imag_o_d1();
    void thread_imag_o_we0();
    void thread_imag_o_we1();
    void thread_newIndex14_fu_462_p4();
    void thread_newIndex15_fu_472_p1();
    void thread_newIndex16_fu_513_p4();
    void thread_newIndex17_fu_488_p4();
    void thread_newIndex18_fu_498_p1();
    void thread_newIndex19_fu_589_p4();
    void thread_newIndex20_fu_599_p1();
    void thread_newIndex21_fu_523_p1();
    void thread_newIndex22_fu_604_p1();
    void thread_newIndex23_fu_548_p1();
    void thread_newIndex3_fu_623_p1();
    void thread_newIndex6_fu_538_p4();
    void thread_newIndex_fu_613_p4();
    void thread_real_i_0_address0();
    void thread_real_i_0_address1();
    void thread_real_i_0_ce0();
    void thread_real_i_0_ce1();
    void thread_real_i_1_address0();
    void thread_real_i_1_address1();
    void thread_real_i_1_ce0();
    void thread_real_i_1_ce1();
    void thread_real_o_0_address0();
    void thread_real_o_0_address1();
    void thread_real_o_0_ce0();
    void thread_real_o_0_ce1();
    void thread_real_o_0_d0();
    void thread_real_o_0_d1();
    void thread_real_o_0_we0();
    void thread_real_o_0_we1();
    void thread_real_o_1_address0();
    void thread_real_o_1_ce0();
    void thread_real_o_1_d0();
    void thread_real_o_1_we0();
    void thread_tmp_1_fu_483_p1();
    void thread_tmp_23_1_fu_628_p1();
    void thread_tmp_23_2_fu_569_p1();
    void thread_tmp_23_3_fu_632_p1();
    void thread_tmp_23_fu_559_p1();
    void thread_tmp_2_fu_508_p1();
    void thread_tmp_31_fu_447_p1();
    void thread_tmp_3_fu_533_p1();
    void thread_tmp_fu_439_p3();
    void thread_tmp_s_fu_457_p1();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
