// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

#ifndef _ofdm_receiver_bit_reverse20_HH_
#define _ofdm_receiver_bit_reverse20_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "ofdm_receiver_bit_reverse20_Loop_rev_proc.h"

namespace ap_rtl {

struct ofdm_receiver_bit_reverse20 : public sc_module {
    // Port declarations 83
    sc_out< sc_lv<9> > real_i_address0;
    sc_out< sc_logic > real_i_ce0;
    sc_out< sc_lv<32> > real_i_d0;
    sc_in< sc_lv<32> > real_i_q0;
    sc_out< sc_logic > real_i_we0;
    sc_out< sc_lv<9> > real_i_address1;
    sc_out< sc_logic > real_i_ce1;
    sc_out< sc_lv<32> > real_i_d1;
    sc_in< sc_lv<32> > real_i_q1;
    sc_out< sc_logic > real_i_we1;
    sc_out< sc_lv<9> > real_i1_address0;
    sc_out< sc_logic > real_i1_ce0;
    sc_out< sc_lv<32> > real_i1_d0;
    sc_in< sc_lv<32> > real_i1_q0;
    sc_out< sc_logic > real_i1_we0;
    sc_out< sc_lv<9> > real_i1_address1;
    sc_out< sc_logic > real_i1_ce1;
    sc_out< sc_lv<32> > real_i1_d1;
    sc_in< sc_lv<32> > real_i1_q1;
    sc_out< sc_logic > real_i1_we1;
    sc_out< sc_lv<10> > imag_i_address0;
    sc_out< sc_logic > imag_i_ce0;
    sc_out< sc_lv<32> > imag_i_d0;
    sc_in< sc_lv<32> > imag_i_q0;
    sc_out< sc_logic > imag_i_we0;
    sc_out< sc_lv<10> > imag_i_address1;
    sc_out< sc_logic > imag_i_ce1;
    sc_out< sc_lv<32> > imag_i_d1;
    sc_in< sc_lv<32> > imag_i_q1;
    sc_out< sc_logic > imag_i_we1;
    sc_out< sc_lv<9> > real_o_address0;
    sc_out< sc_logic > real_o_ce0;
    sc_out< sc_lv<32> > real_o_d0;
    sc_in< sc_lv<32> > real_o_q0;
    sc_out< sc_logic > real_o_we0;
    sc_out< sc_lv<9> > real_o_address1;
    sc_out< sc_logic > real_o_ce1;
    sc_out< sc_lv<32> > real_o_d1;
    sc_in< sc_lv<32> > real_o_q1;
    sc_out< sc_logic > real_o_we1;
    sc_out< sc_lv<9> > real_o2_address0;
    sc_out< sc_logic > real_o2_ce0;
    sc_out< sc_lv<32> > real_o2_d0;
    sc_in< sc_lv<32> > real_o2_q0;
    sc_out< sc_logic > real_o2_we0;
    sc_out< sc_lv<9> > real_o2_address1;
    sc_out< sc_logic > real_o2_ce1;
    sc_out< sc_lv<32> > real_o2_d1;
    sc_in< sc_lv<32> > real_o2_q1;
    sc_out< sc_logic > real_o2_we1;
    sc_out< sc_lv<9> > imag_o_address0;
    sc_out< sc_logic > imag_o_ce0;
    sc_out< sc_lv<32> > imag_o_d0;
    sc_in< sc_lv<32> > imag_o_q0;
    sc_out< sc_logic > imag_o_we0;
    sc_out< sc_lv<9> > imag_o_address1;
    sc_out< sc_logic > imag_o_ce1;
    sc_out< sc_lv<32> > imag_o_d1;
    sc_in< sc_lv<32> > imag_o_q1;
    sc_out< sc_logic > imag_o_we1;
    sc_out< sc_lv<9> > imag_o3_address0;
    sc_out< sc_logic > imag_o3_ce0;
    sc_out< sc_lv<32> > imag_o3_d0;
    sc_in< sc_lv<32> > imag_o3_q0;
    sc_out< sc_logic > imag_o3_we0;
    sc_out< sc_lv<9> > imag_o3_address1;
    sc_out< sc_logic > imag_o3_ce1;
    sc_out< sc_lv<32> > imag_o3_d1;
    sc_in< sc_lv<32> > imag_o3_q1;
    sc_out< sc_logic > imag_o3_we1;
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > imag_i_pipo_status;
    sc_out< sc_logic > imag_i_pipo_update;
    sc_in< sc_logic > real_i_pipo_status;
    sc_out< sc_logic > real_i_pipo_update;
    sc_in< sc_logic > real_i1_pipo_status;
    sc_out< sc_logic > real_i1_pipo_update;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;


    // Module declarations
    ofdm_receiver_bit_reverse20(sc_module_name name);
    SC_HAS_PROCESS(ofdm_receiver_bit_reverse20);

    ~ofdm_receiver_bit_reverse20();

    sc_trace_file* mVcdFile;

    ofdm_receiver_bit_reverse20_Loop_rev_proc* ofdm_receiver_bit_reverse20_Loop_rev_proc_U0;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_start;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_done;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_continue;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_idle;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_ready;
    sc_signal< sc_lv<9> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o2_address0;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o2_ce0;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o2_we0;
    sc_signal< sc_lv<32> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o2_d0;
    sc_signal< sc_lv<10> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_address0;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_ce0;
    sc_signal< sc_lv<32> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_q0;
    sc_signal< sc_lv<10> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_address1;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_ce1;
    sc_signal< sc_lv<32> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_q1;
    sc_signal< sc_lv<9> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o3_address0;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o3_ce0;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o3_we0;
    sc_signal< sc_lv<32> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o3_d0;
    sc_signal< sc_lv<9> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o_address0;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o_ce0;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o_we0;
    sc_signal< sc_lv<32> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_o_d0;
    sc_signal< sc_lv<9> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o_address0;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o_ce0;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o_we0;
    sc_signal< sc_lv<32> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_o_d0;
    sc_signal< sc_lv<9> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_address0;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_ce0;
    sc_signal< sc_lv<32> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_q0;
    sc_signal< sc_lv<9> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_address1;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_ce1;
    sc_signal< sc_lv<32> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_q1;
    sc_signal< sc_lv<9> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_address0;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_ce0;
    sc_signal< sc_lv<32> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_q0;
    sc_signal< sc_lv<9> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_address1;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_ce1;
    sc_signal< sc_lv<32> > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_q1;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_pipo_status;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_pipo_update;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_pipo_status;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_pipo_update;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_pipo_status;
    sc_signal< sc_logic > ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_pipo_update;
    sc_signal< sc_logic > ap_sig_hs_continue;
    sc_signal< sc_logic > ap_reg_procdone_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0;
    sc_signal< sc_logic > ap_sig_hs_done;
    sc_signal< sc_logic > ap_CS;
    sc_signal< sc_logic > ap_sig_top_allready;
    static const sc_lv<9> ap_const_lv9_0;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<32> ap_const_lv32_0;
    static const sc_lv<10> ap_const_lv10_0;
    static const sc_logic ap_const_logic_1;
    static const bool ap_true;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_ap_done();
    void thread_ap_idle();
    void thread_ap_ready();
    void thread_ap_sig_hs_continue();
    void thread_ap_sig_hs_done();
    void thread_ap_sig_top_allready();
    void thread_imag_i_address0();
    void thread_imag_i_address1();
    void thread_imag_i_ce0();
    void thread_imag_i_ce1();
    void thread_imag_i_d0();
    void thread_imag_i_d1();
    void thread_imag_i_pipo_update();
    void thread_imag_i_we0();
    void thread_imag_i_we1();
    void thread_imag_o3_address0();
    void thread_imag_o3_address1();
    void thread_imag_o3_ce0();
    void thread_imag_o3_ce1();
    void thread_imag_o3_d0();
    void thread_imag_o3_d1();
    void thread_imag_o3_we0();
    void thread_imag_o3_we1();
    void thread_imag_o_address0();
    void thread_imag_o_address1();
    void thread_imag_o_ce0();
    void thread_imag_o_ce1();
    void thread_imag_o_d0();
    void thread_imag_o_d1();
    void thread_imag_o_we0();
    void thread_imag_o_we1();
    void thread_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_continue();
    void thread_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_ap_start();
    void thread_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_pipo_status();
    void thread_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_pipo_update();
    void thread_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_q0();
    void thread_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_imag_i_q1();
    void thread_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_pipo_status();
    void thread_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_pipo_update();
    void thread_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_q0();
    void thread_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i1_q1();
    void thread_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_pipo_status();
    void thread_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_pipo_update();
    void thread_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_q0();
    void thread_ofdm_receiver_bit_reverse20_Loop_rev_proc_U0_real_i_q1();
    void thread_real_i1_address0();
    void thread_real_i1_address1();
    void thread_real_i1_ce0();
    void thread_real_i1_ce1();
    void thread_real_i1_d0();
    void thread_real_i1_d1();
    void thread_real_i1_pipo_update();
    void thread_real_i1_we0();
    void thread_real_i1_we1();
    void thread_real_i_address0();
    void thread_real_i_address1();
    void thread_real_i_ce0();
    void thread_real_i_ce1();
    void thread_real_i_d0();
    void thread_real_i_d1();
    void thread_real_i_pipo_update();
    void thread_real_i_we0();
    void thread_real_i_we1();
    void thread_real_o2_address0();
    void thread_real_o2_address1();
    void thread_real_o2_ce0();
    void thread_real_o2_ce1();
    void thread_real_o2_d0();
    void thread_real_o2_d1();
    void thread_real_o2_we0();
    void thread_real_o2_we1();
    void thread_real_o_address0();
    void thread_real_o_address1();
    void thread_real_o_ce0();
    void thread_real_o_ce1();
    void thread_real_o_d0();
    void thread_real_o_d1();
    void thread_real_o_we0();
    void thread_real_o_we1();
};

}

using namespace ap_rtl;

#endif
