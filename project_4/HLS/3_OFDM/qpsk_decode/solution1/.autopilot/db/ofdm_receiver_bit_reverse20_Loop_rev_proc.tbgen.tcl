set moduleName ofdm_receiver_bit_reverse20_Loop_rev_proc
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {ofdm_receiver_bit_reverse20_Loop_rev_proc}
set C_modelType { void 0 }
set C_modelArgList { 
	{ real_o2 float 32 regular {array 512 { 0 3 } 0 1 }  }
	{ imag_i float 32 regular {array 1024 { 1 1 } 1 1 }  }
	{ imag_o3 float 32 regular {array 512 { 0 3 } 0 1 }  }
	{ real_o float 32 regular {array 512 { 0 3 } 0 1 }  }
	{ imag_o float 32 regular {array 512 { 0 3 } 0 1 }  }
	{ real_i float 32 regular {array 512 { 1 1 } 1 1 }  }
	{ real_i1 float 32 regular {array 512 { 1 1 } 1 1 }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "real_o2", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "imag_i", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_o3", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "real_o", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "imag_o", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "real_i", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} ]}
# RTL Port declarations: 
set portNum 41
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ real_o2_address0 sc_out sc_lv 9 signal 0 } 
	{ real_o2_ce0 sc_out sc_logic 1 signal 0 } 
	{ real_o2_we0 sc_out sc_logic 1 signal 0 } 
	{ real_o2_d0 sc_out sc_lv 32 signal 0 } 
	{ imag_i_address0 sc_out sc_lv 10 signal 1 } 
	{ imag_i_ce0 sc_out sc_logic 1 signal 1 } 
	{ imag_i_q0 sc_in sc_lv 32 signal 1 } 
	{ imag_i_address1 sc_out sc_lv 10 signal 1 } 
	{ imag_i_ce1 sc_out sc_logic 1 signal 1 } 
	{ imag_i_q1 sc_in sc_lv 32 signal 1 } 
	{ imag_o3_address0 sc_out sc_lv 9 signal 2 } 
	{ imag_o3_ce0 sc_out sc_logic 1 signal 2 } 
	{ imag_o3_we0 sc_out sc_logic 1 signal 2 } 
	{ imag_o3_d0 sc_out sc_lv 32 signal 2 } 
	{ real_o_address0 sc_out sc_lv 9 signal 3 } 
	{ real_o_ce0 sc_out sc_logic 1 signal 3 } 
	{ real_o_we0 sc_out sc_logic 1 signal 3 } 
	{ real_o_d0 sc_out sc_lv 32 signal 3 } 
	{ imag_o_address0 sc_out sc_lv 9 signal 4 } 
	{ imag_o_ce0 sc_out sc_logic 1 signal 4 } 
	{ imag_o_we0 sc_out sc_logic 1 signal 4 } 
	{ imag_o_d0 sc_out sc_lv 32 signal 4 } 
	{ real_i_address0 sc_out sc_lv 9 signal 5 } 
	{ real_i_ce0 sc_out sc_logic 1 signal 5 } 
	{ real_i_q0 sc_in sc_lv 32 signal 5 } 
	{ real_i_address1 sc_out sc_lv 9 signal 5 } 
	{ real_i_ce1 sc_out sc_logic 1 signal 5 } 
	{ real_i_q1 sc_in sc_lv 32 signal 5 } 
	{ real_i1_address0 sc_out sc_lv 9 signal 6 } 
	{ real_i1_ce0 sc_out sc_logic 1 signal 6 } 
	{ real_i1_q0 sc_in sc_lv 32 signal 6 } 
	{ real_i1_address1 sc_out sc_lv 9 signal 6 } 
	{ real_i1_ce1 sc_out sc_logic 1 signal 6 } 
	{ real_i1_q1 sc_in sc_lv 32 signal 6 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "real_o2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "real_o2", "role": "address0" }} , 
 	{ "name": "real_o2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o2", "role": "ce0" }} , 
 	{ "name": "real_o2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o2", "role": "we0" }} , 
 	{ "name": "real_o2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o2", "role": "d0" }} , 
 	{ "name": "imag_i_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":10, "type": "signal", "bundle":{"name": "imag_i", "role": "address0" }} , 
 	{ "name": "imag_i_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i", "role": "ce0" }} , 
 	{ "name": "imag_i_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i", "role": "q0" }} , 
 	{ "name": "imag_i_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":10, "type": "signal", "bundle":{"name": "imag_i", "role": "address1" }} , 
 	{ "name": "imag_i_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i", "role": "ce1" }} , 
 	{ "name": "imag_i_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i", "role": "q1" }} , 
 	{ "name": "imag_o3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "imag_o3", "role": "address0" }} , 
 	{ "name": "imag_o3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o3", "role": "ce0" }} , 
 	{ "name": "imag_o3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o3", "role": "we0" }} , 
 	{ "name": "imag_o3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o3", "role": "d0" }} , 
 	{ "name": "real_o_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "real_o", "role": "address0" }} , 
 	{ "name": "real_o_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o", "role": "ce0" }} , 
 	{ "name": "real_o_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o", "role": "we0" }} , 
 	{ "name": "real_o_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o", "role": "d0" }} , 
 	{ "name": "imag_o_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "imag_o", "role": "address0" }} , 
 	{ "name": "imag_o_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o", "role": "ce0" }} , 
 	{ "name": "imag_o_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o", "role": "we0" }} , 
 	{ "name": "imag_o_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o", "role": "d0" }} , 
 	{ "name": "real_i_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "real_i", "role": "address0" }} , 
 	{ "name": "real_i_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i", "role": "ce0" }} , 
 	{ "name": "real_i_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i", "role": "q0" }} , 
 	{ "name": "real_i_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "real_i", "role": "address1" }} , 
 	{ "name": "real_i_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i", "role": "ce1" }} , 
 	{ "name": "real_i_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i", "role": "q1" }} , 
 	{ "name": "real_i1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "real_i1", "role": "address0" }} , 
 	{ "name": "real_i1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i1", "role": "ce0" }} , 
 	{ "name": "real_i1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i1", "role": "q0" }} , 
 	{ "name": "real_i1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "real_i1", "role": "address1" }} , 
 	{ "name": "real_i1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i1", "role": "ce1" }} , 
 	{ "name": "real_i1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i1", "role": "q1" }}  ]}
set Spec2ImplPortList { 
	real_o2 { ap_memory {  { real_o2_address0 mem_address 1 9 }  { real_o2_ce0 mem_ce 1 1 }  { real_o2_we0 mem_we 1 1 }  { real_o2_d0 mem_din 1 32 } } }
	imag_i { ap_memory {  { imag_i_address0 mem_address 1 10 }  { imag_i_ce0 mem_ce 1 1 }  { imag_i_q0 mem_dout 0 32 }  { imag_i_address1 mem_address 1 10 }  { imag_i_ce1 mem_ce 1 1 }  { imag_i_q1 mem_dout 0 32 } } }
	imag_o3 { ap_memory {  { imag_o3_address0 mem_address 1 9 }  { imag_o3_ce0 mem_ce 1 1 }  { imag_o3_we0 mem_we 1 1 }  { imag_o3_d0 mem_din 1 32 } } }
	real_o { ap_memory {  { real_o_address0 mem_address 1 9 }  { real_o_ce0 mem_ce 1 1 }  { real_o_we0 mem_we 1 1 }  { real_o_d0 mem_din 1 32 } } }
	imag_o { ap_memory {  { imag_o_address0 mem_address 1 9 }  { imag_o_ce0 mem_ce 1 1 }  { imag_o_we0 mem_we 1 1 }  { imag_o_d0 mem_din 1 32 } } }
	real_i { ap_memory {  { real_i_address0 mem_address 1 9 }  { real_i_ce0 mem_ce 1 1 }  { real_i_q0 mem_dout 0 32 }  { real_i_address1 mem_address 1 9 }  { real_i_ce1 mem_ce 1 1 }  { real_i_q1 mem_dout 0 32 } } }
	real_i1 { ap_memory {  { real_i1_address0 mem_address 1 9 }  { real_i1_ce0 mem_ce 1 1 }  { real_i1_q0 mem_dout 0 32 }  { real_i1_address1 mem_address 1 9 }  { real_i1_ce1 mem_ce 1 1 }  { real_i1_q1 mem_dout 0 32 } } }
}
