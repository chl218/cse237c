set C_TypeInfoList {{ 
"ofdm_receiver" : [[], { "return": [[], "void"]} , [{"ExternC" : 0}], [ {"inptr": [[],{ "pointer": "0"}] }, {"outptr": [[],{ "pointer": "1"}] }],[],""], 
"0": [ "DTYPE", {"typedef": [[[], {"scalar": "float"}],""]}], 
"1": [ "uint32_t", {"typedef": [[[], {"scalar": "unsigned int"}],""]}]
}}
set moduleName ofdm_receiver
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_none
set isOneStateSeq 0
set C_modelName {ofdm_receiver}
set C_modelType { void 0 }
set C_modelArgList { 
	{ inptr float 32 regular {fifo 0 volatile }  }
	{ outptr int 32 regular {fifo 1 volatile }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "inptr", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "inptr","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "outptr", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "outptr","cData": "unsigned int","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} ]}
# RTL Port declarations: 
set portNum 8
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ inptr_dout sc_in sc_lv 32 signal 0 } 
	{ inptr_empty_n sc_in sc_logic 1 signal 0 } 
	{ inptr_read sc_out sc_logic 1 signal 0 } 
	{ outptr_din sc_out sc_lv 32 signal 1 } 
	{ outptr_full_n sc_in sc_logic 1 signal 1 } 
	{ outptr_write sc_out sc_logic 1 signal 1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "inptr_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "inptr", "role": "dout" }} , 
 	{ "name": "inptr_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "inptr", "role": "empty_n" }} , 
 	{ "name": "inptr_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "inptr", "role": "read" }} , 
 	{ "name": "outptr_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "outptr", "role": "din" }} , 
 	{ "name": "outptr_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "outptr", "role": "full_n" }} , 
 	{ "name": "outptr_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "outptr", "role": "write" }}  ]}
set Spec2ImplPortList { 
	inptr { ap_fifo {  { inptr_dout fifo_data 0 32 }  { inptr_empty_n fifo_status 0 1 }  { inptr_read fifo_update 1 1 } } }
	outptr { ap_fifo {  { outptr_din fifo_data 1 32 }  { outptr_full_n fifo_status 0 1 }  { outptr_write fifo_update 1 1 } } }
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
	inptr { fifo_read 2 no_conditional }
	outptr { fifo_write 1 no_conditional }
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
