set moduleName ofdm_receiver_fft
set isCombinational 0
set isDatapathOnly 0
set isPipelined 1
set pipeline_type dataflow
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {ofdm_receiver_fft}
set C_modelType { void 0 }
set C_modelArgList { 
	{ real_i_0 float 32 regular {array 512 { 1 1 } 1 1 }  }
	{ real_i_1 float 32 regular {array 512 { 1 1 } 1 1 }  }
	{ imag_i_0 float 32 regular {array 512 { 1 1 } 1 1 }  }
	{ imag_i_1 float 32 regular {array 512 { 1 1 } 1 1 }  }
	{ real_o_0 float 32 regular {array 512 { 0 3 } 0 1 }  }
	{ real_o_1 float 32 regular {array 512 { 0 3 } 0 1 }  }
	{ imag_o_0 float 32 regular {array 512 { 0 3 } 0 1 }  }
	{ imag_o_1 float 32 regular {array 512 { 0 3 } 0 1 }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "real_i_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_o_0", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "real_o_1", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "imag_o_0", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "imag_o_1", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} ]}
# RTL Port declarations: 
set portNum 86
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ real_i_0_address0 sc_out sc_lv 9 signal 0 } 
	{ real_i_0_ce0 sc_out sc_logic 1 signal 0 } 
	{ real_i_0_d0 sc_out sc_lv 32 signal 0 } 
	{ real_i_0_q0 sc_in sc_lv 32 signal 0 } 
	{ real_i_0_we0 sc_out sc_logic 1 signal 0 } 
	{ real_i_0_address1 sc_out sc_lv 9 signal 0 } 
	{ real_i_0_ce1 sc_out sc_logic 1 signal 0 } 
	{ real_i_0_d1 sc_out sc_lv 32 signal 0 } 
	{ real_i_0_q1 sc_in sc_lv 32 signal 0 } 
	{ real_i_0_we1 sc_out sc_logic 1 signal 0 } 
	{ real_i_1_address0 sc_out sc_lv 9 signal 1 } 
	{ real_i_1_ce0 sc_out sc_logic 1 signal 1 } 
	{ real_i_1_d0 sc_out sc_lv 32 signal 1 } 
	{ real_i_1_q0 sc_in sc_lv 32 signal 1 } 
	{ real_i_1_we0 sc_out sc_logic 1 signal 1 } 
	{ real_i_1_address1 sc_out sc_lv 9 signal 1 } 
	{ real_i_1_ce1 sc_out sc_logic 1 signal 1 } 
	{ real_i_1_d1 sc_out sc_lv 32 signal 1 } 
	{ real_i_1_q1 sc_in sc_lv 32 signal 1 } 
	{ real_i_1_we1 sc_out sc_logic 1 signal 1 } 
	{ imag_i_0_address0 sc_out sc_lv 9 signal 2 } 
	{ imag_i_0_ce0 sc_out sc_logic 1 signal 2 } 
	{ imag_i_0_d0 sc_out sc_lv 32 signal 2 } 
	{ imag_i_0_q0 sc_in sc_lv 32 signal 2 } 
	{ imag_i_0_we0 sc_out sc_logic 1 signal 2 } 
	{ imag_i_0_address1 sc_out sc_lv 9 signal 2 } 
	{ imag_i_0_ce1 sc_out sc_logic 1 signal 2 } 
	{ imag_i_0_d1 sc_out sc_lv 32 signal 2 } 
	{ imag_i_0_q1 sc_in sc_lv 32 signal 2 } 
	{ imag_i_0_we1 sc_out sc_logic 1 signal 2 } 
	{ imag_i_1_address0 sc_out sc_lv 9 signal 3 } 
	{ imag_i_1_ce0 sc_out sc_logic 1 signal 3 } 
	{ imag_i_1_d0 sc_out sc_lv 32 signal 3 } 
	{ imag_i_1_q0 sc_in sc_lv 32 signal 3 } 
	{ imag_i_1_we0 sc_out sc_logic 1 signal 3 } 
	{ imag_i_1_address1 sc_out sc_lv 9 signal 3 } 
	{ imag_i_1_ce1 sc_out sc_logic 1 signal 3 } 
	{ imag_i_1_d1 sc_out sc_lv 32 signal 3 } 
	{ imag_i_1_q1 sc_in sc_lv 32 signal 3 } 
	{ imag_i_1_we1 sc_out sc_logic 1 signal 3 } 
	{ real_o_0_address0 sc_out sc_lv 9 signal 4 } 
	{ real_o_0_ce0 sc_out sc_logic 1 signal 4 } 
	{ real_o_0_d0 sc_out sc_lv 32 signal 4 } 
	{ real_o_0_q0 sc_in sc_lv 32 signal 4 } 
	{ real_o_0_we0 sc_out sc_logic 1 signal 4 } 
	{ real_o_0_address1 sc_out sc_lv 9 signal 4 } 
	{ real_o_0_ce1 sc_out sc_logic 1 signal 4 } 
	{ real_o_0_d1 sc_out sc_lv 32 signal 4 } 
	{ real_o_0_q1 sc_in sc_lv 32 signal 4 } 
	{ real_o_0_we1 sc_out sc_logic 1 signal 4 } 
	{ real_o_1_address0 sc_out sc_lv 9 signal 5 } 
	{ real_o_1_ce0 sc_out sc_logic 1 signal 5 } 
	{ real_o_1_d0 sc_out sc_lv 32 signal 5 } 
	{ real_o_1_q0 sc_in sc_lv 32 signal 5 } 
	{ real_o_1_we0 sc_out sc_logic 1 signal 5 } 
	{ real_o_1_address1 sc_out sc_lv 9 signal 5 } 
	{ real_o_1_ce1 sc_out sc_logic 1 signal 5 } 
	{ real_o_1_d1 sc_out sc_lv 32 signal 5 } 
	{ real_o_1_q1 sc_in sc_lv 32 signal 5 } 
	{ real_o_1_we1 sc_out sc_logic 1 signal 5 } 
	{ imag_o_0_address0 sc_out sc_lv 9 signal 6 } 
	{ imag_o_0_ce0 sc_out sc_logic 1 signal 6 } 
	{ imag_o_0_d0 sc_out sc_lv 32 signal 6 } 
	{ imag_o_0_q0 sc_in sc_lv 32 signal 6 } 
	{ imag_o_0_we0 sc_out sc_logic 1 signal 6 } 
	{ imag_o_0_address1 sc_out sc_lv 9 signal 6 } 
	{ imag_o_0_ce1 sc_out sc_logic 1 signal 6 } 
	{ imag_o_0_d1 sc_out sc_lv 32 signal 6 } 
	{ imag_o_0_q1 sc_in sc_lv 32 signal 6 } 
	{ imag_o_0_we1 sc_out sc_logic 1 signal 6 } 
	{ imag_o_1_address0 sc_out sc_lv 9 signal 7 } 
	{ imag_o_1_ce0 sc_out sc_logic 1 signal 7 } 
	{ imag_o_1_d0 sc_out sc_lv 32 signal 7 } 
	{ imag_o_1_q0 sc_in sc_lv 32 signal 7 } 
	{ imag_o_1_we0 sc_out sc_logic 1 signal 7 } 
	{ imag_o_1_address1 sc_out sc_lv 9 signal 7 } 
	{ imag_o_1_ce1 sc_out sc_logic 1 signal 7 } 
	{ imag_o_1_d1 sc_out sc_lv 32 signal 7 } 
	{ imag_o_1_q1 sc_in sc_lv 32 signal 7 } 
	{ imag_o_1_we1 sc_out sc_logic 1 signal 7 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "real_i_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "real_i_0", "role": "address0" }} , 
 	{ "name": "real_i_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "ce0" }} , 
 	{ "name": "real_i_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "d0" }} , 
 	{ "name": "real_i_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "q0" }} , 
 	{ "name": "real_i_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "we0" }} , 
 	{ "name": "real_i_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "real_i_0", "role": "address1" }} , 
 	{ "name": "real_i_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "ce1" }} , 
 	{ "name": "real_i_0_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "d1" }} , 
 	{ "name": "real_i_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "q1" }} , 
 	{ "name": "real_i_0_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "we1" }} , 
 	{ "name": "real_i_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "real_i_1", "role": "address0" }} , 
 	{ "name": "real_i_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "ce0" }} , 
 	{ "name": "real_i_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "d0" }} , 
 	{ "name": "real_i_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "q0" }} , 
 	{ "name": "real_i_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "we0" }} , 
 	{ "name": "real_i_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "real_i_1", "role": "address1" }} , 
 	{ "name": "real_i_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "ce1" }} , 
 	{ "name": "real_i_1_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "d1" }} , 
 	{ "name": "real_i_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "q1" }} , 
 	{ "name": "real_i_1_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "we1" }} , 
 	{ "name": "imag_i_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "imag_i_0", "role": "address0" }} , 
 	{ "name": "imag_i_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_0", "role": "ce0" }} , 
 	{ "name": "imag_i_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_0", "role": "d0" }} , 
 	{ "name": "imag_i_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_0", "role": "q0" }} , 
 	{ "name": "imag_i_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_0", "role": "we0" }} , 
 	{ "name": "imag_i_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "imag_i_0", "role": "address1" }} , 
 	{ "name": "imag_i_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_0", "role": "ce1" }} , 
 	{ "name": "imag_i_0_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_0", "role": "d1" }} , 
 	{ "name": "imag_i_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_0", "role": "q1" }} , 
 	{ "name": "imag_i_0_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_0", "role": "we1" }} , 
 	{ "name": "imag_i_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "imag_i_1", "role": "address0" }} , 
 	{ "name": "imag_i_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_1", "role": "ce0" }} , 
 	{ "name": "imag_i_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_1", "role": "d0" }} , 
 	{ "name": "imag_i_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_1", "role": "q0" }} , 
 	{ "name": "imag_i_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_1", "role": "we0" }} , 
 	{ "name": "imag_i_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "imag_i_1", "role": "address1" }} , 
 	{ "name": "imag_i_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_1", "role": "ce1" }} , 
 	{ "name": "imag_i_1_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_1", "role": "d1" }} , 
 	{ "name": "imag_i_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_1", "role": "q1" }} , 
 	{ "name": "imag_i_1_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_1", "role": "we1" }} , 
 	{ "name": "real_o_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "real_o_0", "role": "address0" }} , 
 	{ "name": "real_o_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_0", "role": "ce0" }} , 
 	{ "name": "real_o_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_0", "role": "d0" }} , 
 	{ "name": "real_o_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_0", "role": "q0" }} , 
 	{ "name": "real_o_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_0", "role": "we0" }} , 
 	{ "name": "real_o_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "real_o_0", "role": "address1" }} , 
 	{ "name": "real_o_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_0", "role": "ce1" }} , 
 	{ "name": "real_o_0_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_0", "role": "d1" }} , 
 	{ "name": "real_o_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_0", "role": "q1" }} , 
 	{ "name": "real_o_0_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_0", "role": "we1" }} , 
 	{ "name": "real_o_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "real_o_1", "role": "address0" }} , 
 	{ "name": "real_o_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_1", "role": "ce0" }} , 
 	{ "name": "real_o_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_1", "role": "d0" }} , 
 	{ "name": "real_o_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_1", "role": "q0" }} , 
 	{ "name": "real_o_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_1", "role": "we0" }} , 
 	{ "name": "real_o_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "real_o_1", "role": "address1" }} , 
 	{ "name": "real_o_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_1", "role": "ce1" }} , 
 	{ "name": "real_o_1_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_1", "role": "d1" }} , 
 	{ "name": "real_o_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_1", "role": "q1" }} , 
 	{ "name": "real_o_1_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_1", "role": "we1" }} , 
 	{ "name": "imag_o_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "imag_o_0", "role": "address0" }} , 
 	{ "name": "imag_o_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_0", "role": "ce0" }} , 
 	{ "name": "imag_o_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_0", "role": "d0" }} , 
 	{ "name": "imag_o_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_0", "role": "q0" }} , 
 	{ "name": "imag_o_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_0", "role": "we0" }} , 
 	{ "name": "imag_o_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "imag_o_0", "role": "address1" }} , 
 	{ "name": "imag_o_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_0", "role": "ce1" }} , 
 	{ "name": "imag_o_0_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_0", "role": "d1" }} , 
 	{ "name": "imag_o_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_0", "role": "q1" }} , 
 	{ "name": "imag_o_0_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_0", "role": "we1" }} , 
 	{ "name": "imag_o_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "imag_o_1", "role": "address0" }} , 
 	{ "name": "imag_o_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_1", "role": "ce0" }} , 
 	{ "name": "imag_o_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_1", "role": "d0" }} , 
 	{ "name": "imag_o_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_1", "role": "q0" }} , 
 	{ "name": "imag_o_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_1", "role": "we0" }} , 
 	{ "name": "imag_o_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":9, "type": "signal", "bundle":{"name": "imag_o_1", "role": "address1" }} , 
 	{ "name": "imag_o_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_1", "role": "ce1" }} , 
 	{ "name": "imag_o_1_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_1", "role": "d1" }} , 
 	{ "name": "imag_o_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_1", "role": "q1" }} , 
 	{ "name": "imag_o_1_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_1", "role": "we1" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }}  ]}
set Spec2ImplPortList { 
	real_i_0 { ap_memory {  { real_i_0_address0 mem_address 1 9 }  { real_i_0_ce0 mem_ce 1 1 }  { real_i_0_d0 mem_din 1 32 }  { real_i_0_q0 mem_dout 0 32 }  { real_i_0_we0 mem_we 1 1 }  { real_i_0_address1 mem_address 1 9 }  { real_i_0_ce1 mem_ce 1 1 }  { real_i_0_d1 mem_din 1 32 }  { real_i_0_q1 mem_dout 0 32 }  { real_i_0_we1 mem_we 1 1 } } }
	real_i_1 { ap_memory {  { real_i_1_address0 mem_address 1 9 }  { real_i_1_ce0 mem_ce 1 1 }  { real_i_1_d0 mem_din 1 32 }  { real_i_1_q0 mem_dout 0 32 }  { real_i_1_we0 mem_we 1 1 }  { real_i_1_address1 mem_address 1 9 }  { real_i_1_ce1 mem_ce 1 1 }  { real_i_1_d1 mem_din 1 32 }  { real_i_1_q1 mem_dout 0 32 }  { real_i_1_we1 mem_we 1 1 } } }
	imag_i_0 { ap_memory {  { imag_i_0_address0 mem_address 1 9 }  { imag_i_0_ce0 mem_ce 1 1 }  { imag_i_0_d0 mem_din 1 32 }  { imag_i_0_q0 mem_dout 0 32 }  { imag_i_0_we0 mem_we 1 1 }  { imag_i_0_address1 mem_address 1 9 }  { imag_i_0_ce1 mem_ce 1 1 }  { imag_i_0_d1 mem_din 1 32 }  { imag_i_0_q1 mem_dout 0 32 }  { imag_i_0_we1 mem_we 1 1 } } }
	imag_i_1 { ap_memory {  { imag_i_1_address0 mem_address 1 9 }  { imag_i_1_ce0 mem_ce 1 1 }  { imag_i_1_d0 mem_din 1 32 }  { imag_i_1_q0 mem_dout 0 32 }  { imag_i_1_we0 mem_we 1 1 }  { imag_i_1_address1 mem_address 1 9 }  { imag_i_1_ce1 mem_ce 1 1 }  { imag_i_1_d1 mem_din 1 32 }  { imag_i_1_q1 mem_dout 0 32 }  { imag_i_1_we1 mem_we 1 1 } } }
	real_o_0 { ap_memory {  { real_o_0_address0 mem_address 1 9 }  { real_o_0_ce0 mem_ce 1 1 }  { real_o_0_d0 mem_din 1 32 }  { real_o_0_q0 mem_dout 0 32 }  { real_o_0_we0 mem_we 1 1 }  { real_o_0_address1 mem_address 1 9 }  { real_o_0_ce1 mem_ce 1 1 }  { real_o_0_d1 mem_din 1 32 }  { real_o_0_q1 mem_dout 0 32 }  { real_o_0_we1 mem_we 1 1 } } }
	real_o_1 { ap_memory {  { real_o_1_address0 mem_address 1 9 }  { real_o_1_ce0 mem_ce 1 1 }  { real_o_1_d0 mem_din 1 32 }  { real_o_1_q0 mem_dout 0 32 }  { real_o_1_we0 mem_we 1 1 }  { real_o_1_address1 mem_address 1 9 }  { real_o_1_ce1 mem_ce 1 1 }  { real_o_1_d1 mem_din 1 32 }  { real_o_1_q1 mem_dout 0 32 }  { real_o_1_we1 mem_we 1 1 } } }
	imag_o_0 { ap_memory {  { imag_o_0_address0 mem_address 1 9 }  { imag_o_0_ce0 mem_ce 1 1 }  { imag_o_0_d0 mem_din 1 32 }  { imag_o_0_q0 mem_dout 0 32 }  { imag_o_0_we0 mem_we 1 1 }  { imag_o_0_address1 mem_address 1 9 }  { imag_o_0_ce1 mem_ce 1 1 }  { imag_o_0_d1 mem_din 1 32 }  { imag_o_0_q1 mem_dout 0 32 }  { imag_o_0_we1 mem_we 1 1 } } }
	imag_o_1 { ap_memory {  { imag_o_1_address0 mem_address 1 9 }  { imag_o_1_ce0 mem_ce 1 1 }  { imag_o_1_d0 mem_din 1 32 }  { imag_o_1_q0 mem_dout 0 32 }  { imag_o_1_we0 mem_we 1 1 }  { imag_o_1_address1 mem_address 1 9 }  { imag_o_1_ce1 mem_ce 1 1 }  { imag_o_1_d1 mem_din 1 32 }  { imag_o_1_q1 mem_dout 0 32 }  { imag_o_1_we1 mem_we 1 1 } } }
}
