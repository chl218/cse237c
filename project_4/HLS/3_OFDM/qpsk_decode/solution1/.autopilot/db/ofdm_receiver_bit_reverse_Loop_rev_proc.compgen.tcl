# This script segment is generated automatically by AutoPilot

# Memory (RAM/ROM)  definition:
set ID 0
set MemName ofdm_receiver_bit_reverse_Loop_rev_proc_index_table_0
set CoreName ap_simcore_mem
set PortList { 1 }
set DataWd 9
set AddrRange 512
set AddrWd 9
set TrueReset 0
set IsROM 1
set ROMData { "000000000" "100000000" "010000000" "110000000" "001000000" "101000000" "011000000" "111000000" "000100000" "100100000" "010100000" "110100000" "001100000" "101100000" "011100000" "111100000" "000010000" "100010000" "010010000" "110010000" "001010000" "101010000" "011010000" "111010000" "000110000" "100110000" "010110000" "110110000" "001110000" "101110000" "011110000" "111110000" "000001000" "100001000" "010001000" "110001000" "001001000" "101001000" "011001000" "111001000" "000101000" "100101000" "010101000" "110101000" "001101000" "101101000" "011101000" "111101000" "000011000" "100011000" "010011000" "110011000" "001011000" "101011000" "011011000" "111011000" "000111000" "100111000" "010111000" "110111000" "001111000" "101111000" "011111000" "111111000" "000000100" "100000100" "010000100" "110000100" "001000100" "101000100" "011000100" "111000100" "000100100" "100100100" "010100100" "110100100" "001100100" "101100100" "011100100" "111100100" "000010100" "100010100" "010010100" "110010100" "001010100" "101010100" "011010100" "111010100" "000110100" "100110100" "010110100" "110110100" "001110100" "101110100" "011110100" "111110100" "000001100" "100001100" "010001100" "110001100" "001001100" "101001100" "011001100" "111001100" "000101100" "100101100" "010101100" "110101100" "001101100" "101101100" "011101100" "111101100" "000011100" "100011100" "010011100" "110011100" "001011100" "101011100" "011011100" "111011100" "000111100" "100111100" "010111100" "110111100" "001111100" "101111100" "011111100" "111111100" "000000010" "100000010" "010000010" "110000010" "001000010" "101000010" "011000010" "111000010" "000100010" "100100010" "010100010" "110100010" "001100010" "101100010" "011100010" "111100010" "000010010" "100010010" "010010010" "110010010" "001010010" "101010010" "011010010" "111010010" "000110010" "100110010" "010110010" "110110010" "001110010" "101110010" "011110010" "111110010" "000001010" "100001010" "010001010" "110001010" "001001010" "101001010" "011001010" "111001010" "000101010" "100101010" "010101010" "110101010" "001101010" "101101010" "011101010" "111101010" "000011010" "100011010" "010011010" "110011010" "001011010" "101011010" "011011010" "111011010" "000111010" "100111010" "010111010" "110111010" "001111010" "101111010" "011111010" "111111010" "000000110" "100000110" "010000110" "110000110" "001000110" "101000110" "011000110" "111000110" "000100110" "100100110" "010100110" "110100110" "001100110" "101100110" "011100110" "111100110" "000010110" "100010110" "010010110" "110010110" "001010110" "101010110" "011010110" "111010110" "000110110" "100110110" "010110110" "110110110" "001110110" "101110110" "011110110" "111110110" "000001110" "100001110" "010001110" "110001110" "001001110" "101001110" "011001110" "111001110" "000101110" "100101110" "010101110" "110101110" "001101110" "101101110" "011101110" "111101110" "000011110" "100011110" "010011110" "110011110" "001011110" "101011110" "011011110" "111011110" "000111110" "100111110" "010111110" "110111110" "001111110" "101111110" "011111110" "111111110" "000000001" "100000001" "010000001" "110000001" "001000001" "101000001" "011000001" "111000001" "000100001" "100100001" "010100001" "110100001" "001100001" "101100001" "011100001" "111100001" "000010001" "100010001" "010010001" "110010001" "001010001" "101010001" "011010001" "111010001" "000110001" "100110001" "010110001" "110110001" "001110001" "101110001" "011110001" "111110001" "000001001" "100001001" "010001001" "110001001" "001001001" "101001001" "011001001" "111001001" "000101001" "100101001" "010101001" "110101001" "001101001" "101101001" "011101001" "111101001" "000011001" "100011001" "010011001" "110011001" "001011001" "101011001" "011011001" "111011001" "000111001" "100111001" "010111001" "110111001" "001111001" "101111001" "011111001" "111111001" "000000101" "100000101" "010000101" "110000101" "001000101" "101000101" "011000101" "111000101" "000100101" "100100101" "010100101" "110100101" "001100101" "101100101" "011100101" "111100101" "000010101" "100010101" "010010101" "110010101" "001010101" "101010101" "011010101" "111010101" "000110101" "100110101" "010110101" "110110101" "001110101" "101110101" "011110101" "111110101" "000001101" "100001101" "010001101" "110001101" "001001101" "101001101" "011001101" "111001101" "000101101" "100101101" "010101101" "110101101" "001101101" "101101101" "011101101" "111101101" "000011101" "100011101" "010011101" "110011101" "001011101" "101011101" "011011101" "111011101" "000111101" "100111101" "010111101" "110111101" "001111101" "101111101" "011111101" "111111101" "000000011" "100000011" "010000011" "110000011" "001000011" "101000011" "011000011" "111000011" "000100011" "100100011" "010100011" "110100011" "001100011" "101100011" "011100011" "111100011" "000010011" "100010011" "010010011" "110010011" "001010011" "101010011" "011010011" "111010011" "000110011" "100110011" "010110011" "110110011" "001110011" "101110011" "011110011" "111110011" "000001011" "100001011" "010001011" "110001011" "001001011" "101001011" "011001011" "111001011" "000101011" "100101011" "010101011" "110101011" "001101011" "101101011" "011101011" "111101011" "000011011" "100011011" "010011011" "110011011" "001011011" "101011011" "011011011" "111011011" "000111011" "100111011" "010111011" "110111011" "001111011" "101111011" "011111011" "111111011" "000000111" "100000111" "010000111" "110000111" "001000111" "101000111" "011000111" "111000111" "000100111" "100100111" "010100111" "110100111" "001100111" "101100111" "011100111" "111100111" "000010111" "100010111" "010010111" "110010111" "001010111" "101010111" "011010111" "111010111" "000110111" "100110111" "010110111" "110110111" "001110111" "101110111" "011110111" "111110111" "000001111" "100001111" "010001111" "110001111" "001001111" "101001111" "011001111" "111001111" "000101111" "100101111" "010101111" "110101111" "001101111" "101101111" "011101111" "111101111" "000011111" "100011111" "010011111" "110011111" "001011111" "101011111" "011011111" "111011111" "000111111" "100111111" "010111111" "110111111" "001111111" "101111111" "011111111" "111111111" }
set HasInitializer 1
set Initializer $ROMData
set NumOfStage 2
set MaxLatency -1
set DelayBudget 2.39
set ClkPeriod 10
set RegisteredInput 0
if {${::AESL::PGuard_simmodel_gen}} {
if {[info proc ap_gen_simcore_mem] == "ap_gen_simcore_mem"} {
    eval "ap_gen_simcore_mem { \
    id ${ID} \
    name ${MemName} \
    corename ${CoreName}  \
    op mem \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage}  \
    registered_input ${RegisteredInput} \
    port_num 1 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    true_reset ${TrueReset} \
    delay_budget ${DelayBudget} \
    clk_period ${ClkPeriod} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
 } "
} else {
    puts "@W \[IMPL-102\] Cannot find ap_gen_simcore_mem, check your platform lib"
}
}


if {${::AESL::PGuard_rtl_comp_handler}} {
  ::AP::rtl_comp_handler $MemName
}


set CoreName ROM_nP
if {${::AESL::PGuard_autocg_gen} && ${::AESL::PGuard_autocg_ipmgen}} {
if {[info proc ::AESL_LIB_VIRTEX::xil_gen_ROM] == "::AESL_LIB_VIRTEX::xil_gen_ROM"} {
    eval "::AESL_LIB_VIRTEX::xil_gen_ROM { \
    id ${ID} \
    name ${MemName} \
    corename ${CoreName}  \
    op mem \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage}  \
    registered_input ${RegisteredInput} \
    port_num 1 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    true_reset ${TrueReset} \
    delay_budget ${DelayBudget} \
    clk_period ${ClkPeriod} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
 } "
  } else {
    puts "@W \[IMPL-104\] Cannot find ::AESL_LIB_VIRTEX::xil_gen_ROM, check your platform lib"
  }
}


# Memory (RAM/ROM)  definition:
set ID 1
set MemName ofdm_receiver_bit_reverse_Loop_rev_proc_index_table_1
set CoreName ap_simcore_mem
set PortList { 1 }
set DataWd 10
set AddrRange 512
set AddrWd 9
set TrueReset 0
set IsROM 1
set ROMData { "1000000000" "1100000000" "1010000000" "1110000000" "1001000000" "1101000000" "1011000000" "1111000000" "1000100000" "1100100000" "1010100000" "1110100000" "1001100000" "1101100000" "1011100000" "1111100000" "1000010000" "1100010000" "1010010000" "1110010000" "1001010000" "1101010000" "1011010000" "1111010000" "1000110000" "1100110000" "1010110000" "1110110000" "1001110000" "1101110000" "1011110000" "1111110000" "1000001000" "1100001000" "1010001000" "1110001000" "1001001000" "1101001000" "1011001000" "1111001000" "1000101000" "1100101000" "1010101000" "1110101000" "1001101000" "1101101000" "1011101000" "1111101000" "1000011000" "1100011000" "1010011000" "1110011000" "1001011000" "1101011000" "1011011000" "1111011000" "1000111000" "1100111000" "1010111000" "1110111000" "1001111000" "1101111000" "1011111000" "1111111000" "1000000100" "1100000100" "1010000100" "1110000100" "1001000100" "1101000100" "1011000100" "1111000100" "1000100100" "1100100100" "1010100100" "1110100100" "1001100100" "1101100100" "1011100100" "1111100100" "1000010100" "1100010100" "1010010100" "1110010100" "1001010100" "1101010100" "1011010100" "1111010100" "1000110100" "1100110100" "1010110100" "1110110100" "1001110100" "1101110100" "1011110100" "1111110100" "1000001100" "1100001100" "1010001100" "1110001100" "1001001100" "1101001100" "1011001100" "1111001100" "1000101100" "1100101100" "1010101100" "1110101100" "1001101100" "1101101100" "1011101100" "1111101100" "1000011100" "1100011100" "1010011100" "1110011100" "1001011100" "1101011100" "1011011100" "1111011100" "1000111100" "1100111100" "1010111100" "1110111100" "1001111100" "1101111100" "1011111100" "1111111100" "1000000010" "1100000010" "1010000010" "1110000010" "1001000010" "1101000010" "1011000010" "1111000010" "1000100010" "1100100010" "1010100010" "1110100010" "1001100010" "1101100010" "1011100010" "1111100010" "1000010010" "1100010010" "1010010010" "1110010010" "1001010010" "1101010010" "1011010010" "1111010010" "1000110010" "1100110010" "1010110010" "1110110010" "1001110010" "1101110010" "1011110010" "1111110010" "1000001010" "1100001010" "1010001010" "1110001010" "1001001010" "1101001010" "1011001010" "1111001010" "1000101010" "1100101010" "1010101010" "1110101010" "1001101010" "1101101010" "1011101010" "1111101010" "1000011010" "1100011010" "1010011010" "1110011010" "1001011010" "1101011010" "1011011010" "1111011010" "1000111010" "1100111010" "1010111010" "1110111010" "1001111010" "1101111010" "1011111010" "1111111010" "1000000110" "1100000110" "1010000110" "1110000110" "1001000110" "1101000110" "1011000110" "1111000110" "1000100110" "1100100110" "1010100110" "1110100110" "1001100110" "1101100110" "1011100110" "1111100110" "1000010110" "1100010110" "1010010110" "1110010110" "1001010110" "1101010110" "1011010110" "1111010110" "1000110110" "1100110110" "1010110110" "1110110110" "1001110110" "1101110110" "1011110110" "1111110110" "1000001110" "1100001110" "1010001110" "1110001110" "1001001110" "1101001110" "1011001110" "1111001110" "1000101110" "1100101110" "1010101110" "1110101110" "1001101110" "1101101110" "1011101110" "1111101110" "1000011110" "1100011110" "1010011110" "1110011110" "1001011110" "1101011110" "1011011110" "1111011110" "1000111110" "1100111110" "1010111110" "1110111110" "1001111110" "1101111110" "1011111110" "1111111110" "1000000001" "1100000001" "1010000001" "1110000001" "1001000001" "1101000001" "1011000001" "1111000001" "1000100001" "1100100001" "1010100001" "1110100001" "1001100001" "1101100001" "1011100001" "1111100001" "1000010001" "1100010001" "1010010001" "1110010001" "1001010001" "1101010001" "1011010001" "1111010001" "1000110001" "1100110001" "1010110001" "1110110001" "1001110001" "1101110001" "1011110001" "1111110001" "1000001001" "1100001001" "1010001001" "1110001001" "1001001001" "1101001001" "1011001001" "1111001001" "1000101001" "1100101001" "1010101001" "1110101001" "1001101001" "1101101001" "1011101001" "1111101001" "1000011001" "1100011001" "1010011001" "1110011001" "1001011001" "1101011001" "1011011001" "1111011001" "1000111001" "1100111001" "1010111001" "1110111001" "1001111001" "1101111001" "1011111001" "1111111001" "1000000101" "1100000101" "1010000101" "1110000101" "1001000101" "1101000101" "1011000101" "1111000101" "1000100101" "1100100101" "1010100101" "1110100101" "1001100101" "1101100101" "1011100101" "1111100101" "1000010101" "1100010101" "1010010101" "1110010101" "1001010101" "1101010101" "1011010101" "1111010101" "1000110101" "1100110101" "1010110101" "1110110101" "1001110101" "1101110101" "1011110101" "1111110101" "1000001101" "1100001101" "1010001101" "1110001101" "1001001101" "1101001101" "1011001101" "1111001101" "1000101101" "1100101101" "1010101101" "1110101101" "1001101101" "1101101101" "1011101101" "1111101101" "1000011101" "1100011101" "1010011101" "1110011101" "1001011101" "1101011101" "1011011101" "1111011101" "1000111101" "1100111101" "1010111101" "1110111101" "1001111101" "1101111101" "1011111101" "1111111101" "1000000011" "1100000011" "1010000011" "1110000011" "1001000011" "1101000011" "1011000011" "1111000011" "1000100011" "1100100011" "1010100011" "1110100011" "1001100011" "1101100011" "1011100011" "1111100011" "1000010011" "1100010011" "1010010011" "1110010011" "1001010011" "1101010011" "1011010011" "1111010011" "1000110011" "1100110011" "1010110011" "1110110011" "1001110011" "1101110011" "1011110011" "1111110011" "1000001011" "1100001011" "1010001011" "1110001011" "1001001011" "1101001011" "1011001011" "1111001011" "1000101011" "1100101011" "1010101011" "1110101011" "1001101011" "1101101011" "1011101011" "1111101011" "1000011011" "1100011011" "1010011011" "1110011011" "1001011011" "1101011011" "1011011011" "1111011011" "1000111011" "1100111011" "1010111011" "1110111011" "1001111011" "1101111011" "1011111011" "1111111011" "1000000111" "1100000111" "1010000111" "1110000111" "1001000111" "1101000111" "1011000111" "1111000111" "1000100111" "1100100111" "1010100111" "1110100111" "1001100111" "1101100111" "1011100111" "1111100111" "1000010111" "1100010111" "1010010111" "1110010111" "1001010111" "1101010111" "1011010111" "1111010111" "1000110111" "1100110111" "1010110111" "1110110111" "1001110111" "1101110111" "1011110111" "1111110111" "1000001111" "1100001111" "1010001111" "1110001111" "1001001111" "1101001111" "1011001111" "1111001111" "1000101111" "1100101111" "1010101111" "1110101111" "1001101111" "1101101111" "1011101111" "1111101111" "1000011111" "1100011111" "1010011111" "1110011111" "1001011111" "1101011111" "1011011111" "1111011111" "1000111111" "1100111111" "1010111111" "1110111111" "1001111111" "1101111111" "1011111111" "1111111111" }
set HasInitializer 1
set Initializer $ROMData
set NumOfStage 2
set MaxLatency -1
set DelayBudget 2.39
set ClkPeriod 10
set RegisteredInput 0
if {${::AESL::PGuard_simmodel_gen}} {
if {[info proc ap_gen_simcore_mem] == "ap_gen_simcore_mem"} {
    eval "ap_gen_simcore_mem { \
    id ${ID} \
    name ${MemName} \
    corename ${CoreName}  \
    op mem \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage}  \
    registered_input ${RegisteredInput} \
    port_num 1 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    true_reset ${TrueReset} \
    delay_budget ${DelayBudget} \
    clk_period ${ClkPeriod} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
 } "
} else {
    puts "@W \[IMPL-102\] Cannot find ap_gen_simcore_mem, check your platform lib"
}
}


if {${::AESL::PGuard_rtl_comp_handler}} {
  ::AP::rtl_comp_handler $MemName
}


set CoreName ROM_nP
if {${::AESL::PGuard_autocg_gen} && ${::AESL::PGuard_autocg_ipmgen}} {
if {[info proc ::AESL_LIB_VIRTEX::xil_gen_ROM] == "::AESL_LIB_VIRTEX::xil_gen_ROM"} {
    eval "::AESL_LIB_VIRTEX::xil_gen_ROM { \
    id ${ID} \
    name ${MemName} \
    corename ${CoreName}  \
    op mem \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage}  \
    registered_input ${RegisteredInput} \
    port_num 1 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    true_reset ${TrueReset} \
    delay_budget ${DelayBudget} \
    clk_period ${ClkPeriod} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
 } "
  } else {
    puts "@W \[IMPL-104\] Cannot find ::AESL_LIB_VIRTEX::xil_gen_ROM, check your platform lib"
  }
}


# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 2 \
    name imag_o \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o \
    op interface \
    ports { imag_o_address0 { O 10 vector } imag_o_ce0 { O 1 bit } imag_o_we0 { O 1 bit } imag_o_d0 { O 32 vector } imag_o_address1 { O 10 vector } imag_o_ce1 { O 1 bit } imag_o_we1 { O 1 bit } imag_o_d1 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 3 \
    name real_o_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o_1 \
    op interface \
    ports { real_o_1_address0 { O 9 vector } real_o_1_ce0 { O 1 bit } real_o_1_we0 { O 1 bit } real_o_1_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 4 \
    name imag_i_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_0 \
    op interface \
    ports { imag_i_0_address0 { O 9 vector } imag_i_0_ce0 { O 1 bit } imag_i_0_q0 { I 32 vector } imag_i_0_address1 { O 9 vector } imag_i_0_ce1 { O 1 bit } imag_i_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 5 \
    name imag_i_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_1 \
    op interface \
    ports { imag_i_1_address0 { O 9 vector } imag_i_1_ce0 { O 1 bit } imag_i_1_q0 { I 32 vector } imag_i_1_address1 { O 9 vector } imag_i_1_ce1 { O 1 bit } imag_i_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 6 \
    name real_i_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_0 \
    op interface \
    ports { real_i_0_address0 { O 9 vector } real_i_0_ce0 { O 1 bit } real_i_0_q0 { I 32 vector } real_i_0_address1 { O 9 vector } real_i_0_ce1 { O 1 bit } real_i_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 7 \
    name real_i_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_1 \
    op interface \
    ports { real_i_1_address0 { O 9 vector } real_i_1_ce0 { O 1 bit } real_i_1_q0 { I 32 vector } real_i_1_address1 { O 9 vector } real_i_1_ce1 { O 1 bit } real_i_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 8 \
    name real_o_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o_0 \
    op interface \
    ports { real_o_0_address0 { O 9 vector } real_o_0_ce0 { O 1 bit } real_o_0_we0 { O 1 bit } real_o_0_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o_0'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


