#include <stdio.h>
#include "fft_stage_last.h"

/*
 * Function: fft_stage_first_op calculates the LAST stage of FFT.
 * 
INPUT:
	In_R, In_I[]: Real and Imag parts of Complex signal

OUTPUT:
	OUT_R, OUT_I[]: Real and Imag parts of Complex signal

 */

//=======================BEGIN: FFT fft_stage_last =========================
void fft_stage_last(DTYPE real_i[SIZE], DTYPE imag_i[SIZE], DTYPE real_o[SIZE], DTYPE imag_o[SIZE]) {

	int k 		= 0;
	int DFTpts	= 1024;		// DFT = 2^stage = points in sub DFT
	int numBF 	= 512; 		// Butterfly WIDTHS in sub-DFT

	butterfly: for(int j = 0; j < numBF; j++) {
		DTYPE c = W_real[k];
		DTYPE s = W_imag[k];

		printf("k: %5d\tc: %10f\ts: %10f\n", k, c , s);

		DFTpts:for(int i = j; i < SIZE; i += DFTpts) {

			int i_lower = i + numBF;
			DTYPE temp_R = real_i[i_lower]*c - imag_i[i_lower]*s;
			DTYPE temp_I = imag_i[i_lower]*c + real_i[i_lower]*s;

			real_o[i_lower] = real_i[i] - temp_R;
			imag_o[i_lower] = imag_i[i] - temp_I;
			real_o[i] = real_i[i] + temp_R;
			imag_o[i] = imag_i[i] + temp_I;
	 	}
		k++;
	}

}
//=======================END: FFT fft_stage_last =========================

