<project xmlns="com.autoesl.autopilot.project" name="fft_stage_last" top="fft_stage_last">
    <files>
        <file name="fft_stage_last.cpp" sc="0" tb="false" cflags=""/>
        <file name="fft_stage_last.h" sc="0" tb="false" cflags=""/>
        <file name="../../fft_stage_last_test.cpp" sc="0" tb="1" cflags=""/>
        <file name="../../out.gold.dat" sc="0" tb="1" cflags=""/>
    </files>
    <includePaths/>
    <libraryPaths/>
    <Simulation>
        <SimFlow askAgain="false" name="csim" csimMode="0" lastCsimMode="0"/>
    </Simulation>
    <solutions xmlns="">
        <solution name="solution1" status="active"/>
    </solutions>
</project>

