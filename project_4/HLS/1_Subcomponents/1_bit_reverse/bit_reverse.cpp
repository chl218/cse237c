/*
The bit_reverse part of FFT.
INPUT:
	In[]:

OUTPUT:
	Out[]:
 */

#include <stdio.h>
#include "bit_reverse.h"


void bit_reverse(DTYPE real_i[SIZE], DTYPE imag_i[SIZE], DTYPE real_o[SIZE], DTYPE imag_o[SIZE]) {
#pragma HLS DATAFLOW

	rev: for(unsigned int i = 0; i < SIZE; i+=2) {
#pragma HLS UNROLL
#pragma HLS PIPELINE II=1

		unsigned int r0 = index_table[i];
		unsigned int r1 = index_table[i+1];

		real_o[i] = real_i[r0];
		imag_o[i] = imag_i[r0];

		real_o[i+1] = real_i[r1];
		imag_o[i+1] = imag_i[r1];
	}
}
