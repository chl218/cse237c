set moduleName bit_reverse_Loop_rev_proc
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {bit_reverse_Loop_rev_proc}
set C_modelType { void 0 }
set C_modelArgList { 
	{ real_i float 32 regular {array 1024 { 2 2 } 1 1 }  }
	{ imag_i float 32 regular {array 1024 { 2 2 } 1 1 }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "real_i", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE"} , 
 	{ "Name" : "imag_i", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE"} ]}
# RTL Port declarations: 
set portNum 27
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ real_i_address0 sc_out sc_lv 10 signal 0 } 
	{ real_i_ce0 sc_out sc_logic 1 signal 0 } 
	{ real_i_we0 sc_out sc_logic 1 signal 0 } 
	{ real_i_d0 sc_out sc_lv 32 signal 0 } 
	{ real_i_q0 sc_in sc_lv 32 signal 0 } 
	{ real_i_address1 sc_out sc_lv 10 signal 0 } 
	{ real_i_ce1 sc_out sc_logic 1 signal 0 } 
	{ real_i_we1 sc_out sc_logic 1 signal 0 } 
	{ real_i_d1 sc_out sc_lv 32 signal 0 } 
	{ real_i_q1 sc_in sc_lv 32 signal 0 } 
	{ imag_i_address0 sc_out sc_lv 10 signal 1 } 
	{ imag_i_ce0 sc_out sc_logic 1 signal 1 } 
	{ imag_i_we0 sc_out sc_logic 1 signal 1 } 
	{ imag_i_d0 sc_out sc_lv 32 signal 1 } 
	{ imag_i_q0 sc_in sc_lv 32 signal 1 } 
	{ imag_i_address1 sc_out sc_lv 10 signal 1 } 
	{ imag_i_ce1 sc_out sc_logic 1 signal 1 } 
	{ imag_i_we1 sc_out sc_logic 1 signal 1 } 
	{ imag_i_d1 sc_out sc_lv 32 signal 1 } 
	{ imag_i_q1 sc_in sc_lv 32 signal 1 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "real_i_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":10, "type": "signal", "bundle":{"name": "real_i", "role": "address0" }} , 
 	{ "name": "real_i_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i", "role": "ce0" }} , 
 	{ "name": "real_i_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i", "role": "we0" }} , 
 	{ "name": "real_i_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i", "role": "d0" }} , 
 	{ "name": "real_i_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i", "role": "q0" }} , 
 	{ "name": "real_i_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":10, "type": "signal", "bundle":{"name": "real_i", "role": "address1" }} , 
 	{ "name": "real_i_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i", "role": "ce1" }} , 
 	{ "name": "real_i_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i", "role": "we1" }} , 
 	{ "name": "real_i_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i", "role": "d1" }} , 
 	{ "name": "real_i_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i", "role": "q1" }} , 
 	{ "name": "imag_i_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":10, "type": "signal", "bundle":{"name": "imag_i", "role": "address0" }} , 
 	{ "name": "imag_i_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i", "role": "ce0" }} , 
 	{ "name": "imag_i_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i", "role": "we0" }} , 
 	{ "name": "imag_i_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i", "role": "d0" }} , 
 	{ "name": "imag_i_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i", "role": "q0" }} , 
 	{ "name": "imag_i_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":10, "type": "signal", "bundle":{"name": "imag_i", "role": "address1" }} , 
 	{ "name": "imag_i_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i", "role": "ce1" }} , 
 	{ "name": "imag_i_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i", "role": "we1" }} , 
 	{ "name": "imag_i_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i", "role": "d1" }} , 
 	{ "name": "imag_i_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i", "role": "q1" }}  ]}
set Spec2ImplPortList { 
	real_i { ap_memory {  { real_i_address0 mem_address 1 10 }  { real_i_ce0 mem_ce 1 1 }  { real_i_we0 mem_we 1 1 }  { real_i_d0 mem_din 1 32 }  { real_i_q0 mem_dout 0 32 }  { real_i_address1 mem_address 1 10 }  { real_i_ce1 mem_ce 1 1 }  { real_i_we1 mem_we 1 1 }  { real_i_d1 mem_din 1 32 }  { real_i_q1 mem_dout 0 32 } } }
	imag_i { ap_memory {  { imag_i_address0 mem_address 1 10 }  { imag_i_ce0 mem_ce 1 1 }  { imag_i_we0 mem_we 1 1 }  { imag_i_d0 mem_din 1 32 }  { imag_i_q0 mem_dout 0 32 }  { imag_i_address1 mem_address 1 10 }  { imag_i_ce1 mem_ce 1 1 }  { imag_i_we1 mem_we 1 1 }  { imag_i_d1 mem_din 1 32 }  { imag_i_q1 mem_dout 0 32 } } }
}
