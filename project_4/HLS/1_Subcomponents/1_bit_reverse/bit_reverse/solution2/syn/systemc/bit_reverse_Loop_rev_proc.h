// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

#ifndef _bit_reverse_Loop_rev_proc_HH_
#define _bit_reverse_Loop_rev_proc_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "bit_reverse_Loop_rev_proc_table1024.h"

namespace ap_rtl {

struct bit_reverse_Loop_rev_proc : public sc_module {
    // Port declarations 27
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_continue;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_out< sc_lv<10> > real_i_address0;
    sc_out< sc_logic > real_i_ce0;
    sc_out< sc_logic > real_i_we0;
    sc_out< sc_lv<32> > real_i_d0;
    sc_in< sc_lv<32> > real_i_q0;
    sc_out< sc_lv<10> > real_i_address1;
    sc_out< sc_logic > real_i_ce1;
    sc_out< sc_logic > real_i_we1;
    sc_out< sc_lv<32> > real_i_d1;
    sc_in< sc_lv<32> > real_i_q1;
    sc_out< sc_lv<10> > imag_i_address0;
    sc_out< sc_logic > imag_i_ce0;
    sc_out< sc_logic > imag_i_we0;
    sc_out< sc_lv<32> > imag_i_d0;
    sc_in< sc_lv<32> > imag_i_q0;
    sc_out< sc_lv<10> > imag_i_address1;
    sc_out< sc_logic > imag_i_ce1;
    sc_out< sc_logic > imag_i_we1;
    sc_out< sc_lv<32> > imag_i_d1;
    sc_in< sc_lv<32> > imag_i_q1;


    // Module declarations
    bit_reverse_Loop_rev_proc(sc_module_name name);
    SC_HAS_PROCESS(bit_reverse_Loop_rev_proc);

    ~bit_reverse_Loop_rev_proc();

    sc_trace_file* mVcdFile;

    bit_reverse_Loop_rev_proc_table1024* table1024_U;
    sc_signal< sc_logic > ap_done_reg;
    sc_signal< sc_lv<5> > ap_CS_fsm;
    sc_signal< sc_logic > ap_sig_cseq_ST_st1_fsm_0;
    sc_signal< bool > ap_sig_bdd_24;
    sc_signal< sc_lv<10> > table1024_address0;
    sc_signal< sc_logic > table1024_ce0;
    sc_signal< sc_lv<10> > table1024_q0;
    sc_signal< sc_lv<11> > i_0_i_i_reg_92;
    sc_signal< sc_lv<1> > exitcond_i_i_fu_104_p2;
    sc_signal< sc_lv<1> > exitcond_i_i_reg_139;
    sc_signal< sc_logic > ap_sig_cseq_ST_pp0_stg0_fsm_1;
    sc_signal< bool > ap_sig_bdd_73;
    sc_signal< sc_logic > ap_reg_ppiten_pp0_it0;
    sc_signal< sc_logic > ap_reg_ppiten_pp0_it1;
    sc_signal< sc_lv<11> > i_fu_110_p2;
    sc_signal< sc_lv<11> > i_reg_143;
    sc_signal< sc_lv<10> > real_i_addr_reg_153;
    sc_signal< sc_lv<10> > imag_i_addr_reg_159;
    sc_signal< sc_lv<10> > real_i_addr_1_reg_165;
    sc_signal< sc_logic > ap_sig_cseq_ST_pp0_stg1_fsm_2;
    sc_signal< bool > ap_sig_bdd_98;
    sc_signal< sc_lv<10> > imag_i_addr_1_reg_171;
    sc_signal< sc_lv<1> > tmp_2_i_fu_133_p2;
    sc_signal< sc_lv<1> > tmp_2_i_reg_177;
    sc_signal< sc_lv<32> > real_a_reg_181;
    sc_signal< sc_logic > ap_sig_cseq_ST_pp0_stg2_fsm_3;
    sc_signal< bool > ap_sig_bdd_112;
    sc_signal< sc_lv<32> > imag_a_reg_186;
    sc_signal< bool > ap_sig_bdd_120;
    sc_signal< sc_lv<11> > i_0_i_i_phi_fu_96_p4;
    sc_signal< sc_lv<64> > tmp_i_fu_116_p1;
    sc_signal< sc_lv<64> > tmp_1_i_fu_127_p1;
    sc_signal< sc_lv<11> > reversed_cast9_fu_123_p1;
    sc_signal< sc_logic > ap_sig_cseq_ST_st6_fsm_4;
    sc_signal< bool > ap_sig_bdd_170;
    sc_signal< sc_lv<5> > ap_NS_fsm;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<5> ap_ST_st1_fsm_0;
    static const sc_lv<5> ap_ST_pp0_stg0_fsm_1;
    static const sc_lv<5> ap_ST_pp0_stg1_fsm_2;
    static const sc_lv<5> ap_ST_pp0_stg2_fsm_3;
    static const sc_lv<5> ap_ST_st6_fsm_4;
    static const sc_lv<32> ap_const_lv32_0;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<32> ap_const_lv32_1;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<32> ap_const_lv32_2;
    static const sc_lv<32> ap_const_lv32_3;
    static const sc_lv<11> ap_const_lv11_0;
    static const sc_lv<11> ap_const_lv11_400;
    static const sc_lv<11> ap_const_lv11_1;
    static const sc_lv<32> ap_const_lv32_4;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_ap_done();
    void thread_ap_idle();
    void thread_ap_ready();
    void thread_ap_sig_bdd_112();
    void thread_ap_sig_bdd_120();
    void thread_ap_sig_bdd_170();
    void thread_ap_sig_bdd_24();
    void thread_ap_sig_bdd_73();
    void thread_ap_sig_bdd_98();
    void thread_ap_sig_cseq_ST_pp0_stg0_fsm_1();
    void thread_ap_sig_cseq_ST_pp0_stg1_fsm_2();
    void thread_ap_sig_cseq_ST_pp0_stg2_fsm_3();
    void thread_ap_sig_cseq_ST_st1_fsm_0();
    void thread_ap_sig_cseq_ST_st6_fsm_4();
    void thread_exitcond_i_i_fu_104_p2();
    void thread_i_0_i_i_phi_fu_96_p4();
    void thread_i_fu_110_p2();
    void thread_imag_i_address0();
    void thread_imag_i_address1();
    void thread_imag_i_ce0();
    void thread_imag_i_ce1();
    void thread_imag_i_d0();
    void thread_imag_i_d1();
    void thread_imag_i_we0();
    void thread_imag_i_we1();
    void thread_real_i_address0();
    void thread_real_i_address1();
    void thread_real_i_ce0();
    void thread_real_i_ce1();
    void thread_real_i_d0();
    void thread_real_i_d1();
    void thread_real_i_we0();
    void thread_real_i_we1();
    void thread_reversed_cast9_fu_123_p1();
    void thread_table1024_address0();
    void thread_table1024_ce0();
    void thread_tmp_1_i_fu_127_p1();
    void thread_tmp_2_i_fu_133_p2();
    void thread_tmp_i_fu_116_p1();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
