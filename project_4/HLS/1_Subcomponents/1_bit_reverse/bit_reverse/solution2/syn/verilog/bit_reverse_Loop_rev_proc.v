// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module bit_reverse_Loop_rev_proc (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_continue,
        ap_idle,
        ap_ready,
        real_i_address0,
        real_i_ce0,
        real_i_we0,
        real_i_d0,
        real_i_q0,
        real_i_address1,
        real_i_ce1,
        real_i_we1,
        real_i_d1,
        real_i_q1,
        imag_i_address0,
        imag_i_ce0,
        imag_i_we0,
        imag_i_d0,
        imag_i_q0,
        imag_i_address1,
        imag_i_ce1,
        imag_i_we1,
        imag_i_d1,
        imag_i_q1
);

parameter    ap_const_logic_1 = 1'b1;
parameter    ap_const_logic_0 = 1'b0;
parameter    ap_ST_st1_fsm_0 = 5'b1;
parameter    ap_ST_pp0_stg0_fsm_1 = 5'b10;
parameter    ap_ST_pp0_stg1_fsm_2 = 5'b100;
parameter    ap_ST_pp0_stg2_fsm_3 = 5'b1000;
parameter    ap_ST_st6_fsm_4 = 5'b10000;
parameter    ap_const_lv32_0 = 32'b00000000000000000000000000000000;
parameter    ap_const_lv1_1 = 1'b1;
parameter    ap_const_lv32_1 = 32'b1;
parameter    ap_const_lv1_0 = 1'b0;
parameter    ap_const_lv32_2 = 32'b10;
parameter    ap_const_lv32_3 = 32'b11;
parameter    ap_const_lv11_0 = 11'b00000000000;
parameter    ap_const_lv11_400 = 11'b10000000000;
parameter    ap_const_lv11_1 = 11'b1;
parameter    ap_const_lv32_4 = 32'b100;
parameter    ap_true = 1'b1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
input   ap_continue;
output   ap_idle;
output   ap_ready;
output  [9:0] real_i_address0;
output   real_i_ce0;
output   real_i_we0;
output  [31:0] real_i_d0;
input  [31:0] real_i_q0;
output  [9:0] real_i_address1;
output   real_i_ce1;
output   real_i_we1;
output  [31:0] real_i_d1;
input  [31:0] real_i_q1;
output  [9:0] imag_i_address0;
output   imag_i_ce0;
output   imag_i_we0;
output  [31:0] imag_i_d0;
input  [31:0] imag_i_q0;
output  [9:0] imag_i_address1;
output   imag_i_ce1;
output   imag_i_we1;
output  [31:0] imag_i_d1;
input  [31:0] imag_i_q1;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg[9:0] real_i_address0;
reg real_i_ce0;
reg real_i_we0;
reg[9:0] real_i_address1;
reg real_i_ce1;
reg real_i_we1;
reg[9:0] imag_i_address0;
reg imag_i_ce0;
reg imag_i_we0;
reg[9:0] imag_i_address1;
reg imag_i_ce1;
reg imag_i_we1;
reg    ap_done_reg = 1'b0;
(* fsm_encoding = "none" *) reg   [4:0] ap_CS_fsm = 5'b1;
reg    ap_sig_cseq_ST_st1_fsm_0;
reg    ap_sig_bdd_24;
wire   [9:0] table1024_address0;
reg    table1024_ce0;
wire   [9:0] table1024_q0;
reg   [10:0] i_0_i_i_reg_92;
wire   [0:0] exitcond_i_i_fu_104_p2;
reg   [0:0] exitcond_i_i_reg_139;
reg    ap_sig_cseq_ST_pp0_stg0_fsm_1;
reg    ap_sig_bdd_73;
reg    ap_reg_ppiten_pp0_it0 = 1'b0;
reg    ap_reg_ppiten_pp0_it1 = 1'b0;
wire   [10:0] i_fu_110_p2;
reg   [10:0] i_reg_143;
reg   [9:0] real_i_addr_reg_153;
reg   [9:0] imag_i_addr_reg_159;
reg   [9:0] real_i_addr_1_reg_165;
reg    ap_sig_cseq_ST_pp0_stg1_fsm_2;
reg    ap_sig_bdd_98;
reg   [9:0] imag_i_addr_1_reg_171;
wire   [0:0] tmp_2_i_fu_133_p2;
reg   [0:0] tmp_2_i_reg_177;
reg   [31:0] real_a_reg_181;
reg    ap_sig_cseq_ST_pp0_stg2_fsm_3;
reg    ap_sig_bdd_112;
reg   [31:0] imag_a_reg_186;
reg    ap_sig_bdd_120;
reg   [10:0] i_0_i_i_phi_fu_96_p4;
wire   [63:0] tmp_i_fu_116_p1;
wire   [63:0] tmp_1_i_fu_127_p1;
wire   [10:0] reversed_cast9_fu_123_p1;
reg    ap_sig_cseq_ST_st6_fsm_4;
reg    ap_sig_bdd_170;
reg   [4:0] ap_NS_fsm;


bit_reverse_Loop_rev_proc_table1024 #(
    .DataWidth( 10 ),
    .AddressRange( 1024 ),
    .AddressWidth( 10 ))
table1024_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( table1024_address0 ),
    .ce0( table1024_ce0 ),
    .q0( table1024_q0 )
);



always @ (posedge ap_clk) begin : ap_ret_ap_CS_fsm
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_st1_fsm_0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_done_reg
    if (ap_rst == 1'b1) begin
        ap_done_reg <= ap_const_logic_0;
    end else begin
        if ((ap_const_logic_1 == ap_continue)) begin
            ap_done_reg <= ap_const_logic_0;
        end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_4)) begin
            ap_done_reg <= ap_const_logic_1;
        end
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_reg_ppiten_pp0_it0
    if (ap_rst == 1'b1) begin
        ap_reg_ppiten_pp0_it0 <= ap_const_logic_0;
    end else begin
        if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & ~(exitcond_i_i_fu_104_p2 == ap_const_lv1_0))) begin
            ap_reg_ppiten_pp0_it0 <= ap_const_logic_0;
        end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~ap_sig_bdd_120)) begin
            ap_reg_ppiten_pp0_it0 <= ap_const_logic_1;
        end
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_reg_ppiten_pp0_it1
    if (ap_rst == 1'b1) begin
        ap_reg_ppiten_pp0_it1 <= ap_const_logic_0;
    end else begin
        if (((exitcond_i_i_reg_139 == ap_const_lv1_0) & (ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg2_fsm_3))) begin
            ap_reg_ppiten_pp0_it1 <= ap_const_logic_1;
        end else if ((((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~ap_sig_bdd_120) | ((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg2_fsm_3) & ~(exitcond_i_i_reg_139 == ap_const_lv1_0)))) begin
            ap_reg_ppiten_pp0_it1 <= ap_const_logic_0;
        end
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (exitcond_i_i_reg_139 == ap_const_lv1_0))) begin
        i_0_i_i_reg_92 <= i_reg_143;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~ap_sig_bdd_120)) begin
        i_0_i_i_reg_92 <= ap_const_lv11_0;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1)) begin
        exitcond_i_i_reg_139 <= exitcond_i_i_fu_104_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it0))) begin
        i_reg_143 <= i_fu_110_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & (exitcond_i_i_reg_139 == ap_const_lv1_0) & (ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg2_fsm_3))) begin
        imag_a_reg_186 <= imag_i_q0;
        real_a_reg_181 <= real_i_q0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg1_fsm_2) & (exitcond_i_i_reg_139 == ap_const_lv1_0))) begin
        imag_i_addr_1_reg_171 <= tmp_1_i_fu_127_p1;
        real_i_addr_1_reg_165 <= tmp_1_i_fu_127_p1;
        tmp_2_i_reg_177 <= tmp_2_i_fu_133_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (exitcond_i_i_fu_104_p2 == ap_const_lv1_0))) begin
        imag_i_addr_reg_159 <= tmp_i_fu_116_p1;
        real_i_addr_reg_153 <= tmp_i_fu_116_p1;
    end
end

always @ (ap_done_reg or ap_sig_cseq_ST_st6_fsm_4) begin
    if (((ap_const_logic_1 == ap_done_reg) | (ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_4))) begin
        ap_done = ap_const_logic_1;
    end else begin
        ap_done = ap_const_logic_0;
    end
end

always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if ((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0))) begin
        ap_idle = ap_const_logic_1;
    end else begin
        ap_idle = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st6_fsm_4) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st6_fsm_4)) begin
        ap_ready = ap_const_logic_1;
    end else begin
        ap_ready = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_73) begin
    if (ap_sig_bdd_73) begin
        ap_sig_cseq_ST_pp0_stg0_fsm_1 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_pp0_stg0_fsm_1 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_98) begin
    if (ap_sig_bdd_98) begin
        ap_sig_cseq_ST_pp0_stg1_fsm_2 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_pp0_stg1_fsm_2 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_112) begin
    if (ap_sig_bdd_112) begin
        ap_sig_cseq_ST_pp0_stg2_fsm_3 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_pp0_stg2_fsm_3 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_24) begin
    if (ap_sig_bdd_24) begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_170) begin
    if (ap_sig_bdd_170) begin
        ap_sig_cseq_ST_st6_fsm_4 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st6_fsm_4 = ap_const_logic_0;
    end
end

always @ (i_0_i_i_reg_92 or exitcond_i_i_reg_139 or ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it1 or i_reg_143) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (exitcond_i_i_reg_139 == ap_const_lv1_0))) begin
        i_0_i_i_phi_fu_96_p4 = i_reg_143;
    end else begin
        i_0_i_i_phi_fu_96_p4 = i_0_i_i_reg_92;
    end
end

always @ (ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it0 or ap_reg_ppiten_pp0_it1 or imag_i_addr_reg_159 or ap_sig_cseq_ST_pp0_stg1_fsm_2 or imag_i_addr_1_reg_171) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1))) begin
        imag_i_address0 = imag_i_addr_1_reg_171;
    end else if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & (ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg1_fsm_2))) begin
        imag_i_address0 = imag_i_addr_reg_159;
    end else begin
        imag_i_address0 = 'bx;
    end
end

always @ (ap_reg_ppiten_pp0_it0 or imag_i_addr_reg_159 or ap_sig_cseq_ST_pp0_stg1_fsm_2 or ap_sig_cseq_ST_pp0_stg2_fsm_3 or tmp_1_i_fu_127_p1) begin
    if ((ap_const_logic_1 == ap_reg_ppiten_pp0_it0)) begin
        if ((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg2_fsm_3)) begin
            imag_i_address1 = imag_i_addr_reg_159;
        end else if ((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg1_fsm_2)) begin
            imag_i_address1 = tmp_1_i_fu_127_p1;
        end else begin
            imag_i_address1 = 'bx;
        end
    end else begin
        imag_i_address1 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it0 or ap_reg_ppiten_pp0_it1 or ap_sig_cseq_ST_pp0_stg1_fsm_2) begin
    if ((((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1)) | ((ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & (ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg1_fsm_2)))) begin
        imag_i_ce0 = ap_const_logic_1;
    end else begin
        imag_i_ce0 = ap_const_logic_0;
    end
end

always @ (ap_reg_ppiten_pp0_it0 or ap_sig_cseq_ST_pp0_stg1_fsm_2 or ap_sig_cseq_ST_pp0_stg2_fsm_3) begin
    if ((((ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & (ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg2_fsm_3)) | ((ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & (ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg1_fsm_2)))) begin
        imag_i_ce1 = ap_const_logic_1;
    end else begin
        imag_i_ce1 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it1 or tmp_2_i_reg_177) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & ~(ap_const_lv1_0 == tmp_2_i_reg_177))) begin
        imag_i_we0 = ap_const_logic_1;
    end else begin
        imag_i_we0 = ap_const_logic_0;
    end
end

always @ (exitcond_i_i_reg_139 or ap_reg_ppiten_pp0_it0 or tmp_2_i_reg_177 or ap_sig_cseq_ST_pp0_stg2_fsm_3) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & (exitcond_i_i_reg_139 == ap_const_lv1_0) & (ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg2_fsm_3) & ~(ap_const_lv1_0 == tmp_2_i_reg_177))) begin
        imag_i_we1 = ap_const_logic_1;
    end else begin
        imag_i_we1 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it0 or ap_reg_ppiten_pp0_it1 or real_i_addr_reg_153 or real_i_addr_1_reg_165 or ap_sig_cseq_ST_pp0_stg1_fsm_2) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1))) begin
        real_i_address0 = real_i_addr_1_reg_165;
    end else if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & (ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg1_fsm_2))) begin
        real_i_address0 = real_i_addr_reg_153;
    end else begin
        real_i_address0 = 'bx;
    end
end

always @ (ap_reg_ppiten_pp0_it0 or real_i_addr_reg_153 or ap_sig_cseq_ST_pp0_stg1_fsm_2 or ap_sig_cseq_ST_pp0_stg2_fsm_3 or tmp_1_i_fu_127_p1) begin
    if ((ap_const_logic_1 == ap_reg_ppiten_pp0_it0)) begin
        if ((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg2_fsm_3)) begin
            real_i_address1 = real_i_addr_reg_153;
        end else if ((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg1_fsm_2)) begin
            real_i_address1 = tmp_1_i_fu_127_p1;
        end else begin
            real_i_address1 = 'bx;
        end
    end else begin
        real_i_address1 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it0 or ap_reg_ppiten_pp0_it1 or ap_sig_cseq_ST_pp0_stg1_fsm_2) begin
    if ((((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1)) | ((ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & (ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg1_fsm_2)))) begin
        real_i_ce0 = ap_const_logic_1;
    end else begin
        real_i_ce0 = ap_const_logic_0;
    end
end

always @ (ap_reg_ppiten_pp0_it0 or ap_sig_cseq_ST_pp0_stg1_fsm_2 or ap_sig_cseq_ST_pp0_stg2_fsm_3) begin
    if ((((ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & (ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg2_fsm_3)) | ((ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & (ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg1_fsm_2)))) begin
        real_i_ce1 = ap_const_logic_1;
    end else begin
        real_i_ce1 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it1 or tmp_2_i_reg_177) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & ~(ap_const_lv1_0 == tmp_2_i_reg_177))) begin
        real_i_we0 = ap_const_logic_1;
    end else begin
        real_i_we0 = ap_const_logic_0;
    end
end

always @ (exitcond_i_i_reg_139 or ap_reg_ppiten_pp0_it0 or tmp_2_i_reg_177 or ap_sig_cseq_ST_pp0_stg2_fsm_3) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & (exitcond_i_i_reg_139 == ap_const_lv1_0) & (ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg2_fsm_3) & ~(ap_const_lv1_0 == tmp_2_i_reg_177))) begin
        real_i_we1 = ap_const_logic_1;
    end else begin
        real_i_we1 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it0) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it0))) begin
        table1024_ce0 = ap_const_logic_1;
    end else begin
        table1024_ce0 = ap_const_logic_0;
    end
end
always @ (ap_CS_fsm or exitcond_i_i_fu_104_p2 or ap_reg_ppiten_pp0_it0 or ap_sig_bdd_120) begin
    case (ap_CS_fsm)
        ap_ST_st1_fsm_0 : 
        begin
            if (~ap_sig_bdd_120) begin
                ap_NS_fsm = ap_ST_pp0_stg0_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end
        end
        ap_ST_pp0_stg0_fsm_1 : 
        begin
            if (~((ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & ~(exitcond_i_i_fu_104_p2 == ap_const_lv1_0))) begin
                ap_NS_fsm = ap_ST_pp0_stg1_fsm_2;
            end else begin
                ap_NS_fsm = ap_ST_st6_fsm_4;
            end
        end
        ap_ST_pp0_stg1_fsm_2 : 
        begin
            ap_NS_fsm = ap_ST_pp0_stg2_fsm_3;
        end
        ap_ST_pp0_stg2_fsm_3 : 
        begin
            ap_NS_fsm = ap_ST_pp0_stg0_fsm_1;
        end
        ap_ST_st6_fsm_4 : 
        begin
            ap_NS_fsm = ap_ST_st1_fsm_0;
        end
        default : 
        begin
            ap_NS_fsm = 'bx;
        end
    endcase
end



always @ (ap_CS_fsm) begin
    ap_sig_bdd_112 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_3]);
end


always @ (ap_start or ap_done_reg) begin
    ap_sig_bdd_120 = ((ap_start == ap_const_logic_0) | (ap_done_reg == ap_const_logic_1));
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_170 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_4]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_24 = (ap_CS_fsm[ap_const_lv32_0] == ap_const_lv1_1);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_73 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_1]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_98 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_2]);
end

assign exitcond_i_i_fu_104_p2 = (i_0_i_i_phi_fu_96_p4 == ap_const_lv11_400? 1'b1: 1'b0);

assign i_fu_110_p2 = (i_0_i_i_phi_fu_96_p4 + ap_const_lv11_1);

assign imag_i_d0 = imag_a_reg_186;

assign imag_i_d1 = imag_i_q1;

assign real_i_d0 = real_a_reg_181;

assign real_i_d1 = real_i_q1;

assign reversed_cast9_fu_123_p1 = table1024_q0;

assign table1024_address0 = tmp_i_fu_116_p1;

assign tmp_1_i_fu_127_p1 = table1024_q0;

assign tmp_2_i_fu_133_p2 = (i_0_i_i_reg_92 < reversed_cast9_fu_123_p1? 1'b1: 1'b0);

assign tmp_i_fu_116_p1 = i_0_i_i_phi_fu_96_p4;


endmodule //bit_reverse_Loop_rev_proc

