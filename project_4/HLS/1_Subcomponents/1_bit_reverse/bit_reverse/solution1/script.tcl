############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2015 Xilinx Inc. All rights reserved.
############################################################
open_project bit_reverse
set_top bit_reverse
add_files bit_reverse.h
add_files bit_reverse.cpp
add_files -tb out.gold.dat
add_files -tb bit_reverse_test.cpp
open_solution "solution1"
set_part {xc7z020clg484-1}
create_clock -period 10 -name default
#source "./bit_reverse/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -format ip_catalog
