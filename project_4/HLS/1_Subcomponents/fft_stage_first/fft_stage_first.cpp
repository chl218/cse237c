/*
 * Function: fft_stage_first_op calculates the first stage of the FFT.
 * 
INPUT:
	In_R, In_I[]: Real and Imag parts of Complex signal

OUTPUT:
	OUT_R, OUT_I[]: Real and Imag parts of Complex signal
 */

#include <stdio.h>
#include "fft_stage_first.h"

void fft_stage_first(DTYPE real_i[SIZE], DTYPE imag_i[SIZE], DTYPE real_o[SIZE], DTYPE imag_o[SIZE]) {
	int step = SIZE2;
	int k = 0;

	butterfly: for(int j = 0; j < 1; j++) {

		DTYPE c = W_real[k];
		DTYPE s = W_imag[k];

		DFTpts:for(int i = j; i < SIZE; i += 2) {
			int i_lower = i + 1;
			DTYPE temp_R = real_i[i_lower]*c - imag_i[i_lower]*s;
			DTYPE temp_I = imag_i[i_lower]*c + real_i[i_lower]*s;

			real_o[i_lower] = real_i[i] - temp_R;
			imag_o[i_lower] = imag_i[i] - temp_I;
			real_o[i] = real_i[i] + temp_R;
			imag_o[i] = imag_i[i] + temp_I;
	 	}
		k += step;
	}

}

