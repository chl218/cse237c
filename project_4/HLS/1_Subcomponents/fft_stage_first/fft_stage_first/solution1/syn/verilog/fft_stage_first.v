// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

(* CORE_GENERATION_INFO="fft_stage_first,hls_ip_2015_4,{HLS_INPUT_TYPE=cxx,HLS_INPUT_FLOAT=1,HLS_INPUT_FIXED=0,HLS_INPUT_PART=xc7z020clg484-1,HLS_INPUT_CLOCK=10.000000,HLS_INPUT_ARCH=others,HLS_SYN_CLOCK=8.412000,HLS_SYN_LAT=4,HLS_SYN_TPT=none,HLS_SYN_MEM=2,HLS_SYN_DSP=20,HLS_SYN_FF=1999,HLS_SYN_LUT=3139}" *)

module fft_stage_first (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_idle,
        ap_ready,
        real_i_address0,
        real_i_ce0,
        real_i_q0,
        imag_i_address0,
        imag_i_ce0,
        imag_i_q0,
        real_o_address0,
        real_o_ce0,
        real_o_we0,
        real_o_d0,
        real_o_address1,
        real_o_ce1,
        real_o_we1,
        real_o_d1,
        imag_o_address0,
        imag_o_ce0,
        imag_o_we0,
        imag_o_d0,
        imag_o_address1,
        imag_o_ce1,
        imag_o_we1,
        imag_o_d1
);

parameter    ap_const_logic_1 = 1'b1;
parameter    ap_const_logic_0 = 1'b0;
parameter    ap_ST_st1_fsm_0 = 19'b1;
parameter    ap_ST_st2_fsm_1 = 19'b10;
parameter    ap_ST_st3_fsm_2 = 19'b100;
parameter    ap_ST_st4_fsm_3 = 19'b1000;
parameter    ap_ST_st5_fsm_4 = 19'b10000;
parameter    ap_ST_st6_fsm_5 = 19'b100000;
parameter    ap_ST_st7_fsm_6 = 19'b1000000;
parameter    ap_ST_st8_fsm_7 = 19'b10000000;
parameter    ap_ST_st9_fsm_8 = 19'b100000000;
parameter    ap_ST_st10_fsm_9 = 19'b1000000000;
parameter    ap_ST_st11_fsm_10 = 19'b10000000000;
parameter    ap_ST_st12_fsm_11 = 19'b100000000000;
parameter    ap_ST_st13_fsm_12 = 19'b1000000000000;
parameter    ap_ST_st14_fsm_13 = 19'b10000000000000;
parameter    ap_ST_st15_fsm_14 = 19'b100000000000000;
parameter    ap_ST_st16_fsm_15 = 19'b1000000000000000;
parameter    ap_ST_st17_fsm_16 = 19'b10000000000000000;
parameter    ap_ST_st18_fsm_17 = 19'b100000000000000000;
parameter    ap_ST_st19_fsm_18 = 19'b1000000000000000000;
parameter    ap_const_lv32_0 = 32'b00000000000000000000000000000000;
parameter    ap_const_lv1_1 = 1'b1;
parameter    ap_const_lv32_4 = 32'b100;
parameter    ap_const_lv32_C = 32'b1100;
parameter    ap_const_lv32_11 = 32'b10001;
parameter    ap_const_lv32_1 = 32'b1;
parameter    ap_const_lv1_0 = 1'b0;
parameter    ap_const_lv32_2 = 32'b10;
parameter    ap_const_lv32_3 = 32'b11;
parameter    ap_const_lv32_7 = 32'b111;
parameter    ap_const_lv32_B = 32'b1011;
parameter    ap_const_lv10_0 = 10'b0000000000;
parameter    ap_const_lv32_12 = 32'b10010;
parameter    ap_const_lv32_8 = 32'b1000;
parameter    ap_const_lv32_D = 32'b1101;
parameter    ap_const_lv32_A = 32'b1010;
parameter    ap_const_lv32_1F = 32'b11111;
parameter    ap_const_lv22_1 = 22'b1;
parameter    ap_const_lv10_200 = 10'b1000000000;
parameter    ap_const_lv2_0 = 2'b00;
parameter    ap_const_lv2_1 = 2'b1;
parameter    ap_true = 1'b1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
output   ap_idle;
output   ap_ready;
output  [9:0] real_i_address0;
output   real_i_ce0;
input  [31:0] real_i_q0;
output  [9:0] imag_i_address0;
output   imag_i_ce0;
input  [31:0] imag_i_q0;
output  [9:0] real_o_address0;
output   real_o_ce0;
output   real_o_we0;
output  [31:0] real_o_d0;
output  [9:0] real_o_address1;
output   real_o_ce1;
output   real_o_we1;
output  [31:0] real_o_d1;
output  [9:0] imag_o_address0;
output   imag_o_ce0;
output   imag_o_we0;
output  [31:0] imag_o_d0;
output  [9:0] imag_o_address1;
output   imag_o_ce1;
output   imag_o_we1;
output  [31:0] imag_o_d1;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg[9:0] real_i_address0;
reg real_i_ce0;
reg[9:0] imag_i_address0;
reg imag_i_ce0;
reg real_o_ce0;
reg real_o_we0;
reg real_o_ce1;
reg real_o_we1;
reg imag_o_ce0;
reg imag_o_we0;
reg imag_o_ce1;
reg imag_o_we1;
(* fsm_encoding = "none" *) reg   [18:0] ap_CS_fsm = 19'b1;
reg    ap_sig_cseq_ST_st1_fsm_0;
reg    ap_sig_bdd_35;
wire   [8:0] W_real_address0;
reg    W_real_ce0;
wire   [31:0] W_real_q0;
wire   [8:0] W_imag_address0;
reg    W_imag_ce0;
wire   [31:0] W_imag_q0;
reg   [31:0] reg_239;
reg    ap_sig_cseq_ST_st5_fsm_4;
reg    ap_sig_bdd_96;
reg    ap_sig_cseq_ST_st13_fsm_12;
reg    ap_sig_bdd_103;
reg   [31:0] reg_247;
wire   [31:0] grp_fu_203_p2;
reg   [31:0] reg_255;
reg    ap_sig_cseq_ST_st18_fsm_17;
reg    ap_sig_bdd_114;
wire   [31:0] grp_fu_207_p2;
reg   [31:0] reg_262;
wire   [31:0] i_cast1_fu_269_p1;
reg   [31:0] i_cast1_reg_325;
reg    ap_sig_cseq_ST_st2_fsm_1;
reg    ap_sig_bdd_126;
wire   [0:0] i_phi_fu_185_p4;
reg   [31:0] c_reg_340;
reg    ap_sig_cseq_ST_st3_fsm_2;
reg    ap_sig_bdd_142;
reg   [31:0] s_reg_346;
wire  signed [63:0] tmp_2_fu_301_p1;
reg  signed [63:0] tmp_2_reg_355;
reg    ap_sig_cseq_ST_st4_fsm_3;
reg    ap_sig_bdd_152;
wire   [0:0] icmp_fu_289_p2;
wire   [9:0] k_1_fu_307_p2;
wire   [31:0] grp_fu_219_p2;
reg   [31:0] tmp_3_reg_376;
reg    ap_sig_cseq_ST_st8_fsm_7;
reg    ap_sig_bdd_172;
wire   [31:0] grp_fu_224_p2;
reg   [31:0] tmp_4_reg_381;
wire   [31:0] grp_fu_229_p2;
reg   [31:0] tmp_5_reg_386;
wire   [31:0] grp_fu_234_p2;
reg   [31:0] tmp_6_reg_391;
wire  signed [63:0] tmp_7_fu_313_p1;
reg  signed [63:0] tmp_7_reg_396;
reg    ap_sig_cseq_ST_st12_fsm_11;
reg    ap_sig_bdd_187;
wire   [31:0] i_1_fu_319_p2;
reg   [31:0] i_1_reg_412;
wire   [31:0] grp_fu_211_p2;
reg   [31:0] tmp_s_reg_417;
wire   [31:0] grp_fu_215_p2;
reg   [31:0] tmp_10_reg_422;
reg   [9:0] k_reg_168;
reg   [0:0] i_reg_180;
reg   [31:0] i1_reg_193;
reg    ap_sig_cseq_ST_st19_fsm_18;
reg    ap_sig_bdd_213;
wire   [63:0] tmp_fu_273_p1;
reg   [31:0] grp_fu_203_p0;
reg   [31:0] grp_fu_203_p1;
reg    ap_sig_cseq_ST_st9_fsm_8;
reg    ap_sig_bdd_229;
reg    ap_sig_cseq_ST_st14_fsm_13;
reg    ap_sig_bdd_236;
reg   [31:0] grp_fu_207_p0;
reg   [31:0] grp_fu_207_p1;
wire   [21:0] tmp_11_fu_279_p4;
wire   [31:0] i_lower_fu_295_p2;
wire    grp_fu_203_ce;
reg   [1:0] grp_fu_207_opcode;
wire    grp_fu_207_ce;
wire    grp_fu_211_ce;
wire    grp_fu_215_ce;
wire    grp_fu_219_ce;
wire    grp_fu_224_ce;
wire    grp_fu_229_ce;
wire    grp_fu_234_ce;
reg   [18:0] ap_NS_fsm;


fft_stage_first_W_real #(
    .DataWidth( 32 ),
    .AddressRange( 512 ),
    .AddressWidth( 9 ))
W_real_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( W_real_address0 ),
    .ce0( W_real_ce0 ),
    .q0( W_real_q0 )
);

fft_stage_first_W_imag #(
    .DataWidth( 32 ),
    .AddressRange( 512 ),
    .AddressWidth( 9 ))
W_imag_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( W_imag_address0 ),
    .ce0( W_imag_ce0 ),
    .q0( W_imag_q0 )
);

fft_stage_first_fsub_32ns_32ns_32_5_full_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 5 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fft_stage_first_fsub_32ns_32ns_32_5_full_dsp_U0(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( grp_fu_203_p0 ),
    .din1( grp_fu_203_p1 ),
    .ce( grp_fu_203_ce ),
    .dout( grp_fu_203_p2 )
);

fft_stage_first_faddfsub_32ns_32ns_32_5_full_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 5 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fft_stage_first_faddfsub_32ns_32ns_32_5_full_dsp_U1(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( grp_fu_207_p0 ),
    .din1( grp_fu_207_p1 ),
    .opcode( grp_fu_207_opcode ),
    .ce( grp_fu_207_ce ),
    .dout( grp_fu_207_p2 )
);

fft_stage_first_fadd_32ns_32ns_32_5_full_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 5 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fft_stage_first_fadd_32ns_32ns_32_5_full_dsp_U2(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( reg_239 ),
    .din1( reg_255 ),
    .ce( grp_fu_211_ce ),
    .dout( grp_fu_211_p2 )
);

fft_stage_first_fadd_32ns_32ns_32_5_full_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 5 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fft_stage_first_fadd_32ns_32ns_32_5_full_dsp_U3(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( reg_247 ),
    .din1( reg_262 ),
    .ce( grp_fu_215_ce ),
    .dout( grp_fu_215_p2 )
);

fft_stage_first_fmul_32ns_32ns_32_4_max_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 4 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fft_stage_first_fmul_32ns_32ns_32_4_max_dsp_U4(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( real_i_q0 ),
    .din1( c_reg_340 ),
    .ce( grp_fu_219_ce ),
    .dout( grp_fu_219_p2 )
);

fft_stage_first_fmul_32ns_32ns_32_4_max_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 4 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fft_stage_first_fmul_32ns_32ns_32_4_max_dsp_U5(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( imag_i_q0 ),
    .din1( s_reg_346 ),
    .ce( grp_fu_224_ce ),
    .dout( grp_fu_224_p2 )
);

fft_stage_first_fmul_32ns_32ns_32_4_max_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 4 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fft_stage_first_fmul_32ns_32ns_32_4_max_dsp_U6(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( imag_i_q0 ),
    .din1( c_reg_340 ),
    .ce( grp_fu_229_ce ),
    .dout( grp_fu_229_p2 )
);

fft_stage_first_fmul_32ns_32ns_32_4_max_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 4 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
fft_stage_first_fmul_32ns_32ns_32_4_max_dsp_U7(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( real_i_q0 ),
    .din1( s_reg_346 ),
    .ce( grp_fu_234_ce ),
    .dout( grp_fu_234_p2 )
);



always @ (posedge ap_clk) begin : ap_ret_ap_CS_fsm
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_st1_fsm_0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18)) begin
        i1_reg_193 <= i_1_reg_412;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        i1_reg_193 <= i_cast1_reg_325;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) & (ap_const_lv1_0 == icmp_fu_289_p2))) begin
        i_reg_180 <= ap_const_lv1_1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        i_reg_180 <= ap_const_lv1_0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) & (ap_const_lv1_0 == icmp_fu_289_p2))) begin
        k_reg_168 <= k_1_fu_307_p2;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        k_reg_168 <= ap_const_lv10_0;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        c_reg_340 <= W_real_q0;
        s_reg_346 <= W_imag_q0;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11)) begin
        i_1_reg_412 <= i_1_fu_319_p2;
        tmp_7_reg_396 <= tmp_7_fu_313_p1;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        i_cast1_reg_325[0] <= i_cast1_fu_269_p1[0];
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_4) | (ap_const_logic_1 == ap_sig_cseq_ST_st13_fsm_12))) begin
        reg_239 <= real_i_q0;
        reg_247 <= imag_i_q0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st13_fsm_12) | (ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17))) begin
        reg_255 <= grp_fu_203_p2;
        reg_262 <= grp_fu_207_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17)) begin
        tmp_10_reg_422 <= grp_fu_215_p2;
        tmp_s_reg_417 <= grp_fu_211_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) & ~(ap_const_lv1_0 == icmp_fu_289_p2))) begin
        tmp_2_reg_355 <= tmp_2_fu_301_p1;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7)) begin
        tmp_3_reg_376 <= grp_fu_219_p2;
        tmp_4_reg_381 <= grp_fu_224_p2;
        tmp_5_reg_386 <= grp_fu_229_p2;
        tmp_6_reg_391 <= grp_fu_234_p2;
    end
end

always @ (ap_sig_cseq_ST_st2_fsm_1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        W_imag_ce0 = ap_const_logic_1;
    end else begin
        W_imag_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st2_fsm_1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        W_real_ce0 = ap_const_logic_1;
    end else begin
        W_real_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st2_fsm_1 or i_phi_fu_185_p4) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~(i_phi_fu_185_p4 == ap_const_lv1_0))) begin
        ap_done = ap_const_logic_1;
    end else begin
        ap_done = ap_const_logic_0;
    end
end

always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if ((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0))) begin
        ap_idle = ap_const_logic_1;
    end else begin
        ap_idle = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st2_fsm_1 or i_phi_fu_185_p4) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~(i_phi_fu_185_p4 == ap_const_lv1_0))) begin
        ap_ready = ap_const_logic_1;
    end else begin
        ap_ready = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_187) begin
    if (ap_sig_bdd_187) begin
        ap_sig_cseq_ST_st12_fsm_11 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st12_fsm_11 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_103) begin
    if (ap_sig_bdd_103) begin
        ap_sig_cseq_ST_st13_fsm_12 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st13_fsm_12 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_236) begin
    if (ap_sig_bdd_236) begin
        ap_sig_cseq_ST_st14_fsm_13 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st14_fsm_13 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_114) begin
    if (ap_sig_bdd_114) begin
        ap_sig_cseq_ST_st18_fsm_17 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st18_fsm_17 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_213) begin
    if (ap_sig_bdd_213) begin
        ap_sig_cseq_ST_st19_fsm_18 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st19_fsm_18 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_35) begin
    if (ap_sig_bdd_35) begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_126) begin
    if (ap_sig_bdd_126) begin
        ap_sig_cseq_ST_st2_fsm_1 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st2_fsm_1 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_142) begin
    if (ap_sig_bdd_142) begin
        ap_sig_cseq_ST_st3_fsm_2 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st3_fsm_2 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_152) begin
    if (ap_sig_bdd_152) begin
        ap_sig_cseq_ST_st4_fsm_3 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st4_fsm_3 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_96) begin
    if (ap_sig_bdd_96) begin
        ap_sig_cseq_ST_st5_fsm_4 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st5_fsm_4 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_172) begin
    if (ap_sig_bdd_172) begin
        ap_sig_cseq_ST_st8_fsm_7 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st8_fsm_7 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_229) begin
    if (ap_sig_bdd_229) begin
        ap_sig_cseq_ST_st9_fsm_8 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st9_fsm_8 = ap_const_logic_0;
    end
end

always @ (reg_239 or tmp_3_reg_376 or ap_sig_cseq_ST_st9_fsm_8 or ap_sig_cseq_ST_st14_fsm_13) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st14_fsm_13)) begin
        grp_fu_203_p0 = reg_239;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8)) begin
        grp_fu_203_p0 = tmp_3_reg_376;
    end else begin
        grp_fu_203_p0 = 'bx;
    end
end

always @ (reg_255 or tmp_4_reg_381 or ap_sig_cseq_ST_st9_fsm_8 or ap_sig_cseq_ST_st14_fsm_13) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st14_fsm_13)) begin
        grp_fu_203_p1 = reg_255;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8)) begin
        grp_fu_203_p1 = tmp_4_reg_381;
    end else begin
        grp_fu_203_p1 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_st9_fsm_8 or ap_sig_cseq_ST_st14_fsm_13) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st14_fsm_13)) begin
        grp_fu_207_opcode = ap_const_lv2_1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8)) begin
        grp_fu_207_opcode = ap_const_lv2_0;
    end else begin
        grp_fu_207_opcode = 'bx;
    end
end

always @ (reg_247 or tmp_5_reg_386 or ap_sig_cseq_ST_st9_fsm_8 or ap_sig_cseq_ST_st14_fsm_13) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st14_fsm_13)) begin
        grp_fu_207_p0 = reg_247;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8)) begin
        grp_fu_207_p0 = tmp_5_reg_386;
    end else begin
        grp_fu_207_p0 = 'bx;
    end
end

always @ (reg_262 or tmp_6_reg_391 or ap_sig_cseq_ST_st9_fsm_8 or ap_sig_cseq_ST_st14_fsm_13) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st14_fsm_13)) begin
        grp_fu_207_p1 = reg_262;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_8)) begin
        grp_fu_207_p1 = tmp_6_reg_391;
    end else begin
        grp_fu_207_p1 = 'bx;
    end
end

always @ (tmp_2_fu_301_p1 or ap_sig_cseq_ST_st4_fsm_3 or tmp_7_fu_313_p1 or ap_sig_cseq_ST_st12_fsm_11) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11)) begin
        imag_i_address0 = tmp_7_fu_313_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3)) begin
        imag_i_address0 = tmp_2_fu_301_p1;
    end else begin
        imag_i_address0 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_st4_fsm_3 or ap_sig_cseq_ST_st12_fsm_11) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) | (ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11))) begin
        imag_i_ce0 = ap_const_logic_1;
    end else begin
        imag_i_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st19_fsm_18) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18)) begin
        imag_o_ce0 = ap_const_logic_1;
    end else begin
        imag_o_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st19_fsm_18) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18)) begin
        imag_o_ce1 = ap_const_logic_1;
    end else begin
        imag_o_ce1 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st19_fsm_18) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18)) begin
        imag_o_we0 = ap_const_logic_1;
    end else begin
        imag_o_we0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st19_fsm_18) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18)) begin
        imag_o_we1 = ap_const_logic_1;
    end else begin
        imag_o_we1 = ap_const_logic_0;
    end
end

always @ (tmp_2_fu_301_p1 or ap_sig_cseq_ST_st4_fsm_3 or tmp_7_fu_313_p1 or ap_sig_cseq_ST_st12_fsm_11) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11)) begin
        real_i_address0 = tmp_7_fu_313_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3)) begin
        real_i_address0 = tmp_2_fu_301_p1;
    end else begin
        real_i_address0 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_st4_fsm_3 or ap_sig_cseq_ST_st12_fsm_11) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_3) | (ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11))) begin
        real_i_ce0 = ap_const_logic_1;
    end else begin
        real_i_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st19_fsm_18) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18)) begin
        real_o_ce0 = ap_const_logic_1;
    end else begin
        real_o_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st19_fsm_18) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18)) begin
        real_o_ce1 = ap_const_logic_1;
    end else begin
        real_o_ce1 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st19_fsm_18) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18)) begin
        real_o_we0 = ap_const_logic_1;
    end else begin
        real_o_we0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st19_fsm_18) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18)) begin
        real_o_we1 = ap_const_logic_1;
    end else begin
        real_o_we1 = ap_const_logic_0;
    end
end
always @ (ap_start or ap_CS_fsm or i_phi_fu_185_p4 or icmp_fu_289_p2) begin
    case (ap_CS_fsm)
        ap_ST_st1_fsm_0 : 
        begin
            if (~(ap_start == ap_const_logic_0)) begin
                ap_NS_fsm = ap_ST_st2_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end
        end
        ap_ST_st2_fsm_1 : 
        begin
            if (~(i_phi_fu_185_p4 == ap_const_lv1_0)) begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end else begin
                ap_NS_fsm = ap_ST_st3_fsm_2;
            end
        end
        ap_ST_st3_fsm_2 : 
        begin
            ap_NS_fsm = ap_ST_st4_fsm_3;
        end
        ap_ST_st4_fsm_3 : 
        begin
            if ((ap_const_lv1_0 == icmp_fu_289_p2)) begin
                ap_NS_fsm = ap_ST_st2_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st5_fsm_4;
            end
        end
        ap_ST_st5_fsm_4 : 
        begin
            ap_NS_fsm = ap_ST_st6_fsm_5;
        end
        ap_ST_st6_fsm_5 : 
        begin
            ap_NS_fsm = ap_ST_st7_fsm_6;
        end
        ap_ST_st7_fsm_6 : 
        begin
            ap_NS_fsm = ap_ST_st8_fsm_7;
        end
        ap_ST_st8_fsm_7 : 
        begin
            ap_NS_fsm = ap_ST_st9_fsm_8;
        end
        ap_ST_st9_fsm_8 : 
        begin
            ap_NS_fsm = ap_ST_st10_fsm_9;
        end
        ap_ST_st10_fsm_9 : 
        begin
            ap_NS_fsm = ap_ST_st11_fsm_10;
        end
        ap_ST_st11_fsm_10 : 
        begin
            ap_NS_fsm = ap_ST_st12_fsm_11;
        end
        ap_ST_st12_fsm_11 : 
        begin
            ap_NS_fsm = ap_ST_st13_fsm_12;
        end
        ap_ST_st13_fsm_12 : 
        begin
            ap_NS_fsm = ap_ST_st14_fsm_13;
        end
        ap_ST_st14_fsm_13 : 
        begin
            ap_NS_fsm = ap_ST_st15_fsm_14;
        end
        ap_ST_st15_fsm_14 : 
        begin
            ap_NS_fsm = ap_ST_st16_fsm_15;
        end
        ap_ST_st16_fsm_15 : 
        begin
            ap_NS_fsm = ap_ST_st17_fsm_16;
        end
        ap_ST_st17_fsm_16 : 
        begin
            ap_NS_fsm = ap_ST_st18_fsm_17;
        end
        ap_ST_st18_fsm_17 : 
        begin
            ap_NS_fsm = ap_ST_st19_fsm_18;
        end
        ap_ST_st19_fsm_18 : 
        begin
            ap_NS_fsm = ap_ST_st4_fsm_3;
        end
        default : 
        begin
            ap_NS_fsm = 'bx;
        end
    endcase
end


assign W_imag_address0 = tmp_fu_273_p1;

assign W_real_address0 = tmp_fu_273_p1;


always @ (ap_CS_fsm) begin
    ap_sig_bdd_103 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_C]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_114 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_11]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_126 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_1]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_142 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_2]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_152 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_3]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_172 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_7]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_187 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_B]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_213 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_12]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_229 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_8]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_236 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_D]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_35 = (ap_CS_fsm[ap_const_lv32_0] == ap_const_lv1_1);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_96 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_4]);
end

assign grp_fu_203_ce = ap_const_logic_1;

assign grp_fu_207_ce = ap_const_logic_1;

assign grp_fu_211_ce = ap_const_logic_1;

assign grp_fu_215_ce = ap_const_logic_1;

assign grp_fu_219_ce = ap_const_logic_1;

assign grp_fu_224_ce = ap_const_logic_1;

assign grp_fu_229_ce = ap_const_logic_1;

assign grp_fu_234_ce = ap_const_logic_1;

assign i_1_fu_319_p2 = (i1_reg_193 + ap_const_lv32_2);

assign i_cast1_fu_269_p1 = i_reg_180;

assign i_lower_fu_295_p2 = (i1_reg_193 + ap_const_lv32_1);

assign i_phi_fu_185_p4 = i_reg_180;

assign icmp_fu_289_p2 = ($signed(tmp_11_fu_279_p4) < $signed(22'b1)? 1'b1: 1'b0);

assign imag_o_address0 = tmp_2_reg_355;

assign imag_o_address1 = tmp_7_reg_396;

assign imag_o_d0 = reg_262;

assign imag_o_d1 = tmp_10_reg_422;

assign k_1_fu_307_p2 = (k_reg_168 ^ ap_const_lv10_200);

assign real_o_address0 = tmp_2_reg_355;

assign real_o_address1 = tmp_7_reg_396;

assign real_o_d0 = reg_255;

assign real_o_d1 = tmp_s_reg_417;

assign tmp_11_fu_279_p4 = {{i1_reg_193[ap_const_lv32_1F : ap_const_lv32_A]}};

assign tmp_2_fu_301_p1 = $signed(i_lower_fu_295_p2);

assign tmp_7_fu_313_p1 = $signed(i1_reg_193);

assign tmp_fu_273_p1 = k_reg_168;
always @ (posedge ap_clk) begin
    i_cast1_reg_325[31:1] <= 31'b0000000000000000000000000000000;
end



endmodule //fft_stage_first

