############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2015 Xilinx Inc. All rights reserved.
############################################################
open_project fft_stage_first
set_top fft_stage_first
add_files fft_stage_first.h
add_files fft_stage_first.cpp
add_files -tb out.gold.dat
add_files -tb fft_stage_first_test.cpp
open_solution "solution1"
set_part {xc7z020clg484-1}
create_clock -period 10 -name default
#source "./fft_stage_first/solution1/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -format ip_catalog
