; ModuleID = 'D:/Projects/vivado/project_4/HLS/1_Subcomponents/fft_stage_first/fft_stage_first/solution1/.autopilot/db/a.o.3.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a] ; [#uses=0 type=[1 x void ()*]*]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535] ; [#uses=0 type=[1 x i32]*]
@fft_stage_first_str = internal unnamed_addr constant [16 x i8] c"fft_stage_first\00" ; [#uses=1 type=[16 x i8]*]
@W_real = internal unnamed_addr constant [512 x float] [float 1.000000e+00, float 0x3FEFFFD820000000, float 0x3FEFFF62C0000000, float 0x3FEFFE9DA0000000, float 0x3FEFFD88C0000000, float 0x3FEFFC2440000000, float 0x3FEFFA7220000000, float 0x3FEFF87260000000, float 0x3FEFF620E0000000, float 0x3FEFF38400000000, float 0x3FEFF09520000000, float 0x3FEFED58C0000000, float 0x3FEFE9CCC0000000, float 0x3FEFE5F300000000, float 0x3FEFE1CBC0000000, float 0x3FEFDD52C0000000, float 0x3FEFD88E40000000, float 0x3FEFD37A00000000, float 0x3FEFCE1600000000, float 0x3FEFC86480000000, float 0x3FEFC26560000000, float 0x3FEFBC16A0000000, float 0x3FEFB57A40000000, float 0x3FEFAE8E20000000, float 0x3FEFA75680000000, float 0x3FEF9FCF40000000, float 0x3FEF97F840000000, float 0x3FEF8FD5C0000000, float 0x3FEF8765C0000000, float 0x3FEF7EA600000000, float 0x3FEF7598A0000000, float 0x3FEF6C3FC0000000, float 0x3FEF629740000000, float 0x3FEF58A320000000, float 0x3FEF4E5F80000000, float 0x3FEF43D040000000, float 0x3FEF38F360000000, float 0x3FEF2DC900000000, float 0x3FEF225320000000, float 0x3FEF168F80000000, float 0x3FEF0A7E80000000, float 0x3FEEFE21E0000000, float 0x3FEEF177A0000000, float 0x3FEEE48200000000, float 0x3FEED740C0000000, float 0x3FEEC9B200000000, float 0x3FEEBBD9C0000000, float 0x3FEEADB1E0000000, float 0x3FEE9F40A0000000, float 0x3FEE9083E0000000, float 0x3FEE817BA0000000, float 0x3FEE7227E0000000, float 0x3FEE628880000000, float 0x3FEE529FE0000000, float 0x3FEE4269A0000000, float 0x3FEE31EA00000000, float 0x3FEE2120E0000000, float 0x3FEE100C60000000, float 0x3FEDFEAE60000000, float 0x3FEDED0700000000, float 0x3FEDDB1420000000, float 0x3FEDC8D7E0000000, float 0x3FEDB65240000000, float 0x3FEDA38320000000, float 0x3FED906CC0000000, float 0x3FED7D0AE0000000, float 0x3FED6961C0000000, float 0x3FED556F40000000, float 0x3FED413560000000, float 0x3FED2CB200000000, float 0x3FED17E780000000, float 0x3FED02D5A0000000, float 0x3FECED7A60000000, float 0x3FECD7D9E0000000, float 0x3FECC1F000000000, float 0x3FECABC0E0000000, float 0x3FEC954A80000000, float 0x3FEC7E8EE0000000, float 0x3FEC678C00000000, float 0x3FEC5041C0000000, float 0x3FEC38B260000000, float 0x3FEC20DDC0000000, float 0x3FEC08C400000000, float 0x3FEBF064E0000000, float 0x3FEBD7C0C0000000, float 0x3FEBBED740000000, float 0x3FEBA5AAC0000000, float 0x3FEB8C3900000000, float 0x3FEB728420000000, float 0x3FEB588A00000000, float 0x3FEB3E4CE0000000, float 0x3FEB23CC80000000, float 0x3FEB090B40000000, float 0x3FEAEE04C0000000, float 0x3FEAD2BD40000000, float 0x3FEAB732A0000000, float 0x3FEA9B6700000000, float 0x3FEA7F5840000000, float 0x3FEA630880000000, float 0x3FEA4677C0000000, float 0x3FEA29A800000000, float 0x3FEA0C9540000000, float 0x3FE9EF4380000000, float 0x3FE9D1B2E0000000, float 0x3FE9B3E140000000, float 0x3FE995CEA0000000, float 0x3FE9777F20000000, float 0x3FE958F0C0000000, float 0x3FE93A2160000000, float 0x3FE91B1740000000, float 0x3FE8FBCC20000000, float 0x3FE8DC4440000000, float 0x3FE8BC7F80000000, float 0x3FE89C7DE0000000, float 0x3FE87C3F80000000, float 0x3FE85BC440000000, float 0x3FE83B0E60000000, float 0x3FE81A1BA0000000, float 0x3FE7F8EC00000000, float 0x3FE7D783E0000000, float 0x3FE7B5DEE0000000, float 0x3FE7940140000000, float 0x3FE771E6C0000000, float 0x3FE74F93C0000000, float 0x3FE72D0800000000, float 0x3FE70A41A0000000, float 0x3FE6E744C0000000, float 0x3FE6C40D00000000, float 0x3FE6A09EE0000000, float 0x3FE67CF800000000, float 0x3FE65918A0000000, float 0x3FE63502A0000000, float 0x3FE610B840000000, float 0x3FE5EC3540000000, float 0x3FE5C77BC0000000, float 0x3FE5A28DC0000000, float 0x3FE57D6940000000, float 0x3FE5581060000000, float 0x3FE5328300000000, float 0x3FE50CC140000000, float 0x3FE4E6CB20000000, float 0x3FE4C0A060000000, float 0x3FE49A45A0000000, float 0x3FE473B420000000, float 0x3FE44CF280000000, float 0x3FE425FEA0000000, float 0x3FE3FED840000000, float 0x3FE3D781C0000000, float 0x3FE3AFFB00000000, float 0x3FE38841E0000000, float 0x3FE36058A0000000, float 0x3FE3383F00000000, float 0x3FE30FF760000000, float 0x3FE2E78180000000, float 0x3FE2BEDB80000000, float 0x3FE2960740000000, float 0x3FE26D04E0000000, float 0x3FE243D680000000, float 0x3FE21A7A00000000, float 0x3FE1F0F160000000, float 0x3FE1C73AC0000000, float 0x3FE19D5A20000000, float 0x3FE1734D60000000, float 0x3FE14914C0000000, float 0x3FE11EB420000000, float 0x3FE0F42780000000, float 0x3FE0C97100000000, float 0x3FE09E9080000000, float 0x3FE0738820000000, float 0x3FE04855E0000000, float 0x3FE01CFBC0000000, float 0x3FDFE2F7C0000000, float 0x3FDF8BA400000000, float 0x3FDF3404E0000000, float 0x3FDEDC1A40000000, float 0x3FDE83E000000000, float 0x3FDE2B5E60000000, float 0x3FDDD28D00000000, float 0x3FDD7978A0000000, float 0x3FDD2018A0000000, float 0x3FDCC66D40000000, float 0x3FDC6C7EA0000000, float 0x3FDC1248E0000000, float 0x3FDBB7CFE0000000, float 0x3FDB5D0FA0000000, float 0x3FDB020C40000000, float 0x3FDAA6CA00000000, float 0x3FDA4B4080000000, float 0x3FD9EF7800000000, float 0x3FD9937080000000, float 0x3FD9372A40000000, float 0x3FD8DAA500000000, float 0x3FD87DE0E0000000, float 0x3FD820E200000000, float 0x3FD7C3A860000000, float 0x3FD7663420000000, float 0x3FD7088500000000, float 0x3FD6AA9B40000000, float 0x3FD64C7F40000000, float 0x3FD5EE2860000000, float 0x3FD58F9B20000000, float 0x3FD530D740000000, float 0x3FD4D1E100000000, float 0x3FD472B880000000, float 0x3FD4135DA0000000, float 0x3FD3B3D080000000, float 0x3FD3541100000000, float 0x3FD2F42360000000, float 0x3FD2940780000000, float 0x3FD233B960000000, float 0x3FD1D34580000000, float 0x3FD1729F60000000, float 0x3FD111D360000000, float 0x3FD0B0D960000000, float 0x3FD04FB980000000, float 0x3FCFDCDF60000000, float 0x3FCF19F800000000, float 0x3FCE56CD60000000, float 0x3FCD934F00000000, float 0x3FCCCF8D80000000, float 0x3FCC0B8060000000, float 0x3FCB473040000000, float 0x3FCA829D00000000, float 0x3FC9BDCF00000000, float 0x3FC8F8B580000000, float 0x3FC83369C0000000, float 0x3FC76DDAC0000000, float 0x3FC6A81120000000, float 0x3FC5E21540000000, float 0x3FC51BDE80000000, float 0x3FC4557580000000, float 0x3FC38EDA20000000, float 0x3FC2C80C80000000, float 0x3FC20114E0000000, float 0x3FC139F340000000, float 0x3FC0729F60000000, float 0x3FBF5653C0000000, float 0x3FBDC71500000000, float 0x3FBC378240000000, float 0x3FBAA7BD40000000, float 0x3FB917A460000000, float 0x3FB7875920000000, float 0x3FB5F6CAC0000000, float 0x3FB4660A20000000, float 0x3FB2D527E0000000, float 0x3FB1440280000000, float 0x3FAF657760000000, float 0x3FAC4284E0000000, float 0x3FA91F70E0000000, float 0x3FA5FBF840000000, float 0x3FA2D85E00000000, float 0x3F9F694460000000, float 0x3F992146A0000000, float 0x3F92D948E0000000, float 0x3F892189C0000000, float 0x3F79221000000000, float -0.000000e+00, float 0xBF79221000000000, float 0xBF89221000000000, float 0xBF92D948E0000000, float 0xBF992146A0000000, float 0xBF9F694460000000, float 0xBFA2D85E00000000, float 0xBFA5FBF840000000, float 0xBFA91F70E0000000, float 0xBFAC4284E0000000, float 0xBFAF657760000000, float 0xBFB1440280000000, float 0xBFB2D527E0000000, float 0xBFB4660A20000000, float 0xBFB5F6CAC0000000, float 0xBFB7875920000000, float 0xBFB917A460000000, float 0xBFBAA7BD40000000, float 0xBFBC378240000000, float 0xBFBDC71500000000, float 0xBFBF5653C0000000, float 0xBFC0729F60000000, float 0xBFC139F340000000, float 0xBFC20114E0000000, float 0xBFC2C814E0000000, float 0xBFC38EDA20000000, float 0xBFC4557580000000, float 0xBFC51BDE80000000, float 0xBFC5E21540000000, float 0xBFC6A81120000000, float 0xBFC76DDAC0000000, float 0xBFC83369C0000000, float 0xBFC8F8B580000000, float 0xBFC9BDCF00000000, float 0xBFCA829D00000000, float 0xBFCB473040000000, float 0xBFCC0B8060000000, float 0xBFCCCF8D80000000, float 0xBFCD934F00000000, float 0xBFCE56CD60000000, float 0xBFCF19F800000000, float 0xBFCFDCDF60000000, float 0xBFD04FB980000000, float 0xBFD0B0D960000000, float 0xBFD111D360000000, float 0xBFD1729F60000000, float 0xBFD1D34580000000, float 0xBFD233BDA0000000, float 0xBFD2940780000000, float 0xBFD2F42360000000, float 0xBFD3541100000000, float 0xBFD3B3D080000000, float 0xBFD4135DA0000000, float 0xBFD472B880000000, float 0xBFD4D1E100000000, float 0xBFD530D740000000, float 0xBFD58F9B20000000, float 0xBFD5EE2860000000, float 0xBFD64C7F40000000, float 0xBFD6AA9F80000000, float 0xBFD7088500000000, float 0xBFD7663420000000, float 0xBFD7C3A860000000, float 0xBFD820E200000000, float 0xBFD87DE0E0000000, float 0xBFD8DAA500000000, float 0xBFD9372A40000000, float 0xBFD9937080000000, float 0xBFD9EF7800000000, float 0xBFDA4B4080000000, float 0xBFDAA6CA00000000, float 0xBFDB020C40000000, float 0xBFDB5D0FA0000000, float 0xBFDBB7CFE0000000, float 0xBFDC1248E0000000, float 0xBFDC6C7EA0000000, float 0xBFDCC66D40000000, float 0xBFDD2018A0000000, float 0xBFDD7978A0000000, float 0xBFDDD29140000000, float 0xBFDE2B5E60000000, float 0xBFDE83E000000000, float 0xBFDEDC1A40000000, float 0xBFDF3404E0000000, float 0xBFDF8BA400000000, float 0xBFDFE2F7C0000000, float 0xBFE01CFBC0000000, float 0xBFE04855E0000000, float 0xBFE0738820000000, float 0xBFE09E9080000000, float 0xBFE0C97100000000, float 0xBFE0F42780000000, float 0xBFE11EB420000000, float 0xBFE14916C0000000, float 0xBFE1734D60000000, float 0xBFE19D5A20000000, float 0xBFE1C73AC0000000, float 0xBFE1F0F160000000, float 0xBFE21A7A00000000, float 0xBFE243D680000000, float 0xBFE26D04E0000000, float 0xBFE2960740000000, float 0xBFE2BEDB80000000, float 0xBFE2E78180000000, float 0xBFE30FF760000000, float 0xBFE3384120000000, float 0xBFE36058A0000000, float 0xBFE38841E0000000, float 0xBFE3AFFB00000000, float 0xBFE3D781C0000000, float 0xBFE3FEDA60000000, float 0xBFE425FEA0000000, float 0xBFE44CF280000000, float 0xBFE473B640000000, float 0xBFE49A45A0000000, float 0xBFE4C0A060000000, float 0xBFE4E6CB20000000, float 0xBFE50CC140000000, float 0xBFE5328300000000, float 0xBFE5581060000000, float 0xBFE57D6940000000, float 0xBFE5A28DC0000000, float 0xBFE5C77BC0000000, float 0xBFE5EC3540000000, float 0xBFE610B840000000, float 0xBFE63504C0000000, float 0xBFE65918A0000000, float 0xBFE67CF800000000, float 0xBFE6A09EE0000000, float 0xBFE6C40D00000000, float 0xBFE6E744C0000000, float 0xBFE70A43C0000000, float 0xBFE72D0800000000, float 0xBFE74F93C0000000, float 0xBFE771E6C0000000, float 0xBFE7940140000000, float 0xBFE7B5DEE0000000, float 0xBFE7D783E0000000, float 0xBFE7F8EC00000000, float 0xBFE81A1BA0000000, float 0xBFE83B0E60000000, float 0xBFE85BC440000000, float 0xBFE87C3F80000000, float 0xBFE89C7DE0000000, float 0xBFE8BC7F80000000, float 0xBFE8DC4440000000, float 0xBFE8FBCC20000000, float 0xBFE91B1740000000, float 0xBFE93A2160000000, float 0xBFE958F0C0000000, float 0xBFE9777F20000000, float 0xBFE995CEA0000000, float 0xBFE9B3E140000000, float 0xBFE9D1B2E0000000, float 0xBFE9EF4380000000, float 0xBFEA0C9540000000, float 0xBFEA29A800000000, float 0xBFEA4679C0000000, float 0xBFEA630880000000, float 0xBFEA7F5840000000, float 0xBFEA9B6700000000, float 0xBFEAB732A0000000, float 0xBFEAD2BD40000000, float 0xBFEAEE04C0000000, float 0xBFEB090B40000000, float 0xBFEB23CC80000000, float 0xBFEB3E4CE0000000, float 0xBFEB588A00000000, float 0xBFEB728420000000, float 0xBFEB8C3900000000, float 0xBFEBA5AAC0000000, float 0xBFEBBED740000000, float 0xBFEBD7C0C0000000, float 0xBFEBF064E0000000, float 0xBFEC08C400000000, float 0xBFEC20DDC0000000, float 0xBFEC38B260000000, float 0xBFEC5041C0000000, float 0xBFEC678C00000000, float 0xBFEC7E8EE0000000, float 0xBFEC954A80000000, float 0xBFECABC0E0000000, float 0xBFECC1F000000000, float 0xBFECD7D9E0000000, float 0xBFECED7A60000000, float 0xBFED02D5A0000000, float 0xBFED17E780000000, float 0xBFED2CB200000000, float 0xBFED413560000000, float 0xBFED556F40000000, float 0xBFED6961C0000000, float 0xBFED7D0AE0000000, float 0xBFED906CC0000000, float 0xBFEDA38320000000, float 0xBFEDB65240000000, float 0xBFEDC8D7E0000000, float 0xBFEDDB1420000000, float 0xBFEDED0700000000, float 0xBFEDFEAE60000000, float 0xBFEE100C60000000, float 0xBFEE2120E0000000, float 0xBFEE31EC00000000, float 0xBFEE4269A0000000, float 0xBFEE529FE0000000, float 0xBFEE628880000000, float 0xBFEE7227E0000000, float 0xBFEE817BA0000000, float 0xBFEE9083E0000000, float 0xBFEE9F40A0000000, float 0xBFEEADB400000000, float 0xBFEEBBD9C0000000, float 0xBFEEC9B200000000, float 0xBFEED740C0000000, float 0xBFEEE48200000000, float 0xBFEEF177A0000000, float 0xBFEEFE21E0000000, float 0xBFEF0A7E80000000, float 0xBFEF168F80000000, float 0xBFEF225320000000, float 0xBFEF2DC900000000, float 0xBFEF38F360000000, float 0xBFEF43D040000000, float 0xBFEF4E5F80000000, float 0xBFEF58A320000000, float 0xBFEF629740000000, float 0xBFEF6C3FC0000000, float 0xBFEF7598A0000000, float 0xBFEF7EA600000000, float 0xBFEF8765C0000000, float 0xBFEF8FD5C0000000, float 0xBFEF97F840000000, float 0xBFEF9FCF40000000, float 0xBFEFA75680000000, float 0xBFEFAE8E20000000, float 0xBFEFB57A40000000, float 0xBFEFBC16A0000000, float 0xBFEFC26560000000, float 0xBFEFC86480000000, float 0xBFEFCE1600000000, float 0xBFEFD37A00000000, float 0xBFEFD88E40000000, float 0xBFEFDD52C0000000, float 0xBFEFE1CBC0000000, float 0xBFEFE5F300000000, float 0xBFEFE9CCC0000000, float 0xBFEFED58C0000000, float 0xBFEFF09520000000, float 0xBFEFF38400000000, float 0xBFEFF620E0000000, float 0xBFEFF87260000000, float 0xBFEFFA7220000000, float 0xBFEFFC2440000000, float 0xBFEFFD88C0000000, float 0xBFEFFE9DA0000000, float 0xBFEFFF62C0000000, float 0xBFEFFFD820000000], align 16 ; [#uses=1 type=[512 x float]*]
@W_imag = internal unnamed_addr constant [512 x float] [float -0.000000e+00, float 0xBF79221000000000, float 0xBF89221000000000, float 0xBF92D948E0000000, float 0xBF992146A0000000, float 0xBF9F694460000000, float 0xBFA2D85E00000000, float 0xBFA5FBF840000000, float 0xBFA91F70E0000000, float 0xBFAC4284E0000000, float 0xBFAF657760000000, float 0xBFB1440280000000, float 0xBFB2D527E0000000, float 0xBFB4660A20000000, float 0xBFB5F6CAC0000000, float 0xBFB7875920000000, float 0xBFB917A460000000, float 0xBFBAA7BD40000000, float 0xBFBC378240000000, float 0xBFBDC71500000000, float 0xBFBF5653C0000000, float 0xBFC0729F60000000, float 0xBFC139F340000000, float 0xBFC20114E0000000, float 0xBFC2C80C80000000, float 0xBFC38EDA20000000, float 0xBFC4557580000000, float 0xBFC51BDE80000000, float 0xBFC5E21540000000, float 0xBFC6A81120000000, float 0xBFC76DDAC0000000, float 0xBFC83369C0000000, float 0xBFC8F8B580000000, float 0xBFC9BDCF00000000, float 0xBFCA829D00000000, float 0xBFCB473040000000, float 0xBFCC0B8060000000, float 0xBFCCCF8D80000000, float 0xBFCD934F00000000, float 0xBFCE56CD60000000, float 0xBFCF19F800000000, float 0xBFCFDCDF60000000, float 0xBFD04FB980000000, float 0xBFD0B0D960000000, float 0xBFD111D360000000, float 0xBFD1729F60000000, float 0xBFD1D34580000000, float 0xBFD233BDA0000000, float 0xBFD2940780000000, float 0xBFD2F42360000000, float 0xBFD3541100000000, float 0xBFD3B3D080000000, float 0xBFD4135DA0000000, float 0xBFD472B880000000, float 0xBFD4D1E100000000, float 0xBFD530D740000000, float 0xBFD58F9B20000000, float 0xBFD5EE2860000000, float 0xBFD64C7F40000000, float 0xBFD6AA9F80000000, float 0xBFD7088500000000, float 0xBFD7663420000000, float 0xBFD7C3A860000000, float 0xBFD820E200000000, float 0xBFD87DE0E0000000, float 0xBFD8DAA500000000, float 0xBFD9372A40000000, float 0xBFD9937080000000, float 0xBFD9EF7800000000, float 0xBFDA4B4080000000, float 0xBFDAA6CA00000000, float 0xBFDB020C40000000, float 0xBFDB5D0FA0000000, float 0xBFDBB7CFE0000000, float 0xBFDC1248E0000000, float 0xBFDC6C7EA0000000, float 0xBFDCC66D40000000, float 0xBFDD2018A0000000, float 0xBFDD7978A0000000, float 0xBFDDD29140000000, float 0xBFDE2B5E60000000, float 0xBFDE83E000000000, float 0xBFDEDC1A40000000, float 0xBFDF3404E0000000, float 0xBFDF8BA400000000, float 0xBFDFE2F7C0000000, float 0xBFE01CFBC0000000, float 0xBFE04855E0000000, float 0xBFE0738820000000, float 0xBFE09E9080000000, float 0xBFE0C97100000000, float 0xBFE0F42780000000, float 0xBFE11EB420000000, float 0xBFE14916C0000000, float 0xBFE1734D60000000, float 0xBFE19D5A20000000, float 0xBFE1C73AC0000000, float 0xBFE1F0F160000000, float 0xBFE21A7A00000000, float 0xBFE243D680000000, float 0xBFE26D04E0000000, float 0xBFE2960740000000, float 0xBFE2BEDB80000000, float 0xBFE2E78180000000, float 0xBFE30FF760000000, float 0xBFE3384120000000, float 0xBFE36058A0000000, float 0xBFE38841E0000000, float 0xBFE3AFFB00000000, float 0xBFE3D781C0000000, float 0xBFE3FEDA60000000, float 0xBFE425FEA0000000, float 0xBFE44CF280000000, float 0xBFE473B420000000, float 0xBFE49A45A0000000, float 0xBFE4C0A060000000, float 0xBFE4E6CB20000000, float 0xBFE50CC140000000, float 0xBFE5328300000000, float 0xBFE5581060000000, float 0xBFE57D6940000000, float 0xBFE5A28DC0000000, float 0xBFE5C77BC0000000, float 0xBFE5EC3540000000, float 0xBFE610B840000000, float 0xBFE63502A0000000, float 0xBFE65918A0000000, float 0xBFE67CF800000000, float 0xBFE6A09EE0000000, float 0xBFE6C40D00000000, float 0xBFE6E744C0000000, float 0xBFE70A43C0000000, float 0xBFE72D0800000000, float 0xBFE74F93C0000000, float 0xBFE771E6C0000000, float 0xBFE7940140000000, float 0xBFE7B5DEE0000000, float 0xBFE7D783E0000000, float 0xBFE7F8EC00000000, float 0xBFE81A1BA0000000, float 0xBFE83B0E60000000, float 0xBFE85BC440000000, float 0xBFE87C3F80000000, float 0xBFE89C7DE0000000, float 0xBFE8BC7F80000000, float 0xBFE8DC4440000000, float 0xBFE8FBCC20000000, float 0xBFE91B1740000000, float 0xBFE93A2160000000, float 0xBFE958F0C0000000, float 0xBFE9777F20000000, float 0xBFE995CEA0000000, float 0xBFE9B3E140000000, float 0xBFE9D1B2E0000000, float 0xBFE9EF4380000000, float 0xBFEA0C9540000000, float 0xBFEA29A800000000, float 0xBFEA4679C0000000, float 0xBFEA630880000000, float 0xBFEA7F5840000000, float 0xBFEA9B6700000000, float 0xBFEAB732A0000000, float 0xBFEAD2BD40000000, float 0xBFEAEE04C0000000, float 0xBFEB090B40000000, float 0xBFEB23CC80000000, float 0xBFEB3E4CE0000000, float 0xBFEB588A00000000, float 0xBFEB728420000000, float 0xBFEB8C3900000000, float 0xBFEBA5AAC0000000, float 0xBFEBBED740000000, float 0xBFEBD7C0C0000000, float 0xBFEBF064E0000000, float 0xBFEC08C400000000, float 0xBFEC20DDC0000000, float 0xBFEC38B260000000, float 0xBFEC5041C0000000, float 0xBFEC678C00000000, float 0xBFEC7E8EE0000000, float 0xBFEC954A80000000, float 0xBFECABC0E0000000, float 0xBFECC1F000000000, float 0xBFECD7D9E0000000, float 0xBFECED7A60000000, float 0xBFED02D5A0000000, float 0xBFED17E780000000, float 0xBFED2CB200000000, float 0xBFED413560000000, float 0xBFED556F40000000, float 0xBFED6961C0000000, float 0xBFED7D0AE0000000, float 0xBFED906CC0000000, float 0xBFEDA38320000000, float 0xBFEDB65240000000, float 0xBFEDC8D7E0000000, float 0xBFEDDB1420000000, float 0xBFEDED0700000000, float 0xBFEDFEAE60000000, float 0xBFEE100C60000000, float 0xBFEE2120E0000000, float 0xBFEE31EA00000000, float 0xBFEE4269A0000000, float 0xBFEE529FE0000000, float 0xBFEE628880000000, float 0xBFEE7227E0000000, float 0xBFEE817BA0000000, float 0xBFEE9083E0000000, float 0xBFEE9F40A0000000, float 0xBFEEADB1E0000000, float 0xBFEEBBD9C0000000, float 0xBFEEC9B200000000, float 0xBFEED740C0000000, float 0xBFEEE48200000000, float 0xBFEEF177A0000000, float 0xBFEEFE21E0000000, float 0xBFEF0A7E80000000, float 0xBFEF168F80000000, float 0xBFEF225320000000, float 0xBFEF2DC900000000, float 0xBFEF38F360000000, float 0xBFEF43D040000000, float 0xBFEF4E5F80000000, float 0xBFEF58A320000000, float 0xBFEF629740000000, float 0xBFEF6C3FC0000000, float 0xBFEF7598A0000000, float 0xBFEF7EA600000000, float 0xBFEF8765C0000000, float 0xBFEF8FD5C0000000, float 0xBFEF97F840000000, float 0xBFEF9FCF40000000, float 0xBFEFA75680000000, float 0xBFEFAE8E20000000, float 0xBFEFB57A40000000, float 0xBFEFBC16A0000000, float 0xBFEFC26560000000, float 0xBFEFC86480000000, float 0xBFEFCE1600000000, float 0xBFEFD37A00000000, float 0xBFEFD88E40000000, float 0xBFEFDD52C0000000, float 0xBFEFE1CBC0000000, float 0xBFEFE5F300000000, float 0xBFEFE9CCC0000000, float 0xBFEFED58C0000000, float 0xBFEFF09520000000, float 0xBFEFF38400000000, float 0xBFEFF620E0000000, float 0xBFEFF87260000000, float 0xBFEFFA7220000000, float 0xBFEFFC2440000000, float 0xBFEFFD88C0000000, float 0xBFEFFE9DA0000000, float 0xBFEFFF62C0000000, float 0xBFEFFFD820000000, float -1.000000e+00, float 0xBFEFFFD820000000, float 0xBFEFFF62C0000000, float 0xBFEFFE9DA0000000, float 0xBFEFFD88C0000000, float 0xBFEFFC2440000000, float 0xBFEFFA7220000000, float 0xBFEFF87260000000, float 0xBFEFF620E0000000, float 0xBFEFF38400000000, float 0xBFEFF09520000000, float 0xBFEFED58C0000000, float 0xBFEFE9CCC0000000, float 0xBFEFE5F300000000, float 0xBFEFE1CBC0000000, float 0xBFEFDD52C0000000, float 0xBFEFD88E40000000, float 0xBFEFD37A00000000, float 0xBFEFCE1600000000, float 0xBFEFC86480000000, float 0xBFEFC26560000000, float 0xBFEFBC16A0000000, float 0xBFEFB57A40000000, float 0xBFEFAE8E20000000, float 0xBFEFA75680000000, float 0xBFEF9FCF40000000, float 0xBFEF97F840000000, float 0xBFEF8FD5C0000000, float 0xBFEF8765C0000000, float 0xBFEF7EA600000000, float 0xBFEF7598A0000000, float 0xBFEF6C3FC0000000, float 0xBFEF629740000000, float 0xBFEF58A320000000, float 0xBFEF4E5F80000000, float 0xBFEF43D040000000, float 0xBFEF38F360000000, float 0xBFEF2DC900000000, float 0xBFEF225320000000, float 0xBFEF168F80000000, float 0xBFEF0A7E80000000, float 0xBFEEFE21E0000000, float 0xBFEEF177A0000000, float 0xBFEEE48200000000, float 0xBFEED740C0000000, float 0xBFEEC9B200000000, float 0xBFEEBBD9C0000000, float 0xBFEEADB1E0000000, float 0xBFEE9F40A0000000, float 0xBFEE9083E0000000, float 0xBFEE817BA0000000, float 0xBFEE7227E0000000, float 0xBFEE628880000000, float 0xBFEE529FE0000000, float 0xBFEE4269A0000000, float 0xBFEE31EA00000000, float 0xBFEE2120E0000000, float 0xBFEE100C60000000, float 0xBFEDFEAE60000000, float 0xBFEDED04E0000000, float 0xBFEDDB1420000000, float 0xBFEDC8D7E0000000, float 0xBFEDB65240000000, float 0xBFEDA38320000000, float 0xBFED906CC0000000, float 0xBFED7D0AE0000000, float 0xBFED6961C0000000, float 0xBFED556F40000000, float 0xBFED413560000000, float 0xBFED2CB200000000, float 0xBFED17E780000000, float 0xBFED02D5A0000000, float 0xBFECED7A60000000, float 0xBFECD7D9E0000000, float 0xBFECC1F000000000, float 0xBFECABC0E0000000, float 0xBFEC954A80000000, float 0xBFEC7E8EE0000000, float 0xBFEC678C00000000, float 0xBFEC5041C0000000, float 0xBFEC38B260000000, float 0xBFEC20DDC0000000, float 0xBFEC08C400000000, float 0xBFEBF064E0000000, float 0xBFEBD7C0C0000000, float 0xBFEBBED740000000, float 0xBFEBA5AAC0000000, float 0xBFEB8C3900000000, float 0xBFEB728420000000, float 0xBFEB588A00000000, float 0xBFEB3E4CE0000000, float 0xBFEB23CC80000000, float 0xBFEB090B40000000, float 0xBFEAEE04C0000000, float 0xBFEAD2BD40000000, float 0xBFEAB732A0000000, float 0xBFEA9B6700000000, float 0xBFEA7F5840000000, float 0xBFEA630880000000, float 0xBFEA4677C0000000, float 0xBFEA29A800000000, float 0xBFEA0C9540000000, float 0xBFE9EF4380000000, float 0xBFE9D1B2E0000000, float 0xBFE9B3E140000000, float 0xBFE995CEA0000000, float 0xBFE9777F20000000, float 0xBFE958F0C0000000, float 0xBFE93A2160000000, float 0xBFE91B1740000000, float 0xBFE8FBCC20000000, float 0xBFE8DC4440000000, float 0xBFE8BC7F80000000, float 0xBFE89C7DE0000000, float 0xBFE87C3F80000000, float 0xBFE85BC440000000, float 0xBFE83B0E60000000, float 0xBFE81A1BA0000000, float 0xBFE7F8EC00000000, float 0xBFE7D783E0000000, float 0xBFE7B5DEE0000000, float 0xBFE7940140000000, float 0xBFE771E6C0000000, float 0xBFE74F93C0000000, float 0xBFE72D0800000000, float 0xBFE70A41A0000000, float 0xBFE6E744C0000000, float 0xBFE6C40D00000000, float 0xBFE6A09EE0000000, float 0xBFE67CF800000000, float 0xBFE65918A0000000, float 0xBFE63502A0000000, float 0xBFE610B840000000, float 0xBFE5EC3540000000, float 0xBFE5C77BC0000000, float 0xBFE5A28DC0000000, float 0xBFE57D6940000000, float 0xBFE5581060000000, float 0xBFE5328300000000, float 0xBFE50CC140000000, float 0xBFE4E6CB20000000, float 0xBFE4C0A060000000, float 0xBFE49A4380000000, float 0xBFE473B420000000, float 0xBFE44CF280000000, float 0xBFE425FEA0000000, float 0xBFE3FED840000000, float 0xBFE3D781C0000000, float 0xBFE3AFFB00000000, float 0xBFE38841E0000000, float 0xBFE36058A0000000, float 0xBFE3383F00000000, float 0xBFE30FF760000000, float 0xBFE2E78180000000, float 0xBFE2BEDB80000000, float 0xBFE2960740000000, float 0xBFE26D04E0000000, float 0xBFE243D680000000, float 0xBFE21A7A00000000, float 0xBFE1F0EF60000000, float 0xBFE1C73AC0000000, float 0xBFE19D5A20000000, float 0xBFE1734D60000000, float 0xBFE14914C0000000, float 0xBFE11EB420000000, float 0xBFE0F42780000000, float 0xBFE0C97100000000, float 0xBFE09E9080000000, float 0xBFE0738820000000, float 0xBFE04855E0000000, float 0xBFE01CFBC0000000, float 0xBFDFE2F7C0000000, float 0xBFDF8BA400000000, float 0xBFDF3404E0000000, float 0xBFDEDC1A40000000, float 0xBFDE83E000000000, float 0xBFDE2B5E60000000, float 0xBFDDD28D00000000, float 0xBFDD7978A0000000, float 0xBFDD2018A0000000, float 0xBFDCC66D40000000, float 0xBFDC6C7EA0000000, float 0xBFDC1248E0000000, float 0xBFDBB7CFE0000000, float 0xBFDB5D0FA0000000, float 0xBFDB020C40000000, float 0xBFDAA6C5E0000000, float 0xBFDA4B4080000000, float 0xBFD9EF7800000000, float 0xBFD9937080000000, float 0xBFD9372A40000000, float 0xBFD8DAA500000000, float 0xBFD87DE0E0000000, float 0xBFD820E200000000, float 0xBFD7C3A860000000, float 0xBFD7663420000000, float 0xBFD7088500000000, float 0xBFD6AA9B40000000, float 0xBFD64C7F40000000, float 0xBFD5EE2860000000, float 0xBFD58F9B20000000, float 0xBFD530D740000000, float 0xBFD4D1E100000000, float 0xBFD472B880000000, float 0xBFD4135DA0000000, float 0xBFD3B3D080000000, float 0xBFD3541100000000, float 0xBFD2F42360000000, float 0xBFD2940780000000, float 0xBFD233B960000000, float 0xBFD1D34580000000, float 0xBFD1729F60000000, float 0xBFD111D360000000, float 0xBFD0B0D960000000, float 0xBFD04FB980000000, float 0xBFCFDCDF60000000, float 0xBFCF19F800000000, float 0xBFCE56CD60000000, float 0xBFCD934F00000000, float 0xBFCCCF8D80000000, float 0xBFCC0B8060000000, float 0xBFCB473040000000, float 0xBFCA829D00000000, float 0xBFC9BDCF00000000, float 0xBFC8F8B580000000, float 0xBFC83369C0000000, float 0xBFC76DDAC0000000, float 0xBFC6A81120000000, float 0xBFC5E21540000000, float 0xBFC51BDE80000000, float 0xBFC4557580000000, float 0xBFC38EDA20000000, float 0xBFC2C80C80000000, float 0xBFC20114E0000000, float 0xBFC139F340000000, float 0xBFC0729F60000000, float 0xBFBF5653C0000000, float 0xBFBDC71500000000, float 0xBFBC378240000000, float 0xBFBAA7BD40000000, float 0xBFB917A460000000, float 0xBFB7875920000000, float 0xBFB5F6CAC0000000, float 0xBFB4660A20000000, float 0xBFB2D51720000000, float 0xBFB1440280000000, float 0xBFAF657760000000, float 0xBFAC4284E0000000, float 0xBFA91F70E0000000, float 0xBFA5FBF840000000, float 0xBFA2D85E00000000, float 0xBF9F694460000000, float 0xBF992146A0000000, float 0xBF92D948E0000000, float 0xBF892189C0000000, float 0xBF79221000000000], align 16 ; [#uses=1 type=[512 x float]*]
@p_str2 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1 ; [#uses=1 type=[1 x i8]*]
@p_str1 = private unnamed_addr constant [7 x i8] c"DFTpts\00", align 1 ; [#uses=3 type=[7 x i8]*]
@p_str = private unnamed_addr constant [10 x i8] c"butterfly\00", align 1 ; [#uses=3 type=[10 x i8]*]

; [#uses=1]
declare i32 @llvm.part.select.i32(i32, i32, i32) nounwind readnone

; [#uses=12]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=0]
define void @fft_stage_first([1024 x float]* %real_i, [1024 x float]* %imag_i, [1024 x float]* %real_o, [1024 x float]* %imag_o) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap([1024 x float]* %real_i) nounwind, !map !7
  call void (...)* @_ssdm_op_SpecBitsMap([1024 x float]* %imag_i) nounwind, !map !13
  call void (...)* @_ssdm_op_SpecBitsMap([1024 x float]* %real_o) nounwind, !map !17
  call void (...)* @_ssdm_op_SpecBitsMap([1024 x float]* %imag_o) nounwind, !map !21
  call void (...)* @_ssdm_op_SpecTopModule([16 x i8]* @fft_stage_first_str) nounwind
  call void @llvm.dbg.value(metadata !{[1024 x float]* %real_i}, i64 0, metadata !25), !dbg !38 ; [debug line = 14:28] [debug variable = real_i]
  call void @llvm.dbg.value(metadata !{[1024 x float]* %imag_i}, i64 0, metadata !39), !dbg !40 ; [debug line = 14:66] [debug variable = imag_i]
  call void @llvm.dbg.value(metadata !{[1024 x float]* %real_o}, i64 0, metadata !41), !dbg !42 ; [debug line = 14:104] [debug variable = real_o]
  call void @llvm.dbg.value(metadata !{[1024 x float]* %imag_o}, i64 0, metadata !43), !dbg !44 ; [debug line = 14:142] [debug variable = imag_o]
  br label %1, !dbg !45                           ; [debug line = 19:26]

; <label>:1                                       ; preds = %5, %0
  %k = phi i10 [ 0, %0 ], [ %k_1, %5 ]            ; [#uses=2 type=i10]
  %i = phi i1 [ false, %0 ], [ true, %5 ]         ; [#uses=2 type=i1]
  %i_cast1 = zext i1 %i to i32, !dbg !45          ; [#uses=1 type=i32] [debug line = 19:26]
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 1, i64 1, i64 1)
  br i1 %i, label %6, label %2, !dbg !45          ; [debug line = 19:26]

; <label>:2                                       ; preds = %1
  call void (...)* @_ssdm_op_SpecLoopName([10 x i8]* @p_str) nounwind, !dbg !48 ; [debug line = 19:41]
  %tmp_1 = call i32 (...)* @_ssdm_op_SpecRegionBegin([10 x i8]* @p_str) nounwind, !dbg !48 ; [#uses=1 type=i32] [debug line = 19:41]
  %tmp = zext i10 %k to i64, !dbg !50             ; [#uses=2 type=i64] [debug line = 21:22]
  %W_real_addr = getelementptr inbounds [512 x float]* @W_real, i64 0, i64 %tmp, !dbg !50 ; [#uses=1 type=float*] [debug line = 21:22]
  %c = load float* %W_real_addr, align 16, !dbg !50 ; [#uses=2 type=float] [debug line = 21:22]
  call void @llvm.dbg.value(metadata !{float %c}, i64 0, metadata !51), !dbg !50 ; [debug line = 21:22] [debug variable = c]
  %W_imag_addr = getelementptr inbounds [512 x float]* @W_imag, i64 0, i64 %tmp, !dbg !52 ; [#uses=1 type=float*] [debug line = 22:22]
  %s = load float* %W_imag_addr, align 16, !dbg !52 ; [#uses=2 type=float] [debug line = 22:22]
  call void @llvm.dbg.value(metadata !{float %s}, i64 0, metadata !53), !dbg !52 ; [debug line = 22:22] [debug variable = s]
  call void @llvm.dbg.value(metadata !{i1 %i}, i64 0, metadata !54), !dbg !57 ; [debug line = 24:23] [debug variable = i]
  br label %3, !dbg !57                           ; [debug line = 24:23]

; <label>:3                                       ; preds = %4, %2
  %i1 = phi i32 [ %i_cast1, %2 ], [ %i_1, %4 ]    ; [#uses=4 type=i32]
  %tmp_11 = call i22 @_ssdm_op_PartSelect.i22.i32.i32.i32(i32 %i1, i32 10, i32 31), !dbg !57 ; [#uses=1 type=i22] [debug line = 24:23]
  %icmp = icmp slt i22 %tmp_11, 1, !dbg !57       ; [#uses=1 type=i1] [debug line = 24:23]
  br i1 %icmp, label %4, label %5, !dbg !57       ; [debug line = 24:23]

; <label>:4                                       ; preds = %3
  call void (...)* @_ssdm_op_SpecLoopName([7 x i8]* @p_str1) nounwind, !dbg !58 ; [debug line = 24:62]
  %tmp_12 = call i32 (...)* @_ssdm_op_SpecRegionBegin([7 x i8]* @p_str1) nounwind, !dbg !58 ; [#uses=1 type=i32] [debug line = 24:62]
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 0, i32 0, [1 x i8]* @p_str2) nounwind, !dbg !60 ; [debug line = 25:1]
  %i_lower = add nsw i32 %i1, 1, !dbg !61         ; [#uses=1 type=i32] [debug line = 27:21]
  call void @llvm.dbg.value(metadata !{i32 %i_lower}, i64 0, metadata !62), !dbg !61 ; [debug line = 27:21] [debug variable = i_lower]
  %tmp_2 = sext i32 %i_lower to i64, !dbg !63     ; [#uses=4 type=i64] [debug line = 28:56]
  %real_i_addr = getelementptr [1024 x float]* %real_i, i64 0, i64 %tmp_2, !dbg !63 ; [#uses=1 type=float*] [debug line = 28:56]
  %real_i_load = load float* %real_i_addr, align 4, !dbg !63 ; [#uses=2 type=float] [debug line = 28:56]
  %tmp_3 = fmul float %real_i_load, %c, !dbg !63  ; [#uses=1 type=float] [debug line = 28:56]
  %imag_i_addr = getelementptr [1024 x float]* %imag_i, i64 0, i64 %tmp_2, !dbg !63 ; [#uses=1 type=float*] [debug line = 28:56]
  %imag_i_load = load float* %imag_i_addr, align 4, !dbg !63 ; [#uses=2 type=float] [debug line = 28:56]
  %tmp_4 = fmul float %imag_i_load, %s, !dbg !63  ; [#uses=1 type=float] [debug line = 28:56]
  %temp_R = fsub float %tmp_3, %tmp_4, !dbg !63   ; [#uses=2 type=float] [debug line = 28:56]
  call void @llvm.dbg.value(metadata !{float %temp_R}, i64 0, metadata !64), !dbg !63 ; [debug line = 28:56] [debug variable = temp_R]
  %tmp_5 = fmul float %imag_i_load, %c, !dbg !65  ; [#uses=1 type=float] [debug line = 29:56]
  %tmp_6 = fmul float %real_i_load, %s, !dbg !65  ; [#uses=1 type=float] [debug line = 29:56]
  %temp_I = fadd float %tmp_5, %tmp_6, !dbg !65   ; [#uses=2 type=float] [debug line = 29:56]
  call void @llvm.dbg.value(metadata !{float %temp_I}, i64 0, metadata !66), !dbg !65 ; [debug line = 29:56] [debug variable = temp_I]
  %tmp_7 = sext i32 %i1 to i64, !dbg !67          ; [#uses=4 type=i64] [debug line = 31:4]
  %real_i_addr_1 = getelementptr [1024 x float]* %real_i, i64 0, i64 %tmp_7, !dbg !67 ; [#uses=1 type=float*] [debug line = 31:4]
  %real_i_load_1 = load float* %real_i_addr_1, align 4, !dbg !67 ; [#uses=2 type=float] [debug line = 31:4]
  %tmp_8 = fsub float %real_i_load_1, %temp_R, !dbg !67 ; [#uses=1 type=float] [debug line = 31:4]
  %real_o_addr = getelementptr [1024 x float]* %real_o, i64 0, i64 %tmp_2, !dbg !67 ; [#uses=1 type=float*] [debug line = 31:4]
  store float %tmp_8, float* %real_o_addr, align 4, !dbg !67 ; [debug line = 31:4]
  %imag_i_addr_1 = getelementptr [1024 x float]* %imag_i, i64 0, i64 %tmp_7, !dbg !68 ; [#uses=1 type=float*] [debug line = 32:4]
  %imag_i_load_1 = load float* %imag_i_addr_1, align 4, !dbg !68 ; [#uses=2 type=float] [debug line = 32:4]
  %tmp_9 = fsub float %imag_i_load_1, %temp_I, !dbg !68 ; [#uses=1 type=float] [debug line = 32:4]
  %imag_o_addr = getelementptr [1024 x float]* %imag_o, i64 0, i64 %tmp_2, !dbg !68 ; [#uses=1 type=float*] [debug line = 32:4]
  store float %tmp_9, float* %imag_o_addr, align 4, !dbg !68 ; [debug line = 32:4]
  %tmp_s = fadd float %real_i_load_1, %temp_R, !dbg !69 ; [#uses=1 type=float] [debug line = 33:4]
  %real_o_addr_1 = getelementptr [1024 x float]* %real_o, i64 0, i64 %tmp_7, !dbg !69 ; [#uses=1 type=float*] [debug line = 33:4]
  store float %tmp_s, float* %real_o_addr_1, align 4, !dbg !69 ; [debug line = 33:4]
  %tmp_10 = fadd float %imag_i_load_1, %temp_I, !dbg !70 ; [#uses=1 type=float] [debug line = 34:4]
  %imag_o_addr_1 = getelementptr [1024 x float]* %imag_o, i64 0, i64 %tmp_7, !dbg !70 ; [#uses=1 type=float*] [debug line = 34:4]
  store float %tmp_10, float* %imag_o_addr_1, align 4, !dbg !70 ; [debug line = 34:4]
  %empty = call i32 (...)* @_ssdm_op_SpecRegionEnd([7 x i8]* @p_str1, i32 %tmp_12) nounwind, !dbg !71 ; [#uses=0 type=i32] [debug line = 35:4]
  %i_1 = add nsw i32 %i1, 2, !dbg !72             ; [#uses=1 type=i32] [debug line = 24:53]
  call void @llvm.dbg.value(metadata !{i32 %i_1}, i64 0, metadata !54), !dbg !72 ; [debug line = 24:53] [debug variable = i]
  br label %3, !dbg !72                           ; [debug line = 24:53]

; <label>:5                                       ; preds = %3
  %k_1 = xor i10 %k, -512, !dbg !73               ; [#uses=1 type=i10] [debug line = 36:3]
  call void @llvm.dbg.value(metadata !{i10 %k_1}, i64 0, metadata !74), !dbg !73 ; [debug line = 36:3] [debug variable = k]
  %empty_4 = call i32 (...)* @_ssdm_op_SpecRegionEnd([10 x i8]* @p_str, i32 %tmp_1) nounwind, !dbg !75 ; [#uses=0 type=i32] [debug line = 37:2]
  br label %1, !dbg !76                           ; [debug line = 19:35]

; <label>:6                                       ; preds = %1
  ret void, !dbg !77                              ; [debug line = 39:1]
}

; [#uses=1]
define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

; [#uses=2]
define weak i32 @_ssdm_op_SpecRegionEnd(...) {
entry:
  ret i32 0
}

; [#uses=2]
define weak i32 @_ssdm_op_SpecRegionBegin(...) {
entry:
  ret i32 0
}

; [#uses=2]
define weak void @_ssdm_op_SpecLoopTripCount(...) nounwind {
entry:
  ret void
}

; [#uses=2]
define weak void @_ssdm_op_SpecLoopName(...) nounwind {
entry:
  ret void
}

; [#uses=4]
define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

; [#uses=1]
define weak i22 @_ssdm_op_PartSelect.i22.i32.i32.i32(i32, i32, i32) nounwind readnone {
entry:
  %empty = call i32 @llvm.part.select.i32(i32 %0, i32 %1, i32 %2) ; [#uses=1 type=i32]
  %empty_5 = trunc i32 %empty to i22              ; [#uses=1 type=i22]
  ret i22 %empty_5
}

; [#uses=0]
declare i16 @_ssdm_op_HSub(...)

; [#uses=0]
declare i16 @_ssdm_op_HMul(...)

; [#uses=0]
declare i16 @_ssdm_op_HDiv(...)

; [#uses=0]
declare i16 @_ssdm_op_HAdd(...)

; [#uses=1]
declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [1 x i32]* @llvm_global_ctors_0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 0, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 31, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"real_i", metadata !11, metadata !"float", i32 0, i32 31}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 1023, i32 1}
!13 = metadata !{metadata !14}
!14 = metadata !{i32 0, i32 31, metadata !15}
!15 = metadata !{metadata !16}
!16 = metadata !{metadata !"imag_i", metadata !11, metadata !"float", i32 0, i32 31}
!17 = metadata !{metadata !18}
!18 = metadata !{i32 0, i32 31, metadata !19}
!19 = metadata !{metadata !20}
!20 = metadata !{metadata !"real_o", metadata !11, metadata !"float", i32 0, i32 31}
!21 = metadata !{metadata !22}
!22 = metadata !{i32 0, i32 31, metadata !23}
!23 = metadata !{metadata !24}
!24 = metadata !{metadata !"imag_o", metadata !11, metadata !"float", i32 0, i32 31}
!25 = metadata !{i32 786689, metadata !26, metadata !"real_i", null, i32 14, metadata !35, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!26 = metadata !{i32 786478, i32 0, metadata !27, metadata !"fft_stage_first", metadata !"fft_stage_first", metadata !"_Z15fft_stage_firstPfS_S_S_", metadata !27, i32 14, metadata !28, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !33, i32 14} ; [ DW_TAG_subprogram ]
!27 = metadata !{i32 786473, metadata !"fft_stage_first.cpp", metadata !"d:/Projects/vivado/project_4/HLS/1_Subcomponents/fft_stage_first", null} ; [ DW_TAG_file_type ]
!28 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !29, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!29 = metadata !{null, metadata !30, metadata !30, metadata !30, metadata !30}
!30 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !31} ; [ DW_TAG_pointer_type ]
!31 = metadata !{i32 786454, null, metadata !"DTYPE", metadata !27, i32 8, i64 0, i64 0, i64 0, i32 0, metadata !32} ; [ DW_TAG_typedef ]
!32 = metadata !{i32 786468, null, metadata !"float", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 4} ; [ DW_TAG_base_type ]
!33 = metadata !{metadata !34}
!34 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!35 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 0, i64 0, i32 0, i32 0, metadata !31, metadata !36, i32 0, i32 0} ; [ DW_TAG_array_type ]
!36 = metadata !{metadata !37}
!37 = metadata !{i32 786465, i64 0, i64 1023}     ; [ DW_TAG_subrange_type ]
!38 = metadata !{i32 14, i32 28, metadata !26, null}
!39 = metadata !{i32 786689, metadata !26, metadata !"imag_i", null, i32 14, metadata !35, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!40 = metadata !{i32 14, i32 66, metadata !26, null}
!41 = metadata !{i32 786689, metadata !26, metadata !"real_o", null, i32 14, metadata !35, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!42 = metadata !{i32 14, i32 104, metadata !26, null}
!43 = metadata !{i32 786689, metadata !26, metadata !"imag_o", null, i32 14, metadata !35, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!44 = metadata !{i32 14, i32 142, metadata !26, null}
!45 = metadata !{i32 19, i32 26, metadata !46, null}
!46 = metadata !{i32 786443, metadata !47, i32 19, i32 13, metadata !27, i32 1} ; [ DW_TAG_lexical_block ]
!47 = metadata !{i32 786443, metadata !26, i32 14, i32 174, metadata !27, i32 0} ; [ DW_TAG_lexical_block ]
!48 = metadata !{i32 19, i32 41, metadata !49, null}
!49 = metadata !{i32 786443, metadata !46, i32 19, i32 40, metadata !27, i32 2} ; [ DW_TAG_lexical_block ]
!50 = metadata !{i32 21, i32 22, metadata !49, null}
!51 = metadata !{i32 786688, metadata !49, metadata !"c", metadata !27, i32 21, metadata !31, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!52 = metadata !{i32 22, i32 22, metadata !49, null}
!53 = metadata !{i32 786688, metadata !49, metadata !"s", metadata !27, i32 22, metadata !31, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!54 = metadata !{i32 786688, metadata !55, metadata !"i", metadata !27, i32 24, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!55 = metadata !{i32 786443, metadata !49, i32 24, i32 10, metadata !27, i32 3} ; [ DW_TAG_lexical_block ]
!56 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!57 = metadata !{i32 24, i32 23, metadata !55, null}
!58 = metadata !{i32 24, i32 62, metadata !59, null}
!59 = metadata !{i32 786443, metadata !55, i32 24, i32 61, metadata !27, i32 4} ; [ DW_TAG_lexical_block ]
!60 = metadata !{i32 25, i32 1, metadata !59, null}
!61 = metadata !{i32 27, i32 21, metadata !59, null}
!62 = metadata !{i32 786688, metadata !59, metadata !"i_lower", metadata !27, i32 27, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!63 = metadata !{i32 28, i32 56, metadata !59, null}
!64 = metadata !{i32 786688, metadata !59, metadata !"temp_R", metadata !27, i32 28, metadata !31, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!65 = metadata !{i32 29, i32 56, metadata !59, null}
!66 = metadata !{i32 786688, metadata !59, metadata !"temp_I", metadata !27, i32 29, metadata !31, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!67 = metadata !{i32 31, i32 4, metadata !59, null}
!68 = metadata !{i32 32, i32 4, metadata !59, null}
!69 = metadata !{i32 33, i32 4, metadata !59, null}
!70 = metadata !{i32 34, i32 4, metadata !59, null}
!71 = metadata !{i32 35, i32 4, metadata !59, null}
!72 = metadata !{i32 24, i32 53, metadata !55, null}
!73 = metadata !{i32 36, i32 3, metadata !49, null}
!74 = metadata !{i32 786688, metadata !47, metadata !"k", metadata !27, i32 17, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!75 = metadata !{i32 37, i32 2, metadata !49, null}
!76 = metadata !{i32 19, i32 35, metadata !46, null}
!77 = metadata !{i32 39, i32 1, metadata !47, null}
