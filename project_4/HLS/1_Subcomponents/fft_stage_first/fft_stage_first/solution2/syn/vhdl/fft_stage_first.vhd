-- ==============================================================
-- RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
-- Version: 2015.4
-- Copyright (C) 2015 Xilinx Inc. All rights reserved.
-- 
-- ===========================================================

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity fft_stage_first is
port (
    real_i_0_address0 : OUT STD_LOGIC_VECTOR (8 downto 0);
    real_i_0_ce0 : OUT STD_LOGIC;
    real_i_0_d0 : OUT STD_LOGIC_VECTOR (31 downto 0);
    real_i_0_q0 : IN STD_LOGIC_VECTOR (31 downto 0);
    real_i_0_we0 : OUT STD_LOGIC;
    real_i_0_address1 : OUT STD_LOGIC_VECTOR (8 downto 0);
    real_i_0_ce1 : OUT STD_LOGIC;
    real_i_0_d1 : OUT STD_LOGIC_VECTOR (31 downto 0);
    real_i_0_q1 : IN STD_LOGIC_VECTOR (31 downto 0);
    real_i_0_we1 : OUT STD_LOGIC;
    real_i_1_address0 : OUT STD_LOGIC_VECTOR (8 downto 0);
    real_i_1_ce0 : OUT STD_LOGIC;
    real_i_1_d0 : OUT STD_LOGIC_VECTOR (31 downto 0);
    real_i_1_q0 : IN STD_LOGIC_VECTOR (31 downto 0);
    real_i_1_we0 : OUT STD_LOGIC;
    real_i_1_address1 : OUT STD_LOGIC_VECTOR (8 downto 0);
    real_i_1_ce1 : OUT STD_LOGIC;
    real_i_1_d1 : OUT STD_LOGIC_VECTOR (31 downto 0);
    real_i_1_q1 : IN STD_LOGIC_VECTOR (31 downto 0);
    real_i_1_we1 : OUT STD_LOGIC;
    imag_i_0_address0 : OUT STD_LOGIC_VECTOR (8 downto 0);
    imag_i_0_ce0 : OUT STD_LOGIC;
    imag_i_0_d0 : OUT STD_LOGIC_VECTOR (31 downto 0);
    imag_i_0_q0 : IN STD_LOGIC_VECTOR (31 downto 0);
    imag_i_0_we0 : OUT STD_LOGIC;
    imag_i_0_address1 : OUT STD_LOGIC_VECTOR (8 downto 0);
    imag_i_0_ce1 : OUT STD_LOGIC;
    imag_i_0_d1 : OUT STD_LOGIC_VECTOR (31 downto 0);
    imag_i_0_q1 : IN STD_LOGIC_VECTOR (31 downto 0);
    imag_i_0_we1 : OUT STD_LOGIC;
    imag_i_1_address0 : OUT STD_LOGIC_VECTOR (8 downto 0);
    imag_i_1_ce0 : OUT STD_LOGIC;
    imag_i_1_d0 : OUT STD_LOGIC_VECTOR (31 downto 0);
    imag_i_1_q0 : IN STD_LOGIC_VECTOR (31 downto 0);
    imag_i_1_we0 : OUT STD_LOGIC;
    imag_i_1_address1 : OUT STD_LOGIC_VECTOR (8 downto 0);
    imag_i_1_ce1 : OUT STD_LOGIC;
    imag_i_1_d1 : OUT STD_LOGIC_VECTOR (31 downto 0);
    imag_i_1_q1 : IN STD_LOGIC_VECTOR (31 downto 0);
    imag_i_1_we1 : OUT STD_LOGIC;
    real_o_0_address0 : OUT STD_LOGIC_VECTOR (8 downto 0);
    real_o_0_ce0 : OUT STD_LOGIC;
    real_o_0_d0 : OUT STD_LOGIC_VECTOR (31 downto 0);
    real_o_0_q0 : IN STD_LOGIC_VECTOR (31 downto 0);
    real_o_0_we0 : OUT STD_LOGIC;
    real_o_0_address1 : OUT STD_LOGIC_VECTOR (8 downto 0);
    real_o_0_ce1 : OUT STD_LOGIC;
    real_o_0_d1 : OUT STD_LOGIC_VECTOR (31 downto 0);
    real_o_0_q1 : IN STD_LOGIC_VECTOR (31 downto 0);
    real_o_0_we1 : OUT STD_LOGIC;
    real_o_1_address0 : OUT STD_LOGIC_VECTOR (8 downto 0);
    real_o_1_ce0 : OUT STD_LOGIC;
    real_o_1_d0 : OUT STD_LOGIC_VECTOR (31 downto 0);
    real_o_1_q0 : IN STD_LOGIC_VECTOR (31 downto 0);
    real_o_1_we0 : OUT STD_LOGIC;
    real_o_1_address1 : OUT STD_LOGIC_VECTOR (8 downto 0);
    real_o_1_ce1 : OUT STD_LOGIC;
    real_o_1_d1 : OUT STD_LOGIC_VECTOR (31 downto 0);
    real_o_1_q1 : IN STD_LOGIC_VECTOR (31 downto 0);
    real_o_1_we1 : OUT STD_LOGIC;
    imag_o_0_address0 : OUT STD_LOGIC_VECTOR (8 downto 0);
    imag_o_0_ce0 : OUT STD_LOGIC;
    imag_o_0_d0 : OUT STD_LOGIC_VECTOR (31 downto 0);
    imag_o_0_q0 : IN STD_LOGIC_VECTOR (31 downto 0);
    imag_o_0_we0 : OUT STD_LOGIC;
    imag_o_0_address1 : OUT STD_LOGIC_VECTOR (8 downto 0);
    imag_o_0_ce1 : OUT STD_LOGIC;
    imag_o_0_d1 : OUT STD_LOGIC_VECTOR (31 downto 0);
    imag_o_0_q1 : IN STD_LOGIC_VECTOR (31 downto 0);
    imag_o_0_we1 : OUT STD_LOGIC;
    imag_o_1_address0 : OUT STD_LOGIC_VECTOR (8 downto 0);
    imag_o_1_ce0 : OUT STD_LOGIC;
    imag_o_1_d0 : OUT STD_LOGIC_VECTOR (31 downto 0);
    imag_o_1_q0 : IN STD_LOGIC_VECTOR (31 downto 0);
    imag_o_1_we0 : OUT STD_LOGIC;
    imag_o_1_address1 : OUT STD_LOGIC_VECTOR (8 downto 0);
    imag_o_1_ce1 : OUT STD_LOGIC;
    imag_o_1_d1 : OUT STD_LOGIC_VECTOR (31 downto 0);
    imag_o_1_q1 : IN STD_LOGIC_VECTOR (31 downto 0);
    imag_o_1_we1 : OUT STD_LOGIC;
    ap_clk : IN STD_LOGIC;
    ap_rst : IN STD_LOGIC;
    ap_done : OUT STD_LOGIC;
    ap_start : IN STD_LOGIC;
    ap_idle : OUT STD_LOGIC;
    ap_ready : OUT STD_LOGIC );
end;


architecture behav of fft_stage_first is 
    attribute CORE_GENERATION_INFO : STRING;
    attribute CORE_GENERATION_INFO of behav : architecture is
    "fft_stage_first,hls_ip_2015_4,{HLS_INPUT_TYPE=cxx,HLS_INPUT_FLOAT=1,HLS_INPUT_FIXED=0,HLS_INPUT_PART=xc7z020clg484-1,HLS_INPUT_CLOCK=10.000000,HLS_INPUT_ARCH=dataflow,HLS_SYN_CLOCK=8.412000,HLS_SYN_LAT=531,HLS_SYN_TPT=532,HLS_SYN_MEM=2,HLS_SYN_DSP=24,HLS_SYN_FF=2307,HLS_SYN_LUT=3683}";
    constant ap_const_lv9_0 : STD_LOGIC_VECTOR (8 downto 0) := "000000000";
    constant ap_const_logic_0 : STD_LOGIC := '0';
    constant ap_const_lv32_0 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000000000";
    constant ap_const_logic_1 : STD_LOGIC := '1';
    constant ap_true : BOOLEAN := true;

    signal fft_stage_first_Loop_butterfly_proc_U0_ap_start : STD_LOGIC;
    signal fft_stage_first_Loop_butterfly_proc_U0_ap_done : STD_LOGIC;
    signal fft_stage_first_Loop_butterfly_proc_U0_ap_continue : STD_LOGIC;
    signal fft_stage_first_Loop_butterfly_proc_U0_ap_idle : STD_LOGIC;
    signal fft_stage_first_Loop_butterfly_proc_U0_ap_ready : STD_LOGIC;
    signal fft_stage_first_Loop_butterfly_proc_U0_imag_o_0_address0 : STD_LOGIC_VECTOR (8 downto 0);
    signal fft_stage_first_Loop_butterfly_proc_U0_imag_o_0_ce0 : STD_LOGIC;
    signal fft_stage_first_Loop_butterfly_proc_U0_imag_o_0_we0 : STD_LOGIC;
    signal fft_stage_first_Loop_butterfly_proc_U0_imag_o_0_d0 : STD_LOGIC_VECTOR (31 downto 0);
    signal fft_stage_first_Loop_butterfly_proc_U0_imag_o_1_address0 : STD_LOGIC_VECTOR (8 downto 0);
    signal fft_stage_first_Loop_butterfly_proc_U0_imag_o_1_ce0 : STD_LOGIC;
    signal fft_stage_first_Loop_butterfly_proc_U0_imag_o_1_we0 : STD_LOGIC;
    signal fft_stage_first_Loop_butterfly_proc_U0_imag_o_1_d0 : STD_LOGIC_VECTOR (31 downto 0);
    signal fft_stage_first_Loop_butterfly_proc_U0_real_o_0_address0 : STD_LOGIC_VECTOR (8 downto 0);
    signal fft_stage_first_Loop_butterfly_proc_U0_real_o_0_ce0 : STD_LOGIC;
    signal fft_stage_first_Loop_butterfly_proc_U0_real_o_0_we0 : STD_LOGIC;
    signal fft_stage_first_Loop_butterfly_proc_U0_real_o_0_d0 : STD_LOGIC_VECTOR (31 downto 0);
    signal fft_stage_first_Loop_butterfly_proc_U0_real_o_1_address0 : STD_LOGIC_VECTOR (8 downto 0);
    signal fft_stage_first_Loop_butterfly_proc_U0_real_o_1_ce0 : STD_LOGIC;
    signal fft_stage_first_Loop_butterfly_proc_U0_real_o_1_we0 : STD_LOGIC;
    signal fft_stage_first_Loop_butterfly_proc_U0_real_o_1_d0 : STD_LOGIC_VECTOR (31 downto 0);
    signal fft_stage_first_Loop_butterfly_proc_U0_imag_i_0_address0 : STD_LOGIC_VECTOR (8 downto 0);
    signal fft_stage_first_Loop_butterfly_proc_U0_imag_i_0_ce0 : STD_LOGIC;
    signal fft_stage_first_Loop_butterfly_proc_U0_imag_i_0_q0 : STD_LOGIC_VECTOR (31 downto 0);
    signal fft_stage_first_Loop_butterfly_proc_U0_imag_i_1_address0 : STD_LOGIC_VECTOR (8 downto 0);
    signal fft_stage_first_Loop_butterfly_proc_U0_imag_i_1_ce0 : STD_LOGIC;
    signal fft_stage_first_Loop_butterfly_proc_U0_imag_i_1_q0 : STD_LOGIC_VECTOR (31 downto 0);
    signal fft_stage_first_Loop_butterfly_proc_U0_real_i_0_address0 : STD_LOGIC_VECTOR (8 downto 0);
    signal fft_stage_first_Loop_butterfly_proc_U0_real_i_0_ce0 : STD_LOGIC;
    signal fft_stage_first_Loop_butterfly_proc_U0_real_i_0_q0 : STD_LOGIC_VECTOR (31 downto 0);
    signal fft_stage_first_Loop_butterfly_proc_U0_real_i_1_address0 : STD_LOGIC_VECTOR (8 downto 0);
    signal fft_stage_first_Loop_butterfly_proc_U0_real_i_1_ce0 : STD_LOGIC;
    signal fft_stage_first_Loop_butterfly_proc_U0_real_i_1_q0 : STD_LOGIC_VECTOR (31 downto 0);
    signal ap_sig_hs_continue : STD_LOGIC;
    signal ap_reg_procdone_fft_stage_first_Loop_butterfly_proc_U0 : STD_LOGIC := '0';
    signal ap_sig_hs_done : STD_LOGIC;
    signal ap_CS : STD_LOGIC;
    signal ap_sig_top_allready : STD_LOGIC;

    component fft_stage_first_Loop_butterfly_proc IS
    port (
        ap_clk : IN STD_LOGIC;
        ap_rst : IN STD_LOGIC;
        ap_start : IN STD_LOGIC;
        ap_done : OUT STD_LOGIC;
        ap_continue : IN STD_LOGIC;
        ap_idle : OUT STD_LOGIC;
        ap_ready : OUT STD_LOGIC;
        imag_o_0_address0 : OUT STD_LOGIC_VECTOR (8 downto 0);
        imag_o_0_ce0 : OUT STD_LOGIC;
        imag_o_0_we0 : OUT STD_LOGIC;
        imag_o_0_d0 : OUT STD_LOGIC_VECTOR (31 downto 0);
        imag_o_1_address0 : OUT STD_LOGIC_VECTOR (8 downto 0);
        imag_o_1_ce0 : OUT STD_LOGIC;
        imag_o_1_we0 : OUT STD_LOGIC;
        imag_o_1_d0 : OUT STD_LOGIC_VECTOR (31 downto 0);
        real_o_0_address0 : OUT STD_LOGIC_VECTOR (8 downto 0);
        real_o_0_ce0 : OUT STD_LOGIC;
        real_o_0_we0 : OUT STD_LOGIC;
        real_o_0_d0 : OUT STD_LOGIC_VECTOR (31 downto 0);
        real_o_1_address0 : OUT STD_LOGIC_VECTOR (8 downto 0);
        real_o_1_ce0 : OUT STD_LOGIC;
        real_o_1_we0 : OUT STD_LOGIC;
        real_o_1_d0 : OUT STD_LOGIC_VECTOR (31 downto 0);
        imag_i_0_address0 : OUT STD_LOGIC_VECTOR (8 downto 0);
        imag_i_0_ce0 : OUT STD_LOGIC;
        imag_i_0_q0 : IN STD_LOGIC_VECTOR (31 downto 0);
        imag_i_1_address0 : OUT STD_LOGIC_VECTOR (8 downto 0);
        imag_i_1_ce0 : OUT STD_LOGIC;
        imag_i_1_q0 : IN STD_LOGIC_VECTOR (31 downto 0);
        real_i_0_address0 : OUT STD_LOGIC_VECTOR (8 downto 0);
        real_i_0_ce0 : OUT STD_LOGIC;
        real_i_0_q0 : IN STD_LOGIC_VECTOR (31 downto 0);
        real_i_1_address0 : OUT STD_LOGIC_VECTOR (8 downto 0);
        real_i_1_ce0 : OUT STD_LOGIC;
        real_i_1_q0 : IN STD_LOGIC_VECTOR (31 downto 0) );
    end component;



begin
    fft_stage_first_Loop_butterfly_proc_U0 : component fft_stage_first_Loop_butterfly_proc
    port map (
        ap_clk => ap_clk,
        ap_rst => ap_rst,
        ap_start => fft_stage_first_Loop_butterfly_proc_U0_ap_start,
        ap_done => fft_stage_first_Loop_butterfly_proc_U0_ap_done,
        ap_continue => fft_stage_first_Loop_butterfly_proc_U0_ap_continue,
        ap_idle => fft_stage_first_Loop_butterfly_proc_U0_ap_idle,
        ap_ready => fft_stage_first_Loop_butterfly_proc_U0_ap_ready,
        imag_o_0_address0 => fft_stage_first_Loop_butterfly_proc_U0_imag_o_0_address0,
        imag_o_0_ce0 => fft_stage_first_Loop_butterfly_proc_U0_imag_o_0_ce0,
        imag_o_0_we0 => fft_stage_first_Loop_butterfly_proc_U0_imag_o_0_we0,
        imag_o_0_d0 => fft_stage_first_Loop_butterfly_proc_U0_imag_o_0_d0,
        imag_o_1_address0 => fft_stage_first_Loop_butterfly_proc_U0_imag_o_1_address0,
        imag_o_1_ce0 => fft_stage_first_Loop_butterfly_proc_U0_imag_o_1_ce0,
        imag_o_1_we0 => fft_stage_first_Loop_butterfly_proc_U0_imag_o_1_we0,
        imag_o_1_d0 => fft_stage_first_Loop_butterfly_proc_U0_imag_o_1_d0,
        real_o_0_address0 => fft_stage_first_Loop_butterfly_proc_U0_real_o_0_address0,
        real_o_0_ce0 => fft_stage_first_Loop_butterfly_proc_U0_real_o_0_ce0,
        real_o_0_we0 => fft_stage_first_Loop_butterfly_proc_U0_real_o_0_we0,
        real_o_0_d0 => fft_stage_first_Loop_butterfly_proc_U0_real_o_0_d0,
        real_o_1_address0 => fft_stage_first_Loop_butterfly_proc_U0_real_o_1_address0,
        real_o_1_ce0 => fft_stage_first_Loop_butterfly_proc_U0_real_o_1_ce0,
        real_o_1_we0 => fft_stage_first_Loop_butterfly_proc_U0_real_o_1_we0,
        real_o_1_d0 => fft_stage_first_Loop_butterfly_proc_U0_real_o_1_d0,
        imag_i_0_address0 => fft_stage_first_Loop_butterfly_proc_U0_imag_i_0_address0,
        imag_i_0_ce0 => fft_stage_first_Loop_butterfly_proc_U0_imag_i_0_ce0,
        imag_i_0_q0 => fft_stage_first_Loop_butterfly_proc_U0_imag_i_0_q0,
        imag_i_1_address0 => fft_stage_first_Loop_butterfly_proc_U0_imag_i_1_address0,
        imag_i_1_ce0 => fft_stage_first_Loop_butterfly_proc_U0_imag_i_1_ce0,
        imag_i_1_q0 => fft_stage_first_Loop_butterfly_proc_U0_imag_i_1_q0,
        real_i_0_address0 => fft_stage_first_Loop_butterfly_proc_U0_real_i_0_address0,
        real_i_0_ce0 => fft_stage_first_Loop_butterfly_proc_U0_real_i_0_ce0,
        real_i_0_q0 => fft_stage_first_Loop_butterfly_proc_U0_real_i_0_q0,
        real_i_1_address0 => fft_stage_first_Loop_butterfly_proc_U0_real_i_1_address0,
        real_i_1_ce0 => fft_stage_first_Loop_butterfly_proc_U0_real_i_1_ce0,
        real_i_1_q0 => fft_stage_first_Loop_butterfly_proc_U0_real_i_1_q0);





    -- ap_reg_procdone_fft_stage_first_Loop_butterfly_proc_U0 assign process. --
    ap_reg_procdone_fft_stage_first_Loop_butterfly_proc_U0_assign_proc : process(ap_clk)
    begin
        if (ap_clk'event and ap_clk =  '1') then
            if (ap_rst = '1') then
                ap_reg_procdone_fft_stage_first_Loop_butterfly_proc_U0 <= ap_const_logic_0;
            else
                if ((ap_const_logic_1 = ap_sig_hs_done)) then 
                    ap_reg_procdone_fft_stage_first_Loop_butterfly_proc_U0 <= ap_const_logic_0;
                elsif ((fft_stage_first_Loop_butterfly_proc_U0_ap_done = ap_const_logic_1)) then 
                    ap_reg_procdone_fft_stage_first_Loop_butterfly_proc_U0 <= ap_const_logic_1;
                end if; 
            end if;
        end if;
    end process;


    -- ap_CS assign process. --
    ap_CS_assign_proc : process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            ap_CS <= ap_const_logic_0;
        end if;
    end process;
    ap_done <= ap_sig_hs_done;

    -- ap_idle assign process. --
    ap_idle_assign_proc : process(fft_stage_first_Loop_butterfly_proc_U0_ap_idle)
    begin
        if ((fft_stage_first_Loop_butterfly_proc_U0_ap_idle = ap_const_logic_1)) then 
            ap_idle <= ap_const_logic_1;
        else 
            ap_idle <= ap_const_logic_0;
        end if; 
    end process;

    ap_ready <= ap_sig_top_allready;
    ap_sig_hs_continue <= ap_const_logic_1;

    -- ap_sig_hs_done assign process. --
    ap_sig_hs_done_assign_proc : process(fft_stage_first_Loop_butterfly_proc_U0_ap_done)
    begin
        if ((fft_stage_first_Loop_butterfly_proc_U0_ap_done = ap_const_logic_1)) then 
            ap_sig_hs_done <= ap_const_logic_1;
        else 
            ap_sig_hs_done <= ap_const_logic_0;
        end if; 
    end process;

    ap_sig_top_allready <= fft_stage_first_Loop_butterfly_proc_U0_ap_ready;
    fft_stage_first_Loop_butterfly_proc_U0_ap_continue <= ap_sig_hs_continue;
    fft_stage_first_Loop_butterfly_proc_U0_ap_start <= ap_start;
    fft_stage_first_Loop_butterfly_proc_U0_imag_i_0_q0 <= imag_i_0_q0;
    fft_stage_first_Loop_butterfly_proc_U0_imag_i_1_q0 <= imag_i_1_q0;
    fft_stage_first_Loop_butterfly_proc_U0_real_i_0_q0 <= real_i_0_q0;
    fft_stage_first_Loop_butterfly_proc_U0_real_i_1_q0 <= real_i_1_q0;
    imag_i_0_address0 <= fft_stage_first_Loop_butterfly_proc_U0_imag_i_0_address0;
    imag_i_0_address1 <= ap_const_lv9_0;
    imag_i_0_ce0 <= fft_stage_first_Loop_butterfly_proc_U0_imag_i_0_ce0;
    imag_i_0_ce1 <= ap_const_logic_0;
    imag_i_0_d0 <= ap_const_lv32_0;
    imag_i_0_d1 <= ap_const_lv32_0;
    imag_i_0_we0 <= ap_const_logic_0;
    imag_i_0_we1 <= ap_const_logic_0;
    imag_i_1_address0 <= fft_stage_first_Loop_butterfly_proc_U0_imag_i_1_address0;
    imag_i_1_address1 <= ap_const_lv9_0;
    imag_i_1_ce0 <= fft_stage_first_Loop_butterfly_proc_U0_imag_i_1_ce0;
    imag_i_1_ce1 <= ap_const_logic_0;
    imag_i_1_d0 <= ap_const_lv32_0;
    imag_i_1_d1 <= ap_const_lv32_0;
    imag_i_1_we0 <= ap_const_logic_0;
    imag_i_1_we1 <= ap_const_logic_0;
    imag_o_0_address0 <= fft_stage_first_Loop_butterfly_proc_U0_imag_o_0_address0;
    imag_o_0_address1 <= ap_const_lv9_0;
    imag_o_0_ce0 <= fft_stage_first_Loop_butterfly_proc_U0_imag_o_0_ce0;
    imag_o_0_ce1 <= ap_const_logic_0;
    imag_o_0_d0 <= fft_stage_first_Loop_butterfly_proc_U0_imag_o_0_d0;
    imag_o_0_d1 <= ap_const_lv32_0;
    imag_o_0_we0 <= fft_stage_first_Loop_butterfly_proc_U0_imag_o_0_we0;
    imag_o_0_we1 <= ap_const_logic_0;
    imag_o_1_address0 <= fft_stage_first_Loop_butterfly_proc_U0_imag_o_1_address0;
    imag_o_1_address1 <= ap_const_lv9_0;
    imag_o_1_ce0 <= fft_stage_first_Loop_butterfly_proc_U0_imag_o_1_ce0;
    imag_o_1_ce1 <= ap_const_logic_0;
    imag_o_1_d0 <= fft_stage_first_Loop_butterfly_proc_U0_imag_o_1_d0;
    imag_o_1_d1 <= ap_const_lv32_0;
    imag_o_1_we0 <= fft_stage_first_Loop_butterfly_proc_U0_imag_o_1_we0;
    imag_o_1_we1 <= ap_const_logic_0;
    real_i_0_address0 <= fft_stage_first_Loop_butterfly_proc_U0_real_i_0_address0;
    real_i_0_address1 <= ap_const_lv9_0;
    real_i_0_ce0 <= fft_stage_first_Loop_butterfly_proc_U0_real_i_0_ce0;
    real_i_0_ce1 <= ap_const_logic_0;
    real_i_0_d0 <= ap_const_lv32_0;
    real_i_0_d1 <= ap_const_lv32_0;
    real_i_0_we0 <= ap_const_logic_0;
    real_i_0_we1 <= ap_const_logic_0;
    real_i_1_address0 <= fft_stage_first_Loop_butterfly_proc_U0_real_i_1_address0;
    real_i_1_address1 <= ap_const_lv9_0;
    real_i_1_ce0 <= fft_stage_first_Loop_butterfly_proc_U0_real_i_1_ce0;
    real_i_1_ce1 <= ap_const_logic_0;
    real_i_1_d0 <= ap_const_lv32_0;
    real_i_1_d1 <= ap_const_lv32_0;
    real_i_1_we0 <= ap_const_logic_0;
    real_i_1_we1 <= ap_const_logic_0;
    real_o_0_address0 <= fft_stage_first_Loop_butterfly_proc_U0_real_o_0_address0;
    real_o_0_address1 <= ap_const_lv9_0;
    real_o_0_ce0 <= fft_stage_first_Loop_butterfly_proc_U0_real_o_0_ce0;
    real_o_0_ce1 <= ap_const_logic_0;
    real_o_0_d0 <= fft_stage_first_Loop_butterfly_proc_U0_real_o_0_d0;
    real_o_0_d1 <= ap_const_lv32_0;
    real_o_0_we0 <= fft_stage_first_Loop_butterfly_proc_U0_real_o_0_we0;
    real_o_0_we1 <= ap_const_logic_0;
    real_o_1_address0 <= fft_stage_first_Loop_butterfly_proc_U0_real_o_1_address0;
    real_o_1_address1 <= ap_const_lv9_0;
    real_o_1_ce0 <= fft_stage_first_Loop_butterfly_proc_U0_real_o_1_ce0;
    real_o_1_ce1 <= ap_const_logic_0;
    real_o_1_d0 <= fft_stage_first_Loop_butterfly_proc_U0_real_o_1_d0;
    real_o_1_d1 <= ap_const_lv32_0;
    real_o_1_we0 <= fft_stage_first_Loop_butterfly_proc_U0_real_o_1_we0;
    real_o_1_we1 <= ap_const_logic_0;
end behav;
