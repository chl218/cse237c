; ModuleID = 'D:/Projects/vivado/project_4/HLS/1_Subcomponents/fft_stage_first/fft_stage_first/solution3/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@llvm.global_ctors.1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a] ; [#uses=0 type=[1 x void ()*]*]
@llvm.global_ctors.0 = appending global [1 x i32] [i32 65535] ; [#uses=0 type=[1 x i32]*]
@fft_stage_first.str = internal unnamed_addr constant [16 x i8] c"fft_stage_first\00" ; [#uses=1 type=[16 x i8]*]
@W_real = internal unnamed_addr constant [512 x float] [float 1.000000e+00, float 0x3FEFFFD820000000, float 0x3FEFFF62C0000000, float 0x3FEFFE9DA0000000, float 0x3FEFFD88C0000000, float 0x3FEFFC2440000000, float 0x3FEFFA7220000000, float 0x3FEFF87260000000, float 0x3FEFF620E0000000, float 0x3FEFF38400000000, float 0x3FEFF09520000000, float 0x3FEFED58C0000000, float 0x3FEFE9CCC0000000, float 0x3FEFE5F300000000, float 0x3FEFE1CBC0000000, float 0x3FEFDD52C0000000, float 0x3FEFD88E40000000, float 0x3FEFD37A00000000, float 0x3FEFCE1600000000, float 0x3FEFC86480000000, float 0x3FEFC26560000000, float 0x3FEFBC16A0000000, float 0x3FEFB57A40000000, float 0x3FEFAE8E20000000, float 0x3FEFA75680000000, float 0x3FEF9FCF40000000, float 0x3FEF97F840000000, float 0x3FEF8FD5C0000000, float 0x3FEF8765C0000000, float 0x3FEF7EA600000000, float 0x3FEF7598A0000000, float 0x3FEF6C3FC0000000, float 0x3FEF629740000000, float 0x3FEF58A320000000, float 0x3FEF4E5F80000000, float 0x3FEF43D040000000, float 0x3FEF38F360000000, float 0x3FEF2DC900000000, float 0x3FEF225320000000, float 0x3FEF168F80000000, float 0x3FEF0A7E80000000, float 0x3FEEFE21E0000000, float 0x3FEEF177A0000000, float 0x3FEEE48200000000, float 0x3FEED740C0000000, float 0x3FEEC9B200000000, float 0x3FEEBBD9C0000000, float 0x3FEEADB1E0000000, float 0x3FEE9F40A0000000, float 0x3FEE9083E0000000, float 0x3FEE817BA0000000, float 0x3FEE7227E0000000, float 0x3FEE628880000000, float 0x3FEE529FE0000000, float 0x3FEE4269A0000000, float 0x3FEE31EA00000000, float 0x3FEE2120E0000000, float 0x3FEE100C60000000, float 0x3FEDFEAE60000000, float 0x3FEDED0700000000, float 0x3FEDDB1420000000, float 0x3FEDC8D7E0000000, float 0x3FEDB65240000000, float 0x3FEDA38320000000, float 0x3FED906CC0000000, float 0x3FED7D0AE0000000, float 0x3FED6961C0000000, float 0x3FED556F40000000, float 0x3FED413560000000, float 0x3FED2CB200000000, float 0x3FED17E780000000, float 0x3FED02D5A0000000, float 0x3FECED7A60000000, float 0x3FECD7D9E0000000, float 0x3FECC1F000000000, float 0x3FECABC0E0000000, float 0x3FEC954A80000000, float 0x3FEC7E8EE0000000, float 0x3FEC678C00000000, float 0x3FEC5041C0000000, float 0x3FEC38B260000000, float 0x3FEC20DDC0000000, float 0x3FEC08C400000000, float 0x3FEBF064E0000000, float 0x3FEBD7C0C0000000, float 0x3FEBBED740000000, float 0x3FEBA5AAC0000000, float 0x3FEB8C3900000000, float 0x3FEB728420000000, float 0x3FEB588A00000000, float 0x3FEB3E4CE0000000, float 0x3FEB23CC80000000, float 0x3FEB090B40000000, float 0x3FEAEE04C0000000, float 0x3FEAD2BD40000000, float 0x3FEAB732A0000000, float 0x3FEA9B6700000000, float 0x3FEA7F5840000000, float 0x3FEA630880000000, float 0x3FEA4677C0000000, float 0x3FEA29A800000000, float 0x3FEA0C9540000000, float 0x3FE9EF4380000000, float 0x3FE9D1B2E0000000, float 0x3FE9B3E140000000, float 0x3FE995CEA0000000, float 0x3FE9777F20000000, float 0x3FE958F0C0000000, float 0x3FE93A2160000000, float 0x3FE91B1740000000, float 0x3FE8FBCC20000000, float 0x3FE8DC4440000000, float 0x3FE8BC7F80000000, float 0x3FE89C7DE0000000, float 0x3FE87C3F80000000, float 0x3FE85BC440000000, float 0x3FE83B0E60000000, float 0x3FE81A1BA0000000, float 0x3FE7F8EC00000000, float 0x3FE7D783E0000000, float 0x3FE7B5DEE0000000, float 0x3FE7940140000000, float 0x3FE771E6C0000000, float 0x3FE74F93C0000000, float 0x3FE72D0800000000, float 0x3FE70A41A0000000, float 0x3FE6E744C0000000, float 0x3FE6C40D00000000, float 0x3FE6A09EE0000000, float 0x3FE67CF800000000, float 0x3FE65918A0000000, float 0x3FE63502A0000000, float 0x3FE610B840000000, float 0x3FE5EC3540000000, float 0x3FE5C77BC0000000, float 0x3FE5A28DC0000000, float 0x3FE57D6940000000, float 0x3FE5581060000000, float 0x3FE5328300000000, float 0x3FE50CC140000000, float 0x3FE4E6CB20000000, float 0x3FE4C0A060000000, float 0x3FE49A45A0000000, float 0x3FE473B420000000, float 0x3FE44CF280000000, float 0x3FE425FEA0000000, float 0x3FE3FED840000000, float 0x3FE3D781C0000000, float 0x3FE3AFFB00000000, float 0x3FE38841E0000000, float 0x3FE36058A0000000, float 0x3FE3383F00000000, float 0x3FE30FF760000000, float 0x3FE2E78180000000, float 0x3FE2BEDB80000000, float 0x3FE2960740000000, float 0x3FE26D04E0000000, float 0x3FE243D680000000, float 0x3FE21A7A00000000, float 0x3FE1F0F160000000, float 0x3FE1C73AC0000000, float 0x3FE19D5A20000000, float 0x3FE1734D60000000, float 0x3FE14914C0000000, float 0x3FE11EB420000000, float 0x3FE0F42780000000, float 0x3FE0C97100000000, float 0x3FE09E9080000000, float 0x3FE0738820000000, float 0x3FE04855E0000000, float 0x3FE01CFBC0000000, float 0x3FDFE2F7C0000000, float 0x3FDF8BA400000000, float 0x3FDF3404E0000000, float 0x3FDEDC1A40000000, float 0x3FDE83E000000000, float 0x3FDE2B5E60000000, float 0x3FDDD28D00000000, float 0x3FDD7978A0000000, float 0x3FDD2018A0000000, float 0x3FDCC66D40000000, float 0x3FDC6C7EA0000000, float 0x3FDC1248E0000000, float 0x3FDBB7CFE0000000, float 0x3FDB5D0FA0000000, float 0x3FDB020C40000000, float 0x3FDAA6CA00000000, float 0x3FDA4B4080000000, float 0x3FD9EF7800000000, float 0x3FD9937080000000, float 0x3FD9372A40000000, float 0x3FD8DAA500000000, float 0x3FD87DE0E0000000, float 0x3FD820E200000000, float 0x3FD7C3A860000000, float 0x3FD7663420000000, float 0x3FD7088500000000, float 0x3FD6AA9B40000000, float 0x3FD64C7F40000000, float 0x3FD5EE2860000000, float 0x3FD58F9B20000000, float 0x3FD530D740000000, float 0x3FD4D1E100000000, float 0x3FD472B880000000, float 0x3FD4135DA0000000, float 0x3FD3B3D080000000, float 0x3FD3541100000000, float 0x3FD2F42360000000, float 0x3FD2940780000000, float 0x3FD233B960000000, float 0x3FD1D34580000000, float 0x3FD1729F60000000, float 0x3FD111D360000000, float 0x3FD0B0D960000000, float 0x3FD04FB980000000, float 0x3FCFDCDF60000000, float 0x3FCF19F800000000, float 0x3FCE56CD60000000, float 0x3FCD934F00000000, float 0x3FCCCF8D80000000, float 0x3FCC0B8060000000, float 0x3FCB473040000000, float 0x3FCA829D00000000, float 0x3FC9BDCF00000000, float 0x3FC8F8B580000000, float 0x3FC83369C0000000, float 0x3FC76DDAC0000000, float 0x3FC6A81120000000, float 0x3FC5E21540000000, float 0x3FC51BDE80000000, float 0x3FC4557580000000, float 0x3FC38EDA20000000, float 0x3FC2C80C80000000, float 0x3FC20114E0000000, float 0x3FC139F340000000, float 0x3FC0729F60000000, float 0x3FBF5653C0000000, float 0x3FBDC71500000000, float 0x3FBC378240000000, float 0x3FBAA7BD40000000, float 0x3FB917A460000000, float 0x3FB7875920000000, float 0x3FB5F6CAC0000000, float 0x3FB4660A20000000, float 0x3FB2D527E0000000, float 0x3FB1440280000000, float 0x3FAF657760000000, float 0x3FAC4284E0000000, float 0x3FA91F70E0000000, float 0x3FA5FBF840000000, float 0x3FA2D85E00000000, float 0x3F9F694460000000, float 0x3F992146A0000000, float 0x3F92D948E0000000, float 0x3F892189C0000000, float 0x3F79221000000000, float -0.000000e+00, float 0xBF79221000000000, float 0xBF89221000000000, float 0xBF92D948E0000000, float 0xBF992146A0000000, float 0xBF9F694460000000, float 0xBFA2D85E00000000, float 0xBFA5FBF840000000, float 0xBFA91F70E0000000, float 0xBFAC4284E0000000, float 0xBFAF657760000000, float 0xBFB1440280000000, float 0xBFB2D527E0000000, float 0xBFB4660A20000000, float 0xBFB5F6CAC0000000, float 0xBFB7875920000000, float 0xBFB917A460000000, float 0xBFBAA7BD40000000, float 0xBFBC378240000000, float 0xBFBDC71500000000, float 0xBFBF5653C0000000, float 0xBFC0729F60000000, float 0xBFC139F340000000, float 0xBFC20114E0000000, float 0xBFC2C814E0000000, float 0xBFC38EDA20000000, float 0xBFC4557580000000, float 0xBFC51BDE80000000, float 0xBFC5E21540000000, float 0xBFC6A81120000000, float 0xBFC76DDAC0000000, float 0xBFC83369C0000000, float 0xBFC8F8B580000000, float 0xBFC9BDCF00000000, float 0xBFCA829D00000000, float 0xBFCB473040000000, float 0xBFCC0B8060000000, float 0xBFCCCF8D80000000, float 0xBFCD934F00000000, float 0xBFCE56CD60000000, float 0xBFCF19F800000000, float 0xBFCFDCDF60000000, float 0xBFD04FB980000000, float 0xBFD0B0D960000000, float 0xBFD111D360000000, float 0xBFD1729F60000000, float 0xBFD1D34580000000, float 0xBFD233BDA0000000, float 0xBFD2940780000000, float 0xBFD2F42360000000, float 0xBFD3541100000000, float 0xBFD3B3D080000000, float 0xBFD4135DA0000000, float 0xBFD472B880000000, float 0xBFD4D1E100000000, float 0xBFD530D740000000, float 0xBFD58F9B20000000, float 0xBFD5EE2860000000, float 0xBFD64C7F40000000, float 0xBFD6AA9F80000000, float 0xBFD7088500000000, float 0xBFD7663420000000, float 0xBFD7C3A860000000, float 0xBFD820E200000000, float 0xBFD87DE0E0000000, float 0xBFD8DAA500000000, float 0xBFD9372A40000000, float 0xBFD9937080000000, float 0xBFD9EF7800000000, float 0xBFDA4B4080000000, float 0xBFDAA6CA00000000, float 0xBFDB020C40000000, float 0xBFDB5D0FA0000000, float 0xBFDBB7CFE0000000, float 0xBFDC1248E0000000, float 0xBFDC6C7EA0000000, float 0xBFDCC66D40000000, float 0xBFDD2018A0000000, float 0xBFDD7978A0000000, float 0xBFDDD29140000000, float 0xBFDE2B5E60000000, float 0xBFDE83E000000000, float 0xBFDEDC1A40000000, float 0xBFDF3404E0000000, float 0xBFDF8BA400000000, float 0xBFDFE2F7C0000000, float 0xBFE01CFBC0000000, float 0xBFE04855E0000000, float 0xBFE0738820000000, float 0xBFE09E9080000000, float 0xBFE0C97100000000, float 0xBFE0F42780000000, float 0xBFE11EB420000000, float 0xBFE14916C0000000, float 0xBFE1734D60000000, float 0xBFE19D5A20000000, float 0xBFE1C73AC0000000, float 0xBFE1F0F160000000, float 0xBFE21A7A00000000, float 0xBFE243D680000000, float 0xBFE26D04E0000000, float 0xBFE2960740000000, float 0xBFE2BEDB80000000, float 0xBFE2E78180000000, float 0xBFE30FF760000000, float 0xBFE3384120000000, float 0xBFE36058A0000000, float 0xBFE38841E0000000, float 0xBFE3AFFB00000000, float 0xBFE3D781C0000000, float 0xBFE3FEDA60000000, float 0xBFE425FEA0000000, float 0xBFE44CF280000000, float 0xBFE473B640000000, float 0xBFE49A45A0000000, float 0xBFE4C0A060000000, float 0xBFE4E6CB20000000, float 0xBFE50CC140000000, float 0xBFE5328300000000, float 0xBFE5581060000000, float 0xBFE57D6940000000, float 0xBFE5A28DC0000000, float 0xBFE5C77BC0000000, float 0xBFE5EC3540000000, float 0xBFE610B840000000, float 0xBFE63504C0000000, float 0xBFE65918A0000000, float 0xBFE67CF800000000, float 0xBFE6A09EE0000000, float 0xBFE6C40D00000000, float 0xBFE6E744C0000000, float 0xBFE70A43C0000000, float 0xBFE72D0800000000, float 0xBFE74F93C0000000, float 0xBFE771E6C0000000, float 0xBFE7940140000000, float 0xBFE7B5DEE0000000, float 0xBFE7D783E0000000, float 0xBFE7F8EC00000000, float 0xBFE81A1BA0000000, float 0xBFE83B0E60000000, float 0xBFE85BC440000000, float 0xBFE87C3F80000000, float 0xBFE89C7DE0000000, float 0xBFE8BC7F80000000, float 0xBFE8DC4440000000, float 0xBFE8FBCC20000000, float 0xBFE91B1740000000, float 0xBFE93A2160000000, float 0xBFE958F0C0000000, float 0xBFE9777F20000000, float 0xBFE995CEA0000000, float 0xBFE9B3E140000000, float 0xBFE9D1B2E0000000, float 0xBFE9EF4380000000, float 0xBFEA0C9540000000, float 0xBFEA29A800000000, float 0xBFEA4679C0000000, float 0xBFEA630880000000, float 0xBFEA7F5840000000, float 0xBFEA9B6700000000, float 0xBFEAB732A0000000, float 0xBFEAD2BD40000000, float 0xBFEAEE04C0000000, float 0xBFEB090B40000000, float 0xBFEB23CC80000000, float 0xBFEB3E4CE0000000, float 0xBFEB588A00000000, float 0xBFEB728420000000, float 0xBFEB8C3900000000, float 0xBFEBA5AAC0000000, float 0xBFEBBED740000000, float 0xBFEBD7C0C0000000, float 0xBFEBF064E0000000, float 0xBFEC08C400000000, float 0xBFEC20DDC0000000, float 0xBFEC38B260000000, float 0xBFEC5041C0000000, float 0xBFEC678C00000000, float 0xBFEC7E8EE0000000, float 0xBFEC954A80000000, float 0xBFECABC0E0000000, float 0xBFECC1F000000000, float 0xBFECD7D9E0000000, float 0xBFECED7A60000000, float 0xBFED02D5A0000000, float 0xBFED17E780000000, float 0xBFED2CB200000000, float 0xBFED413560000000, float 0xBFED556F40000000, float 0xBFED6961C0000000, float 0xBFED7D0AE0000000, float 0xBFED906CC0000000, float 0xBFEDA38320000000, float 0xBFEDB65240000000, float 0xBFEDC8D7E0000000, float 0xBFEDDB1420000000, float 0xBFEDED0700000000, float 0xBFEDFEAE60000000, float 0xBFEE100C60000000, float 0xBFEE2120E0000000, float 0xBFEE31EC00000000, float 0xBFEE4269A0000000, float 0xBFEE529FE0000000, float 0xBFEE628880000000, float 0xBFEE7227E0000000, float 0xBFEE817BA0000000, float 0xBFEE9083E0000000, float 0xBFEE9F40A0000000, float 0xBFEEADB400000000, float 0xBFEEBBD9C0000000, float 0xBFEEC9B200000000, float 0xBFEED740C0000000, float 0xBFEEE48200000000, float 0xBFEEF177A0000000, float 0xBFEEFE21E0000000, float 0xBFEF0A7E80000000, float 0xBFEF168F80000000, float 0xBFEF225320000000, float 0xBFEF2DC900000000, float 0xBFEF38F360000000, float 0xBFEF43D040000000, float 0xBFEF4E5F80000000, float 0xBFEF58A320000000, float 0xBFEF629740000000, float 0xBFEF6C3FC0000000, float 0xBFEF7598A0000000, float 0xBFEF7EA600000000, float 0xBFEF8765C0000000, float 0xBFEF8FD5C0000000, float 0xBFEF97F840000000, float 0xBFEF9FCF40000000, float 0xBFEFA75680000000, float 0xBFEFAE8E20000000, float 0xBFEFB57A40000000, float 0xBFEFBC16A0000000, float 0xBFEFC26560000000, float 0xBFEFC86480000000, float 0xBFEFCE1600000000, float 0xBFEFD37A00000000, float 0xBFEFD88E40000000, float 0xBFEFDD52C0000000, float 0xBFEFE1CBC0000000, float 0xBFEFE5F300000000, float 0xBFEFE9CCC0000000, float 0xBFEFED58C0000000, float 0xBFEFF09520000000, float 0xBFEFF38400000000, float 0xBFEFF620E0000000, float 0xBFEFF87260000000, float 0xBFEFFA7220000000, float 0xBFEFFC2440000000, float 0xBFEFFD88C0000000, float 0xBFEFFE9DA0000000, float 0xBFEFFF62C0000000, float 0xBFEFFFD820000000], align 16 ; [#uses=1 type=[512 x float]*]
@W_imag = internal unnamed_addr constant [512 x float] [float -0.000000e+00, float 0xBF79221000000000, float 0xBF89221000000000, float 0xBF92D948E0000000, float 0xBF992146A0000000, float 0xBF9F694460000000, float 0xBFA2D85E00000000, float 0xBFA5FBF840000000, float 0xBFA91F70E0000000, float 0xBFAC4284E0000000, float 0xBFAF657760000000, float 0xBFB1440280000000, float 0xBFB2D527E0000000, float 0xBFB4660A20000000, float 0xBFB5F6CAC0000000, float 0xBFB7875920000000, float 0xBFB917A460000000, float 0xBFBAA7BD40000000, float 0xBFBC378240000000, float 0xBFBDC71500000000, float 0xBFBF5653C0000000, float 0xBFC0729F60000000, float 0xBFC139F340000000, float 0xBFC20114E0000000, float 0xBFC2C80C80000000, float 0xBFC38EDA20000000, float 0xBFC4557580000000, float 0xBFC51BDE80000000, float 0xBFC5E21540000000, float 0xBFC6A81120000000, float 0xBFC76DDAC0000000, float 0xBFC83369C0000000, float 0xBFC8F8B580000000, float 0xBFC9BDCF00000000, float 0xBFCA829D00000000, float 0xBFCB473040000000, float 0xBFCC0B8060000000, float 0xBFCCCF8D80000000, float 0xBFCD934F00000000, float 0xBFCE56CD60000000, float 0xBFCF19F800000000, float 0xBFCFDCDF60000000, float 0xBFD04FB980000000, float 0xBFD0B0D960000000, float 0xBFD111D360000000, float 0xBFD1729F60000000, float 0xBFD1D34580000000, float 0xBFD233BDA0000000, float 0xBFD2940780000000, float 0xBFD2F42360000000, float 0xBFD3541100000000, float 0xBFD3B3D080000000, float 0xBFD4135DA0000000, float 0xBFD472B880000000, float 0xBFD4D1E100000000, float 0xBFD530D740000000, float 0xBFD58F9B20000000, float 0xBFD5EE2860000000, float 0xBFD64C7F40000000, float 0xBFD6AA9F80000000, float 0xBFD7088500000000, float 0xBFD7663420000000, float 0xBFD7C3A860000000, float 0xBFD820E200000000, float 0xBFD87DE0E0000000, float 0xBFD8DAA500000000, float 0xBFD9372A40000000, float 0xBFD9937080000000, float 0xBFD9EF7800000000, float 0xBFDA4B4080000000, float 0xBFDAA6CA00000000, float 0xBFDB020C40000000, float 0xBFDB5D0FA0000000, float 0xBFDBB7CFE0000000, float 0xBFDC1248E0000000, float 0xBFDC6C7EA0000000, float 0xBFDCC66D40000000, float 0xBFDD2018A0000000, float 0xBFDD7978A0000000, float 0xBFDDD29140000000, float 0xBFDE2B5E60000000, float 0xBFDE83E000000000, float 0xBFDEDC1A40000000, float 0xBFDF3404E0000000, float 0xBFDF8BA400000000, float 0xBFDFE2F7C0000000, float 0xBFE01CFBC0000000, float 0xBFE04855E0000000, float 0xBFE0738820000000, float 0xBFE09E9080000000, float 0xBFE0C97100000000, float 0xBFE0F42780000000, float 0xBFE11EB420000000, float 0xBFE14916C0000000, float 0xBFE1734D60000000, float 0xBFE19D5A20000000, float 0xBFE1C73AC0000000, float 0xBFE1F0F160000000, float 0xBFE21A7A00000000, float 0xBFE243D680000000, float 0xBFE26D04E0000000, float 0xBFE2960740000000, float 0xBFE2BEDB80000000, float 0xBFE2E78180000000, float 0xBFE30FF760000000, float 0xBFE3384120000000, float 0xBFE36058A0000000, float 0xBFE38841E0000000, float 0xBFE3AFFB00000000, float 0xBFE3D781C0000000, float 0xBFE3FEDA60000000, float 0xBFE425FEA0000000, float 0xBFE44CF280000000, float 0xBFE473B420000000, float 0xBFE49A45A0000000, float 0xBFE4C0A060000000, float 0xBFE4E6CB20000000, float 0xBFE50CC140000000, float 0xBFE5328300000000, float 0xBFE5581060000000, float 0xBFE57D6940000000, float 0xBFE5A28DC0000000, float 0xBFE5C77BC0000000, float 0xBFE5EC3540000000, float 0xBFE610B840000000, float 0xBFE63502A0000000, float 0xBFE65918A0000000, float 0xBFE67CF800000000, float 0xBFE6A09EE0000000, float 0xBFE6C40D00000000, float 0xBFE6E744C0000000, float 0xBFE70A43C0000000, float 0xBFE72D0800000000, float 0xBFE74F93C0000000, float 0xBFE771E6C0000000, float 0xBFE7940140000000, float 0xBFE7B5DEE0000000, float 0xBFE7D783E0000000, float 0xBFE7F8EC00000000, float 0xBFE81A1BA0000000, float 0xBFE83B0E60000000, float 0xBFE85BC440000000, float 0xBFE87C3F80000000, float 0xBFE89C7DE0000000, float 0xBFE8BC7F80000000, float 0xBFE8DC4440000000, float 0xBFE8FBCC20000000, float 0xBFE91B1740000000, float 0xBFE93A2160000000, float 0xBFE958F0C0000000, float 0xBFE9777F20000000, float 0xBFE995CEA0000000, float 0xBFE9B3E140000000, float 0xBFE9D1B2E0000000, float 0xBFE9EF4380000000, float 0xBFEA0C9540000000, float 0xBFEA29A800000000, float 0xBFEA4679C0000000, float 0xBFEA630880000000, float 0xBFEA7F5840000000, float 0xBFEA9B6700000000, float 0xBFEAB732A0000000, float 0xBFEAD2BD40000000, float 0xBFEAEE04C0000000, float 0xBFEB090B40000000, float 0xBFEB23CC80000000, float 0xBFEB3E4CE0000000, float 0xBFEB588A00000000, float 0xBFEB728420000000, float 0xBFEB8C3900000000, float 0xBFEBA5AAC0000000, float 0xBFEBBED740000000, float 0xBFEBD7C0C0000000, float 0xBFEBF064E0000000, float 0xBFEC08C400000000, float 0xBFEC20DDC0000000, float 0xBFEC38B260000000, float 0xBFEC5041C0000000, float 0xBFEC678C00000000, float 0xBFEC7E8EE0000000, float 0xBFEC954A80000000, float 0xBFECABC0E0000000, float 0xBFECC1F000000000, float 0xBFECD7D9E0000000, float 0xBFECED7A60000000, float 0xBFED02D5A0000000, float 0xBFED17E780000000, float 0xBFED2CB200000000, float 0xBFED413560000000, float 0xBFED556F40000000, float 0xBFED6961C0000000, float 0xBFED7D0AE0000000, float 0xBFED906CC0000000, float 0xBFEDA38320000000, float 0xBFEDB65240000000, float 0xBFEDC8D7E0000000, float 0xBFEDDB1420000000, float 0xBFEDED0700000000, float 0xBFEDFEAE60000000, float 0xBFEE100C60000000, float 0xBFEE2120E0000000, float 0xBFEE31EA00000000, float 0xBFEE4269A0000000, float 0xBFEE529FE0000000, float 0xBFEE628880000000, float 0xBFEE7227E0000000, float 0xBFEE817BA0000000, float 0xBFEE9083E0000000, float 0xBFEE9F40A0000000, float 0xBFEEADB1E0000000, float 0xBFEEBBD9C0000000, float 0xBFEEC9B200000000, float 0xBFEED740C0000000, float 0xBFEEE48200000000, float 0xBFEEF177A0000000, float 0xBFEEFE21E0000000, float 0xBFEF0A7E80000000, float 0xBFEF168F80000000, float 0xBFEF225320000000, float 0xBFEF2DC900000000, float 0xBFEF38F360000000, float 0xBFEF43D040000000, float 0xBFEF4E5F80000000, float 0xBFEF58A320000000, float 0xBFEF629740000000, float 0xBFEF6C3FC0000000, float 0xBFEF7598A0000000, float 0xBFEF7EA600000000, float 0xBFEF8765C0000000, float 0xBFEF8FD5C0000000, float 0xBFEF97F840000000, float 0xBFEF9FCF40000000, float 0xBFEFA75680000000, float 0xBFEFAE8E20000000, float 0xBFEFB57A40000000, float 0xBFEFBC16A0000000, float 0xBFEFC26560000000, float 0xBFEFC86480000000, float 0xBFEFCE1600000000, float 0xBFEFD37A00000000, float 0xBFEFD88E40000000, float 0xBFEFDD52C0000000, float 0xBFEFE1CBC0000000, float 0xBFEFE5F300000000, float 0xBFEFE9CCC0000000, float 0xBFEFED58C0000000, float 0xBFEFF09520000000, float 0xBFEFF38400000000, float 0xBFEFF620E0000000, float 0xBFEFF87260000000, float 0xBFEFFA7220000000, float 0xBFEFFC2440000000, float 0xBFEFFD88C0000000, float 0xBFEFFE9DA0000000, float 0xBFEFFF62C0000000, float 0xBFEFFFD820000000, float -1.000000e+00, float 0xBFEFFFD820000000, float 0xBFEFFF62C0000000, float 0xBFEFFE9DA0000000, float 0xBFEFFD88C0000000, float 0xBFEFFC2440000000, float 0xBFEFFA7220000000, float 0xBFEFF87260000000, float 0xBFEFF620E0000000, float 0xBFEFF38400000000, float 0xBFEFF09520000000, float 0xBFEFED58C0000000, float 0xBFEFE9CCC0000000, float 0xBFEFE5F300000000, float 0xBFEFE1CBC0000000, float 0xBFEFDD52C0000000, float 0xBFEFD88E40000000, float 0xBFEFD37A00000000, float 0xBFEFCE1600000000, float 0xBFEFC86480000000, float 0xBFEFC26560000000, float 0xBFEFBC16A0000000, float 0xBFEFB57A40000000, float 0xBFEFAE8E20000000, float 0xBFEFA75680000000, float 0xBFEF9FCF40000000, float 0xBFEF97F840000000, float 0xBFEF8FD5C0000000, float 0xBFEF8765C0000000, float 0xBFEF7EA600000000, float 0xBFEF7598A0000000, float 0xBFEF6C3FC0000000, float 0xBFEF629740000000, float 0xBFEF58A320000000, float 0xBFEF4E5F80000000, float 0xBFEF43D040000000, float 0xBFEF38F360000000, float 0xBFEF2DC900000000, float 0xBFEF225320000000, float 0xBFEF168F80000000, float 0xBFEF0A7E80000000, float 0xBFEEFE21E0000000, float 0xBFEEF177A0000000, float 0xBFEEE48200000000, float 0xBFEED740C0000000, float 0xBFEEC9B200000000, float 0xBFEEBBD9C0000000, float 0xBFEEADB1E0000000, float 0xBFEE9F40A0000000, float 0xBFEE9083E0000000, float 0xBFEE817BA0000000, float 0xBFEE7227E0000000, float 0xBFEE628880000000, float 0xBFEE529FE0000000, float 0xBFEE4269A0000000, float 0xBFEE31EA00000000, float 0xBFEE2120E0000000, float 0xBFEE100C60000000, float 0xBFEDFEAE60000000, float 0xBFEDED04E0000000, float 0xBFEDDB1420000000, float 0xBFEDC8D7E0000000, float 0xBFEDB65240000000, float 0xBFEDA38320000000, float 0xBFED906CC0000000, float 0xBFED7D0AE0000000, float 0xBFED6961C0000000, float 0xBFED556F40000000, float 0xBFED413560000000, float 0xBFED2CB200000000, float 0xBFED17E780000000, float 0xBFED02D5A0000000, float 0xBFECED7A60000000, float 0xBFECD7D9E0000000, float 0xBFECC1F000000000, float 0xBFECABC0E0000000, float 0xBFEC954A80000000, float 0xBFEC7E8EE0000000, float 0xBFEC678C00000000, float 0xBFEC5041C0000000, float 0xBFEC38B260000000, float 0xBFEC20DDC0000000, float 0xBFEC08C400000000, float 0xBFEBF064E0000000, float 0xBFEBD7C0C0000000, float 0xBFEBBED740000000, float 0xBFEBA5AAC0000000, float 0xBFEB8C3900000000, float 0xBFEB728420000000, float 0xBFEB588A00000000, float 0xBFEB3E4CE0000000, float 0xBFEB23CC80000000, float 0xBFEB090B40000000, float 0xBFEAEE04C0000000, float 0xBFEAD2BD40000000, float 0xBFEAB732A0000000, float 0xBFEA9B6700000000, float 0xBFEA7F5840000000, float 0xBFEA630880000000, float 0xBFEA4677C0000000, float 0xBFEA29A800000000, float 0xBFEA0C9540000000, float 0xBFE9EF4380000000, float 0xBFE9D1B2E0000000, float 0xBFE9B3E140000000, float 0xBFE995CEA0000000, float 0xBFE9777F20000000, float 0xBFE958F0C0000000, float 0xBFE93A2160000000, float 0xBFE91B1740000000, float 0xBFE8FBCC20000000, float 0xBFE8DC4440000000, float 0xBFE8BC7F80000000, float 0xBFE89C7DE0000000, float 0xBFE87C3F80000000, float 0xBFE85BC440000000, float 0xBFE83B0E60000000, float 0xBFE81A1BA0000000, float 0xBFE7F8EC00000000, float 0xBFE7D783E0000000, float 0xBFE7B5DEE0000000, float 0xBFE7940140000000, float 0xBFE771E6C0000000, float 0xBFE74F93C0000000, float 0xBFE72D0800000000, float 0xBFE70A41A0000000, float 0xBFE6E744C0000000, float 0xBFE6C40D00000000, float 0xBFE6A09EE0000000, float 0xBFE67CF800000000, float 0xBFE65918A0000000, float 0xBFE63502A0000000, float 0xBFE610B840000000, float 0xBFE5EC3540000000, float 0xBFE5C77BC0000000, float 0xBFE5A28DC0000000, float 0xBFE57D6940000000, float 0xBFE5581060000000, float 0xBFE5328300000000, float 0xBFE50CC140000000, float 0xBFE4E6CB20000000, float 0xBFE4C0A060000000, float 0xBFE49A4380000000, float 0xBFE473B420000000, float 0xBFE44CF280000000, float 0xBFE425FEA0000000, float 0xBFE3FED840000000, float 0xBFE3D781C0000000, float 0xBFE3AFFB00000000, float 0xBFE38841E0000000, float 0xBFE36058A0000000, float 0xBFE3383F00000000, float 0xBFE30FF760000000, float 0xBFE2E78180000000, float 0xBFE2BEDB80000000, float 0xBFE2960740000000, float 0xBFE26D04E0000000, float 0xBFE243D680000000, float 0xBFE21A7A00000000, float 0xBFE1F0EF60000000, float 0xBFE1C73AC0000000, float 0xBFE19D5A20000000, float 0xBFE1734D60000000, float 0xBFE14914C0000000, float 0xBFE11EB420000000, float 0xBFE0F42780000000, float 0xBFE0C97100000000, float 0xBFE09E9080000000, float 0xBFE0738820000000, float 0xBFE04855E0000000, float 0xBFE01CFBC0000000, float 0xBFDFE2F7C0000000, float 0xBFDF8BA400000000, float 0xBFDF3404E0000000, float 0xBFDEDC1A40000000, float 0xBFDE83E000000000, float 0xBFDE2B5E60000000, float 0xBFDDD28D00000000, float 0xBFDD7978A0000000, float 0xBFDD2018A0000000, float 0xBFDCC66D40000000, float 0xBFDC6C7EA0000000, float 0xBFDC1248E0000000, float 0xBFDBB7CFE0000000, float 0xBFDB5D0FA0000000, float 0xBFDB020C40000000, float 0xBFDAA6C5E0000000, float 0xBFDA4B4080000000, float 0xBFD9EF7800000000, float 0xBFD9937080000000, float 0xBFD9372A40000000, float 0xBFD8DAA500000000, float 0xBFD87DE0E0000000, float 0xBFD820E200000000, float 0xBFD7C3A860000000, float 0xBFD7663420000000, float 0xBFD7088500000000, float 0xBFD6AA9B40000000, float 0xBFD64C7F40000000, float 0xBFD5EE2860000000, float 0xBFD58F9B20000000, float 0xBFD530D740000000, float 0xBFD4D1E100000000, float 0xBFD472B880000000, float 0xBFD4135DA0000000, float 0xBFD3B3D080000000, float 0xBFD3541100000000, float 0xBFD2F42360000000, float 0xBFD2940780000000, float 0xBFD233B960000000, float 0xBFD1D34580000000, float 0xBFD1729F60000000, float 0xBFD111D360000000, float 0xBFD0B0D960000000, float 0xBFD04FB980000000, float 0xBFCFDCDF60000000, float 0xBFCF19F800000000, float 0xBFCE56CD60000000, float 0xBFCD934F00000000, float 0xBFCCCF8D80000000, float 0xBFCC0B8060000000, float 0xBFCB473040000000, float 0xBFCA829D00000000, float 0xBFC9BDCF00000000, float 0xBFC8F8B580000000, float 0xBFC83369C0000000, float 0xBFC76DDAC0000000, float 0xBFC6A81120000000, float 0xBFC5E21540000000, float 0xBFC51BDE80000000, float 0xBFC4557580000000, float 0xBFC38EDA20000000, float 0xBFC2C80C80000000, float 0xBFC20114E0000000, float 0xBFC139F340000000, float 0xBFC0729F60000000, float 0xBFBF5653C0000000, float 0xBFBDC71500000000, float 0xBFBC378240000000, float 0xBFBAA7BD40000000, float 0xBFB917A460000000, float 0xBFB7875920000000, float 0xBFB5F6CAC0000000, float 0xBFB4660A20000000, float 0xBFB2D51720000000, float 0xBFB1440280000000, float 0xBFAF657760000000, float 0xBFAC4284E0000000, float 0xBFA91F70E0000000, float 0xBFA5FBF840000000, float 0xBFA2D85E00000000, float 0xBF9F694460000000, float 0xBF992146A0000000, float 0xBF92D948E0000000, float 0xBF892189C0000000, float 0xBF79221000000000], align 16 ; [#uses=1 type=[512 x float]*]
@.str3 = private unnamed_addr constant [7 x i8] c"DFTpts\00", align 1 ; [#uses=3 type=[7 x i8]*]
@.str2 = private unnamed_addr constant [10 x i8] c"butterfly\00", align 1 ; [#uses=3 type=[10 x i8]*]
@.str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1 ; [#uses=3 type=[1 x i8]*]

; [#uses=16]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=0]
define void @fft_stage_first([512 x float]* %"real_i[0]", [512 x float]* %"real_i[1]", [512 x float]* %"imag_i[0]", [512 x float]* %"imag_i[1]", [512 x float]* %"real_o[0]", [512 x float]* %"real_o[1]", [512 x float]* %"imag_o[0]", [512 x float]* %"imag_o[1]") {
codeRepl:
  call void (...)* @_ssdm_op_SpecDataflowPipeline(i32 -1, [1 x i8]* @.str1) nounwind, !dbg !29 ; [debug line = 19:1]
  call void (...)* @_ssdm_op_SpecBitsMap([512 x float]* %"real_o[1]"), !map !35
  call void (...)* @_ssdm_op_SpecBitsMap([512 x float]* %"real_o[0]"), !map !41
  call void (...)* @_ssdm_op_SpecBitsMap([512 x float]* %"imag_o[1]"), !map !47
  call void (...)* @_ssdm_op_SpecBitsMap([512 x float]* %"imag_o[0]"), !map !51
  call void (...)* @_ssdm_op_SpecBitsMap([512 x float]* %"imag_i[1]"), !map !55
  call void (...)* @_ssdm_op_SpecBitsMap([512 x float]* %"imag_i[0]"), !map !59
  call void (...)* @_ssdm_op_SpecBitsMap([512 x float]* %"real_i[1]"), !map !63
  call void (...)* @_ssdm_op_SpecBitsMap([512 x float]* %"real_i[0]"), !map !67
  call void (...)* @_ssdm_op_SpecTopModule([16 x i8]* @fft_stage_first.str) nounwind
  call void @llvm.dbg.value(metadata !{[512 x float]* %"real_i[0]"}, i64 0, metadata !71), !dbg !76 ; [debug line = 14:28] [debug variable = real_i[0]]
  call void @llvm.dbg.value(metadata !{[512 x float]* %"real_i[1]"}, i64 0, metadata !77), !dbg !76 ; [debug line = 14:28] [debug variable = real_i[1]]
  call void @llvm.dbg.value(metadata !{[512 x float]* %"imag_i[0]"}, i64 0, metadata !78), !dbg !80 ; [debug line = 14:66] [debug variable = imag_i[0]]
  call void @llvm.dbg.value(metadata !{[512 x float]* %"imag_i[1]"}, i64 0, metadata !81), !dbg !80 ; [debug line = 14:66] [debug variable = imag_i[1]]
  call void @llvm.dbg.value(metadata !{[512 x float]* %"real_o[0]"}, i64 0, metadata !82), !dbg !84 ; [debug line = 14:104] [debug variable = real_o[0]]
  call void @llvm.dbg.value(metadata !{[512 x float]* %"real_o[1]"}, i64 0, metadata !85), !dbg !84 ; [debug line = 14:104] [debug variable = real_o[1]]
  call void @llvm.dbg.value(metadata !{[512 x float]* %"imag_o[0]"}, i64 0, metadata !86), !dbg !88 ; [debug line = 14:142] [debug variable = imag_o[0]]
  call void @llvm.dbg.value(metadata !{[512 x float]* %"imag_o[1]"}, i64 0, metadata !89), !dbg !88 ; [debug line = 14:142] [debug variable = imag_o[1]]
  call fastcc void @Loop_butterfly_proc([512 x float]* %"imag_o[0]", [512 x float]* %"imag_o[1]", [512 x float]* %"real_o[0]", [512 x float]* %"real_o[1]", [512 x float]* %"imag_i[0]", [512 x float]* %"imag_i[1]", [512 x float]* %"real_i[0]", [512 x float]* %"real_i[1]")
  ret void, !dbg !90                              ; [debug line = 45:1]
}

; [#uses=1]
declare void @_ssdm_op_SpecTopModule(...)

; [#uses=2]
declare i32 @_ssdm_op_SpecRegionEnd(...)

; [#uses=2]
declare i32 @_ssdm_op_SpecRegionBegin(...)

; [#uses=1]
declare void @_ssdm_op_SpecPipeline(...) nounwind

; [#uses=2]
declare void @_ssdm_op_SpecLoopTripCount(...) nounwind

; [#uses=2]
declare void @_ssdm_op_SpecLoopName(...) nounwind

; [#uses=1]
declare void @_ssdm_op_SpecDataflowPipeline(...) nounwind

; [#uses=8]
declare void @_ssdm_op_SpecBitsMap(...)

; [#uses=1]
declare void @_GLOBAL__I_a() nounwind

; [#uses=1]
define internal fastcc void @Loop_butterfly_proc([512 x float]* nocapture %"imag_o[0]", [512 x float]* nocapture %"imag_o[1]", [512 x float]* nocapture %"real_o[0]", [512 x float]* nocapture %"real_o[1]", [512 x float]* nocapture %"imag_i[0]", [512 x float]* nocapture %"imag_i[1]", [512 x float]* nocapture %"real_i[0]", [512 x float]* nocapture %"real_i[1]") {
newFuncRoot:
  br label %0

fft_stage_first_.exit2.exitStub:                  ; preds = %0
  ret void

; <label>:0                                       ; preds = %1, %newFuncRoot
  %k.0.i.i = phi i10 [ 0, %newFuncRoot ], [ %k, %1 ] ; [#uses=2 type=i10]
  %i = phi i1 [ false, %newFuncRoot ], [ true, %1 ] ; [#uses=2 type=i1]
  %i.cast = zext i1 %i to i32, !dbg !91           ; [#uses=1 type=i32] [debug line = 24:26]
  call void (...)* @_ssdm_op_SpecLoopTripCount(i64 1, i64 1, i64 1)
  br i1 %i, label %fft_stage_first_.exit2.exitStub, label %4, !dbg !91 ; [debug line = 24:26]

; <label>:1                                       ; preds = %3
  %k = xor i10 %k.0.i.i, -512, !dbg !93           ; [#uses=1 type=i10] [debug line = 42:3]
  call void @llvm.dbg.value(metadata !{i10 %k}, i64 0, metadata !95) nounwind, !dbg !93 ; [debug line = 42:3] [debug variable = k]
  %2 = call i32 (...)* @_ssdm_op_SpecRegionEnd([10 x i8]* @.str2, i32 %tmp) nounwind, !dbg !97 ; [#uses=0 type=i32] [debug line = 43:2]
  br label %0, !dbg !98                           ; [debug line = 24:35]

; <label>:3                                       ; preds = %5, %4
  %i.0.i.i = phi i32 [ %i.cast, %4 ], [ %i.1, %5 ] ; [#uses=5 type=i32]
  %tmp.1.i = icmp slt i32 %i.0.i.i, 1024, !dbg !99 ; [#uses=1 type=i1] [debug line = 29:23]
  br i1 %tmp.1.i, label %_ifconv, label %1, !dbg !99 ; [debug line = 29:23]

; <label>:4                                       ; preds = %0
  call void (...)* @_ssdm_op_SpecLoopName([10 x i8]* @.str2) nounwind, !dbg !101 ; [debug line = 24:41]
  %tmp = call i32 (...)* @_ssdm_op_SpecRegionBegin([10 x i8]* @.str2) nounwind, !dbg !101 ; [#uses=1 type=i32] [debug line = 24:41]
  %tmp.i = zext i10 %k.0.i.i to i64, !dbg !102    ; [#uses=2 type=i64] [debug line = 26:22]
  %W_real.addr = getelementptr inbounds [512 x float]* @W_real, i64 0, i64 %tmp.i, !dbg !102 ; [#uses=1 type=float*] [debug line = 26:22]
  %c = load float* %W_real.addr, align 16, !dbg !102 ; [#uses=2 type=float] [debug line = 26:22]
  call void @llvm.dbg.value(metadata !{float %c}, i64 0, metadata !103) nounwind, !dbg !102 ; [debug line = 26:22] [debug variable = c]
  %W_imag.addr = getelementptr inbounds [512 x float]* @W_imag, i64 0, i64 %tmp.i, !dbg !104 ; [#uses=1 type=float*] [debug line = 27:22]
  %s = load float* %W_imag.addr, align 16, !dbg !104 ; [#uses=2 type=float] [debug line = 27:22]
  call void @llvm.dbg.value(metadata !{float %s}, i64 0, metadata !105) nounwind, !dbg !104 ; [debug line = 27:22] [debug variable = s]
  call void @llvm.dbg.value(metadata !{i1 %i}, i64 0, metadata !106) nounwind, !dbg !99 ; [debug line = 29:23] [debug variable = i]
  br label %3, !dbg !99                           ; [debug line = 29:23]

; <label>:5                                       ; preds = %branch19, %branch08
  %6 = call i32 (...)* @_ssdm_op_SpecRegionEnd([7 x i8]* @.str3, i32 %tmp.1) nounwind, !dbg !107 ; [#uses=0 type=i32] [debug line = 41:4]
  %i.1 = add nsw i32 %i.0.i.i, 2, !dbg !109       ; [#uses=1 type=i32] [debug line = 29:53]
  call void @llvm.dbg.value(metadata !{i32 %i.1}, i64 0, metadata !106) nounwind, !dbg !109 ; [debug line = 29:53] [debug variable = i]
  br label %3, !dbg !109                          ; [debug line = 29:53]

branch08:                                         ; preds = %7
  store float %tmp.10.i, float* %"imag_o[0].addr.1", align 4, !dbg !110 ; [debug line = 40:4]
  br label %5, !dbg !110                          ; [debug line = 40:4]

; <label>:7                                       ; preds = %branch320, %branch219
  %tmp.10.i = fadd float %imag_i.load.1.i.phi, %temp_I, !dbg !110 ; [#uses=2 type=float] [debug line = 40:4]
  %"imag_o[0].addr.1" = getelementptr [512 x float]* %"imag_o[0]", i64 0, i64 %newIndex1, !dbg !110 ; [#uses=1 type=float*] [debug line = 40:4]
  %"imag_o[1].addr.1" = getelementptr [512 x float]* %"imag_o[1]", i64 0, i64 %newIndex1, !dbg !110 ; [#uses=1 type=float*] [debug line = 40:4]
  br i1 %arrayNo5, label %branch19, label %branch08, !dbg !110 ; [debug line = 40:4]

branch219:                                        ; preds = %8
  store float %tmp..i, float* %"real_o[0].addr.1", align 4, !dbg !111 ; [debug line = 39:4]
  br label %7, !dbg !111                          ; [debug line = 39:4]

; <label>:8                                       ; preds = %branch318, %branch217
  %tmp..i = fadd float %real_i.load.1.i.phi, %temp_R, !dbg !111 ; [#uses=2 type=float] [debug line = 39:4]
  %"real_o[0].addr.1" = getelementptr [512 x float]* %"real_o[0]", i64 0, i64 %newIndex1, !dbg !111 ; [#uses=1 type=float*] [debug line = 39:4]
  %"real_o[1].addr.1" = getelementptr [512 x float]* %"real_o[1]", i64 0, i64 %newIndex1, !dbg !111 ; [#uses=1 type=float*] [debug line = 39:4]
  br i1 %arrayNo5, label %branch320, label %branch219, !dbg !111 ; [debug line = 39:4]

branch217:                                        ; preds = %_ifconv1
  store float %tmp.9.i, float* %"imag_o[1].addr", align 4, !dbg !112 ; [debug line = 38:4]
  br label %8, !dbg !112                          ; [debug line = 38:4]

_ifconv1:                                         ; preds = %branch113, %branch012
  %"imag_i[0].addr.1" = getelementptr [512 x float]* %"imag_i[0]", i64 0, i64 %newIndex1, !dbg !112 ; [#uses=1 type=float*] [debug line = 38:4]
  %"imag_i[1].addr.1" = getelementptr [512 x float]* %"imag_i[1]", i64 0, i64 %newIndex1, !dbg !112 ; [#uses=1 type=float*] [debug line = 38:4]
  %"imag_i[1].load.1" = load float* %"imag_i[1].addr.1", align 4, !dbg !112 ; [#uses=1 type=float] [debug line = 38:4]
  %"imag_i[0].load.1" = load float* %"imag_i[0].addr.1", align 4, !dbg !112 ; [#uses=1 type=float] [debug line = 38:4]
  %imag_i.load.1.i.phi = select i1 %arrayNo5, float %"imag_i[1].load.1", float %"imag_i[0].load.1", !dbg !112 ; [#uses=2 type=float] [debug line = 38:4]
  %tmp.9.i = fsub float %imag_i.load.1.i.phi, %temp_I, !dbg !112 ; [#uses=2 type=float] [debug line = 38:4]
  %"imag_o[1].addr" = getelementptr [512 x float]* %"imag_o[1]", i64 0, i64 %newIndex2, !dbg !112 ; [#uses=1 type=float*] [debug line = 38:4]
  %"imag_o[0].addr" = getelementptr [512 x float]* %"imag_o[0]", i64 0, i64 %newIndex2, !dbg !112 ; [#uses=1 type=float*] [debug line = 38:4]
  br i1 %arrayNo5, label %branch318, label %branch217, !dbg !112 ; [debug line = 38:4]

branch012:                                        ; preds = %_ifconv
  store float %tmp.8.i, float* %"real_o[1].addr", align 4, !dbg !113 ; [debug line = 37:4]
  br label %_ifconv1, !dbg !113                   ; [debug line = 37:4]

_ifconv:                                          ; preds = %3
  call void (...)* @_ssdm_op_SpecLoopName([7 x i8]* @.str3) nounwind, !dbg !114 ; [debug line = 29:62]
  %tmp.1 = call i32 (...)* @_ssdm_op_SpecRegionBegin([7 x i8]* @.str3) nounwind, !dbg !114 ; [#uses=1 type=i32] [debug line = 29:62]
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @.str1) nounwind, !dbg !115 ; [debug line = 30:1]
  call void (...)* @_ssdm_op_SpecLoopTripCount(i32 0, i32 0, i32 0, [1 x i8]* @.str1) nounwind, !dbg !116 ; [debug line = 31:1]
  %i_lower = add nsw i32 %i.0.i.i, 1, !dbg !117   ; [#uses=1 type=i32] [debug line = 33:21]
  call void @llvm.dbg.value(metadata !{i32 %i_lower}, i64 0, metadata !118) nounwind, !dbg !117 ; [debug line = 33:21] [debug variable = i_lower]
  %arrayNo5 = trunc i32 %i.0.i.i to i1            ; [#uses=8 type=i1]
  %newIndex6 = lshr i32 %i_lower, 1               ; [#uses=1 type=i32]
  %newIndex2 = zext i32 %newIndex6 to i64         ; [#uses=8 type=i64]
  %"real_i[1].addr" = getelementptr [512 x float]* %"real_i[1]", i64 0, i64 %newIndex2, !dbg !119 ; [#uses=1 type=float*] [debug line = 34:56]
  %"real_i[0].addr" = getelementptr [512 x float]* %"real_i[0]", i64 0, i64 %newIndex2, !dbg !119 ; [#uses=1 type=float*] [debug line = 34:56]
  %"real_i[0].load" = load float* %"real_i[0].addr", align 4, !dbg !119 ; [#uses=1 type=float] [debug line = 34:56]
  %"real_i[1].load" = load float* %"real_i[1].addr", align 4, !dbg !119 ; [#uses=1 type=float] [debug line = 34:56]
  %real_i.load.i.phi = select i1 %arrayNo5, float %"real_i[0].load", float %"real_i[1].load", !dbg !119 ; [#uses=2 type=float] [debug line = 34:56]
  %tmp.3.i = fmul float %real_i.load.i.phi, %c, !dbg !119 ; [#uses=1 type=float] [debug line = 34:56]
  %"imag_i[1].addr" = getelementptr [512 x float]* %"imag_i[1]", i64 0, i64 %newIndex2, !dbg !119 ; [#uses=1 type=float*] [debug line = 34:56]
  %"imag_i[0].addr" = getelementptr [512 x float]* %"imag_i[0]", i64 0, i64 %newIndex2, !dbg !119 ; [#uses=1 type=float*] [debug line = 34:56]
  %"imag_i[0].load" = load float* %"imag_i[0].addr", align 4, !dbg !119 ; [#uses=1 type=float] [debug line = 34:56]
  %"imag_i[1].load" = load float* %"imag_i[1].addr", align 4, !dbg !119 ; [#uses=1 type=float] [debug line = 34:56]
  %imag_i.load.i.phi = select i1 %arrayNo5, float %"imag_i[0].load", float %"imag_i[1].load", !dbg !119 ; [#uses=2 type=float] [debug line = 34:56]
  %tmp.4.i = fmul float %imag_i.load.i.phi, %s, !dbg !119 ; [#uses=1 type=float] [debug line = 34:56]
  %temp_R = fsub float %tmp.3.i, %tmp.4.i, !dbg !119 ; [#uses=2 type=float] [debug line = 34:56]
  call void @llvm.dbg.value(metadata !{float %temp_R}, i64 0, metadata !120) nounwind, !dbg !119 ; [debug line = 34:56] [debug variable = temp_R]
  %tmp.5.i = fmul float %imag_i.load.i.phi, %c, !dbg !121 ; [#uses=1 type=float] [debug line = 35:56]
  %tmp.6.i = fmul float %real_i.load.i.phi, %s, !dbg !121 ; [#uses=1 type=float] [debug line = 35:56]
  %temp_I = fadd float %tmp.5.i, %tmp.6.i, !dbg !121 ; [#uses=2 type=float] [debug line = 35:56]
  call void @llvm.dbg.value(metadata !{float %temp_I}, i64 0, metadata !122) nounwind, !dbg !121 ; [debug line = 35:56] [debug variable = temp_I]
  %newIndex = lshr i32 %i.0.i.i, 1                ; [#uses=1 type=i32]
  %newIndex1 = zext i32 %newIndex to i64          ; [#uses=8 type=i64]
  %"real_i[0].addr.1" = getelementptr [512 x float]* %"real_i[0]", i64 0, i64 %newIndex1, !dbg !113 ; [#uses=1 type=float*] [debug line = 37:4]
  %"real_i[1].addr.1" = getelementptr [512 x float]* %"real_i[1]", i64 0, i64 %newIndex1, !dbg !113 ; [#uses=1 type=float*] [debug line = 37:4]
  %"real_i[1].load.1" = load float* %"real_i[1].addr.1", align 4, !dbg !113 ; [#uses=1 type=float] [debug line = 37:4]
  %"real_i[0].load.1" = load float* %"real_i[0].addr.1", align 4, !dbg !113 ; [#uses=1 type=float] [debug line = 37:4]
  %real_i.load.1.i.phi = select i1 %arrayNo5, float %"real_i[1].load.1", float %"real_i[0].load.1", !dbg !113 ; [#uses=2 type=float] [debug line = 37:4]
  %tmp.8.i = fsub float %real_i.load.1.i.phi, %temp_R, !dbg !113 ; [#uses=2 type=float] [debug line = 37:4]
  %"real_o[1].addr" = getelementptr [512 x float]* %"real_o[1]", i64 0, i64 %newIndex2, !dbg !113 ; [#uses=1 type=float*] [debug line = 37:4]
  %"real_o[0].addr" = getelementptr [512 x float]* %"real_o[0]", i64 0, i64 %newIndex2, !dbg !113 ; [#uses=1 type=float*] [debug line = 37:4]
  br i1 %arrayNo5, label %branch113, label %branch012, !dbg !113 ; [debug line = 37:4]

branch113:                                        ; preds = %_ifconv
  store float %tmp.8.i, float* %"real_o[0].addr", align 4, !dbg !113 ; [debug line = 37:4]
  br label %_ifconv1, !dbg !113                   ; [debug line = 37:4]

branch318:                                        ; preds = %_ifconv1
  store float %tmp.9.i, float* %"imag_o[0].addr", align 4, !dbg !112 ; [debug line = 38:4]
  br label %8, !dbg !112                          ; [debug line = 38:4]

branch320:                                        ; preds = %8
  store float %tmp..i, float* %"real_o[1].addr.1", align 4, !dbg !111 ; [debug line = 39:4]
  br label %7, !dbg !111                          ; [debug line = 39:4]

branch19:                                         ; preds = %7
  store float %tmp.10.i, float* %"imag_o[1].addr.1", align 4, !dbg !110 ; [debug line = 40:4]
  br label %5, !dbg !110                          ; [debug line = 40:4]
}

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}
!llvm.dbg.cu = !{!7}

!0 = metadata !{metadata !1, [1 x i32]* @llvm.global_ctors.0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 0, i32 1}
!7 = metadata !{i32 786449, i32 0, i32 4, metadata !"D:/Projects/vivado/project_4/HLS/1_Subcomponents/fft_stage_first/fft_stage_first/solution3/.autopilot/db/fft_stage_first.pragma.2.cpp", metadata !"d:/Projects/vivado/project_4/HLS/1_Subcomponents/fft_stage_first", metadata !"clang version 3.1 ", i1 true, i1 false, metadata !"", i32 0, null, null, null, metadata !8} ; [ DW_TAG_compile_unit ]
!8 = metadata !{metadata !9}
!9 = metadata !{metadata !10, metadata !19, metadata !20}
!10 = metadata !{i32 786484, i32 0, null, metadata !"W_real", metadata !"W_real", metadata !"_ZL6W_real", metadata !11, i32 15, metadata !12, i32 1, i32 1, [512 x float]* @W_real} ; [ DW_TAG_variable ]
!11 = metadata !{i32 786473, metadata !"./fft_stage_first.h", metadata !"d:/Projects/vivado/project_4/HLS/1_Subcomponents/fft_stage_first", null} ; [ DW_TAG_file_type ]
!12 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 16384, i64 32, i32 0, i32 0, metadata !13, metadata !17, i32 0, i32 0} ; [ DW_TAG_array_type ]
!13 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !14} ; [ DW_TAG_const_type ]
!14 = metadata !{i32 786454, null, metadata !"DTYPE", metadata !15, i32 8, i64 0, i64 0, i64 0, i32 0, metadata !16} ; [ DW_TAG_typedef ]
!15 = metadata !{i32 786473, metadata !"fft_stage_first.cpp", metadata !"d:/Projects/vivado/project_4/HLS/1_Subcomponents/fft_stage_first", null} ; [ DW_TAG_file_type ]
!16 = metadata !{i32 786468, null, metadata !"float", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 4} ; [ DW_TAG_base_type ]
!17 = metadata !{metadata !18}
!18 = metadata !{i32 786465, i64 0, i64 511}      ; [ DW_TAG_subrange_type ]
!19 = metadata !{i32 786484, i32 0, null, metadata !"W_imag", metadata !"W_imag", metadata !"_ZL6W_imag", metadata !11, i32 16, metadata !12, i32 1, i32 1, [512 x float]* @W_imag} ; [ DW_TAG_variable ]
!20 = metadata !{i32 786484, i32 0, null, metadata !"ssdm_global_array_ins", metadata !"ssdm_global_array_ins", metadata !"_ZL21ssdm_global_array_ins", metadata !15, i32 55, metadata !21, i32 1, i32 1, null} ; [ DW_TAG_variable ]
!21 = metadata !{i32 786434, null, metadata !"ssdm_global_array_fft_stage_firstpp0cppaplinecpp", metadata !15, i32 48, i64 8, i64 8, i32 0, i32 0, null, metadata !22, i32 0, null, null} ; [ DW_TAG_class_type ]
!22 = metadata !{metadata !23}
!23 = metadata !{i32 786478, i32 0, metadata !21, metadata !"ssdm_global_array_fft_stage_firstpp0cppaplinecpp", metadata !"ssdm_global_array_fft_stage_firstpp0cppaplinecpp", metadata !"", metadata !15, i32 50, metadata !24, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !27, i32 50} ; [ DW_TAG_subprogram ]
!24 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !25, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!25 = metadata !{null, metadata !26}
!26 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !21} ; [ DW_TAG_pointer_type ]
!27 = metadata !{metadata !28}
!28 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!29 = metadata !{i32 19, i32 1, metadata !30, null}
!30 = metadata !{i32 786443, metadata !31, i32 14, i32 174, metadata !15, i32 0} ; [ DW_TAG_lexical_block ]
!31 = metadata !{i32 786478, i32 0, metadata !15, metadata !"fft_stage_first", metadata !"fft_stage_first", metadata !"_Z15fft_stage_firstPfS_S_S_", metadata !15, i32 14, metadata !32, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !27, i32 14} ; [ DW_TAG_subprogram ]
!32 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !33, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!33 = metadata !{null, metadata !34, metadata !34, metadata !34, metadata !34}
!34 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !14} ; [ DW_TAG_pointer_type ]
!35 = metadata !{metadata !36}
!36 = metadata !{i32 0, i32 31, metadata !37}
!37 = metadata !{metadata !38}
!38 = metadata !{metadata !"real_o", metadata !39, metadata !"float", i32 0, i32 31}
!39 = metadata !{metadata !40}
!40 = metadata !{i32 1, i32 1023, i32 2}
!41 = metadata !{metadata !42}
!42 = metadata !{i32 0, i32 31, metadata !43}
!43 = metadata !{metadata !44}
!44 = metadata !{metadata !"real_o", metadata !45, metadata !"float", i32 0, i32 31}
!45 = metadata !{metadata !46}
!46 = metadata !{i32 0, i32 1022, i32 2}
!47 = metadata !{metadata !48}
!48 = metadata !{i32 0, i32 31, metadata !49}
!49 = metadata !{metadata !50}
!50 = metadata !{metadata !"imag_o", metadata !39, metadata !"float", i32 0, i32 31}
!51 = metadata !{metadata !52}
!52 = metadata !{i32 0, i32 31, metadata !53}
!53 = metadata !{metadata !54}
!54 = metadata !{metadata !"imag_o", metadata !45, metadata !"float", i32 0, i32 31}
!55 = metadata !{metadata !56}
!56 = metadata !{i32 0, i32 31, metadata !57}
!57 = metadata !{metadata !58}
!58 = metadata !{metadata !"imag_i", metadata !39, metadata !"float", i32 0, i32 31}
!59 = metadata !{metadata !60}
!60 = metadata !{i32 0, i32 31, metadata !61}
!61 = metadata !{metadata !62}
!62 = metadata !{metadata !"imag_i", metadata !45, metadata !"float", i32 0, i32 31}
!63 = metadata !{metadata !64}
!64 = metadata !{i32 0, i32 31, metadata !65}
!65 = metadata !{metadata !66}
!66 = metadata !{metadata !"real_i", metadata !39, metadata !"float", i32 0, i32 31}
!67 = metadata !{metadata !68}
!68 = metadata !{i32 0, i32 31, metadata !69}
!69 = metadata !{metadata !70}
!70 = metadata !{metadata !"real_i", metadata !45, metadata !"float", i32 0, i32 31}
!71 = metadata !{i32 790531, metadata !72, metadata !"real_i[0]", null, i32 14, metadata !14, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!72 = metadata !{i32 786689, metadata !31, metadata !"real_i", null, i32 14, metadata !73, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!73 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 0, i64 0, i32 0, i32 0, metadata !14, metadata !74, i32 0, i32 0} ; [ DW_TAG_array_type ]
!74 = metadata !{metadata !75}
!75 = metadata !{i32 786465, i64 0, i64 1023}     ; [ DW_TAG_subrange_type ]
!76 = metadata !{i32 14, i32 28, metadata !31, null}
!77 = metadata !{i32 790531, metadata !72, metadata !"real_i[1]", null, i32 14, metadata !14, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!78 = metadata !{i32 790531, metadata !79, metadata !"imag_i[0]", null, i32 14, metadata !14, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!79 = metadata !{i32 786689, metadata !31, metadata !"imag_i", null, i32 14, metadata !73, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!80 = metadata !{i32 14, i32 66, metadata !31, null}
!81 = metadata !{i32 790531, metadata !79, metadata !"imag_i[1]", null, i32 14, metadata !14, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!82 = metadata !{i32 790531, metadata !83, metadata !"real_o[0]", null, i32 14, metadata !14, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!83 = metadata !{i32 786689, metadata !31, metadata !"real_o", null, i32 14, metadata !73, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!84 = metadata !{i32 14, i32 104, metadata !31, null}
!85 = metadata !{i32 790531, metadata !83, metadata !"real_o[1]", null, i32 14, metadata !14, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!86 = metadata !{i32 790531, metadata !87, metadata !"imag_o[0]", null, i32 14, metadata !14, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!87 = metadata !{i32 786689, metadata !31, metadata !"imag_o", null, i32 14, metadata !73, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!88 = metadata !{i32 14, i32 142, metadata !31, null}
!89 = metadata !{i32 790531, metadata !87, metadata !"imag_o[1]", null, i32 14, metadata !14, i32 0, i32 0} ; [ DW_TAG_arg_variable_field ]
!90 = metadata !{i32 45, i32 1, metadata !30, null}
!91 = metadata !{i32 24, i32 26, metadata !92, null}
!92 = metadata !{i32 786443, metadata !30, i32 24, i32 13, metadata !15, i32 1} ; [ DW_TAG_lexical_block ]
!93 = metadata !{i32 42, i32 3, metadata !94, null}
!94 = metadata !{i32 786443, metadata !92, i32 24, i32 40, metadata !15, i32 2} ; [ DW_TAG_lexical_block ]
!95 = metadata !{i32 786688, metadata !30, metadata !"k", metadata !15, i32 22, metadata !96, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!96 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!97 = metadata !{i32 43, i32 2, metadata !94, null}
!98 = metadata !{i32 24, i32 35, metadata !92, null}
!99 = metadata !{i32 29, i32 23, metadata !100, null}
!100 = metadata !{i32 786443, metadata !94, i32 29, i32 10, metadata !15, i32 3} ; [ DW_TAG_lexical_block ]
!101 = metadata !{i32 24, i32 41, metadata !94, null}
!102 = metadata !{i32 26, i32 22, metadata !94, null}
!103 = metadata !{i32 786688, metadata !94, metadata !"c", metadata !15, i32 26, metadata !14, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!104 = metadata !{i32 27, i32 22, metadata !94, null}
!105 = metadata !{i32 786688, metadata !94, metadata !"s", metadata !15, i32 27, metadata !14, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!106 = metadata !{i32 786688, metadata !100, metadata !"i", metadata !15, i32 29, metadata !96, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!107 = metadata !{i32 41, i32 4, metadata !108, null}
!108 = metadata !{i32 786443, metadata !100, i32 29, i32 61, metadata !15, i32 4} ; [ DW_TAG_lexical_block ]
!109 = metadata !{i32 29, i32 53, metadata !100, null}
!110 = metadata !{i32 40, i32 4, metadata !108, null}
!111 = metadata !{i32 39, i32 4, metadata !108, null}
!112 = metadata !{i32 38, i32 4, metadata !108, null}
!113 = metadata !{i32 37, i32 4, metadata !108, null}
!114 = metadata !{i32 29, i32 62, metadata !108, null}
!115 = metadata !{i32 30, i32 1, metadata !108, null}
!116 = metadata !{i32 31, i32 1, metadata !108, null}
!117 = metadata !{i32 33, i32 21, metadata !108, null}
!118 = metadata !{i32 786688, metadata !108, metadata !"i_lower", metadata !15, i32 33, metadata !96, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!119 = metadata !{i32 34, i32 56, metadata !108, null}
!120 = metadata !{i32 786688, metadata !108, metadata !"temp_R", metadata !15, i32 34, metadata !14, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!121 = metadata !{i32 35, i32 56, metadata !108, null}
!122 = metadata !{i32 786688, metadata !108, metadata !"temp_I", metadata !15, i32 35, metadata !14, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
