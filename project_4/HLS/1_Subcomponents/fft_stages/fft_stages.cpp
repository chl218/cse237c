/**
 * Function: fft_stage_first_op calculates the first stage of FFT.
 * 
 * INPUT:
 *		In_R, In_I[]: Real and Imag parts of Complex signal
 *		stage: stage number: //Possible values of STAGE=2,3,4,5,6,7,8,9
 *
 * OUTPUT:
 * 		OUT_R, OUT_I[]: Real and Imag parts of Complex signal
 **/

#include <stdio.h>
#include "fft_stages.h"

//=======================BEGIN: FFT fft_stages =========================
void fft_stages(DTYPE real_i[SIZE], DTYPE imag_i[SIZE], int stage, DTYPE real_o[SIZE], DTYPE imag_o[SIZE]) {

	int DFTpts 	= 1 << stage;		// DFT = 2^stage = points in sub DFT
	int numBF 	= DFTpts/2; 			// Butterfly WIDTHS in sub-DFT
	int k 		= 0;
	int step 	= step_table[stage];

	printf("stage: %5d\tsteps: %5d\tnumBF :%5d\tDFTpts: %5d\n",
				stage, step, numBF, DFTpts);

	butterfly: for(int j = 0; j < numBF; j++) {

		DTYPE c = W_real[k];
		DTYPE s = W_imag[k];

		DFTpts:for(int i = j; i < SIZE; i += DFTpts) {

			int i_lower = i + numBF;
			DTYPE temp_R = real_i[i_lower]*c - imag_i[i_lower]*s;
			DTYPE temp_I = imag_i[i_lower]*c + real_i[i_lower]*s;

			real_o[i_lower] = real_i[i] - temp_R;
			imag_o[i_lower] = imag_i[i] - temp_I;
			real_o[i] = real_i[i] + temp_R;
			imag_o[i] = imag_i[i] + temp_I;
	 	}
		k+=step;
	}

}
//=======================END: FFT fft_stages =========================

