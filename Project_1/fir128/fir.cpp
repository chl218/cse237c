/*
	Filename: fir.cpp
		FIR lab wirtten for WES/CSE237C class at UCSD.
		Match filter
	INPUT:
		x: signal (chirp)

	OUTPUT:
		y: filtered output

*/

#include "fir.h"
void fir (data_t *y, data_t x) {

	static ap_int<8> shift_reg[N];
	#pragma HLS ARRAY_PARTITION variable=shift_reg cyclic factor=8 dim=1

	ap_int<8> c[N] = {10, 11, 11, 8, 3, -3, -8, -11, -11, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -10, -11, -11, -8, -3, 3, 8, 11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 8, 3, -3, -8, -11, -11, -10, -10, -10, -10, -10, -10, -10, -10, -11, -11, -8, -3, 3, 8, 11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 11, 11, 8, 3, -3, -8, -11, -11, -10, -10, -10, -10, -10, -10, -10, -10, -11, -11, -8, -3, 3, 8, 11, 11, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10, 10};
	ap_int<16> acc = 0;
	ap_int<16> data;
	ap_int<8> i = 0;
	Shift_Accum_Loop: for(i = N-1; i >= 0; i--) {
		if(i == 0) {
			shift_reg[0] = x;
			data = x;
		}
		else {
			shift_reg[i] = shift_reg[i-1];
			data = shift_reg[i];
		}
		acc += data * c[i];
	}
	*y = acc;
}



