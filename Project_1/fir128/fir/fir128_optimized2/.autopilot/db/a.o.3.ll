; ModuleID = 'D:/Projects/vivado/Project_1/project1/fir128/fir/fir128_optimized2/.autopilot/db/a.o.3.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@shift_reg = internal unnamed_addr global [128 x i32] zeroinitializer, align 16 ; [#uses=3 type=[128 x i32]*]
@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a] ; [#uses=0 type=[1 x void ()*]*]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535] ; [#uses=0 type=[1 x i32]*]
@fir_str = internal unnamed_addr constant [4 x i8] c"fir\00" ; [#uses=1 type=[4 x i8]*]
@c = internal unnamed_addr constant [128 x i5] [i5 10, i5 11, i5 11, i5 8, i5 3, i5 -3, i5 -8, i5 -11, i5 -11, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -11, i5 -11, i5 -8, i5 -3, i5 3, i5 8, i5 11, i5 11, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 11, i5 11, i5 8, i5 3, i5 -3, i5 -8, i5 -11, i5 -11, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -11, i5 -11, i5 -8, i5 -3, i5 3, i5 8, i5 11, i5 11, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 11, i5 11, i5 8, i5 3, i5 -3, i5 -8, i5 -11, i5 -11, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -11, i5 -11, i5 -8, i5 -3, i5 3, i5 8, i5 11, i5 11, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10] ; [#uses=1 type=[128 x i5]*]
@p_str = private unnamed_addr constant [17 x i8] c"Shift_Accum_Loop\00", align 1 ; [#uses=1 type=[17 x i8]*]

; [#uses=5]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=0]
define void @fir(i32* %y, i32 %x) nounwind uwtable {
.preheader:
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %y) nounwind, !map !7
  call void (...)* @_ssdm_op_SpecBitsMap(i32 %x) nounwind, !map !11
  call void (...)* @_ssdm_op_SpecTopModule([4 x i8]* @fir_str) nounwind
  %x_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %x) nounwind ; [#uses=3 type=i32]
  call void @llvm.dbg.value(metadata !{i32 %x_read}, i64 0, metadata !17), !dbg !27 ; [debug line = 14:29] [debug variable = x]
  call void @llvm.dbg.value(metadata !{i32* %y}, i64 0, metadata !28), !dbg !29 ; [debug line = 14:19] [debug variable = y]
  call void @llvm.dbg.value(metadata !{i32 %x}, i64 0, metadata !17), !dbg !27 ; [debug line = 14:29] [debug variable = x]
  br label %0, !dbg !30                           ; [debug line = 25:24]

; <label>:0                                       ; preds = %1, %.preheader
  %acc = phi i32 [ %acc_1, %1 ], [ 0, %.preheader ] ; [#uses=2 type=i32]
  %i = phi i7 [ %i_1, %1 ], [ -1, %.preheader ]   ; [#uses=3 type=i7]
  %tmp = icmp eq i7 %i, 0, !dbg !30               ; [#uses=1 type=i1] [debug line = 25:24]
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 127, i64 127, i64 127) nounwind ; [#uses=0 type=i32]
  br i1 %tmp, label %2, label %1, !dbg !30        ; [debug line = 25:24]

; <label>:1                                       ; preds = %0
  call void (...)* @_ssdm_op_SpecLoopName([17 x i8]* @p_str) nounwind, !dbg !33 ; [debug line = 25:47]
  %i_1 = add i7 %i, -1, !dbg !35                  ; [#uses=2 type=i7] [debug line = 26:3]
  %tmp_2 = zext i7 %i_1 to i64, !dbg !35          ; [#uses=1 type=i64] [debug line = 26:3]
  %shift_reg_addr = getelementptr inbounds [128 x i32]* @shift_reg, i64 0, i64 %tmp_2, !dbg !35 ; [#uses=1 type=i32*] [debug line = 26:3]
  %shift_reg_load = load i32* %shift_reg_addr, align 4, !dbg !35 ; [#uses=2 type=i32] [debug line = 26:3]
  %tmp_3 = zext i7 %i to i64, !dbg !35            ; [#uses=2 type=i64] [debug line = 26:3]
  %shift_reg_addr_1 = getelementptr inbounds [128 x i32]* @shift_reg, i64 0, i64 %tmp_3, !dbg !35 ; [#uses=1 type=i32*] [debug line = 26:3]
  store i32 %shift_reg_load, i32* %shift_reg_addr_1, align 4, !dbg !35 ; [debug line = 26:3]
  %c_addr = getelementptr [128 x i5]* @c, i64 0, i64 %tmp_3, !dbg !36 ; [#uses=1 type=i5*] [debug line = 27:3]
  %c_load = load i5* %c_addr, align 1, !dbg !36   ; [#uses=1 type=i5] [debug line = 27:3]
  %c_load_cast = sext i5 %c_load to i32, !dbg !36 ; [#uses=1 type=i32] [debug line = 27:3]
  %tmp_4 = mul nsw i32 %shift_reg_load, %c_load_cast, !dbg !36 ; [#uses=1 type=i32] [debug line = 27:3]
  %acc_1 = add nsw i32 %tmp_4, %acc, !dbg !36     ; [#uses=1 type=i32] [debug line = 27:3]
  call void @llvm.dbg.value(metadata !{i32 %acc_1}, i64 0, metadata !37), !dbg !36 ; [debug line = 27:3] [debug variable = acc]
  call void @llvm.dbg.value(metadata !{i7 %i_1}, i64 0, metadata !39), !dbg !40 ; [debug line = 25:41] [debug variable = i]
  br label %0, !dbg !40                           ; [debug line = 25:41]

; <label>:2                                       ; preds = %0
  store i32 %x_read, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 0), align 16, !dbg !41 ; [debug line = 30:2]
  %tmp_1 = shl i32 %x_read, 3, !dbg !42           ; [#uses=1 type=i32] [debug line = 31:2]
  %tmp_5 = shl i32 %x_read, 1, !dbg !42           ; [#uses=1 type=i32] [debug line = 31:2]
  %tmp1 = add i32 %tmp_5, %acc, !dbg !42          ; [#uses=1 type=i32] [debug line = 31:2]
  %tmp_7 = add nsw i32 %tmp1, %tmp_1, !dbg !42    ; [#uses=1 type=i32] [debug line = 31:2]
  call void @_ssdm_op_Write.ap_auto.i32P(i32* %y, i32 %tmp_7) nounwind, !dbg !42 ; [debug line = 31:2]
  ret void, !dbg !43                              ; [debug line = 33:1]
}

; [#uses=1]
define weak void @_ssdm_op_Write.ap_auto.i32P(i32*, i32) {
entry:
  store i32 %1, i32* %0
  ret void
}

; [#uses=1]
define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

; [#uses=1]
define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

; [#uses=1]
define weak void @_ssdm_op_SpecLoopName(...) nounwind {
entry:
  ret void
}

; [#uses=2]
define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

; [#uses=1]
define weak i32 @_ssdm_op_Read.ap_auto.i32(i32) {
entry:
  ret i32 %0
}

; [#uses=0]
declare i31 @_ssdm_op_PartSelect.i31.i32.i32.i32(i32, i32, i32) nounwind readnone

; [#uses=0]
declare i29 @_ssdm_op_PartSelect.i29.i32.i32.i32(i32, i32, i32) nounwind readnone

; [#uses=0]
declare i16 @_ssdm_op_HSub(...)

; [#uses=0]
declare i16 @_ssdm_op_HMul(...)

; [#uses=0]
declare i16 @_ssdm_op_HDiv(...)

; [#uses=0]
declare i16 @_ssdm_op_HAdd(...)

; [#uses=0]
declare i32 @_ssdm_op_BitConcatenate.i32.i31.i1(i31, i1) nounwind readnone

; [#uses=0]
declare i32 @_ssdm_op_BitConcatenate.i32.i29.i3(i29, i3) nounwind readnone

; [#uses=1]
declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [1 x i32]* @llvm_global_ctors_0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 0, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 31, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"y", metadata !5, metadata !"int", i32 0, i32 31}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 31, metadata !13}
!13 = metadata !{metadata !14}
!14 = metadata !{metadata !"x", metadata !15, metadata !"int", i32 0, i32 31}
!15 = metadata !{metadata !16}
!16 = metadata !{i32 0, i32 0, i32 0}
!17 = metadata !{i32 786689, metadata !18, metadata !"x", metadata !19, i32 33554446, metadata !23, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!18 = metadata !{i32 786478, i32 0, metadata !19, metadata !"fir", metadata !"fir", metadata !"_Z3firPii", metadata !19, i32 14, metadata !20, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (i32*, i32)* @fir, null, null, metadata !25, i32 14} ; [ DW_TAG_subprogram ]
!19 = metadata !{i32 786473, metadata !"fir.cpp", metadata !"d:/Projects/vivado/Project_1/project1/fir128", null} ; [ DW_TAG_file_type ]
!20 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !21, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!21 = metadata !{null, metadata !22, metadata !23}
!22 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !23} ; [ DW_TAG_pointer_type ]
!23 = metadata !{i32 786454, null, metadata !"data_t", metadata !19, i32 20, i64 0, i64 0, i64 0, i32 0, metadata !24} ; [ DW_TAG_typedef ]
!24 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!25 = metadata !{metadata !26}
!26 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!27 = metadata !{i32 14, i32 29, metadata !18, null}
!28 = metadata !{i32 786689, metadata !18, metadata !"y", metadata !19, i32 16777230, metadata !22, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!29 = metadata !{i32 14, i32 19, metadata !18, null}
!30 = metadata !{i32 25, i32 24, metadata !31, null}
!31 = metadata !{i32 786443, metadata !32, i32 25, i32 20, metadata !19, i32 1} ; [ DW_TAG_lexical_block ]
!32 = metadata !{i32 786443, metadata !18, i32 14, i32 32, metadata !19, i32 0} ; [ DW_TAG_lexical_block ]
!33 = metadata !{i32 25, i32 47, metadata !34, null}
!34 = metadata !{i32 786443, metadata !31, i32 25, i32 46, metadata !19, i32 2} ; [ DW_TAG_lexical_block ]
!35 = metadata !{i32 26, i32 3, metadata !34, null}
!36 = metadata !{i32 27, i32 3, metadata !34, null}
!37 = metadata !{i32 786688, metadata !32, metadata !"acc", metadata !19, i32 19, metadata !38, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!38 = metadata !{i32 786454, null, metadata !"acc_t", metadata !19, i32 21, i64 0, i64 0, i64 0, i32 0, metadata !24} ; [ DW_TAG_typedef ]
!39 = metadata !{i32 786688, metadata !32, metadata !"i", metadata !19, i32 24, metadata !24, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!40 = metadata !{i32 25, i32 41, metadata !31, null}
!41 = metadata !{i32 30, i32 2, metadata !32, null}
!42 = metadata !{i32 31, i32 2, metadata !32, null}
!43 = metadata !{i32 33, i32 1, metadata !32, null}
