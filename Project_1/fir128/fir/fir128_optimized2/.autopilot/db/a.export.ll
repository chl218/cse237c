; ModuleID = 'D:/Projects/vivado/Project_1/project1/fir128/fir/fir128_optimized2/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@shift_reg = internal unnamed_addr global [128 x i32] zeroinitializer, align 16
@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535]
@fir_str = internal unnamed_addr constant [4 x i8] c"fir\00"
@c = internal unnamed_addr constant [128 x i5] [i5 10, i5 11, i5 11, i5 8, i5 3, i5 -3, i5 -8, i5 -11, i5 -11, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -11, i5 -11, i5 -8, i5 -3, i5 3, i5 8, i5 11, i5 11, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 11, i5 11, i5 8, i5 3, i5 -3, i5 -8, i5 -11, i5 -11, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -11, i5 -11, i5 -8, i5 -3, i5 3, i5 8, i5 11, i5 11, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 11, i5 11, i5 8, i5 3, i5 -3, i5 -8, i5 -11, i5 -11, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -11, i5 -11, i5 -8, i5 -3, i5 3, i5 8, i5 11, i5 11, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10]
@p_str = private unnamed_addr constant [17 x i8] c"Shift_Accum_Loop\00", align 1

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

define void @fir(i32* %y, i32 %x) nounwind uwtable {
.preheader:
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %y) nounwind, !map !7
  call void (...)* @_ssdm_op_SpecBitsMap(i32 %x) nounwind, !map !11
  call void (...)* @_ssdm_op_SpecTopModule([4 x i8]* @fir_str) nounwind
  %x_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %x) nounwind
  br label %0

; <label>:0                                       ; preds = %1, %.preheader
  %acc = phi i32 [ %acc_1, %1 ], [ 0, %.preheader ]
  %i = phi i7 [ %i_1, %1 ], [ -1, %.preheader ]
  %tmp = icmp eq i7 %i, 0
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 127, i64 127, i64 127) nounwind
  br i1 %tmp, label %2, label %1

; <label>:1                                       ; preds = %0
  call void (...)* @_ssdm_op_SpecLoopName([17 x i8]* @p_str) nounwind
  %i_1 = add i7 %i, -1
  %tmp_2 = zext i7 %i_1 to i64
  %shift_reg_addr = getelementptr inbounds [128 x i32]* @shift_reg, i64 0, i64 %tmp_2
  %shift_reg_load = load i32* %shift_reg_addr, align 4
  %tmp_3 = zext i7 %i to i64
  %shift_reg_addr_1 = getelementptr inbounds [128 x i32]* @shift_reg, i64 0, i64 %tmp_3
  store i32 %shift_reg_load, i32* %shift_reg_addr_1, align 4
  %c_addr = getelementptr [128 x i5]* @c, i64 0, i64 %tmp_3
  %c_load = load i5* %c_addr, align 1
  %c_load_cast = sext i5 %c_load to i32
  %tmp_4 = mul nsw i32 %shift_reg_load, %c_load_cast
  %acc_1 = add nsw i32 %tmp_4, %acc
  br label %0

; <label>:2                                       ; preds = %0
  store i32 %x_read, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 0), align 16
  %tmp_1 = shl i32 %x_read, 3
  %tmp_5 = shl i32 %x_read, 1
  %tmp1 = add i32 %tmp_5, %acc
  %tmp_7 = add nsw i32 %tmp1, %tmp_1
  call void @_ssdm_op_Write.ap_auto.i32P(i32* %y, i32 %tmp_7) nounwind
  ret void
}

define weak void @_ssdm_op_Write.ap_auto.i32P(i32*, i32) {
entry:
  store i32 %1, i32* %0
  ret void
}

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecLoopName(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_Read.ap_auto.i32(i32) {
entry:
  ret i32 %0
}

declare i31 @_ssdm_op_PartSelect.i31.i32.i32.i32(i32, i32, i32) nounwind readnone

declare i29 @_ssdm_op_PartSelect.i29.i32.i32.i32(i32, i32, i32) nounwind readnone

declare i16 @_ssdm_op_HSub(...)

declare i16 @_ssdm_op_HMul(...)

declare i16 @_ssdm_op_HDiv(...)

declare i16 @_ssdm_op_HAdd(...)

declare i32 @_ssdm_op_BitConcatenate.i32.i31.i1(i31, i1) nounwind readnone

declare i32 @_ssdm_op_BitConcatenate.i32.i29.i3(i29, i3) nounwind readnone

declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [1 x i32]* @llvm_global_ctors_0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 0, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 31, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"y", metadata !5, metadata !"int", i32 0, i32 31}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 31, metadata !13}
!13 = metadata !{metadata !14}
!14 = metadata !{metadata !"x", metadata !15, metadata !"int", i32 0, i32 31}
!15 = metadata !{metadata !16}
!16 = metadata !{i32 0, i32 0, i32 0}
