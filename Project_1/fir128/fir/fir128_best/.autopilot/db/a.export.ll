; ModuleID = 'D:/Projects/vivado/Project_1/project1/fir128/fir/fir128_best/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@shift_reg_V_7 = internal unnamed_addr global [16 x i8] zeroinitializer
@shift_reg_V_6 = internal unnamed_addr global [16 x i8] zeroinitializer
@shift_reg_V_5 = internal unnamed_addr global [16 x i8] zeroinitializer
@shift_reg_V_4 = internal unnamed_addr global [16 x i8] zeroinitializer
@shift_reg_V_3 = internal unnamed_addr global [16 x i8] zeroinitializer
@shift_reg_V_2 = internal unnamed_addr global [16 x i8] zeroinitializer
@shift_reg_V_1 = internal unnamed_addr global [16 x i8] zeroinitializer
@shift_reg_V_0 = internal unnamed_addr global [16 x i8] zeroinitializer
@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535]
@fir_str = internal unnamed_addr constant [4 x i8] c"fir\00"
@c = internal unnamed_addr constant [128 x i5] [i5 10, i5 11, i5 11, i5 8, i5 3, i5 -3, i5 -8, i5 -11, i5 -11, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -11, i5 -11, i5 -8, i5 -3, i5 3, i5 8, i5 11, i5 11, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 11, i5 11, i5 8, i5 3, i5 -3, i5 -8, i5 -11, i5 -11, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -11, i5 -11, i5 -8, i5 -3, i5 3, i5 8, i5 11, i5 11, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 11, i5 11, i5 8, i5 3, i5 -3, i5 -8, i5 -11, i5 -11, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -11, i5 -11, i5 -8, i5 -3, i5 3, i5 8, i5 11, i5 11, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10]
@p_str2 = private unnamed_addr constant [17 x i8] c"Shift_Accum_Loop\00", align 1
@p_str1 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1

declare i7 @llvm.part.select.i7(i7, i32, i32) nounwind readnone

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

define void @fir(i32* %y, i32 %x) nounwind uwtable {
.preheader.preheader:
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %y) nounwind, !map !7
  call void (...)* @_ssdm_op_SpecBitsMap(i32 %x) nounwind, !map !11
  call void (...)* @_ssdm_op_SpecTopModule([4 x i8]* @fir_str) nounwind
  %x_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %x) nounwind
  br label %0

; <label>:0                                       ; preds = %2, %.preheader.preheader
  %p_s = phi i16 [ 0, %.preheader.preheader ], [ %acc_V, %2 ]
  %t_V = phi i7 [ -1, %.preheader.preheader ], [ %r_V, %2 ]
  %tmp = icmp eq i7 %t_V, 0
  br i1 %tmp, label %3, label %1

; <label>:1                                       ; preds = %0
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 127, i64 127, i64 127) nounwind
  call void (...)* @_ssdm_op_SpecLoopName([17 x i8]* @p_str2) nounwind
  %tmp_2 = call i32 (...)* @_ssdm_op_SpecRegionBegin([17 x i8]* @p_str2) nounwind
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str1) nounwind
  %tmp_1 = zext i7 %t_V to i64
  %r_V = add i7 -1, %t_V
  %arrayNo = call i3 @_ssdm_op_PartSelect.i3.i7.i32.i32(i7 %r_V, i32 4, i32 6)
  %arrayNo_cast = zext i3 %arrayNo to i33
  %tmp_8 = trunc i7 %r_V to i4
  %newIndex2 = zext i4 %tmp_8 to i64
  %shift_reg_V_0_addr_1 = getelementptr [16 x i8]* @shift_reg_V_0, i64 0, i64 %newIndex2
  %shift_reg_V_0_load = load i8* %shift_reg_V_0_addr_1, align 1
  %shift_reg_V_1_addr_1 = getelementptr [16 x i8]* @shift_reg_V_1, i64 0, i64 %newIndex2
  %shift_reg_V_1_load = load i8* %shift_reg_V_1_addr_1, align 1
  %shift_reg_V_2_addr_1 = getelementptr [16 x i8]* @shift_reg_V_2, i64 0, i64 %newIndex2
  %shift_reg_V_2_load = load i8* %shift_reg_V_2_addr_1, align 1
  %shift_reg_V_3_addr_1 = getelementptr [16 x i8]* @shift_reg_V_3, i64 0, i64 %newIndex2
  %shift_reg_V_3_load = load i8* %shift_reg_V_3_addr_1, align 1
  %shift_reg_V_4_addr_1 = getelementptr [16 x i8]* @shift_reg_V_4, i64 0, i64 %newIndex2
  %shift_reg_V_4_load = load i8* %shift_reg_V_4_addr_1, align 1
  %shift_reg_V_5_addr_1 = getelementptr [16 x i8]* @shift_reg_V_5, i64 0, i64 %newIndex2
  %shift_reg_V_5_load = load i8* %shift_reg_V_5_addr_1, align 1
  %shift_reg_V_6_addr_1 = getelementptr [16 x i8]* @shift_reg_V_6, i64 0, i64 %newIndex2
  %shift_reg_V_6_load = load i8* %shift_reg_V_6_addr_1, align 1
  %shift_reg_V_7_addr_1 = getelementptr [16 x i8]* @shift_reg_V_7, i64 0, i64 %newIndex2
  %shift_reg_V_7_load = load i8* %shift_reg_V_7_addr_1, align 1
  %tmp_3 = call i8 @_ssdm_op_Mux.ap_auto.8i8.i33(i8 %shift_reg_V_0_load, i8 %shift_reg_V_1_load, i8 %shift_reg_V_2_load, i8 %shift_reg_V_3_load, i8 %shift_reg_V_4_load, i8 %shift_reg_V_5_load, i8 %shift_reg_V_6_load, i8 %shift_reg_V_7_load, i33 %arrayNo_cast) nounwind
  %tmp_6 = call i3 @_ssdm_op_PartSelect.i3.i7.i32.i32(i7 %t_V, i32 4, i32 6)
  %tmp_10 = trunc i7 %t_V to i4
  %newIndex3 = zext i4 %tmp_10 to i64
  %shift_reg_V_0_addr = getelementptr [16 x i8]* @shift_reg_V_0, i64 0, i64 %newIndex3
  %shift_reg_V_1_addr = getelementptr [16 x i8]* @shift_reg_V_1, i64 0, i64 %newIndex3
  %shift_reg_V_2_addr = getelementptr [16 x i8]* @shift_reg_V_2, i64 0, i64 %newIndex3
  %shift_reg_V_3_addr = getelementptr [16 x i8]* @shift_reg_V_3, i64 0, i64 %newIndex3
  %shift_reg_V_4_addr = getelementptr [16 x i8]* @shift_reg_V_4, i64 0, i64 %newIndex3
  %shift_reg_V_5_addr = getelementptr [16 x i8]* @shift_reg_V_5, i64 0, i64 %newIndex3
  %shift_reg_V_6_addr = getelementptr [16 x i8]* @shift_reg_V_6, i64 0, i64 %newIndex3
  %shift_reg_V_7_addr = getelementptr [16 x i8]* @shift_reg_V_7, i64 0, i64 %newIndex3
  switch i3 %tmp_6, label %branch7 [
    i3 0, label %branch0
    i3 1, label %branch1
    i3 2, label %branch2
    i3 3, label %branch3
    i3 -4, label %branch4
    i3 -3, label %branch5
    i3 -2, label %branch6
  ]

; <label>:2                                       ; preds = %branch7, %branch6, %branch5, %branch4, %branch3, %branch2, %branch1, %branch0
  %lhs_V_1_cast_cast = sext i8 %tmp_3 to i13
  %c_addr = getelementptr [128 x i5]* @c, i64 0, i64 %tmp_1
  %c_load = load i5* %c_addr, align 1
  %rhs_V_cast_cast = sext i5 %c_load to i13
  %r_V_1 = mul i13 %rhs_V_cast_cast, %lhs_V_1_cast_cast
  %r_V_1_cast = sext i13 %r_V_1 to i16
  %acc_V = add i16 %p_s, %r_V_1_cast
  %empty_4 = call i32 (...)* @_ssdm_op_SpecRegionEnd([17 x i8]* @p_str2, i32 %tmp_2) nounwind
  br label %0

; <label>:3                                       ; preds = %0
  %tmp_4 = trunc i32 %x_read to i8
  store i8 %tmp_4, i8* getelementptr inbounds ([16 x i8]* @shift_reg_V_0, i64 0, i64 0), align 16
  %tmp_5 = shl i32 %x_read, 3
  %tmp_7 = shl i32 %x_read, 1
  %tmp_9 = sext i16 %p_s to i32
  %tmp1 = add i32 %tmp_7, %tmp_9
  %tmp_s = add i32 %tmp1, %tmp_5
  call void @_ssdm_op_Write.ap_auto.i32P(i32* %y, i32 %tmp_s) nounwind
  ret void

branch0:                                          ; preds = %1
  store i8 %tmp_3, i8* %shift_reg_V_0_addr, align 1
  br label %2

branch1:                                          ; preds = %1
  store i8 %tmp_3, i8* %shift_reg_V_1_addr, align 1
  br label %2

branch2:                                          ; preds = %1
  store i8 %tmp_3, i8* %shift_reg_V_2_addr, align 1
  br label %2

branch3:                                          ; preds = %1
  store i8 %tmp_3, i8* %shift_reg_V_3_addr, align 1
  br label %2

branch4:                                          ; preds = %1
  store i8 %tmp_3, i8* %shift_reg_V_4_addr, align 1
  br label %2

branch5:                                          ; preds = %1
  store i8 %tmp_3, i8* %shift_reg_V_5_addr, align 1
  br label %2

branch6:                                          ; preds = %1
  store i8 %tmp_3, i8* %shift_reg_V_6_addr, align 1
  br label %2

branch7:                                          ; preds = %1
  store i8 %tmp_3, i8* %shift_reg_V_7_addr, align 1
  br label %2
}

define weak void @_ssdm_op_Write.ap_auto.i32P(i32*, i32) {
entry:
  store i32 %1, i32* %0
  ret void
}

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecRegionEnd(...) {
entry:
  ret i32 0
}

define weak i32 @_ssdm_op_SpecRegionBegin(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecPipeline(...) nounwind {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecLoopName(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_Read.ap_auto.i32(i32) {
entry:
  ret i32 %0
}

declare i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32, i32, i32) nounwind readnone

declare i4 @_ssdm_op_PartSelect.i4.i7.i32.i32(i7, i32, i32) nounwind readnone

declare i31 @_ssdm_op_PartSelect.i31.i32.i32.i32(i32, i32, i32) nounwind readnone

define weak i3 @_ssdm_op_PartSelect.i3.i7.i32.i32(i7, i32, i32) nounwind readnone {
entry:
  %empty = call i7 @llvm.part.select.i7(i7 %0, i32 %1, i32 %2)
  %empty_5 = trunc i7 %empty to i3
  ret i3 %empty_5
}

declare i29 @_ssdm_op_PartSelect.i29.i32.i32.i32(i32, i32, i32) nounwind readnone

define weak i8 @_ssdm_op_Mux.ap_auto.8i8.i33(i8, i8, i8, i8, i8, i8, i8, i8, i33) {
entry:
  switch i33 %8, label %case7 [
    i33 0, label %case0
    i33 1, label %case1
    i33 2, label %case2
    i33 3, label %case3
    i33 4, label %case4
    i33 5, label %case5
    i33 6, label %case6
  ]

case0:                                            ; preds = %case7, %case6, %case5, %case4, %case3, %case2, %case1, %entry
  %merge = phi i8 [ %0, %entry ], [ %1, %case1 ], [ %2, %case2 ], [ %3, %case3 ], [ %4, %case4 ], [ %5, %case5 ], [ %6, %case6 ], [ %7, %case7 ]
  ret i8 %merge

case1:                                            ; preds = %entry
  br label %case0

case2:                                            ; preds = %entry
  br label %case0

case3:                                            ; preds = %entry
  br label %case0

case4:                                            ; preds = %entry
  br label %case0

case5:                                            ; preds = %entry
  br label %case0

case6:                                            ; preds = %entry
  br label %case0

case7:                                            ; preds = %entry
  br label %case0
}

declare i16 @_ssdm_op_HSub(...)

declare i16 @_ssdm_op_HMul(...)

declare i16 @_ssdm_op_HDiv(...)

declare i16 @_ssdm_op_HAdd(...)

declare i32 @_ssdm_op_BitConcatenate.i32.i31.i1(i31, i1) nounwind readnone

declare i32 @_ssdm_op_BitConcatenate.i32.i29.i3(i29, i3) nounwind readnone

declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [1 x i32]* @llvm_global_ctors_0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 0, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 31, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"y", metadata !5, metadata !"int", i32 0, i32 31}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 31, metadata !13}
!13 = metadata !{metadata !14}
!14 = metadata !{metadata !"x", metadata !15, metadata !"int", i32 0, i32 31}
!15 = metadata !{metadata !16}
!16 = metadata !{i32 0, i32 0, i32 0}
