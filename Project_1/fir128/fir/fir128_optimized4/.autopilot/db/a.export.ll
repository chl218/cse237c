; ModuleID = 'D:/Projects/vivado/Project_1/project1/fir128/fir/fir128_optimized4/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@shift_reg = internal unnamed_addr global [128 x i32] zeroinitializer, align 16
@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535]
@fir_str = internal unnamed_addr constant [4 x i8] c"fir\00"

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

define void @fir(i32* %y, i32 %x) nounwind uwtable {
.preheader:
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %y) nounwind, !map !7
  call void (...)* @_ssdm_op_SpecBitsMap(i32 %x) nounwind, !map !11
  call void (...)* @_ssdm_op_SpecTopModule([4 x i8]* @fir_str) nounwind
  %x_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %x) nounwind
  %shift_reg_load = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 126), align 8
  store i32 %shift_reg_load, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 127), align 4
  %shift_reg_load_1 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 125), align 4
  store i32 %shift_reg_load_1, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 126), align 8
  %shift_reg_load_2 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 124), align 16
  store i32 %shift_reg_load_2, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 125), align 4
  %shift_reg_load_3 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 123), align 4
  store i32 %shift_reg_load_3, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 124), align 16
  %shift_reg_load_4 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 122), align 8
  store i32 %shift_reg_load_4, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 123), align 4
  %shift_reg_load_5 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 121), align 4
  store i32 %shift_reg_load_5, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 122), align 8
  %shift_reg_load_6 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 120), align 16
  store i32 %shift_reg_load_6, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 121), align 4
  %shift_reg_load_7 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 119), align 4
  store i32 %shift_reg_load_7, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 120), align 16
  %shift_reg_load_8 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 118), align 8
  store i32 %shift_reg_load_8, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 119), align 4
  %shift_reg_load_9 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 117), align 4
  store i32 %shift_reg_load_9, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 118), align 8
  %shift_reg_load_10 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 116), align 16
  store i32 %shift_reg_load_10, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 117), align 4
  %shift_reg_load_11 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 115), align 4
  store i32 %shift_reg_load_11, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 116), align 16
  %shift_reg_load_12 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 114), align 8
  store i32 %shift_reg_load_12, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 115), align 4
  %shift_reg_load_13 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 113), align 4
  store i32 %shift_reg_load_13, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 114), align 8
  %shift_reg_load_14 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 112), align 16
  store i32 %shift_reg_load_14, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 113), align 4
  %shift_reg_load_15 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 111), align 4
  store i32 %shift_reg_load_15, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 112), align 16
  %shift_reg_load_16 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 110), align 8
  store i32 %shift_reg_load_16, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 111), align 4
  %shift_reg_load_17 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 109), align 4
  store i32 %shift_reg_load_17, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 110), align 8
  %shift_reg_load_18 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 108), align 16
  store i32 %shift_reg_load_18, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 109), align 4
  %shift_reg_load_19 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 107), align 4
  store i32 %shift_reg_load_19, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 108), align 16
  %shift_reg_load_20 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 106), align 8
  store i32 %shift_reg_load_20, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 107), align 4
  %shift_reg_load_21 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 105), align 4
  store i32 %shift_reg_load_21, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 106), align 8
  %shift_reg_load_22 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 104), align 16
  store i32 %shift_reg_load_22, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 105), align 4
  %tmp = add i32 %shift_reg_load_21, %shift_reg_load_20
  %tmp1 = add i32 %shift_reg_load_18, %shift_reg_load_16
  %tmp2 = add i32 %shift_reg_load_19, %tmp1
  %tmp3 = add i32 %tmp, %tmp2
  %tmp4 = add i32 %shift_reg_load_11, %shift_reg_load_12
  %tmp5 = add i32 %shift_reg_load_17, %tmp4
  %tmp6 = add i32 %shift_reg_load_14, %shift_reg_load_15
  %tmp7 = add i32 %shift_reg_load_13, %tmp6
  %tmp8 = add i32 %tmp5, %tmp7
  %tmp9 = add i32 %tmp3, %tmp8
  %tmp10 = add i32 %shift_reg_load_2, %shift_reg_load_1
  %tmp11 = add i32 %shift_reg_load, %tmp10
  %tmp12 = add i32 %shift_reg_load_5, %shift_reg_load_3
  %tmp13 = add i32 %shift_reg_load_4, %tmp12
  %tmp14 = add i32 %tmp11, %tmp13
  %tmp15 = add i32 %shift_reg_load_8, %shift_reg_load_6
  %tmp16 = add i32 %shift_reg_load_7, %tmp15
  %tmp17 = add i32 %shift_reg_load_22, %shift_reg_load_9
  %tmp18 = add i32 %shift_reg_load_10, %tmp17
  %tmp19 = add i32 %tmp16, %tmp18
  %tmp20 = add i32 %tmp14, %tmp19
  %tmp_6_s = add i32 %tmp9, %tmp20
  %tmp_21 = shl i32 %tmp_6_s, 3
  %tmp_22 = shl i32 %tmp_6_s, 1
  %shift_reg_load_23 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 103), align 4
  store i32 %shift_reg_load_23, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 104), align 16
  %tmp_6_1 = mul nsw i32 %shift_reg_load_23, 11
  %shift_reg_load_24 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 102), align 8
  store i32 %shift_reg_load_24, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 103), align 4
  %tmp_6_2 = mul nsw i32 %shift_reg_load_24, 11
  %shift_reg_load_25 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 101), align 4
  store i32 %shift_reg_load_25, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 102), align 8
  %tmp_23 = shl i32 %shift_reg_load_25, 3
  %shift_reg_load_26 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 100), align 16
  store i32 %shift_reg_load_26, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 101), align 4
  %tmp_24 = shl i32 %shift_reg_load_26, 2
  %tmp_6_4 = sub i32 %tmp_24, %shift_reg_load_26
  %shift_reg_load_27 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 99), align 4
  store i32 %shift_reg_load_27, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 100), align 16
  %tmp_25 = shl i32 %shift_reg_load_27, 2
  %p_neg40 = sub i32 0, %tmp_25
  %tmp_6_5 = add i32 %shift_reg_load_27, %p_neg40
  %tmp21 = add i32 %tmp_6_1, %tmp_21
  %tmp22 = add i32 %tmp_22, %tmp21
  %tmp23 = add i32 %tmp_23, %tmp_6_2
  %tmp24 = add i32 %tmp_6_5, %tmp_6_4
  %tmp25 = add i32 %tmp23, %tmp24
  %acc_1_5 = add nsw i32 %tmp22, %tmp25
  %shift_reg_load_28 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 98), align 8
  store i32 %shift_reg_load_28, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 99), align 4
  %tmp_26 = shl i32 %shift_reg_load_28, 3
  %acc_1_6 = sub i32 %acc_1_5, %tmp_26
  %shift_reg_load_29 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 97), align 4
  store i32 %shift_reg_load_29, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 98), align 8
  %tmp_6_6 = mul nsw i32 %shift_reg_load_29, -11
  %shift_reg_load_30 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 96), align 16
  store i32 %shift_reg_load_30, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 97), align 4
  %tmp_6_7 = mul nsw i32 %shift_reg_load_30, -11
  %shift_reg_load_31 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 95), align 4
  store i32 %shift_reg_load_31, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 96), align 16
  %tmp_27 = shl i32 %shift_reg_load_31, 3
  %p_neg2 = sub i32 0, %tmp_27
  %tmp_28 = shl i32 %shift_reg_load_31, 1
  %tmp_6_8 = sub i32 %p_neg2, %tmp_28
  %shift_reg_load_32 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 94), align 8
  store i32 %shift_reg_load_32, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 95), align 4
  %tmp_29 = shl i32 %shift_reg_load_32, 3
  %p_neg3 = sub i32 0, %tmp_29
  %tmp_30 = shl i32 %shift_reg_load_32, 1
  %tmp_6_9 = sub i32 %p_neg3, %tmp_30
  %shift_reg_load_33 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 93), align 4
  store i32 %shift_reg_load_33, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 94), align 8
  %tmp_31 = shl i32 %shift_reg_load_33, 3
  %p_neg4 = sub i32 0, %tmp_31
  %tmp_32 = shl i32 %shift_reg_load_33, 1
  %tmp_6_3 = sub i32 %p_neg4, %tmp_32
  %shift_reg_load_34 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 92), align 16
  store i32 %shift_reg_load_34, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 93), align 4
  %tmp_33 = shl i32 %shift_reg_load_34, 3
  %p_neg5 = sub i32 0, %tmp_33
  %tmp_34 = shl i32 %shift_reg_load_34, 1
  %tmp_6_10 = sub i32 %p_neg5, %tmp_34
  %shift_reg_load_35 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 91), align 4
  store i32 %shift_reg_load_35, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 92), align 16
  %tmp_35 = shl i32 %shift_reg_load_35, 3
  %p_neg6 = sub i32 0, %tmp_35
  %tmp_36 = shl i32 %shift_reg_load_35, 1
  %tmp_6_11 = sub i32 %p_neg6, %tmp_36
  %shift_reg_load_36 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 90), align 8
  store i32 %shift_reg_load_36, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 91), align 4
  %tmp_37 = shl i32 %shift_reg_load_36, 3
  %p_neg7 = sub i32 0, %tmp_37
  %tmp_38 = shl i32 %shift_reg_load_36, 1
  %tmp_6_12 = sub i32 %p_neg7, %tmp_38
  %shift_reg_load_37 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 89), align 4
  store i32 %shift_reg_load_37, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 90), align 8
  %tmp_39 = shl i32 %shift_reg_load_37, 3
  %p_neg8 = sub i32 0, %tmp_39
  %tmp_40 = shl i32 %shift_reg_load_37, 1
  %tmp_6_13 = sub i32 %p_neg8, %tmp_40
  %shift_reg_load_38 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 88), align 16
  store i32 %shift_reg_load_38, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 89), align 4
  %tmp_41 = shl i32 %shift_reg_load_38, 3
  %p_neg9 = sub i32 0, %tmp_41
  %tmp_42 = shl i32 %shift_reg_load_38, 1
  %tmp_6_14 = sub i32 %p_neg9, %tmp_42
  %shift_reg_load_39 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 87), align 4
  store i32 %shift_reg_load_39, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 88), align 16
  %shift_reg_load_40 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 86), align 8
  store i32 %shift_reg_load_40, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 87), align 4
  %tmp26 = add i32 %tmp_6_7, %acc_1_6
  %tmp27 = add i32 %tmp_6_6, %tmp26
  %tmp28 = add i32 %tmp_6_3, %tmp_6_9
  %tmp29 = add i32 %tmp_6_8, %tmp28
  %tmp30 = add i32 %tmp27, %tmp29
  %tmp31 = add i32 %tmp_6_12, %tmp_6_11
  %tmp32 = add i32 %tmp_6_10, %tmp31
  %tmp33 = add i32 %tmp_6_14, %tmp_6_13
  %tmp_6_15 = add i32 %shift_reg_load_40, %shift_reg_load_39
  %tmp34 = mul i32 %tmp_6_15, -11
  %tmp35 = add i32 %tmp33, %tmp34
  %tmp36 = add i32 %tmp32, %tmp35
  %acc_1_s = add nsw i32 %tmp30, %tmp36
  %shift_reg_load_41 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 85), align 4
  store i32 %shift_reg_load_41, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 86), align 8
  %tmp_43 = shl i32 %shift_reg_load_41, 3
  %acc_1_1 = sub i32 %acc_1_s, %tmp_43
  %shift_reg_load_42 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 84), align 16
  store i32 %shift_reg_load_42, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 85), align 4
  %tmp_44 = shl i32 %shift_reg_load_42, 2
  %p_neg41 = sub i32 0, %tmp_44
  %tmp_6_16 = add i32 %shift_reg_load_42, %p_neg41
  %shift_reg_load_43 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 83), align 4
  store i32 %shift_reg_load_43, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 84), align 16
  %tmp_45 = shl i32 %shift_reg_load_43, 2
  %tmp_6_17 = sub i32 %tmp_45, %shift_reg_load_43
  %shift_reg_load_44 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 82), align 8
  store i32 %shift_reg_load_44, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 83), align 4
  %tmp_46 = shl i32 %shift_reg_load_44, 3
  %shift_reg_load_45 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 81), align 4
  store i32 %shift_reg_load_45, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 82), align 8
  %tmp_6_18 = mul nsw i32 %shift_reg_load_45, 11
  %shift_reg_load_46 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 80), align 16
  store i32 %shift_reg_load_46, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 81), align 4
  %tmp_6_19 = mul nsw i32 %shift_reg_load_46, 11
  %shift_reg_load_47 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 79), align 4
  store i32 %shift_reg_load_47, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 80), align 16
  %tmp_47 = shl i32 %shift_reg_load_47, 3
  %tmp_48 = shl i32 %shift_reg_load_47, 1
  %shift_reg_load_48 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 78), align 8
  store i32 %shift_reg_load_48, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 79), align 4
  %tmp_49 = shl i32 %shift_reg_load_48, 3
  %tmp_50 = shl i32 %shift_reg_load_48, 1
  %shift_reg_load_49 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 77), align 4
  store i32 %shift_reg_load_49, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 78), align 8
  %tmp_51 = shl i32 %shift_reg_load_49, 3
  %tmp_52 = shl i32 %shift_reg_load_49, 1
  %shift_reg_load_50 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 76), align 16
  store i32 %shift_reg_load_50, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 77), align 4
  %tmp_53 = shl i32 %shift_reg_load_50, 3
  %tmp_54 = shl i32 %shift_reg_load_50, 1
  %shift_reg_load_51 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 75), align 4
  store i32 %shift_reg_load_51, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 76), align 16
  %tmp_55 = shl i32 %shift_reg_load_51, 3
  %tmp_56 = shl i32 %shift_reg_load_51, 1
  %shift_reg_load_52 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 74), align 8
  store i32 %shift_reg_load_52, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 75), align 4
  %tmp_57 = shl i32 %shift_reg_load_52, 3
  %tmp_58 = shl i32 %shift_reg_load_52, 1
  %shift_reg_load_53 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 73), align 4
  store i32 %shift_reg_load_53, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 74), align 8
  %tmp_59 = shl i32 %shift_reg_load_53, 3
  %tmp_60 = shl i32 %shift_reg_load_53, 1
  %shift_reg_load_54 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 72), align 16
  store i32 %shift_reg_load_54, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 73), align 4
  %tmp_61 = shl i32 %shift_reg_load_54, 3
  %tmp_62 = shl i32 %shift_reg_load_54, 1
  %shift_reg_load_55 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 71), align 4
  store i32 %shift_reg_load_55, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 72), align 16
  %tmp_6_20 = mul nsw i32 %shift_reg_load_55, 11
  %shift_reg_load_56 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 70), align 8
  store i32 %shift_reg_load_56, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 71), align 4
  %tmp_6_21 = mul nsw i32 %shift_reg_load_56, 11
  %shift_reg_load_57 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 69), align 4
  store i32 %shift_reg_load_57, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 70), align 8
  %tmp_63 = shl i32 %shift_reg_load_57, 3
  %shift_reg_load_58 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 68), align 16
  store i32 %shift_reg_load_58, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 69), align 4
  %tmp_64 = shl i32 %shift_reg_load_58, 2
  %tmp_6_22 = sub i32 %tmp_64, %shift_reg_load_58
  %shift_reg_load_59 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 67), align 4
  store i32 %shift_reg_load_59, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 68), align 16
  %tmp_65 = shl i32 %shift_reg_load_59, 2
  %p_neg42 = sub i32 0, %tmp_65
  %tmp_6_23 = add i32 %shift_reg_load_59, %p_neg42
  %tmp37 = add i32 %acc_1_1, %tmp_61
  %tmp38 = add i32 %tmp_62, %tmp37
  %tmp39 = add i32 %tmp_46, %tmp_6_17
  %tmp40 = add i32 %tmp_6_16, %tmp39
  %tmp41 = add i32 %tmp38, %tmp40
  %tmp42 = add i32 %tmp_47, %tmp_6_19
  %tmp43 = add i32 %tmp_6_18, %tmp42
  %tmp44 = add i32 %tmp_49, %tmp_48
  %tmp45 = add i32 %tmp_51, %tmp_50
  %tmp46 = add i32 %tmp44, %tmp45
  %tmp47 = add i32 %tmp43, %tmp46
  %tmp48 = add i32 %tmp41, %tmp47
  %tmp49 = add i32 %tmp_54, %tmp_53
  %tmp50 = add i32 %tmp_52, %tmp49
  %tmp51 = add i32 %tmp_56, %tmp_55
  %tmp52 = add i32 %tmp_58, %tmp_57
  %tmp53 = add i32 %tmp51, %tmp52
  %tmp54 = add i32 %tmp50, %tmp53
  %tmp55 = add i32 %tmp_6_20, %tmp_60
  %tmp56 = add i32 %tmp_59, %tmp55
  %tmp57 = add i32 %tmp_63, %tmp_6_21
  %tmp58 = add i32 %tmp_6_23, %tmp_6_22
  %tmp59 = add i32 %tmp57, %tmp58
  %tmp60 = add i32 %tmp56, %tmp59
  %tmp61 = add i32 %tmp54, %tmp60
  %acc_1_2 = add nsw i32 %tmp48, %tmp61
  %shift_reg_load_60 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 66), align 8
  store i32 %shift_reg_load_60, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 67), align 4
  %tmp_66 = shl i32 %shift_reg_load_60, 3
  %acc_1_3 = sub i32 %acc_1_2, %tmp_66
  %shift_reg_load_61 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 65), align 4
  store i32 %shift_reg_load_61, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 66), align 8
  %tmp_6_24 = mul nsw i32 %shift_reg_load_61, -11
  %shift_reg_load_62 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 64), align 16
  store i32 %shift_reg_load_62, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 65), align 4
  %tmp_6_25 = mul nsw i32 %shift_reg_load_62, -11
  %shift_reg_load_63 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 63), align 4
  store i32 %shift_reg_load_63, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 64), align 16
  %tmp_67 = shl i32 %shift_reg_load_63, 3
  %p_neg = sub i32 0, %tmp_67
  %tmp_68 = shl i32 %shift_reg_load_63, 1
  %tmp_6_26 = sub i32 %p_neg, %tmp_68
  %shift_reg_load_64 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 62), align 8
  store i32 %shift_reg_load_64, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 63), align 4
  %tmp_69 = shl i32 %shift_reg_load_64, 3
  %p_neg1 = sub i32 0, %tmp_69
  %tmp_70 = shl i32 %shift_reg_load_64, 1
  %tmp_6_27 = sub i32 %p_neg1, %tmp_70
  %shift_reg_load_65 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 61), align 4
  store i32 %shift_reg_load_65, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 62), align 8
  %tmp_71 = shl i32 %shift_reg_load_65, 3
  %p_neg10 = sub i32 0, %tmp_71
  %tmp_72 = shl i32 %shift_reg_load_65, 1
  %tmp_6_28 = sub i32 %p_neg10, %tmp_72
  %shift_reg_load_66 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 60), align 16
  store i32 %shift_reg_load_66, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 61), align 4
  %tmp_73 = shl i32 %shift_reg_load_66, 3
  %p_neg11 = sub i32 0, %tmp_73
  %tmp_74 = shl i32 %shift_reg_load_66, 1
  %tmp_6_29 = sub i32 %p_neg11, %tmp_74
  %shift_reg_load_67 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 59), align 4
  store i32 %shift_reg_load_67, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 60), align 16
  %tmp_75 = shl i32 %shift_reg_load_67, 3
  %p_neg12 = sub i32 0, %tmp_75
  %tmp_76 = shl i32 %shift_reg_load_67, 1
  %tmp_6_30 = sub i32 %p_neg12, %tmp_76
  %shift_reg_load_68 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 58), align 8
  store i32 %shift_reg_load_68, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 59), align 4
  %tmp_77 = shl i32 %shift_reg_load_68, 3
  %p_neg13 = sub i32 0, %tmp_77
  %tmp_78 = shl i32 %shift_reg_load_68, 1
  %tmp_6_31 = sub i32 %p_neg13, %tmp_78
  %shift_reg_load_69 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 57), align 4
  store i32 %shift_reg_load_69, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 58), align 8
  %tmp_79 = shl i32 %shift_reg_load_69, 3
  %p_neg14 = sub i32 0, %tmp_79
  %tmp_80 = shl i32 %shift_reg_load_69, 1
  %tmp_6_32 = sub i32 %p_neg14, %tmp_80
  %shift_reg_load_70 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 56), align 16
  store i32 %shift_reg_load_70, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 57), align 4
  %tmp_81 = shl i32 %shift_reg_load_70, 3
  %p_neg15 = sub i32 0, %tmp_81
  %tmp_82 = shl i32 %shift_reg_load_70, 1
  %tmp_6_33 = sub i32 %p_neg15, %tmp_82
  %shift_reg_load_71 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 55), align 4
  store i32 %shift_reg_load_71, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 56), align 16
  %shift_reg_load_72 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 54), align 8
  store i32 %shift_reg_load_72, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 55), align 4
  %tmp62 = add i32 %tmp_6_25, %acc_1_3
  %tmp63 = add i32 %tmp_6_24, %tmp62
  %tmp64 = add i32 %tmp_6_28, %tmp_6_27
  %tmp65 = add i32 %tmp_6_26, %tmp64
  %tmp66 = add i32 %tmp63, %tmp65
  %tmp67 = add i32 %tmp_6_31, %tmp_6_30
  %tmp68 = add i32 %tmp_6_29, %tmp67
  %tmp69 = add i32 %tmp_6_33, %tmp_6_32
  %tmp_6_34 = add i32 %shift_reg_load_72, %shift_reg_load_71
  %tmp70 = mul i32 %tmp_6_34, -11
  %tmp71 = add i32 %tmp69, %tmp70
  %tmp72 = add i32 %tmp68, %tmp71
  %acc_1_4 = add nsw i32 %tmp66, %tmp72
  %shift_reg_load_73 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 53), align 4
  store i32 %shift_reg_load_73, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 54), align 8
  %tmp_83 = shl i32 %shift_reg_load_73, 3
  %acc_1_7 = sub i32 %acc_1_4, %tmp_83
  %shift_reg_load_74 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 52), align 16
  store i32 %shift_reg_load_74, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 53), align 4
  %tmp_84 = shl i32 %shift_reg_load_74, 2
  %p_neg43 = sub i32 0, %tmp_84
  %tmp_6_35 = add i32 %shift_reg_load_74, %p_neg43
  %shift_reg_load_75 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 51), align 4
  store i32 %shift_reg_load_75, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 52), align 16
  %tmp_85 = shl i32 %shift_reg_load_75, 2
  %tmp_6_36 = sub i32 %tmp_85, %shift_reg_load_75
  %shift_reg_load_76 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 50), align 8
  store i32 %shift_reg_load_76, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 51), align 4
  %tmp_86 = shl i32 %shift_reg_load_76, 3
  %shift_reg_load_77 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 49), align 4
  store i32 %shift_reg_load_77, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 50), align 8
  %tmp_6_37 = mul nsw i32 %shift_reg_load_77, 11
  %shift_reg_load_78 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 48), align 16
  store i32 %shift_reg_load_78, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 49), align 4
  %tmp_6_38 = mul nsw i32 %shift_reg_load_78, 11
  %shift_reg_load_79 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 47), align 4
  store i32 %shift_reg_load_79, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 48), align 16
  %tmp_87 = shl i32 %shift_reg_load_79, 3
  %tmp_88 = shl i32 %shift_reg_load_79, 1
  %shift_reg_load_80 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 46), align 8
  store i32 %shift_reg_load_80, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 47), align 4
  %tmp_89 = shl i32 %shift_reg_load_80, 3
  %tmp_90 = shl i32 %shift_reg_load_80, 1
  %shift_reg_load_81 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 45), align 4
  store i32 %shift_reg_load_81, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 46), align 8
  %tmp_91 = shl i32 %shift_reg_load_81, 3
  %tmp_92 = shl i32 %shift_reg_load_81, 1
  %shift_reg_load_82 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 44), align 16
  store i32 %shift_reg_load_82, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 45), align 4
  %tmp_93 = shl i32 %shift_reg_load_82, 3
  %tmp_94 = shl i32 %shift_reg_load_82, 1
  %shift_reg_load_83 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 43), align 4
  store i32 %shift_reg_load_83, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 44), align 16
  %tmp_95 = shl i32 %shift_reg_load_83, 3
  %tmp_96 = shl i32 %shift_reg_load_83, 1
  %shift_reg_load_84 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 42), align 8
  store i32 %shift_reg_load_84, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 43), align 4
  %tmp_97 = shl i32 %shift_reg_load_84, 3
  %tmp_98 = shl i32 %shift_reg_load_84, 1
  %shift_reg_load_85 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 41), align 4
  store i32 %shift_reg_load_85, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 42), align 8
  %tmp_99 = shl i32 %shift_reg_load_85, 3
  %tmp_100 = shl i32 %shift_reg_load_85, 1
  %shift_reg_load_86 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 40), align 16
  store i32 %shift_reg_load_86, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 41), align 4
  %tmp_101 = shl i32 %shift_reg_load_86, 3
  %tmp_102 = shl i32 %shift_reg_load_86, 1
  %shift_reg_load_87 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 39), align 4
  store i32 %shift_reg_load_87, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 40), align 16
  %tmp_6_39 = mul nsw i32 %shift_reg_load_87, 11
  %shift_reg_load_88 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 38), align 8
  store i32 %shift_reg_load_88, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 39), align 4
  %tmp_6_40 = mul nsw i32 %shift_reg_load_88, 11
  %shift_reg_load_89 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 37), align 4
  store i32 %shift_reg_load_89, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 38), align 8
  %tmp_103 = shl i32 %shift_reg_load_89, 3
  %shift_reg_load_90 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 36), align 16
  store i32 %shift_reg_load_90, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 37), align 4
  %tmp_104 = shl i32 %shift_reg_load_90, 2
  %tmp_6_41 = sub i32 %tmp_104, %shift_reg_load_90
  %shift_reg_load_91 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 35), align 4
  store i32 %shift_reg_load_91, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 36), align 16
  %tmp_105 = shl i32 %shift_reg_load_91, 2
  %p_neg44 = sub i32 0, %tmp_105
  %tmp_6_42 = add i32 %shift_reg_load_91, %p_neg44
  %tmp73 = add i32 %acc_1_7, %tmp_101
  %tmp74 = add i32 %tmp_102, %tmp73
  %tmp75 = add i32 %tmp_86, %tmp_6_36
  %tmp76 = add i32 %tmp_6_35, %tmp75
  %tmp77 = add i32 %tmp74, %tmp76
  %tmp78 = add i32 %tmp_87, %tmp_6_38
  %tmp79 = add i32 %tmp_6_37, %tmp78
  %tmp80 = add i32 %tmp_89, %tmp_88
  %tmp81 = add i32 %tmp_91, %tmp_90
  %tmp82 = add i32 %tmp80, %tmp81
  %tmp83 = add i32 %tmp79, %tmp82
  %tmp84 = add i32 %tmp77, %tmp83
  %tmp85 = add i32 %tmp_94, %tmp_93
  %tmp86 = add i32 %tmp_92, %tmp85
  %tmp87 = add i32 %tmp_96, %tmp_95
  %tmp88 = add i32 %tmp_98, %tmp_97
  %tmp89 = add i32 %tmp87, %tmp88
  %tmp90 = add i32 %tmp86, %tmp89
  %tmp91 = add i32 %tmp_6_39, %tmp_100
  %tmp92 = add i32 %tmp_99, %tmp91
  %tmp93 = add i32 %tmp_103, %tmp_6_40
  %tmp94 = add i32 %tmp_6_42, %tmp_6_41
  %tmp95 = add i32 %tmp93, %tmp94
  %tmp96 = add i32 %tmp92, %tmp95
  %tmp97 = add i32 %tmp90, %tmp96
  %acc_1_8 = add nsw i32 %tmp84, %tmp97
  %shift_reg_load_92 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 34), align 8
  store i32 %shift_reg_load_92, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 35), align 4
  %tmp_106 = shl i32 %shift_reg_load_92, 3
  %acc_1_9 = sub i32 %acc_1_8, %tmp_106
  %shift_reg_load_93 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 33), align 4
  store i32 %shift_reg_load_93, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 34), align 8
  %tmp_6_43 = mul nsw i32 %shift_reg_load_93, -11
  %shift_reg_load_94 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 32), align 16
  store i32 %shift_reg_load_94, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 33), align 4
  %tmp_6_44 = mul nsw i32 %shift_reg_load_94, -11
  %shift_reg_load_95 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 31), align 4
  store i32 %shift_reg_load_95, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 32), align 16
  %tmp_107 = shl i32 %shift_reg_load_95, 3
  %p_neg16 = sub i32 0, %tmp_107
  %tmp_108 = shl i32 %shift_reg_load_95, 1
  %tmp_6_45 = sub i32 %p_neg16, %tmp_108
  %shift_reg_load_96 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 30), align 8
  store i32 %shift_reg_load_96, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 31), align 4
  %tmp_109 = shl i32 %shift_reg_load_96, 3
  %p_neg17 = sub i32 0, %tmp_109
  %tmp_110 = shl i32 %shift_reg_load_96, 1
  %tmp_6_46 = sub i32 %p_neg17, %tmp_110
  %shift_reg_load_97 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 29), align 4
  store i32 %shift_reg_load_97, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 30), align 8
  %tmp_111 = shl i32 %shift_reg_load_97, 3
  %p_neg18 = sub i32 0, %tmp_111
  %tmp_112 = shl i32 %shift_reg_load_97, 1
  %tmp_6_47 = sub i32 %p_neg18, %tmp_112
  %shift_reg_load_98 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 28), align 16
  store i32 %shift_reg_load_98, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 29), align 4
  %tmp_113 = shl i32 %shift_reg_load_98, 3
  %p_neg19 = sub i32 0, %tmp_113
  %tmp_114 = shl i32 %shift_reg_load_98, 1
  %tmp_6_48 = sub i32 %p_neg19, %tmp_114
  %shift_reg_load_99 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 27), align 4
  store i32 %shift_reg_load_99, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 28), align 16
  %tmp_115 = shl i32 %shift_reg_load_99, 3
  %p_neg20 = sub i32 0, %tmp_115
  %tmp_116 = shl i32 %shift_reg_load_99, 1
  %tmp_6_49 = sub i32 %p_neg20, %tmp_116
  %shift_reg_load_100 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 26), align 8
  store i32 %shift_reg_load_100, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 27), align 4
  %tmp_117 = shl i32 %shift_reg_load_100, 3
  %p_neg21 = sub i32 0, %tmp_117
  %tmp_118 = shl i32 %shift_reg_load_100, 1
  %tmp_6_50 = sub i32 %p_neg21, %tmp_118
  %shift_reg_load_101 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 25), align 4
  store i32 %shift_reg_load_101, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 26), align 8
  %tmp_119 = shl i32 %shift_reg_load_101, 3
  %p_neg22 = sub i32 0, %tmp_119
  %tmp_120 = shl i32 %shift_reg_load_101, 1
  %tmp_6_51 = sub i32 %p_neg22, %tmp_120
  %shift_reg_load_102 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 24), align 16
  store i32 %shift_reg_load_102, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 25), align 4
  %tmp_121 = shl i32 %shift_reg_load_102, 3
  %p_neg23 = sub i32 0, %tmp_121
  %tmp_122 = shl i32 %shift_reg_load_102, 1
  %tmp_6_52 = sub i32 %p_neg23, %tmp_122
  %shift_reg_load_103 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 23), align 4
  store i32 %shift_reg_load_103, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 24), align 16
  %tmp_123 = shl i32 %shift_reg_load_103, 3
  %p_neg24 = sub i32 0, %tmp_123
  %tmp_124 = shl i32 %shift_reg_load_103, 1
  %tmp_6_53 = sub i32 %p_neg24, %tmp_124
  %shift_reg_load_104 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 22), align 8
  store i32 %shift_reg_load_104, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 23), align 4
  %tmp_125 = shl i32 %shift_reg_load_104, 3
  %p_neg25 = sub i32 0, %tmp_125
  %tmp_126 = shl i32 %shift_reg_load_104, 1
  %tmp_6_54 = sub i32 %p_neg25, %tmp_126
  %shift_reg_load_105 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 21), align 4
  store i32 %shift_reg_load_105, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 22), align 8
  %tmp_127 = shl i32 %shift_reg_load_105, 3
  %p_neg26 = sub i32 0, %tmp_127
  %tmp_128 = shl i32 %shift_reg_load_105, 1
  %tmp_6_55 = sub i32 %p_neg26, %tmp_128
  %shift_reg_load_106 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 20), align 16
  store i32 %shift_reg_load_106, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 21), align 4
  %tmp_129 = shl i32 %shift_reg_load_106, 3
  %p_neg27 = sub i32 0, %tmp_129
  %tmp_130 = shl i32 %shift_reg_load_106, 1
  %tmp_6_56 = sub i32 %p_neg27, %tmp_130
  %shift_reg_load_107 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 19), align 4
  store i32 %shift_reg_load_107, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 20), align 16
  %tmp_131 = shl i32 %shift_reg_load_107, 3
  %p_neg28 = sub i32 0, %tmp_131
  %tmp_132 = shl i32 %shift_reg_load_107, 1
  %tmp_6_57 = sub i32 %p_neg28, %tmp_132
  %shift_reg_load_108 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 18), align 8
  store i32 %shift_reg_load_108, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 19), align 4
  %tmp_133 = shl i32 %shift_reg_load_108, 3
  %p_neg29 = sub i32 0, %tmp_133
  %tmp_134 = shl i32 %shift_reg_load_108, 1
  %tmp_6_58 = sub i32 %p_neg29, %tmp_134
  %shift_reg_load_109 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 17), align 4
  store i32 %shift_reg_load_109, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 18), align 8
  %tmp_135 = shl i32 %shift_reg_load_109, 3
  %p_neg30 = sub i32 0, %tmp_135
  %tmp_136 = shl i32 %shift_reg_load_109, 1
  %tmp_6_59 = sub i32 %p_neg30, %tmp_136
  %shift_reg_load_110 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 16), align 16
  store i32 %shift_reg_load_110, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 17), align 4
  %tmp_137 = shl i32 %shift_reg_load_110, 3
  %p_neg31 = sub i32 0, %tmp_137
  %tmp_138 = shl i32 %shift_reg_load_110, 1
  %tmp_6_60 = sub i32 %p_neg31, %tmp_138
  %shift_reg_load_111 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 15), align 4
  store i32 %shift_reg_load_111, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 16), align 16
  %tmp_139 = shl i32 %shift_reg_load_111, 3
  %p_neg32 = sub i32 0, %tmp_139
  %tmp_140 = shl i32 %shift_reg_load_111, 1
  %tmp_6_61 = sub i32 %p_neg32, %tmp_140
  %shift_reg_load_112 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 14), align 8
  store i32 %shift_reg_load_112, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 15), align 4
  %tmp_141 = shl i32 %shift_reg_load_112, 3
  %p_neg33 = sub i32 0, %tmp_141
  %tmp_142 = shl i32 %shift_reg_load_112, 1
  %tmp_6_62 = sub i32 %p_neg33, %tmp_142
  %shift_reg_load_113 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 13), align 4
  store i32 %shift_reg_load_113, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 14), align 8
  %tmp_143 = shl i32 %shift_reg_load_113, 3
  %p_neg34 = sub i32 0, %tmp_143
  %tmp_144 = shl i32 %shift_reg_load_113, 1
  %tmp_6_63 = sub i32 %p_neg34, %tmp_144
  %shift_reg_load_114 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 12), align 16
  store i32 %shift_reg_load_114, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 13), align 4
  %tmp_145 = shl i32 %shift_reg_load_114, 3
  %p_neg35 = sub i32 0, %tmp_145
  %tmp_146 = shl i32 %shift_reg_load_114, 1
  %tmp_6_64 = sub i32 %p_neg35, %tmp_146
  %shift_reg_load_115 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 11), align 4
  store i32 %shift_reg_load_115, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 12), align 16
  %tmp_147 = shl i32 %shift_reg_load_115, 3
  %p_neg36 = sub i32 0, %tmp_147
  %tmp_148 = shl i32 %shift_reg_load_115, 1
  %tmp_6_65 = sub i32 %p_neg36, %tmp_148
  %shift_reg_load_116 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 10), align 8
  store i32 %shift_reg_load_116, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 11), align 4
  %tmp_149 = shl i32 %shift_reg_load_116, 3
  %p_neg37 = sub i32 0, %tmp_149
  %tmp_150 = shl i32 %shift_reg_load_116, 1
  %tmp_6_66 = sub i32 %p_neg37, %tmp_150
  %shift_reg_load_117 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 9), align 4
  store i32 %shift_reg_load_117, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 10), align 8
  %tmp_151 = shl i32 %shift_reg_load_117, 3
  %p_neg38 = sub i32 0, %tmp_151
  %tmp_152 = shl i32 %shift_reg_load_117, 1
  %tmp_6_67 = sub i32 %p_neg38, %tmp_152
  %shift_reg_load_118 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 8), align 16
  store i32 %shift_reg_load_118, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 9), align 4
  %tmp_153 = shl i32 %shift_reg_load_118, 3
  %p_neg39 = sub i32 0, %tmp_153
  %tmp_154 = shl i32 %shift_reg_load_118, 1
  %tmp_6_68 = sub i32 %p_neg39, %tmp_154
  %shift_reg_load_119 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 7), align 4
  store i32 %shift_reg_load_119, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 8), align 16
  %shift_reg_load_120 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 6), align 8
  store i32 %shift_reg_load_120, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 7), align 4
  %tmp98 = add i32 %tmp_6_44, %acc_1_9
  %tmp99 = add i32 %tmp_6_43, %tmp98
  %tmp100 = add i32 %tmp_6_46, %tmp_6_45
  %tmp101 = add i32 %tmp_6_48, %tmp_6_47
  %tmp102 = add i32 %tmp100, %tmp101
  %tmp103 = add i32 %tmp99, %tmp102
  %tmp104 = add i32 %tmp_6_51, %tmp_6_50
  %tmp105 = add i32 %tmp_6_49, %tmp104
  %tmp106 = add i32 %tmp_6_53, %tmp_6_52
  %tmp107 = add i32 %tmp_6_55, %tmp_6_54
  %tmp108 = add i32 %tmp106, %tmp107
  %tmp109 = add i32 %tmp105, %tmp108
  %tmp110 = add i32 %tmp103, %tmp109
  %tmp111 = add i32 %tmp_6_58, %tmp_6_57
  %tmp112 = add i32 %tmp_6_56, %tmp111
  %tmp113 = add i32 %tmp_6_60, %tmp_6_59
  %tmp114 = add i32 %tmp_6_62, %tmp_6_61
  %tmp115 = add i32 %tmp113, %tmp114
  %tmp116 = add i32 %tmp112, %tmp115
  %tmp117 = add i32 %tmp_6_64, %tmp_6_63
  %tmp118 = add i32 %tmp_6_66, %tmp_6_65
  %tmp119 = add i32 %tmp117, %tmp118
  %tmp120 = add i32 %tmp_6_68, %tmp_6_67
  %tmp_6_69 = add i32 %shift_reg_load_120, %shift_reg_load_119
  %tmp121 = mul i32 %tmp_6_69, -11
  %tmp122 = add i32 %tmp120, %tmp121
  %tmp123 = add i32 %tmp119, %tmp122
  %tmp124 = add i32 %tmp116, %tmp123
  %acc_1_10 = add nsw i32 %tmp110, %tmp124
  %shift_reg_load_121 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 5), align 4
  store i32 %shift_reg_load_121, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 6), align 8
  %tmp_155 = shl i32 %shift_reg_load_121, 3
  %acc_1_11 = sub i32 %acc_1_10, %tmp_155
  %shift_reg_load_122 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 4), align 16
  store i32 %shift_reg_load_122, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 5), align 4
  %tmp_156 = shl i32 %shift_reg_load_122, 2
  %p_neg45 = sub i32 0, %tmp_156
  %tmp_6_70 = add i32 %shift_reg_load_122, %p_neg45
  %shift_reg_load_123 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 3), align 4
  store i32 %shift_reg_load_123, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 4), align 16
  %tmp_157 = shl i32 %shift_reg_load_123, 2
  %tmp_6_71 = sub i32 %tmp_157, %shift_reg_load_123
  %shift_reg_load_124 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 2), align 8
  store i32 %shift_reg_load_124, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 3), align 4
  %tmp_158 = shl i32 %shift_reg_load_124, 3
  %shift_reg_load_125 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 1), align 4
  store i32 %shift_reg_load_125, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 2), align 8
  %shift_reg_load_126 = load i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 0), align 16
  store i32 %shift_reg_load_126, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 1), align 4
  store i32 %x_read, i32* getelementptr inbounds ([128 x i32]* @shift_reg, i64 0, i64 0), align 16
  %tmp_159 = shl i32 %x_read, 3
  %tmp_160 = shl i32 %x_read, 1
  %tmp125 = add i32 %tmp_159, %tmp_160
  %tmp126 = add i32 %tmp_6_70, %acc_1_11
  %tmp127 = add i32 %tmp125, %tmp126
  %tmp128 = add i32 %tmp_158, %tmp_6_71
  %tmp_6_72 = add i32 %shift_reg_load_126, %shift_reg_load_125
  %tmp129 = mul i32 %tmp_6_72, 11
  %tmp130 = add i32 %tmp128, %tmp129
  %acc_1_12 = add nsw i32 %tmp127, %tmp130
  call void @_ssdm_op_Write.ap_auto.i32P(i32* %y, i32 %acc_1_12) nounwind
  ret void
}

define weak void @_ssdm_op_Write.ap_auto.i32P(i32*, i32) {
entry:
  store i32 %1, i32* %0
  ret void
}

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_Read.ap_auto.i32(i32) {
entry:
  ret i32 %0
}

declare i31 @_ssdm_op_PartSelect.i31.i32.i32.i32(i32, i32, i32) nounwind readnone

declare i30 @_ssdm_op_PartSelect.i30.i32.i32.i32(i32, i32, i32) nounwind readnone

declare i29 @_ssdm_op_PartSelect.i29.i32.i32.i32(i32, i32, i32) nounwind readnone

declare i16 @_ssdm_op_HSub(...)

declare i16 @_ssdm_op_HMul(...)

declare i16 @_ssdm_op_HDiv(...)

declare i16 @_ssdm_op_HAdd(...)

declare i32 @_ssdm_op_BitConcatenate.i32.i31.i1(i31, i1) nounwind readnone

declare i32 @_ssdm_op_BitConcatenate.i32.i30.i2(i30, i2) nounwind readnone

declare i32 @_ssdm_op_BitConcatenate.i32.i29.i3(i29, i3) nounwind readnone

declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [1 x i32]* @llvm_global_ctors_0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 0, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 31, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"y", metadata !5, metadata !"int", i32 0, i32 31}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 31, metadata !13}
!13 = metadata !{metadata !14}
!14 = metadata !{metadata !"x", metadata !15, metadata !"int", i32 0, i32 31}
!15 = metadata !{metadata !16}
!16 = metadata !{i32 0, i32 0, i32 0}
