; ModuleID = 'D:/Projects/vivado/Project_1/project1/fir128/fir/fir128_optimized1/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@shift_reg_V = internal unnamed_addr global [128 x i8] zeroinitializer
@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535]
@fir_str = internal unnamed_addr constant [4 x i8] c"fir\00"
@c = internal unnamed_addr constant [128 x i5] [i5 10, i5 11, i5 11, i5 8, i5 3, i5 -3, i5 -8, i5 -11, i5 -11, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -11, i5 -11, i5 -8, i5 -3, i5 3, i5 8, i5 11, i5 11, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 11, i5 11, i5 8, i5 3, i5 -3, i5 -8, i5 -11, i5 -11, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -11, i5 -11, i5 -8, i5 -3, i5 3, i5 8, i5 11, i5 11, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 11, i5 11, i5 8, i5 3, i5 -3, i5 -8, i5 -11, i5 -11, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -10, i5 -11, i5 -11, i5 -8, i5 -3, i5 3, i5 8, i5 11, i5 11, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10, i5 10]
@p_str = private unnamed_addr constant [17 x i8] c"Shift_Accum_Loop\00", align 1

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

define void @fir(i32* %y, i32 %x) nounwind uwtable {
.preheader.preheader:
  call void (...)* @_ssdm_op_SpecBitsMap(i32* %y) nounwind, !map !7
  call void (...)* @_ssdm_op_SpecBitsMap(i32 %x) nounwind, !map !11
  call void (...)* @_ssdm_op_SpecTopModule([4 x i8]* @fir_str) nounwind
  %x_read = call i32 @_ssdm_op_Read.ap_auto.i32(i32 %x) nounwind
  %tmp = trunc i32 %x_read to i8
  %data_V = trunc i32 %x_read to i16
  br label %0

; <label>:0                                       ; preds = %4, %.preheader.preheader
  %p_1 = phi i16 [ 0, %.preheader.preheader ], [ %acc_V, %4 ]
  %t_V = phi i8 [ 127, %.preheader.preheader ], [ %i_V, %4 ]
  %tmp_5 = call i1 @_ssdm_op_BitSelect.i1.i8.i32(i8 %t_V, i32 7)
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 128, i64 128, i64 128) nounwind
  br i1 %tmp_5, label %5, label %1

; <label>:1                                       ; preds = %0
  call void (...)* @_ssdm_op_SpecLoopName([17 x i8]* @p_str) nounwind
  %tmp_1 = icmp eq i8 %t_V, 0
  br i1 %tmp_1, label %2, label %3

; <label>:2                                       ; preds = %1
  store i8 %tmp, i8* getelementptr inbounds ([128 x i8]* @shift_reg_V, i64 0, i64 0), align 16
  br label %4

; <label>:3                                       ; preds = %1
  %tmp_3 = zext i8 %t_V to i64
  %tmp_10 = trunc i8 %t_V to i7
  %r_V = add i7 -1, %tmp_10
  %tmp_6 = zext i7 %r_V to i64
  %shift_reg_V_addr = getelementptr [128 x i8]* @shift_reg_V, i64 0, i64 %tmp_6
  %shift_reg_V_load = load i8* %shift_reg_V_addr, align 1
  %shift_reg_V_addr_1 = getelementptr [128 x i8]* @shift_reg_V, i64 0, i64 %tmp_3
  store i8 %shift_reg_V_load, i8* %shift_reg_V_addr_1, align 1
  %data_V_1 = sext i8 %shift_reg_V_load to i16
  br label %4

; <label>:4                                       ; preds = %3, %2
  %p_2 = phi i16 [ %data_V, %2 ], [ %data_V_1, %3 ]
  %tmp_7 = zext i8 %t_V to i64
  %c_addr = getelementptr [128 x i5]* @c, i64 0, i64 %tmp_7
  %c_load = load i5* %c_addr, align 1
  %tmp_8 = sext i5 %c_load to i16
  %tmp_9 = mul i16 %p_2, %tmp_8
  %acc_V = add i16 %tmp_9, %p_1
  %i_V = add i8 %t_V, -1
  br label %0

; <label>:5                                       ; preds = %0
  %tmp_2 = sext i16 %p_1 to i32
  call void @_ssdm_op_Write.ap_auto.i32P(i32* %y, i32 %tmp_2) nounwind
  ret void
}

define weak void @_ssdm_op_Write.ap_auto.i32P(i32*, i32) {
entry:
  store i32 %1, i32* %0
  ret void
}

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecLoopName(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_Read.ap_auto.i32(i32) {
entry:
  ret i32 %0
}

declare i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32, i32, i32) nounwind readnone

declare i7 @_ssdm_op_PartSelect.i7.i8.i32.i32(i8, i32, i32) nounwind readnone

declare i16 @_ssdm_op_PartSelect.i16.i32.i32.i32(i32, i32, i32) nounwind readnone

declare i16 @_ssdm_op_HSub(...)

declare i16 @_ssdm_op_HMul(...)

declare i16 @_ssdm_op_HDiv(...)

declare i16 @_ssdm_op_HAdd(...)

define weak i1 @_ssdm_op_BitSelect.i1.i8.i32(i8, i32) nounwind readnone {
entry:
  %empty = trunc i32 %1 to i8
  %empty_2 = shl i8 1, %empty
  %empty_3 = and i8 %0, %empty_2
  %empty_4 = icmp ne i8 %empty_3, 0
  ret i1 %empty_4
}

declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [1 x i32]* @llvm_global_ctors_0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 0, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 31, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"y", metadata !5, metadata !"int", i32 0, i32 31}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 31, metadata !13}
!13 = metadata !{metadata !14}
!14 = metadata !{metadata !"x", metadata !15, metadata !"int", i32 0, i32 31}
!15 = metadata !{metadata !16}
!16 = metadata !{i32 0, i32 0, i32 0}
