############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2015 Xilinx Inc. All rights reserved.
############################################################
open_project image_segmentation
set_top image_segmentation
add_files segmentation.cpp
add_files segmentation.h
add_files -tb eventframe4.npy
add_files -tb segmentation_test.cpp
add_files -tb ../python/var.golden.dat
add_files -tb ../python/varb.golden.dat
open_solution "baseline"
set_part {xc7vx485tffg1761-2}
create_clock -period 10 -name default
#source "./image_segmentation/baseline/directives.tcl"
csim_design
csynth_design
cosim_design
export_design -format ip_catalog
