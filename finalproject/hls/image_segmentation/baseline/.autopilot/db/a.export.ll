; ModuleID = 'D:/Projects/vivado/finalproject/hls/image_segmentation/baseline/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535]
@image_segmentation_str = internal unnamed_addr constant [19 x i8] c"image_segmentation\00"

declare i62 @llvm.part.select.i62(i62, i32, i32) nounwind readnone

declare i32 @llvm.part.select.i32(i32, i32, i32) nounwind readnone

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

define void @image_segmentation([16384 x i16]* %frame, [16384 x i16]* %variance, [16384 x i16]* %threshold) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap([16384 x i16]* %frame) nounwind, !map !7
  call void (...)* @_ssdm_op_SpecBitsMap([16384 x i16]* %variance) nounwind, !map !13
  call void (...)* @_ssdm_op_SpecBitsMap([16384 x i16]* %threshold) nounwind, !map !17
  call void (...)* @_ssdm_op_SpecTopModule([19 x i8]* @image_segmentation_str) nounwind
  br label %1

; <label>:1                                       ; preds = %2, %0
  %i = phi i7 [ 2, %0 ], [ %i_1, %2 ]
  %exitcond3 = icmp eq i7 %i, -2
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 124, i64 124, i64 124) nounwind
  br i1 %exitcond3, label %.preheader4, label %.preheader5.preheader

.preheader5.preheader:                            ; preds = %1
  %tmp = call i14 @_ssdm_op_BitConcatenate.i14.i7.i7(i7 %i, i7 0)
  %tmp_1 = or i14 %tmp, 1
  %tmp_1_cast = zext i14 %tmp_1 to i15
  %tmp_2 = or i14 %tmp, 127
  %tmp_2_cast = zext i14 %tmp_2 to i15
  br label %.preheader5

.preheader5:                                      ; preds = %_ifconv, %.preheader5.preheader
  %j = phi i7 [ %j_1, %_ifconv ], [ 2, %.preheader5.preheader ]
  %j_cast1 = zext i7 %j to i15
  %j_cast2 = zext i7 %j to i9
  %j_cast = zext i7 %j to i8
  %exitcond2 = icmp eq i7 %j, -2
  %empty_6 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 124, i64 124, i64 124) nounwind
  br i1 %exitcond2, label %2, label %_ifconv

_ifconv:                                          ; preds = %.preheader5
  %tmp1 = add i9 -129, %j_cast2
  %tmp1_cast = sext i9 %tmp1 to i14
  %tmp_6 = add i14 %tmp, %tmp1_cast
  %tmp_7 = zext i14 %tmp_6 to i64
  %frame_addr = getelementptr [16384 x i16]* %frame, i64 0, i64 %tmp_7
  %frame_load = load i16* %frame_addr, align 2
  %tmp_8_cast = sext i16 %frame_load to i17
  %tmp2 = call i8 @_ssdm_op_BitConcatenate.i8.i1.i7(i1 true, i7 %j)
  %tmp2_cast = sext i8 %tmp2 to i14
  %tmp_s = add i14 %tmp, %tmp2_cast
  %tmp_4 = zext i14 %tmp_s to i64
  %frame_addr_1 = getelementptr [16384 x i16]* %frame, i64 0, i64 %tmp_4
  %frame_load_1 = load i16* %frame_addr_1, align 2
  %tmp_10_cast = sext i16 %frame_load_1 to i17
  %tmp3 = add i8 -127, %j_cast
  %tmp3_cast = sext i8 %tmp3 to i14
  %tmp_5 = add i14 %tmp, %tmp3_cast
  %tmp_8 = zext i14 %tmp_5 to i64
  %frame_addr_2 = getelementptr [16384 x i16]* %frame, i64 0, i64 %tmp_8
  %frame_load_2 = load i16* %frame_addr_2, align 2
  %tmp_14_cast = sext i16 %frame_load_2 to i17
  %tmp4 = add i7 -1, %j
  %tmp_9 = call i14 @_ssdm_op_BitConcatenate.i14.i7.i7(i7 %i, i7 %tmp4)
  %tmp_10 = zext i14 %tmp_9 to i64
  %frame_addr_3 = getelementptr [16384 x i16]* %frame, i64 0, i64 %tmp_10
  %frame_load_3 = load i16* %frame_addr_3, align 2
  %tmp_18_cast = sext i16 %frame_load_3 to i17
  %tmp_11 = call i14 @_ssdm_op_BitConcatenate.i14.i7.i7(i7 %i, i7 %j)
  %tmp_12 = zext i14 %tmp_11 to i64
  %frame_addr_4 = getelementptr [16384 x i16]* %frame, i64 0, i64 %tmp_12
  %frame_load_4 = load i16* %frame_addr_4, align 2
  %tmp_21_cast = sext i16 %frame_load_4 to i17
  %tmp_13 = add i15 %j_cast1, %tmp_1_cast
  %tmp_14 = zext i15 %tmp_13 to i64
  %frame_addr_5 = getelementptr [16384 x i16]* %frame, i64 0, i64 %tmp_14
  %frame_load_5 = load i16* %frame_addr_5, align 2
  %tmp_24_cast = sext i16 %frame_load_5 to i17
  %tmp_15 = add i15 %j_cast1, %tmp_2_cast
  %tmp_16 = zext i15 %tmp_15 to i64
  %frame_addr_6 = getelementptr [16384 x i16]* %frame, i64 0, i64 %tmp_16
  %frame_load_6 = load i16* %frame_addr_6, align 2
  %tmp_27_cast = sext i16 %frame_load_6 to i18
  %tmp5_cast = zext i8 %tmp2 to i14
  %tmp_17 = add i14 %tmp, %tmp5_cast
  %tmp_18 = zext i14 %tmp_17 to i64
  %frame_addr_7 = getelementptr [16384 x i16]* %frame, i64 0, i64 %tmp_18
  %frame_load_7 = load i16* %frame_addr_7, align 2
  %tmp_31_cast = sext i16 %frame_load_7 to i17
  %tmp6_cast = zext i8 %tmp3 to i14
  %tmp_19 = add i14 %tmp, %tmp6_cast
  %tmp_20 = zext i14 %tmp_19 to i64
  %frame_addr_8 = getelementptr [16384 x i16]* %frame, i64 0, i64 %tmp_20
  %frame_load_8 = load i16* %frame_addr_8, align 2
  %tmp_35_cast = sext i16 %frame_load_8 to i17
  %tmp8 = add i17 %tmp_10_cast, %tmp_8_cast
  %tmp8_cast = sext i17 %tmp8 to i18
  %tmp9 = add i17 %tmp_18_cast, %tmp_14_cast
  %tmp9_cast = sext i17 %tmp9 to i18
  %tmp7 = add i18 %tmp8_cast, %tmp9_cast
  %tmp7_cast = sext i18 %tmp7 to i20
  %tmp11 = add i17 %tmp_24_cast, %tmp_21_cast
  %tmp11_cast = sext i17 %tmp11 to i19
  %tmp13 = add i17 %tmp_35_cast, %tmp_31_cast
  %tmp13_cast = sext i17 %tmp13 to i18
  %tmp12 = add i18 %tmp_27_cast, %tmp13_cast
  %tmp12_cast = sext i18 %tmp12 to i19
  %tmp10 = add i19 %tmp11_cast, %tmp12_cast
  %tmp10_cast = sext i19 %tmp10 to i20
  %tmp_21 = add i20 %tmp7_cast, %tmp10_cast
  %mean1 = sext i20 %tmp_21 to i32
  %mean = sitofp i32 %mean1 to float
  %mean_1 = fdiv float %mean, 9.000000e+00
  %tmp_61 = sext i16 %frame_load to i32
  %tmp_22 = sitofp i32 %tmp_61 to float
  %tmp_23 = fsub float %tmp_22, %mean_1
  %tmp_24 = fmul float %tmp_23, %tmp_23
  %tmp_64 = sext i16 %frame_load_1 to i32
  %tmp_25 = sitofp i32 %tmp_64 to float
  %tmp_26 = fsub float %tmp_25, %mean_1
  %tmp_27 = fmul float %tmp_26, %tmp_26
  %tmp_28 = fadd float %tmp_24, %tmp_27
  %tmp_65 = sext i16 %frame_load_2 to i32
  %tmp_29 = sitofp i32 %tmp_65 to float
  %tmp_30 = fsub float %tmp_29, %mean_1
  %tmp_31 = fmul float %tmp_30, %tmp_30
  %tmp_32 = fadd float %tmp_28, %tmp_31
  %tmp_66 = sext i16 %frame_load_3 to i32
  %tmp_33 = sitofp i32 %tmp_66 to float
  %tmp_34 = fsub float %tmp_33, %mean_1
  %tmp_35 = fmul float %tmp_34, %tmp_34
  %tmp_36 = fadd float %tmp_32, %tmp_35
  %tmp_67 = sext i16 %frame_load_4 to i32
  %tmp_37 = sitofp i32 %tmp_67 to float
  %tmp_38 = fsub float %tmp_37, %mean_1
  %tmp_39 = fmul float %tmp_38, %tmp_38
  %tmp_40 = fadd float %tmp_36, %tmp_39
  %tmp_68 = sext i16 %frame_load_5 to i32
  %tmp_41 = sitofp i32 %tmp_68 to float
  %tmp_42 = fsub float %tmp_41, %mean_1
  %tmp_43 = fmul float %tmp_42, %tmp_42
  %tmp_44 = fadd float %tmp_40, %tmp_43
  %tmp_69 = sext i16 %frame_load_6 to i32
  %tmp_45 = sitofp i32 %tmp_69 to float
  %tmp_46 = fsub float %tmp_45, %mean_1
  %tmp_47 = fmul float %tmp_46, %tmp_46
  %tmp_48 = fadd float %tmp_44, %tmp_47
  %tmp_70 = sext i16 %frame_load_7 to i32
  %tmp_49 = sitofp i32 %tmp_70 to float
  %tmp_50 = fsub float %tmp_49, %mean_1
  %tmp_51 = fmul float %tmp_50, %tmp_50
  %tmp_52 = fadd float %tmp_48, %tmp_51
  %tmp_71 = sext i16 %frame_load_8 to i32
  %tmp_53 = sitofp i32 %tmp_71 to float
  %tmp_54 = fsub float %tmp_53, %mean_1
  %tmp_55 = fmul float %tmp_54, %tmp_54
  %vari = fadd float %tmp_52, %tmp_55
  %vari_1 = fdiv float %vari, 9.000000e+00
  %p_Val2_s = bitcast float %vari_1 to i32
  %p_Result_s = call i1 @_ssdm_op_BitSelect.i1.i32.i32(i32 %p_Val2_s, i32 31)
  %loc_V = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %p_Val2_s, i32 23, i32 30) nounwind
  %loc_V_1 = trunc i32 %p_Val2_s to i23
  %p_Result_1 = call i24 @_ssdm_op_BitConcatenate.i24.i1.i23(i1 true, i23 %loc_V_1) nounwind
  %tmp_i_i = zext i24 %p_Result_1 to i62
  %tmp_i_i_i_cast = zext i8 %loc_V to i9
  %sh_assign = add i9 -127, %tmp_i_i_i_cast
  %isNeg = call i1 @_ssdm_op_BitSelect.i1.i9.i32(i9 %sh_assign, i32 8)
  %tmp_83_i_i = sub i8 127, %loc_V
  %tmp_83_i_i_cast = sext i8 %tmp_83_i_i to i9
  %sh_assign_1 = select i1 %isNeg, i9 %tmp_83_i_i_cast, i9 %sh_assign
  %sh_assign_1_cast = sext i9 %sh_assign_1 to i32
  %sh_assign_1_cast_cast = sext i9 %sh_assign_1 to i24
  %tmp_84_i_i = zext i32 %sh_assign_1_cast to i62
  %tmp_85_i_i = lshr i24 %p_Result_1, %sh_assign_1_cast_cast
  %tmp_87_i_i = shl i62 %tmp_i_i, %tmp_84_i_i
  %tmp_75 = call i1 @_ssdm_op_BitSelect.i1.i24.i32(i24 %tmp_85_i_i, i32 23)
  %tmp_62 = zext i1 %tmp_75 to i16
  %tmp_63 = call i16 @_ssdm_op_PartSelect.i16.i62.i32.i32(i62 %tmp_87_i_i, i32 23, i32 38)
  %p_Val2_3 = select i1 %isNeg, i16 %tmp_62, i16 %tmp_63
  %p_Val2_7_i_i = sub i16 0, %p_Val2_3
  %p_Val2_5 = select i1 %p_Result_s, i16 %p_Val2_7_i_i, i16 %p_Val2_3
  %variance_addr = getelementptr [16384 x i16]* %variance, i64 0, i64 %tmp_12
  store i16 %p_Val2_5, i16* %variance_addr, align 2
  %j_1 = add i7 1, %j
  br label %.preheader5

; <label>:2                                       ; preds = %.preheader5
  %i_1 = add i7 %i, 1
  br label %1

.preheader4:                                      ; preds = %.preheader, %1
  %i1 = phi i8 [ 0, %1 ], [ %i_2, %.preheader ]
  %exitcond1 = icmp eq i8 %i1, -128
  %empty_7 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 128, i64 128, i64 128) nounwind
  %i_2 = add i8 %i1, 1
  br i1 %exitcond1, label %4, label %.preheader.preheader

.preheader.preheader:                             ; preds = %.preheader4
  %tmp_60 = trunc i8 %i1 to i7
  %tmp_3 = call i14 @_ssdm_op_BitConcatenate.i14.i7.i7(i7 %tmp_60, i7 0)
  br label %.preheader

.preheader:                                       ; preds = %3, %.preheader.preheader
  %j2 = phi i8 [ %j_2, %3 ], [ 0, %.preheader.preheader ]
  %j2_cast = zext i8 %j2 to i14
  %exitcond = icmp eq i8 %j2, -128
  %empty_8 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 128, i64 128, i64 128) nounwind
  %j_2 = add i8 %j2, 1
  br i1 %exitcond, label %.preheader4, label %3

; <label>:3                                       ; preds = %.preheader
  %tmp_56 = add i14 %tmp_3, %j2_cast
  %tmp_57 = zext i14 %tmp_56 to i64
  %variance_addr_1 = getelementptr [16384 x i16]* %variance, i64 0, i64 %tmp_57
  %variance_load = load i16* %variance_addr_1, align 2
  %tmp_58 = icmp sgt i16 %variance_load, 84
  %tmp_59 = zext i1 %tmp_58 to i16
  %threshold_addr = getelementptr [16384 x i16]* %threshold, i64 0, i64 %tmp_57
  store i16 %tmp_59, i16* %threshold_addr, align 2
  br label %.preheader

; <label>:4                                       ; preds = %.preheader4
  ret void
}

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32, i32, i32) nounwind readnone {
entry:
  %empty = call i32 @llvm.part.select.i32(i32 %0, i32 %1, i32 %2)
  %empty_9 = trunc i32 %empty to i8
  ret i8 %empty_9
}

declare i7 @_ssdm_op_PartSelect.i7.i8.i32.i32(i8, i32, i32) nounwind readnone

declare i23 @_ssdm_op_PartSelect.i23.i32.i32.i32(i32, i32, i32) nounwind readnone

define weak i16 @_ssdm_op_PartSelect.i16.i62.i32.i32(i62, i32, i32) nounwind readnone {
entry:
  %empty = call i62 @llvm.part.select.i62(i62 %0, i32 %1, i32 %2)
  %empty_10 = trunc i62 %empty to i16
  ret i16 %empty_10
}

declare i16 @_ssdm_op_HSub(...)

declare i16 @_ssdm_op_HMul(...)

declare i16 @_ssdm_op_HDiv(...)

declare i16 @_ssdm_op_HAdd(...)

define weak i1 @_ssdm_op_BitSelect.i1.i9.i32(i9, i32) nounwind readnone {
entry:
  %empty = trunc i32 %1 to i9
  %empty_11 = shl i9 1, %empty
  %empty_12 = and i9 %0, %empty_11
  %empty_13 = icmp ne i9 %empty_12, 0
  ret i1 %empty_13
}

define weak i1 @_ssdm_op_BitSelect.i1.i32.i32(i32, i32) nounwind readnone {
entry:
  %empty = shl i32 1, %1
  %empty_14 = and i32 %0, %empty
  %empty_15 = icmp ne i32 %empty_14, 0
  ret i1 %empty_15
}

define weak i1 @_ssdm_op_BitSelect.i1.i24.i32(i24, i32) nounwind readnone {
entry:
  %empty = trunc i32 %1 to i24
  %empty_16 = shl i24 1, %empty
  %empty_17 = and i24 %0, %empty_16
  %empty_18 = icmp ne i24 %empty_17, 0
  ret i1 %empty_18
}

define weak i8 @_ssdm_op_BitConcatenate.i8.i1.i7(i1, i7) nounwind readnone {
entry:
  %empty = zext i1 %0 to i8
  %empty_19 = zext i7 %1 to i8
  %empty_20 = shl i8 %empty, 7
  %empty_21 = or i8 %empty_20, %empty_19
  ret i8 %empty_21
}

define weak i24 @_ssdm_op_BitConcatenate.i24.i1.i23(i1, i23) nounwind readnone {
entry:
  %empty = zext i1 %0 to i24
  %empty_22 = zext i23 %1 to i24
  %empty_23 = shl i24 %empty, 23
  %empty_24 = or i24 %empty_23, %empty_22
  ret i24 %empty_24
}

define weak i14 @_ssdm_op_BitConcatenate.i14.i7.i7(i7, i7) nounwind readnone {
entry:
  %empty = zext i7 %0 to i14
  %empty_25 = zext i7 %1 to i14
  %empty_26 = shl i14 %empty, 7
  %empty_27 = or i14 %empty_26, %empty_25
  ret i14 %empty_27
}

declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [1 x i32]* @llvm_global_ctors_0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 0, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 15, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"frame", metadata !11, metadata !"short", i32 0, i32 15}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 16383, i32 1}
!13 = metadata !{metadata !14}
!14 = metadata !{i32 0, i32 15, metadata !15}
!15 = metadata !{metadata !16}
!16 = metadata !{metadata !"variance", metadata !11, metadata !"short", i32 0, i32 15}
!17 = metadata !{metadata !18}
!18 = metadata !{i32 0, i32 15, metadata !19}
!19 = metadata !{metadata !20}
!20 = metadata !{metadata !"threshold", metadata !11, metadata !"short", i32 0, i32 15}
