
set TopModule "image_segmentation"
set ClockPeriod "10.000000"
set ClockList {ap_clk}
set multiClockList {}
set PortClockMap {}
set CombLogicFlag 0
set PipelineFlag 0
set DataflowTaskPipelineFlag  1
set TrivialPipelineFlag 0
set noPortSwitchingFlag 0
set FloatingPointFlag 1
set FftOrFirFlag 0
set NbRWValue 0
set intNbAccess 0
set NewDSPMapping 1
set HasDSPModule 0
set ResetLevelFlag 1
set ResetStyle "control"
set ResetSyncFlag 1
set ResetRegisterFlag 0
set ResetVariableFlag 0
set fsmEncStyle "onehot"
set maxFanout "0"
set RtlPrefix ""
set ExtraCCFlags ""
set ExtraCLdFlags ""
set SynCheckOptions ""
set PresynOptions ""
set PreprocOptions ""
set SchedOptions ""
set BindOptions ""
set RtlGenOptions ""
set RtlWriterOptions ""
set CbcGenFlag ""
set CasGenFlag ""
set CasMonitorFlag ""
set AutoSimOptions {}
set ExportMCPathFlag "0"
set SCTraceFileName "mytrace"
set SCTraceFileFormat "vcd"
set SCTraceOption "all"
set TargetInfo "xc7vx485t:ffg1761:-2"
set SourceFiles {sc {} c ../../segmentation.cpp}
set SourceFlags {sc {} c {{}}}
set DirectiveFile {D:/Projects/vivado/finalproject/hls/image_segmentation/baseline/baseline.directive}
set TBFiles {verilog {../../../python/varb.golden.dat ../../../python/var.golden.dat ../../segmentation_test.cpp ../../eventframe4.npy} bc {../../../python/varb.golden.dat ../../../python/var.golden.dat ../../segmentation_test.cpp ../../eventframe4.npy} sc {../../../python/varb.golden.dat ../../../python/var.golden.dat ../../segmentation_test.cpp ../../eventframe4.npy} vhdl {../../../python/varb.golden.dat ../../../python/var.golden.dat ../../segmentation_test.cpp ../../eventframe4.npy} c {} cas {../../../python/varb.golden.dat ../../../python/var.golden.dat ../../segmentation_test.cpp ../../eventframe4.npy}}
set SpecLanguage "C"
set TVInFiles {bc {} c {} sc {} cas {} vhdl {} verilog {}}
set TVOutFiles {bc {} c {} sc {} cas {} vhdl {} verilog {}}
set TBTops {verilog {} bc {} sc {} vhdl {} c {} cas {}}
set TBInstNames {verilog {} bc {} sc {} vhdl {} c {} cas {}}
set ExtraGlobalOptions {"area_timing" 1 "clock_gate" 1 "impl_flow" map "power_gate" 0}
set PlatformFiles {{DefaultPlatform {xilinx/virtex7/virtex7 xilinx/virtex7/virtex7_fpv6}}}
set DefaultPlatform "DefaultPlatform"
set TBTVFileNotFound ""
set AppFile "../vivado_hls.app"
set ApsFile "baseline.aps"
set AvePath "../.."
set HPFPO "0"
