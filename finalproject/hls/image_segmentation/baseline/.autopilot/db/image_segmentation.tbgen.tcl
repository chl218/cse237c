set C_TypeInfoList {{ 
"image_segmentation" : [[], { "return": [[], "void"]} , [{"ExternC" : 0}], [ {"frame": [[], {"array": ["0", [16384]]}] }, {"variance": [[], {"array": ["0", [16384]]}] }, {"threshold": [[], {"array": ["0", [16384]]}] }],[],""], 
"0": [ "data_t", {"typedef": [[[],"1"],""]}], 
"1": [ "int16_t", {"typedef": [[[], {"scalar": "short"}],""]}]
}}
set moduleName image_segmentation
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {image_segmentation}
set C_modelType { void 0 }
set C_modelArgList { 
	{ frame int 16 regular {array 16384 { 1 1 } 1 1 }  }
	{ variance int 16 regular {array 16384 { 2 3 } 1 1 }  }
	{ threshold int 16 regular {array 16384 { 0 3 } 0 1 }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "frame", "interface" : "memory", "bitwidth" : 16, "direction" : "READONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "frame","cData": "short","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 16383,"step" : 1}]}]}]} , 
 	{ "Name" : "variance", "interface" : "memory", "bitwidth" : 16, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "variance","cData": "short","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 16383,"step" : 1}]}]}]} , 
 	{ "Name" : "threshold", "interface" : "memory", "bitwidth" : 16, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":15,"cElement": [{"cName": "threshold","cData": "short","bit_use": { "low": 0,"up": 15},"cArray": [{"low" : 0,"up" : 16383,"step" : 1}]}]}]} ]}
# RTL Port declarations: 
set portNum 21
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ frame_address0 sc_out sc_lv 14 signal 0 } 
	{ frame_ce0 sc_out sc_logic 1 signal 0 } 
	{ frame_q0 sc_in sc_lv 16 signal 0 } 
	{ frame_address1 sc_out sc_lv 14 signal 0 } 
	{ frame_ce1 sc_out sc_logic 1 signal 0 } 
	{ frame_q1 sc_in sc_lv 16 signal 0 } 
	{ variance_address0 sc_out sc_lv 14 signal 1 } 
	{ variance_ce0 sc_out sc_logic 1 signal 1 } 
	{ variance_we0 sc_out sc_logic 1 signal 1 } 
	{ variance_d0 sc_out sc_lv 16 signal 1 } 
	{ variance_q0 sc_in sc_lv 16 signal 1 } 
	{ threshold_address0 sc_out sc_lv 14 signal 2 } 
	{ threshold_ce0 sc_out sc_logic 1 signal 2 } 
	{ threshold_we0 sc_out sc_logic 1 signal 2 } 
	{ threshold_d0 sc_out sc_lv 16 signal 2 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "frame_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":14, "type": "signal", "bundle":{"name": "frame", "role": "address0" }} , 
 	{ "name": "frame_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "frame", "role": "ce0" }} , 
 	{ "name": "frame_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "frame", "role": "q0" }} , 
 	{ "name": "frame_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":14, "type": "signal", "bundle":{"name": "frame", "role": "address1" }} , 
 	{ "name": "frame_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "frame", "role": "ce1" }} , 
 	{ "name": "frame_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "frame", "role": "q1" }} , 
 	{ "name": "variance_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":14, "type": "signal", "bundle":{"name": "variance", "role": "address0" }} , 
 	{ "name": "variance_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "variance", "role": "ce0" }} , 
 	{ "name": "variance_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "variance", "role": "we0" }} , 
 	{ "name": "variance_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "variance", "role": "d0" }} , 
 	{ "name": "variance_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "variance", "role": "q0" }} , 
 	{ "name": "threshold_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":14, "type": "signal", "bundle":{"name": "threshold", "role": "address0" }} , 
 	{ "name": "threshold_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "threshold", "role": "ce0" }} , 
 	{ "name": "threshold_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "threshold", "role": "we0" }} , 
 	{ "name": "threshold_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":16, "type": "signal", "bundle":{"name": "threshold", "role": "d0" }}  ]}
set Spec2ImplPortList { 
	frame { ap_memory {  { frame_address0 mem_address 1 14 }  { frame_ce0 mem_ce 1 1 }  { frame_q0 mem_dout 0 16 }  { frame_address1 mem_address 1 14 }  { frame_ce1 mem_ce 1 1 }  { frame_q1 mem_dout 0 16 } } }
	variance { ap_memory {  { variance_address0 mem_address 1 14 }  { variance_ce0 mem_ce 1 1 }  { variance_we0 mem_we 1 1 }  { variance_d0 mem_din 1 16 }  { variance_q0 mem_dout 0 16 } } }
	threshold { ap_memory {  { threshold_address0 mem_address 1 14 }  { threshold_ce0 mem_ce 1 1 }  { threshold_we0 mem_we 1 1 }  { threshold_d0 mem_din 1 16 } } }
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
