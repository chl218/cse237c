
#include "segmentation.h"

// the top function of image segmentation for one frame analysis
void image_segmentation(data_t frame[SIZE], data_t variance[SIZE], data_t threshold[SIZE]){
	float mean = 0.0;
	float vari = 0.0;

	// Step 1: Generating mean-filtered and variance images
	// 				variance = sum (x-mean)^2 / N
	for(int i = 2; i < M-2; i++) {
		for(int j = 2; j < N-2; j++) {
			mean = frame[(i-1)*N+(j-1)] + frame[(i-1)*N+j] + frame[(i-1)*N+(j+1)]
				 + frame[ i   *N+(j-1)] + frame[ i   *N+j] + frame[ i   *N+(j+1)]
				 + frame[(i+1)*N+(j-1)] + frame[(i+1)*N+j] + frame[(i+1)*N+(j+1)];
			mean /= 9;

			vari = (frame[(i-1)*N+(j-1)]-mean)*(frame[(i-1)*N+(j-1)]-mean)
				 + (frame[(i-1)*N+ j]   -mean)*(frame[(i-1)*N+ j]   -mean)
				 + (frame[(i-1)*N+(j+1)]-mean)*(frame[(i-1)*N+(j+1)]-mean)
				 + (frame[ i   *N+(j-1)]-mean)*(frame[ i   *N+(j-1)]-mean)
				 + (frame[ i   *N+ j]   -mean)*(frame[ i   *N+ j]   -mean)
				 + (frame[ i   *N+(j+1)]-mean)*(frame[ i   *N+(j+1)]-mean)
				 + (frame[(i+1)*N+(j-1)]-mean)*(frame[(i+1)*N+(j-1)]-mean)
				 + (frame[(i+1)*N+ j]   -mean)*(frame[(i+1)*N+ j]   -mean)
				 + (frame[(i+1)*N+(j+1)]-mean)*(frame[(i+1)*N+(j+1)]-mean);
			vari /= 9;
			variance[i*N+j] = vari;

//			printf("window:\n");
//			printf("%3d %3d %3d\n", frame[(i-1)*N+(j-1)] , frame[(i-1)*N+j] , frame[(i-1)*N+(j+1)]);
//			printf("%3d %3d %3d\n", frame[ i   *N+(j-1)] , frame[ i   *N+j] , frame[ i   *N+(j+1)]);
//			printf("%3d %3d %3d\n", frame[(i+1)*N+(j-1)] , frame[(i+1)*N+j] , frame[(i+1)*N+(j+1)]);
//			printf("%3d %3d\nmean:%5f\nvariance:%5f\n",i, j, (float)mean, (float)variance[i*N+j]);

		}
	}

	// Step 2: Thresholding a variance image
	for(int i = 0; i < M; i++) {
		for(int j = 0; j < N; j++) {
			threshold[i*N+j] = variance[i*N+j] > 50*1.28+20;
		}
	}

}
