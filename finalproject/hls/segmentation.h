
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

//#include "ap_fixed.h"
//#include "ap_int.h"


#ifdef BIT_ACCURATE

typedef ap_int<16>    data_t;

#else

typedef int16_t       data_t;

#endif

#define TRUE 		1
#define FALSE 		0
#define M 			128
#define N 			128
#define F 			18
#define SIZE 		M*N
#define DATALENTH M*N*F

// the top function of image segmentation for one frame analysis
void image_segmentation(data_t frame[SIZE], data_t variance[SIZE], data_t threshold[SIZE]);
