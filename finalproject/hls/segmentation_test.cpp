
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#include "segmentation.h"

const int FRAME_COUNT = 18;



struct Rmse {
    int 	num_sq;
    float 	sum_sq;
    float 	error;

    Rmse(){
    	num_sq = 0;
    	sum_sq = 0;
    	error = 0;
    }

    float add_value(float d_n) {
        num_sq++;
        sum_sq += (d_n*d_n);
        error = sqrtf(sum_sq / num_sq);
        return error;
    }
};

Rmse rmse_v,  rmse_t;


void read_frame(char *infilename, int fnum, int16_t * framestream) {
    
    FILE *fin = fopen(infilename,"r");
    if ( fin == NULL ) {
        printf("%s\n", infilename);
        perror("Fail to open input image file");
        exit(1);
    }

    fseek(fin, sizeof(int16_t)*48 + sizeof(int16_t)*SIZE*fnum, SEEK_SET);
    fread(framestream, sizeof(int16_t)*SIZE, 1, fin);
    fclose(fin);
}


int main(int argc, const char * argv[]) {

	char * infilename;
    
    int16_t frame[SIZE]		= {0};
    int16_t variance[SIZE]  = {0};
    int16_t threshold[SIZE] = {0};
    

    infilename    = "eventframe4.npy";

    int v_gold  = 0;
    int t_gold = 0;

    FILE * fp1 = fopen("var.golden.dat", "r");
    FILE * fp2 = fopen("varb.golden.dat","r");
    if (fp1 == NULL || fp2 == NULL) {
        perror("Fail to open golden solution files\n");
        exit(1);
    }

    for(int fnum = 0; fnum < FRAME_COUNT; fnum++) {

    	read_frame(infilename, fnum, frame);

    	image_segmentation(frame,  variance, threshold);

//    	printf("frame: %d\n", fnum);
//    	for(int ii = 0; ii < M; ii++) {
//    		for(int jj = 0; jj < N; jj++) {
//    			printf("%3d ", variance[ii*N+jj]);
//    		}
//    		printf("\n");
//    	}

        for (int i = 0; i < SIZE; i++) {
        	fscanf(fp1, "%d", &v_gold);
        	fscanf(fp2, "%d", &t_gold);
        	rmse_v.add_value((float)(variance[i]  - (int16_t)v_gold));
        	rmse_t.add_value((float)(threshold[i] - (int16_t)t_gold));
        }

    }

   fclose(fp1);
   fclose(fp2);



   printf("----------------------------------------------\n");
   printf("   RMSE(V)           RMSE(T)\n");
   printf("%0.15f %0.15f\n", rmse_v.error, rmse_t.error);
   printf("----------------------------------------------\n");
   float _threshold = 1.0;
   if (rmse_v.error > _threshold || rmse_t.error > _threshold ) {
	   fprintf(stdout, "*************************************************\n");
	   fprintf(stdout, "* FAIL: Output DOES NOT match the golden output *\n");
	   fprintf(stdout, "*************************************************\n");
	   return 1;
   }
   else {
	   fprintf(stdout, "***********************************************\n");
	   fprintf(stdout, "* PASS: The output matches the golden output! *\n");
	   fprintf(stdout, "***********************************************\n");
	   return 0;
   }
}



