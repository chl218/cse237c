; ModuleID = 'D:/Projects/vivado/project_3/dft_32_precomputed/dft/solution1/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@sct = internal unnamed_addr constant [32 x float] [float 0.000000e+00, float 0xBFC8F8B580000000, float 0xBFD87DE0E0000000, float 0xBFE1C73AC0000000, float 0xBFE6A09EE0000000, float 0xBFEA9B6700000000, float 0xBFED906CC0000000, float 0xBFEF629740000000, float -1.000000e+00, float 0xBFEF629740000000, float 0xBFED906CC0000000, float 0xBFEA9B6700000000, float 0xBFE6A09EE0000000, float 0xBFE1C73AC0000000, float 0xBFD87DE0E0000000, float 0xBFC8F8B580000000, float -0.000000e+00, float 0x3FC8F8B580000000, float 0x3FD87DE0E0000000, float 0x3FE1C73AC0000000, float 0x3FE6A09EE0000000, float 0x3FEA9B6700000000, float 0x3FED906CC0000000, float 0x3FEF629740000000, float 1.000000e+00, float 0x3FEF629740000000, float 0x3FED906CC0000000, float 0x3FEA9B6700000000, float 0x3FE6A09EE0000000, float 0x3FE1C73AC0000000, float 0x3FD87DE0E0000000, float 0x3FC8F8B580000000], align 16
@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535]
@dft_str = internal unnamed_addr constant [4 x i8] c"dft\00"
@cct = internal unnamed_addr constant [32 x float] [float 1.000000e+00, float 0x3FEF629740000000, float 0x3FED906CC0000000, float 0x3FEA9B6700000000, float 0x3FE6A09EE0000000, float 0x3FE1C73AC0000000, float 0x3FD87DE0E0000000, float 0x3FC8F8B580000000, float 0.000000e+00, float 0xBFC8F8B580000000, float 0xBFD87DE0E0000000, float 0xBFE1C73AC0000000, float 0xBFE6A09EE0000000, float 0xBFEA9B6700000000, float 0xBFED906CC0000000, float 0xBFEF629740000000, float -1.000000e+00, float 0xBFEF629740000000, float 0xBFED906CC0000000, float 0xBFEA9B6700000000, float 0xBFE6A09EE0000000, float 0xBFE1C73AC0000000, float 0xBFD87DE0E0000000, float 0xBFC8F8B580000000, float -0.000000e+00, float 0x3FC8F8B580000000, float 0x3FD87DE0E0000000, float 0x3FE1C73AC0000000, float 0x3FE6A09EE0000000, float 0x3FEA9B6700000000, float 0x3FED906CC0000000, float 0x3FEF629740000000], align 16

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

define void @dft([32 x float]* %real_i, [32 x float]* %imag_i) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap([32 x float]* %real_i) nounwind, !map !7
  call void (...)* @_ssdm_op_SpecBitsMap([32 x float]* %imag_i) nounwind, !map !13
  call void (...)* @_ssdm_op_SpecTopModule([4 x i8]* @dft_str) nounwind
  %tmp_real = alloca [32 x float], align 16
  %tmp_imag = alloca [32 x float], align 16
  br label %.loopexit

.loopexit:                                        ; preds = %2, %0
  %i = phi i6 [ 0, %0 ], [ %i_2, %2 ]
  %tmp_1 = trunc i6 %i to i5
  %exitcond2 = icmp eq i6 %i, -32
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 32, i64 32, i64 32) nounwind
  %i_2 = add i6 1, %i
  br i1 %exitcond2, label %.preheader, label %1

; <label>:1                                       ; preds = %.loopexit
  %tmp = zext i6 %i to i64
  %tmp_real_addr = getelementptr inbounds [32 x float]* %tmp_real, i64 0, i64 %tmp
  store float 0.000000e+00, float* %tmp_real_addr, align 4
  %tmp_imag_addr = getelementptr inbounds [32 x float]* %tmp_imag, i64 0, i64 %tmp
  br label %2

; <label>:2                                       ; preds = %3, %1
  %storemerge = phi float [ 0.000000e+00, %1 ], [ %tmp_12, %3 ]
  %tmp_2 = phi float [ 0.000000e+00, %1 ], [ %tmp_5, %3 ]
  %j = phi i6 [ 0, %1 ], [ %j_1, %3 ]
  store float %storemerge, float* %tmp_imag_addr, align 4
  %tmp_13 = trunc i6 %j to i5
  %exitcond1 = icmp eq i6 %j, -32
  %empty_3 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 32, i64 32, i64 32) nounwind
  %j_1 = add i6 1, %j
  br i1 %exitcond1, label %.loopexit, label %3

; <label>:3                                       ; preds = %2
  %index = mul i5 %tmp_1, %tmp_13
  %tmp_7 = zext i6 %j to i64
  %real_i_addr_1 = getelementptr [32 x float]* %real_i, i64 0, i64 %tmp_7
  %real_i_load = load float* %real_i_addr_1, align 4
  %tmp_8 = zext i5 %index to i64
  %cct_addr = getelementptr inbounds [32 x float]* @cct, i64 0, i64 %tmp_8
  %cct_load = load float* %cct_addr, align 4
  %tmp_9 = fmul float %real_i_load, %cct_load
  %imag_i_addr_1 = getelementptr [32 x float]* %imag_i, i64 0, i64 %tmp_7
  %imag_i_load = load float* %imag_i_addr_1, align 4
  %sct_addr = getelementptr inbounds [32 x float]* @sct, i64 0, i64 %tmp_8
  %sct_load = load float* %sct_addr, align 4
  %tmp_s = fmul float %imag_i_load, %sct_load
  %tmp_4 = fsub float %tmp_9, %tmp_s
  %tmp_5 = fadd float %tmp_2, %tmp_4
  store float %tmp_5, float* %tmp_real_addr, align 4
  %tmp_6 = fmul float %real_i_load, %sct_load
  %tmp_10 = fmul float %imag_i_load, %cct_load
  %tmp_11 = fadd float %tmp_6, %tmp_10
  %tmp_12 = fadd float %storemerge, %tmp_11
  br label %2

.preheader:                                       ; preds = %.loopexit, %4
  %i1 = phi i6 [ %i_1, %4 ], [ 0, %.loopexit ]
  %exitcond = icmp eq i6 %i1, -32
  %empty_4 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 32, i64 32, i64 32) nounwind
  %i_1 = add i6 %i1, 1
  br i1 %exitcond, label %5, label %4

; <label>:4                                       ; preds = %.preheader
  %tmp_3 = zext i6 %i1 to i64
  %tmp_real_addr_1 = getelementptr inbounds [32 x float]* %tmp_real, i64 0, i64 %tmp_3
  %tmp_real_load = load float* %tmp_real_addr_1, align 4
  %real_i_addr = getelementptr [32 x float]* %real_i, i64 0, i64 %tmp_3
  store float %tmp_real_load, float* %real_i_addr, align 4
  %tmp_imag_addr_1 = getelementptr inbounds [32 x float]* %tmp_imag, i64 0, i64 %tmp_3
  %tmp_imag_load = load float* %tmp_imag_addr_1, align 4
  %imag_i_addr = getelementptr [32 x float]* %imag_i, i64 0, i64 %tmp_3
  store float %tmp_imag_load, float* %imag_i_addr, align 4
  br label %.preheader

; <label>:5                                       ; preds = %.preheader
  ret void
}

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

declare i5 @_ssdm_op_PartSelect.i5.i6.i32.i32(i6, i32, i32) nounwind readnone

declare i16 @_ssdm_op_HSub(...)

declare i16 @_ssdm_op_HMul(...)

declare i16 @_ssdm_op_HDiv(...)

declare i16 @_ssdm_op_HAdd(...)

declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [1 x i32]* @llvm_global_ctors_0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 0, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 31, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"real_i", metadata !11, metadata !"float", i32 0, i32 31}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 31, i32 1}
!13 = metadata !{metadata !14}
!14 = metadata !{i32 0, i32 31, metadata !15}
!15 = metadata !{metadata !16}
!16 = metadata !{metadata !"imag_i", metadata !11, metadata !"float", i32 0, i32 31}
