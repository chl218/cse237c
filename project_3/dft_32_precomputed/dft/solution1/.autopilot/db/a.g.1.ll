; ModuleID = 'D:/Projects/vivado/project_3/dft_32_precomputed/dft/solution1/.autopilot/db/a.g.1.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@sct = internal unnamed_addr constant [32 x float] [float 0.000000e+00, float 0xBFC8F8B580000000, float 0xBFD87DE0E0000000, float 0xBFE1C73AC0000000, float 0xBFE6A09EE0000000, float 0xBFEA9B6700000000, float 0xBFED906CC0000000, float 0xBFEF629740000000, float -1.000000e+00, float 0xBFEF629740000000, float 0xBFED906CC0000000, float 0xBFEA9B6700000000, float 0xBFE6A09EE0000000, float 0xBFE1C73AC0000000, float 0xBFD87DE0E0000000, float 0xBFC8F8B580000000, float -0.000000e+00, float 0x3FC8F8B580000000, float 0x3FD87DE0E0000000, float 0x3FE1C73AC0000000, float 0x3FE6A09EE0000000, float 0x3FEA9B6700000000, float 0x3FED906CC0000000, float 0x3FEF629740000000, float 1.000000e+00, float 0x3FEF629740000000, float 0x3FED906CC0000000, float 0x3FEA9B6700000000, float 0x3FE6A09EE0000000, float 0x3FE1C73AC0000000, float 0x3FD87DE0E0000000, float 0x3FC8F8B580000000], align 16 ; [#uses=1 type=[32 x float]*]
@llvm.global_ctors = appending global [1 x { i32, void ()* }] [{ i32, void ()* } { i32 65535, void ()* @_GLOBAL__I_a }] ; [#uses=0 type=[1 x { i32, void ()* }]*]
@dft.str = internal unnamed_addr constant [4 x i8] c"dft\00" ; [#uses=1 type=[4 x i8]*]
@cct = internal unnamed_addr constant [32 x float] [float 1.000000e+00, float 0x3FEF629740000000, float 0x3FED906CC0000000, float 0x3FEA9B6700000000, float 0x3FE6A09EE0000000, float 0x3FE1C73AC0000000, float 0x3FD87DE0E0000000, float 0x3FC8F8B580000000, float 0.000000e+00, float 0xBFC8F8B580000000, float 0xBFD87DE0E0000000, float 0xBFE1C73AC0000000, float 0xBFE6A09EE0000000, float 0xBFEA9B6700000000, float 0xBFED906CC0000000, float 0xBFEF629740000000, float -1.000000e+00, float 0xBFEF629740000000, float 0xBFED906CC0000000, float 0xBFEA9B6700000000, float 0xBFE6A09EE0000000, float 0xBFE1C73AC0000000, float 0xBFD87DE0E0000000, float 0xBFC8F8B580000000, float -0.000000e+00, float 0x3FC8F8B580000000, float 0x3FD87DE0E0000000, float 0x3FE1C73AC0000000, float 0x3FE6A09EE0000000, float 0x3FEA9B6700000000, float 0x3FED906CC0000000, float 0x3FEF629740000000], align 16 ; [#uses=1 type=[32 x float]*]

; [#uses=6]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=2]
declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

; [#uses=0]
define void @dft(float* %real_i, float* %imag_i) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecTopModule([4 x i8]* @dft.str) nounwind
  %tmp_real = alloca [32 x float], align 16       ; [#uses=2 type=[32 x float]*]
  %tmp_imag = alloca [32 x float], align 16       ; [#uses=2 type=[32 x float]*]
  call void @llvm.dbg.value(metadata !{float* %real_i}, i64 0, metadata !32), !dbg !33 ; [debug line = 4:16] [debug variable = real_i]
  call void @llvm.dbg.value(metadata !{float* %imag_i}, i64 0, metadata !34), !dbg !35 ; [debug line = 4:52] [debug variable = imag_i]
  call void (...)* @_ssdm_SpecArrayDimSize(float* %real_i, i32 32) nounwind, !dbg !36 ; [debug line = 5:2]
  call void (...)* @_ssdm_SpecArrayDimSize(float* %imag_i, i32 32) nounwind, !dbg !38 ; [debug line = 5:36]
  call void @llvm.dbg.declare(metadata !{[32 x float]* %tmp_real}, metadata !39), !dbg !41 ; [debug line = 7:8] [debug variable = tmp_real]
  call void @llvm.dbg.declare(metadata !{[32 x float]* %tmp_imag}, metadata !42), !dbg !43 ; [debug line = 8:8] [debug variable = tmp_imag]
  br label %1, !dbg !44                           ; [debug line = 10:15]

; <label>:1                                       ; preds = %5, %0
  %i = phi i32 [ 0, %0 ], [ %i.2, %5 ]            ; [#uses=4 type=i32]
  %exitcond2 = icmp eq i32 %i, 32, !dbg !44       ; [#uses=1 type=i1] [debug line = 10:15]
  br i1 %exitcond2, label %.preheader.preheader, label %2, !dbg !44 ; [debug line = 10:15]

.preheader.preheader:                             ; preds = %1
  br label %.preheader, !dbg !46                  ; [debug line = 21:15]

; <label>:2                                       ; preds = %1
  %tmp = sext i32 %i to i64, !dbg !48             ; [#uses=2 type=i64] [debug line = 11:3]
  %tmp_real.addr = getelementptr inbounds [32 x float]* %tmp_real, i64 0, i64 %tmp, !dbg !48 ; [#uses=2 type=float*] [debug line = 11:3]
  store float 0.000000e+00, float* %tmp_real.addr, align 4, !dbg !48 ; [debug line = 11:3]
  %tmp_imag.addr = getelementptr inbounds [32 x float]* %tmp_imag, i64 0, i64 %tmp, !dbg !50 ; [#uses=2 type=float*] [debug line = 12:3]
  store float 0.000000e+00, float* %tmp_imag.addr, align 4, !dbg !50 ; [debug line = 12:3]
  br label %3, !dbg !51                           ; [debug line = 14:16]

; <label>:3                                       ; preds = %4, %2
  %tmp.1 = phi float [ 0.000000e+00, %2 ], [ %tmp.16, %4 ] ; [#uses=2 type=float]
  %tmp.2 = phi float [ 0.000000e+00, %2 ], [ %tmp.12, %4 ] ; [#uses=2 type=float]
  %j = phi i32 [ 0, %2 ], [ %j.1, %4 ]            ; [#uses=4 type=i32]
  %exitcond1 = icmp eq i32 %j, 32, !dbg !51       ; [#uses=1 type=i1] [debug line = 14:16]
  br i1 %exitcond1, label %5, label %4, !dbg !51  ; [debug line = 14:16]

; <label>:4                                       ; preds = %3
  %tmp.6 = mul nsw i32 %j, %i, !dbg !53           ; [#uses=1 type=i32] [debug line = 15:42]
  %index = srem i32 %tmp.6, 32, !dbg !53          ; [#uses=1 type=i32] [debug line = 15:42]
  call void @llvm.dbg.value(metadata !{i32 %index}, i64 0, metadata !55), !dbg !53 ; [debug line = 15:42] [debug variable = index]
  %tmp.7 = sext i32 %j to i64, !dbg !57           ; [#uses=2 type=i64] [debug line = 16:4]
  %real_i.addr.1 = getelementptr inbounds float* %real_i, i64 %tmp.7, !dbg !57 ; [#uses=1 type=float*] [debug line = 16:4]
  %real_i.load = load float* %real_i.addr.1, align 4, !dbg !57 ; [#uses=4 type=float] [debug line = 16:4]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %real_i.load) nounwind
  %tmp.8 = sext i32 %index to i64, !dbg !57       ; [#uses=2 type=i64] [debug line = 16:4]
  %cct.addr = getelementptr inbounds [32 x float]* @cct, i64 0, i64 %tmp.8, !dbg !57 ; [#uses=1 type=float*] [debug line = 16:4]
  %cct.load = load float* %cct.addr, align 4, !dbg !57 ; [#uses=4 type=float] [debug line = 16:4]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %cct.load) nounwind
  %tmp.9 = fmul float %real_i.load, %cct.load, !dbg !57 ; [#uses=1 type=float] [debug line = 16:4]
  %imag_i.addr.1 = getelementptr inbounds float* %imag_i, i64 %tmp.7, !dbg !57 ; [#uses=1 type=float*] [debug line = 16:4]
  %imag_i.load = load float* %imag_i.addr.1, align 4, !dbg !57 ; [#uses=4 type=float] [debug line = 16:4]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %imag_i.load) nounwind
  %sct.addr = getelementptr inbounds [32 x float]* @sct, i64 0, i64 %tmp.8, !dbg !57 ; [#uses=1 type=float*] [debug line = 16:4]
  %sct.load = load float* %sct.addr, align 4, !dbg !57 ; [#uses=4 type=float] [debug line = 16:4]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %sct.load) nounwind
  %tmp.10 = fmul float %imag_i.load, %sct.load, !dbg !57 ; [#uses=1 type=float] [debug line = 16:4]
  %tmp.11 = fsub float %tmp.9, %tmp.10, !dbg !57  ; [#uses=1 type=float] [debug line = 16:4]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %tmp.2) nounwind
  %tmp.12 = fadd float %tmp.2, %tmp.11, !dbg !57  ; [#uses=2 type=float] [debug line = 16:4]
  store float %tmp.12, float* %tmp_real.addr, align 4, !dbg !57 ; [debug line = 16:4]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %real_i.load) nounwind
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %sct.load) nounwind
  %tmp.13 = fmul float %real_i.load, %sct.load, !dbg !58 ; [#uses=1 type=float] [debug line = 17:4]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %imag_i.load) nounwind
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %cct.load) nounwind
  %tmp.14 = fmul float %imag_i.load, %cct.load, !dbg !58 ; [#uses=1 type=float] [debug line = 17:4]
  %tmp.15 = fadd float %tmp.13, %tmp.14, !dbg !58 ; [#uses=1 type=float] [debug line = 17:4]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %tmp.1) nounwind
  %tmp.16 = fadd float %tmp.1, %tmp.15, !dbg !58  ; [#uses=2 type=float] [debug line = 17:4]
  store float %tmp.16, float* %tmp_imag.addr, align 4, !dbg !58 ; [debug line = 17:4]
  %j.1 = add nsw i32 %j, 1, !dbg !59              ; [#uses=1 type=i32] [debug line = 14:44]
  call void @llvm.dbg.value(metadata !{i32 %j.1}, i64 0, metadata !60), !dbg !59 ; [debug line = 14:44] [debug variable = j]
  br label %3, !dbg !59                           ; [debug line = 14:44]

; <label>:5                                       ; preds = %3
  %i.2 = add nsw i32 %i, 1, !dbg !61              ; [#uses=1 type=i32] [debug line = 10:43]
  call void @llvm.dbg.value(metadata !{i32 %i.2}, i64 0, metadata !62), !dbg !61 ; [debug line = 10:43] [debug variable = i]
  br label %1, !dbg !61                           ; [debug line = 10:43]

.preheader:                                       ; preds = %6, %.preheader.preheader
  %i1 = phi i32 [ %i.1, %6 ], [ 0, %.preheader.preheader ] ; [#uses=3 type=i32]
  %exitcond = icmp eq i32 %i1, 32, !dbg !46       ; [#uses=1 type=i1] [debug line = 21:15]
  br i1 %exitcond, label %7, label %6, !dbg !46   ; [debug line = 21:15]

; <label>:6                                       ; preds = %.preheader
  %tmp.3 = sext i32 %i1 to i64, !dbg !63          ; [#uses=4 type=i64] [debug line = 22:3]
  %tmp_real.addr.1 = getelementptr inbounds [32 x float]* %tmp_real, i64 0, i64 %tmp.3, !dbg !63 ; [#uses=1 type=float*] [debug line = 22:3]
  %tmp_real.load = load float* %tmp_real.addr.1, align 4, !dbg !63 ; [#uses=2 type=float] [debug line = 22:3]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %tmp_real.load) nounwind
  %real_i.addr = getelementptr inbounds float* %real_i, i64 %tmp.3, !dbg !63 ; [#uses=1 type=float*] [debug line = 22:3]
  store float %tmp_real.load, float* %real_i.addr, align 4, !dbg !63 ; [debug line = 22:3]
  %tmp_imag.addr.1 = getelementptr inbounds [32 x float]* %tmp_imag, i64 0, i64 %tmp.3, !dbg !65 ; [#uses=1 type=float*] [debug line = 23:3]
  %tmp_imag.load = load float* %tmp_imag.addr.1, align 4, !dbg !65 ; [#uses=2 type=float] [debug line = 23:3]
  call void (...)* @_ssdm_SpecKeepArrayLoad(float %tmp_imag.load) nounwind
  %imag_i.addr = getelementptr inbounds float* %imag_i, i64 %tmp.3, !dbg !65 ; [#uses=1 type=float*] [debug line = 23:3]
  store float %tmp_imag.load, float* %imag_i.addr, align 4, !dbg !65 ; [debug line = 23:3]
  %i.1 = add nsw i32 %i1, 1, !dbg !66             ; [#uses=1 type=i32] [debug line = 21:43]
  call void @llvm.dbg.value(metadata !{i32 %i.1}, i64 0, metadata !67), !dbg !66 ; [debug line = 21:43] [debug variable = i]
  br label %.preheader, !dbg !66                  ; [debug line = 21:43]

; <label>:7                                       ; preds = %.preheader
  ret void, !dbg !68                              ; [debug line = 26:1]
}

; [#uses=1]
declare void @_ssdm_op_SpecTopModule(...)

; [#uses=12]
declare void @_ssdm_SpecKeepArrayLoad(...)

; [#uses=2]
declare void @_ssdm_SpecArrayDimSize(...) nounwind

; [#uses=1]
declare void @_GLOBAL__I_a() nounwind

!llvm.dbg.cu = !{!0}
!hls.encrypted.func = !{}

!0 = metadata !{i32 786449, i32 0, i32 4, metadata !"D:/Projects/vivado/project_3/dft_32_precomputed/dft/solution1/.autopilot/db/dft.pragma.2.cpp", metadata !"d:/Projects/vivado/project_3/dft_32_precomputed", metadata !"clang version 3.1 ", i1 true, i1 false, metadata !"", i32 0, metadata !1, metadata !1, metadata !3, metadata !22} ; [ DW_TAG_compile_unit ]
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !5, metadata !14, metadata !21}
!5 = metadata !{i32 786478, i32 0, metadata !6, metadata !"dft", metadata !"dft", metadata !"_Z3dftPfS_", metadata !6, i32 4, metadata !7, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (float*, float*)* @dft, null, null, metadata !12, i32 5} ; [ DW_TAG_subprogram ]
!6 = metadata !{i32 786473, metadata !"dft.cpp", metadata !"d:/Projects/vivado/project_3/dft_32_precomputed", null} ; [ DW_TAG_file_type ]
!7 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !8, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!8 = metadata !{null, metadata !9, metadata !9}
!9 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !10} ; [ DW_TAG_pointer_type ]
!10 = metadata !{i32 786454, null, metadata !"DTYPE", metadata !6, i32 2, i64 0, i64 0, i64 0, i32 0, metadata !11} ; [ DW_TAG_typedef ]
!11 = metadata !{i32 786468, null, metadata !"float", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 4} ; [ DW_TAG_base_type ]
!12 = metadata !{metadata !13}
!13 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!14 = metadata !{i32 786478, i32 0, null, metadata !"ssdm_global_array_dftpp0cppaplinecpp", metadata !"ssdm_global_array_dftpp0cppaplinecpp", metadata !"_ZN36ssdm_global_array_dftpp0cppaplinecppC1Ev", metadata !6, i32 30, metadata !15, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !20, metadata !12, i32 30} ; [ DW_TAG_subprogram ]
!15 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !16, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!16 = metadata !{null, metadata !17}
!17 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !18} ; [ DW_TAG_pointer_type ]
!18 = metadata !{i32 786434, null, metadata !"ssdm_global_array_dftpp0cppaplinecpp", metadata !6, i32 28, i64 8, i64 8, i32 0, i32 0, null, metadata !19, i32 0, null, null} ; [ DW_TAG_class_type ]
!19 = metadata !{metadata !20}
!20 = metadata !{i32 786478, i32 0, metadata !18, metadata !"ssdm_global_array_dftpp0cppaplinecpp", metadata !"ssdm_global_array_dftpp0cppaplinecpp", metadata !"", metadata !6, i32 30, metadata !15, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !12, i32 30} ; [ DW_TAG_subprogram ]
!21 = metadata !{i32 786478, i32 0, null, metadata !"ssdm_global_array_dftpp0cppaplinecpp", metadata !"ssdm_global_array_dftpp0cppaplinecpp", metadata !"_ZN36ssdm_global_array_dftpp0cppaplinecppC2Ev", metadata !6, i32 30, metadata !15, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, metadata !20, metadata !12, i32 30} ; [ DW_TAG_subprogram ]
!22 = metadata !{metadata !23}
!23 = metadata !{metadata !24, metadata !25, metadata !31}
!24 = metadata !{i32 786484, i32 0, null, metadata !"ssdm_global_array_ins", metadata !"ssdm_global_array_ins", metadata !"_ZL21ssdm_global_array_ins", metadata !6, i32 35, metadata !18, i32 1, i32 1, null} ; [ DW_TAG_variable ]
!25 = metadata !{i32 786484, i32 0, null, metadata !"sct", metadata !"sct", metadata !"_ZL3sct", metadata !26, i32 3, metadata !27, i32 1, i32 1, [32 x float]* @sct} ; [ DW_TAG_variable ]
!26 = metadata !{i32 786473, metadata !"./coefficients32.h", metadata !"d:/Projects/vivado/project_3/dft_32_precomputed", null} ; [ DW_TAG_file_type ]
!27 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 1024, i64 32, i32 0, i32 0, metadata !28, metadata !29, i32 0, i32 0} ; [ DW_TAG_array_type ]
!28 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !10} ; [ DW_TAG_const_type ]
!29 = metadata !{metadata !30}
!30 = metadata !{i32 786465, i64 0, i64 31}       ; [ DW_TAG_subrange_type ]
!31 = metadata !{i32 786484, i32 0, null, metadata !"cct", metadata !"cct", metadata !"_ZL3cct", metadata !26, i32 1, metadata !27, i32 1, i32 1, [32 x float]* @cct} ; [ DW_TAG_variable ]
!32 = metadata !{i32 786689, metadata !5, metadata !"real_i", metadata !6, i32 16777220, metadata !9, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!33 = metadata !{i32 4, i32 16, metadata !5, null}
!34 = metadata !{i32 786689, metadata !5, metadata !"imag_i", metadata !6, i32 33554436, metadata !9, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!35 = metadata !{i32 4, i32 52, metadata !5, null}
!36 = metadata !{i32 5, i32 2, metadata !37, null}
!37 = metadata !{i32 786443, metadata !5, i32 5, i32 1, metadata !6, i32 0} ; [ DW_TAG_lexical_block ]
!38 = metadata !{i32 5, i32 36, metadata !37, null}
!39 = metadata !{i32 786688, metadata !37, metadata !"tmp_real", metadata !6, i32 7, metadata !40, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!40 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 1024, i64 32, i32 0, i32 0, metadata !10, metadata !29, i32 0, i32 0} ; [ DW_TAG_array_type ]
!41 = metadata !{i32 7, i32 8, metadata !37, null}
!42 = metadata !{i32 786688, metadata !37, metadata !"tmp_imag", metadata !6, i32 8, metadata !40, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!43 = metadata !{i32 8, i32 8, metadata !37, null}
!44 = metadata !{i32 10, i32 15, metadata !45, null}
!45 = metadata !{i32 786443, metadata !37, i32 10, i32 2, metadata !6, i32 1} ; [ DW_TAG_lexical_block ]
!46 = metadata !{i32 21, i32 15, metadata !47, null}
!47 = metadata !{i32 786443, metadata !37, i32 21, i32 2, metadata !6, i32 5} ; [ DW_TAG_lexical_block ]
!48 = metadata !{i32 11, i32 3, metadata !49, null}
!49 = metadata !{i32 786443, metadata !45, i32 10, i32 48, metadata !6, i32 2} ; [ DW_TAG_lexical_block ]
!50 = metadata !{i32 12, i32 3, metadata !49, null}
!51 = metadata !{i32 14, i32 16, metadata !52, null}
!52 = metadata !{i32 786443, metadata !49, i32 14, i32 3, metadata !6, i32 3} ; [ DW_TAG_lexical_block ]
!53 = metadata !{i32 15, i32 42, metadata !54, null}
!54 = metadata !{i32 786443, metadata !52, i32 14, i32 49, metadata !6, i32 4} ; [ DW_TAG_lexical_block ]
!55 = metadata !{i32 786688, metadata !54, metadata !"index", metadata !6, i32 15, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!56 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!57 = metadata !{i32 16, i32 4, metadata !54, null}
!58 = metadata !{i32 17, i32 4, metadata !54, null}
!59 = metadata !{i32 14, i32 44, metadata !52, null}
!60 = metadata !{i32 786688, metadata !52, metadata !"j", metadata !6, i32 14, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!61 = metadata !{i32 10, i32 43, metadata !45, null}
!62 = metadata !{i32 786688, metadata !45, metadata !"i", metadata !6, i32 10, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!63 = metadata !{i32 22, i32 3, metadata !64, null}
!64 = metadata !{i32 786443, metadata !47, i32 21, i32 48, metadata !6, i32 6} ; [ DW_TAG_lexical_block ]
!65 = metadata !{i32 23, i32 3, metadata !64, null}
!66 = metadata !{i32 21, i32 43, metadata !47, null}
!67 = metadata !{i32 786688, metadata !47, metadata !"i", metadata !6, i32 21, metadata !56, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!68 = metadata !{i32 26, i32 1, metadata !37, null}
