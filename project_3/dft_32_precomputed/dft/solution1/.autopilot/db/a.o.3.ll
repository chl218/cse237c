; ModuleID = 'D:/Projects/vivado/project_3/dft_32_precomputed/dft/solution1/.autopilot/db/a.o.3.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@sct = internal unnamed_addr constant [32 x float] [float 0.000000e+00, float 0xBFC8F8B580000000, float 0xBFD87DE0E0000000, float 0xBFE1C73AC0000000, float 0xBFE6A09EE0000000, float 0xBFEA9B6700000000, float 0xBFED906CC0000000, float 0xBFEF629740000000, float -1.000000e+00, float 0xBFEF629740000000, float 0xBFED906CC0000000, float 0xBFEA9B6700000000, float 0xBFE6A09EE0000000, float 0xBFE1C73AC0000000, float 0xBFD87DE0E0000000, float 0xBFC8F8B580000000, float -0.000000e+00, float 0x3FC8F8B580000000, float 0x3FD87DE0E0000000, float 0x3FE1C73AC0000000, float 0x3FE6A09EE0000000, float 0x3FEA9B6700000000, float 0x3FED906CC0000000, float 0x3FEF629740000000, float 1.000000e+00, float 0x3FEF629740000000, float 0x3FED906CC0000000, float 0x3FEA9B6700000000, float 0x3FE6A09EE0000000, float 0x3FE1C73AC0000000, float 0x3FD87DE0E0000000, float 0x3FC8F8B580000000], align 16 ; [#uses=1 type=[32 x float]*]
@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a] ; [#uses=0 type=[1 x void ()*]*]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535] ; [#uses=0 type=[1 x i32]*]
@dft_str = internal unnamed_addr constant [4 x i8] c"dft\00" ; [#uses=1 type=[4 x i8]*]
@cct = internal unnamed_addr constant [32 x float] [float 1.000000e+00, float 0x3FEF629740000000, float 0x3FED906CC0000000, float 0x3FEA9B6700000000, float 0x3FE6A09EE0000000, float 0x3FE1C73AC0000000, float 0x3FD87DE0E0000000, float 0x3FC8F8B580000000, float 0.000000e+00, float 0xBFC8F8B580000000, float 0xBFD87DE0E0000000, float 0xBFE1C73AC0000000, float 0xBFE6A09EE0000000, float 0xBFEA9B6700000000, float 0xBFED906CC0000000, float 0xBFEF629740000000, float -1.000000e+00, float 0xBFEF629740000000, float 0xBFED906CC0000000, float 0xBFEA9B6700000000, float 0xBFE6A09EE0000000, float 0xBFE1C73AC0000000, float 0xBFD87DE0E0000000, float 0xBFC8F8B580000000, float -0.000000e+00, float 0x3FC8F8B580000000, float 0x3FD87DE0E0000000, float 0x3FE1C73AC0000000, float 0x3FE6A09EE0000000, float 0x3FEA9B6700000000, float 0x3FED906CC0000000, float 0x3FEF629740000000], align 16 ; [#uses=1 type=[32 x float]*]

; [#uses=6]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=2]
declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

; [#uses=0]
define void @dft([32 x float]* %real_i, [32 x float]* %imag_i) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap([32 x float]* %real_i) nounwind, !map !7
  call void (...)* @_ssdm_op_SpecBitsMap([32 x float]* %imag_i) nounwind, !map !13
  call void (...)* @_ssdm_op_SpecTopModule([4 x i8]* @dft_str) nounwind
  %tmp_real = alloca [32 x float], align 16       ; [#uses=2 type=[32 x float]*]
  %tmp_imag = alloca [32 x float], align 16       ; [#uses=2 type=[32 x float]*]
  call void @llvm.dbg.value(metadata !{[32 x float]* %real_i}, i64 0, metadata !17), !dbg !30 ; [debug line = 4:16] [debug variable = real_i]
  call void @llvm.dbg.value(metadata !{[32 x float]* %imag_i}, i64 0, metadata !31), !dbg !32 ; [debug line = 4:52] [debug variable = imag_i]
  call void @llvm.dbg.declare(metadata !{[32 x float]* %tmp_real}, metadata !33), !dbg !36 ; [debug line = 7:8] [debug variable = tmp_real]
  call void @llvm.dbg.declare(metadata !{[32 x float]* %tmp_imag}, metadata !37), !dbg !38 ; [debug line = 8:8] [debug variable = tmp_imag]
  br label %.loopexit, !dbg !39                   ; [debug line = 10:15]

.loopexit:                                        ; preds = %2, %0
  %i = phi i6 [ 0, %0 ], [ %i_2, %2 ]             ; [#uses=4 type=i6]
  %tmp_1 = trunc i6 %i to i5, !dbg !41            ; [#uses=1 type=i5] [debug line = 15:42]
  %exitcond2 = icmp eq i6 %i, -32, !dbg !39       ; [#uses=1 type=i1] [debug line = 10:15]
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 32, i64 32, i64 32) nounwind ; [#uses=0 type=i32]
  %i_2 = add i6 1, %i, !dbg !45                   ; [#uses=1 type=i6] [debug line = 10:43]
  call void @llvm.dbg.value(metadata !{i6 %i_2}, i64 0, metadata !46), !dbg !45 ; [debug line = 10:43] [debug variable = i]
  br i1 %exitcond2, label %.preheader, label %1, !dbg !39 ; [debug line = 10:15]

; <label>:1                                       ; preds = %.loopexit
  %tmp = zext i6 %i to i64, !dbg !48              ; [#uses=2 type=i64] [debug line = 11:3]
  %tmp_real_addr = getelementptr inbounds [32 x float]* %tmp_real, i64 0, i64 %tmp, !dbg !48 ; [#uses=2 type=float*] [debug line = 11:3]
  store float 0.000000e+00, float* %tmp_real_addr, align 4, !dbg !48 ; [debug line = 11:3]
  %tmp_imag_addr = getelementptr inbounds [32 x float]* %tmp_imag, i64 0, i64 %tmp, !dbg !49 ; [#uses=1 type=float*] [debug line = 12:3]
  br label %2, !dbg !50                           ; [debug line = 14:16]

; <label>:2                                       ; preds = %3, %1
  %storemerge = phi float [ 0.000000e+00, %1 ], [ %tmp_12, %3 ] ; [#uses=2 type=float]
  %tmp_2 = phi float [ 0.000000e+00, %1 ], [ %tmp_5, %3 ] ; [#uses=1 type=float]
  %j = phi i6 [ 0, %1 ], [ %j_1, %3 ]             ; [#uses=4 type=i6]
  store float %storemerge, float* %tmp_imag_addr, align 4, !dbg !51 ; [debug line = 17:4]
  %tmp_13 = trunc i6 %j to i5, !dbg !41           ; [#uses=1 type=i5] [debug line = 15:42]
  %exitcond1 = icmp eq i6 %j, -32, !dbg !50       ; [#uses=1 type=i1] [debug line = 14:16]
  %empty_3 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 32, i64 32, i64 32) nounwind ; [#uses=0 type=i32]
  %j_1 = add i6 1, %j, !dbg !52                   ; [#uses=1 type=i6] [debug line = 14:44]
  br i1 %exitcond1, label %.loopexit, label %3, !dbg !50 ; [debug line = 14:16]

; <label>:3                                       ; preds = %2
  %index = mul i5 %tmp_1, %tmp_13, !dbg !41       ; [#uses=1 type=i5] [debug line = 15:42]
  call void @llvm.dbg.value(metadata !{i5 %index}, i64 0, metadata !53), !dbg !41 ; [debug line = 15:42] [debug variable = index]
  %tmp_7 = zext i6 %j to i64, !dbg !54            ; [#uses=2 type=i64] [debug line = 16:4]
  %real_i_addr_1 = getelementptr [32 x float]* %real_i, i64 0, i64 %tmp_7, !dbg !54 ; [#uses=1 type=float*] [debug line = 16:4]
  %real_i_load = load float* %real_i_addr_1, align 4, !dbg !54 ; [#uses=2 type=float] [debug line = 16:4]
  %tmp_8 = zext i5 %index to i64, !dbg !54        ; [#uses=2 type=i64] [debug line = 16:4]
  %cct_addr = getelementptr inbounds [32 x float]* @cct, i64 0, i64 %tmp_8, !dbg !54 ; [#uses=1 type=float*] [debug line = 16:4]
  %cct_load = load float* %cct_addr, align 4, !dbg !54 ; [#uses=2 type=float] [debug line = 16:4]
  %tmp_9 = fmul float %real_i_load, %cct_load, !dbg !54 ; [#uses=1 type=float] [debug line = 16:4]
  %imag_i_addr_1 = getelementptr [32 x float]* %imag_i, i64 0, i64 %tmp_7, !dbg !54 ; [#uses=1 type=float*] [debug line = 16:4]
  %imag_i_load = load float* %imag_i_addr_1, align 4, !dbg !54 ; [#uses=2 type=float] [debug line = 16:4]
  %sct_addr = getelementptr inbounds [32 x float]* @sct, i64 0, i64 %tmp_8, !dbg !54 ; [#uses=1 type=float*] [debug line = 16:4]
  %sct_load = load float* %sct_addr, align 4, !dbg !54 ; [#uses=2 type=float] [debug line = 16:4]
  %tmp_s = fmul float %imag_i_load, %sct_load, !dbg !54 ; [#uses=1 type=float] [debug line = 16:4]
  %tmp_4 = fsub float %tmp_9, %tmp_s, !dbg !54    ; [#uses=1 type=float] [debug line = 16:4]
  %tmp_5 = fadd float %tmp_2, %tmp_4, !dbg !54    ; [#uses=2 type=float] [debug line = 16:4]
  store float %tmp_5, float* %tmp_real_addr, align 4, !dbg !54 ; [debug line = 16:4]
  %tmp_6 = fmul float %real_i_load, %sct_load, !dbg !51 ; [#uses=1 type=float] [debug line = 17:4]
  %tmp_10 = fmul float %imag_i_load, %cct_load, !dbg !51 ; [#uses=1 type=float] [debug line = 17:4]
  %tmp_11 = fadd float %tmp_6, %tmp_10, !dbg !51  ; [#uses=1 type=float] [debug line = 17:4]
  %tmp_12 = fadd float %storemerge, %tmp_11, !dbg !51 ; [#uses=1 type=float] [debug line = 17:4]
  call void @llvm.dbg.value(metadata !{i6 %j_1}, i64 0, metadata !55), !dbg !52 ; [debug line = 14:44] [debug variable = j]
  br label %2, !dbg !52                           ; [debug line = 14:44]

.preheader:                                       ; preds = %4, %.loopexit
  %i1 = phi i6 [ %i_1, %4 ], [ 0, %.loopexit ]    ; [#uses=3 type=i6]
  %exitcond = icmp eq i6 %i1, -32, !dbg !56       ; [#uses=1 type=i1] [debug line = 21:15]
  %empty_4 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 32, i64 32, i64 32) nounwind ; [#uses=0 type=i32]
  %i_1 = add i6 %i1, 1, !dbg !58                  ; [#uses=1 type=i6] [debug line = 21:43]
  br i1 %exitcond, label %5, label %4, !dbg !56   ; [debug line = 21:15]

; <label>:4                                       ; preds = %.preheader
  %tmp_3 = zext i6 %i1 to i64, !dbg !59           ; [#uses=4 type=i64] [debug line = 22:3]
  %tmp_real_addr_1 = getelementptr inbounds [32 x float]* %tmp_real, i64 0, i64 %tmp_3, !dbg !59 ; [#uses=1 type=float*] [debug line = 22:3]
  %tmp_real_load = load float* %tmp_real_addr_1, align 4, !dbg !59 ; [#uses=1 type=float] [debug line = 22:3]
  %real_i_addr = getelementptr [32 x float]* %real_i, i64 0, i64 %tmp_3, !dbg !59 ; [#uses=1 type=float*] [debug line = 22:3]
  store float %tmp_real_load, float* %real_i_addr, align 4, !dbg !59 ; [debug line = 22:3]
  %tmp_imag_addr_1 = getelementptr inbounds [32 x float]* %tmp_imag, i64 0, i64 %tmp_3, !dbg !61 ; [#uses=1 type=float*] [debug line = 23:3]
  %tmp_imag_load = load float* %tmp_imag_addr_1, align 4, !dbg !61 ; [#uses=1 type=float] [debug line = 23:3]
  %imag_i_addr = getelementptr [32 x float]* %imag_i, i64 0, i64 %tmp_3, !dbg !61 ; [#uses=1 type=float*] [debug line = 23:3]
  store float %tmp_imag_load, float* %imag_i_addr, align 4, !dbg !61 ; [debug line = 23:3]
  call void @llvm.dbg.value(metadata !{i6 %i_1}, i64 0, metadata !62), !dbg !58 ; [debug line = 21:43] [debug variable = i]
  br label %.preheader, !dbg !58                  ; [debug line = 21:43]

; <label>:5                                       ; preds = %.preheader
  ret void, !dbg !63                              ; [debug line = 26:1]
}

; [#uses=1]
define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

; [#uses=3]
define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

; [#uses=2]
define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

; [#uses=0]
declare i5 @_ssdm_op_PartSelect.i5.i6.i32.i32(i6, i32, i32) nounwind readnone

; [#uses=0]
declare i16 @_ssdm_op_HSub(...)

; [#uses=0]
declare i16 @_ssdm_op_HMul(...)

; [#uses=0]
declare i16 @_ssdm_op_HDiv(...)

; [#uses=0]
declare i16 @_ssdm_op_HAdd(...)

; [#uses=1]
declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [1 x i32]* @llvm_global_ctors_0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 0, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 31, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"real_i", metadata !11, metadata !"float", i32 0, i32 31}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 31, i32 1}
!13 = metadata !{metadata !14}
!14 = metadata !{i32 0, i32 31, metadata !15}
!15 = metadata !{metadata !16}
!16 = metadata !{metadata !"imag_i", metadata !11, metadata !"float", i32 0, i32 31}
!17 = metadata !{i32 786689, metadata !18, metadata !"real_i", null, i32 4, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!18 = metadata !{i32 786478, i32 0, metadata !19, metadata !"dft", metadata !"dft", metadata !"_Z3dftPfS_", metadata !19, i32 4, metadata !20, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !25, i32 5} ; [ DW_TAG_subprogram ]
!19 = metadata !{i32 786473, metadata !"dft.cpp", metadata !"d:/Projects/vivado/project_3/dft_32_precomputed", null} ; [ DW_TAG_file_type ]
!20 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !21, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!21 = metadata !{null, metadata !22, metadata !22}
!22 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !23} ; [ DW_TAG_pointer_type ]
!23 = metadata !{i32 786454, null, metadata !"DTYPE", metadata !19, i32 2, i64 0, i64 0, i64 0, i32 0, metadata !24} ; [ DW_TAG_typedef ]
!24 = metadata !{i32 786468, null, metadata !"float", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 4} ; [ DW_TAG_base_type ]
!25 = metadata !{metadata !26}
!26 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!27 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 0, i64 0, i32 0, i32 0, metadata !23, metadata !28, i32 0, i32 0} ; [ DW_TAG_array_type ]
!28 = metadata !{metadata !29}
!29 = metadata !{i32 786465, i64 0, i64 31}       ; [ DW_TAG_subrange_type ]
!30 = metadata !{i32 4, i32 16, metadata !18, null}
!31 = metadata !{i32 786689, metadata !18, metadata !"imag_i", null, i32 4, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!32 = metadata !{i32 4, i32 52, metadata !18, null}
!33 = metadata !{i32 786688, metadata !34, metadata !"tmp_real", metadata !19, i32 7, metadata !35, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!34 = metadata !{i32 786443, metadata !18, i32 5, i32 1, metadata !19, i32 0} ; [ DW_TAG_lexical_block ]
!35 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 1024, i64 32, i32 0, i32 0, metadata !23, metadata !28, i32 0, i32 0} ; [ DW_TAG_array_type ]
!36 = metadata !{i32 7, i32 8, metadata !34, null}
!37 = metadata !{i32 786688, metadata !34, metadata !"tmp_imag", metadata !19, i32 8, metadata !35, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!38 = metadata !{i32 8, i32 8, metadata !34, null}
!39 = metadata !{i32 10, i32 15, metadata !40, null}
!40 = metadata !{i32 786443, metadata !34, i32 10, i32 2, metadata !19, i32 1} ; [ DW_TAG_lexical_block ]
!41 = metadata !{i32 15, i32 42, metadata !42, null}
!42 = metadata !{i32 786443, metadata !43, i32 14, i32 49, metadata !19, i32 4} ; [ DW_TAG_lexical_block ]
!43 = metadata !{i32 786443, metadata !44, i32 14, i32 3, metadata !19, i32 3} ; [ DW_TAG_lexical_block ]
!44 = metadata !{i32 786443, metadata !40, i32 10, i32 48, metadata !19, i32 2} ; [ DW_TAG_lexical_block ]
!45 = metadata !{i32 10, i32 43, metadata !40, null}
!46 = metadata !{i32 786688, metadata !40, metadata !"i", metadata !19, i32 10, metadata !47, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!47 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!48 = metadata !{i32 11, i32 3, metadata !44, null}
!49 = metadata !{i32 12, i32 3, metadata !44, null}
!50 = metadata !{i32 14, i32 16, metadata !43, null}
!51 = metadata !{i32 17, i32 4, metadata !42, null}
!52 = metadata !{i32 14, i32 44, metadata !43, null}
!53 = metadata !{i32 786688, metadata !42, metadata !"index", metadata !19, i32 15, metadata !47, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!54 = metadata !{i32 16, i32 4, metadata !42, null}
!55 = metadata !{i32 786688, metadata !43, metadata !"j", metadata !19, i32 14, metadata !47, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!56 = metadata !{i32 21, i32 15, metadata !57, null}
!57 = metadata !{i32 786443, metadata !34, i32 21, i32 2, metadata !19, i32 5} ; [ DW_TAG_lexical_block ]
!58 = metadata !{i32 21, i32 43, metadata !57, null}
!59 = metadata !{i32 22, i32 3, metadata !60, null}
!60 = metadata !{i32 786443, metadata !57, i32 21, i32 48, metadata !19, i32 6} ; [ DW_TAG_lexical_block ]
!61 = metadata !{i32 23, i32 3, metadata !60, null}
!62 = metadata !{i32 786688, metadata !57, metadata !"i", metadata !19, i32 21, metadata !47, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!63 = metadata !{i32 26, i32 1, metadata !34, null}
