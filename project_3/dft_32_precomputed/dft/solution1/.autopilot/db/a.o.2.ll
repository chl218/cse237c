; ModuleID = 'D:/Projects/vivado/project_3/dft_32_precomputed/dft/solution1/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@sct = internal unnamed_addr constant [32 x float] [float 0.000000e+00, float 0xBFC8F8B580000000, float 0xBFD87DE0E0000000, float 0xBFE1C73AC0000000, float 0xBFE6A09EE0000000, float 0xBFEA9B6700000000, float 0xBFED906CC0000000, float 0xBFEF629740000000, float -1.000000e+00, float 0xBFEF629740000000, float 0xBFED906CC0000000, float 0xBFEA9B6700000000, float 0xBFE6A09EE0000000, float 0xBFE1C73AC0000000, float 0xBFD87DE0E0000000, float 0xBFC8F8B580000000, float -0.000000e+00, float 0x3FC8F8B580000000, float 0x3FD87DE0E0000000, float 0x3FE1C73AC0000000, float 0x3FE6A09EE0000000, float 0x3FEA9B6700000000, float 0x3FED906CC0000000, float 0x3FEF629740000000, float 1.000000e+00, float 0x3FEF629740000000, float 0x3FED906CC0000000, float 0x3FEA9B6700000000, float 0x3FE6A09EE0000000, float 0x3FE1C73AC0000000, float 0x3FD87DE0E0000000, float 0x3FC8F8B580000000], align 16 ; [#uses=1 type=[32 x float]*]
@llvm.global_ctors.1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a] ; [#uses=0 type=[1 x void ()*]*]
@llvm.global_ctors.0 = appending global [1 x i32] [i32 65535] ; [#uses=0 type=[1 x i32]*]
@dft.str = internal unnamed_addr constant [4 x i8] c"dft\00" ; [#uses=1 type=[4 x i8]*]
@cct = internal unnamed_addr constant [32 x float] [float 1.000000e+00, float 0x3FEF629740000000, float 0x3FED906CC0000000, float 0x3FEA9B6700000000, float 0x3FE6A09EE0000000, float 0x3FE1C73AC0000000, float 0x3FD87DE0E0000000, float 0x3FC8F8B580000000, float 0.000000e+00, float 0xBFC8F8B580000000, float 0xBFD87DE0E0000000, float 0xBFE1C73AC0000000, float 0xBFE6A09EE0000000, float 0xBFEA9B6700000000, float 0xBFED906CC0000000, float 0xBFEF629740000000, float -1.000000e+00, float 0xBFEF629740000000, float 0xBFED906CC0000000, float 0xBFEA9B6700000000, float 0xBFE6A09EE0000000, float 0xBFE1C73AC0000000, float 0xBFD87DE0E0000000, float 0xBFC8F8B580000000, float -0.000000e+00, float 0x3FC8F8B580000000, float 0x3FD87DE0E0000000, float 0x3FE1C73AC0000000, float 0x3FE6A09EE0000000, float 0x3FEA9B6700000000, float 0x3FED906CC0000000, float 0x3FEF629740000000], align 16 ; [#uses=1 type=[32 x float]*]

; [#uses=6]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=2]
declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

; [#uses=0]
define void @dft([32 x float]* %real_i, [32 x float]* %imag_i) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap([32 x float]* %real_i) nounwind, !map !29
  call void (...)* @_ssdm_op_SpecBitsMap([32 x float]* %imag_i) nounwind, !map !35
  call void (...)* @_ssdm_op_SpecTopModule([4 x i8]* @dft.str) nounwind
  %tmp_real = alloca [32 x float], align 16       ; [#uses=2 type=[32 x float]*]
  %tmp_imag = alloca [32 x float], align 16       ; [#uses=2 type=[32 x float]*]
  call void @llvm.dbg.value(metadata !{[32 x float]* %real_i}, i64 0, metadata !39), !dbg !45 ; [debug line = 4:16] [debug variable = real_i]
  call void @llvm.dbg.value(metadata !{[32 x float]* %imag_i}, i64 0, metadata !46), !dbg !47 ; [debug line = 4:52] [debug variable = imag_i]
  call void @llvm.dbg.declare(metadata !{[32 x float]* %tmp_real}, metadata !48), !dbg !51 ; [debug line = 7:8] [debug variable = tmp_real]
  call void @llvm.dbg.declare(metadata !{[32 x float]* %tmp_imag}, metadata !52), !dbg !53 ; [debug line = 8:8] [debug variable = tmp_imag]
  br label %1, !dbg !54                           ; [debug line = 10:15]

; <label>:1                                       ; preds = %7, %0
  %i = phi i6 [ 0, %0 ], [ %i.2, %7 ]             ; [#uses=4 type=i6]
  %i.cast5 = trunc i6 %i to i5, !dbg !56          ; [#uses=1 type=i5] [debug line = 15:42]
  %exitcond2 = icmp eq i6 %i, -32, !dbg !54       ; [#uses=1 type=i1] [debug line = 10:15]
  %2 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 32, i64 32, i64 32) nounwind ; [#uses=0 type=i32]
  br i1 %exitcond2, label %.preheader, label %3, !dbg !54 ; [debug line = 10:15]

; <label>:3                                       ; preds = %1
  %tmp = zext i6 %i to i64, !dbg !60              ; [#uses=2 type=i64] [debug line = 11:3]
  %tmp_real.addr = getelementptr inbounds [32 x float]* %tmp_real, i64 0, i64 %tmp, !dbg !60 ; [#uses=2 type=float*] [debug line = 11:3]
  store float 0.000000e+00, float* %tmp_real.addr, align 4, !dbg !60 ; [debug line = 11:3]
  %tmp_imag.addr = getelementptr inbounds [32 x float]* %tmp_imag, i64 0, i64 %tmp, !dbg !61 ; [#uses=2 type=float*] [debug line = 12:3]
  store float 0.000000e+00, float* %tmp_imag.addr, align 4, !dbg !61 ; [debug line = 12:3]
  br label %4, !dbg !62                           ; [debug line = 14:16]

; <label>:4                                       ; preds = %6, %3
  %tmp.1 = phi float [ 0.000000e+00, %3 ], [ %tmp.12, %6 ] ; [#uses=1 type=float]
  %tmp.2 = phi float [ 0.000000e+00, %3 ], [ %tmp.5, %6 ] ; [#uses=1 type=float]
  %j = phi i6 [ 0, %3 ], [ %j.1, %6 ]             ; [#uses=4 type=i6]
  %j.cast3 = trunc i6 %j to i5, !dbg !56          ; [#uses=1 type=i5] [debug line = 15:42]
  %exitcond1 = icmp eq i6 %j, -32, !dbg !62       ; [#uses=1 type=i1] [debug line = 14:16]
  %5 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 32, i64 32, i64 32) nounwind ; [#uses=0 type=i32]
  br i1 %exitcond1, label %7, label %6, !dbg !62  ; [debug line = 14:16]

; <label>:6                                       ; preds = %4
  %index = mul i5 %j.cast3, %i.cast5, !dbg !56    ; [#uses=1 type=i5] [debug line = 15:42]
  call void @llvm.dbg.value(metadata !{i5 %index}, i64 0, metadata !63), !dbg !56 ; [debug line = 15:42] [debug variable = index]
  %tmp.7 = zext i6 %j to i64, !dbg !65            ; [#uses=2 type=i64] [debug line = 16:4]
  %real_i.addr.1 = getelementptr [32 x float]* %real_i, i64 0, i64 %tmp.7, !dbg !65 ; [#uses=1 type=float*] [debug line = 16:4]
  %real_i.load = load float* %real_i.addr.1, align 4, !dbg !65 ; [#uses=2 type=float] [debug line = 16:4]
  %tmp.8 = zext i5 %index to i64, !dbg !65        ; [#uses=2 type=i64] [debug line = 16:4]
  %cct.addr = getelementptr inbounds [32 x float]* @cct, i64 0, i64 %tmp.8, !dbg !65 ; [#uses=1 type=float*] [debug line = 16:4]
  %cct.load = load float* %cct.addr, align 4, !dbg !65 ; [#uses=2 type=float] [debug line = 16:4]
  %tmp.9 = fmul float %real_i.load, %cct.load, !dbg !65 ; [#uses=1 type=float] [debug line = 16:4]
  %imag_i.addr.1 = getelementptr [32 x float]* %imag_i, i64 0, i64 %tmp.7, !dbg !65 ; [#uses=1 type=float*] [debug line = 16:4]
  %imag_i.load = load float* %imag_i.addr.1, align 4, !dbg !65 ; [#uses=2 type=float] [debug line = 16:4]
  %sct.addr = getelementptr inbounds [32 x float]* @sct, i64 0, i64 %tmp.8, !dbg !65 ; [#uses=1 type=float*] [debug line = 16:4]
  %sct.load = load float* %sct.addr, align 4, !dbg !65 ; [#uses=2 type=float] [debug line = 16:4]
  %tmp. = fmul float %imag_i.load, %sct.load, !dbg !65 ; [#uses=1 type=float] [debug line = 16:4]
  %tmp.4 = fsub float %tmp.9, %tmp., !dbg !65     ; [#uses=1 type=float] [debug line = 16:4]
  %tmp.5 = fadd float %tmp.2, %tmp.4, !dbg !65    ; [#uses=2 type=float] [debug line = 16:4]
  store float %tmp.5, float* %tmp_real.addr, align 4, !dbg !65 ; [debug line = 16:4]
  %tmp.6 = fmul float %real_i.load, %sct.load, !dbg !66 ; [#uses=1 type=float] [debug line = 17:4]
  %tmp.10 = fmul float %imag_i.load, %cct.load, !dbg !66 ; [#uses=1 type=float] [debug line = 17:4]
  %tmp.11 = fadd float %tmp.6, %tmp.10, !dbg !66  ; [#uses=1 type=float] [debug line = 17:4]
  %tmp.12 = fadd float %tmp.1, %tmp.11, !dbg !66  ; [#uses=2 type=float] [debug line = 17:4]
  store float %tmp.12, float* %tmp_imag.addr, align 4, !dbg !66 ; [debug line = 17:4]
  %j.1 = add i6 %j, 1, !dbg !67                   ; [#uses=1 type=i6] [debug line = 14:44]
  call void @llvm.dbg.value(metadata !{i6 %j.1}, i64 0, metadata !68), !dbg !67 ; [debug line = 14:44] [debug variable = j]
  br label %4, !dbg !67                           ; [debug line = 14:44]

; <label>:7                                       ; preds = %4
  %i.2 = add i6 %i, 1, !dbg !69                   ; [#uses=1 type=i6] [debug line = 10:43]
  call void @llvm.dbg.value(metadata !{i6 %i.2}, i64 0, metadata !70), !dbg !69 ; [debug line = 10:43] [debug variable = i]
  br label %1, !dbg !69                           ; [debug line = 10:43]

.preheader:                                       ; preds = %9, %1
  %i1 = phi i6 [ %i.1, %9 ], [ 0, %1 ]            ; [#uses=3 type=i6]
  %exitcond = icmp eq i6 %i1, -32, !dbg !71       ; [#uses=1 type=i1] [debug line = 21:15]
  %8 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 32, i64 32, i64 32) nounwind ; [#uses=0 type=i32]
  br i1 %exitcond, label %10, label %9, !dbg !71  ; [debug line = 21:15]

; <label>:9                                       ; preds = %.preheader
  %tmp.3 = zext i6 %i1 to i64, !dbg !73           ; [#uses=4 type=i64] [debug line = 22:3]
  %tmp_real.addr.1 = getelementptr inbounds [32 x float]* %tmp_real, i64 0, i64 %tmp.3, !dbg !73 ; [#uses=1 type=float*] [debug line = 22:3]
  %tmp_real.load = load float* %tmp_real.addr.1, align 4, !dbg !73 ; [#uses=1 type=float] [debug line = 22:3]
  %real_i.addr = getelementptr [32 x float]* %real_i, i64 0, i64 %tmp.3, !dbg !73 ; [#uses=1 type=float*] [debug line = 22:3]
  store float %tmp_real.load, float* %real_i.addr, align 4, !dbg !73 ; [debug line = 22:3]
  %tmp_imag.addr.1 = getelementptr inbounds [32 x float]* %tmp_imag, i64 0, i64 %tmp.3, !dbg !75 ; [#uses=1 type=float*] [debug line = 23:3]
  %tmp_imag.load = load float* %tmp_imag.addr.1, align 4, !dbg !75 ; [#uses=1 type=float] [debug line = 23:3]
  %imag_i.addr = getelementptr [32 x float]* %imag_i, i64 0, i64 %tmp.3, !dbg !75 ; [#uses=1 type=float*] [debug line = 23:3]
  store float %tmp_imag.load, float* %imag_i.addr, align 4, !dbg !75 ; [debug line = 23:3]
  %i.1 = add i6 %i1, 1, !dbg !76                  ; [#uses=1 type=i6] [debug line = 21:43]
  call void @llvm.dbg.value(metadata !{i6 %i.1}, i64 0, metadata !77), !dbg !76 ; [debug line = 21:43] [debug variable = i]
  br label %.preheader, !dbg !76                  ; [debug line = 21:43]

; <label>:10                                      ; preds = %.preheader
  ret void, !dbg !78                              ; [debug line = 26:1]
}

; [#uses=1]
declare void @_ssdm_op_SpecTopModule(...)

; [#uses=3]
declare i32 @_ssdm_op_SpecLoopTripCount(...)

; [#uses=2]
declare void @_ssdm_op_SpecBitsMap(...)

; [#uses=1]
declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}
!llvm.dbg.cu = !{!7}

!0 = metadata !{metadata !1, [1 x i32]* @llvm.global_ctors.0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 0, i32 1}
!7 = metadata !{i32 786449, i32 0, i32 4, metadata !"D:/Projects/vivado/project_3/dft_32_precomputed/dft/solution1/.autopilot/db/dft.pragma.2.cpp", metadata !"d:/Projects/vivado/project_3/dft_32_precomputed", metadata !"clang version 3.1 ", i1 true, i1 false, metadata !"", i32 0, null, null, null, metadata !8} ; [ DW_TAG_compile_unit ]
!8 = metadata !{metadata !9}
!9 = metadata !{metadata !10, metadata !19, metadata !20}
!10 = metadata !{i32 786484, i32 0, null, metadata !"sct", metadata !"sct", metadata !"_ZL3sct", metadata !11, i32 3, metadata !12, i32 1, i32 1, [32 x float]* @sct} ; [ DW_TAG_variable ]
!11 = metadata !{i32 786473, metadata !"./coefficients32.h", metadata !"d:/Projects/vivado/project_3/dft_32_precomputed", null} ; [ DW_TAG_file_type ]
!12 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 1024, i64 32, i32 0, i32 0, metadata !13, metadata !17, i32 0, i32 0} ; [ DW_TAG_array_type ]
!13 = metadata !{i32 786470, null, metadata !"", null, i32 0, i64 0, i64 0, i64 0, i32 0, metadata !14} ; [ DW_TAG_const_type ]
!14 = metadata !{i32 786454, null, metadata !"DTYPE", metadata !15, i32 2, i64 0, i64 0, i64 0, i32 0, metadata !16} ; [ DW_TAG_typedef ]
!15 = metadata !{i32 786473, metadata !"dft.cpp", metadata !"d:/Projects/vivado/project_3/dft_32_precomputed", null} ; [ DW_TAG_file_type ]
!16 = metadata !{i32 786468, null, metadata !"float", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 4} ; [ DW_TAG_base_type ]
!17 = metadata !{metadata !18}
!18 = metadata !{i32 786465, i64 0, i64 31}       ; [ DW_TAG_subrange_type ]
!19 = metadata !{i32 786484, i32 0, null, metadata !"cct", metadata !"cct", metadata !"_ZL3cct", metadata !11, i32 1, metadata !12, i32 1, i32 1, [32 x float]* @cct} ; [ DW_TAG_variable ]
!20 = metadata !{i32 786484, i32 0, null, metadata !"ssdm_global_array_ins", metadata !"ssdm_global_array_ins", metadata !"_ZL21ssdm_global_array_ins", metadata !15, i32 35, metadata !21, i32 1, i32 1, null} ; [ DW_TAG_variable ]
!21 = metadata !{i32 786434, null, metadata !"ssdm_global_array_dftpp0cppaplinecpp", metadata !15, i32 28, i64 8, i64 8, i32 0, i32 0, null, metadata !22, i32 0, null, null} ; [ DW_TAG_class_type ]
!22 = metadata !{metadata !23}
!23 = metadata !{i32 786478, i32 0, metadata !21, metadata !"ssdm_global_array_dftpp0cppaplinecpp", metadata !"ssdm_global_array_dftpp0cppaplinecpp", metadata !"", metadata !15, i32 30, metadata !24, i1 false, i1 false, i32 0, i32 0, null, i32 256, i1 false, null, null, i32 0, metadata !27, i32 30} ; [ DW_TAG_subprogram ]
!24 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !25, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!25 = metadata !{null, metadata !26}
!26 = metadata !{i32 786447, i32 0, metadata !"", i32 0, i32 0, i64 64, i64 64, i64 0, i32 64, metadata !21} ; [ DW_TAG_pointer_type ]
!27 = metadata !{metadata !28}
!28 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!29 = metadata !{metadata !30}
!30 = metadata !{i32 0, i32 31, metadata !31}
!31 = metadata !{metadata !32}
!32 = metadata !{metadata !"real_i", metadata !33, metadata !"float", i32 0, i32 31}
!33 = metadata !{metadata !34}
!34 = metadata !{i32 0, i32 31, i32 1}
!35 = metadata !{metadata !36}
!36 = metadata !{i32 0, i32 31, metadata !37}
!37 = metadata !{metadata !38}
!38 = metadata !{metadata !"imag_i", metadata !33, metadata !"float", i32 0, i32 31}
!39 = metadata !{i32 786689, metadata !40, metadata !"real_i", null, i32 4, metadata !44, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!40 = metadata !{i32 786478, i32 0, metadata !15, metadata !"dft", metadata !"dft", metadata !"_Z3dftPfS_", metadata !15, i32 4, metadata !41, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !27, i32 5} ; [ DW_TAG_subprogram ]
!41 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !42, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!42 = metadata !{null, metadata !43, metadata !43}
!43 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !14} ; [ DW_TAG_pointer_type ]
!44 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 0, i64 0, i32 0, i32 0, metadata !14, metadata !17, i32 0, i32 0} ; [ DW_TAG_array_type ]
!45 = metadata !{i32 4, i32 16, metadata !40, null}
!46 = metadata !{i32 786689, metadata !40, metadata !"imag_i", null, i32 4, metadata !44, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!47 = metadata !{i32 4, i32 52, metadata !40, null}
!48 = metadata !{i32 786688, metadata !49, metadata !"tmp_real", metadata !15, i32 7, metadata !50, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!49 = metadata !{i32 786443, metadata !40, i32 5, i32 1, metadata !15, i32 0} ; [ DW_TAG_lexical_block ]
!50 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 1024, i64 32, i32 0, i32 0, metadata !14, metadata !17, i32 0, i32 0} ; [ DW_TAG_array_type ]
!51 = metadata !{i32 7, i32 8, metadata !49, null}
!52 = metadata !{i32 786688, metadata !49, metadata !"tmp_imag", metadata !15, i32 8, metadata !50, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!53 = metadata !{i32 8, i32 8, metadata !49, null}
!54 = metadata !{i32 10, i32 15, metadata !55, null}
!55 = metadata !{i32 786443, metadata !49, i32 10, i32 2, metadata !15, i32 1} ; [ DW_TAG_lexical_block ]
!56 = metadata !{i32 15, i32 42, metadata !57, null}
!57 = metadata !{i32 786443, metadata !58, i32 14, i32 49, metadata !15, i32 4} ; [ DW_TAG_lexical_block ]
!58 = metadata !{i32 786443, metadata !59, i32 14, i32 3, metadata !15, i32 3} ; [ DW_TAG_lexical_block ]
!59 = metadata !{i32 786443, metadata !55, i32 10, i32 48, metadata !15, i32 2} ; [ DW_TAG_lexical_block ]
!60 = metadata !{i32 11, i32 3, metadata !59, null}
!61 = metadata !{i32 12, i32 3, metadata !59, null}
!62 = metadata !{i32 14, i32 16, metadata !58, null}
!63 = metadata !{i32 786688, metadata !57, metadata !"index", metadata !15, i32 15, metadata !64, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!64 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!65 = metadata !{i32 16, i32 4, metadata !57, null}
!66 = metadata !{i32 17, i32 4, metadata !57, null}
!67 = metadata !{i32 14, i32 44, metadata !58, null}
!68 = metadata !{i32 786688, metadata !58, metadata !"j", metadata !15, i32 14, metadata !64, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!69 = metadata !{i32 10, i32 43, metadata !55, null}
!70 = metadata !{i32 786688, metadata !55, metadata !"i", metadata !15, i32 10, metadata !64, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!71 = metadata !{i32 21, i32 15, metadata !72, null}
!72 = metadata !{i32 786443, metadata !49, i32 21, i32 2, metadata !15, i32 5} ; [ DW_TAG_lexical_block ]
!73 = metadata !{i32 22, i32 3, metadata !74, null}
!74 = metadata !{i32 786443, metadata !72, i32 21, i32 48, metadata !15, i32 6} ; [ DW_TAG_lexical_block ]
!75 = metadata !{i32 23, i32 3, metadata !74, null}
!76 = metadata !{i32 21, i32 43, metadata !72, null}
!77 = metadata !{i32 786688, metadata !72, metadata !"i", metadata !15, i32 21, metadata !64, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!78 = metadata !{i32 26, i32 1, metadata !49, null}
