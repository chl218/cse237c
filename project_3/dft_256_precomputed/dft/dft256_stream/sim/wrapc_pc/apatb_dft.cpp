// ==============================================================
// File generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// ==============================================================

#include <systemc>
#include <iostream>
#include <cstdlib>
#include <cstddef>
#include <stdint.h>
#include "SysCFileHandler.h"
#include "ap_int.h"
#include "ap_fixed.h"
#include <complex>
#include <stdbool.h>
#include "autopilot_cbe.h"
#include "ap_stream.h"
#include "hls_stream.h"
#include "hls_half.h"

using namespace std;
using namespace sc_core;
using namespace sc_dt;


// [dump_struct_tree [build_nameSpaceTree] dumpedStructList] ---------->
	typedef struct  {
		hls::stream<float > real;
		hls::stream<float > imag;
	} DTYPE;

extern float real_i[256];

extern float imag_i[256];



// [dump_enumeration [get_enumeration_list]] ---------->


// wrapc file define: "stream_i_real_V"
#define AUTOTB_TVIN_stream_i_real_V  "../tv/cdatafile/c.dft.autotvin_stream_i_real_V.dat"
#define WRAPC_STREAM_SIZE_IN_stream_i_real_V  "../tv/stream_size/stream_size_in_stream_i_real_V.dat"
#define WRAPC_STREAM_INGRESS_STATUS_stream_i_real_V  "../tv/stream_size/stream_ingress_status_stream_i_real_V.dat"
// wrapc file define: "stream_i_imag_V"
#define AUTOTB_TVIN_stream_i_imag_V  "../tv/cdatafile/c.dft.autotvin_stream_i_imag_V.dat"
#define WRAPC_STREAM_SIZE_IN_stream_i_imag_V  "../tv/stream_size/stream_size_in_stream_i_imag_V.dat"
#define WRAPC_STREAM_INGRESS_STATUS_stream_i_imag_V  "../tv/stream_size/stream_ingress_status_stream_i_imag_V.dat"
// wrapc file define: "stream_o_real_V"
#define AUTOTB_TVOUT_stream_o_real_V  "../tv/cdatafile/c.dft.autotvout_stream_o_real_V.dat"
#define AUTOTB_TVIN_stream_o_real_V  "../tv/cdatafile/c.dft.autotvin_stream_o_real_V.dat"
#define WRAPC_STREAM_SIZE_OUT_stream_o_real_V  "../tv/stream_size/stream_size_out_stream_o_real_V.dat"
#define WRAPC_STREAM_EGRESS_STATUS_stream_o_real_V  "../tv/stream_size/stream_egress_status_stream_o_real_V.dat"
// wrapc file define: "stream_o_imag_V"
#define AUTOTB_TVOUT_stream_o_imag_V  "../tv/cdatafile/c.dft.autotvout_stream_o_imag_V.dat"
#define AUTOTB_TVIN_stream_o_imag_V  "../tv/cdatafile/c.dft.autotvin_stream_o_imag_V.dat"
#define WRAPC_STREAM_SIZE_OUT_stream_o_imag_V  "../tv/stream_size/stream_size_out_stream_o_imag_V.dat"
#define WRAPC_STREAM_EGRESS_STATUS_stream_o_imag_V  "../tv/stream_size/stream_egress_status_stream_o_imag_V.dat"

#define INTER_TCL  "../tv/cdatafile/ref.tcl"

// tvout file define: "stream_o_real_V"
#define AUTOTB_TVOUT_PC_stream_o_real_V  "../tv/rtldatafile/rtl.dft.autotvout_stream_o_real_V.dat"
// tvout file define: "stream_o_imag_V"
#define AUTOTB_TVOUT_PC_stream_o_imag_V  "../tv/rtldatafile/rtl.dft.autotvout_stream_o_imag_V.dat"

class INTER_TCL_FILE {
	public:
		INTER_TCL_FILE(const char* name) {
			mName = name;
			stream_i_real_V_depth = 0;
			stream_i_imag_V_depth = 0;
			stream_o_real_V_depth = 0;
			stream_o_imag_V_depth = 0;
			trans_num =0;
		}

		~INTER_TCL_FILE() {
			mFile.open(mName);
			if (!mFile.good()) {
				cout << "Failed to open file ref.tcl" << endl;
				exit (1);
			}
			string total_list = get_depth_list();
			mFile << "set depth_list {\n";
			mFile << total_list;
			mFile << "}\n";
			mFile << "set trans_num "<<trans_num<<endl;
			mFile.close();
		}

		string get_depth_list () {
			stringstream total_list;
			total_list << "{stream_i_real_V " << stream_i_real_V_depth << "}\n";
			total_list << "{stream_i_imag_V " << stream_i_imag_V_depth << "}\n";
			total_list << "{stream_o_real_V " << stream_o_real_V_depth << "}\n";
			total_list << "{stream_o_imag_V " << stream_o_imag_V_depth << "}\n";
			return total_list.str();
		}

		void set_num (int num , int* class_num) {
			(*class_num) = (*class_num) > num ? (*class_num) : num;
		}
	public:
		int stream_i_real_V_depth;
		int stream_i_imag_V_depth;
		int stream_o_real_V_depth;
		int stream_o_imag_V_depth;
		int trans_num;

	private:
		ofstream mFile;
		const char* mName;
};


#define dft AESL_ORIG_DUT_dft
extern void dft (
DTYPE& stream_i,
DTYPE& stream_o);
#undef dft

void dft (
DTYPE& stream_i,
DTYPE& stream_o)
{
	fstream wrapc_switch_file_token;
	wrapc_switch_file_token.open(".hls_cosim_wrapc_switch.log");
	int AESL_i;
	if (wrapc_switch_file_token.good())
	{
		static unsigned AESL_transaction_pc = 0;
		string AESL_token;
		string AESL_num;
		static AESL_FILE_HANDLER aesl_fh;

		// pop stream input: "stream_i.real"
		aesl_fh.read(WRAPC_STREAM_SIZE_IN_stream_i_real_V, AESL_token); // [[transaction]]
		aesl_fh.read(WRAPC_STREAM_SIZE_IN_stream_i_real_V, AESL_num); // transaction number

		if (atoi(AESL_num.c_str()) == AESL_transaction_pc)
		{
			aesl_fh.read(WRAPC_STREAM_SIZE_IN_stream_i_real_V, AESL_token); // pop_size
			int aesl_tmp_1 = atoi(AESL_token.c_str());
			for (int i = 0; i < aesl_tmp_1; i++)
			{
				stream_i.real.read();
			}
			aesl_fh.read(WRAPC_STREAM_SIZE_IN_stream_i_real_V, AESL_token); // [[/transaction]]
		}

		// pop stream input: "stream_i.imag"
		aesl_fh.read(WRAPC_STREAM_SIZE_IN_stream_i_imag_V, AESL_token); // [[transaction]]
		aesl_fh.read(WRAPC_STREAM_SIZE_IN_stream_i_imag_V, AESL_num); // transaction number

		if (atoi(AESL_num.c_str()) == AESL_transaction_pc)
		{
			aesl_fh.read(WRAPC_STREAM_SIZE_IN_stream_i_imag_V, AESL_token); // pop_size
			int aesl_tmp_4 = atoi(AESL_token.c_str());
			for (int i = 0; i < aesl_tmp_4; i++)
			{
				stream_i.imag.read();
			}
			aesl_fh.read(WRAPC_STREAM_SIZE_IN_stream_i_imag_V, AESL_token); // [[/transaction]]
		}

		// define output stream variables: "stream_o.real"
		std::vector<float > aesl_tmp_6;
		int aesl_tmp_7;
		int aesl_tmp_8 = 0;

		// read output stream size: "stream_o.real"
		aesl_fh.read(WRAPC_STREAM_SIZE_OUT_stream_o_real_V, AESL_token); // [[transaction]]
		aesl_fh.read(WRAPC_STREAM_SIZE_OUT_stream_o_real_V, AESL_num); // transaction number

		if (atoi(AESL_num.c_str()) == AESL_transaction_pc)
		{
			aesl_fh.read(WRAPC_STREAM_SIZE_OUT_stream_o_real_V, AESL_token); // pop_size
			aesl_tmp_7 = atoi(AESL_token.c_str());
			aesl_fh.read(WRAPC_STREAM_SIZE_OUT_stream_o_real_V, AESL_token); // [[/transaction]]
		}

		// define output stream variables: "stream_o.imag"
		std::vector<float > aesl_tmp_9;
		int aesl_tmp_10;
		int aesl_tmp_11 = 0;

		// read output stream size: "stream_o.imag"
		aesl_fh.read(WRAPC_STREAM_SIZE_OUT_stream_o_imag_V, AESL_token); // [[transaction]]
		aesl_fh.read(WRAPC_STREAM_SIZE_OUT_stream_o_imag_V, AESL_num); // transaction number

		if (atoi(AESL_num.c_str()) == AESL_transaction_pc)
		{
			aesl_fh.read(WRAPC_STREAM_SIZE_OUT_stream_o_imag_V, AESL_token); // pop_size
			aesl_tmp_10 = atoi(AESL_token.c_str());
			aesl_fh.read(WRAPC_STREAM_SIZE_OUT_stream_o_imag_V, AESL_token); // [[/transaction]]
		}

		// output port post check: "stream_o_real_V"
		aesl_fh.read(AUTOTB_TVOUT_PC_stream_o_real_V, AESL_token); // [[transaction]]
		if (AESL_token != "[[transaction]]")
		{
			exit(1);
		}
		aesl_fh.read(AUTOTB_TVOUT_PC_stream_o_real_V, AESL_num); // transaction number

		if (atoi(AESL_num.c_str()) == AESL_transaction_pc)
		{
			aesl_fh.read(AUTOTB_TVOUT_PC_stream_o_real_V, AESL_token); // data

			std::vector<sc_bv<32> > stream_o_real_V_pc_buffer;
			int i = 0;

			while (AESL_token != "[[/transaction]]")
			{
				bool no_x = false;
				bool err = false;

				// search and replace 'X' with "0" from the 1st char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('X');
					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "@W [SIM-201] RTL produces unknown value 'X' on port 'stream_o_real_V', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				no_x = false;

				// search and replace 'x' with "0" from the 3rd char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('x', 2);

					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "@W [SIM-201] RTL produces unknown value 'X' on port 'stream_o_real_V', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				// push token into output port buffer
				if (AESL_token != "")
				{
					stream_o_real_V_pc_buffer.push_back(AESL_token.c_str());
					i++;
				}

				aesl_fh.read(AUTOTB_TVOUT_PC_stream_o_real_V, AESL_token); // data or [[/transaction]]

				if (AESL_token == "[[[/runtime]]]" || aesl_fh.eof(AUTOTB_TVOUT_PC_stream_o_real_V))
				{
					exit(1);
				}
			}

			// correct the buffer size the current transaction
			if (i != aesl_tmp_7)
			{
				aesl_tmp_7 = i;
			}

			if (aesl_tmp_7 > 0 && aesl_tmp_6.size() < aesl_tmp_7)
			{
				int aesl_tmp_6_size = aesl_tmp_6.size();

				for (int tmp_aesl_tmp_6 = 0; tmp_aesl_tmp_6 < aesl_tmp_7 - aesl_tmp_6_size; tmp_aesl_tmp_6++)
				{
					float tmp;
					aesl_tmp_6.push_back(tmp);
				}
			}

			// ***********************************
			if (i > 0)
			{
				// RTL Name: stream_o_real_V
				{
					// bitslice(31, 0)
					// {
						// celement: stream_o.real.V(31, 0)
						// {
							sc_lv<32>* stream_o_real_V_lv0_0_0_1 = new sc_lv<32>[aesl_tmp_7 - aesl_tmp_8];
						// }
					// }

					// bitslice(31, 0)
					{
						int hls_map_index = 0;
						// celement: stream_o.real.V(31, 0)
						{
							// carray: (aesl_tmp_8) => (aesl_tmp_7 - 1) @ (1)
							for (int i_0 = aesl_tmp_8; i_0 <= aesl_tmp_7 - 1; i_0 += 1)
							{
								if (&(aesl_tmp_6[0]) != NULL) // check the null address if the c port is array or others
								{
									stream_o_real_V_lv0_0_0_1[hls_map_index++].range(31, 0) = sc_bv<32>(stream_o_real_V_pc_buffer[hls_map_index].range(31, 0));
								}
							}
						}
					}

					// bitslice(31, 0)
					{
						int hls_map_index = 0;
						// celement: stream_o.real.V(31, 0)
						{
							// carray: (aesl_tmp_8) => (aesl_tmp_7 - 1) @ (1)
							for (int i_0 = aesl_tmp_8; i_0 <= aesl_tmp_7 - 1; i_0 += 1)
							{
								// sub                    : i_0
								// ori_name               : aesl_tmp_6[i_0]
								// sub_1st_elem           : 0
								// ori_name_1st_elem      : aesl_tmp_6[0]
								// output_left_conversion : *(int*)&aesl_tmp_6[i_0]
								// output_type_conversion : (stream_o_real_V_lv0_0_0_1[hls_map_index++]).to_uint64()
								if (&(aesl_tmp_6[0]) != NULL) // check the null address if the c port is array or others
								{
									*(int*)&aesl_tmp_6[i_0] = (stream_o_real_V_lv0_0_0_1[hls_map_index++]).to_uint64();
								}
							}
						}
					}
				}
			}
		}

		// output port post check: "stream_o_imag_V"
		aesl_fh.read(AUTOTB_TVOUT_PC_stream_o_imag_V, AESL_token); // [[transaction]]
		if (AESL_token != "[[transaction]]")
		{
			exit(1);
		}
		aesl_fh.read(AUTOTB_TVOUT_PC_stream_o_imag_V, AESL_num); // transaction number

		if (atoi(AESL_num.c_str()) == AESL_transaction_pc)
		{
			aesl_fh.read(AUTOTB_TVOUT_PC_stream_o_imag_V, AESL_token); // data

			std::vector<sc_bv<32> > stream_o_imag_V_pc_buffer;
			int i = 0;

			while (AESL_token != "[[/transaction]]")
			{
				bool no_x = false;
				bool err = false;

				// search and replace 'X' with "0" from the 1st char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('X');
					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "@W [SIM-201] RTL produces unknown value 'X' on port 'stream_o_imag_V', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				no_x = false;

				// search and replace 'x' with "0" from the 3rd char of token
				while (!no_x)
				{
					size_t x_found = AESL_token.find('x', 2);

					if (x_found != string::npos)
					{
						if (!err)
						{
							cerr << "@W [SIM-201] RTL produces unknown value 'X' on port 'stream_o_imag_V', possible cause: There are uninitialized variables in the C design." << endl;
							err = true;
						}
						AESL_token.replace(x_found, 1, "0");
					}
					else
					{
						no_x = true;
					}
				}

				// push token into output port buffer
				if (AESL_token != "")
				{
					stream_o_imag_V_pc_buffer.push_back(AESL_token.c_str());
					i++;
				}

				aesl_fh.read(AUTOTB_TVOUT_PC_stream_o_imag_V, AESL_token); // data or [[/transaction]]

				if (AESL_token == "[[[/runtime]]]" || aesl_fh.eof(AUTOTB_TVOUT_PC_stream_o_imag_V))
				{
					exit(1);
				}
			}

			// correct the buffer size the current transaction
			if (i != aesl_tmp_10)
			{
				aesl_tmp_10 = i;
			}

			if (aesl_tmp_10 > 0 && aesl_tmp_9.size() < aesl_tmp_10)
			{
				int aesl_tmp_9_size = aesl_tmp_9.size();

				for (int tmp_aesl_tmp_9 = 0; tmp_aesl_tmp_9 < aesl_tmp_10 - aesl_tmp_9_size; tmp_aesl_tmp_9++)
				{
					float tmp;
					aesl_tmp_9.push_back(tmp);
				}
			}

			// ***********************************
			if (i > 0)
			{
				// RTL Name: stream_o_imag_V
				{
					// bitslice(31, 0)
					// {
						// celement: stream_o.imag.V(31, 0)
						// {
							sc_lv<32>* stream_o_imag_V_lv0_0_0_1 = new sc_lv<32>[aesl_tmp_10 - aesl_tmp_11];
						// }
					// }

					// bitslice(31, 0)
					{
						int hls_map_index = 0;
						// celement: stream_o.imag.V(31, 0)
						{
							// carray: (aesl_tmp_11) => (aesl_tmp_10 - 1) @ (1)
							for (int i_0 = aesl_tmp_11; i_0 <= aesl_tmp_10 - 1; i_0 += 1)
							{
								if (&(aesl_tmp_9[0]) != NULL) // check the null address if the c port is array or others
								{
									stream_o_imag_V_lv0_0_0_1[hls_map_index++].range(31, 0) = sc_bv<32>(stream_o_imag_V_pc_buffer[hls_map_index].range(31, 0));
								}
							}
						}
					}

					// bitslice(31, 0)
					{
						int hls_map_index = 0;
						// celement: stream_o.imag.V(31, 0)
						{
							// carray: (aesl_tmp_11) => (aesl_tmp_10 - 1) @ (1)
							for (int i_0 = aesl_tmp_11; i_0 <= aesl_tmp_10 - 1; i_0 += 1)
							{
								// sub                    : i_0
								// ori_name               : aesl_tmp_9[i_0]
								// sub_1st_elem           : 0
								// ori_name_1st_elem      : aesl_tmp_9[0]
								// output_left_conversion : *(int*)&aesl_tmp_9[i_0]
								// output_type_conversion : (stream_o_imag_V_lv0_0_0_1[hls_map_index++]).to_uint64()
								if (&(aesl_tmp_9[0]) != NULL) // check the null address if the c port is array or others
								{
									*(int*)&aesl_tmp_9[i_0] = (stream_o_imag_V_lv0_0_0_1[hls_map_index++]).to_uint64();
								}
							}
						}
					}
				}
			}
		}

		// push back output stream: "stream_o.real"
		for (int i = 0; i < aesl_tmp_7; i++)
		{
			stream_o.real.write(aesl_tmp_6[i]);
		}

		// push back output stream: "stream_o.imag"
		for (int i = 0; i < aesl_tmp_10; i++)
		{
			stream_o.imag.write(aesl_tmp_9[i]);
		}

		AESL_transaction_pc++;
	}
	else
	{
		static unsigned AESL_transaction;

		static AESL_FILE_HANDLER aesl_fh;

		// "stream_i_real_V"
		char* tvin_stream_i_real_V = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_stream_i_real_V);
		char* wrapc_stream_size_in_stream_i_real_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_SIZE_IN_stream_i_real_V);
		char* wrapc_stream_ingress_status_stream_i_real_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_INGRESS_STATUS_stream_i_real_V);

		// "stream_i_imag_V"
		char* tvin_stream_i_imag_V = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_stream_i_imag_V);
		char* wrapc_stream_size_in_stream_i_imag_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_SIZE_IN_stream_i_imag_V);
		char* wrapc_stream_ingress_status_stream_i_imag_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_INGRESS_STATUS_stream_i_imag_V);

		// "stream_o_real_V"
		char* tvin_stream_o_real_V = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_stream_o_real_V);
		char* tvout_stream_o_real_V = new char[50];
		aesl_fh.touch(AUTOTB_TVOUT_stream_o_real_V);
		char* wrapc_stream_size_out_stream_o_real_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_SIZE_OUT_stream_o_real_V);
		char* wrapc_stream_egress_status_stream_o_real_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_EGRESS_STATUS_stream_o_real_V);

		// "stream_o_imag_V"
		char* tvin_stream_o_imag_V = new char[50];
		aesl_fh.touch(AUTOTB_TVIN_stream_o_imag_V);
		char* tvout_stream_o_imag_V = new char[50];
		aesl_fh.touch(AUTOTB_TVOUT_stream_o_imag_V);
		char* wrapc_stream_size_out_stream_o_imag_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_SIZE_OUT_stream_o_imag_V);
		char* wrapc_stream_egress_status_stream_o_imag_V = new char[50];
		aesl_fh.touch(WRAPC_STREAM_EGRESS_STATUS_stream_o_imag_V);

		static INTER_TCL_FILE tcl_file(INTER_TCL);
		int leading_zero;

		// dump stream tvin: "stream_i.real"
		std::vector<float > aesl_tmp_0;
		int aesl_tmp_1 = 0;
		while (!stream_i.real.empty())
		{
			aesl_tmp_0.push_back(stream_i.real.read());
			aesl_tmp_1++;
		}

		// dump stream tvin: "stream_i.imag"
		std::vector<float > aesl_tmp_3;
		int aesl_tmp_4 = 0;
		while (!stream_i.imag.empty())
		{
			aesl_tmp_3.push_back(stream_i.imag.read());
			aesl_tmp_4++;
		}

		// dump stream tvin: "stream_o.real"
		std::vector<float > aesl_tmp_6;
		int aesl_tmp_7 = 0;
		while (!stream_o.real.empty())
		{
			aesl_tmp_6.push_back(stream_o.real.read());
			aesl_tmp_7++;
		}

		// dump stream tvin: "stream_o.imag"
		std::vector<float > aesl_tmp_9;
		int aesl_tmp_10 = 0;
		while (!stream_o.imag.empty())
		{
			aesl_tmp_9.push_back(stream_o.imag.read());
			aesl_tmp_10++;
		}

		// push back input stream: "stream_i.real"
		for (int i = 0; i < aesl_tmp_1; i++)
		{
			stream_i.real.write(aesl_tmp_0[i]);
		}

		// push back input stream: "stream_i.imag"
		for (int i = 0; i < aesl_tmp_4; i++)
		{
			stream_i.imag.write(aesl_tmp_3[i]);
		}

		// push back input stream: "stream_o.real"
		for (int i = 0; i < aesl_tmp_7; i++)
		{
			stream_o.real.write(aesl_tmp_6[i]);
		}

		// push back input stream: "stream_o.imag"
		for (int i = 0; i < aesl_tmp_10; i++)
		{
			stream_o.imag.write(aesl_tmp_9[i]);
		}

// [call_c_dut] ---------->

		AESL_ORIG_DUT_dft(stream_i, stream_o);

		// record input size to tv3: "stream_i.real"
		int aesl_tmp_2 = stream_i.real.size();

		// record input size to tv3: "stream_i.imag"
		int aesl_tmp_5 = stream_i.imag.size();

		// pop output stream: "stream_o.real"
		int aesl_tmp_8 = aesl_tmp_7;
		aesl_tmp_7 = 0;
     aesl_tmp_6.clear();
		while (!stream_o.real.empty())
		{
			aesl_tmp_6.push_back(stream_o.real.read());
			aesl_tmp_7++;
		}

		// pop output stream: "stream_o.imag"
		int aesl_tmp_11 = aesl_tmp_10;
		aesl_tmp_10 = 0;
     aesl_tmp_9.clear();
		while (!stream_o.imag.empty())
		{
			aesl_tmp_9.push_back(stream_o.imag.read());
			aesl_tmp_10++;
		}

		// [[transaction]]
		sprintf(tvin_stream_i_real_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_stream_i_real_V, tvin_stream_i_real_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_stream_i_real_V, tvin_stream_i_real_V);

		sc_bv<32>* stream_i_real_V_tvin_wrapc_buffer = new sc_bv<32>[aesl_tmp_1 - aesl_tmp_2];

		// RTL Name: stream_i_real_V
		{
			// bitslice(31, 0)
			{
				int hls_map_index = 0;
				// celement: stream_i.real.V(31, 0)
				{
					// carray: (0) => (aesl_tmp_1 - aesl_tmp_2 - 1) @ (1)
					for (int i_0 = 0; i_0 <= aesl_tmp_1 - aesl_tmp_2 - 1; i_0 += 1)
					{
						// sub                   : i_0
						// ori_name              : aesl_tmp_0[i_0]
						// sub_1st_elem          : 0
						// ori_name_1st_elem     : aesl_tmp_0[0]
						// regulate_c_name       : stream_i_real_V
						// input_type_conversion : *(int*)&aesl_tmp_0[i_0]
						if (&(aesl_tmp_0[0]) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> stream_i_real_V_tmp_mem;
							stream_i_real_V_tmp_mem = *(int*)&aesl_tmp_0[i_0];
							stream_i_real_V_tvin_wrapc_buffer[hls_map_index++].range(31, 0) = stream_i_real_V_tmp_mem.range(31, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2; i++)
		{
			sprintf(tvin_stream_i_real_V, "%s\n", (stream_i_real_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_stream_i_real_V, tvin_stream_i_real_V);
		}

		// dump stream ingress status to file
		sc_int<32> stream_ingress_size_stream_i_real_V = aesl_tmp_1;
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_stream_i_real_V, stream_ingress_size_stream_i_real_V.to_string().c_str());
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_stream_i_real_V, "\n");

		for (int i = 0; i < aesl_tmp_1 - aesl_tmp_2; i++)
		{
			stream_ingress_size_stream_i_real_V--;
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_stream_i_real_V, stream_ingress_size_stream_i_real_V.to_string().c_str());
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_stream_i_real_V, "\n");
		}

		tcl_file.set_num(aesl_tmp_1 - aesl_tmp_2, &tcl_file.stream_i_real_V_depth);
		sprintf(tvin_stream_i_real_V, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_stream_i_real_V, tvin_stream_i_real_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_stream_i_real_V, tvin_stream_i_real_V);

		// release memory allocation
		delete [] stream_i_real_V_tvin_wrapc_buffer;

		// dump stream size
		sprintf(wrapc_stream_size_in_stream_i_real_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_stream_i_real_V, wrapc_stream_size_in_stream_i_real_V);
		sprintf(wrapc_stream_size_in_stream_i_real_V, "%d\n", aesl_tmp_1 - aesl_tmp_2);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_stream_i_real_V, wrapc_stream_size_in_stream_i_real_V);
		sprintf(wrapc_stream_size_in_stream_i_real_V, "[[/transaction]] \n");
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_stream_i_real_V, wrapc_stream_size_in_stream_i_real_V);

		// [[transaction]]
		sprintf(tvin_stream_i_imag_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVIN_stream_i_imag_V, tvin_stream_i_imag_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_stream_i_imag_V, tvin_stream_i_imag_V);

		sc_bv<32>* stream_i_imag_V_tvin_wrapc_buffer = new sc_bv<32>[aesl_tmp_4 - aesl_tmp_5];

		// RTL Name: stream_i_imag_V
		{
			// bitslice(31, 0)
			{
				int hls_map_index = 0;
				// celement: stream_i.imag.V(31, 0)
				{
					// carray: (0) => (aesl_tmp_4 - aesl_tmp_5 - 1) @ (1)
					for (int i_0 = 0; i_0 <= aesl_tmp_4 - aesl_tmp_5 - 1; i_0 += 1)
					{
						// sub                   : i_0
						// ori_name              : aesl_tmp_3[i_0]
						// sub_1st_elem          : 0
						// ori_name_1st_elem     : aesl_tmp_3[0]
						// regulate_c_name       : stream_i_imag_V
						// input_type_conversion : *(int*)&aesl_tmp_3[i_0]
						if (&(aesl_tmp_3[0]) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> stream_i_imag_V_tmp_mem;
							stream_i_imag_V_tmp_mem = *(int*)&aesl_tmp_3[i_0];
							stream_i_imag_V_tvin_wrapc_buffer[hls_map_index++].range(31, 0) = stream_i_imag_V_tmp_mem.range(31, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < aesl_tmp_4 - aesl_tmp_5; i++)
		{
			sprintf(tvin_stream_i_imag_V, "%s\n", (stream_i_imag_V_tvin_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVIN_stream_i_imag_V, tvin_stream_i_imag_V);
		}

		// dump stream ingress status to file
		sc_int<32> stream_ingress_size_stream_i_imag_V = aesl_tmp_4;
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_stream_i_imag_V, stream_ingress_size_stream_i_imag_V.to_string().c_str());
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_stream_i_imag_V, "\n");

		for (int i = 0; i < aesl_tmp_4 - aesl_tmp_5; i++)
		{
			stream_ingress_size_stream_i_imag_V--;
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_stream_i_imag_V, stream_ingress_size_stream_i_imag_V.to_string().c_str());
			aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_stream_i_imag_V, "\n");
		}

		tcl_file.set_num(aesl_tmp_4 - aesl_tmp_5, &tcl_file.stream_i_imag_V_depth);
		sprintf(tvin_stream_i_imag_V, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVIN_stream_i_imag_V, tvin_stream_i_imag_V);
		aesl_fh.write(WRAPC_STREAM_INGRESS_STATUS_stream_i_imag_V, tvin_stream_i_imag_V);

		// release memory allocation
		delete [] stream_i_imag_V_tvin_wrapc_buffer;

		// dump stream size
		sprintf(wrapc_stream_size_in_stream_i_imag_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_stream_i_imag_V, wrapc_stream_size_in_stream_i_imag_V);
		sprintf(wrapc_stream_size_in_stream_i_imag_V, "%d\n", aesl_tmp_4 - aesl_tmp_5);
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_stream_i_imag_V, wrapc_stream_size_in_stream_i_imag_V);
		sprintf(wrapc_stream_size_in_stream_i_imag_V, "[[/transaction]] \n");
		aesl_fh.write(WRAPC_STREAM_SIZE_IN_stream_i_imag_V, wrapc_stream_size_in_stream_i_imag_V);

		// [[transaction]]
		sprintf(tvout_stream_o_real_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVOUT_stream_o_real_V, tvout_stream_o_real_V);

		sc_bv<32>* stream_o_real_V_tvout_wrapc_buffer = new sc_bv<32>[aesl_tmp_7 - aesl_tmp_8];

		// RTL Name: stream_o_real_V
		{
			// bitslice(31, 0)
			{
				int hls_map_index = 0;
				// celement: stream_o.real.V(31, 0)
				{
					// carray: (aesl_tmp_8) => (aesl_tmp_7 - 1) @ (1)
					for (int i_0 = aesl_tmp_8; i_0 <= aesl_tmp_7 - 1; i_0 += 1)
					{
						// sub                   : i_0
						// ori_name              : aesl_tmp_6[i_0]
						// sub_1st_elem          : 0
						// ori_name_1st_elem     : aesl_tmp_6[0]
						// regulate_c_name       : stream_o_real_V
						// input_type_conversion : *(int*)&aesl_tmp_6[i_0]
						if (&(aesl_tmp_6[0]) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> stream_o_real_V_tmp_mem;
							stream_o_real_V_tmp_mem = *(int*)&aesl_tmp_6[i_0];
							stream_o_real_V_tvout_wrapc_buffer[hls_map_index++].range(31, 0) = stream_o_real_V_tmp_mem.range(31, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < aesl_tmp_7 - aesl_tmp_8; i++)
		{
			sprintf(tvout_stream_o_real_V, "%s\n", (stream_o_real_V_tvout_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVOUT_stream_o_real_V, tvout_stream_o_real_V);
		}

		tcl_file.set_num(aesl_tmp_7 - aesl_tmp_8, &tcl_file.stream_o_real_V_depth);
		sprintf(tvout_stream_o_real_V, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVOUT_stream_o_real_V, tvout_stream_o_real_V);

		// release memory allocation
		delete [] stream_o_real_V_tvout_wrapc_buffer;

		// dump stream size
		sprintf(wrapc_stream_size_out_stream_o_real_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(WRAPC_STREAM_SIZE_OUT_stream_o_real_V, wrapc_stream_size_out_stream_o_real_V);
		sprintf(wrapc_stream_size_out_stream_o_real_V, "%d\n", aesl_tmp_7 - aesl_tmp_8);
		aesl_fh.write(WRAPC_STREAM_SIZE_OUT_stream_o_real_V, wrapc_stream_size_out_stream_o_real_V);
		sprintf(wrapc_stream_size_out_stream_o_real_V, "[[/transaction]] \n");
		aesl_fh.write(WRAPC_STREAM_SIZE_OUT_stream_o_real_V, wrapc_stream_size_out_stream_o_real_V);

		// [[transaction]]
		sprintf(tvout_stream_o_imag_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(AUTOTB_TVOUT_stream_o_imag_V, tvout_stream_o_imag_V);

		sc_bv<32>* stream_o_imag_V_tvout_wrapc_buffer = new sc_bv<32>[aesl_tmp_10 - aesl_tmp_11];

		// RTL Name: stream_o_imag_V
		{
			// bitslice(31, 0)
			{
				int hls_map_index = 0;
				// celement: stream_o.imag.V(31, 0)
				{
					// carray: (aesl_tmp_11) => (aesl_tmp_10 - 1) @ (1)
					for (int i_0 = aesl_tmp_11; i_0 <= aesl_tmp_10 - 1; i_0 += 1)
					{
						// sub                   : i_0
						// ori_name              : aesl_tmp_9[i_0]
						// sub_1st_elem          : 0
						// ori_name_1st_elem     : aesl_tmp_9[0]
						// regulate_c_name       : stream_o_imag_V
						// input_type_conversion : *(int*)&aesl_tmp_9[i_0]
						if (&(aesl_tmp_9[0]) != NULL) // check the null address if the c port is array or others
						{
							sc_lv<32> stream_o_imag_V_tmp_mem;
							stream_o_imag_V_tmp_mem = *(int*)&aesl_tmp_9[i_0];
							stream_o_imag_V_tvout_wrapc_buffer[hls_map_index++].range(31, 0) = stream_o_imag_V_tmp_mem.range(31, 0);
						}
					}
				}
			}
		}

		// dump tv to file
		for (int i = 0; i < aesl_tmp_10 - aesl_tmp_11; i++)
		{
			sprintf(tvout_stream_o_imag_V, "%s\n", (stream_o_imag_V_tvout_wrapc_buffer[i]).to_string(SC_HEX).c_str());
			aesl_fh.write(AUTOTB_TVOUT_stream_o_imag_V, tvout_stream_o_imag_V);
		}

		tcl_file.set_num(aesl_tmp_10 - aesl_tmp_11, &tcl_file.stream_o_imag_V_depth);
		sprintf(tvout_stream_o_imag_V, "[[/transaction]] \n");
		aesl_fh.write(AUTOTB_TVOUT_stream_o_imag_V, tvout_stream_o_imag_V);

		// release memory allocation
		delete [] stream_o_imag_V_tvout_wrapc_buffer;

		// dump stream size
		sprintf(wrapc_stream_size_out_stream_o_imag_V, "[[transaction]] %d\n", AESL_transaction);
		aesl_fh.write(WRAPC_STREAM_SIZE_OUT_stream_o_imag_V, wrapc_stream_size_out_stream_o_imag_V);
		sprintf(wrapc_stream_size_out_stream_o_imag_V, "%d\n", aesl_tmp_10 - aesl_tmp_11);
		aesl_fh.write(WRAPC_STREAM_SIZE_OUT_stream_o_imag_V, wrapc_stream_size_out_stream_o_imag_V);
		sprintf(wrapc_stream_size_out_stream_o_imag_V, "[[/transaction]] \n");
		aesl_fh.write(WRAPC_STREAM_SIZE_OUT_stream_o_imag_V, wrapc_stream_size_out_stream_o_imag_V);

		// push back output stream: "stream_o.real"
		for (int i = 0; i < aesl_tmp_7; i++)
		{
			stream_o.real.write(aesl_tmp_6[i]);
		}

		// push back output stream: "stream_o.imag"
		for (int i = 0; i < aesl_tmp_10; i++)
		{
			stream_o.imag.write(aesl_tmp_9[i]);
		}

		// release memory allocation: "stream_i_real_V"
		delete [] tvin_stream_i_real_V;
		delete [] wrapc_stream_size_in_stream_i_real_V;
		// release memory allocation: "stream_i_imag_V"
		delete [] tvin_stream_i_imag_V;
		delete [] wrapc_stream_size_in_stream_i_imag_V;
		// release memory allocation: "stream_o_real_V"
		delete [] tvout_stream_o_real_V;
		delete [] tvin_stream_o_real_V;
		delete [] wrapc_stream_size_out_stream_o_real_V;
		// release memory allocation: "stream_o_imag_V"
		delete [] tvout_stream_o_imag_V;
		delete [] tvin_stream_o_imag_V;
		delete [] wrapc_stream_size_out_stream_o_imag_V;

		AESL_transaction++;

		tcl_file.set_num(AESL_transaction , &tcl_file.trans_num);
	}
}

