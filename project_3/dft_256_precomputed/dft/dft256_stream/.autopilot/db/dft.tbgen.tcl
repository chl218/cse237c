set C_TypeInfoList {{ 
"dft" : [[], { "return": [[], "void"]} , [{"ExternC" : 0}], [ {"stream_i": [[], {"reference": "0"}] }, {"stream_o": [[], {"reference": "0"}] }],["1","2"],""],
 "1": [ "real_i", [[], {"array": [ {"scalar": "float"}, [256]]}],""],
 "2": [ "imag_i", [[], {"array": [ {"scalar": "float"}, [256]]}],""], 
"0": [ "DTYPE", {"typedef": [[[],"3"],""]}], 
"3": [ "", {"struct": [[],[],[{ "real": [[], "4"]},{ "imag": [[], "4"]}],""]}], 
"4": [ "stream<float>", {"hls_type": {"stream": [[[[], {"scalar": "float"}]],"5"]}}],
"5": ["hls", ""]
}}
set moduleName dft
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {dft}
set C_modelType { void 0 }
set C_modelArgList { 
	{ stream_i_real_V float 32 regular {fifo 0 volatile }  }
	{ stream_i_imag_V float 32 regular {fifo 0 volatile }  }
	{ stream_o_real_V float 32 regular {fifo 1 volatile }  }
	{ stream_o_imag_V float 32 regular {fifo 1 volatile }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "stream_i_real_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "stream_i.real.V","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "stream_i_imag_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "stream_i.imag.V","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "stream_o_real_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "stream_o.real.V","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "stream_o_imag_V", "interface" : "fifo", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "stream_o.imag.V","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} ]}
# RTL Port declarations: 
set portNum 18
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ stream_i_real_V_dout sc_in sc_lv 32 signal 0 } 
	{ stream_i_real_V_empty_n sc_in sc_logic 1 signal 0 } 
	{ stream_i_real_V_read sc_out sc_logic 1 signal 0 } 
	{ stream_i_imag_V_dout sc_in sc_lv 32 signal 1 } 
	{ stream_i_imag_V_empty_n sc_in sc_logic 1 signal 1 } 
	{ stream_i_imag_V_read sc_out sc_logic 1 signal 1 } 
	{ stream_o_real_V_din sc_out sc_lv 32 signal 2 } 
	{ stream_o_real_V_full_n sc_in sc_logic 1 signal 2 } 
	{ stream_o_real_V_write sc_out sc_logic 1 signal 2 } 
	{ stream_o_imag_V_din sc_out sc_lv 32 signal 3 } 
	{ stream_o_imag_V_full_n sc_in sc_logic 1 signal 3 } 
	{ stream_o_imag_V_write sc_out sc_logic 1 signal 3 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "stream_i_real_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "stream_i_real_V", "role": "dout" }} , 
 	{ "name": "stream_i_real_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "stream_i_real_V", "role": "empty_n" }} , 
 	{ "name": "stream_i_real_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "stream_i_real_V", "role": "read" }} , 
 	{ "name": "stream_i_imag_V_dout", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "stream_i_imag_V", "role": "dout" }} , 
 	{ "name": "stream_i_imag_V_empty_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "stream_i_imag_V", "role": "empty_n" }} , 
 	{ "name": "stream_i_imag_V_read", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "stream_i_imag_V", "role": "read" }} , 
 	{ "name": "stream_o_real_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "stream_o_real_V", "role": "din" }} , 
 	{ "name": "stream_o_real_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "stream_o_real_V", "role": "full_n" }} , 
 	{ "name": "stream_o_real_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "stream_o_real_V", "role": "write" }} , 
 	{ "name": "stream_o_imag_V_din", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "stream_o_imag_V", "role": "din" }} , 
 	{ "name": "stream_o_imag_V_full_n", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "stream_o_imag_V", "role": "full_n" }} , 
 	{ "name": "stream_o_imag_V_write", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "stream_o_imag_V", "role": "write" }}  ]}
set Spec2ImplPortList { 
	stream_i_real_V { ap_fifo {  { stream_i_real_V_dout fifo_data 0 32 }  { stream_i_real_V_empty_n fifo_status 0 1 }  { stream_i_real_V_read fifo_update 1 1 } } }
	stream_i_imag_V { ap_fifo {  { stream_i_imag_V_dout fifo_data 0 32 }  { stream_i_imag_V_empty_n fifo_status 0 1 }  { stream_i_imag_V_read fifo_update 1 1 } } }
	stream_o_real_V { ap_fifo {  { stream_o_real_V_din fifo_data 1 32 }  { stream_o_real_V_full_n fifo_status 0 1 }  { stream_o_real_V_write fifo_update 1 1 } } }
	stream_o_imag_V { ap_fifo {  { stream_o_imag_V_din fifo_data 1 32 }  { stream_o_imag_V_full_n fifo_status 0 1 }  { stream_o_imag_V_write fifo_update 1 1 } } }
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
	stream_i_real_V { fifo_read 256 no_conditional }
	stream_i_imag_V { fifo_read 256 no_conditional }
	stream_o_real_V { fifo_write 256 no_conditional }
	stream_o_imag_V { fifo_write 256 no_conditional }
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
