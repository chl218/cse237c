# This script segment is generated automatically by AutoPilot

set id 0
set name dft_faddfsub_32ns_32ns_32_5_full_dsp
set corename simcore_faddfsub
set op faddfsub
set stage_num 5
set max_latency -1
set registered_input 1
set impl_style full_dsp
set in0_width 32
set in0_signed 0
set in1_width 32
set in1_signed 0
set out_width 32
if {${::AESL::PGuard_simmodel_gen}} {
if {[info proc ap_gen_simcore_faddfsub] == "ap_gen_simcore_faddfsub"} {
eval "ap_gen_simcore_faddfsub { \
    id ${id} \
    name ${name} \
    corename ${corename} \
    op ${op} \
    reset_level 1 \
    sync_rst true \
    stage_num ${stage_num} \
    max_latency ${max_latency} \
    registered_input ${registered_input} \
    style ${impl_style} \
    in0_width ${in0_width} \
    in0_signed ${in0_signed} \
    in1_width ${in1_width} \
    in1_signed ${in1_signed} \
    out_width ${out_width} \
}"
} else {
puts "@W \[IMPL-100\] Cannot find ap_gen_simcore_faddfsub, check your AutoPilot builtin lib"
}
}


if {${::AESL::PGuard_rtl_comp_handler}} {
	::AP::rtl_comp_handler ${name}
}


set op faddfsub
set corename FAddSub
if {${::AESL::PGuard_autocg_gen} && (${::AESL::PGuard_autocg_fpip} || ${::AESL::PGuard_autocg_fpv6en} || ${::AESL::PGuard_autocg_hpen})} {
if {[info proc ::AESL_LIB_XILINX_FPV6::fpv6_gen] == "::AESL_LIB_XILINX_FPV6::fpv6_gen"} {
eval "::AESL_LIB_XILINX_FPV6::fpv6_gen { \
    id ${id} \
    name ${name} \
    corename ${corename} \
    op ${op} \
    reset_level 1 \
    sync_rst true \
    stage_num ${stage_num} \
    max_latency ${max_latency} \
    registered_input ${registered_input} \
    style ${impl_style} \
    in0_width ${in0_width} \
    in0_signed ${in0_signed} \
    in1_width ${in1_width} \
    in1_signed ${in1_signed} \
    out_width ${out_width} \
}"
} else {
puts "@W \[IMPL-101\] Cannot find ::AESL_LIB_XILINX_FPV6::fpv6_gen, check your platform lib"
}
}


set id 1
set name dft_fadd_32ns_32ns_32_5_full_dsp
set corename simcore_fadd
set op fadd
set stage_num 5
set max_latency -1
set registered_input 1
set impl_style full_dsp
set in0_width 32
set in0_signed 0
set in1_width 32
set in1_signed 0
set out_width 32
if {${::AESL::PGuard_simmodel_gen}} {
if {[info proc ap_gen_simcore_fadd] == "ap_gen_simcore_fadd"} {
eval "ap_gen_simcore_fadd { \
    id ${id} \
    name ${name} \
    corename ${corename} \
    op ${op} \
    reset_level 1 \
    sync_rst true \
    stage_num ${stage_num} \
    max_latency ${max_latency} \
    registered_input ${registered_input} \
    style ${impl_style} \
    in0_width ${in0_width} \
    in0_signed ${in0_signed} \
    in1_width ${in1_width} \
    in1_signed ${in1_signed} \
    out_width ${out_width} \
}"
} else {
puts "@W \[IMPL-100\] Cannot find ap_gen_simcore_fadd, check your AutoPilot builtin lib"
}
}


if {${::AESL::PGuard_rtl_comp_handler}} {
	::AP::rtl_comp_handler ${name}
}


set op fadd
set corename FAddSub
if {${::AESL::PGuard_autocg_gen} && (${::AESL::PGuard_autocg_fpip} || ${::AESL::PGuard_autocg_fpv6en} || ${::AESL::PGuard_autocg_hpen})} {
if {[info proc ::AESL_LIB_XILINX_FPV6::fpv6_gen] == "::AESL_LIB_XILINX_FPV6::fpv6_gen"} {
eval "::AESL_LIB_XILINX_FPV6::fpv6_gen { \
    id ${id} \
    name ${name} \
    corename ${corename} \
    op ${op} \
    reset_level 1 \
    sync_rst true \
    stage_num ${stage_num} \
    max_latency ${max_latency} \
    registered_input ${registered_input} \
    style ${impl_style} \
    in0_width ${in0_width} \
    in0_signed ${in0_signed} \
    in1_width ${in1_width} \
    in1_signed ${in1_signed} \
    out_width ${out_width} \
}"
} else {
puts "@W \[IMPL-101\] Cannot find ::AESL_LIB_XILINX_FPV6::fpv6_gen, check your platform lib"
}
}


set id 2
set name dft_fmul_32ns_32ns_32_4_max_dsp
set corename simcore_fmul
set op fmul
set stage_num 4
set max_latency -1
set registered_input 1
set impl_style max_dsp
set in0_width 32
set in0_signed 0
set in1_width 32
set in1_signed 0
set out_width 32
if {${::AESL::PGuard_simmodel_gen}} {
if {[info proc ap_gen_simcore_fmul] == "ap_gen_simcore_fmul"} {
eval "ap_gen_simcore_fmul { \
    id ${id} \
    name ${name} \
    corename ${corename} \
    op ${op} \
    reset_level 1 \
    sync_rst true \
    stage_num ${stage_num} \
    max_latency ${max_latency} \
    registered_input ${registered_input} \
    style ${impl_style} \
    in0_width ${in0_width} \
    in0_signed ${in0_signed} \
    in1_width ${in1_width} \
    in1_signed ${in1_signed} \
    out_width ${out_width} \
}"
} else {
puts "@W \[IMPL-100\] Cannot find ap_gen_simcore_fmul, check your AutoPilot builtin lib"
}
}


if {${::AESL::PGuard_rtl_comp_handler}} {
	::AP::rtl_comp_handler ${name}
}


set op fmul
set corename FMul
if {${::AESL::PGuard_autocg_gen} && (${::AESL::PGuard_autocg_fpip} || ${::AESL::PGuard_autocg_fpv6en} || ${::AESL::PGuard_autocg_hpen})} {
if {[info proc ::AESL_LIB_XILINX_FPV6::fpv6_gen] == "::AESL_LIB_XILINX_FPV6::fpv6_gen"} {
eval "::AESL_LIB_XILINX_FPV6::fpv6_gen { \
    id ${id} \
    name ${name} \
    corename ${corename} \
    op ${op} \
    reset_level 1 \
    sync_rst true \
    stage_num ${stage_num} \
    max_latency ${max_latency} \
    registered_input ${registered_input} \
    style ${impl_style} \
    in0_width ${in0_width} \
    in0_signed ${in0_signed} \
    in1_width ${in1_width} \
    in1_signed ${in1_signed} \
    out_width ${out_width} \
}"
} else {
puts "@W \[IMPL-101\] Cannot find ::AESL_LIB_XILINX_FPV6::fpv6_gen, check your platform lib"
}
}


set id 6
set name dft_sitofp_32s_32_6
set corename simcore_sitofp
set op sitofp
set stage_num 6
set max_latency -1
set registered_input 1
set in0_width 32
set in0_signed 1
set out_width 32
if {${::AESL::PGuard_simmodel_gen}} {
if {[info proc ap_gen_simcore_sitofp] == "ap_gen_simcore_sitofp"} {
eval "ap_gen_simcore_sitofp { \
    id ${id} \
    name ${name} \
    corename ${corename} \
    op ${op} \
    reset_level 1 \
    sync_rst true \
    stage_num ${stage_num} \
    max_latency ${max_latency} \
    registered_input ${registered_input} \
    in0_width ${in0_width} \
    in0_signed ${in0_signed} \
    out_width ${out_width} \
}"
} else {
puts "@W \[IMPL-100\] Cannot find ap_gen_simcore_sitofp, check your AutoPilot builtin lib"
}
}


if {${::AESL::PGuard_rtl_comp_handler}} {
	::AP::rtl_comp_handler ${name}
}


set op sitofp
set corename Int2Float
if {${::AESL::PGuard_autocg_gen} && (${::AESL::PGuard_autocg_fpip} || ${::AESL::PGuard_autocg_fpv6en} || ${::AESL::PGuard_autocg_hpen})} {
if {[info proc ::AESL_LIB_XILINX_FPV6::fpv6_gen] == "::AESL_LIB_XILINX_FPV6::fpv6_gen"} {
eval "::AESL_LIB_XILINX_FPV6::fpv6_gen { \
    id ${id} \
    name ${name} \
    corename ${corename} \
    op ${op} \
    reset_level 1 \
    sync_rst true \
    stage_num ${stage_num} \
    max_latency ${max_latency} \
    registered_input ${registered_input} \
    in0_width ${in0_width} \
    in0_signed ${in0_signed} \
    out_width ${out_width} \
}"
} else {
puts "@W \[IMPL-101\] Cannot find ::AESL_LIB_XILINX_FPV6::fpv6_gen, check your platform lib"
}
}


# Memory (RAM/ROM)  definition:
set ID 8
set MemName dft_cct_V
set CoreName ap_simcore_mem
set PortList { 1 }
set DataWd 20
set AddrRange 256
set AddrWd 8
set TrueReset 0
set IsROM 1
set ROMData { "01000000000000000000" "00111111111110110001" "00111111111011000100" "00111111110100111001" "00111111101100010001" "00111111100001001100" "00111111010011101010" "00111111000011101100" "00111110110001010010" "00111110011100011110" "00111110000101001111" "00111101101011101000" "00111101001111101000" "00111100110001010001" "00111100010000100100" "00111011101101100010" "00111011001000001101" "00111010100000100110" "00111001110110101111" "00111001001010101001" "00111000011100010110" "00110111101011111000" "00110110111001010000" "00110110000100100001" "00110101001101101100" "00110100010100110101" "00110011011001111100" "00110010011101000100" "00110001011110001111" "00110000011101100001" "00101111011010111011" "00101110010110100001" "00101101010000010011" "00101100001000010111" "00101010111110101101" "00101001110011011001" "00101000100110011110" "00100111010111111111" "00100110000111111110" "00100100110110100000" "00100011100011100111" "00100010001111010110" "00100000111001110001" "00011111100010111010" "00011110001010110101" "00011100110001100110" "00011011010111010000" "00011001111011110111" "00011000011111011110" "00010111000010001000" "00010101100011111001" "00010100000100110101" "00010010100101000000" "00010001000100011101" "00001111100011001111" "00001110000001011100" "00001100011111000101" "00001010111100010000" "00001001011001000000" "00000111110101011001" "00000110010001011110" "00000100101101010100" "00000011001000111110" "00000001100100100001" "00000000000000000000" "11111110011011011110" "11111100110111000001" "11111011010010101011" "11111001101110100001" "11111000001010100110" "11110110100110111111" "11110101000011101111" "11110011100000111010" "11110001111110100011" "11110000011100110000" "11101110111011100010" "11101101011010111111" "11101011111011001010" "11101010011100000110" "11101000111101110111" "11100111100000100001" "11100110000100001000" "11100100101000101111" "11100011001110011001" "11100001110101001010" "11100000011101000101" "11011111000110001110" "11011101110000101001" "11011100011100011000" "11011011001001011111" "11011001111000000001" "11011000101000000000" "11010111011001100001" "11010110001100100110" "11010101000001010010" "11010011110111101000" "11010010101111101100" "11010001101001011110" "11010000100101000100" "11001111100010011110" "11001110100001110000" "11001101100010111011" "11001100100110000011" "11001011101011001010" "11001010110010010011" "11001001111011011110" "11001001000110101111" "11001000010100000111" "11000111100011101001" "11000110110101010110" "11000110001001010000" "11000101011111011001" "11000100110111110010" "11000100010010011101" "11000011101111011011" "11000011001110101110" "11000010110000010111" "11000010010100010111" "11000001111010110000" "11000001100011100001" "11000001001110101101" "11000000111100010011" "11000000101100010101" "11000000011110110011" "11000000010011101110" "11000000001011000110" "11000000000100111011" "11000000000001001110" "11000000000000000000" "11000000000001001110" "11000000000100111011" "11000000001011000110" "11000000010011101110" "11000000011110110011" "11000000101100010101" "11000000111100010011" "11000001001110101101" "11000001100011100001" "11000001111010110000" "11000010010100010111" "11000010110000010111" "11000011001110101110" "11000011101111011011" "11000100010010011101" "11000100110111110010" "11000101011111011001" "11000110001001010000" "11000110110101010110" "11000111100011101001" "11001000010100000111" "11001001000110101111" "11001001111011011110" "11001010110010010011" "11001011101011001010" "11001100100110000011" "11001101100010111011" "11001110100001110000" "11001111100010011110" "11010000100101000100" "11010001101001011110" "11010010101111101100" "11010011110111101000" "11010101000001010010" "11010110001100100110" "11010111011001100001" "11011000101000000000" "11011001111000000001" "11011011001001011111" "11011100011100011000" "11011101110000101001" "11011111000110001110" "11100000011101000101" "11100001110101001010" "11100011001110011001" "11100100101000101111" "11100110000100001000" "11100111100000100001" "11101000111101110111" "11101010011100000110" "11101011111011001010" "11101101011010111111" "11101110111011100010" "11110000011100110000" "11110001111110100011" "11110011100000111010" "11110101000011101111" "11110110100110111111" "11111000001010100110" "11111001101110100001" "11111011010010101011" "11111100110111000001" "11111110011011011110" "00000000000000000000" "00000001100100100001" "00000011001000111110" "00000100101101010100" "00000110010001011110" "00000111110101011001" "00001001011001000000" "00001010111100010000" "00001100011111000101" "00001110000001011100" "00001111100011001111" "00010001000100011101" "00010010100101000000" "00010100000100110101" "00010101100011111001" "00010111000010001000" "00011000011111011110" "00011001111011110111" "00011011010111010000" "00011100110001100110" "00011110001010110101" "00011111100010111010" "00100000111001110001" "00100010001111010110" "00100011100011100111" "00100100110110100000" "00100110000111111110" "00100111010111111111" "00101000100110011110" "00101001110011011001" "00101010111110101101" "00101100001000010111" "00101101010000010011" "00101110010110100001" "00101111011010111011" "00110000011101100001" "00110001011110001111" "00110010011101000100" "00110011011001111100" "00110100010100110101" "00110101001101101100" "00110110000100100001" "00110110111001010000" "00110111101011111000" "00111000011100010110" "00111001001010101001" "00111001110110101111" "00111010100000100110" "00111011001000001101" "00111011101101100010" "00111100010000100100" "00111100110001010001" "00111101001111101000" "00111101101011101000" "00111110000101001111" "00111110011100011110" "00111110110001010010" "00111111000011101100" "00111111010011101010" "00111111100001001100" "00111111101100010001" "00111111110100111001" "00111111111011000100" "00111111111110110001" }
set HasInitializer 1
set Initializer $ROMData
set NumOfStage 2
set MaxLatency -1
set DelayBudget 2.39
set ClkPeriod 10
set RegisteredInput 0
if {${::AESL::PGuard_simmodel_gen}} {
if {[info proc ap_gen_simcore_mem] == "ap_gen_simcore_mem"} {
    eval "ap_gen_simcore_mem { \
    id ${ID} \
    name ${MemName} \
    corename ${CoreName}  \
    op mem \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage}  \
    registered_input ${RegisteredInput} \
    port_num 1 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    true_reset ${TrueReset} \
    delay_budget ${DelayBudget} \
    clk_period ${ClkPeriod} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
 } "
} else {
    puts "@W \[IMPL-102\] Cannot find ap_gen_simcore_mem, check your platform lib"
}
}


if {${::AESL::PGuard_rtl_comp_handler}} {
  ::AP::rtl_comp_handler $MemName
}


set CoreName ROM_nP
if {${::AESL::PGuard_autocg_gen} && ${::AESL::PGuard_autocg_ipmgen}} {
if {[info proc ::AESL_LIB_VIRTEX::xil_gen_ROM] == "::AESL_LIB_VIRTEX::xil_gen_ROM"} {
    eval "::AESL_LIB_VIRTEX::xil_gen_ROM { \
    id ${ID} \
    name ${MemName} \
    corename ${CoreName}  \
    op mem \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage}  \
    registered_input ${RegisteredInput} \
    port_num 1 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    true_reset ${TrueReset} \
    delay_budget ${DelayBudget} \
    clk_period ${ClkPeriod} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
 } "
  } else {
    puts "@W \[IMPL-104\] Cannot find ::AESL_LIB_VIRTEX::xil_gen_ROM, check your platform lib"
  }
}


# Memory (RAM/ROM)  definition:
set ID 9
set MemName dft_sct_V
set CoreName ap_simcore_mem
set PortList { 1 }
set DataWd 20
set AddrRange 256
set AddrWd 8
set TrueReset 0
set IsROM 1
set ROMData { "00000000000000000000" "11111110011011011110" "11111100110111000001" "11111011010010101011" "11111001101110100001" "11111000001010100110" "11110110100110111111" "11110101000011101111" "11110011100000111010" "11110001111110100011" "11110000011100110000" "11101110111011100010" "11101101011010111111" "11101011111011001010" "11101010011100000110" "11101000111101110111" "11100111100000100001" "11100110000100001000" "11100100101000101111" "11100011001110011001" "11100001110101001010" "11100000011101000101" "11011111000110001110" "11011101110000101001" "11011100011100011000" "11011011001001011111" "11011001111000000001" "11011000101000000000" "11010111011001100001" "11010110001100100110" "11010101000001010010" "11010011110111101000" "11010010101111101100" "11010001101001011110" "11010000100101000100" "11001111100010011110" "11001110100001110000" "11001101100010111011" "11001100100110000011" "11001011101011001010" "11001010110010010011" "11001001111011011110" "11001001000110101111" "11001000010100000111" "11000111100011101001" "11000110110101010110" "11000110001001010000" "11000101011111011001" "11000100110111110010" "11000100010010011101" "11000011101111011011" "11000011001110101110" "11000010110000010111" "11000010010100010111" "11000001111010110000" "11000001100011100001" "11000001001110101101" "11000000111100010011" "11000000101100010101" "11000000011110110011" "11000000010011101110" "11000000001011000110" "11000000000100111011" "11000000000001001110" "11000000000000000000" "11000000000001001110" "11000000000100111011" "11000000001011000110" "11000000010011101110" "11000000011110110011" "11000000101100010101" "11000000111100010011" "11000001001110101101" "11000001100011100001" "11000001111010110000" "11000010010100010111" "11000010110000010111" "11000011001110101110" "11000011101111011011" "11000100010010011101" "11000100110111110010" "11000101011111011001" "11000110001001010000" "11000110110101010110" "11000111100011101001" "11001000010100000111" "11001001000110101111" "11001001111011011110" "11001010110010010011" "11001011101011001010" "11001100100110000011" "11001101100010111011" "11001110100001110000" "11001111100010011110" "11010000100101000100" "11010001101001011110" "11010010101111101100" "11010011110111101000" "11010101000001010010" "11010110001100100110" "11010111011001100001" "11011000101000000000" "11011001111000000001" "11011011001001011111" "11011100011100011000" "11011101110000101001" "11011111000110001110" "11100000011101000101" "11100001110101001010" "11100011001110011001" "11100100101000101111" "11100110000100001000" "11100111100000100001" "11101000111101110111" "11101010011100000110" "11101011111011001010" "11101101011010111111" "11101110111011100010" "11110000011100110000" "11110001111110100011" "11110011100000111010" "11110101000011101111" "11110110100110111111" "11111000001010100110" "11111001101110100001" "11111011010010101011" "11111100110111000001" "11111110011011011110" "00000000000000000000" "00000001100100100001" "00000011001000111110" "00000100101101010100" "00000110010001011110" "00000111110101011001" "00001001011001000000" "00001010111100010000" "00001100011111000101" "00001110000001011100" "00001111100011001111" "00010001000100011101" "00010010100101000000" "00010100000100110101" "00010101100011111001" "00010111000010001000" "00011000011111011110" "00011001111011110111" "00011011010111010000" "00011100110001100110" "00011110001010110101" "00011111100010111010" "00100000111001110001" "00100010001111010110" "00100011100011100111" "00100100110110100000" "00100110000111111110" "00100111010111111111" "00101000100110011110" "00101001110011011001" "00101010111110101101" "00101100001000010111" "00101101010000010011" "00101110010110100001" "00101111011010111011" "00110000011101100001" "00110001011110001111" "00110010011101000100" "00110011011001111100" "00110100010100110101" "00110101001101101100" "00110110000100100001" "00110110111001010000" "00110111101011111000" "00111000011100010110" "00111001001010101001" "00111001110110101111" "00111010100000100110" "00111011001000001101" "00111011101101100010" "00111100010000100100" "00111100110001010001" "00111101001111101000" "00111101101011101000" "00111110000101001111" "00111110011100011110" "00111110110001010010" "00111111000011101100" "00111111010011101010" "00111111100001001100" "00111111101100010001" "00111111110100111001" "00111111111011000100" "00111111111110110001" "01000000000000000000" "00111111111110110001" "00111111111011000100" "00111111110100111001" "00111111101100010001" "00111111100001001100" "00111111010011101010" "00111111000011101100" "00111110110001010010" "00111110011100011110" "00111110000101001111" "00111101101011101000" "00111101001111101000" "00111100110001010001" "00111100010000100100" "00111011101101100010" "00111011001000001101" "00111010100000100110" "00111001110110101111" "00111001001010101001" "00111000011100010110" "00110111101011111000" "00110110111001010000" "00110110000100100001" "00110101001101101100" "00110100010100110101" "00110011011001111100" "00110010011101000100" "00110001011110001111" "00110000011101100001" "00101111011010111011" "00101110010110100001" "00101101010000010011" "00101100001000010111" "00101010111110101101" "00101001110011011001" "00101000100110011110" "00100111010111111111" "00100110000111111110" "00100100110110100000" "00100011100011100111" "00100010001111010110" "00100000111001110001" "00011111100010111010" "00011110001010110101" "00011100110001100110" "00011011010111010000" "00011001111011110111" "00011000011111011110" "00010111000010001000" "00010101100011111001" "00010100000100110101" "00010010100101000000" "00010001000100011101" "00001111100011001111" "00001110000001011100" "00001100011111000101" "00001010111100010000" "00001001011001000000" "00000111110101011001" "00000110010001011110" "00000100101101010100" "00000011001000111110" "00000001100100100001" }
set HasInitializer 1
set Initializer $ROMData
set NumOfStage 2
set MaxLatency -1
set DelayBudget 2.39
set ClkPeriod 10
set RegisteredInput 0
if {${::AESL::PGuard_simmodel_gen}} {
if {[info proc ap_gen_simcore_mem] == "ap_gen_simcore_mem"} {
    eval "ap_gen_simcore_mem { \
    id ${ID} \
    name ${MemName} \
    corename ${CoreName}  \
    op mem \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage}  \
    registered_input ${RegisteredInput} \
    port_num 1 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    true_reset ${TrueReset} \
    delay_budget ${DelayBudget} \
    clk_period ${ClkPeriod} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
 } "
} else {
    puts "@W \[IMPL-102\] Cannot find ap_gen_simcore_mem, check your platform lib"
}
}


if {${::AESL::PGuard_rtl_comp_handler}} {
  ::AP::rtl_comp_handler $MemName
}


set CoreName ROM_nP
if {${::AESL::PGuard_autocg_gen} && ${::AESL::PGuard_autocg_ipmgen}} {
if {[info proc ::AESL_LIB_VIRTEX::xil_gen_ROM] == "::AESL_LIB_VIRTEX::xil_gen_ROM"} {
    eval "::AESL_LIB_VIRTEX::xil_gen_ROM { \
    id ${ID} \
    name ${MemName} \
    corename ${CoreName}  \
    op mem \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage}  \
    registered_input ${RegisteredInput} \
    port_num 1 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    true_reset ${TrueReset} \
    delay_budget ${DelayBudget} \
    clk_period ${ClkPeriod} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
 } "
  } else {
    puts "@W \[IMPL-104\] Cannot find ::AESL_LIB_VIRTEX::xil_gen_ROM, check your platform lib"
  }
}


# Memory (RAM/ROM)  definition:
set ID 10
set MemName dft_tmp_real
set CoreName ap_simcore_mem
set PortList { 2 3 }
set DataWd 32
set AddrRange 256
set AddrWd 8
set impl_style block
set TrueReset 0
set HasInitializer 0
set IsROM 0
set ROMData {}
set NumOfStage 2
set MaxLatency -1
set DelayBudget 2.71
set ClkPeriod 10
set RegisteredInput 0
if {${::AESL::PGuard_simmodel_gen}} {
if {[info proc ap_gen_simcore_mem] == "ap_gen_simcore_mem"} {
    eval "ap_gen_simcore_mem { \
    id ${ID} \
    name ${MemName} \
    corename ${CoreName}  \
    op mem \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage}  \
    registered_input ${RegisteredInput} \
    port_num 2 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    style ${impl_style} \
    true_reset ${TrueReset} \
    delay_budget ${DelayBudget} \
    clk_period ${ClkPeriod} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
 } "
} else {
    puts "@W \[IMPL-102\] Cannot find ap_gen_simcore_mem, check your platform lib"
}
}


if {${::AESL::PGuard_rtl_comp_handler}} {
  ::AP::rtl_comp_handler $MemName
}


set CoreName RAM
if {${::AESL::PGuard_autocg_gen} && ${::AESL::PGuard_autocg_ipmgen}} {
if {[info proc ::AESL_LIB_VIRTEX::xil_gen_RAM] == "::AESL_LIB_VIRTEX::xil_gen_RAM"} {
    eval "::AESL_LIB_VIRTEX::xil_gen_RAM { \
    id ${ID} \
    name ${MemName} \
    corename ${CoreName}  \
    op mem \
    reset_level 1 \
    sync_rst true \
    stage_num ${NumOfStage}  \
    registered_input ${RegisteredInput} \
    port_num 2 \
    port_list \{${PortList}\} \
    data_wd ${DataWd} \
    addr_wd ${AddrWd} \
    addr_range ${AddrRange} \
    style ${impl_style} \
    true_reset ${TrueReset} \
    delay_budget ${DelayBudget} \
    clk_period ${ClkPeriod} \
    HasInitializer ${HasInitializer} \
    rom_data \{${ROMData}\} \
 } "
  } else {
    puts "@W \[IMPL-104\] Cannot find ::AESL_LIB_VIRTEX::xil_gen_RAM, check your platform lib"
  }
}


# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 11 \
    name real_i \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_i \
    op interface \
    ports { real_i_address0 { O 8 vector } real_i_ce0 { O 1 bit } real_i_we0 { O 1 bit } real_i_d0 { O 32 vector } real_i_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 12 \
    name imag_i \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_i \
    op interface \
    ports { imag_i_address0 { O 8 vector } imag_i_ce0 { O 1 bit } imag_i_we0 { O 1 bit } imag_i_d0 { O 32 vector } imag_i_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


