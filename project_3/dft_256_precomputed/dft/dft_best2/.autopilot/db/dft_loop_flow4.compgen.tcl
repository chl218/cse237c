# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 293 \
    name real_arr8_6 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_6 \
    op interface \
    ports { real_arr8_6 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 294 \
    name real_arr8_7 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_7 \
    op interface \
    ports { real_arr8_7 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 295 \
    name imag_arr8_6 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_6 \
    op interface \
    ports { imag_arr8_6 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 296 \
    name imag_arr8_7 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_7 \
    op interface \
    ports { imag_arr8_7 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 297 \
    name imag_arr8_1 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_1 \
    op interface \
    ports { imag_arr8_1 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 298 \
    name imag_arr8_3 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_3 \
    op interface \
    ports { imag_arr8_3 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 299 \
    name imag_arr8_5 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_5 \
    op interface \
    ports { imag_arr8_5 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 300 \
    name imag_arr8_0 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_0 \
    op interface \
    ports { imag_arr8_0 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 301 \
    name imag_arr8_2 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_2 \
    op interface \
    ports { imag_arr8_2 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 302 \
    name imag_arr8_4 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_4 \
    op interface \
    ports { imag_arr8_4 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 303 \
    name real_arr8_1 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_1 \
    op interface \
    ports { real_arr8_1 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 304 \
    name real_arr8_3 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_3 \
    op interface \
    ports { real_arr8_3 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 305 \
    name real_arr8_5 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_5 \
    op interface \
    ports { real_arr8_5 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 306 \
    name real_arr8_0 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_0 \
    op interface \
    ports { real_arr8_0 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 307 \
    name real_arr8_2 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_2 \
    op interface \
    ports { real_arr8_2 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 308 \
    name real_arr8_4 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_4 \
    op interface \
    ports { real_arr8_4 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 309 \
    name imag_arr4_0 \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr4_0 \
    op interface \
    ports { imag_arr4_0 { O 32 vector } imag_arr4_0_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 310 \
    name imag_arr4_1 \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr4_1 \
    op interface \
    ports { imag_arr4_1 { O 32 vector } imag_arr4_1_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 311 \
    name imag_arr4_2 \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr4_2 \
    op interface \
    ports { imag_arr4_2 { O 32 vector } imag_arr4_2_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 312 \
    name imag_arr4_3 \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr4_3 \
    op interface \
    ports { imag_arr4_3 { O 32 vector } imag_arr4_3_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 313 \
    name real_arr4_0 \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr4_0 \
    op interface \
    ports { real_arr4_0 { O 32 vector } real_arr4_0_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 314 \
    name real_arr4_1 \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr4_1 \
    op interface \
    ports { real_arr4_1 { O 32 vector } real_arr4_1_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 315 \
    name real_arr4_2 \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr4_2 \
    op interface \
    ports { real_arr4_2 { O 32 vector } real_arr4_2_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 316 \
    name real_arr4_3 \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr4_3 \
    op interface \
    ports { real_arr4_3 { O 32 vector } real_arr4_3_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


