set C_TypeInfoList {{ 
"dft" : [[], { "return": [[], "void"]} , [{"ExternC" : 0}], [ {"real_i": [[], {"array": ["0", [256]]}] }, {"imag_i": [[], {"array": ["0", [256]]}] }, {"real_o": [[], {"array": ["0", [256]]}] }, {"imag_o": [[], {"array": ["0", [256]]}] }],["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16"],""],
 "1": [ "real_arr8", [[], {"array": ["0", [8]]}],""],
 "2": [ "real_arr64", [[], {"array": ["0", [64]]}],""],
 "3": [ "real_arr4", [[], {"array": ["0", [4]]}],""],
 "4": [ "real_arr32", [[], {"array": ["0", [32]]}],""],
 "5": [ "real_arr256", [[], {"array": ["0", [256]]}],""],
 "6": [ "real_arr2", [[], {"array": ["0", [2]]}],""],
 "7": [ "real_arr16", [[], {"array": ["0", [16]]}],""],
 "8": [ "real_arr128", [[], {"array": ["0", [128]]}],""],
 "9": [ "imag_arr8", [[], {"array": ["0", [8]]}],""],
 "10": [ "imag_arr64", [[], {"array": ["0", [64]]}],""],
 "11": [ "imag_arr4", [[], {"array": ["0", [4]]}],""],
 "12": [ "imag_arr32", [[], {"array": ["0", [32]]}],""],
 "13": [ "imag_arr256", [[], {"array": ["0", [256]]}],""],
 "14": [ "imag_arr2", [[], {"array": ["0", [2]]}],""],
 "15": [ "imag_arr16", [[], {"array": ["0", [16]]}],""],
 "16": [ "imag_arr128", [[], {"array": ["0", [128]]}],""], 
"0": [ "DTYPE", {"typedef": [[[], {"scalar": "float"}],""]}]
}}
set moduleName dft
set isCombinational 0
set isDatapathOnly 0
set isPipelined 1
set pipeline_type dataflow
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {dft}
set C_modelType { void 0 }
set C_modelArgList { 
	{ real_i_0 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_1 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_2 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_3 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_4 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_5 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_6 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_7 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_8 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_9 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_10 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_11 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_12 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_13 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_14 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_15 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_16 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_17 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_18 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_19 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_20 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_21 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_22 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_23 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_24 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_25 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_26 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_27 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_28 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_29 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_30 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_31 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_0 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_1 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_2 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_3 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_4 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_5 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_6 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_7 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_8 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_9 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_10 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_11 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_12 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_13 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_14 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_15 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_16 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_17 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_18 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_19 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_20 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_21 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_22 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_23 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_24 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_25 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_26 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_27 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_28 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_29 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_30 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_31 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_o float 32 regular {array 256 { 0 3 } 0 1 }  }
	{ imag_o float 32 regular {array 256 { 0 3 } 0 1 }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "real_i_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 224,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 225,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 226,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 227,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_4", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 4,"up" : 228,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_5", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 5,"up" : 229,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_6", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 6,"up" : 230,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_7", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 7,"up" : 231,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_8", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 8,"up" : 232,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_9", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 9,"up" : 233,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_10", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 10,"up" : 234,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_11", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 11,"up" : 235,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_12", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 12,"up" : 236,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_13", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 13,"up" : 237,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_14", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 14,"up" : 238,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_15", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 15,"up" : 239,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_16", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 16,"up" : 240,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_17", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 17,"up" : 241,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_18", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 18,"up" : 242,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_19", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 19,"up" : 243,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_20", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 20,"up" : 244,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_21", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 21,"up" : 245,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_22", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 22,"up" : 246,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_23", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 23,"up" : 247,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_24", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 24,"up" : 248,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_25", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 25,"up" : 249,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_26", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 26,"up" : 250,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_27", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 27,"up" : 251,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_28", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 28,"up" : 252,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_29", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 29,"up" : 253,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_30", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 30,"up" : 254,"step" : 32}]}]}]} , 
 	{ "Name" : "real_i_31", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 31,"up" : 255,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 224,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 225,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 226,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 227,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_4", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 4,"up" : 228,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_5", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 5,"up" : 229,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_6", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 6,"up" : 230,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_7", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 7,"up" : 231,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_8", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 8,"up" : 232,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_9", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 9,"up" : 233,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_10", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 10,"up" : 234,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_11", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 11,"up" : 235,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_12", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 12,"up" : 236,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_13", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 13,"up" : 237,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_14", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 14,"up" : 238,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_15", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 15,"up" : 239,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_16", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 16,"up" : 240,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_17", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 17,"up" : 241,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_18", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 18,"up" : 242,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_19", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 19,"up" : 243,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_20", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 20,"up" : 244,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_21", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 21,"up" : 245,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_22", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 22,"up" : 246,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_23", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 23,"up" : 247,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_24", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 24,"up" : 248,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_25", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 25,"up" : 249,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_26", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 26,"up" : 250,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_27", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 27,"up" : 251,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_28", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 28,"up" : 252,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_29", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 29,"up" : 253,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_30", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 30,"up" : 254,"step" : 32}]}]}]} , 
 	{ "Name" : "imag_i_31", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 31,"up" : 255,"step" : 32}]}]}]} , 
 	{ "Name" : "real_o", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 255,"step" : 1}]}]}]} , 
 	{ "Name" : "imag_o", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 255,"step" : 1}]}]}]} ]}
# RTL Port declarations: 
set portNum 666
set portList { 
	{ real_i_0_address0 sc_out sc_lv 3 signal 0 } 
	{ real_i_0_ce0 sc_out sc_logic 1 signal 0 } 
	{ real_i_0_d0 sc_out sc_lv 32 signal 0 } 
	{ real_i_0_q0 sc_in sc_lv 32 signal 0 } 
	{ real_i_0_we0 sc_out sc_logic 1 signal 0 } 
	{ real_i_0_address1 sc_out sc_lv 3 signal 0 } 
	{ real_i_0_ce1 sc_out sc_logic 1 signal 0 } 
	{ real_i_0_d1 sc_out sc_lv 32 signal 0 } 
	{ real_i_0_q1 sc_in sc_lv 32 signal 0 } 
	{ real_i_0_we1 sc_out sc_logic 1 signal 0 } 
	{ real_i_1_address0 sc_out sc_lv 3 signal 1 } 
	{ real_i_1_ce0 sc_out sc_logic 1 signal 1 } 
	{ real_i_1_d0 sc_out sc_lv 32 signal 1 } 
	{ real_i_1_q0 sc_in sc_lv 32 signal 1 } 
	{ real_i_1_we0 sc_out sc_logic 1 signal 1 } 
	{ real_i_1_address1 sc_out sc_lv 3 signal 1 } 
	{ real_i_1_ce1 sc_out sc_logic 1 signal 1 } 
	{ real_i_1_d1 sc_out sc_lv 32 signal 1 } 
	{ real_i_1_q1 sc_in sc_lv 32 signal 1 } 
	{ real_i_1_we1 sc_out sc_logic 1 signal 1 } 
	{ real_i_2_address0 sc_out sc_lv 3 signal 2 } 
	{ real_i_2_ce0 sc_out sc_logic 1 signal 2 } 
	{ real_i_2_d0 sc_out sc_lv 32 signal 2 } 
	{ real_i_2_q0 sc_in sc_lv 32 signal 2 } 
	{ real_i_2_we0 sc_out sc_logic 1 signal 2 } 
	{ real_i_2_address1 sc_out sc_lv 3 signal 2 } 
	{ real_i_2_ce1 sc_out sc_logic 1 signal 2 } 
	{ real_i_2_d1 sc_out sc_lv 32 signal 2 } 
	{ real_i_2_q1 sc_in sc_lv 32 signal 2 } 
	{ real_i_2_we1 sc_out sc_logic 1 signal 2 } 
	{ real_i_3_address0 sc_out sc_lv 3 signal 3 } 
	{ real_i_3_ce0 sc_out sc_logic 1 signal 3 } 
	{ real_i_3_d0 sc_out sc_lv 32 signal 3 } 
	{ real_i_3_q0 sc_in sc_lv 32 signal 3 } 
	{ real_i_3_we0 sc_out sc_logic 1 signal 3 } 
	{ real_i_3_address1 sc_out sc_lv 3 signal 3 } 
	{ real_i_3_ce1 sc_out sc_logic 1 signal 3 } 
	{ real_i_3_d1 sc_out sc_lv 32 signal 3 } 
	{ real_i_3_q1 sc_in sc_lv 32 signal 3 } 
	{ real_i_3_we1 sc_out sc_logic 1 signal 3 } 
	{ real_i_4_address0 sc_out sc_lv 3 signal 4 } 
	{ real_i_4_ce0 sc_out sc_logic 1 signal 4 } 
	{ real_i_4_d0 sc_out sc_lv 32 signal 4 } 
	{ real_i_4_q0 sc_in sc_lv 32 signal 4 } 
	{ real_i_4_we0 sc_out sc_logic 1 signal 4 } 
	{ real_i_4_address1 sc_out sc_lv 3 signal 4 } 
	{ real_i_4_ce1 sc_out sc_logic 1 signal 4 } 
	{ real_i_4_d1 sc_out sc_lv 32 signal 4 } 
	{ real_i_4_q1 sc_in sc_lv 32 signal 4 } 
	{ real_i_4_we1 sc_out sc_logic 1 signal 4 } 
	{ real_i_5_address0 sc_out sc_lv 3 signal 5 } 
	{ real_i_5_ce0 sc_out sc_logic 1 signal 5 } 
	{ real_i_5_d0 sc_out sc_lv 32 signal 5 } 
	{ real_i_5_q0 sc_in sc_lv 32 signal 5 } 
	{ real_i_5_we0 sc_out sc_logic 1 signal 5 } 
	{ real_i_5_address1 sc_out sc_lv 3 signal 5 } 
	{ real_i_5_ce1 sc_out sc_logic 1 signal 5 } 
	{ real_i_5_d1 sc_out sc_lv 32 signal 5 } 
	{ real_i_5_q1 sc_in sc_lv 32 signal 5 } 
	{ real_i_5_we1 sc_out sc_logic 1 signal 5 } 
	{ real_i_6_address0 sc_out sc_lv 3 signal 6 } 
	{ real_i_6_ce0 sc_out sc_logic 1 signal 6 } 
	{ real_i_6_d0 sc_out sc_lv 32 signal 6 } 
	{ real_i_6_q0 sc_in sc_lv 32 signal 6 } 
	{ real_i_6_we0 sc_out sc_logic 1 signal 6 } 
	{ real_i_6_address1 sc_out sc_lv 3 signal 6 } 
	{ real_i_6_ce1 sc_out sc_logic 1 signal 6 } 
	{ real_i_6_d1 sc_out sc_lv 32 signal 6 } 
	{ real_i_6_q1 sc_in sc_lv 32 signal 6 } 
	{ real_i_6_we1 sc_out sc_logic 1 signal 6 } 
	{ real_i_7_address0 sc_out sc_lv 3 signal 7 } 
	{ real_i_7_ce0 sc_out sc_logic 1 signal 7 } 
	{ real_i_7_d0 sc_out sc_lv 32 signal 7 } 
	{ real_i_7_q0 sc_in sc_lv 32 signal 7 } 
	{ real_i_7_we0 sc_out sc_logic 1 signal 7 } 
	{ real_i_7_address1 sc_out sc_lv 3 signal 7 } 
	{ real_i_7_ce1 sc_out sc_logic 1 signal 7 } 
	{ real_i_7_d1 sc_out sc_lv 32 signal 7 } 
	{ real_i_7_q1 sc_in sc_lv 32 signal 7 } 
	{ real_i_7_we1 sc_out sc_logic 1 signal 7 } 
	{ real_i_8_address0 sc_out sc_lv 3 signal 8 } 
	{ real_i_8_ce0 sc_out sc_logic 1 signal 8 } 
	{ real_i_8_d0 sc_out sc_lv 32 signal 8 } 
	{ real_i_8_q0 sc_in sc_lv 32 signal 8 } 
	{ real_i_8_we0 sc_out sc_logic 1 signal 8 } 
	{ real_i_8_address1 sc_out sc_lv 3 signal 8 } 
	{ real_i_8_ce1 sc_out sc_logic 1 signal 8 } 
	{ real_i_8_d1 sc_out sc_lv 32 signal 8 } 
	{ real_i_8_q1 sc_in sc_lv 32 signal 8 } 
	{ real_i_8_we1 sc_out sc_logic 1 signal 8 } 
	{ real_i_9_address0 sc_out sc_lv 3 signal 9 } 
	{ real_i_9_ce0 sc_out sc_logic 1 signal 9 } 
	{ real_i_9_d0 sc_out sc_lv 32 signal 9 } 
	{ real_i_9_q0 sc_in sc_lv 32 signal 9 } 
	{ real_i_9_we0 sc_out sc_logic 1 signal 9 } 
	{ real_i_9_address1 sc_out sc_lv 3 signal 9 } 
	{ real_i_9_ce1 sc_out sc_logic 1 signal 9 } 
	{ real_i_9_d1 sc_out sc_lv 32 signal 9 } 
	{ real_i_9_q1 sc_in sc_lv 32 signal 9 } 
	{ real_i_9_we1 sc_out sc_logic 1 signal 9 } 
	{ real_i_10_address0 sc_out sc_lv 3 signal 10 } 
	{ real_i_10_ce0 sc_out sc_logic 1 signal 10 } 
	{ real_i_10_d0 sc_out sc_lv 32 signal 10 } 
	{ real_i_10_q0 sc_in sc_lv 32 signal 10 } 
	{ real_i_10_we0 sc_out sc_logic 1 signal 10 } 
	{ real_i_10_address1 sc_out sc_lv 3 signal 10 } 
	{ real_i_10_ce1 sc_out sc_logic 1 signal 10 } 
	{ real_i_10_d1 sc_out sc_lv 32 signal 10 } 
	{ real_i_10_q1 sc_in sc_lv 32 signal 10 } 
	{ real_i_10_we1 sc_out sc_logic 1 signal 10 } 
	{ real_i_11_address0 sc_out sc_lv 3 signal 11 } 
	{ real_i_11_ce0 sc_out sc_logic 1 signal 11 } 
	{ real_i_11_d0 sc_out sc_lv 32 signal 11 } 
	{ real_i_11_q0 sc_in sc_lv 32 signal 11 } 
	{ real_i_11_we0 sc_out sc_logic 1 signal 11 } 
	{ real_i_11_address1 sc_out sc_lv 3 signal 11 } 
	{ real_i_11_ce1 sc_out sc_logic 1 signal 11 } 
	{ real_i_11_d1 sc_out sc_lv 32 signal 11 } 
	{ real_i_11_q1 sc_in sc_lv 32 signal 11 } 
	{ real_i_11_we1 sc_out sc_logic 1 signal 11 } 
	{ real_i_12_address0 sc_out sc_lv 3 signal 12 } 
	{ real_i_12_ce0 sc_out sc_logic 1 signal 12 } 
	{ real_i_12_d0 sc_out sc_lv 32 signal 12 } 
	{ real_i_12_q0 sc_in sc_lv 32 signal 12 } 
	{ real_i_12_we0 sc_out sc_logic 1 signal 12 } 
	{ real_i_12_address1 sc_out sc_lv 3 signal 12 } 
	{ real_i_12_ce1 sc_out sc_logic 1 signal 12 } 
	{ real_i_12_d1 sc_out sc_lv 32 signal 12 } 
	{ real_i_12_q1 sc_in sc_lv 32 signal 12 } 
	{ real_i_12_we1 sc_out sc_logic 1 signal 12 } 
	{ real_i_13_address0 sc_out sc_lv 3 signal 13 } 
	{ real_i_13_ce0 sc_out sc_logic 1 signal 13 } 
	{ real_i_13_d0 sc_out sc_lv 32 signal 13 } 
	{ real_i_13_q0 sc_in sc_lv 32 signal 13 } 
	{ real_i_13_we0 sc_out sc_logic 1 signal 13 } 
	{ real_i_13_address1 sc_out sc_lv 3 signal 13 } 
	{ real_i_13_ce1 sc_out sc_logic 1 signal 13 } 
	{ real_i_13_d1 sc_out sc_lv 32 signal 13 } 
	{ real_i_13_q1 sc_in sc_lv 32 signal 13 } 
	{ real_i_13_we1 sc_out sc_logic 1 signal 13 } 
	{ real_i_14_address0 sc_out sc_lv 3 signal 14 } 
	{ real_i_14_ce0 sc_out sc_logic 1 signal 14 } 
	{ real_i_14_d0 sc_out sc_lv 32 signal 14 } 
	{ real_i_14_q0 sc_in sc_lv 32 signal 14 } 
	{ real_i_14_we0 sc_out sc_logic 1 signal 14 } 
	{ real_i_14_address1 sc_out sc_lv 3 signal 14 } 
	{ real_i_14_ce1 sc_out sc_logic 1 signal 14 } 
	{ real_i_14_d1 sc_out sc_lv 32 signal 14 } 
	{ real_i_14_q1 sc_in sc_lv 32 signal 14 } 
	{ real_i_14_we1 sc_out sc_logic 1 signal 14 } 
	{ real_i_15_address0 sc_out sc_lv 3 signal 15 } 
	{ real_i_15_ce0 sc_out sc_logic 1 signal 15 } 
	{ real_i_15_d0 sc_out sc_lv 32 signal 15 } 
	{ real_i_15_q0 sc_in sc_lv 32 signal 15 } 
	{ real_i_15_we0 sc_out sc_logic 1 signal 15 } 
	{ real_i_15_address1 sc_out sc_lv 3 signal 15 } 
	{ real_i_15_ce1 sc_out sc_logic 1 signal 15 } 
	{ real_i_15_d1 sc_out sc_lv 32 signal 15 } 
	{ real_i_15_q1 sc_in sc_lv 32 signal 15 } 
	{ real_i_15_we1 sc_out sc_logic 1 signal 15 } 
	{ real_i_16_address0 sc_out sc_lv 3 signal 16 } 
	{ real_i_16_ce0 sc_out sc_logic 1 signal 16 } 
	{ real_i_16_d0 sc_out sc_lv 32 signal 16 } 
	{ real_i_16_q0 sc_in sc_lv 32 signal 16 } 
	{ real_i_16_we0 sc_out sc_logic 1 signal 16 } 
	{ real_i_16_address1 sc_out sc_lv 3 signal 16 } 
	{ real_i_16_ce1 sc_out sc_logic 1 signal 16 } 
	{ real_i_16_d1 sc_out sc_lv 32 signal 16 } 
	{ real_i_16_q1 sc_in sc_lv 32 signal 16 } 
	{ real_i_16_we1 sc_out sc_logic 1 signal 16 } 
	{ real_i_17_address0 sc_out sc_lv 3 signal 17 } 
	{ real_i_17_ce0 sc_out sc_logic 1 signal 17 } 
	{ real_i_17_d0 sc_out sc_lv 32 signal 17 } 
	{ real_i_17_q0 sc_in sc_lv 32 signal 17 } 
	{ real_i_17_we0 sc_out sc_logic 1 signal 17 } 
	{ real_i_17_address1 sc_out sc_lv 3 signal 17 } 
	{ real_i_17_ce1 sc_out sc_logic 1 signal 17 } 
	{ real_i_17_d1 sc_out sc_lv 32 signal 17 } 
	{ real_i_17_q1 sc_in sc_lv 32 signal 17 } 
	{ real_i_17_we1 sc_out sc_logic 1 signal 17 } 
	{ real_i_18_address0 sc_out sc_lv 3 signal 18 } 
	{ real_i_18_ce0 sc_out sc_logic 1 signal 18 } 
	{ real_i_18_d0 sc_out sc_lv 32 signal 18 } 
	{ real_i_18_q0 sc_in sc_lv 32 signal 18 } 
	{ real_i_18_we0 sc_out sc_logic 1 signal 18 } 
	{ real_i_18_address1 sc_out sc_lv 3 signal 18 } 
	{ real_i_18_ce1 sc_out sc_logic 1 signal 18 } 
	{ real_i_18_d1 sc_out sc_lv 32 signal 18 } 
	{ real_i_18_q1 sc_in sc_lv 32 signal 18 } 
	{ real_i_18_we1 sc_out sc_logic 1 signal 18 } 
	{ real_i_19_address0 sc_out sc_lv 3 signal 19 } 
	{ real_i_19_ce0 sc_out sc_logic 1 signal 19 } 
	{ real_i_19_d0 sc_out sc_lv 32 signal 19 } 
	{ real_i_19_q0 sc_in sc_lv 32 signal 19 } 
	{ real_i_19_we0 sc_out sc_logic 1 signal 19 } 
	{ real_i_19_address1 sc_out sc_lv 3 signal 19 } 
	{ real_i_19_ce1 sc_out sc_logic 1 signal 19 } 
	{ real_i_19_d1 sc_out sc_lv 32 signal 19 } 
	{ real_i_19_q1 sc_in sc_lv 32 signal 19 } 
	{ real_i_19_we1 sc_out sc_logic 1 signal 19 } 
	{ real_i_20_address0 sc_out sc_lv 3 signal 20 } 
	{ real_i_20_ce0 sc_out sc_logic 1 signal 20 } 
	{ real_i_20_d0 sc_out sc_lv 32 signal 20 } 
	{ real_i_20_q0 sc_in sc_lv 32 signal 20 } 
	{ real_i_20_we0 sc_out sc_logic 1 signal 20 } 
	{ real_i_20_address1 sc_out sc_lv 3 signal 20 } 
	{ real_i_20_ce1 sc_out sc_logic 1 signal 20 } 
	{ real_i_20_d1 sc_out sc_lv 32 signal 20 } 
	{ real_i_20_q1 sc_in sc_lv 32 signal 20 } 
	{ real_i_20_we1 sc_out sc_logic 1 signal 20 } 
	{ real_i_21_address0 sc_out sc_lv 3 signal 21 } 
	{ real_i_21_ce0 sc_out sc_logic 1 signal 21 } 
	{ real_i_21_d0 sc_out sc_lv 32 signal 21 } 
	{ real_i_21_q0 sc_in sc_lv 32 signal 21 } 
	{ real_i_21_we0 sc_out sc_logic 1 signal 21 } 
	{ real_i_21_address1 sc_out sc_lv 3 signal 21 } 
	{ real_i_21_ce1 sc_out sc_logic 1 signal 21 } 
	{ real_i_21_d1 sc_out sc_lv 32 signal 21 } 
	{ real_i_21_q1 sc_in sc_lv 32 signal 21 } 
	{ real_i_21_we1 sc_out sc_logic 1 signal 21 } 
	{ real_i_22_address0 sc_out sc_lv 3 signal 22 } 
	{ real_i_22_ce0 sc_out sc_logic 1 signal 22 } 
	{ real_i_22_d0 sc_out sc_lv 32 signal 22 } 
	{ real_i_22_q0 sc_in sc_lv 32 signal 22 } 
	{ real_i_22_we0 sc_out sc_logic 1 signal 22 } 
	{ real_i_22_address1 sc_out sc_lv 3 signal 22 } 
	{ real_i_22_ce1 sc_out sc_logic 1 signal 22 } 
	{ real_i_22_d1 sc_out sc_lv 32 signal 22 } 
	{ real_i_22_q1 sc_in sc_lv 32 signal 22 } 
	{ real_i_22_we1 sc_out sc_logic 1 signal 22 } 
	{ real_i_23_address0 sc_out sc_lv 3 signal 23 } 
	{ real_i_23_ce0 sc_out sc_logic 1 signal 23 } 
	{ real_i_23_d0 sc_out sc_lv 32 signal 23 } 
	{ real_i_23_q0 sc_in sc_lv 32 signal 23 } 
	{ real_i_23_we0 sc_out sc_logic 1 signal 23 } 
	{ real_i_23_address1 sc_out sc_lv 3 signal 23 } 
	{ real_i_23_ce1 sc_out sc_logic 1 signal 23 } 
	{ real_i_23_d1 sc_out sc_lv 32 signal 23 } 
	{ real_i_23_q1 sc_in sc_lv 32 signal 23 } 
	{ real_i_23_we1 sc_out sc_logic 1 signal 23 } 
	{ real_i_24_address0 sc_out sc_lv 3 signal 24 } 
	{ real_i_24_ce0 sc_out sc_logic 1 signal 24 } 
	{ real_i_24_d0 sc_out sc_lv 32 signal 24 } 
	{ real_i_24_q0 sc_in sc_lv 32 signal 24 } 
	{ real_i_24_we0 sc_out sc_logic 1 signal 24 } 
	{ real_i_24_address1 sc_out sc_lv 3 signal 24 } 
	{ real_i_24_ce1 sc_out sc_logic 1 signal 24 } 
	{ real_i_24_d1 sc_out sc_lv 32 signal 24 } 
	{ real_i_24_q1 sc_in sc_lv 32 signal 24 } 
	{ real_i_24_we1 sc_out sc_logic 1 signal 24 } 
	{ real_i_25_address0 sc_out sc_lv 3 signal 25 } 
	{ real_i_25_ce0 sc_out sc_logic 1 signal 25 } 
	{ real_i_25_d0 sc_out sc_lv 32 signal 25 } 
	{ real_i_25_q0 sc_in sc_lv 32 signal 25 } 
	{ real_i_25_we0 sc_out sc_logic 1 signal 25 } 
	{ real_i_25_address1 sc_out sc_lv 3 signal 25 } 
	{ real_i_25_ce1 sc_out sc_logic 1 signal 25 } 
	{ real_i_25_d1 sc_out sc_lv 32 signal 25 } 
	{ real_i_25_q1 sc_in sc_lv 32 signal 25 } 
	{ real_i_25_we1 sc_out sc_logic 1 signal 25 } 
	{ real_i_26_address0 sc_out sc_lv 3 signal 26 } 
	{ real_i_26_ce0 sc_out sc_logic 1 signal 26 } 
	{ real_i_26_d0 sc_out sc_lv 32 signal 26 } 
	{ real_i_26_q0 sc_in sc_lv 32 signal 26 } 
	{ real_i_26_we0 sc_out sc_logic 1 signal 26 } 
	{ real_i_26_address1 sc_out sc_lv 3 signal 26 } 
	{ real_i_26_ce1 sc_out sc_logic 1 signal 26 } 
	{ real_i_26_d1 sc_out sc_lv 32 signal 26 } 
	{ real_i_26_q1 sc_in sc_lv 32 signal 26 } 
	{ real_i_26_we1 sc_out sc_logic 1 signal 26 } 
	{ real_i_27_address0 sc_out sc_lv 3 signal 27 } 
	{ real_i_27_ce0 sc_out sc_logic 1 signal 27 } 
	{ real_i_27_d0 sc_out sc_lv 32 signal 27 } 
	{ real_i_27_q0 sc_in sc_lv 32 signal 27 } 
	{ real_i_27_we0 sc_out sc_logic 1 signal 27 } 
	{ real_i_27_address1 sc_out sc_lv 3 signal 27 } 
	{ real_i_27_ce1 sc_out sc_logic 1 signal 27 } 
	{ real_i_27_d1 sc_out sc_lv 32 signal 27 } 
	{ real_i_27_q1 sc_in sc_lv 32 signal 27 } 
	{ real_i_27_we1 sc_out sc_logic 1 signal 27 } 
	{ real_i_28_address0 sc_out sc_lv 3 signal 28 } 
	{ real_i_28_ce0 sc_out sc_logic 1 signal 28 } 
	{ real_i_28_d0 sc_out sc_lv 32 signal 28 } 
	{ real_i_28_q0 sc_in sc_lv 32 signal 28 } 
	{ real_i_28_we0 sc_out sc_logic 1 signal 28 } 
	{ real_i_28_address1 sc_out sc_lv 3 signal 28 } 
	{ real_i_28_ce1 sc_out sc_logic 1 signal 28 } 
	{ real_i_28_d1 sc_out sc_lv 32 signal 28 } 
	{ real_i_28_q1 sc_in sc_lv 32 signal 28 } 
	{ real_i_28_we1 sc_out sc_logic 1 signal 28 } 
	{ real_i_29_address0 sc_out sc_lv 3 signal 29 } 
	{ real_i_29_ce0 sc_out sc_logic 1 signal 29 } 
	{ real_i_29_d0 sc_out sc_lv 32 signal 29 } 
	{ real_i_29_q0 sc_in sc_lv 32 signal 29 } 
	{ real_i_29_we0 sc_out sc_logic 1 signal 29 } 
	{ real_i_29_address1 sc_out sc_lv 3 signal 29 } 
	{ real_i_29_ce1 sc_out sc_logic 1 signal 29 } 
	{ real_i_29_d1 sc_out sc_lv 32 signal 29 } 
	{ real_i_29_q1 sc_in sc_lv 32 signal 29 } 
	{ real_i_29_we1 sc_out sc_logic 1 signal 29 } 
	{ real_i_30_address0 sc_out sc_lv 3 signal 30 } 
	{ real_i_30_ce0 sc_out sc_logic 1 signal 30 } 
	{ real_i_30_d0 sc_out sc_lv 32 signal 30 } 
	{ real_i_30_q0 sc_in sc_lv 32 signal 30 } 
	{ real_i_30_we0 sc_out sc_logic 1 signal 30 } 
	{ real_i_30_address1 sc_out sc_lv 3 signal 30 } 
	{ real_i_30_ce1 sc_out sc_logic 1 signal 30 } 
	{ real_i_30_d1 sc_out sc_lv 32 signal 30 } 
	{ real_i_30_q1 sc_in sc_lv 32 signal 30 } 
	{ real_i_30_we1 sc_out sc_logic 1 signal 30 } 
	{ real_i_31_address0 sc_out sc_lv 3 signal 31 } 
	{ real_i_31_ce0 sc_out sc_logic 1 signal 31 } 
	{ real_i_31_d0 sc_out sc_lv 32 signal 31 } 
	{ real_i_31_q0 sc_in sc_lv 32 signal 31 } 
	{ real_i_31_we0 sc_out sc_logic 1 signal 31 } 
	{ real_i_31_address1 sc_out sc_lv 3 signal 31 } 
	{ real_i_31_ce1 sc_out sc_logic 1 signal 31 } 
	{ real_i_31_d1 sc_out sc_lv 32 signal 31 } 
	{ real_i_31_q1 sc_in sc_lv 32 signal 31 } 
	{ real_i_31_we1 sc_out sc_logic 1 signal 31 } 
	{ imag_i_0_address0 sc_out sc_lv 3 signal 32 } 
	{ imag_i_0_ce0 sc_out sc_logic 1 signal 32 } 
	{ imag_i_0_d0 sc_out sc_lv 32 signal 32 } 
	{ imag_i_0_q0 sc_in sc_lv 32 signal 32 } 
	{ imag_i_0_we0 sc_out sc_logic 1 signal 32 } 
	{ imag_i_0_address1 sc_out sc_lv 3 signal 32 } 
	{ imag_i_0_ce1 sc_out sc_logic 1 signal 32 } 
	{ imag_i_0_d1 sc_out sc_lv 32 signal 32 } 
	{ imag_i_0_q1 sc_in sc_lv 32 signal 32 } 
	{ imag_i_0_we1 sc_out sc_logic 1 signal 32 } 
	{ imag_i_1_address0 sc_out sc_lv 3 signal 33 } 
	{ imag_i_1_ce0 sc_out sc_logic 1 signal 33 } 
	{ imag_i_1_d0 sc_out sc_lv 32 signal 33 } 
	{ imag_i_1_q0 sc_in sc_lv 32 signal 33 } 
	{ imag_i_1_we0 sc_out sc_logic 1 signal 33 } 
	{ imag_i_1_address1 sc_out sc_lv 3 signal 33 } 
	{ imag_i_1_ce1 sc_out sc_logic 1 signal 33 } 
	{ imag_i_1_d1 sc_out sc_lv 32 signal 33 } 
	{ imag_i_1_q1 sc_in sc_lv 32 signal 33 } 
	{ imag_i_1_we1 sc_out sc_logic 1 signal 33 } 
	{ imag_i_2_address0 sc_out sc_lv 3 signal 34 } 
	{ imag_i_2_ce0 sc_out sc_logic 1 signal 34 } 
	{ imag_i_2_d0 sc_out sc_lv 32 signal 34 } 
	{ imag_i_2_q0 sc_in sc_lv 32 signal 34 } 
	{ imag_i_2_we0 sc_out sc_logic 1 signal 34 } 
	{ imag_i_2_address1 sc_out sc_lv 3 signal 34 } 
	{ imag_i_2_ce1 sc_out sc_logic 1 signal 34 } 
	{ imag_i_2_d1 sc_out sc_lv 32 signal 34 } 
	{ imag_i_2_q1 sc_in sc_lv 32 signal 34 } 
	{ imag_i_2_we1 sc_out sc_logic 1 signal 34 } 
	{ imag_i_3_address0 sc_out sc_lv 3 signal 35 } 
	{ imag_i_3_ce0 sc_out sc_logic 1 signal 35 } 
	{ imag_i_3_d0 sc_out sc_lv 32 signal 35 } 
	{ imag_i_3_q0 sc_in sc_lv 32 signal 35 } 
	{ imag_i_3_we0 sc_out sc_logic 1 signal 35 } 
	{ imag_i_3_address1 sc_out sc_lv 3 signal 35 } 
	{ imag_i_3_ce1 sc_out sc_logic 1 signal 35 } 
	{ imag_i_3_d1 sc_out sc_lv 32 signal 35 } 
	{ imag_i_3_q1 sc_in sc_lv 32 signal 35 } 
	{ imag_i_3_we1 sc_out sc_logic 1 signal 35 } 
	{ imag_i_4_address0 sc_out sc_lv 3 signal 36 } 
	{ imag_i_4_ce0 sc_out sc_logic 1 signal 36 } 
	{ imag_i_4_d0 sc_out sc_lv 32 signal 36 } 
	{ imag_i_4_q0 sc_in sc_lv 32 signal 36 } 
	{ imag_i_4_we0 sc_out sc_logic 1 signal 36 } 
	{ imag_i_4_address1 sc_out sc_lv 3 signal 36 } 
	{ imag_i_4_ce1 sc_out sc_logic 1 signal 36 } 
	{ imag_i_4_d1 sc_out sc_lv 32 signal 36 } 
	{ imag_i_4_q1 sc_in sc_lv 32 signal 36 } 
	{ imag_i_4_we1 sc_out sc_logic 1 signal 36 } 
	{ imag_i_5_address0 sc_out sc_lv 3 signal 37 } 
	{ imag_i_5_ce0 sc_out sc_logic 1 signal 37 } 
	{ imag_i_5_d0 sc_out sc_lv 32 signal 37 } 
	{ imag_i_5_q0 sc_in sc_lv 32 signal 37 } 
	{ imag_i_5_we0 sc_out sc_logic 1 signal 37 } 
	{ imag_i_5_address1 sc_out sc_lv 3 signal 37 } 
	{ imag_i_5_ce1 sc_out sc_logic 1 signal 37 } 
	{ imag_i_5_d1 sc_out sc_lv 32 signal 37 } 
	{ imag_i_5_q1 sc_in sc_lv 32 signal 37 } 
	{ imag_i_5_we1 sc_out sc_logic 1 signal 37 } 
	{ imag_i_6_address0 sc_out sc_lv 3 signal 38 } 
	{ imag_i_6_ce0 sc_out sc_logic 1 signal 38 } 
	{ imag_i_6_d0 sc_out sc_lv 32 signal 38 } 
	{ imag_i_6_q0 sc_in sc_lv 32 signal 38 } 
	{ imag_i_6_we0 sc_out sc_logic 1 signal 38 } 
	{ imag_i_6_address1 sc_out sc_lv 3 signal 38 } 
	{ imag_i_6_ce1 sc_out sc_logic 1 signal 38 } 
	{ imag_i_6_d1 sc_out sc_lv 32 signal 38 } 
	{ imag_i_6_q1 sc_in sc_lv 32 signal 38 } 
	{ imag_i_6_we1 sc_out sc_logic 1 signal 38 } 
	{ imag_i_7_address0 sc_out sc_lv 3 signal 39 } 
	{ imag_i_7_ce0 sc_out sc_logic 1 signal 39 } 
	{ imag_i_7_d0 sc_out sc_lv 32 signal 39 } 
	{ imag_i_7_q0 sc_in sc_lv 32 signal 39 } 
	{ imag_i_7_we0 sc_out sc_logic 1 signal 39 } 
	{ imag_i_7_address1 sc_out sc_lv 3 signal 39 } 
	{ imag_i_7_ce1 sc_out sc_logic 1 signal 39 } 
	{ imag_i_7_d1 sc_out sc_lv 32 signal 39 } 
	{ imag_i_7_q1 sc_in sc_lv 32 signal 39 } 
	{ imag_i_7_we1 sc_out sc_logic 1 signal 39 } 
	{ imag_i_8_address0 sc_out sc_lv 3 signal 40 } 
	{ imag_i_8_ce0 sc_out sc_logic 1 signal 40 } 
	{ imag_i_8_d0 sc_out sc_lv 32 signal 40 } 
	{ imag_i_8_q0 sc_in sc_lv 32 signal 40 } 
	{ imag_i_8_we0 sc_out sc_logic 1 signal 40 } 
	{ imag_i_8_address1 sc_out sc_lv 3 signal 40 } 
	{ imag_i_8_ce1 sc_out sc_logic 1 signal 40 } 
	{ imag_i_8_d1 sc_out sc_lv 32 signal 40 } 
	{ imag_i_8_q1 sc_in sc_lv 32 signal 40 } 
	{ imag_i_8_we1 sc_out sc_logic 1 signal 40 } 
	{ imag_i_9_address0 sc_out sc_lv 3 signal 41 } 
	{ imag_i_9_ce0 sc_out sc_logic 1 signal 41 } 
	{ imag_i_9_d0 sc_out sc_lv 32 signal 41 } 
	{ imag_i_9_q0 sc_in sc_lv 32 signal 41 } 
	{ imag_i_9_we0 sc_out sc_logic 1 signal 41 } 
	{ imag_i_9_address1 sc_out sc_lv 3 signal 41 } 
	{ imag_i_9_ce1 sc_out sc_logic 1 signal 41 } 
	{ imag_i_9_d1 sc_out sc_lv 32 signal 41 } 
	{ imag_i_9_q1 sc_in sc_lv 32 signal 41 } 
	{ imag_i_9_we1 sc_out sc_logic 1 signal 41 } 
	{ imag_i_10_address0 sc_out sc_lv 3 signal 42 } 
	{ imag_i_10_ce0 sc_out sc_logic 1 signal 42 } 
	{ imag_i_10_d0 sc_out sc_lv 32 signal 42 } 
	{ imag_i_10_q0 sc_in sc_lv 32 signal 42 } 
	{ imag_i_10_we0 sc_out sc_logic 1 signal 42 } 
	{ imag_i_10_address1 sc_out sc_lv 3 signal 42 } 
	{ imag_i_10_ce1 sc_out sc_logic 1 signal 42 } 
	{ imag_i_10_d1 sc_out sc_lv 32 signal 42 } 
	{ imag_i_10_q1 sc_in sc_lv 32 signal 42 } 
	{ imag_i_10_we1 sc_out sc_logic 1 signal 42 } 
	{ imag_i_11_address0 sc_out sc_lv 3 signal 43 } 
	{ imag_i_11_ce0 sc_out sc_logic 1 signal 43 } 
	{ imag_i_11_d0 sc_out sc_lv 32 signal 43 } 
	{ imag_i_11_q0 sc_in sc_lv 32 signal 43 } 
	{ imag_i_11_we0 sc_out sc_logic 1 signal 43 } 
	{ imag_i_11_address1 sc_out sc_lv 3 signal 43 } 
	{ imag_i_11_ce1 sc_out sc_logic 1 signal 43 } 
	{ imag_i_11_d1 sc_out sc_lv 32 signal 43 } 
	{ imag_i_11_q1 sc_in sc_lv 32 signal 43 } 
	{ imag_i_11_we1 sc_out sc_logic 1 signal 43 } 
	{ imag_i_12_address0 sc_out sc_lv 3 signal 44 } 
	{ imag_i_12_ce0 sc_out sc_logic 1 signal 44 } 
	{ imag_i_12_d0 sc_out sc_lv 32 signal 44 } 
	{ imag_i_12_q0 sc_in sc_lv 32 signal 44 } 
	{ imag_i_12_we0 sc_out sc_logic 1 signal 44 } 
	{ imag_i_12_address1 sc_out sc_lv 3 signal 44 } 
	{ imag_i_12_ce1 sc_out sc_logic 1 signal 44 } 
	{ imag_i_12_d1 sc_out sc_lv 32 signal 44 } 
	{ imag_i_12_q1 sc_in sc_lv 32 signal 44 } 
	{ imag_i_12_we1 sc_out sc_logic 1 signal 44 } 
	{ imag_i_13_address0 sc_out sc_lv 3 signal 45 } 
	{ imag_i_13_ce0 sc_out sc_logic 1 signal 45 } 
	{ imag_i_13_d0 sc_out sc_lv 32 signal 45 } 
	{ imag_i_13_q0 sc_in sc_lv 32 signal 45 } 
	{ imag_i_13_we0 sc_out sc_logic 1 signal 45 } 
	{ imag_i_13_address1 sc_out sc_lv 3 signal 45 } 
	{ imag_i_13_ce1 sc_out sc_logic 1 signal 45 } 
	{ imag_i_13_d1 sc_out sc_lv 32 signal 45 } 
	{ imag_i_13_q1 sc_in sc_lv 32 signal 45 } 
	{ imag_i_13_we1 sc_out sc_logic 1 signal 45 } 
	{ imag_i_14_address0 sc_out sc_lv 3 signal 46 } 
	{ imag_i_14_ce0 sc_out sc_logic 1 signal 46 } 
	{ imag_i_14_d0 sc_out sc_lv 32 signal 46 } 
	{ imag_i_14_q0 sc_in sc_lv 32 signal 46 } 
	{ imag_i_14_we0 sc_out sc_logic 1 signal 46 } 
	{ imag_i_14_address1 sc_out sc_lv 3 signal 46 } 
	{ imag_i_14_ce1 sc_out sc_logic 1 signal 46 } 
	{ imag_i_14_d1 sc_out sc_lv 32 signal 46 } 
	{ imag_i_14_q1 sc_in sc_lv 32 signal 46 } 
	{ imag_i_14_we1 sc_out sc_logic 1 signal 46 } 
	{ imag_i_15_address0 sc_out sc_lv 3 signal 47 } 
	{ imag_i_15_ce0 sc_out sc_logic 1 signal 47 } 
	{ imag_i_15_d0 sc_out sc_lv 32 signal 47 } 
	{ imag_i_15_q0 sc_in sc_lv 32 signal 47 } 
	{ imag_i_15_we0 sc_out sc_logic 1 signal 47 } 
	{ imag_i_15_address1 sc_out sc_lv 3 signal 47 } 
	{ imag_i_15_ce1 sc_out sc_logic 1 signal 47 } 
	{ imag_i_15_d1 sc_out sc_lv 32 signal 47 } 
	{ imag_i_15_q1 sc_in sc_lv 32 signal 47 } 
	{ imag_i_15_we1 sc_out sc_logic 1 signal 47 } 
	{ imag_i_16_address0 sc_out sc_lv 3 signal 48 } 
	{ imag_i_16_ce0 sc_out sc_logic 1 signal 48 } 
	{ imag_i_16_d0 sc_out sc_lv 32 signal 48 } 
	{ imag_i_16_q0 sc_in sc_lv 32 signal 48 } 
	{ imag_i_16_we0 sc_out sc_logic 1 signal 48 } 
	{ imag_i_16_address1 sc_out sc_lv 3 signal 48 } 
	{ imag_i_16_ce1 sc_out sc_logic 1 signal 48 } 
	{ imag_i_16_d1 sc_out sc_lv 32 signal 48 } 
	{ imag_i_16_q1 sc_in sc_lv 32 signal 48 } 
	{ imag_i_16_we1 sc_out sc_logic 1 signal 48 } 
	{ imag_i_17_address0 sc_out sc_lv 3 signal 49 } 
	{ imag_i_17_ce0 sc_out sc_logic 1 signal 49 } 
	{ imag_i_17_d0 sc_out sc_lv 32 signal 49 } 
	{ imag_i_17_q0 sc_in sc_lv 32 signal 49 } 
	{ imag_i_17_we0 sc_out sc_logic 1 signal 49 } 
	{ imag_i_17_address1 sc_out sc_lv 3 signal 49 } 
	{ imag_i_17_ce1 sc_out sc_logic 1 signal 49 } 
	{ imag_i_17_d1 sc_out sc_lv 32 signal 49 } 
	{ imag_i_17_q1 sc_in sc_lv 32 signal 49 } 
	{ imag_i_17_we1 sc_out sc_logic 1 signal 49 } 
	{ imag_i_18_address0 sc_out sc_lv 3 signal 50 } 
	{ imag_i_18_ce0 sc_out sc_logic 1 signal 50 } 
	{ imag_i_18_d0 sc_out sc_lv 32 signal 50 } 
	{ imag_i_18_q0 sc_in sc_lv 32 signal 50 } 
	{ imag_i_18_we0 sc_out sc_logic 1 signal 50 } 
	{ imag_i_18_address1 sc_out sc_lv 3 signal 50 } 
	{ imag_i_18_ce1 sc_out sc_logic 1 signal 50 } 
	{ imag_i_18_d1 sc_out sc_lv 32 signal 50 } 
	{ imag_i_18_q1 sc_in sc_lv 32 signal 50 } 
	{ imag_i_18_we1 sc_out sc_logic 1 signal 50 } 
	{ imag_i_19_address0 sc_out sc_lv 3 signal 51 } 
	{ imag_i_19_ce0 sc_out sc_logic 1 signal 51 } 
	{ imag_i_19_d0 sc_out sc_lv 32 signal 51 } 
	{ imag_i_19_q0 sc_in sc_lv 32 signal 51 } 
	{ imag_i_19_we0 sc_out sc_logic 1 signal 51 } 
	{ imag_i_19_address1 sc_out sc_lv 3 signal 51 } 
	{ imag_i_19_ce1 sc_out sc_logic 1 signal 51 } 
	{ imag_i_19_d1 sc_out sc_lv 32 signal 51 } 
	{ imag_i_19_q1 sc_in sc_lv 32 signal 51 } 
	{ imag_i_19_we1 sc_out sc_logic 1 signal 51 } 
	{ imag_i_20_address0 sc_out sc_lv 3 signal 52 } 
	{ imag_i_20_ce0 sc_out sc_logic 1 signal 52 } 
	{ imag_i_20_d0 sc_out sc_lv 32 signal 52 } 
	{ imag_i_20_q0 sc_in sc_lv 32 signal 52 } 
	{ imag_i_20_we0 sc_out sc_logic 1 signal 52 } 
	{ imag_i_20_address1 sc_out sc_lv 3 signal 52 } 
	{ imag_i_20_ce1 sc_out sc_logic 1 signal 52 } 
	{ imag_i_20_d1 sc_out sc_lv 32 signal 52 } 
	{ imag_i_20_q1 sc_in sc_lv 32 signal 52 } 
	{ imag_i_20_we1 sc_out sc_logic 1 signal 52 } 
	{ imag_i_21_address0 sc_out sc_lv 3 signal 53 } 
	{ imag_i_21_ce0 sc_out sc_logic 1 signal 53 } 
	{ imag_i_21_d0 sc_out sc_lv 32 signal 53 } 
	{ imag_i_21_q0 sc_in sc_lv 32 signal 53 } 
	{ imag_i_21_we0 sc_out sc_logic 1 signal 53 } 
	{ imag_i_21_address1 sc_out sc_lv 3 signal 53 } 
	{ imag_i_21_ce1 sc_out sc_logic 1 signal 53 } 
	{ imag_i_21_d1 sc_out sc_lv 32 signal 53 } 
	{ imag_i_21_q1 sc_in sc_lv 32 signal 53 } 
	{ imag_i_21_we1 sc_out sc_logic 1 signal 53 } 
	{ imag_i_22_address0 sc_out sc_lv 3 signal 54 } 
	{ imag_i_22_ce0 sc_out sc_logic 1 signal 54 } 
	{ imag_i_22_d0 sc_out sc_lv 32 signal 54 } 
	{ imag_i_22_q0 sc_in sc_lv 32 signal 54 } 
	{ imag_i_22_we0 sc_out sc_logic 1 signal 54 } 
	{ imag_i_22_address1 sc_out sc_lv 3 signal 54 } 
	{ imag_i_22_ce1 sc_out sc_logic 1 signal 54 } 
	{ imag_i_22_d1 sc_out sc_lv 32 signal 54 } 
	{ imag_i_22_q1 sc_in sc_lv 32 signal 54 } 
	{ imag_i_22_we1 sc_out sc_logic 1 signal 54 } 
	{ imag_i_23_address0 sc_out sc_lv 3 signal 55 } 
	{ imag_i_23_ce0 sc_out sc_logic 1 signal 55 } 
	{ imag_i_23_d0 sc_out sc_lv 32 signal 55 } 
	{ imag_i_23_q0 sc_in sc_lv 32 signal 55 } 
	{ imag_i_23_we0 sc_out sc_logic 1 signal 55 } 
	{ imag_i_23_address1 sc_out sc_lv 3 signal 55 } 
	{ imag_i_23_ce1 sc_out sc_logic 1 signal 55 } 
	{ imag_i_23_d1 sc_out sc_lv 32 signal 55 } 
	{ imag_i_23_q1 sc_in sc_lv 32 signal 55 } 
	{ imag_i_23_we1 sc_out sc_logic 1 signal 55 } 
	{ imag_i_24_address0 sc_out sc_lv 3 signal 56 } 
	{ imag_i_24_ce0 sc_out sc_logic 1 signal 56 } 
	{ imag_i_24_d0 sc_out sc_lv 32 signal 56 } 
	{ imag_i_24_q0 sc_in sc_lv 32 signal 56 } 
	{ imag_i_24_we0 sc_out sc_logic 1 signal 56 } 
	{ imag_i_24_address1 sc_out sc_lv 3 signal 56 } 
	{ imag_i_24_ce1 sc_out sc_logic 1 signal 56 } 
	{ imag_i_24_d1 sc_out sc_lv 32 signal 56 } 
	{ imag_i_24_q1 sc_in sc_lv 32 signal 56 } 
	{ imag_i_24_we1 sc_out sc_logic 1 signal 56 } 
	{ imag_i_25_address0 sc_out sc_lv 3 signal 57 } 
	{ imag_i_25_ce0 sc_out sc_logic 1 signal 57 } 
	{ imag_i_25_d0 sc_out sc_lv 32 signal 57 } 
	{ imag_i_25_q0 sc_in sc_lv 32 signal 57 } 
	{ imag_i_25_we0 sc_out sc_logic 1 signal 57 } 
	{ imag_i_25_address1 sc_out sc_lv 3 signal 57 } 
	{ imag_i_25_ce1 sc_out sc_logic 1 signal 57 } 
	{ imag_i_25_d1 sc_out sc_lv 32 signal 57 } 
	{ imag_i_25_q1 sc_in sc_lv 32 signal 57 } 
	{ imag_i_25_we1 sc_out sc_logic 1 signal 57 } 
	{ imag_i_26_address0 sc_out sc_lv 3 signal 58 } 
	{ imag_i_26_ce0 sc_out sc_logic 1 signal 58 } 
	{ imag_i_26_d0 sc_out sc_lv 32 signal 58 } 
	{ imag_i_26_q0 sc_in sc_lv 32 signal 58 } 
	{ imag_i_26_we0 sc_out sc_logic 1 signal 58 } 
	{ imag_i_26_address1 sc_out sc_lv 3 signal 58 } 
	{ imag_i_26_ce1 sc_out sc_logic 1 signal 58 } 
	{ imag_i_26_d1 sc_out sc_lv 32 signal 58 } 
	{ imag_i_26_q1 sc_in sc_lv 32 signal 58 } 
	{ imag_i_26_we1 sc_out sc_logic 1 signal 58 } 
	{ imag_i_27_address0 sc_out sc_lv 3 signal 59 } 
	{ imag_i_27_ce0 sc_out sc_logic 1 signal 59 } 
	{ imag_i_27_d0 sc_out sc_lv 32 signal 59 } 
	{ imag_i_27_q0 sc_in sc_lv 32 signal 59 } 
	{ imag_i_27_we0 sc_out sc_logic 1 signal 59 } 
	{ imag_i_27_address1 sc_out sc_lv 3 signal 59 } 
	{ imag_i_27_ce1 sc_out sc_logic 1 signal 59 } 
	{ imag_i_27_d1 sc_out sc_lv 32 signal 59 } 
	{ imag_i_27_q1 sc_in sc_lv 32 signal 59 } 
	{ imag_i_27_we1 sc_out sc_logic 1 signal 59 } 
	{ imag_i_28_address0 sc_out sc_lv 3 signal 60 } 
	{ imag_i_28_ce0 sc_out sc_logic 1 signal 60 } 
	{ imag_i_28_d0 sc_out sc_lv 32 signal 60 } 
	{ imag_i_28_q0 sc_in sc_lv 32 signal 60 } 
	{ imag_i_28_we0 sc_out sc_logic 1 signal 60 } 
	{ imag_i_28_address1 sc_out sc_lv 3 signal 60 } 
	{ imag_i_28_ce1 sc_out sc_logic 1 signal 60 } 
	{ imag_i_28_d1 sc_out sc_lv 32 signal 60 } 
	{ imag_i_28_q1 sc_in sc_lv 32 signal 60 } 
	{ imag_i_28_we1 sc_out sc_logic 1 signal 60 } 
	{ imag_i_29_address0 sc_out sc_lv 3 signal 61 } 
	{ imag_i_29_ce0 sc_out sc_logic 1 signal 61 } 
	{ imag_i_29_d0 sc_out sc_lv 32 signal 61 } 
	{ imag_i_29_q0 sc_in sc_lv 32 signal 61 } 
	{ imag_i_29_we0 sc_out sc_logic 1 signal 61 } 
	{ imag_i_29_address1 sc_out sc_lv 3 signal 61 } 
	{ imag_i_29_ce1 sc_out sc_logic 1 signal 61 } 
	{ imag_i_29_d1 sc_out sc_lv 32 signal 61 } 
	{ imag_i_29_q1 sc_in sc_lv 32 signal 61 } 
	{ imag_i_29_we1 sc_out sc_logic 1 signal 61 } 
	{ imag_i_30_address0 sc_out sc_lv 3 signal 62 } 
	{ imag_i_30_ce0 sc_out sc_logic 1 signal 62 } 
	{ imag_i_30_d0 sc_out sc_lv 32 signal 62 } 
	{ imag_i_30_q0 sc_in sc_lv 32 signal 62 } 
	{ imag_i_30_we0 sc_out sc_logic 1 signal 62 } 
	{ imag_i_30_address1 sc_out sc_lv 3 signal 62 } 
	{ imag_i_30_ce1 sc_out sc_logic 1 signal 62 } 
	{ imag_i_30_d1 sc_out sc_lv 32 signal 62 } 
	{ imag_i_30_q1 sc_in sc_lv 32 signal 62 } 
	{ imag_i_30_we1 sc_out sc_logic 1 signal 62 } 
	{ imag_i_31_address0 sc_out sc_lv 3 signal 63 } 
	{ imag_i_31_ce0 sc_out sc_logic 1 signal 63 } 
	{ imag_i_31_d0 sc_out sc_lv 32 signal 63 } 
	{ imag_i_31_q0 sc_in sc_lv 32 signal 63 } 
	{ imag_i_31_we0 sc_out sc_logic 1 signal 63 } 
	{ imag_i_31_address1 sc_out sc_lv 3 signal 63 } 
	{ imag_i_31_ce1 sc_out sc_logic 1 signal 63 } 
	{ imag_i_31_d1 sc_out sc_lv 32 signal 63 } 
	{ imag_i_31_q1 sc_in sc_lv 32 signal 63 } 
	{ imag_i_31_we1 sc_out sc_logic 1 signal 63 } 
	{ real_o_address0 sc_out sc_lv 8 signal 64 } 
	{ real_o_ce0 sc_out sc_logic 1 signal 64 } 
	{ real_o_d0 sc_out sc_lv 32 signal 64 } 
	{ real_o_q0 sc_in sc_lv 32 signal 64 } 
	{ real_o_we0 sc_out sc_logic 1 signal 64 } 
	{ real_o_address1 sc_out sc_lv 8 signal 64 } 
	{ real_o_ce1 sc_out sc_logic 1 signal 64 } 
	{ real_o_d1 sc_out sc_lv 32 signal 64 } 
	{ real_o_q1 sc_in sc_lv 32 signal 64 } 
	{ real_o_we1 sc_out sc_logic 1 signal 64 } 
	{ imag_o_address0 sc_out sc_lv 8 signal 65 } 
	{ imag_o_ce0 sc_out sc_logic 1 signal 65 } 
	{ imag_o_d0 sc_out sc_lv 32 signal 65 } 
	{ imag_o_q0 sc_in sc_lv 32 signal 65 } 
	{ imag_o_we0 sc_out sc_logic 1 signal 65 } 
	{ imag_o_address1 sc_out sc_lv 8 signal 65 } 
	{ imag_o_ce1 sc_out sc_logic 1 signal 65 } 
	{ imag_o_d1 sc_out sc_lv 32 signal 65 } 
	{ imag_o_q1 sc_in sc_lv 32 signal 65 } 
	{ imag_o_we1 sc_out sc_logic 1 signal 65 } 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
}
set NewPortList {[ 
	{ "name": "real_i_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_0", "role": "address0" }} , 
 	{ "name": "real_i_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "ce0" }} , 
 	{ "name": "real_i_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "d0" }} , 
 	{ "name": "real_i_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "q0" }} , 
 	{ "name": "real_i_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "we0" }} , 
 	{ "name": "real_i_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_0", "role": "address1" }} , 
 	{ "name": "real_i_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "ce1" }} , 
 	{ "name": "real_i_0_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "d1" }} , 
 	{ "name": "real_i_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "q1" }} , 
 	{ "name": "real_i_0_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "we1" }} , 
 	{ "name": "real_i_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_1", "role": "address0" }} , 
 	{ "name": "real_i_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "ce0" }} , 
 	{ "name": "real_i_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "d0" }} , 
 	{ "name": "real_i_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "q0" }} , 
 	{ "name": "real_i_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "we0" }} , 
 	{ "name": "real_i_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_1", "role": "address1" }} , 
 	{ "name": "real_i_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "ce1" }} , 
 	{ "name": "real_i_1_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "d1" }} , 
 	{ "name": "real_i_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "q1" }} , 
 	{ "name": "real_i_1_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "we1" }} , 
 	{ "name": "real_i_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_2", "role": "address0" }} , 
 	{ "name": "real_i_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_2", "role": "ce0" }} , 
 	{ "name": "real_i_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_2", "role": "d0" }} , 
 	{ "name": "real_i_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_2", "role": "q0" }} , 
 	{ "name": "real_i_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_2", "role": "we0" }} , 
 	{ "name": "real_i_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_2", "role": "address1" }} , 
 	{ "name": "real_i_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_2", "role": "ce1" }} , 
 	{ "name": "real_i_2_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_2", "role": "d1" }} , 
 	{ "name": "real_i_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_2", "role": "q1" }} , 
 	{ "name": "real_i_2_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_2", "role": "we1" }} , 
 	{ "name": "real_i_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_3", "role": "address0" }} , 
 	{ "name": "real_i_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_3", "role": "ce0" }} , 
 	{ "name": "real_i_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_3", "role": "d0" }} , 
 	{ "name": "real_i_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_3", "role": "q0" }} , 
 	{ "name": "real_i_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_3", "role": "we0" }} , 
 	{ "name": "real_i_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_3", "role": "address1" }} , 
 	{ "name": "real_i_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_3", "role": "ce1" }} , 
 	{ "name": "real_i_3_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_3", "role": "d1" }} , 
 	{ "name": "real_i_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_3", "role": "q1" }} , 
 	{ "name": "real_i_3_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_3", "role": "we1" }} , 
 	{ "name": "real_i_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_4", "role": "address0" }} , 
 	{ "name": "real_i_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_4", "role": "ce0" }} , 
 	{ "name": "real_i_4_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_4", "role": "d0" }} , 
 	{ "name": "real_i_4_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_4", "role": "q0" }} , 
 	{ "name": "real_i_4_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_4", "role": "we0" }} , 
 	{ "name": "real_i_4_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_4", "role": "address1" }} , 
 	{ "name": "real_i_4_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_4", "role": "ce1" }} , 
 	{ "name": "real_i_4_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_4", "role": "d1" }} , 
 	{ "name": "real_i_4_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_4", "role": "q1" }} , 
 	{ "name": "real_i_4_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_4", "role": "we1" }} , 
 	{ "name": "real_i_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_5", "role": "address0" }} , 
 	{ "name": "real_i_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_5", "role": "ce0" }} , 
 	{ "name": "real_i_5_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_5", "role": "d0" }} , 
 	{ "name": "real_i_5_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_5", "role": "q0" }} , 
 	{ "name": "real_i_5_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_5", "role": "we0" }} , 
 	{ "name": "real_i_5_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_5", "role": "address1" }} , 
 	{ "name": "real_i_5_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_5", "role": "ce1" }} , 
 	{ "name": "real_i_5_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_5", "role": "d1" }} , 
 	{ "name": "real_i_5_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_5", "role": "q1" }} , 
 	{ "name": "real_i_5_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_5", "role": "we1" }} , 
 	{ "name": "real_i_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_6", "role": "address0" }} , 
 	{ "name": "real_i_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_6", "role": "ce0" }} , 
 	{ "name": "real_i_6_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_6", "role": "d0" }} , 
 	{ "name": "real_i_6_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_6", "role": "q0" }} , 
 	{ "name": "real_i_6_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_6", "role": "we0" }} , 
 	{ "name": "real_i_6_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_6", "role": "address1" }} , 
 	{ "name": "real_i_6_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_6", "role": "ce1" }} , 
 	{ "name": "real_i_6_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_6", "role": "d1" }} , 
 	{ "name": "real_i_6_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_6", "role": "q1" }} , 
 	{ "name": "real_i_6_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_6", "role": "we1" }} , 
 	{ "name": "real_i_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_7", "role": "address0" }} , 
 	{ "name": "real_i_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_7", "role": "ce0" }} , 
 	{ "name": "real_i_7_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_7", "role": "d0" }} , 
 	{ "name": "real_i_7_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_7", "role": "q0" }} , 
 	{ "name": "real_i_7_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_7", "role": "we0" }} , 
 	{ "name": "real_i_7_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_7", "role": "address1" }} , 
 	{ "name": "real_i_7_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_7", "role": "ce1" }} , 
 	{ "name": "real_i_7_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_7", "role": "d1" }} , 
 	{ "name": "real_i_7_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_7", "role": "q1" }} , 
 	{ "name": "real_i_7_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_7", "role": "we1" }} , 
 	{ "name": "real_i_8_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_8", "role": "address0" }} , 
 	{ "name": "real_i_8_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_8", "role": "ce0" }} , 
 	{ "name": "real_i_8_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_8", "role": "d0" }} , 
 	{ "name": "real_i_8_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_8", "role": "q0" }} , 
 	{ "name": "real_i_8_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_8", "role": "we0" }} , 
 	{ "name": "real_i_8_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_8", "role": "address1" }} , 
 	{ "name": "real_i_8_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_8", "role": "ce1" }} , 
 	{ "name": "real_i_8_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_8", "role": "d1" }} , 
 	{ "name": "real_i_8_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_8", "role": "q1" }} , 
 	{ "name": "real_i_8_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_8", "role": "we1" }} , 
 	{ "name": "real_i_9_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_9", "role": "address0" }} , 
 	{ "name": "real_i_9_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_9", "role": "ce0" }} , 
 	{ "name": "real_i_9_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_9", "role": "d0" }} , 
 	{ "name": "real_i_9_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_9", "role": "q0" }} , 
 	{ "name": "real_i_9_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_9", "role": "we0" }} , 
 	{ "name": "real_i_9_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_9", "role": "address1" }} , 
 	{ "name": "real_i_9_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_9", "role": "ce1" }} , 
 	{ "name": "real_i_9_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_9", "role": "d1" }} , 
 	{ "name": "real_i_9_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_9", "role": "q1" }} , 
 	{ "name": "real_i_9_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_9", "role": "we1" }} , 
 	{ "name": "real_i_10_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_10", "role": "address0" }} , 
 	{ "name": "real_i_10_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_10", "role": "ce0" }} , 
 	{ "name": "real_i_10_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_10", "role": "d0" }} , 
 	{ "name": "real_i_10_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_10", "role": "q0" }} , 
 	{ "name": "real_i_10_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_10", "role": "we0" }} , 
 	{ "name": "real_i_10_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_10", "role": "address1" }} , 
 	{ "name": "real_i_10_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_10", "role": "ce1" }} , 
 	{ "name": "real_i_10_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_10", "role": "d1" }} , 
 	{ "name": "real_i_10_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_10", "role": "q1" }} , 
 	{ "name": "real_i_10_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_10", "role": "we1" }} , 
 	{ "name": "real_i_11_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_11", "role": "address0" }} , 
 	{ "name": "real_i_11_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_11", "role": "ce0" }} , 
 	{ "name": "real_i_11_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_11", "role": "d0" }} , 
 	{ "name": "real_i_11_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_11", "role": "q0" }} , 
 	{ "name": "real_i_11_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_11", "role": "we0" }} , 
 	{ "name": "real_i_11_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_11", "role": "address1" }} , 
 	{ "name": "real_i_11_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_11", "role": "ce1" }} , 
 	{ "name": "real_i_11_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_11", "role": "d1" }} , 
 	{ "name": "real_i_11_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_11", "role": "q1" }} , 
 	{ "name": "real_i_11_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_11", "role": "we1" }} , 
 	{ "name": "real_i_12_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_12", "role": "address0" }} , 
 	{ "name": "real_i_12_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_12", "role": "ce0" }} , 
 	{ "name": "real_i_12_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_12", "role": "d0" }} , 
 	{ "name": "real_i_12_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_12", "role": "q0" }} , 
 	{ "name": "real_i_12_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_12", "role": "we0" }} , 
 	{ "name": "real_i_12_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_12", "role": "address1" }} , 
 	{ "name": "real_i_12_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_12", "role": "ce1" }} , 
 	{ "name": "real_i_12_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_12", "role": "d1" }} , 
 	{ "name": "real_i_12_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_12", "role": "q1" }} , 
 	{ "name": "real_i_12_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_12", "role": "we1" }} , 
 	{ "name": "real_i_13_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_13", "role": "address0" }} , 
 	{ "name": "real_i_13_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_13", "role": "ce0" }} , 
 	{ "name": "real_i_13_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_13", "role": "d0" }} , 
 	{ "name": "real_i_13_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_13", "role": "q0" }} , 
 	{ "name": "real_i_13_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_13", "role": "we0" }} , 
 	{ "name": "real_i_13_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_13", "role": "address1" }} , 
 	{ "name": "real_i_13_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_13", "role": "ce1" }} , 
 	{ "name": "real_i_13_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_13", "role": "d1" }} , 
 	{ "name": "real_i_13_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_13", "role": "q1" }} , 
 	{ "name": "real_i_13_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_13", "role": "we1" }} , 
 	{ "name": "real_i_14_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_14", "role": "address0" }} , 
 	{ "name": "real_i_14_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_14", "role": "ce0" }} , 
 	{ "name": "real_i_14_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_14", "role": "d0" }} , 
 	{ "name": "real_i_14_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_14", "role": "q0" }} , 
 	{ "name": "real_i_14_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_14", "role": "we0" }} , 
 	{ "name": "real_i_14_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_14", "role": "address1" }} , 
 	{ "name": "real_i_14_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_14", "role": "ce1" }} , 
 	{ "name": "real_i_14_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_14", "role": "d1" }} , 
 	{ "name": "real_i_14_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_14", "role": "q1" }} , 
 	{ "name": "real_i_14_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_14", "role": "we1" }} , 
 	{ "name": "real_i_15_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_15", "role": "address0" }} , 
 	{ "name": "real_i_15_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_15", "role": "ce0" }} , 
 	{ "name": "real_i_15_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_15", "role": "d0" }} , 
 	{ "name": "real_i_15_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_15", "role": "q0" }} , 
 	{ "name": "real_i_15_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_15", "role": "we0" }} , 
 	{ "name": "real_i_15_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_15", "role": "address1" }} , 
 	{ "name": "real_i_15_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_15", "role": "ce1" }} , 
 	{ "name": "real_i_15_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_15", "role": "d1" }} , 
 	{ "name": "real_i_15_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_15", "role": "q1" }} , 
 	{ "name": "real_i_15_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_15", "role": "we1" }} , 
 	{ "name": "real_i_16_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_16", "role": "address0" }} , 
 	{ "name": "real_i_16_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_16", "role": "ce0" }} , 
 	{ "name": "real_i_16_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_16", "role": "d0" }} , 
 	{ "name": "real_i_16_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_16", "role": "q0" }} , 
 	{ "name": "real_i_16_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_16", "role": "we0" }} , 
 	{ "name": "real_i_16_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_16", "role": "address1" }} , 
 	{ "name": "real_i_16_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_16", "role": "ce1" }} , 
 	{ "name": "real_i_16_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_16", "role": "d1" }} , 
 	{ "name": "real_i_16_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_16", "role": "q1" }} , 
 	{ "name": "real_i_16_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_16", "role": "we1" }} , 
 	{ "name": "real_i_17_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_17", "role": "address0" }} , 
 	{ "name": "real_i_17_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_17", "role": "ce0" }} , 
 	{ "name": "real_i_17_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_17", "role": "d0" }} , 
 	{ "name": "real_i_17_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_17", "role": "q0" }} , 
 	{ "name": "real_i_17_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_17", "role": "we0" }} , 
 	{ "name": "real_i_17_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_17", "role": "address1" }} , 
 	{ "name": "real_i_17_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_17", "role": "ce1" }} , 
 	{ "name": "real_i_17_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_17", "role": "d1" }} , 
 	{ "name": "real_i_17_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_17", "role": "q1" }} , 
 	{ "name": "real_i_17_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_17", "role": "we1" }} , 
 	{ "name": "real_i_18_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_18", "role": "address0" }} , 
 	{ "name": "real_i_18_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_18", "role": "ce0" }} , 
 	{ "name": "real_i_18_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_18", "role": "d0" }} , 
 	{ "name": "real_i_18_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_18", "role": "q0" }} , 
 	{ "name": "real_i_18_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_18", "role": "we0" }} , 
 	{ "name": "real_i_18_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_18", "role": "address1" }} , 
 	{ "name": "real_i_18_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_18", "role": "ce1" }} , 
 	{ "name": "real_i_18_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_18", "role": "d1" }} , 
 	{ "name": "real_i_18_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_18", "role": "q1" }} , 
 	{ "name": "real_i_18_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_18", "role": "we1" }} , 
 	{ "name": "real_i_19_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_19", "role": "address0" }} , 
 	{ "name": "real_i_19_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_19", "role": "ce0" }} , 
 	{ "name": "real_i_19_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_19", "role": "d0" }} , 
 	{ "name": "real_i_19_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_19", "role": "q0" }} , 
 	{ "name": "real_i_19_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_19", "role": "we0" }} , 
 	{ "name": "real_i_19_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_19", "role": "address1" }} , 
 	{ "name": "real_i_19_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_19", "role": "ce1" }} , 
 	{ "name": "real_i_19_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_19", "role": "d1" }} , 
 	{ "name": "real_i_19_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_19", "role": "q1" }} , 
 	{ "name": "real_i_19_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_19", "role": "we1" }} , 
 	{ "name": "real_i_20_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_20", "role": "address0" }} , 
 	{ "name": "real_i_20_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_20", "role": "ce0" }} , 
 	{ "name": "real_i_20_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_20", "role": "d0" }} , 
 	{ "name": "real_i_20_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_20", "role": "q0" }} , 
 	{ "name": "real_i_20_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_20", "role": "we0" }} , 
 	{ "name": "real_i_20_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_20", "role": "address1" }} , 
 	{ "name": "real_i_20_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_20", "role": "ce1" }} , 
 	{ "name": "real_i_20_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_20", "role": "d1" }} , 
 	{ "name": "real_i_20_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_20", "role": "q1" }} , 
 	{ "name": "real_i_20_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_20", "role": "we1" }} , 
 	{ "name": "real_i_21_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_21", "role": "address0" }} , 
 	{ "name": "real_i_21_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_21", "role": "ce0" }} , 
 	{ "name": "real_i_21_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_21", "role": "d0" }} , 
 	{ "name": "real_i_21_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_21", "role": "q0" }} , 
 	{ "name": "real_i_21_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_21", "role": "we0" }} , 
 	{ "name": "real_i_21_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_21", "role": "address1" }} , 
 	{ "name": "real_i_21_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_21", "role": "ce1" }} , 
 	{ "name": "real_i_21_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_21", "role": "d1" }} , 
 	{ "name": "real_i_21_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_21", "role": "q1" }} , 
 	{ "name": "real_i_21_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_21", "role": "we1" }} , 
 	{ "name": "real_i_22_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_22", "role": "address0" }} , 
 	{ "name": "real_i_22_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_22", "role": "ce0" }} , 
 	{ "name": "real_i_22_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_22", "role": "d0" }} , 
 	{ "name": "real_i_22_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_22", "role": "q0" }} , 
 	{ "name": "real_i_22_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_22", "role": "we0" }} , 
 	{ "name": "real_i_22_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_22", "role": "address1" }} , 
 	{ "name": "real_i_22_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_22", "role": "ce1" }} , 
 	{ "name": "real_i_22_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_22", "role": "d1" }} , 
 	{ "name": "real_i_22_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_22", "role": "q1" }} , 
 	{ "name": "real_i_22_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_22", "role": "we1" }} , 
 	{ "name": "real_i_23_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_23", "role": "address0" }} , 
 	{ "name": "real_i_23_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_23", "role": "ce0" }} , 
 	{ "name": "real_i_23_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_23", "role": "d0" }} , 
 	{ "name": "real_i_23_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_23", "role": "q0" }} , 
 	{ "name": "real_i_23_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_23", "role": "we0" }} , 
 	{ "name": "real_i_23_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_23", "role": "address1" }} , 
 	{ "name": "real_i_23_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_23", "role": "ce1" }} , 
 	{ "name": "real_i_23_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_23", "role": "d1" }} , 
 	{ "name": "real_i_23_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_23", "role": "q1" }} , 
 	{ "name": "real_i_23_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_23", "role": "we1" }} , 
 	{ "name": "real_i_24_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_24", "role": "address0" }} , 
 	{ "name": "real_i_24_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_24", "role": "ce0" }} , 
 	{ "name": "real_i_24_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_24", "role": "d0" }} , 
 	{ "name": "real_i_24_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_24", "role": "q0" }} , 
 	{ "name": "real_i_24_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_24", "role": "we0" }} , 
 	{ "name": "real_i_24_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_24", "role": "address1" }} , 
 	{ "name": "real_i_24_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_24", "role": "ce1" }} , 
 	{ "name": "real_i_24_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_24", "role": "d1" }} , 
 	{ "name": "real_i_24_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_24", "role": "q1" }} , 
 	{ "name": "real_i_24_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_24", "role": "we1" }} , 
 	{ "name": "real_i_25_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_25", "role": "address0" }} , 
 	{ "name": "real_i_25_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_25", "role": "ce0" }} , 
 	{ "name": "real_i_25_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_25", "role": "d0" }} , 
 	{ "name": "real_i_25_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_25", "role": "q0" }} , 
 	{ "name": "real_i_25_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_25", "role": "we0" }} , 
 	{ "name": "real_i_25_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_25", "role": "address1" }} , 
 	{ "name": "real_i_25_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_25", "role": "ce1" }} , 
 	{ "name": "real_i_25_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_25", "role": "d1" }} , 
 	{ "name": "real_i_25_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_25", "role": "q1" }} , 
 	{ "name": "real_i_25_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_25", "role": "we1" }} , 
 	{ "name": "real_i_26_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_26", "role": "address0" }} , 
 	{ "name": "real_i_26_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_26", "role": "ce0" }} , 
 	{ "name": "real_i_26_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_26", "role": "d0" }} , 
 	{ "name": "real_i_26_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_26", "role": "q0" }} , 
 	{ "name": "real_i_26_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_26", "role": "we0" }} , 
 	{ "name": "real_i_26_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_26", "role": "address1" }} , 
 	{ "name": "real_i_26_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_26", "role": "ce1" }} , 
 	{ "name": "real_i_26_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_26", "role": "d1" }} , 
 	{ "name": "real_i_26_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_26", "role": "q1" }} , 
 	{ "name": "real_i_26_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_26", "role": "we1" }} , 
 	{ "name": "real_i_27_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_27", "role": "address0" }} , 
 	{ "name": "real_i_27_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_27", "role": "ce0" }} , 
 	{ "name": "real_i_27_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_27", "role": "d0" }} , 
 	{ "name": "real_i_27_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_27", "role": "q0" }} , 
 	{ "name": "real_i_27_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_27", "role": "we0" }} , 
 	{ "name": "real_i_27_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_27", "role": "address1" }} , 
 	{ "name": "real_i_27_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_27", "role": "ce1" }} , 
 	{ "name": "real_i_27_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_27", "role": "d1" }} , 
 	{ "name": "real_i_27_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_27", "role": "q1" }} , 
 	{ "name": "real_i_27_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_27", "role": "we1" }} , 
 	{ "name": "real_i_28_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_28", "role": "address0" }} , 
 	{ "name": "real_i_28_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_28", "role": "ce0" }} , 
 	{ "name": "real_i_28_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_28", "role": "d0" }} , 
 	{ "name": "real_i_28_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_28", "role": "q0" }} , 
 	{ "name": "real_i_28_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_28", "role": "we0" }} , 
 	{ "name": "real_i_28_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_28", "role": "address1" }} , 
 	{ "name": "real_i_28_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_28", "role": "ce1" }} , 
 	{ "name": "real_i_28_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_28", "role": "d1" }} , 
 	{ "name": "real_i_28_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_28", "role": "q1" }} , 
 	{ "name": "real_i_28_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_28", "role": "we1" }} , 
 	{ "name": "real_i_29_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_29", "role": "address0" }} , 
 	{ "name": "real_i_29_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_29", "role": "ce0" }} , 
 	{ "name": "real_i_29_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_29", "role": "d0" }} , 
 	{ "name": "real_i_29_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_29", "role": "q0" }} , 
 	{ "name": "real_i_29_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_29", "role": "we0" }} , 
 	{ "name": "real_i_29_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_29", "role": "address1" }} , 
 	{ "name": "real_i_29_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_29", "role": "ce1" }} , 
 	{ "name": "real_i_29_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_29", "role": "d1" }} , 
 	{ "name": "real_i_29_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_29", "role": "q1" }} , 
 	{ "name": "real_i_29_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_29", "role": "we1" }} , 
 	{ "name": "real_i_30_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_30", "role": "address0" }} , 
 	{ "name": "real_i_30_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_30", "role": "ce0" }} , 
 	{ "name": "real_i_30_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_30", "role": "d0" }} , 
 	{ "name": "real_i_30_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_30", "role": "q0" }} , 
 	{ "name": "real_i_30_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_30", "role": "we0" }} , 
 	{ "name": "real_i_30_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_30", "role": "address1" }} , 
 	{ "name": "real_i_30_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_30", "role": "ce1" }} , 
 	{ "name": "real_i_30_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_30", "role": "d1" }} , 
 	{ "name": "real_i_30_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_30", "role": "q1" }} , 
 	{ "name": "real_i_30_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_30", "role": "we1" }} , 
 	{ "name": "real_i_31_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_31", "role": "address0" }} , 
 	{ "name": "real_i_31_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_31", "role": "ce0" }} , 
 	{ "name": "real_i_31_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_31", "role": "d0" }} , 
 	{ "name": "real_i_31_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_31", "role": "q0" }} , 
 	{ "name": "real_i_31_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_31", "role": "we0" }} , 
 	{ "name": "real_i_31_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_31", "role": "address1" }} , 
 	{ "name": "real_i_31_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_31", "role": "ce1" }} , 
 	{ "name": "real_i_31_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_31", "role": "d1" }} , 
 	{ "name": "real_i_31_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_31", "role": "q1" }} , 
 	{ "name": "real_i_31_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_31", "role": "we1" }} , 
 	{ "name": "imag_i_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_0", "role": "address0" }} , 
 	{ "name": "imag_i_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_0", "role": "ce0" }} , 
 	{ "name": "imag_i_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_0", "role": "d0" }} , 
 	{ "name": "imag_i_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_0", "role": "q0" }} , 
 	{ "name": "imag_i_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_0", "role": "we0" }} , 
 	{ "name": "imag_i_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_0", "role": "address1" }} , 
 	{ "name": "imag_i_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_0", "role": "ce1" }} , 
 	{ "name": "imag_i_0_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_0", "role": "d1" }} , 
 	{ "name": "imag_i_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_0", "role": "q1" }} , 
 	{ "name": "imag_i_0_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_0", "role": "we1" }} , 
 	{ "name": "imag_i_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_1", "role": "address0" }} , 
 	{ "name": "imag_i_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_1", "role": "ce0" }} , 
 	{ "name": "imag_i_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_1", "role": "d0" }} , 
 	{ "name": "imag_i_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_1", "role": "q0" }} , 
 	{ "name": "imag_i_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_1", "role": "we0" }} , 
 	{ "name": "imag_i_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_1", "role": "address1" }} , 
 	{ "name": "imag_i_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_1", "role": "ce1" }} , 
 	{ "name": "imag_i_1_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_1", "role": "d1" }} , 
 	{ "name": "imag_i_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_1", "role": "q1" }} , 
 	{ "name": "imag_i_1_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_1", "role": "we1" }} , 
 	{ "name": "imag_i_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_2", "role": "address0" }} , 
 	{ "name": "imag_i_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_2", "role": "ce0" }} , 
 	{ "name": "imag_i_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_2", "role": "d0" }} , 
 	{ "name": "imag_i_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_2", "role": "q0" }} , 
 	{ "name": "imag_i_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_2", "role": "we0" }} , 
 	{ "name": "imag_i_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_2", "role": "address1" }} , 
 	{ "name": "imag_i_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_2", "role": "ce1" }} , 
 	{ "name": "imag_i_2_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_2", "role": "d1" }} , 
 	{ "name": "imag_i_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_2", "role": "q1" }} , 
 	{ "name": "imag_i_2_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_2", "role": "we1" }} , 
 	{ "name": "imag_i_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_3", "role": "address0" }} , 
 	{ "name": "imag_i_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_3", "role": "ce0" }} , 
 	{ "name": "imag_i_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_3", "role": "d0" }} , 
 	{ "name": "imag_i_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_3", "role": "q0" }} , 
 	{ "name": "imag_i_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_3", "role": "we0" }} , 
 	{ "name": "imag_i_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_3", "role": "address1" }} , 
 	{ "name": "imag_i_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_3", "role": "ce1" }} , 
 	{ "name": "imag_i_3_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_3", "role": "d1" }} , 
 	{ "name": "imag_i_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_3", "role": "q1" }} , 
 	{ "name": "imag_i_3_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_3", "role": "we1" }} , 
 	{ "name": "imag_i_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_4", "role": "address0" }} , 
 	{ "name": "imag_i_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_4", "role": "ce0" }} , 
 	{ "name": "imag_i_4_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_4", "role": "d0" }} , 
 	{ "name": "imag_i_4_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_4", "role": "q0" }} , 
 	{ "name": "imag_i_4_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_4", "role": "we0" }} , 
 	{ "name": "imag_i_4_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_4", "role": "address1" }} , 
 	{ "name": "imag_i_4_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_4", "role": "ce1" }} , 
 	{ "name": "imag_i_4_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_4", "role": "d1" }} , 
 	{ "name": "imag_i_4_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_4", "role": "q1" }} , 
 	{ "name": "imag_i_4_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_4", "role": "we1" }} , 
 	{ "name": "imag_i_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_5", "role": "address0" }} , 
 	{ "name": "imag_i_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_5", "role": "ce0" }} , 
 	{ "name": "imag_i_5_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_5", "role": "d0" }} , 
 	{ "name": "imag_i_5_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_5", "role": "q0" }} , 
 	{ "name": "imag_i_5_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_5", "role": "we0" }} , 
 	{ "name": "imag_i_5_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_5", "role": "address1" }} , 
 	{ "name": "imag_i_5_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_5", "role": "ce1" }} , 
 	{ "name": "imag_i_5_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_5", "role": "d1" }} , 
 	{ "name": "imag_i_5_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_5", "role": "q1" }} , 
 	{ "name": "imag_i_5_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_5", "role": "we1" }} , 
 	{ "name": "imag_i_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_6", "role": "address0" }} , 
 	{ "name": "imag_i_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_6", "role": "ce0" }} , 
 	{ "name": "imag_i_6_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_6", "role": "d0" }} , 
 	{ "name": "imag_i_6_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_6", "role": "q0" }} , 
 	{ "name": "imag_i_6_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_6", "role": "we0" }} , 
 	{ "name": "imag_i_6_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_6", "role": "address1" }} , 
 	{ "name": "imag_i_6_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_6", "role": "ce1" }} , 
 	{ "name": "imag_i_6_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_6", "role": "d1" }} , 
 	{ "name": "imag_i_6_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_6", "role": "q1" }} , 
 	{ "name": "imag_i_6_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_6", "role": "we1" }} , 
 	{ "name": "imag_i_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_7", "role": "address0" }} , 
 	{ "name": "imag_i_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_7", "role": "ce0" }} , 
 	{ "name": "imag_i_7_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_7", "role": "d0" }} , 
 	{ "name": "imag_i_7_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_7", "role": "q0" }} , 
 	{ "name": "imag_i_7_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_7", "role": "we0" }} , 
 	{ "name": "imag_i_7_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_7", "role": "address1" }} , 
 	{ "name": "imag_i_7_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_7", "role": "ce1" }} , 
 	{ "name": "imag_i_7_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_7", "role": "d1" }} , 
 	{ "name": "imag_i_7_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_7", "role": "q1" }} , 
 	{ "name": "imag_i_7_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_7", "role": "we1" }} , 
 	{ "name": "imag_i_8_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_8", "role": "address0" }} , 
 	{ "name": "imag_i_8_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_8", "role": "ce0" }} , 
 	{ "name": "imag_i_8_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_8", "role": "d0" }} , 
 	{ "name": "imag_i_8_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_8", "role": "q0" }} , 
 	{ "name": "imag_i_8_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_8", "role": "we0" }} , 
 	{ "name": "imag_i_8_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_8", "role": "address1" }} , 
 	{ "name": "imag_i_8_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_8", "role": "ce1" }} , 
 	{ "name": "imag_i_8_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_8", "role": "d1" }} , 
 	{ "name": "imag_i_8_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_8", "role": "q1" }} , 
 	{ "name": "imag_i_8_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_8", "role": "we1" }} , 
 	{ "name": "imag_i_9_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_9", "role": "address0" }} , 
 	{ "name": "imag_i_9_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_9", "role": "ce0" }} , 
 	{ "name": "imag_i_9_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_9", "role": "d0" }} , 
 	{ "name": "imag_i_9_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_9", "role": "q0" }} , 
 	{ "name": "imag_i_9_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_9", "role": "we0" }} , 
 	{ "name": "imag_i_9_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_9", "role": "address1" }} , 
 	{ "name": "imag_i_9_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_9", "role": "ce1" }} , 
 	{ "name": "imag_i_9_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_9", "role": "d1" }} , 
 	{ "name": "imag_i_9_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_9", "role": "q1" }} , 
 	{ "name": "imag_i_9_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_9", "role": "we1" }} , 
 	{ "name": "imag_i_10_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_10", "role": "address0" }} , 
 	{ "name": "imag_i_10_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_10", "role": "ce0" }} , 
 	{ "name": "imag_i_10_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_10", "role": "d0" }} , 
 	{ "name": "imag_i_10_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_10", "role": "q0" }} , 
 	{ "name": "imag_i_10_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_10", "role": "we0" }} , 
 	{ "name": "imag_i_10_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_10", "role": "address1" }} , 
 	{ "name": "imag_i_10_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_10", "role": "ce1" }} , 
 	{ "name": "imag_i_10_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_10", "role": "d1" }} , 
 	{ "name": "imag_i_10_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_10", "role": "q1" }} , 
 	{ "name": "imag_i_10_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_10", "role": "we1" }} , 
 	{ "name": "imag_i_11_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_11", "role": "address0" }} , 
 	{ "name": "imag_i_11_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_11", "role": "ce0" }} , 
 	{ "name": "imag_i_11_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_11", "role": "d0" }} , 
 	{ "name": "imag_i_11_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_11", "role": "q0" }} , 
 	{ "name": "imag_i_11_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_11", "role": "we0" }} , 
 	{ "name": "imag_i_11_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_11", "role": "address1" }} , 
 	{ "name": "imag_i_11_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_11", "role": "ce1" }} , 
 	{ "name": "imag_i_11_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_11", "role": "d1" }} , 
 	{ "name": "imag_i_11_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_11", "role": "q1" }} , 
 	{ "name": "imag_i_11_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_11", "role": "we1" }} , 
 	{ "name": "imag_i_12_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_12", "role": "address0" }} , 
 	{ "name": "imag_i_12_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_12", "role": "ce0" }} , 
 	{ "name": "imag_i_12_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_12", "role": "d0" }} , 
 	{ "name": "imag_i_12_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_12", "role": "q0" }} , 
 	{ "name": "imag_i_12_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_12", "role": "we0" }} , 
 	{ "name": "imag_i_12_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_12", "role": "address1" }} , 
 	{ "name": "imag_i_12_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_12", "role": "ce1" }} , 
 	{ "name": "imag_i_12_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_12", "role": "d1" }} , 
 	{ "name": "imag_i_12_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_12", "role": "q1" }} , 
 	{ "name": "imag_i_12_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_12", "role": "we1" }} , 
 	{ "name": "imag_i_13_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_13", "role": "address0" }} , 
 	{ "name": "imag_i_13_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_13", "role": "ce0" }} , 
 	{ "name": "imag_i_13_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_13", "role": "d0" }} , 
 	{ "name": "imag_i_13_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_13", "role": "q0" }} , 
 	{ "name": "imag_i_13_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_13", "role": "we0" }} , 
 	{ "name": "imag_i_13_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_13", "role": "address1" }} , 
 	{ "name": "imag_i_13_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_13", "role": "ce1" }} , 
 	{ "name": "imag_i_13_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_13", "role": "d1" }} , 
 	{ "name": "imag_i_13_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_13", "role": "q1" }} , 
 	{ "name": "imag_i_13_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_13", "role": "we1" }} , 
 	{ "name": "imag_i_14_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_14", "role": "address0" }} , 
 	{ "name": "imag_i_14_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_14", "role": "ce0" }} , 
 	{ "name": "imag_i_14_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_14", "role": "d0" }} , 
 	{ "name": "imag_i_14_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_14", "role": "q0" }} , 
 	{ "name": "imag_i_14_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_14", "role": "we0" }} , 
 	{ "name": "imag_i_14_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_14", "role": "address1" }} , 
 	{ "name": "imag_i_14_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_14", "role": "ce1" }} , 
 	{ "name": "imag_i_14_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_14", "role": "d1" }} , 
 	{ "name": "imag_i_14_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_14", "role": "q1" }} , 
 	{ "name": "imag_i_14_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_14", "role": "we1" }} , 
 	{ "name": "imag_i_15_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_15", "role": "address0" }} , 
 	{ "name": "imag_i_15_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_15", "role": "ce0" }} , 
 	{ "name": "imag_i_15_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_15", "role": "d0" }} , 
 	{ "name": "imag_i_15_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_15", "role": "q0" }} , 
 	{ "name": "imag_i_15_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_15", "role": "we0" }} , 
 	{ "name": "imag_i_15_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_15", "role": "address1" }} , 
 	{ "name": "imag_i_15_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_15", "role": "ce1" }} , 
 	{ "name": "imag_i_15_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_15", "role": "d1" }} , 
 	{ "name": "imag_i_15_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_15", "role": "q1" }} , 
 	{ "name": "imag_i_15_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_15", "role": "we1" }} , 
 	{ "name": "imag_i_16_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_16", "role": "address0" }} , 
 	{ "name": "imag_i_16_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_16", "role": "ce0" }} , 
 	{ "name": "imag_i_16_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_16", "role": "d0" }} , 
 	{ "name": "imag_i_16_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_16", "role": "q0" }} , 
 	{ "name": "imag_i_16_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_16", "role": "we0" }} , 
 	{ "name": "imag_i_16_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_16", "role": "address1" }} , 
 	{ "name": "imag_i_16_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_16", "role": "ce1" }} , 
 	{ "name": "imag_i_16_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_16", "role": "d1" }} , 
 	{ "name": "imag_i_16_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_16", "role": "q1" }} , 
 	{ "name": "imag_i_16_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_16", "role": "we1" }} , 
 	{ "name": "imag_i_17_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_17", "role": "address0" }} , 
 	{ "name": "imag_i_17_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_17", "role": "ce0" }} , 
 	{ "name": "imag_i_17_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_17", "role": "d0" }} , 
 	{ "name": "imag_i_17_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_17", "role": "q0" }} , 
 	{ "name": "imag_i_17_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_17", "role": "we0" }} , 
 	{ "name": "imag_i_17_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_17", "role": "address1" }} , 
 	{ "name": "imag_i_17_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_17", "role": "ce1" }} , 
 	{ "name": "imag_i_17_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_17", "role": "d1" }} , 
 	{ "name": "imag_i_17_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_17", "role": "q1" }} , 
 	{ "name": "imag_i_17_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_17", "role": "we1" }} , 
 	{ "name": "imag_i_18_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_18", "role": "address0" }} , 
 	{ "name": "imag_i_18_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_18", "role": "ce0" }} , 
 	{ "name": "imag_i_18_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_18", "role": "d0" }} , 
 	{ "name": "imag_i_18_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_18", "role": "q0" }} , 
 	{ "name": "imag_i_18_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_18", "role": "we0" }} , 
 	{ "name": "imag_i_18_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_18", "role": "address1" }} , 
 	{ "name": "imag_i_18_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_18", "role": "ce1" }} , 
 	{ "name": "imag_i_18_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_18", "role": "d1" }} , 
 	{ "name": "imag_i_18_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_18", "role": "q1" }} , 
 	{ "name": "imag_i_18_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_18", "role": "we1" }} , 
 	{ "name": "imag_i_19_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_19", "role": "address0" }} , 
 	{ "name": "imag_i_19_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_19", "role": "ce0" }} , 
 	{ "name": "imag_i_19_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_19", "role": "d0" }} , 
 	{ "name": "imag_i_19_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_19", "role": "q0" }} , 
 	{ "name": "imag_i_19_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_19", "role": "we0" }} , 
 	{ "name": "imag_i_19_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_19", "role": "address1" }} , 
 	{ "name": "imag_i_19_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_19", "role": "ce1" }} , 
 	{ "name": "imag_i_19_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_19", "role": "d1" }} , 
 	{ "name": "imag_i_19_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_19", "role": "q1" }} , 
 	{ "name": "imag_i_19_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_19", "role": "we1" }} , 
 	{ "name": "imag_i_20_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_20", "role": "address0" }} , 
 	{ "name": "imag_i_20_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_20", "role": "ce0" }} , 
 	{ "name": "imag_i_20_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_20", "role": "d0" }} , 
 	{ "name": "imag_i_20_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_20", "role": "q0" }} , 
 	{ "name": "imag_i_20_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_20", "role": "we0" }} , 
 	{ "name": "imag_i_20_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_20", "role": "address1" }} , 
 	{ "name": "imag_i_20_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_20", "role": "ce1" }} , 
 	{ "name": "imag_i_20_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_20", "role": "d1" }} , 
 	{ "name": "imag_i_20_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_20", "role": "q1" }} , 
 	{ "name": "imag_i_20_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_20", "role": "we1" }} , 
 	{ "name": "imag_i_21_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_21", "role": "address0" }} , 
 	{ "name": "imag_i_21_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_21", "role": "ce0" }} , 
 	{ "name": "imag_i_21_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_21", "role": "d0" }} , 
 	{ "name": "imag_i_21_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_21", "role": "q0" }} , 
 	{ "name": "imag_i_21_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_21", "role": "we0" }} , 
 	{ "name": "imag_i_21_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_21", "role": "address1" }} , 
 	{ "name": "imag_i_21_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_21", "role": "ce1" }} , 
 	{ "name": "imag_i_21_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_21", "role": "d1" }} , 
 	{ "name": "imag_i_21_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_21", "role": "q1" }} , 
 	{ "name": "imag_i_21_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_21", "role": "we1" }} , 
 	{ "name": "imag_i_22_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_22", "role": "address0" }} , 
 	{ "name": "imag_i_22_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_22", "role": "ce0" }} , 
 	{ "name": "imag_i_22_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_22", "role": "d0" }} , 
 	{ "name": "imag_i_22_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_22", "role": "q0" }} , 
 	{ "name": "imag_i_22_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_22", "role": "we0" }} , 
 	{ "name": "imag_i_22_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_22", "role": "address1" }} , 
 	{ "name": "imag_i_22_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_22", "role": "ce1" }} , 
 	{ "name": "imag_i_22_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_22", "role": "d1" }} , 
 	{ "name": "imag_i_22_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_22", "role": "q1" }} , 
 	{ "name": "imag_i_22_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_22", "role": "we1" }} , 
 	{ "name": "imag_i_23_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_23", "role": "address0" }} , 
 	{ "name": "imag_i_23_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_23", "role": "ce0" }} , 
 	{ "name": "imag_i_23_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_23", "role": "d0" }} , 
 	{ "name": "imag_i_23_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_23", "role": "q0" }} , 
 	{ "name": "imag_i_23_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_23", "role": "we0" }} , 
 	{ "name": "imag_i_23_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_23", "role": "address1" }} , 
 	{ "name": "imag_i_23_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_23", "role": "ce1" }} , 
 	{ "name": "imag_i_23_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_23", "role": "d1" }} , 
 	{ "name": "imag_i_23_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_23", "role": "q1" }} , 
 	{ "name": "imag_i_23_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_23", "role": "we1" }} , 
 	{ "name": "imag_i_24_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_24", "role": "address0" }} , 
 	{ "name": "imag_i_24_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_24", "role": "ce0" }} , 
 	{ "name": "imag_i_24_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_24", "role": "d0" }} , 
 	{ "name": "imag_i_24_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_24", "role": "q0" }} , 
 	{ "name": "imag_i_24_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_24", "role": "we0" }} , 
 	{ "name": "imag_i_24_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_24", "role": "address1" }} , 
 	{ "name": "imag_i_24_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_24", "role": "ce1" }} , 
 	{ "name": "imag_i_24_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_24", "role": "d1" }} , 
 	{ "name": "imag_i_24_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_24", "role": "q1" }} , 
 	{ "name": "imag_i_24_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_24", "role": "we1" }} , 
 	{ "name": "imag_i_25_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_25", "role": "address0" }} , 
 	{ "name": "imag_i_25_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_25", "role": "ce0" }} , 
 	{ "name": "imag_i_25_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_25", "role": "d0" }} , 
 	{ "name": "imag_i_25_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_25", "role": "q0" }} , 
 	{ "name": "imag_i_25_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_25", "role": "we0" }} , 
 	{ "name": "imag_i_25_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_25", "role": "address1" }} , 
 	{ "name": "imag_i_25_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_25", "role": "ce1" }} , 
 	{ "name": "imag_i_25_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_25", "role": "d1" }} , 
 	{ "name": "imag_i_25_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_25", "role": "q1" }} , 
 	{ "name": "imag_i_25_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_25", "role": "we1" }} , 
 	{ "name": "imag_i_26_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_26", "role": "address0" }} , 
 	{ "name": "imag_i_26_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_26", "role": "ce0" }} , 
 	{ "name": "imag_i_26_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_26", "role": "d0" }} , 
 	{ "name": "imag_i_26_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_26", "role": "q0" }} , 
 	{ "name": "imag_i_26_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_26", "role": "we0" }} , 
 	{ "name": "imag_i_26_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_26", "role": "address1" }} , 
 	{ "name": "imag_i_26_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_26", "role": "ce1" }} , 
 	{ "name": "imag_i_26_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_26", "role": "d1" }} , 
 	{ "name": "imag_i_26_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_26", "role": "q1" }} , 
 	{ "name": "imag_i_26_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_26", "role": "we1" }} , 
 	{ "name": "imag_i_27_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_27", "role": "address0" }} , 
 	{ "name": "imag_i_27_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_27", "role": "ce0" }} , 
 	{ "name": "imag_i_27_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_27", "role": "d0" }} , 
 	{ "name": "imag_i_27_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_27", "role": "q0" }} , 
 	{ "name": "imag_i_27_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_27", "role": "we0" }} , 
 	{ "name": "imag_i_27_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_27", "role": "address1" }} , 
 	{ "name": "imag_i_27_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_27", "role": "ce1" }} , 
 	{ "name": "imag_i_27_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_27", "role": "d1" }} , 
 	{ "name": "imag_i_27_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_27", "role": "q1" }} , 
 	{ "name": "imag_i_27_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_27", "role": "we1" }} , 
 	{ "name": "imag_i_28_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_28", "role": "address0" }} , 
 	{ "name": "imag_i_28_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_28", "role": "ce0" }} , 
 	{ "name": "imag_i_28_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_28", "role": "d0" }} , 
 	{ "name": "imag_i_28_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_28", "role": "q0" }} , 
 	{ "name": "imag_i_28_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_28", "role": "we0" }} , 
 	{ "name": "imag_i_28_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_28", "role": "address1" }} , 
 	{ "name": "imag_i_28_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_28", "role": "ce1" }} , 
 	{ "name": "imag_i_28_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_28", "role": "d1" }} , 
 	{ "name": "imag_i_28_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_28", "role": "q1" }} , 
 	{ "name": "imag_i_28_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_28", "role": "we1" }} , 
 	{ "name": "imag_i_29_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_29", "role": "address0" }} , 
 	{ "name": "imag_i_29_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_29", "role": "ce0" }} , 
 	{ "name": "imag_i_29_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_29", "role": "d0" }} , 
 	{ "name": "imag_i_29_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_29", "role": "q0" }} , 
 	{ "name": "imag_i_29_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_29", "role": "we0" }} , 
 	{ "name": "imag_i_29_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_29", "role": "address1" }} , 
 	{ "name": "imag_i_29_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_29", "role": "ce1" }} , 
 	{ "name": "imag_i_29_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_29", "role": "d1" }} , 
 	{ "name": "imag_i_29_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_29", "role": "q1" }} , 
 	{ "name": "imag_i_29_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_29", "role": "we1" }} , 
 	{ "name": "imag_i_30_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_30", "role": "address0" }} , 
 	{ "name": "imag_i_30_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_30", "role": "ce0" }} , 
 	{ "name": "imag_i_30_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_30", "role": "d0" }} , 
 	{ "name": "imag_i_30_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_30", "role": "q0" }} , 
 	{ "name": "imag_i_30_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_30", "role": "we0" }} , 
 	{ "name": "imag_i_30_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_30", "role": "address1" }} , 
 	{ "name": "imag_i_30_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_30", "role": "ce1" }} , 
 	{ "name": "imag_i_30_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_30", "role": "d1" }} , 
 	{ "name": "imag_i_30_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_30", "role": "q1" }} , 
 	{ "name": "imag_i_30_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_30", "role": "we1" }} , 
 	{ "name": "imag_i_31_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_31", "role": "address0" }} , 
 	{ "name": "imag_i_31_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_31", "role": "ce0" }} , 
 	{ "name": "imag_i_31_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_31", "role": "d0" }} , 
 	{ "name": "imag_i_31_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_31", "role": "q0" }} , 
 	{ "name": "imag_i_31_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_31", "role": "we0" }} , 
 	{ "name": "imag_i_31_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_31", "role": "address1" }} , 
 	{ "name": "imag_i_31_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_31", "role": "ce1" }} , 
 	{ "name": "imag_i_31_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_31", "role": "d1" }} , 
 	{ "name": "imag_i_31_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_31", "role": "q1" }} , 
 	{ "name": "imag_i_31_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_31", "role": "we1" }} , 
 	{ "name": "real_o_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_o", "role": "address0" }} , 
 	{ "name": "real_o_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o", "role": "ce0" }} , 
 	{ "name": "real_o_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o", "role": "d0" }} , 
 	{ "name": "real_o_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o", "role": "q0" }} , 
 	{ "name": "real_o_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o", "role": "we0" }} , 
 	{ "name": "real_o_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_o", "role": "address1" }} , 
 	{ "name": "real_o_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o", "role": "ce1" }} , 
 	{ "name": "real_o_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o", "role": "d1" }} , 
 	{ "name": "real_o_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o", "role": "q1" }} , 
 	{ "name": "real_o_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o", "role": "we1" }} , 
 	{ "name": "imag_o_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "imag_o", "role": "address0" }} , 
 	{ "name": "imag_o_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o", "role": "ce0" }} , 
 	{ "name": "imag_o_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o", "role": "d0" }} , 
 	{ "name": "imag_o_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o", "role": "q0" }} , 
 	{ "name": "imag_o_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o", "role": "we0" }} , 
 	{ "name": "imag_o_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "imag_o", "role": "address1" }} , 
 	{ "name": "imag_o_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o", "role": "ce1" }} , 
 	{ "name": "imag_o_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o", "role": "d1" }} , 
 	{ "name": "imag_o_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o", "role": "q1" }} , 
 	{ "name": "imag_o_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o", "role": "we1" }} , 
 	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }}  ]}
set Spec2ImplPortList { 
	real_i_0 { ap_memory {  { real_i_0_address0 mem_address 1 3 }  { real_i_0_ce0 mem_ce 1 1 }  { real_i_0_d0 mem_din 1 32 }  { real_i_0_q0 mem_dout 0 32 }  { real_i_0_we0 mem_we 1 1 }  { real_i_0_address1 mem_address 1 3 }  { real_i_0_ce1 mem_ce 1 1 }  { real_i_0_d1 mem_din 1 32 }  { real_i_0_q1 mem_dout 0 32 }  { real_i_0_we1 mem_we 1 1 } } }
	real_i_1 { ap_memory {  { real_i_1_address0 mem_address 1 3 }  { real_i_1_ce0 mem_ce 1 1 }  { real_i_1_d0 mem_din 1 32 }  { real_i_1_q0 mem_dout 0 32 }  { real_i_1_we0 mem_we 1 1 }  { real_i_1_address1 mem_address 1 3 }  { real_i_1_ce1 mem_ce 1 1 }  { real_i_1_d1 mem_din 1 32 }  { real_i_1_q1 mem_dout 0 32 }  { real_i_1_we1 mem_we 1 1 } } }
	real_i_2 { ap_memory {  { real_i_2_address0 mem_address 1 3 }  { real_i_2_ce0 mem_ce 1 1 }  { real_i_2_d0 mem_din 1 32 }  { real_i_2_q0 mem_dout 0 32 }  { real_i_2_we0 mem_we 1 1 }  { real_i_2_address1 mem_address 1 3 }  { real_i_2_ce1 mem_ce 1 1 }  { real_i_2_d1 mem_din 1 32 }  { real_i_2_q1 mem_dout 0 32 }  { real_i_2_we1 mem_we 1 1 } } }
	real_i_3 { ap_memory {  { real_i_3_address0 mem_address 1 3 }  { real_i_3_ce0 mem_ce 1 1 }  { real_i_3_d0 mem_din 1 32 }  { real_i_3_q0 mem_dout 0 32 }  { real_i_3_we0 mem_we 1 1 }  { real_i_3_address1 mem_address 1 3 }  { real_i_3_ce1 mem_ce 1 1 }  { real_i_3_d1 mem_din 1 32 }  { real_i_3_q1 mem_dout 0 32 }  { real_i_3_we1 mem_we 1 1 } } }
	real_i_4 { ap_memory {  { real_i_4_address0 mem_address 1 3 }  { real_i_4_ce0 mem_ce 1 1 }  { real_i_4_d0 mem_din 1 32 }  { real_i_4_q0 mem_dout 0 32 }  { real_i_4_we0 mem_we 1 1 }  { real_i_4_address1 mem_address 1 3 }  { real_i_4_ce1 mem_ce 1 1 }  { real_i_4_d1 mem_din 1 32 }  { real_i_4_q1 mem_dout 0 32 }  { real_i_4_we1 mem_we 1 1 } } }
	real_i_5 { ap_memory {  { real_i_5_address0 mem_address 1 3 }  { real_i_5_ce0 mem_ce 1 1 }  { real_i_5_d0 mem_din 1 32 }  { real_i_5_q0 mem_dout 0 32 }  { real_i_5_we0 mem_we 1 1 }  { real_i_5_address1 mem_address 1 3 }  { real_i_5_ce1 mem_ce 1 1 }  { real_i_5_d1 mem_din 1 32 }  { real_i_5_q1 mem_dout 0 32 }  { real_i_5_we1 mem_we 1 1 } } }
	real_i_6 { ap_memory {  { real_i_6_address0 mem_address 1 3 }  { real_i_6_ce0 mem_ce 1 1 }  { real_i_6_d0 mem_din 1 32 }  { real_i_6_q0 mem_dout 0 32 }  { real_i_6_we0 mem_we 1 1 }  { real_i_6_address1 mem_address 1 3 }  { real_i_6_ce1 mem_ce 1 1 }  { real_i_6_d1 mem_din 1 32 }  { real_i_6_q1 mem_dout 0 32 }  { real_i_6_we1 mem_we 1 1 } } }
	real_i_7 { ap_memory {  { real_i_7_address0 mem_address 1 3 }  { real_i_7_ce0 mem_ce 1 1 }  { real_i_7_d0 mem_din 1 32 }  { real_i_7_q0 mem_dout 0 32 }  { real_i_7_we0 mem_we 1 1 }  { real_i_7_address1 mem_address 1 3 }  { real_i_7_ce1 mem_ce 1 1 }  { real_i_7_d1 mem_din 1 32 }  { real_i_7_q1 mem_dout 0 32 }  { real_i_7_we1 mem_we 1 1 } } }
	real_i_8 { ap_memory {  { real_i_8_address0 mem_address 1 3 }  { real_i_8_ce0 mem_ce 1 1 }  { real_i_8_d0 mem_din 1 32 }  { real_i_8_q0 mem_dout 0 32 }  { real_i_8_we0 mem_we 1 1 }  { real_i_8_address1 mem_address 1 3 }  { real_i_8_ce1 mem_ce 1 1 }  { real_i_8_d1 mem_din 1 32 }  { real_i_8_q1 mem_dout 0 32 }  { real_i_8_we1 mem_we 1 1 } } }
	real_i_9 { ap_memory {  { real_i_9_address0 mem_address 1 3 }  { real_i_9_ce0 mem_ce 1 1 }  { real_i_9_d0 mem_din 1 32 }  { real_i_9_q0 mem_dout 0 32 }  { real_i_9_we0 mem_we 1 1 }  { real_i_9_address1 mem_address 1 3 }  { real_i_9_ce1 mem_ce 1 1 }  { real_i_9_d1 mem_din 1 32 }  { real_i_9_q1 mem_dout 0 32 }  { real_i_9_we1 mem_we 1 1 } } }
	real_i_10 { ap_memory {  { real_i_10_address0 mem_address 1 3 }  { real_i_10_ce0 mem_ce 1 1 }  { real_i_10_d0 mem_din 1 32 }  { real_i_10_q0 mem_dout 0 32 }  { real_i_10_we0 mem_we 1 1 }  { real_i_10_address1 mem_address 1 3 }  { real_i_10_ce1 mem_ce 1 1 }  { real_i_10_d1 mem_din 1 32 }  { real_i_10_q1 mem_dout 0 32 }  { real_i_10_we1 mem_we 1 1 } } }
	real_i_11 { ap_memory {  { real_i_11_address0 mem_address 1 3 }  { real_i_11_ce0 mem_ce 1 1 }  { real_i_11_d0 mem_din 1 32 }  { real_i_11_q0 mem_dout 0 32 }  { real_i_11_we0 mem_we 1 1 }  { real_i_11_address1 mem_address 1 3 }  { real_i_11_ce1 mem_ce 1 1 }  { real_i_11_d1 mem_din 1 32 }  { real_i_11_q1 mem_dout 0 32 }  { real_i_11_we1 mem_we 1 1 } } }
	real_i_12 { ap_memory {  { real_i_12_address0 mem_address 1 3 }  { real_i_12_ce0 mem_ce 1 1 }  { real_i_12_d0 mem_din 1 32 }  { real_i_12_q0 mem_dout 0 32 }  { real_i_12_we0 mem_we 1 1 }  { real_i_12_address1 mem_address 1 3 }  { real_i_12_ce1 mem_ce 1 1 }  { real_i_12_d1 mem_din 1 32 }  { real_i_12_q1 mem_dout 0 32 }  { real_i_12_we1 mem_we 1 1 } } }
	real_i_13 { ap_memory {  { real_i_13_address0 mem_address 1 3 }  { real_i_13_ce0 mem_ce 1 1 }  { real_i_13_d0 mem_din 1 32 }  { real_i_13_q0 mem_dout 0 32 }  { real_i_13_we0 mem_we 1 1 }  { real_i_13_address1 mem_address 1 3 }  { real_i_13_ce1 mem_ce 1 1 }  { real_i_13_d1 mem_din 1 32 }  { real_i_13_q1 mem_dout 0 32 }  { real_i_13_we1 mem_we 1 1 } } }
	real_i_14 { ap_memory {  { real_i_14_address0 mem_address 1 3 }  { real_i_14_ce0 mem_ce 1 1 }  { real_i_14_d0 mem_din 1 32 }  { real_i_14_q0 mem_dout 0 32 }  { real_i_14_we0 mem_we 1 1 }  { real_i_14_address1 mem_address 1 3 }  { real_i_14_ce1 mem_ce 1 1 }  { real_i_14_d1 mem_din 1 32 }  { real_i_14_q1 mem_dout 0 32 }  { real_i_14_we1 mem_we 1 1 } } }
	real_i_15 { ap_memory {  { real_i_15_address0 mem_address 1 3 }  { real_i_15_ce0 mem_ce 1 1 }  { real_i_15_d0 mem_din 1 32 }  { real_i_15_q0 mem_dout 0 32 }  { real_i_15_we0 mem_we 1 1 }  { real_i_15_address1 mem_address 1 3 }  { real_i_15_ce1 mem_ce 1 1 }  { real_i_15_d1 mem_din 1 32 }  { real_i_15_q1 mem_dout 0 32 }  { real_i_15_we1 mem_we 1 1 } } }
	real_i_16 { ap_memory {  { real_i_16_address0 mem_address 1 3 }  { real_i_16_ce0 mem_ce 1 1 }  { real_i_16_d0 mem_din 1 32 }  { real_i_16_q0 mem_dout 0 32 }  { real_i_16_we0 mem_we 1 1 }  { real_i_16_address1 mem_address 1 3 }  { real_i_16_ce1 mem_ce 1 1 }  { real_i_16_d1 mem_din 1 32 }  { real_i_16_q1 mem_dout 0 32 }  { real_i_16_we1 mem_we 1 1 } } }
	real_i_17 { ap_memory {  { real_i_17_address0 mem_address 1 3 }  { real_i_17_ce0 mem_ce 1 1 }  { real_i_17_d0 mem_din 1 32 }  { real_i_17_q0 mem_dout 0 32 }  { real_i_17_we0 mem_we 1 1 }  { real_i_17_address1 mem_address 1 3 }  { real_i_17_ce1 mem_ce 1 1 }  { real_i_17_d1 mem_din 1 32 }  { real_i_17_q1 mem_dout 0 32 }  { real_i_17_we1 mem_we 1 1 } } }
	real_i_18 { ap_memory {  { real_i_18_address0 mem_address 1 3 }  { real_i_18_ce0 mem_ce 1 1 }  { real_i_18_d0 mem_din 1 32 }  { real_i_18_q0 mem_dout 0 32 }  { real_i_18_we0 mem_we 1 1 }  { real_i_18_address1 mem_address 1 3 }  { real_i_18_ce1 mem_ce 1 1 }  { real_i_18_d1 mem_din 1 32 }  { real_i_18_q1 mem_dout 0 32 }  { real_i_18_we1 mem_we 1 1 } } }
	real_i_19 { ap_memory {  { real_i_19_address0 mem_address 1 3 }  { real_i_19_ce0 mem_ce 1 1 }  { real_i_19_d0 mem_din 1 32 }  { real_i_19_q0 mem_dout 0 32 }  { real_i_19_we0 mem_we 1 1 }  { real_i_19_address1 mem_address 1 3 }  { real_i_19_ce1 mem_ce 1 1 }  { real_i_19_d1 mem_din 1 32 }  { real_i_19_q1 mem_dout 0 32 }  { real_i_19_we1 mem_we 1 1 } } }
	real_i_20 { ap_memory {  { real_i_20_address0 mem_address 1 3 }  { real_i_20_ce0 mem_ce 1 1 }  { real_i_20_d0 mem_din 1 32 }  { real_i_20_q0 mem_dout 0 32 }  { real_i_20_we0 mem_we 1 1 }  { real_i_20_address1 mem_address 1 3 }  { real_i_20_ce1 mem_ce 1 1 }  { real_i_20_d1 mem_din 1 32 }  { real_i_20_q1 mem_dout 0 32 }  { real_i_20_we1 mem_we 1 1 } } }
	real_i_21 { ap_memory {  { real_i_21_address0 mem_address 1 3 }  { real_i_21_ce0 mem_ce 1 1 }  { real_i_21_d0 mem_din 1 32 }  { real_i_21_q0 mem_dout 0 32 }  { real_i_21_we0 mem_we 1 1 }  { real_i_21_address1 mem_address 1 3 }  { real_i_21_ce1 mem_ce 1 1 }  { real_i_21_d1 mem_din 1 32 }  { real_i_21_q1 mem_dout 0 32 }  { real_i_21_we1 mem_we 1 1 } } }
	real_i_22 { ap_memory {  { real_i_22_address0 mem_address 1 3 }  { real_i_22_ce0 mem_ce 1 1 }  { real_i_22_d0 mem_din 1 32 }  { real_i_22_q0 mem_dout 0 32 }  { real_i_22_we0 mem_we 1 1 }  { real_i_22_address1 mem_address 1 3 }  { real_i_22_ce1 mem_ce 1 1 }  { real_i_22_d1 mem_din 1 32 }  { real_i_22_q1 mem_dout 0 32 }  { real_i_22_we1 mem_we 1 1 } } }
	real_i_23 { ap_memory {  { real_i_23_address0 mem_address 1 3 }  { real_i_23_ce0 mem_ce 1 1 }  { real_i_23_d0 mem_din 1 32 }  { real_i_23_q0 mem_dout 0 32 }  { real_i_23_we0 mem_we 1 1 }  { real_i_23_address1 mem_address 1 3 }  { real_i_23_ce1 mem_ce 1 1 }  { real_i_23_d1 mem_din 1 32 }  { real_i_23_q1 mem_dout 0 32 }  { real_i_23_we1 mem_we 1 1 } } }
	real_i_24 { ap_memory {  { real_i_24_address0 mem_address 1 3 }  { real_i_24_ce0 mem_ce 1 1 }  { real_i_24_d0 mem_din 1 32 }  { real_i_24_q0 mem_dout 0 32 }  { real_i_24_we0 mem_we 1 1 }  { real_i_24_address1 mem_address 1 3 }  { real_i_24_ce1 mem_ce 1 1 }  { real_i_24_d1 mem_din 1 32 }  { real_i_24_q1 mem_dout 0 32 }  { real_i_24_we1 mem_we 1 1 } } }
	real_i_25 { ap_memory {  { real_i_25_address0 mem_address 1 3 }  { real_i_25_ce0 mem_ce 1 1 }  { real_i_25_d0 mem_din 1 32 }  { real_i_25_q0 mem_dout 0 32 }  { real_i_25_we0 mem_we 1 1 }  { real_i_25_address1 mem_address 1 3 }  { real_i_25_ce1 mem_ce 1 1 }  { real_i_25_d1 mem_din 1 32 }  { real_i_25_q1 mem_dout 0 32 }  { real_i_25_we1 mem_we 1 1 } } }
	real_i_26 { ap_memory {  { real_i_26_address0 mem_address 1 3 }  { real_i_26_ce0 mem_ce 1 1 }  { real_i_26_d0 mem_din 1 32 }  { real_i_26_q0 mem_dout 0 32 }  { real_i_26_we0 mem_we 1 1 }  { real_i_26_address1 mem_address 1 3 }  { real_i_26_ce1 mem_ce 1 1 }  { real_i_26_d1 mem_din 1 32 }  { real_i_26_q1 mem_dout 0 32 }  { real_i_26_we1 mem_we 1 1 } } }
	real_i_27 { ap_memory {  { real_i_27_address0 mem_address 1 3 }  { real_i_27_ce0 mem_ce 1 1 }  { real_i_27_d0 mem_din 1 32 }  { real_i_27_q0 mem_dout 0 32 }  { real_i_27_we0 mem_we 1 1 }  { real_i_27_address1 mem_address 1 3 }  { real_i_27_ce1 mem_ce 1 1 }  { real_i_27_d1 mem_din 1 32 }  { real_i_27_q1 mem_dout 0 32 }  { real_i_27_we1 mem_we 1 1 } } }
	real_i_28 { ap_memory {  { real_i_28_address0 mem_address 1 3 }  { real_i_28_ce0 mem_ce 1 1 }  { real_i_28_d0 mem_din 1 32 }  { real_i_28_q0 mem_dout 0 32 }  { real_i_28_we0 mem_we 1 1 }  { real_i_28_address1 mem_address 1 3 }  { real_i_28_ce1 mem_ce 1 1 }  { real_i_28_d1 mem_din 1 32 }  { real_i_28_q1 mem_dout 0 32 }  { real_i_28_we1 mem_we 1 1 } } }
	real_i_29 { ap_memory {  { real_i_29_address0 mem_address 1 3 }  { real_i_29_ce0 mem_ce 1 1 }  { real_i_29_d0 mem_din 1 32 }  { real_i_29_q0 mem_dout 0 32 }  { real_i_29_we0 mem_we 1 1 }  { real_i_29_address1 mem_address 1 3 }  { real_i_29_ce1 mem_ce 1 1 }  { real_i_29_d1 mem_din 1 32 }  { real_i_29_q1 mem_dout 0 32 }  { real_i_29_we1 mem_we 1 1 } } }
	real_i_30 { ap_memory {  { real_i_30_address0 mem_address 1 3 }  { real_i_30_ce0 mem_ce 1 1 }  { real_i_30_d0 mem_din 1 32 }  { real_i_30_q0 mem_dout 0 32 }  { real_i_30_we0 mem_we 1 1 }  { real_i_30_address1 mem_address 1 3 }  { real_i_30_ce1 mem_ce 1 1 }  { real_i_30_d1 mem_din 1 32 }  { real_i_30_q1 mem_dout 0 32 }  { real_i_30_we1 mem_we 1 1 } } }
	real_i_31 { ap_memory {  { real_i_31_address0 mem_address 1 3 }  { real_i_31_ce0 mem_ce 1 1 }  { real_i_31_d0 mem_din 1 32 }  { real_i_31_q0 mem_dout 0 32 }  { real_i_31_we0 mem_we 1 1 }  { real_i_31_address1 mem_address 1 3 }  { real_i_31_ce1 mem_ce 1 1 }  { real_i_31_d1 mem_din 1 32 }  { real_i_31_q1 mem_dout 0 32 }  { real_i_31_we1 mem_we 1 1 } } }
	imag_i_0 { ap_memory {  { imag_i_0_address0 mem_address 1 3 }  { imag_i_0_ce0 mem_ce 1 1 }  { imag_i_0_d0 mem_din 1 32 }  { imag_i_0_q0 mem_dout 0 32 }  { imag_i_0_we0 mem_we 1 1 }  { imag_i_0_address1 mem_address 1 3 }  { imag_i_0_ce1 mem_ce 1 1 }  { imag_i_0_d1 mem_din 1 32 }  { imag_i_0_q1 mem_dout 0 32 }  { imag_i_0_we1 mem_we 1 1 } } }
	imag_i_1 { ap_memory {  { imag_i_1_address0 mem_address 1 3 }  { imag_i_1_ce0 mem_ce 1 1 }  { imag_i_1_d0 mem_din 1 32 }  { imag_i_1_q0 mem_dout 0 32 }  { imag_i_1_we0 mem_we 1 1 }  { imag_i_1_address1 mem_address 1 3 }  { imag_i_1_ce1 mem_ce 1 1 }  { imag_i_1_d1 mem_din 1 32 }  { imag_i_1_q1 mem_dout 0 32 }  { imag_i_1_we1 mem_we 1 1 } } }
	imag_i_2 { ap_memory {  { imag_i_2_address0 mem_address 1 3 }  { imag_i_2_ce0 mem_ce 1 1 }  { imag_i_2_d0 mem_din 1 32 }  { imag_i_2_q0 mem_dout 0 32 }  { imag_i_2_we0 mem_we 1 1 }  { imag_i_2_address1 mem_address 1 3 }  { imag_i_2_ce1 mem_ce 1 1 }  { imag_i_2_d1 mem_din 1 32 }  { imag_i_2_q1 mem_dout 0 32 }  { imag_i_2_we1 mem_we 1 1 } } }
	imag_i_3 { ap_memory {  { imag_i_3_address0 mem_address 1 3 }  { imag_i_3_ce0 mem_ce 1 1 }  { imag_i_3_d0 mem_din 1 32 }  { imag_i_3_q0 mem_dout 0 32 }  { imag_i_3_we0 mem_we 1 1 }  { imag_i_3_address1 mem_address 1 3 }  { imag_i_3_ce1 mem_ce 1 1 }  { imag_i_3_d1 mem_din 1 32 }  { imag_i_3_q1 mem_dout 0 32 }  { imag_i_3_we1 mem_we 1 1 } } }
	imag_i_4 { ap_memory {  { imag_i_4_address0 mem_address 1 3 }  { imag_i_4_ce0 mem_ce 1 1 }  { imag_i_4_d0 mem_din 1 32 }  { imag_i_4_q0 mem_dout 0 32 }  { imag_i_4_we0 mem_we 1 1 }  { imag_i_4_address1 mem_address 1 3 }  { imag_i_4_ce1 mem_ce 1 1 }  { imag_i_4_d1 mem_din 1 32 }  { imag_i_4_q1 mem_dout 0 32 }  { imag_i_4_we1 mem_we 1 1 } } }
	imag_i_5 { ap_memory {  { imag_i_5_address0 mem_address 1 3 }  { imag_i_5_ce0 mem_ce 1 1 }  { imag_i_5_d0 mem_din 1 32 }  { imag_i_5_q0 mem_dout 0 32 }  { imag_i_5_we0 mem_we 1 1 }  { imag_i_5_address1 mem_address 1 3 }  { imag_i_5_ce1 mem_ce 1 1 }  { imag_i_5_d1 mem_din 1 32 }  { imag_i_5_q1 mem_dout 0 32 }  { imag_i_5_we1 mem_we 1 1 } } }
	imag_i_6 { ap_memory {  { imag_i_6_address0 mem_address 1 3 }  { imag_i_6_ce0 mem_ce 1 1 }  { imag_i_6_d0 mem_din 1 32 }  { imag_i_6_q0 mem_dout 0 32 }  { imag_i_6_we0 mem_we 1 1 }  { imag_i_6_address1 mem_address 1 3 }  { imag_i_6_ce1 mem_ce 1 1 }  { imag_i_6_d1 mem_din 1 32 }  { imag_i_6_q1 mem_dout 0 32 }  { imag_i_6_we1 mem_we 1 1 } } }
	imag_i_7 { ap_memory {  { imag_i_7_address0 mem_address 1 3 }  { imag_i_7_ce0 mem_ce 1 1 }  { imag_i_7_d0 mem_din 1 32 }  { imag_i_7_q0 mem_dout 0 32 }  { imag_i_7_we0 mem_we 1 1 }  { imag_i_7_address1 mem_address 1 3 }  { imag_i_7_ce1 mem_ce 1 1 }  { imag_i_7_d1 mem_din 1 32 }  { imag_i_7_q1 mem_dout 0 32 }  { imag_i_7_we1 mem_we 1 1 } } }
	imag_i_8 { ap_memory {  { imag_i_8_address0 mem_address 1 3 }  { imag_i_8_ce0 mem_ce 1 1 }  { imag_i_8_d0 mem_din 1 32 }  { imag_i_8_q0 mem_dout 0 32 }  { imag_i_8_we0 mem_we 1 1 }  { imag_i_8_address1 mem_address 1 3 }  { imag_i_8_ce1 mem_ce 1 1 }  { imag_i_8_d1 mem_din 1 32 }  { imag_i_8_q1 mem_dout 0 32 }  { imag_i_8_we1 mem_we 1 1 } } }
	imag_i_9 { ap_memory {  { imag_i_9_address0 mem_address 1 3 }  { imag_i_9_ce0 mem_ce 1 1 }  { imag_i_9_d0 mem_din 1 32 }  { imag_i_9_q0 mem_dout 0 32 }  { imag_i_9_we0 mem_we 1 1 }  { imag_i_9_address1 mem_address 1 3 }  { imag_i_9_ce1 mem_ce 1 1 }  { imag_i_9_d1 mem_din 1 32 }  { imag_i_9_q1 mem_dout 0 32 }  { imag_i_9_we1 mem_we 1 1 } } }
	imag_i_10 { ap_memory {  { imag_i_10_address0 mem_address 1 3 }  { imag_i_10_ce0 mem_ce 1 1 }  { imag_i_10_d0 mem_din 1 32 }  { imag_i_10_q0 mem_dout 0 32 }  { imag_i_10_we0 mem_we 1 1 }  { imag_i_10_address1 mem_address 1 3 }  { imag_i_10_ce1 mem_ce 1 1 }  { imag_i_10_d1 mem_din 1 32 }  { imag_i_10_q1 mem_dout 0 32 }  { imag_i_10_we1 mem_we 1 1 } } }
	imag_i_11 { ap_memory {  { imag_i_11_address0 mem_address 1 3 }  { imag_i_11_ce0 mem_ce 1 1 }  { imag_i_11_d0 mem_din 1 32 }  { imag_i_11_q0 mem_dout 0 32 }  { imag_i_11_we0 mem_we 1 1 }  { imag_i_11_address1 mem_address 1 3 }  { imag_i_11_ce1 mem_ce 1 1 }  { imag_i_11_d1 mem_din 1 32 }  { imag_i_11_q1 mem_dout 0 32 }  { imag_i_11_we1 mem_we 1 1 } } }
	imag_i_12 { ap_memory {  { imag_i_12_address0 mem_address 1 3 }  { imag_i_12_ce0 mem_ce 1 1 }  { imag_i_12_d0 mem_din 1 32 }  { imag_i_12_q0 mem_dout 0 32 }  { imag_i_12_we0 mem_we 1 1 }  { imag_i_12_address1 mem_address 1 3 }  { imag_i_12_ce1 mem_ce 1 1 }  { imag_i_12_d1 mem_din 1 32 }  { imag_i_12_q1 mem_dout 0 32 }  { imag_i_12_we1 mem_we 1 1 } } }
	imag_i_13 { ap_memory {  { imag_i_13_address0 mem_address 1 3 }  { imag_i_13_ce0 mem_ce 1 1 }  { imag_i_13_d0 mem_din 1 32 }  { imag_i_13_q0 mem_dout 0 32 }  { imag_i_13_we0 mem_we 1 1 }  { imag_i_13_address1 mem_address 1 3 }  { imag_i_13_ce1 mem_ce 1 1 }  { imag_i_13_d1 mem_din 1 32 }  { imag_i_13_q1 mem_dout 0 32 }  { imag_i_13_we1 mem_we 1 1 } } }
	imag_i_14 { ap_memory {  { imag_i_14_address0 mem_address 1 3 }  { imag_i_14_ce0 mem_ce 1 1 }  { imag_i_14_d0 mem_din 1 32 }  { imag_i_14_q0 mem_dout 0 32 }  { imag_i_14_we0 mem_we 1 1 }  { imag_i_14_address1 mem_address 1 3 }  { imag_i_14_ce1 mem_ce 1 1 }  { imag_i_14_d1 mem_din 1 32 }  { imag_i_14_q1 mem_dout 0 32 }  { imag_i_14_we1 mem_we 1 1 } } }
	imag_i_15 { ap_memory {  { imag_i_15_address0 mem_address 1 3 }  { imag_i_15_ce0 mem_ce 1 1 }  { imag_i_15_d0 mem_din 1 32 }  { imag_i_15_q0 mem_dout 0 32 }  { imag_i_15_we0 mem_we 1 1 }  { imag_i_15_address1 mem_address 1 3 }  { imag_i_15_ce1 mem_ce 1 1 }  { imag_i_15_d1 mem_din 1 32 }  { imag_i_15_q1 mem_dout 0 32 }  { imag_i_15_we1 mem_we 1 1 } } }
	imag_i_16 { ap_memory {  { imag_i_16_address0 mem_address 1 3 }  { imag_i_16_ce0 mem_ce 1 1 }  { imag_i_16_d0 mem_din 1 32 }  { imag_i_16_q0 mem_dout 0 32 }  { imag_i_16_we0 mem_we 1 1 }  { imag_i_16_address1 mem_address 1 3 }  { imag_i_16_ce1 mem_ce 1 1 }  { imag_i_16_d1 mem_din 1 32 }  { imag_i_16_q1 mem_dout 0 32 }  { imag_i_16_we1 mem_we 1 1 } } }
	imag_i_17 { ap_memory {  { imag_i_17_address0 mem_address 1 3 }  { imag_i_17_ce0 mem_ce 1 1 }  { imag_i_17_d0 mem_din 1 32 }  { imag_i_17_q0 mem_dout 0 32 }  { imag_i_17_we0 mem_we 1 1 }  { imag_i_17_address1 mem_address 1 3 }  { imag_i_17_ce1 mem_ce 1 1 }  { imag_i_17_d1 mem_din 1 32 }  { imag_i_17_q1 mem_dout 0 32 }  { imag_i_17_we1 mem_we 1 1 } } }
	imag_i_18 { ap_memory {  { imag_i_18_address0 mem_address 1 3 }  { imag_i_18_ce0 mem_ce 1 1 }  { imag_i_18_d0 mem_din 1 32 }  { imag_i_18_q0 mem_dout 0 32 }  { imag_i_18_we0 mem_we 1 1 }  { imag_i_18_address1 mem_address 1 3 }  { imag_i_18_ce1 mem_ce 1 1 }  { imag_i_18_d1 mem_din 1 32 }  { imag_i_18_q1 mem_dout 0 32 }  { imag_i_18_we1 mem_we 1 1 } } }
	imag_i_19 { ap_memory {  { imag_i_19_address0 mem_address 1 3 }  { imag_i_19_ce0 mem_ce 1 1 }  { imag_i_19_d0 mem_din 1 32 }  { imag_i_19_q0 mem_dout 0 32 }  { imag_i_19_we0 mem_we 1 1 }  { imag_i_19_address1 mem_address 1 3 }  { imag_i_19_ce1 mem_ce 1 1 }  { imag_i_19_d1 mem_din 1 32 }  { imag_i_19_q1 mem_dout 0 32 }  { imag_i_19_we1 mem_we 1 1 } } }
	imag_i_20 { ap_memory {  { imag_i_20_address0 mem_address 1 3 }  { imag_i_20_ce0 mem_ce 1 1 }  { imag_i_20_d0 mem_din 1 32 }  { imag_i_20_q0 mem_dout 0 32 }  { imag_i_20_we0 mem_we 1 1 }  { imag_i_20_address1 mem_address 1 3 }  { imag_i_20_ce1 mem_ce 1 1 }  { imag_i_20_d1 mem_din 1 32 }  { imag_i_20_q1 mem_dout 0 32 }  { imag_i_20_we1 mem_we 1 1 } } }
	imag_i_21 { ap_memory {  { imag_i_21_address0 mem_address 1 3 }  { imag_i_21_ce0 mem_ce 1 1 }  { imag_i_21_d0 mem_din 1 32 }  { imag_i_21_q0 mem_dout 0 32 }  { imag_i_21_we0 mem_we 1 1 }  { imag_i_21_address1 mem_address 1 3 }  { imag_i_21_ce1 mem_ce 1 1 }  { imag_i_21_d1 mem_din 1 32 }  { imag_i_21_q1 mem_dout 0 32 }  { imag_i_21_we1 mem_we 1 1 } } }
	imag_i_22 { ap_memory {  { imag_i_22_address0 mem_address 1 3 }  { imag_i_22_ce0 mem_ce 1 1 }  { imag_i_22_d0 mem_din 1 32 }  { imag_i_22_q0 mem_dout 0 32 }  { imag_i_22_we0 mem_we 1 1 }  { imag_i_22_address1 mem_address 1 3 }  { imag_i_22_ce1 mem_ce 1 1 }  { imag_i_22_d1 mem_din 1 32 }  { imag_i_22_q1 mem_dout 0 32 }  { imag_i_22_we1 mem_we 1 1 } } }
	imag_i_23 { ap_memory {  { imag_i_23_address0 mem_address 1 3 }  { imag_i_23_ce0 mem_ce 1 1 }  { imag_i_23_d0 mem_din 1 32 }  { imag_i_23_q0 mem_dout 0 32 }  { imag_i_23_we0 mem_we 1 1 }  { imag_i_23_address1 mem_address 1 3 }  { imag_i_23_ce1 mem_ce 1 1 }  { imag_i_23_d1 mem_din 1 32 }  { imag_i_23_q1 mem_dout 0 32 }  { imag_i_23_we1 mem_we 1 1 } } }
	imag_i_24 { ap_memory {  { imag_i_24_address0 mem_address 1 3 }  { imag_i_24_ce0 mem_ce 1 1 }  { imag_i_24_d0 mem_din 1 32 }  { imag_i_24_q0 mem_dout 0 32 }  { imag_i_24_we0 mem_we 1 1 }  { imag_i_24_address1 mem_address 1 3 }  { imag_i_24_ce1 mem_ce 1 1 }  { imag_i_24_d1 mem_din 1 32 }  { imag_i_24_q1 mem_dout 0 32 }  { imag_i_24_we1 mem_we 1 1 } } }
	imag_i_25 { ap_memory {  { imag_i_25_address0 mem_address 1 3 }  { imag_i_25_ce0 mem_ce 1 1 }  { imag_i_25_d0 mem_din 1 32 }  { imag_i_25_q0 mem_dout 0 32 }  { imag_i_25_we0 mem_we 1 1 }  { imag_i_25_address1 mem_address 1 3 }  { imag_i_25_ce1 mem_ce 1 1 }  { imag_i_25_d1 mem_din 1 32 }  { imag_i_25_q1 mem_dout 0 32 }  { imag_i_25_we1 mem_we 1 1 } } }
	imag_i_26 { ap_memory {  { imag_i_26_address0 mem_address 1 3 }  { imag_i_26_ce0 mem_ce 1 1 }  { imag_i_26_d0 mem_din 1 32 }  { imag_i_26_q0 mem_dout 0 32 }  { imag_i_26_we0 mem_we 1 1 }  { imag_i_26_address1 mem_address 1 3 }  { imag_i_26_ce1 mem_ce 1 1 }  { imag_i_26_d1 mem_din 1 32 }  { imag_i_26_q1 mem_dout 0 32 }  { imag_i_26_we1 mem_we 1 1 } } }
	imag_i_27 { ap_memory {  { imag_i_27_address0 mem_address 1 3 }  { imag_i_27_ce0 mem_ce 1 1 }  { imag_i_27_d0 mem_din 1 32 }  { imag_i_27_q0 mem_dout 0 32 }  { imag_i_27_we0 mem_we 1 1 }  { imag_i_27_address1 mem_address 1 3 }  { imag_i_27_ce1 mem_ce 1 1 }  { imag_i_27_d1 mem_din 1 32 }  { imag_i_27_q1 mem_dout 0 32 }  { imag_i_27_we1 mem_we 1 1 } } }
	imag_i_28 { ap_memory {  { imag_i_28_address0 mem_address 1 3 }  { imag_i_28_ce0 mem_ce 1 1 }  { imag_i_28_d0 mem_din 1 32 }  { imag_i_28_q0 mem_dout 0 32 }  { imag_i_28_we0 mem_we 1 1 }  { imag_i_28_address1 mem_address 1 3 }  { imag_i_28_ce1 mem_ce 1 1 }  { imag_i_28_d1 mem_din 1 32 }  { imag_i_28_q1 mem_dout 0 32 }  { imag_i_28_we1 mem_we 1 1 } } }
	imag_i_29 { ap_memory {  { imag_i_29_address0 mem_address 1 3 }  { imag_i_29_ce0 mem_ce 1 1 }  { imag_i_29_d0 mem_din 1 32 }  { imag_i_29_q0 mem_dout 0 32 }  { imag_i_29_we0 mem_we 1 1 }  { imag_i_29_address1 mem_address 1 3 }  { imag_i_29_ce1 mem_ce 1 1 }  { imag_i_29_d1 mem_din 1 32 }  { imag_i_29_q1 mem_dout 0 32 }  { imag_i_29_we1 mem_we 1 1 } } }
	imag_i_30 { ap_memory {  { imag_i_30_address0 mem_address 1 3 }  { imag_i_30_ce0 mem_ce 1 1 }  { imag_i_30_d0 mem_din 1 32 }  { imag_i_30_q0 mem_dout 0 32 }  { imag_i_30_we0 mem_we 1 1 }  { imag_i_30_address1 mem_address 1 3 }  { imag_i_30_ce1 mem_ce 1 1 }  { imag_i_30_d1 mem_din 1 32 }  { imag_i_30_q1 mem_dout 0 32 }  { imag_i_30_we1 mem_we 1 1 } } }
	imag_i_31 { ap_memory {  { imag_i_31_address0 mem_address 1 3 }  { imag_i_31_ce0 mem_ce 1 1 }  { imag_i_31_d0 mem_din 1 32 }  { imag_i_31_q0 mem_dout 0 32 }  { imag_i_31_we0 mem_we 1 1 }  { imag_i_31_address1 mem_address 1 3 }  { imag_i_31_ce1 mem_ce 1 1 }  { imag_i_31_d1 mem_din 1 32 }  { imag_i_31_q1 mem_dout 0 32 }  { imag_i_31_we1 mem_we 1 1 } } }
	real_o { ap_memory {  { real_o_address0 mem_address 1 8 }  { real_o_ce0 mem_ce 1 1 }  { real_o_d0 mem_din 1 32 }  { real_o_q0 mem_dout 0 32 }  { real_o_we0 mem_we 1 1 }  { real_o_address1 mem_address 1 8 }  { real_o_ce1 mem_ce 1 1 }  { real_o_d1 mem_din 1 32 }  { real_o_q1 mem_dout 0 32 }  { real_o_we1 mem_we 1 1 } } }
	imag_o { ap_memory {  { imag_o_address0 mem_address 1 8 }  { imag_o_ce0 mem_ce 1 1 }  { imag_o_d0 mem_din 1 32 }  { imag_o_q0 mem_dout 0 32 }  { imag_o_we0 mem_we 1 1 }  { imag_o_address1 mem_address 1 8 }  { imag_o_ce1 mem_ce 1 1 }  { imag_o_d1 mem_din 1 32 }  { imag_o_q1 mem_dout 0 32 }  { imag_o_we1 mem_we 1 1 } } }
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
