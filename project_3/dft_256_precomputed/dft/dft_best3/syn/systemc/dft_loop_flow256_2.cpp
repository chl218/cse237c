#include "dft_loop_flow256.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void dft_loop_flow256::thread_ap_clk_no_reset_() {
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_CS_fsm = ap_ST_st1_fsm_0;
    } else {
        ap_CS_fsm = ap_NS_fsm.read();
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ppiten_pp0_it0 = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
             !esl_seteq<1,1,1>(exitcond_fu_7237_p2.read(), ap_const_lv1_0))) {
            ap_reg_ppiten_pp0_it0 = ap_const_logic_0;
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st1_fsm_0.read()) && 
                    !esl_seteq<1,1,1>(ap_start.read(), ap_const_logic_0))) {
            ap_reg_ppiten_pp0_it0 = ap_const_logic_1;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ppiten_pp0_it1 = ap_const_logic_0;
    } else {
        if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
             esl_seteq<1,1,1>(exitcond_fu_7237_p2.read(), ap_const_lv1_0))) {
            ap_reg_ppiten_pp0_it1 = ap_const_logic_1;
        } else if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st1_fsm_0.read()) && 
                     !esl_seteq<1,1,1>(ap_start.read(), ap_const_logic_0)) || 
                    (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
                     !esl_seteq<1,1,1>(exitcond_fu_7237_p2.read(), ap_const_lv1_0)))) {
            ap_reg_ppiten_pp0_it1 = ap_const_logic_0;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ppiten_pp0_it10 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_true, ap_true)) {
            ap_reg_ppiten_pp0_it10 = ap_reg_ppiten_pp0_it9.read();
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ppiten_pp0_it11 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_true, ap_true)) {
            ap_reg_ppiten_pp0_it11 = ap_reg_ppiten_pp0_it10.read();
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ppiten_pp0_it12 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_true, ap_true)) {
            ap_reg_ppiten_pp0_it12 = ap_reg_ppiten_pp0_it11.read();
        } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st1_fsm_0.read()) && 
                    !esl_seteq<1,1,1>(ap_start.read(), ap_const_logic_0))) {
            ap_reg_ppiten_pp0_it12 = ap_const_logic_0;
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ppiten_pp0_it2 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_true, ap_true)) {
            ap_reg_ppiten_pp0_it2 = ap_reg_ppiten_pp0_it1.read();
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ppiten_pp0_it3 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_true, ap_true)) {
            ap_reg_ppiten_pp0_it3 = ap_reg_ppiten_pp0_it2.read();
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ppiten_pp0_it4 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_true, ap_true)) {
            ap_reg_ppiten_pp0_it4 = ap_reg_ppiten_pp0_it3.read();
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ppiten_pp0_it5 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_true, ap_true)) {
            ap_reg_ppiten_pp0_it5 = ap_reg_ppiten_pp0_it4.read();
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ppiten_pp0_it6 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_true, ap_true)) {
            ap_reg_ppiten_pp0_it6 = ap_reg_ppiten_pp0_it5.read();
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ppiten_pp0_it7 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_true, ap_true)) {
            ap_reg_ppiten_pp0_it7 = ap_reg_ppiten_pp0_it6.read();
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ppiten_pp0_it8 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_true, ap_true)) {
            ap_reg_ppiten_pp0_it8 = ap_reg_ppiten_pp0_it7.read();
        }
    }
    if ( ap_rst.read() == ap_const_logic_1) {
        ap_reg_ppiten_pp0_it9 = ap_const_logic_0;
    } else {
        if (esl_seteq<1,1,1>(ap_true, ap_true)) {
            ap_reg_ppiten_pp0_it9 = ap_reg_ppiten_pp0_it8.read();
        }
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it0.read()) && 
         esl_seteq<1,1,1>(exitcond_fu_7237_p2.read(), ap_const_lv1_0))) {
        p_s_reg_7034 = j_V_7_fu_7673_p2.read();
    } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st1_fsm_0.read()) && 
                !esl_seteq<1,1,1>(ap_start.read(), ap_const_logic_0))) {
        p_s_reg_7034 = ap_const_lv9_0;
    }
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read())) {
        ap_reg_ppstg_exitcond_reg_9765_pp0_it1 = exitcond_reg_9765.read();
        ap_reg_ppstg_newIndex12_reg_9958_pp0_it1 = newIndex12_reg_9958.read();
        ap_reg_ppstg_newIndex15_reg_9992_pp0_it1 = newIndex15_reg_9992.read();
        ap_reg_ppstg_newIndex18_reg_10026_pp0_it1 = newIndex18_reg_10026.read();
        ap_reg_ppstg_newIndex2_reg_9803_pp0_it1 = newIndex2_reg_9803.read();
        ap_reg_ppstg_newIndex8_reg_9924_pp0_it1 = newIndex8_reg_9924.read();
        ap_reg_ppstg_sel_tmp12_reg_9808_pp0_it1 = sel_tmp12_reg_9808.read();
        ap_reg_ppstg_sel_tmp14_reg_9814_pp0_it1 = sel_tmp14_reg_9814.read();
        ap_reg_ppstg_sel_tmp16_reg_9820_pp0_it1 = sel_tmp16_reg_9820.read();
        ap_reg_ppstg_sel_tmp19_reg_9837_pp0_it1 = sel_tmp19_reg_9837.read();
        ap_reg_ppstg_sel_tmp21_reg_9843_pp0_it1 = sel_tmp21_reg_9843.read();
        ap_reg_ppstg_sel_tmp23_reg_9849_pp0_it1 = sel_tmp23_reg_9849.read();
        ap_reg_ppstg_sel_tmp26_reg_9866_pp0_it1 = sel_tmp26_reg_9866.read();
        ap_reg_ppstg_sel_tmp28_reg_9872_pp0_it1 = sel_tmp28_reg_9872.read();
        ap_reg_ppstg_sel_tmp2_reg_9780_pp0_it1 = sel_tmp2_reg_9780.read();
        ap_reg_ppstg_sel_tmp30_reg_9878_pp0_it1 = sel_tmp30_reg_9878.read();
        ap_reg_ppstg_sel_tmp33_reg_9895_pp0_it1 = sel_tmp33_reg_9895.read();
        ap_reg_ppstg_sel_tmp35_reg_9901_pp0_it1 = sel_tmp35_reg_9901.read();
        ap_reg_ppstg_sel_tmp37_reg_9907_pp0_it1 = sel_tmp37_reg_9907.read();
        ap_reg_ppstg_sel_tmp40_reg_9929_pp0_it1 = sel_tmp40_reg_9929.read();
        ap_reg_ppstg_sel_tmp42_reg_9935_pp0_it1 = sel_tmp42_reg_9935.read();
        ap_reg_ppstg_sel_tmp44_reg_9941_pp0_it1 = sel_tmp44_reg_9941.read();
        ap_reg_ppstg_sel_tmp47_reg_9963_pp0_it1 = sel_tmp47_reg_9963.read();
        ap_reg_ppstg_sel_tmp49_reg_9969_pp0_it1 = sel_tmp49_reg_9969.read();
        ap_reg_ppstg_sel_tmp4_reg_9786_pp0_it1 = sel_tmp4_reg_9786.read();
        ap_reg_ppstg_sel_tmp51_reg_9975_pp0_it1 = sel_tmp51_reg_9975.read();
        ap_reg_ppstg_sel_tmp54_reg_9997_pp0_it1 = sel_tmp54_reg_9997.read();
        ap_reg_ppstg_sel_tmp56_reg_10003_pp0_it1 = sel_tmp56_reg_10003.read();
        ap_reg_ppstg_sel_tmp58_reg_10009_pp0_it1 = sel_tmp58_reg_10009.read();
        ap_reg_ppstg_sel_tmp_reg_9774_pp0_it1 = sel_tmp_reg_9774.read();
        ap_reg_ppstg_tmp_30_reg_9792_pp0_it1 = tmp_30_reg_9792.read();
        ap_reg_ppstg_tmp_32_reg_9826_pp0_it1 = tmp_32_reg_9826.read();
        ap_reg_ppstg_tmp_34_reg_9855_pp0_it1 = tmp_34_reg_9855.read();
        ap_reg_ppstg_tmp_36_reg_9884_pp0_it1 = tmp_36_reg_9884.read();
        ap_reg_ppstg_tmp_38_reg_9913_pp0_it1 = tmp_38_reg_9913.read();
        ap_reg_ppstg_tmp_40_reg_9947_pp0_it1 = tmp_40_reg_9947.read();
        ap_reg_ppstg_tmp_42_reg_9981_pp0_it1 = tmp_42_reg_9981.read();
        ap_reg_ppstg_tmp_44_reg_10015_pp0_it1 = tmp_44_reg_10015.read();
        exitcond_reg_9765 = exitcond_fu_7237_p2.read();
    }
    if (esl_seteq<1,1,1>(ap_true, ap_true)) {
        ap_reg_ppstg_exitcond_reg_9765_pp0_it10 = ap_reg_ppstg_exitcond_reg_9765_pp0_it9.read();
        ap_reg_ppstg_exitcond_reg_9765_pp0_it11 = ap_reg_ppstg_exitcond_reg_9765_pp0_it10.read();
        ap_reg_ppstg_exitcond_reg_9765_pp0_it2 = ap_reg_ppstg_exitcond_reg_9765_pp0_it1.read();
        ap_reg_ppstg_exitcond_reg_9765_pp0_it3 = ap_reg_ppstg_exitcond_reg_9765_pp0_it2.read();
        ap_reg_ppstg_exitcond_reg_9765_pp0_it4 = ap_reg_ppstg_exitcond_reg_9765_pp0_it3.read();
        ap_reg_ppstg_exitcond_reg_9765_pp0_it5 = ap_reg_ppstg_exitcond_reg_9765_pp0_it4.read();
        ap_reg_ppstg_exitcond_reg_9765_pp0_it6 = ap_reg_ppstg_exitcond_reg_9765_pp0_it5.read();
        ap_reg_ppstg_exitcond_reg_9765_pp0_it7 = ap_reg_ppstg_exitcond_reg_9765_pp0_it6.read();
        ap_reg_ppstg_exitcond_reg_9765_pp0_it8 = ap_reg_ppstg_exitcond_reg_9765_pp0_it7.read();
        ap_reg_ppstg_exitcond_reg_9765_pp0_it9 = ap_reg_ppstg_exitcond_reg_9765_pp0_it8.read();
        ap_reg_ppstg_newIndex12_reg_9958_pp0_it10 = ap_reg_ppstg_newIndex12_reg_9958_pp0_it9.read();
        ap_reg_ppstg_newIndex12_reg_9958_pp0_it11 = ap_reg_ppstg_newIndex12_reg_9958_pp0_it10.read();
        ap_reg_ppstg_newIndex12_reg_9958_pp0_it2 = ap_reg_ppstg_newIndex12_reg_9958_pp0_it1.read();
        ap_reg_ppstg_newIndex12_reg_9958_pp0_it3 = ap_reg_ppstg_newIndex12_reg_9958_pp0_it2.read();
        ap_reg_ppstg_newIndex12_reg_9958_pp0_it4 = ap_reg_ppstg_newIndex12_reg_9958_pp0_it3.read();
        ap_reg_ppstg_newIndex12_reg_9958_pp0_it5 = ap_reg_ppstg_newIndex12_reg_9958_pp0_it4.read();
        ap_reg_ppstg_newIndex12_reg_9958_pp0_it6 = ap_reg_ppstg_newIndex12_reg_9958_pp0_it5.read();
        ap_reg_ppstg_newIndex12_reg_9958_pp0_it7 = ap_reg_ppstg_newIndex12_reg_9958_pp0_it6.read();
        ap_reg_ppstg_newIndex12_reg_9958_pp0_it8 = ap_reg_ppstg_newIndex12_reg_9958_pp0_it7.read();
        ap_reg_ppstg_newIndex12_reg_9958_pp0_it9 = ap_reg_ppstg_newIndex12_reg_9958_pp0_it8.read();
        ap_reg_ppstg_newIndex15_reg_9992_pp0_it10 = ap_reg_ppstg_newIndex15_reg_9992_pp0_it9.read();
        ap_reg_ppstg_newIndex15_reg_9992_pp0_it11 = ap_reg_ppstg_newIndex15_reg_9992_pp0_it10.read();
        ap_reg_ppstg_newIndex15_reg_9992_pp0_it2 = ap_reg_ppstg_newIndex15_reg_9992_pp0_it1.read();
        ap_reg_ppstg_newIndex15_reg_9992_pp0_it3 = ap_reg_ppstg_newIndex15_reg_9992_pp0_it2.read();
        ap_reg_ppstg_newIndex15_reg_9992_pp0_it4 = ap_reg_ppstg_newIndex15_reg_9992_pp0_it3.read();
        ap_reg_ppstg_newIndex15_reg_9992_pp0_it5 = ap_reg_ppstg_newIndex15_reg_9992_pp0_it4.read();
        ap_reg_ppstg_newIndex15_reg_9992_pp0_it6 = ap_reg_ppstg_newIndex15_reg_9992_pp0_it5.read();
        ap_reg_ppstg_newIndex15_reg_9992_pp0_it7 = ap_reg_ppstg_newIndex15_reg_9992_pp0_it6.read();
        ap_reg_ppstg_newIndex15_reg_9992_pp0_it8 = ap_reg_ppstg_newIndex15_reg_9992_pp0_it7.read();
        ap_reg_ppstg_newIndex15_reg_9992_pp0_it9 = ap_reg_ppstg_newIndex15_reg_9992_pp0_it8.read();
        ap_reg_ppstg_newIndex18_reg_10026_pp0_it10 = ap_reg_ppstg_newIndex18_reg_10026_pp0_it9.read();
        ap_reg_ppstg_newIndex18_reg_10026_pp0_it11 = ap_reg_ppstg_newIndex18_reg_10026_pp0_it10.read();
        ap_reg_ppstg_newIndex18_reg_10026_pp0_it2 = ap_reg_ppstg_newIndex18_reg_10026_pp0_it1.read();
        ap_reg_ppstg_newIndex18_reg_10026_pp0_it3 = ap_reg_ppstg_newIndex18_reg_10026_pp0_it2.read();
        ap_reg_ppstg_newIndex18_reg_10026_pp0_it4 = ap_reg_ppstg_newIndex18_reg_10026_pp0_it3.read();
        ap_reg_ppstg_newIndex18_reg_10026_pp0_it5 = ap_reg_ppstg_newIndex18_reg_10026_pp0_it4.read();
        ap_reg_ppstg_newIndex18_reg_10026_pp0_it6 = ap_reg_ppstg_newIndex18_reg_10026_pp0_it5.read();
        ap_reg_ppstg_newIndex18_reg_10026_pp0_it7 = ap_reg_ppstg_newIndex18_reg_10026_pp0_it6.read();
        ap_reg_ppstg_newIndex18_reg_10026_pp0_it8 = ap_reg_ppstg_newIndex18_reg_10026_pp0_it7.read();
        ap_reg_ppstg_newIndex18_reg_10026_pp0_it9 = ap_reg_ppstg_newIndex18_reg_10026_pp0_it8.read();
        ap_reg_ppstg_newIndex2_reg_9803_pp0_it10 = ap_reg_ppstg_newIndex2_reg_9803_pp0_it9.read();
        ap_reg_ppstg_newIndex2_reg_9803_pp0_it11 = ap_reg_ppstg_newIndex2_reg_9803_pp0_it10.read();
        ap_reg_ppstg_newIndex2_reg_9803_pp0_it2 = ap_reg_ppstg_newIndex2_reg_9803_pp0_it1.read();
        ap_reg_ppstg_newIndex2_reg_9803_pp0_it3 = ap_reg_ppstg_newIndex2_reg_9803_pp0_it2.read();
        ap_reg_ppstg_newIndex2_reg_9803_pp0_it4 = ap_reg_ppstg_newIndex2_reg_9803_pp0_it3.read();
        ap_reg_ppstg_newIndex2_reg_9803_pp0_it5 = ap_reg_ppstg_newIndex2_reg_9803_pp0_it4.read();
        ap_reg_ppstg_newIndex2_reg_9803_pp0_it6 = ap_reg_ppstg_newIndex2_reg_9803_pp0_it5.read();
        ap_reg_ppstg_newIndex2_reg_9803_pp0_it7 = ap_reg_ppstg_newIndex2_reg_9803_pp0_it6.read();
        ap_reg_ppstg_newIndex2_reg_9803_pp0_it8 = ap_reg_ppstg_newIndex2_reg_9803_pp0_it7.read();
        ap_reg_ppstg_newIndex2_reg_9803_pp0_it9 = ap_reg_ppstg_newIndex2_reg_9803_pp0_it8.read();
        ap_reg_ppstg_newIndex8_reg_9924_pp0_it10 = ap_reg_ppstg_newIndex8_reg_9924_pp0_it9.read();
        ap_reg_ppstg_newIndex8_reg_9924_pp0_it11 = ap_reg_ppstg_newIndex8_reg_9924_pp0_it10.read();
        ap_reg_ppstg_newIndex8_reg_9924_pp0_it2 = ap_reg_ppstg_newIndex8_reg_9924_pp0_it1.read();
        ap_reg_ppstg_newIndex8_reg_9924_pp0_it3 = ap_reg_ppstg_newIndex8_reg_9924_pp0_it2.read();
        ap_reg_ppstg_newIndex8_reg_9924_pp0_it4 = ap_reg_ppstg_newIndex8_reg_9924_pp0_it3.read();
        ap_reg_ppstg_newIndex8_reg_9924_pp0_it5 = ap_reg_ppstg_newIndex8_reg_9924_pp0_it4.read();
        ap_reg_ppstg_newIndex8_reg_9924_pp0_it6 = ap_reg_ppstg_newIndex8_reg_9924_pp0_it5.read();
        ap_reg_ppstg_newIndex8_reg_9924_pp0_it7 = ap_reg_ppstg_newIndex8_reg_9924_pp0_it6.read();
        ap_reg_ppstg_newIndex8_reg_9924_pp0_it8 = ap_reg_ppstg_newIndex8_reg_9924_pp0_it7.read();
        ap_reg_ppstg_newIndex8_reg_9924_pp0_it9 = ap_reg_ppstg_newIndex8_reg_9924_pp0_it8.read();
    }
    if (esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it1.read(), ap_const_lv1_0)) {
        imag_i_load_0_phi_reg_12928 = imag_i_load_0_phi_fu_8386_p3.read();
        imag_i_load_1_phi_reg_12952 = imag_i_load_1_phi_fu_8566_p3.read();
        imag_i_load_2_phi_reg_12976 = imag_i_load_2_phi_fu_8746_p3.read();
        imag_i_load_3_phi_reg_13000 = imag_i_load_3_phi_fu_8926_p3.read();
        imag_i_load_4_phi_reg_13024 = imag_i_load_4_phi_fu_9106_p3.read();
        imag_i_load_5_phi_reg_13048 = imag_i_load_5_phi_fu_9286_p3.read();
        imag_i_load_6_phi_reg_13072 = imag_i_load_6_phi_fu_9466_p3.read();
        imag_i_load_7_phi_reg_13096 = imag_i_load_7_phi_fu_9646_p3.read();
        real_i_load_0_phi_reg_12916 = real_i_load_0_phi_fu_8296_p3.read();
        real_i_load_1_phi_reg_12940 = real_i_load_1_phi_fu_8476_p3.read();
        real_i_load_2_phi_reg_12964 = real_i_load_2_phi_fu_8656_p3.read();
        real_i_load_3_phi_reg_12988 = real_i_load_3_phi_fu_8836_p3.read();
        real_i_load_4_phi_reg_13012 = real_i_load_4_phi_fu_9016_p3.read();
        real_i_load_5_phi_reg_13036 = real_i_load_5_phi_fu_9196_p3.read();
        real_i_load_6_phi_reg_13060 = real_i_load_6_phi_fu_9376_p3.read();
        real_i_load_7_phi_reg_13084 = real_i_load_7_phi_fu_9556_p3.read();
        tmp_15_reg_12946 = tmp_15_fu_8483_p34.read();
        tmp_16_reg_12958 = tmp_16_fu_8573_p34.read();
        tmp_17_reg_12970 = tmp_17_fu_8663_p34.read();
        tmp_18_reg_12982 = tmp_18_fu_8753_p34.read();
        tmp_19_reg_12994 = tmp_19_fu_8843_p34.read();
        tmp_20_reg_13006 = tmp_20_fu_8933_p34.read();
        tmp_21_reg_13018 = tmp_21_fu_9023_p34.read();
        tmp_22_reg_13030 = tmp_22_fu_9113_p34.read();
        tmp_23_reg_13042 = tmp_23_fu_9203_p34.read();
        tmp_24_reg_13054 = tmp_24_fu_9293_p34.read();
        tmp_25_reg_13066 = tmp_25_fu_9383_p34.read();
        tmp_26_reg_13078 = tmp_26_fu_9473_p34.read();
        tmp_27_reg_13090 = tmp_27_fu_9563_p34.read();
        tmp_28_reg_13102 = tmp_28_fu_9653_p34.read();
        tmp_8_reg_12922 = tmp_8_fu_8303_p34.read();
        tmp_9_reg_12934 = tmp_9_fu_8393_p34.read();
    }
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && esl_seteq<1,1,1>(exitcond_fu_7237_p2.read(), ap_const_lv1_0))) {
        index0_V_1_cast_reg_9832 = tmp_1_fu_7310_p2.read().range(7, 5);
        index0_V_2_cast_reg_9861 = tmp_2_fu_7357_p2.read().range(7, 5);
        index0_V_3_cast_reg_9890 = tmp_3_fu_7404_p2.read().range(7, 5);
        index0_V_4_cast_reg_9919 = tmp_4_fu_7451_p2.read().range(7, 5);
        index0_V_5_cast_reg_9953 = tmp_5_fu_7508_p2.read().range(7, 5);
        index0_V_6_cast_reg_9987 = tmp_6_fu_7565_p2.read().range(7, 5);
        index0_V_7_cast_reg_10021 = tmp_7_fu_7622_p2.read().range(7, 5);
        index0_V_cast_reg_9798 = tmp1_fu_7243_p2.read().range(7, 5);
        newIndex12_reg_9958 = j_V_9_fu_7502_p2.read().range(8, 2);
        newIndex15_reg_9992 = j_V_2_fu_7559_p2.read().range(8, 2);
        newIndex18_reg_10026 = j_V_3_fu_7616_p2.read().range(8, 2);
        newIndex2_reg_9803 = p_s_reg_7034.read().range(8, 2);
        newIndex8_reg_9924 = j_V_8_fu_7445_p2.read().range(8, 2);
        newIndex_reg_9769 = p_s_reg_7034.read().range(8, 5);
        sel_tmp12_reg_9808 = sel_tmp12_fu_7319_p2.read();
        sel_tmp14_reg_9814 = sel_tmp14_fu_7325_p2.read();
        sel_tmp16_reg_9820 = sel_tmp16_fu_7331_p2.read();
        sel_tmp19_reg_9837 = sel_tmp19_fu_7366_p2.read();
        sel_tmp21_reg_9843 = sel_tmp21_fu_7372_p2.read();
        sel_tmp23_reg_9849 = sel_tmp23_fu_7378_p2.read();
        sel_tmp26_reg_9866 = sel_tmp26_fu_7413_p2.read();
        sel_tmp28_reg_9872 = sel_tmp28_fu_7419_p2.read();
        sel_tmp2_reg_9780 = sel_tmp2_fu_7268_p2.read();
        sel_tmp30_reg_9878 = sel_tmp30_fu_7425_p2.read();
        sel_tmp33_reg_9895 = sel_tmp33_fu_7460_p2.read();
        sel_tmp35_reg_9901 = sel_tmp35_fu_7466_p2.read();
        sel_tmp37_reg_9907 = sel_tmp37_fu_7472_p2.read();
        sel_tmp40_reg_9929 = sel_tmp40_fu_7517_p2.read();
        sel_tmp42_reg_9935 = sel_tmp42_fu_7523_p2.read();
        sel_tmp44_reg_9941 = sel_tmp44_fu_7529_p2.read();
        sel_tmp47_reg_9963 = sel_tmp47_fu_7574_p2.read();
        sel_tmp49_reg_9969 = sel_tmp49_fu_7580_p2.read();
        sel_tmp4_reg_9786 = sel_tmp4_fu_7274_p2.read();
        sel_tmp51_reg_9975 = sel_tmp51_fu_7586_p2.read();
        sel_tmp54_reg_9997 = sel_tmp54_fu_7631_p2.read();
        sel_tmp56_reg_10003 = sel_tmp56_fu_7637_p2.read();
        sel_tmp58_reg_10009 = sel_tmp58_fu_7643_p2.read();
        sel_tmp_reg_9774 = sel_tmp_fu_7262_p2.read();
        tmp_30_reg_9792 = tmp_30_fu_7280_p1.read();
        tmp_32_reg_9826 = tmp_32_fu_7337_p1.read();
        tmp_34_reg_9855 = tmp_34_fu_7384_p1.read();
        tmp_36_reg_9884 = tmp_36_fu_7431_p1.read();
        tmp_38_reg_9913 = tmp_38_fu_7478_p1.read();
        tmp_40_reg_9947 = tmp_40_fu_7535_p1.read();
        tmp_42_reg_9981 = tmp_42_fu_7592_p1.read();
        tmp_44_reg_10015 = tmp_44_fu_7649_p1.read();
    }
    if (esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it5.read(), ap_const_lv1_0)) {
        tmp_10_reg_13113 = grp_fu_7113_p2.read();
        tmp_11_1_reg_13128 = grp_fu_7125_p2.read();
        tmp_11_2_reg_13148 = grp_fu_7141_p2.read();
        tmp_11_3_reg_13168 = grp_fu_7157_p2.read();
        tmp_11_4_reg_13188 = grp_fu_7173_p2.read();
        tmp_11_5_reg_13208 = grp_fu_7189_p2.read();
        tmp_11_6_reg_13228 = grp_fu_7205_p2.read();
        tmp_11_7_reg_13248 = grp_fu_7221_p2.read();
        tmp_12_1_reg_13133 = grp_fu_7129_p2.read();
        tmp_12_2_reg_13153 = grp_fu_7145_p2.read();
        tmp_12_3_reg_13173 = grp_fu_7161_p2.read();
        tmp_12_4_reg_13193 = grp_fu_7177_p2.read();
        tmp_12_5_reg_13213 = grp_fu_7193_p2.read();
        tmp_12_6_reg_13233 = grp_fu_7209_p2.read();
        tmp_12_7_reg_13253 = grp_fu_7225_p2.read();
        tmp_12_reg_13118 = grp_fu_7117_p2.read();
        tmp_13_reg_13123 = grp_fu_7121_p2.read();
        tmp_14_1_reg_13138 = grp_fu_7133_p2.read();
        tmp_14_2_reg_13158 = grp_fu_7149_p2.read();
        tmp_14_3_reg_13178 = grp_fu_7165_p2.read();
        tmp_14_4_reg_13198 = grp_fu_7181_p2.read();
        tmp_14_5_reg_13218 = grp_fu_7197_p2.read();
        tmp_14_6_reg_13238 = grp_fu_7213_p2.read();
        tmp_14_7_reg_13258 = grp_fu_7229_p2.read();
        tmp_15_1_reg_13143 = grp_fu_7137_p2.read();
        tmp_15_2_reg_13163 = grp_fu_7153_p2.read();
        tmp_15_3_reg_13183 = grp_fu_7169_p2.read();
        tmp_15_4_reg_13203 = grp_fu_7185_p2.read();
        tmp_15_5_reg_13223 = grp_fu_7201_p2.read();
        tmp_15_6_reg_13243 = grp_fu_7217_p2.read();
        tmp_15_7_reg_13263 = grp_fu_7233_p2.read();
        tmp_s_reg_13108 = grp_fu_7109_p2.read();
    }
    if (esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it10.read(), ap_const_lv1_0)) {
        tmp_11_reg_13268 = grp_fu_7045_p2.read();
        tmp_13_1_reg_13278 = grp_fu_7053_p2.read();
        tmp_13_2_reg_13288 = grp_fu_7061_p2.read();
        tmp_13_3_reg_13298 = grp_fu_7069_p2.read();
        tmp_13_4_reg_13308 = grp_fu_7077_p2.read();
        tmp_13_5_reg_13318 = grp_fu_7085_p2.read();
        tmp_13_6_reg_13328 = grp_fu_7093_p2.read();
        tmp_13_7_reg_13338 = grp_fu_7101_p2.read();
        tmp_14_reg_13273 = grp_fu_7049_p2.read();
        tmp_16_1_reg_13283 = grp_fu_7057_p2.read();
        tmp_16_2_reg_13293 = grp_fu_7065_p2.read();
        tmp_16_3_reg_13303 = grp_fu_7073_p2.read();
        tmp_16_4_reg_13313 = grp_fu_7081_p2.read();
        tmp_16_5_reg_13323 = grp_fu_7089_p2.read();
        tmp_16_6_reg_13333 = grp_fu_7097_p2.read();
        tmp_16_7_reg_13343 = grp_fu_7105_p2.read();
    }
}

void dft_loop_flow256::thread_ap_NS_fsm() {
    switch (ap_CS_fsm.read().to_uint64()) {
        case 1 : 
            if (!esl_seteq<1,1,1>(ap_start.read(), ap_const_logic_0)) {
                ap_NS_fsm = ap_ST_pp0_stg0_fsm_1;
            } else {
                ap_NS_fsm = ap_ST_st1_fsm_0;
            }
            break;
        case 2 : 
            if ((!(esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read()) && !esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it11.read())) && !(esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it0.read()) && !esl_seteq<1,1,1>(exitcond_fu_7237_p2.read(), ap_const_lv1_0) && !esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read())))) {
                ap_NS_fsm = ap_ST_pp0_stg0_fsm_1;
            } else if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it0.read()) && !esl_seteq<1,1,1>(exitcond_fu_7237_p2.read(), ap_const_lv1_0) && !esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
                ap_NS_fsm = ap_ST_st15_fsm_2;
            } else {
                ap_NS_fsm = ap_ST_st15_fsm_2;
            }
            break;
        case 4 : 
            ap_NS_fsm = ap_ST_st1_fsm_0;
            break;
        default : 
            ap_NS_fsm = "XXX";
            break;
    }
}

}

