# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 338 \
    name real_arr128_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr128_0 \
    op interface \
    ports { real_arr128_0_address0 { O 5 vector } real_arr128_0_ce0 { O 1 bit } real_arr128_0_d0 { O 32 vector } real_arr128_0_q0 { I 32 vector } real_arr128_0_we0 { O 1 bit } real_arr128_0_address1 { O 5 vector } real_arr128_0_ce1 { O 1 bit } real_arr128_0_d1 { O 32 vector } real_arr128_0_q1 { I 32 vector } real_arr128_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr128_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 339 \
    name real_arr128_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr128_1 \
    op interface \
    ports { real_arr128_1_address0 { O 5 vector } real_arr128_1_ce0 { O 1 bit } real_arr128_1_d0 { O 32 vector } real_arr128_1_q0 { I 32 vector } real_arr128_1_we0 { O 1 bit } real_arr128_1_address1 { O 5 vector } real_arr128_1_ce1 { O 1 bit } real_arr128_1_d1 { O 32 vector } real_arr128_1_q1 { I 32 vector } real_arr128_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr128_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 340 \
    name real_arr64_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr64_0 \
    op interface \
    ports { real_arr64_0_address0 { O 4 vector } real_arr64_0_ce0 { O 1 bit } real_arr64_0_d0 { O 32 vector } real_arr64_0_q0 { I 32 vector } real_arr64_0_we0 { O 1 bit } real_arr64_0_address1 { O 4 vector } real_arr64_0_ce1 { O 1 bit } real_arr64_0_d1 { O 32 vector } real_arr64_0_q1 { I 32 vector } real_arr64_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr64_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 341 \
    name imag_arr128_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr128_0 \
    op interface \
    ports { imag_arr128_0_address0 { O 5 vector } imag_arr128_0_ce0 { O 1 bit } imag_arr128_0_d0 { O 32 vector } imag_arr128_0_q0 { I 32 vector } imag_arr128_0_we0 { O 1 bit } imag_arr128_0_address1 { O 5 vector } imag_arr128_0_ce1 { O 1 bit } imag_arr128_0_d1 { O 32 vector } imag_arr128_0_q1 { I 32 vector } imag_arr128_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr128_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 342 \
    name imag_arr128_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr128_1 \
    op interface \
    ports { imag_arr128_1_address0 { O 5 vector } imag_arr128_1_ce0 { O 1 bit } imag_arr128_1_d0 { O 32 vector } imag_arr128_1_q0 { I 32 vector } imag_arr128_1_we0 { O 1 bit } imag_arr128_1_address1 { O 5 vector } imag_arr128_1_ce1 { O 1 bit } imag_arr128_1_d1 { O 32 vector } imag_arr128_1_q1 { I 32 vector } imag_arr128_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr128_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 343 \
    name imag_arr64_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr64_0 \
    op interface \
    ports { imag_arr64_0_address0 { O 4 vector } imag_arr64_0_ce0 { O 1 bit } imag_arr64_0_d0 { O 32 vector } imag_arr64_0_q0 { I 32 vector } imag_arr64_0_we0 { O 1 bit } imag_arr64_0_address1 { O 4 vector } imag_arr64_0_ce1 { O 1 bit } imag_arr64_0_d1 { O 32 vector } imag_arr64_0_q1 { I 32 vector } imag_arr64_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr64_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 344 \
    name real_arr128_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr128_2 \
    op interface \
    ports { real_arr128_2_address0 { O 5 vector } real_arr128_2_ce0 { O 1 bit } real_arr128_2_d0 { O 32 vector } real_arr128_2_q0 { I 32 vector } real_arr128_2_we0 { O 1 bit } real_arr128_2_address1 { O 5 vector } real_arr128_2_ce1 { O 1 bit } real_arr128_2_d1 { O 32 vector } real_arr128_2_q1 { I 32 vector } real_arr128_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr128_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 345 \
    name real_arr128_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr128_3 \
    op interface \
    ports { real_arr128_3_address0 { O 5 vector } real_arr128_3_ce0 { O 1 bit } real_arr128_3_d0 { O 32 vector } real_arr128_3_q0 { I 32 vector } real_arr128_3_we0 { O 1 bit } real_arr128_3_address1 { O 5 vector } real_arr128_3_ce1 { O 1 bit } real_arr128_3_d1 { O 32 vector } real_arr128_3_q1 { I 32 vector } real_arr128_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr128_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 346 \
    name real_arr64_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr64_1 \
    op interface \
    ports { real_arr64_1_address0 { O 4 vector } real_arr64_1_ce0 { O 1 bit } real_arr64_1_d0 { O 32 vector } real_arr64_1_q0 { I 32 vector } real_arr64_1_we0 { O 1 bit } real_arr64_1_address1 { O 4 vector } real_arr64_1_ce1 { O 1 bit } real_arr64_1_d1 { O 32 vector } real_arr64_1_q1 { I 32 vector } real_arr64_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr64_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 347 \
    name imag_arr128_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr128_2 \
    op interface \
    ports { imag_arr128_2_address0 { O 5 vector } imag_arr128_2_ce0 { O 1 bit } imag_arr128_2_d0 { O 32 vector } imag_arr128_2_q0 { I 32 vector } imag_arr128_2_we0 { O 1 bit } imag_arr128_2_address1 { O 5 vector } imag_arr128_2_ce1 { O 1 bit } imag_arr128_2_d1 { O 32 vector } imag_arr128_2_q1 { I 32 vector } imag_arr128_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr128_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 348 \
    name imag_arr128_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr128_3 \
    op interface \
    ports { imag_arr128_3_address0 { O 5 vector } imag_arr128_3_ce0 { O 1 bit } imag_arr128_3_d0 { O 32 vector } imag_arr128_3_q0 { I 32 vector } imag_arr128_3_we0 { O 1 bit } imag_arr128_3_address1 { O 5 vector } imag_arr128_3_ce1 { O 1 bit } imag_arr128_3_d1 { O 32 vector } imag_arr128_3_q1 { I 32 vector } imag_arr128_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr128_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 349 \
    name imag_arr64_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr64_1 \
    op interface \
    ports { imag_arr64_1_address0 { O 4 vector } imag_arr64_1_ce0 { O 1 bit } imag_arr64_1_d0 { O 32 vector } imag_arr64_1_q0 { I 32 vector } imag_arr64_1_we0 { O 1 bit } imag_arr64_1_address1 { O 4 vector } imag_arr64_1_ce1 { O 1 bit } imag_arr64_1_d1 { O 32 vector } imag_arr64_1_q1 { I 32 vector } imag_arr64_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr64_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 350 \
    name real_arr64_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr64_2 \
    op interface \
    ports { real_arr64_2_address0 { O 4 vector } real_arr64_2_ce0 { O 1 bit } real_arr64_2_d0 { O 32 vector } real_arr64_2_q0 { I 32 vector } real_arr64_2_we0 { O 1 bit } real_arr64_2_address1 { O 4 vector } real_arr64_2_ce1 { O 1 bit } real_arr64_2_d1 { O 32 vector } real_arr64_2_q1 { I 32 vector } real_arr64_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr64_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 351 \
    name imag_arr64_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr64_2 \
    op interface \
    ports { imag_arr64_2_address0 { O 4 vector } imag_arr64_2_ce0 { O 1 bit } imag_arr64_2_d0 { O 32 vector } imag_arr64_2_q0 { I 32 vector } imag_arr64_2_we0 { O 1 bit } imag_arr64_2_address1 { O 4 vector } imag_arr64_2_ce1 { O 1 bit } imag_arr64_2_d1 { O 32 vector } imag_arr64_2_q1 { I 32 vector } imag_arr64_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr64_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 352 \
    name real_arr64_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr64_3 \
    op interface \
    ports { real_arr64_3_address0 { O 4 vector } real_arr64_3_ce0 { O 1 bit } real_arr64_3_d0 { O 32 vector } real_arr64_3_q0 { I 32 vector } real_arr64_3_we0 { O 1 bit } real_arr64_3_address1 { O 4 vector } real_arr64_3_ce1 { O 1 bit } real_arr64_3_d1 { O 32 vector } real_arr64_3_q1 { I 32 vector } real_arr64_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr64_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 353 \
    name imag_arr64_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr64_3 \
    op interface \
    ports { imag_arr64_3_address0 { O 4 vector } imag_arr64_3_ce0 { O 1 bit } imag_arr64_3_d0 { O 32 vector } imag_arr64_3_q0 { I 32 vector } imag_arr64_3_we0 { O 1 bit } imag_arr64_3_address1 { O 4 vector } imag_arr64_3_ce1 { O 1 bit } imag_arr64_3_d1 { O 32 vector } imag_arr64_3_q1 { I 32 vector } imag_arr64_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr64_3'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


