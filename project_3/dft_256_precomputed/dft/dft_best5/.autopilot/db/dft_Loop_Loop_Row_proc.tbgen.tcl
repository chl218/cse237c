set moduleName dft_Loop_Loop_Row_proc
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {dft_Loop_Loop_Row_proc}
set C_modelType { void 0 }
set C_modelArgList { 
	{ real_i_0 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_1 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_2 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_3 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_4 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_5 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_6 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_7 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_8 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_9 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_10 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_11 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_12 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_13 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_14 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_15 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_16 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_17 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_18 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_19 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_20 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_21 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_22 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_23 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_24 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_25 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_26 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_27 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_28 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_29 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_30 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_i_31 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_0 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_1 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_2 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_3 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_4 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_5 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_6 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_7 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_8 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_9 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_10 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_11 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_12 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_13 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_14 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_15 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_16 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_17 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_18 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_19 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_20 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_21 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_22 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_23 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_24 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_25 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_26 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_27 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_28 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_29 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_30 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ imag_i_31 float 32 regular {array 8 { 1 3 } 1 1 }  }
	{ real_o float 32 regular {array 256 { 0 3 } 0 1 }  }
	{ imag_o float 32 regular {array 256 { 0 3 } 0 1 }  }
	{ real_arr256_0 float 32 regular {array 64 { 2 2 } 1 1 } {global 2}  }
	{ imag_arr256_0 float 32 regular {array 64 { 2 2 } 1 1 } {global 2}  }
	{ real_arr256_1 float 32 regular {array 64 { 2 2 } 1 1 } {global 2}  }
	{ imag_arr256_1 float 32 regular {array 64 { 2 2 } 1 1 } {global 2}  }
	{ real_arr256_2 float 32 regular {array 64 { 2 2 } 1 1 } {global 2}  }
	{ imag_arr256_2 float 32 regular {array 64 { 2 2 } 1 1 } {global 2}  }
	{ real_arr256_3 float 32 regular {array 64 { 2 2 } 1 1 } {global 2}  }
	{ imag_arr256_3 float 32 regular {array 64 { 2 2 } 1 1 } {global 2}  }
	{ real_arr128_0 float 32 regular {array 32 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr128_0 float 32 regular {array 32 { 2 1 } 1 1 } {global 2}  }
	{ real_arr128_1 float 32 regular {array 32 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr128_1 float 32 regular {array 32 { 2 1 } 1 1 } {global 2}  }
	{ real_arr128_2 float 32 regular {array 32 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr128_2 float 32 regular {array 32 { 2 1 } 1 1 } {global 2}  }
	{ real_arr128_3 float 32 regular {array 32 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr128_3 float 32 regular {array 32 { 2 1 } 1 1 } {global 2}  }
	{ real_arr64_0 float 32 regular {array 16 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr64_0 float 32 regular {array 16 { 2 1 } 1 1 } {global 2}  }
	{ real_arr64_1 float 32 regular {array 16 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr64_1 float 32 regular {array 16 { 2 1 } 1 1 } {global 2}  }
	{ real_arr64_2 float 32 regular {array 16 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr64_2 float 32 regular {array 16 { 2 1 } 1 1 } {global 2}  }
	{ real_arr64_3 float 32 regular {array 16 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr64_3 float 32 regular {array 16 { 2 1 } 1 1 } {global 2}  }
	{ real_arr32_0 float 32 regular {array 8 { 2 3 } 1 1 } {global 2}  }
	{ imag_arr32_0 float 32 regular {array 8 { 2 3 } 1 1 } {global 2}  }
	{ real_arr32_1 float 32 regular {array 8 { 2 3 } 1 1 } {global 2}  }
	{ imag_arr32_1 float 32 regular {array 8 { 2 3 } 1 1 } {global 2}  }
	{ real_arr32_2 float 32 regular {array 8 { 2 3 } 1 1 } {global 2}  }
	{ imag_arr32_2 float 32 regular {array 8 { 2 3 } 1 1 } {global 2}  }
	{ real_arr32_3 float 32 regular {array 8 { 2 3 } 1 1 } {global 2}  }
	{ imag_arr32_3 float 32 regular {array 8 { 2 3 } 1 1 } {global 2}  }
	{ imag_arr16_1 float 32 regular {array 4 { 2 3 } 1 1 } {global 2}  }
	{ real_arr16_1 float 32 regular {array 4 { 2 3 } 1 1 } {global 2}  }
	{ imag_arr16_0 float 32 regular {array 4 { 2 3 } 1 1 } {global 2}  }
	{ real_arr16_0 float 32 regular {array 4 { 2 3 } 1 1 } {global 2}  }
	{ real_arr16_2 float 32 regular {array 4 { 2 3 } 1 1 } {global 2}  }
	{ imag_arr16_2 float 32 regular {array 4 { 2 3 } 1 1 } {global 2}  }
	{ real_arr16_3 float 32 regular {array 4 { 2 3 } 1 1 } {global 2}  }
	{ imag_arr16_3 float 32 regular {array 4 { 2 3 } 1 1 } {global 2}  }
	{ imag_arr8_1 float 32 regular {pointer 2} {global 2}  }
	{ real_arr8_1 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr8_0 float 32 regular {pointer 2} {global 2}  }
	{ real_arr8_0 float 32 regular {pointer 2} {global 2}  }
	{ real_arr8_2 float 32 regular {pointer 2} {global 2}  }
	{ real_arr8_4 float 32 regular {pointer 2} {global 2}  }
	{ real_arr8_6 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr8_2 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr8_4 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr8_6 float 32 regular {pointer 2} {global 2}  }
	{ real_arr8_3 float 32 regular {pointer 2} {global 2}  }
	{ real_arr8_5 float 32 regular {pointer 2} {global 2}  }
	{ real_arr8_7 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr8_3 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr8_5 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr8_7 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr4_0 float 32 regular {pointer 2} {global 2}  }
	{ real_arr4_0 float 32 regular {pointer 2} {global 2}  }
	{ real_arr4_1 float 32 regular {pointer 2} {global 2}  }
	{ real_arr4_2 float 32 regular {pointer 2} {global 2}  }
	{ real_arr4_3 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr4_1 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr4_2 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr4_3 float 32 regular {pointer 2} {global 2}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "real_i_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_4", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_5", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_6", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_7", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_8", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_9", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_10", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_11", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_12", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_13", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_14", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_15", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_16", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_17", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_18", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_19", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_20", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_21", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_22", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_23", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_24", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_25", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_26", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_27", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_28", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_29", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_30", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_31", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_4", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_5", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_6", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_7", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_8", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_9", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_10", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_11", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_12", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_13", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_14", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_15", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_16", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_17", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_18", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_19", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_20", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_21", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_22", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_23", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_24", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_25", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_26", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_27", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_28", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_29", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_30", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_31", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_o", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "imag_o", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "real_arr256_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 252,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr256_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 252,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr256_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 253,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr256_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 253,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr256_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 254,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr256_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 254,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr256_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 255,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr256_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 255,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr128_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 124,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr128_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 124,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr128_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 125,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr128_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 125,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr128_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 126,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr128_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 126,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr128_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 127,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr128_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 127,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr64_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr64","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 60,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr64_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr64","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 60,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr64_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr64","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 61,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr64_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr64","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 61,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr64_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr64","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 62,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr64_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr64","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 62,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr64_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr64","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 63,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr64_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr64","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 63,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr32_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr32","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 28,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr32_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr32","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 28,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr32_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr32","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 29,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr32_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr32","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 29,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr32_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr32","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 30,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr32_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr32","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 30,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr32_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr32","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 31,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr32_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr32","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 31,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr16_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 13,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr16_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 13,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr16_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 12,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr16_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 12,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr16_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 14,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr16_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 14,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr16_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 15,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr16_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 15,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_1", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_1", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_0", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_0", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_2", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 2,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_4", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 4,"up" : 4,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_6", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 6,"up" : 6,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_2", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 2,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_4", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 4,"up" : 4,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_6", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 6,"up" : 6,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_3", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 3,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_5", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 5,"up" : 5,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_7", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 7,"up" : 7,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_3", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 3,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_5", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 5,"up" : 5,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_7", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 7,"up" : 7,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr4_0", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr4_0", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr4_1", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr4_2", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 2,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr4_3", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 3,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr4_1", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr4_2", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 2,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr4_3", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 3,"step" : 2}]}]}], "extern" : 0} ]}
# RTL Port declarations: 
set portNum 567
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ real_i_0_address0 sc_out sc_lv 3 signal 0 } 
	{ real_i_0_ce0 sc_out sc_logic 1 signal 0 } 
	{ real_i_0_q0 sc_in sc_lv 32 signal 0 } 
	{ real_i_1_address0 sc_out sc_lv 3 signal 1 } 
	{ real_i_1_ce0 sc_out sc_logic 1 signal 1 } 
	{ real_i_1_q0 sc_in sc_lv 32 signal 1 } 
	{ real_i_2_address0 sc_out sc_lv 3 signal 2 } 
	{ real_i_2_ce0 sc_out sc_logic 1 signal 2 } 
	{ real_i_2_q0 sc_in sc_lv 32 signal 2 } 
	{ real_i_3_address0 sc_out sc_lv 3 signal 3 } 
	{ real_i_3_ce0 sc_out sc_logic 1 signal 3 } 
	{ real_i_3_q0 sc_in sc_lv 32 signal 3 } 
	{ real_i_4_address0 sc_out sc_lv 3 signal 4 } 
	{ real_i_4_ce0 sc_out sc_logic 1 signal 4 } 
	{ real_i_4_q0 sc_in sc_lv 32 signal 4 } 
	{ real_i_5_address0 sc_out sc_lv 3 signal 5 } 
	{ real_i_5_ce0 sc_out sc_logic 1 signal 5 } 
	{ real_i_5_q0 sc_in sc_lv 32 signal 5 } 
	{ real_i_6_address0 sc_out sc_lv 3 signal 6 } 
	{ real_i_6_ce0 sc_out sc_logic 1 signal 6 } 
	{ real_i_6_q0 sc_in sc_lv 32 signal 6 } 
	{ real_i_7_address0 sc_out sc_lv 3 signal 7 } 
	{ real_i_7_ce0 sc_out sc_logic 1 signal 7 } 
	{ real_i_7_q0 sc_in sc_lv 32 signal 7 } 
	{ real_i_8_address0 sc_out sc_lv 3 signal 8 } 
	{ real_i_8_ce0 sc_out sc_logic 1 signal 8 } 
	{ real_i_8_q0 sc_in sc_lv 32 signal 8 } 
	{ real_i_9_address0 sc_out sc_lv 3 signal 9 } 
	{ real_i_9_ce0 sc_out sc_logic 1 signal 9 } 
	{ real_i_9_q0 sc_in sc_lv 32 signal 9 } 
	{ real_i_10_address0 sc_out sc_lv 3 signal 10 } 
	{ real_i_10_ce0 sc_out sc_logic 1 signal 10 } 
	{ real_i_10_q0 sc_in sc_lv 32 signal 10 } 
	{ real_i_11_address0 sc_out sc_lv 3 signal 11 } 
	{ real_i_11_ce0 sc_out sc_logic 1 signal 11 } 
	{ real_i_11_q0 sc_in sc_lv 32 signal 11 } 
	{ real_i_12_address0 sc_out sc_lv 3 signal 12 } 
	{ real_i_12_ce0 sc_out sc_logic 1 signal 12 } 
	{ real_i_12_q0 sc_in sc_lv 32 signal 12 } 
	{ real_i_13_address0 sc_out sc_lv 3 signal 13 } 
	{ real_i_13_ce0 sc_out sc_logic 1 signal 13 } 
	{ real_i_13_q0 sc_in sc_lv 32 signal 13 } 
	{ real_i_14_address0 sc_out sc_lv 3 signal 14 } 
	{ real_i_14_ce0 sc_out sc_logic 1 signal 14 } 
	{ real_i_14_q0 sc_in sc_lv 32 signal 14 } 
	{ real_i_15_address0 sc_out sc_lv 3 signal 15 } 
	{ real_i_15_ce0 sc_out sc_logic 1 signal 15 } 
	{ real_i_15_q0 sc_in sc_lv 32 signal 15 } 
	{ real_i_16_address0 sc_out sc_lv 3 signal 16 } 
	{ real_i_16_ce0 sc_out sc_logic 1 signal 16 } 
	{ real_i_16_q0 sc_in sc_lv 32 signal 16 } 
	{ real_i_17_address0 sc_out sc_lv 3 signal 17 } 
	{ real_i_17_ce0 sc_out sc_logic 1 signal 17 } 
	{ real_i_17_q0 sc_in sc_lv 32 signal 17 } 
	{ real_i_18_address0 sc_out sc_lv 3 signal 18 } 
	{ real_i_18_ce0 sc_out sc_logic 1 signal 18 } 
	{ real_i_18_q0 sc_in sc_lv 32 signal 18 } 
	{ real_i_19_address0 sc_out sc_lv 3 signal 19 } 
	{ real_i_19_ce0 sc_out sc_logic 1 signal 19 } 
	{ real_i_19_q0 sc_in sc_lv 32 signal 19 } 
	{ real_i_20_address0 sc_out sc_lv 3 signal 20 } 
	{ real_i_20_ce0 sc_out sc_logic 1 signal 20 } 
	{ real_i_20_q0 sc_in sc_lv 32 signal 20 } 
	{ real_i_21_address0 sc_out sc_lv 3 signal 21 } 
	{ real_i_21_ce0 sc_out sc_logic 1 signal 21 } 
	{ real_i_21_q0 sc_in sc_lv 32 signal 21 } 
	{ real_i_22_address0 sc_out sc_lv 3 signal 22 } 
	{ real_i_22_ce0 sc_out sc_logic 1 signal 22 } 
	{ real_i_22_q0 sc_in sc_lv 32 signal 22 } 
	{ real_i_23_address0 sc_out sc_lv 3 signal 23 } 
	{ real_i_23_ce0 sc_out sc_logic 1 signal 23 } 
	{ real_i_23_q0 sc_in sc_lv 32 signal 23 } 
	{ real_i_24_address0 sc_out sc_lv 3 signal 24 } 
	{ real_i_24_ce0 sc_out sc_logic 1 signal 24 } 
	{ real_i_24_q0 sc_in sc_lv 32 signal 24 } 
	{ real_i_25_address0 sc_out sc_lv 3 signal 25 } 
	{ real_i_25_ce0 sc_out sc_logic 1 signal 25 } 
	{ real_i_25_q0 sc_in sc_lv 32 signal 25 } 
	{ real_i_26_address0 sc_out sc_lv 3 signal 26 } 
	{ real_i_26_ce0 sc_out sc_logic 1 signal 26 } 
	{ real_i_26_q0 sc_in sc_lv 32 signal 26 } 
	{ real_i_27_address0 sc_out sc_lv 3 signal 27 } 
	{ real_i_27_ce0 sc_out sc_logic 1 signal 27 } 
	{ real_i_27_q0 sc_in sc_lv 32 signal 27 } 
	{ real_i_28_address0 sc_out sc_lv 3 signal 28 } 
	{ real_i_28_ce0 sc_out sc_logic 1 signal 28 } 
	{ real_i_28_q0 sc_in sc_lv 32 signal 28 } 
	{ real_i_29_address0 sc_out sc_lv 3 signal 29 } 
	{ real_i_29_ce0 sc_out sc_logic 1 signal 29 } 
	{ real_i_29_q0 sc_in sc_lv 32 signal 29 } 
	{ real_i_30_address0 sc_out sc_lv 3 signal 30 } 
	{ real_i_30_ce0 sc_out sc_logic 1 signal 30 } 
	{ real_i_30_q0 sc_in sc_lv 32 signal 30 } 
	{ real_i_31_address0 sc_out sc_lv 3 signal 31 } 
	{ real_i_31_ce0 sc_out sc_logic 1 signal 31 } 
	{ real_i_31_q0 sc_in sc_lv 32 signal 31 } 
	{ imag_i_0_address0 sc_out sc_lv 3 signal 32 } 
	{ imag_i_0_ce0 sc_out sc_logic 1 signal 32 } 
	{ imag_i_0_q0 sc_in sc_lv 32 signal 32 } 
	{ imag_i_1_address0 sc_out sc_lv 3 signal 33 } 
	{ imag_i_1_ce0 sc_out sc_logic 1 signal 33 } 
	{ imag_i_1_q0 sc_in sc_lv 32 signal 33 } 
	{ imag_i_2_address0 sc_out sc_lv 3 signal 34 } 
	{ imag_i_2_ce0 sc_out sc_logic 1 signal 34 } 
	{ imag_i_2_q0 sc_in sc_lv 32 signal 34 } 
	{ imag_i_3_address0 sc_out sc_lv 3 signal 35 } 
	{ imag_i_3_ce0 sc_out sc_logic 1 signal 35 } 
	{ imag_i_3_q0 sc_in sc_lv 32 signal 35 } 
	{ imag_i_4_address0 sc_out sc_lv 3 signal 36 } 
	{ imag_i_4_ce0 sc_out sc_logic 1 signal 36 } 
	{ imag_i_4_q0 sc_in sc_lv 32 signal 36 } 
	{ imag_i_5_address0 sc_out sc_lv 3 signal 37 } 
	{ imag_i_5_ce0 sc_out sc_logic 1 signal 37 } 
	{ imag_i_5_q0 sc_in sc_lv 32 signal 37 } 
	{ imag_i_6_address0 sc_out sc_lv 3 signal 38 } 
	{ imag_i_6_ce0 sc_out sc_logic 1 signal 38 } 
	{ imag_i_6_q0 sc_in sc_lv 32 signal 38 } 
	{ imag_i_7_address0 sc_out sc_lv 3 signal 39 } 
	{ imag_i_7_ce0 sc_out sc_logic 1 signal 39 } 
	{ imag_i_7_q0 sc_in sc_lv 32 signal 39 } 
	{ imag_i_8_address0 sc_out sc_lv 3 signal 40 } 
	{ imag_i_8_ce0 sc_out sc_logic 1 signal 40 } 
	{ imag_i_8_q0 sc_in sc_lv 32 signal 40 } 
	{ imag_i_9_address0 sc_out sc_lv 3 signal 41 } 
	{ imag_i_9_ce0 sc_out sc_logic 1 signal 41 } 
	{ imag_i_9_q0 sc_in sc_lv 32 signal 41 } 
	{ imag_i_10_address0 sc_out sc_lv 3 signal 42 } 
	{ imag_i_10_ce0 sc_out sc_logic 1 signal 42 } 
	{ imag_i_10_q0 sc_in sc_lv 32 signal 42 } 
	{ imag_i_11_address0 sc_out sc_lv 3 signal 43 } 
	{ imag_i_11_ce0 sc_out sc_logic 1 signal 43 } 
	{ imag_i_11_q0 sc_in sc_lv 32 signal 43 } 
	{ imag_i_12_address0 sc_out sc_lv 3 signal 44 } 
	{ imag_i_12_ce0 sc_out sc_logic 1 signal 44 } 
	{ imag_i_12_q0 sc_in sc_lv 32 signal 44 } 
	{ imag_i_13_address0 sc_out sc_lv 3 signal 45 } 
	{ imag_i_13_ce0 sc_out sc_logic 1 signal 45 } 
	{ imag_i_13_q0 sc_in sc_lv 32 signal 45 } 
	{ imag_i_14_address0 sc_out sc_lv 3 signal 46 } 
	{ imag_i_14_ce0 sc_out sc_logic 1 signal 46 } 
	{ imag_i_14_q0 sc_in sc_lv 32 signal 46 } 
	{ imag_i_15_address0 sc_out sc_lv 3 signal 47 } 
	{ imag_i_15_ce0 sc_out sc_logic 1 signal 47 } 
	{ imag_i_15_q0 sc_in sc_lv 32 signal 47 } 
	{ imag_i_16_address0 sc_out sc_lv 3 signal 48 } 
	{ imag_i_16_ce0 sc_out sc_logic 1 signal 48 } 
	{ imag_i_16_q0 sc_in sc_lv 32 signal 48 } 
	{ imag_i_17_address0 sc_out sc_lv 3 signal 49 } 
	{ imag_i_17_ce0 sc_out sc_logic 1 signal 49 } 
	{ imag_i_17_q0 sc_in sc_lv 32 signal 49 } 
	{ imag_i_18_address0 sc_out sc_lv 3 signal 50 } 
	{ imag_i_18_ce0 sc_out sc_logic 1 signal 50 } 
	{ imag_i_18_q0 sc_in sc_lv 32 signal 50 } 
	{ imag_i_19_address0 sc_out sc_lv 3 signal 51 } 
	{ imag_i_19_ce0 sc_out sc_logic 1 signal 51 } 
	{ imag_i_19_q0 sc_in sc_lv 32 signal 51 } 
	{ imag_i_20_address0 sc_out sc_lv 3 signal 52 } 
	{ imag_i_20_ce0 sc_out sc_logic 1 signal 52 } 
	{ imag_i_20_q0 sc_in sc_lv 32 signal 52 } 
	{ imag_i_21_address0 sc_out sc_lv 3 signal 53 } 
	{ imag_i_21_ce0 sc_out sc_logic 1 signal 53 } 
	{ imag_i_21_q0 sc_in sc_lv 32 signal 53 } 
	{ imag_i_22_address0 sc_out sc_lv 3 signal 54 } 
	{ imag_i_22_ce0 sc_out sc_logic 1 signal 54 } 
	{ imag_i_22_q0 sc_in sc_lv 32 signal 54 } 
	{ imag_i_23_address0 sc_out sc_lv 3 signal 55 } 
	{ imag_i_23_ce0 sc_out sc_logic 1 signal 55 } 
	{ imag_i_23_q0 sc_in sc_lv 32 signal 55 } 
	{ imag_i_24_address0 sc_out sc_lv 3 signal 56 } 
	{ imag_i_24_ce0 sc_out sc_logic 1 signal 56 } 
	{ imag_i_24_q0 sc_in sc_lv 32 signal 56 } 
	{ imag_i_25_address0 sc_out sc_lv 3 signal 57 } 
	{ imag_i_25_ce0 sc_out sc_logic 1 signal 57 } 
	{ imag_i_25_q0 sc_in sc_lv 32 signal 57 } 
	{ imag_i_26_address0 sc_out sc_lv 3 signal 58 } 
	{ imag_i_26_ce0 sc_out sc_logic 1 signal 58 } 
	{ imag_i_26_q0 sc_in sc_lv 32 signal 58 } 
	{ imag_i_27_address0 sc_out sc_lv 3 signal 59 } 
	{ imag_i_27_ce0 sc_out sc_logic 1 signal 59 } 
	{ imag_i_27_q0 sc_in sc_lv 32 signal 59 } 
	{ imag_i_28_address0 sc_out sc_lv 3 signal 60 } 
	{ imag_i_28_ce0 sc_out sc_logic 1 signal 60 } 
	{ imag_i_28_q0 sc_in sc_lv 32 signal 60 } 
	{ imag_i_29_address0 sc_out sc_lv 3 signal 61 } 
	{ imag_i_29_ce0 sc_out sc_logic 1 signal 61 } 
	{ imag_i_29_q0 sc_in sc_lv 32 signal 61 } 
	{ imag_i_30_address0 sc_out sc_lv 3 signal 62 } 
	{ imag_i_30_ce0 sc_out sc_logic 1 signal 62 } 
	{ imag_i_30_q0 sc_in sc_lv 32 signal 62 } 
	{ imag_i_31_address0 sc_out sc_lv 3 signal 63 } 
	{ imag_i_31_ce0 sc_out sc_logic 1 signal 63 } 
	{ imag_i_31_q0 sc_in sc_lv 32 signal 63 } 
	{ real_o_address0 sc_out sc_lv 8 signal 64 } 
	{ real_o_ce0 sc_out sc_logic 1 signal 64 } 
	{ real_o_we0 sc_out sc_logic 1 signal 64 } 
	{ real_o_d0 sc_out sc_lv 32 signal 64 } 
	{ imag_o_address0 sc_out sc_lv 8 signal 65 } 
	{ imag_o_ce0 sc_out sc_logic 1 signal 65 } 
	{ imag_o_we0 sc_out sc_logic 1 signal 65 } 
	{ imag_o_d0 sc_out sc_lv 32 signal 65 } 
	{ real_arr256_0_address0 sc_out sc_lv 6 signal 66 } 
	{ real_arr256_0_ce0 sc_out sc_logic 1 signal 66 } 
	{ real_arr256_0_we0 sc_out sc_logic 1 signal 66 } 
	{ real_arr256_0_d0 sc_out sc_lv 32 signal 66 } 
	{ real_arr256_0_q0 sc_in sc_lv 32 signal 66 } 
	{ real_arr256_0_address1 sc_out sc_lv 6 signal 66 } 
	{ real_arr256_0_ce1 sc_out sc_logic 1 signal 66 } 
	{ real_arr256_0_we1 sc_out sc_logic 1 signal 66 } 
	{ real_arr256_0_d1 sc_out sc_lv 32 signal 66 } 
	{ real_arr256_0_q1 sc_in sc_lv 32 signal 66 } 
	{ imag_arr256_0_address0 sc_out sc_lv 6 signal 67 } 
	{ imag_arr256_0_ce0 sc_out sc_logic 1 signal 67 } 
	{ imag_arr256_0_we0 sc_out sc_logic 1 signal 67 } 
	{ imag_arr256_0_d0 sc_out sc_lv 32 signal 67 } 
	{ imag_arr256_0_q0 sc_in sc_lv 32 signal 67 } 
	{ imag_arr256_0_address1 sc_out sc_lv 6 signal 67 } 
	{ imag_arr256_0_ce1 sc_out sc_logic 1 signal 67 } 
	{ imag_arr256_0_we1 sc_out sc_logic 1 signal 67 } 
	{ imag_arr256_0_d1 sc_out sc_lv 32 signal 67 } 
	{ imag_arr256_0_q1 sc_in sc_lv 32 signal 67 } 
	{ real_arr256_1_address0 sc_out sc_lv 6 signal 68 } 
	{ real_arr256_1_ce0 sc_out sc_logic 1 signal 68 } 
	{ real_arr256_1_we0 sc_out sc_logic 1 signal 68 } 
	{ real_arr256_1_d0 sc_out sc_lv 32 signal 68 } 
	{ real_arr256_1_q0 sc_in sc_lv 32 signal 68 } 
	{ real_arr256_1_address1 sc_out sc_lv 6 signal 68 } 
	{ real_arr256_1_ce1 sc_out sc_logic 1 signal 68 } 
	{ real_arr256_1_we1 sc_out sc_logic 1 signal 68 } 
	{ real_arr256_1_d1 sc_out sc_lv 32 signal 68 } 
	{ real_arr256_1_q1 sc_in sc_lv 32 signal 68 } 
	{ imag_arr256_1_address0 sc_out sc_lv 6 signal 69 } 
	{ imag_arr256_1_ce0 sc_out sc_logic 1 signal 69 } 
	{ imag_arr256_1_we0 sc_out sc_logic 1 signal 69 } 
	{ imag_arr256_1_d0 sc_out sc_lv 32 signal 69 } 
	{ imag_arr256_1_q0 sc_in sc_lv 32 signal 69 } 
	{ imag_arr256_1_address1 sc_out sc_lv 6 signal 69 } 
	{ imag_arr256_1_ce1 sc_out sc_logic 1 signal 69 } 
	{ imag_arr256_1_we1 sc_out sc_logic 1 signal 69 } 
	{ imag_arr256_1_d1 sc_out sc_lv 32 signal 69 } 
	{ imag_arr256_1_q1 sc_in sc_lv 32 signal 69 } 
	{ real_arr256_2_address0 sc_out sc_lv 6 signal 70 } 
	{ real_arr256_2_ce0 sc_out sc_logic 1 signal 70 } 
	{ real_arr256_2_we0 sc_out sc_logic 1 signal 70 } 
	{ real_arr256_2_d0 sc_out sc_lv 32 signal 70 } 
	{ real_arr256_2_q0 sc_in sc_lv 32 signal 70 } 
	{ real_arr256_2_address1 sc_out sc_lv 6 signal 70 } 
	{ real_arr256_2_ce1 sc_out sc_logic 1 signal 70 } 
	{ real_arr256_2_we1 sc_out sc_logic 1 signal 70 } 
	{ real_arr256_2_d1 sc_out sc_lv 32 signal 70 } 
	{ real_arr256_2_q1 sc_in sc_lv 32 signal 70 } 
	{ imag_arr256_2_address0 sc_out sc_lv 6 signal 71 } 
	{ imag_arr256_2_ce0 sc_out sc_logic 1 signal 71 } 
	{ imag_arr256_2_we0 sc_out sc_logic 1 signal 71 } 
	{ imag_arr256_2_d0 sc_out sc_lv 32 signal 71 } 
	{ imag_arr256_2_q0 sc_in sc_lv 32 signal 71 } 
	{ imag_arr256_2_address1 sc_out sc_lv 6 signal 71 } 
	{ imag_arr256_2_ce1 sc_out sc_logic 1 signal 71 } 
	{ imag_arr256_2_we1 sc_out sc_logic 1 signal 71 } 
	{ imag_arr256_2_d1 sc_out sc_lv 32 signal 71 } 
	{ imag_arr256_2_q1 sc_in sc_lv 32 signal 71 } 
	{ real_arr256_3_address0 sc_out sc_lv 6 signal 72 } 
	{ real_arr256_3_ce0 sc_out sc_logic 1 signal 72 } 
	{ real_arr256_3_we0 sc_out sc_logic 1 signal 72 } 
	{ real_arr256_3_d0 sc_out sc_lv 32 signal 72 } 
	{ real_arr256_3_q0 sc_in sc_lv 32 signal 72 } 
	{ real_arr256_3_address1 sc_out sc_lv 6 signal 72 } 
	{ real_arr256_3_ce1 sc_out sc_logic 1 signal 72 } 
	{ real_arr256_3_we1 sc_out sc_logic 1 signal 72 } 
	{ real_arr256_3_d1 sc_out sc_lv 32 signal 72 } 
	{ real_arr256_3_q1 sc_in sc_lv 32 signal 72 } 
	{ imag_arr256_3_address0 sc_out sc_lv 6 signal 73 } 
	{ imag_arr256_3_ce0 sc_out sc_logic 1 signal 73 } 
	{ imag_arr256_3_we0 sc_out sc_logic 1 signal 73 } 
	{ imag_arr256_3_d0 sc_out sc_lv 32 signal 73 } 
	{ imag_arr256_3_q0 sc_in sc_lv 32 signal 73 } 
	{ imag_arr256_3_address1 sc_out sc_lv 6 signal 73 } 
	{ imag_arr256_3_ce1 sc_out sc_logic 1 signal 73 } 
	{ imag_arr256_3_we1 sc_out sc_logic 1 signal 73 } 
	{ imag_arr256_3_d1 sc_out sc_lv 32 signal 73 } 
	{ imag_arr256_3_q1 sc_in sc_lv 32 signal 73 } 
	{ real_arr128_0_address0 sc_out sc_lv 5 signal 74 } 
	{ real_arr128_0_ce0 sc_out sc_logic 1 signal 74 } 
	{ real_arr128_0_we0 sc_out sc_logic 1 signal 74 } 
	{ real_arr128_0_d0 sc_out sc_lv 32 signal 74 } 
	{ real_arr128_0_q0 sc_in sc_lv 32 signal 74 } 
	{ real_arr128_0_address1 sc_out sc_lv 5 signal 74 } 
	{ real_arr128_0_ce1 sc_out sc_logic 1 signal 74 } 
	{ real_arr128_0_q1 sc_in sc_lv 32 signal 74 } 
	{ imag_arr128_0_address0 sc_out sc_lv 5 signal 75 } 
	{ imag_arr128_0_ce0 sc_out sc_logic 1 signal 75 } 
	{ imag_arr128_0_we0 sc_out sc_logic 1 signal 75 } 
	{ imag_arr128_0_d0 sc_out sc_lv 32 signal 75 } 
	{ imag_arr128_0_q0 sc_in sc_lv 32 signal 75 } 
	{ imag_arr128_0_address1 sc_out sc_lv 5 signal 75 } 
	{ imag_arr128_0_ce1 sc_out sc_logic 1 signal 75 } 
	{ imag_arr128_0_q1 sc_in sc_lv 32 signal 75 } 
	{ real_arr128_1_address0 sc_out sc_lv 5 signal 76 } 
	{ real_arr128_1_ce0 sc_out sc_logic 1 signal 76 } 
	{ real_arr128_1_we0 sc_out sc_logic 1 signal 76 } 
	{ real_arr128_1_d0 sc_out sc_lv 32 signal 76 } 
	{ real_arr128_1_q0 sc_in sc_lv 32 signal 76 } 
	{ real_arr128_1_address1 sc_out sc_lv 5 signal 76 } 
	{ real_arr128_1_ce1 sc_out sc_logic 1 signal 76 } 
	{ real_arr128_1_q1 sc_in sc_lv 32 signal 76 } 
	{ imag_arr128_1_address0 sc_out sc_lv 5 signal 77 } 
	{ imag_arr128_1_ce0 sc_out sc_logic 1 signal 77 } 
	{ imag_arr128_1_we0 sc_out sc_logic 1 signal 77 } 
	{ imag_arr128_1_d0 sc_out sc_lv 32 signal 77 } 
	{ imag_arr128_1_q0 sc_in sc_lv 32 signal 77 } 
	{ imag_arr128_1_address1 sc_out sc_lv 5 signal 77 } 
	{ imag_arr128_1_ce1 sc_out sc_logic 1 signal 77 } 
	{ imag_arr128_1_q1 sc_in sc_lv 32 signal 77 } 
	{ real_arr128_2_address0 sc_out sc_lv 5 signal 78 } 
	{ real_arr128_2_ce0 sc_out sc_logic 1 signal 78 } 
	{ real_arr128_2_we0 sc_out sc_logic 1 signal 78 } 
	{ real_arr128_2_d0 sc_out sc_lv 32 signal 78 } 
	{ real_arr128_2_q0 sc_in sc_lv 32 signal 78 } 
	{ real_arr128_2_address1 sc_out sc_lv 5 signal 78 } 
	{ real_arr128_2_ce1 sc_out sc_logic 1 signal 78 } 
	{ real_arr128_2_q1 sc_in sc_lv 32 signal 78 } 
	{ imag_arr128_2_address0 sc_out sc_lv 5 signal 79 } 
	{ imag_arr128_2_ce0 sc_out sc_logic 1 signal 79 } 
	{ imag_arr128_2_we0 sc_out sc_logic 1 signal 79 } 
	{ imag_arr128_2_d0 sc_out sc_lv 32 signal 79 } 
	{ imag_arr128_2_q0 sc_in sc_lv 32 signal 79 } 
	{ imag_arr128_2_address1 sc_out sc_lv 5 signal 79 } 
	{ imag_arr128_2_ce1 sc_out sc_logic 1 signal 79 } 
	{ imag_arr128_2_q1 sc_in sc_lv 32 signal 79 } 
	{ real_arr128_3_address0 sc_out sc_lv 5 signal 80 } 
	{ real_arr128_3_ce0 sc_out sc_logic 1 signal 80 } 
	{ real_arr128_3_we0 sc_out sc_logic 1 signal 80 } 
	{ real_arr128_3_d0 sc_out sc_lv 32 signal 80 } 
	{ real_arr128_3_q0 sc_in sc_lv 32 signal 80 } 
	{ real_arr128_3_address1 sc_out sc_lv 5 signal 80 } 
	{ real_arr128_3_ce1 sc_out sc_logic 1 signal 80 } 
	{ real_arr128_3_q1 sc_in sc_lv 32 signal 80 } 
	{ imag_arr128_3_address0 sc_out sc_lv 5 signal 81 } 
	{ imag_arr128_3_ce0 sc_out sc_logic 1 signal 81 } 
	{ imag_arr128_3_we0 sc_out sc_logic 1 signal 81 } 
	{ imag_arr128_3_d0 sc_out sc_lv 32 signal 81 } 
	{ imag_arr128_3_q0 sc_in sc_lv 32 signal 81 } 
	{ imag_arr128_3_address1 sc_out sc_lv 5 signal 81 } 
	{ imag_arr128_3_ce1 sc_out sc_logic 1 signal 81 } 
	{ imag_arr128_3_q1 sc_in sc_lv 32 signal 81 } 
	{ real_arr64_0_address0 sc_out sc_lv 4 signal 82 } 
	{ real_arr64_0_ce0 sc_out sc_logic 1 signal 82 } 
	{ real_arr64_0_we0 sc_out sc_logic 1 signal 82 } 
	{ real_arr64_0_d0 sc_out sc_lv 32 signal 82 } 
	{ real_arr64_0_q0 sc_in sc_lv 32 signal 82 } 
	{ real_arr64_0_address1 sc_out sc_lv 4 signal 82 } 
	{ real_arr64_0_ce1 sc_out sc_logic 1 signal 82 } 
	{ real_arr64_0_q1 sc_in sc_lv 32 signal 82 } 
	{ imag_arr64_0_address0 sc_out sc_lv 4 signal 83 } 
	{ imag_arr64_0_ce0 sc_out sc_logic 1 signal 83 } 
	{ imag_arr64_0_we0 sc_out sc_logic 1 signal 83 } 
	{ imag_arr64_0_d0 sc_out sc_lv 32 signal 83 } 
	{ imag_arr64_0_q0 sc_in sc_lv 32 signal 83 } 
	{ imag_arr64_0_address1 sc_out sc_lv 4 signal 83 } 
	{ imag_arr64_0_ce1 sc_out sc_logic 1 signal 83 } 
	{ imag_arr64_0_q1 sc_in sc_lv 32 signal 83 } 
	{ real_arr64_1_address0 sc_out sc_lv 4 signal 84 } 
	{ real_arr64_1_ce0 sc_out sc_logic 1 signal 84 } 
	{ real_arr64_1_we0 sc_out sc_logic 1 signal 84 } 
	{ real_arr64_1_d0 sc_out sc_lv 32 signal 84 } 
	{ real_arr64_1_q0 sc_in sc_lv 32 signal 84 } 
	{ real_arr64_1_address1 sc_out sc_lv 4 signal 84 } 
	{ real_arr64_1_ce1 sc_out sc_logic 1 signal 84 } 
	{ real_arr64_1_q1 sc_in sc_lv 32 signal 84 } 
	{ imag_arr64_1_address0 sc_out sc_lv 4 signal 85 } 
	{ imag_arr64_1_ce0 sc_out sc_logic 1 signal 85 } 
	{ imag_arr64_1_we0 sc_out sc_logic 1 signal 85 } 
	{ imag_arr64_1_d0 sc_out sc_lv 32 signal 85 } 
	{ imag_arr64_1_q0 sc_in sc_lv 32 signal 85 } 
	{ imag_arr64_1_address1 sc_out sc_lv 4 signal 85 } 
	{ imag_arr64_1_ce1 sc_out sc_logic 1 signal 85 } 
	{ imag_arr64_1_q1 sc_in sc_lv 32 signal 85 } 
	{ real_arr64_2_address0 sc_out sc_lv 4 signal 86 } 
	{ real_arr64_2_ce0 sc_out sc_logic 1 signal 86 } 
	{ real_arr64_2_we0 sc_out sc_logic 1 signal 86 } 
	{ real_arr64_2_d0 sc_out sc_lv 32 signal 86 } 
	{ real_arr64_2_q0 sc_in sc_lv 32 signal 86 } 
	{ real_arr64_2_address1 sc_out sc_lv 4 signal 86 } 
	{ real_arr64_2_ce1 sc_out sc_logic 1 signal 86 } 
	{ real_arr64_2_q1 sc_in sc_lv 32 signal 86 } 
	{ imag_arr64_2_address0 sc_out sc_lv 4 signal 87 } 
	{ imag_arr64_2_ce0 sc_out sc_logic 1 signal 87 } 
	{ imag_arr64_2_we0 sc_out sc_logic 1 signal 87 } 
	{ imag_arr64_2_d0 sc_out sc_lv 32 signal 87 } 
	{ imag_arr64_2_q0 sc_in sc_lv 32 signal 87 } 
	{ imag_arr64_2_address1 sc_out sc_lv 4 signal 87 } 
	{ imag_arr64_2_ce1 sc_out sc_logic 1 signal 87 } 
	{ imag_arr64_2_q1 sc_in sc_lv 32 signal 87 } 
	{ real_arr64_3_address0 sc_out sc_lv 4 signal 88 } 
	{ real_arr64_3_ce0 sc_out sc_logic 1 signal 88 } 
	{ real_arr64_3_we0 sc_out sc_logic 1 signal 88 } 
	{ real_arr64_3_d0 sc_out sc_lv 32 signal 88 } 
	{ real_arr64_3_q0 sc_in sc_lv 32 signal 88 } 
	{ real_arr64_3_address1 sc_out sc_lv 4 signal 88 } 
	{ real_arr64_3_ce1 sc_out sc_logic 1 signal 88 } 
	{ real_arr64_3_q1 sc_in sc_lv 32 signal 88 } 
	{ imag_arr64_3_address0 sc_out sc_lv 4 signal 89 } 
	{ imag_arr64_3_ce0 sc_out sc_logic 1 signal 89 } 
	{ imag_arr64_3_we0 sc_out sc_logic 1 signal 89 } 
	{ imag_arr64_3_d0 sc_out sc_lv 32 signal 89 } 
	{ imag_arr64_3_q0 sc_in sc_lv 32 signal 89 } 
	{ imag_arr64_3_address1 sc_out sc_lv 4 signal 89 } 
	{ imag_arr64_3_ce1 sc_out sc_logic 1 signal 89 } 
	{ imag_arr64_3_q1 sc_in sc_lv 32 signal 89 } 
	{ real_arr32_0_address0 sc_out sc_lv 3 signal 90 } 
	{ real_arr32_0_ce0 sc_out sc_logic 1 signal 90 } 
	{ real_arr32_0_we0 sc_out sc_logic 1 signal 90 } 
	{ real_arr32_0_d0 sc_out sc_lv 32 signal 90 } 
	{ real_arr32_0_q0 sc_in sc_lv 32 signal 90 } 
	{ imag_arr32_0_address0 sc_out sc_lv 3 signal 91 } 
	{ imag_arr32_0_ce0 sc_out sc_logic 1 signal 91 } 
	{ imag_arr32_0_we0 sc_out sc_logic 1 signal 91 } 
	{ imag_arr32_0_d0 sc_out sc_lv 32 signal 91 } 
	{ imag_arr32_0_q0 sc_in sc_lv 32 signal 91 } 
	{ real_arr32_1_address0 sc_out sc_lv 3 signal 92 } 
	{ real_arr32_1_ce0 sc_out sc_logic 1 signal 92 } 
	{ real_arr32_1_we0 sc_out sc_logic 1 signal 92 } 
	{ real_arr32_1_d0 sc_out sc_lv 32 signal 92 } 
	{ real_arr32_1_q0 sc_in sc_lv 32 signal 92 } 
	{ imag_arr32_1_address0 sc_out sc_lv 3 signal 93 } 
	{ imag_arr32_1_ce0 sc_out sc_logic 1 signal 93 } 
	{ imag_arr32_1_we0 sc_out sc_logic 1 signal 93 } 
	{ imag_arr32_1_d0 sc_out sc_lv 32 signal 93 } 
	{ imag_arr32_1_q0 sc_in sc_lv 32 signal 93 } 
	{ real_arr32_2_address0 sc_out sc_lv 3 signal 94 } 
	{ real_arr32_2_ce0 sc_out sc_logic 1 signal 94 } 
	{ real_arr32_2_we0 sc_out sc_logic 1 signal 94 } 
	{ real_arr32_2_d0 sc_out sc_lv 32 signal 94 } 
	{ real_arr32_2_q0 sc_in sc_lv 32 signal 94 } 
	{ imag_arr32_2_address0 sc_out sc_lv 3 signal 95 } 
	{ imag_arr32_2_ce0 sc_out sc_logic 1 signal 95 } 
	{ imag_arr32_2_we0 sc_out sc_logic 1 signal 95 } 
	{ imag_arr32_2_d0 sc_out sc_lv 32 signal 95 } 
	{ imag_arr32_2_q0 sc_in sc_lv 32 signal 95 } 
	{ real_arr32_3_address0 sc_out sc_lv 3 signal 96 } 
	{ real_arr32_3_ce0 sc_out sc_logic 1 signal 96 } 
	{ real_arr32_3_we0 sc_out sc_logic 1 signal 96 } 
	{ real_arr32_3_d0 sc_out sc_lv 32 signal 96 } 
	{ real_arr32_3_q0 sc_in sc_lv 32 signal 96 } 
	{ imag_arr32_3_address0 sc_out sc_lv 3 signal 97 } 
	{ imag_arr32_3_ce0 sc_out sc_logic 1 signal 97 } 
	{ imag_arr32_3_we0 sc_out sc_logic 1 signal 97 } 
	{ imag_arr32_3_d0 sc_out sc_lv 32 signal 97 } 
	{ imag_arr32_3_q0 sc_in sc_lv 32 signal 97 } 
	{ imag_arr16_1_address0 sc_out sc_lv 2 signal 98 } 
	{ imag_arr16_1_ce0 sc_out sc_logic 1 signal 98 } 
	{ imag_arr16_1_we0 sc_out sc_logic 1 signal 98 } 
	{ imag_arr16_1_d0 sc_out sc_lv 32 signal 98 } 
	{ imag_arr16_1_q0 sc_in sc_lv 32 signal 98 } 
	{ real_arr16_1_address0 sc_out sc_lv 2 signal 99 } 
	{ real_arr16_1_ce0 sc_out sc_logic 1 signal 99 } 
	{ real_arr16_1_we0 sc_out sc_logic 1 signal 99 } 
	{ real_arr16_1_d0 sc_out sc_lv 32 signal 99 } 
	{ real_arr16_1_q0 sc_in sc_lv 32 signal 99 } 
	{ imag_arr16_0_address0 sc_out sc_lv 2 signal 100 } 
	{ imag_arr16_0_ce0 sc_out sc_logic 1 signal 100 } 
	{ imag_arr16_0_we0 sc_out sc_logic 1 signal 100 } 
	{ imag_arr16_0_d0 sc_out sc_lv 32 signal 100 } 
	{ imag_arr16_0_q0 sc_in sc_lv 32 signal 100 } 
	{ real_arr16_0_address0 sc_out sc_lv 2 signal 101 } 
	{ real_arr16_0_ce0 sc_out sc_logic 1 signal 101 } 
	{ real_arr16_0_we0 sc_out sc_logic 1 signal 101 } 
	{ real_arr16_0_d0 sc_out sc_lv 32 signal 101 } 
	{ real_arr16_0_q0 sc_in sc_lv 32 signal 101 } 
	{ real_arr16_2_address0 sc_out sc_lv 2 signal 102 } 
	{ real_arr16_2_ce0 sc_out sc_logic 1 signal 102 } 
	{ real_arr16_2_we0 sc_out sc_logic 1 signal 102 } 
	{ real_arr16_2_d0 sc_out sc_lv 32 signal 102 } 
	{ real_arr16_2_q0 sc_in sc_lv 32 signal 102 } 
	{ imag_arr16_2_address0 sc_out sc_lv 2 signal 103 } 
	{ imag_arr16_2_ce0 sc_out sc_logic 1 signal 103 } 
	{ imag_arr16_2_we0 sc_out sc_logic 1 signal 103 } 
	{ imag_arr16_2_d0 sc_out sc_lv 32 signal 103 } 
	{ imag_arr16_2_q0 sc_in sc_lv 32 signal 103 } 
	{ real_arr16_3_address0 sc_out sc_lv 2 signal 104 } 
	{ real_arr16_3_ce0 sc_out sc_logic 1 signal 104 } 
	{ real_arr16_3_we0 sc_out sc_logic 1 signal 104 } 
	{ real_arr16_3_d0 sc_out sc_lv 32 signal 104 } 
	{ real_arr16_3_q0 sc_in sc_lv 32 signal 104 } 
	{ imag_arr16_3_address0 sc_out sc_lv 2 signal 105 } 
	{ imag_arr16_3_ce0 sc_out sc_logic 1 signal 105 } 
	{ imag_arr16_3_we0 sc_out sc_logic 1 signal 105 } 
	{ imag_arr16_3_d0 sc_out sc_lv 32 signal 105 } 
	{ imag_arr16_3_q0 sc_in sc_lv 32 signal 105 } 
	{ imag_arr8_1_i sc_in sc_lv 32 signal 106 } 
	{ imag_arr8_1_o sc_out sc_lv 32 signal 106 } 
	{ imag_arr8_1_o_ap_vld sc_out sc_logic 1 outvld 106 } 
	{ real_arr8_1_i sc_in sc_lv 32 signal 107 } 
	{ real_arr8_1_o sc_out sc_lv 32 signal 107 } 
	{ real_arr8_1_o_ap_vld sc_out sc_logic 1 outvld 107 } 
	{ imag_arr8_0_i sc_in sc_lv 32 signal 108 } 
	{ imag_arr8_0_o sc_out sc_lv 32 signal 108 } 
	{ imag_arr8_0_o_ap_vld sc_out sc_logic 1 outvld 108 } 
	{ real_arr8_0_i sc_in sc_lv 32 signal 109 } 
	{ real_arr8_0_o sc_out sc_lv 32 signal 109 } 
	{ real_arr8_0_o_ap_vld sc_out sc_logic 1 outvld 109 } 
	{ real_arr8_2_i sc_in sc_lv 32 signal 110 } 
	{ real_arr8_2_o sc_out sc_lv 32 signal 110 } 
	{ real_arr8_2_o_ap_vld sc_out sc_logic 1 outvld 110 } 
	{ real_arr8_4_i sc_in sc_lv 32 signal 111 } 
	{ real_arr8_4_o sc_out sc_lv 32 signal 111 } 
	{ real_arr8_4_o_ap_vld sc_out sc_logic 1 outvld 111 } 
	{ real_arr8_6_i sc_in sc_lv 32 signal 112 } 
	{ real_arr8_6_o sc_out sc_lv 32 signal 112 } 
	{ real_arr8_6_o_ap_vld sc_out sc_logic 1 outvld 112 } 
	{ imag_arr8_2_i sc_in sc_lv 32 signal 113 } 
	{ imag_arr8_2_o sc_out sc_lv 32 signal 113 } 
	{ imag_arr8_2_o_ap_vld sc_out sc_logic 1 outvld 113 } 
	{ imag_arr8_4_i sc_in sc_lv 32 signal 114 } 
	{ imag_arr8_4_o sc_out sc_lv 32 signal 114 } 
	{ imag_arr8_4_o_ap_vld sc_out sc_logic 1 outvld 114 } 
	{ imag_arr8_6_i sc_in sc_lv 32 signal 115 } 
	{ imag_arr8_6_o sc_out sc_lv 32 signal 115 } 
	{ imag_arr8_6_o_ap_vld sc_out sc_logic 1 outvld 115 } 
	{ real_arr8_3_i sc_in sc_lv 32 signal 116 } 
	{ real_arr8_3_o sc_out sc_lv 32 signal 116 } 
	{ real_arr8_3_o_ap_vld sc_out sc_logic 1 outvld 116 } 
	{ real_arr8_5_i sc_in sc_lv 32 signal 117 } 
	{ real_arr8_5_o sc_out sc_lv 32 signal 117 } 
	{ real_arr8_5_o_ap_vld sc_out sc_logic 1 outvld 117 } 
	{ real_arr8_7_i sc_in sc_lv 32 signal 118 } 
	{ real_arr8_7_o sc_out sc_lv 32 signal 118 } 
	{ real_arr8_7_o_ap_vld sc_out sc_logic 1 outvld 118 } 
	{ imag_arr8_3_i sc_in sc_lv 32 signal 119 } 
	{ imag_arr8_3_o sc_out sc_lv 32 signal 119 } 
	{ imag_arr8_3_o_ap_vld sc_out sc_logic 1 outvld 119 } 
	{ imag_arr8_5_i sc_in sc_lv 32 signal 120 } 
	{ imag_arr8_5_o sc_out sc_lv 32 signal 120 } 
	{ imag_arr8_5_o_ap_vld sc_out sc_logic 1 outvld 120 } 
	{ imag_arr8_7_i sc_in sc_lv 32 signal 121 } 
	{ imag_arr8_7_o sc_out sc_lv 32 signal 121 } 
	{ imag_arr8_7_o_ap_vld sc_out sc_logic 1 outvld 121 } 
	{ imag_arr4_0_i sc_in sc_lv 32 signal 122 } 
	{ imag_arr4_0_o sc_out sc_lv 32 signal 122 } 
	{ imag_arr4_0_o_ap_vld sc_out sc_logic 1 outvld 122 } 
	{ real_arr4_0_i sc_in sc_lv 32 signal 123 } 
	{ real_arr4_0_o sc_out sc_lv 32 signal 123 } 
	{ real_arr4_0_o_ap_vld sc_out sc_logic 1 outvld 123 } 
	{ real_arr4_1_i sc_in sc_lv 32 signal 124 } 
	{ real_arr4_1_o sc_out sc_lv 32 signal 124 } 
	{ real_arr4_1_o_ap_vld sc_out sc_logic 1 outvld 124 } 
	{ real_arr4_2_i sc_in sc_lv 32 signal 125 } 
	{ real_arr4_2_o sc_out sc_lv 32 signal 125 } 
	{ real_arr4_2_o_ap_vld sc_out sc_logic 1 outvld 125 } 
	{ real_arr4_3_i sc_in sc_lv 32 signal 126 } 
	{ real_arr4_3_o sc_out sc_lv 32 signal 126 } 
	{ real_arr4_3_o_ap_vld sc_out sc_logic 1 outvld 126 } 
	{ imag_arr4_1_i sc_in sc_lv 32 signal 127 } 
	{ imag_arr4_1_o sc_out sc_lv 32 signal 127 } 
	{ imag_arr4_1_o_ap_vld sc_out sc_logic 1 outvld 127 } 
	{ imag_arr4_2_i sc_in sc_lv 32 signal 128 } 
	{ imag_arr4_2_o sc_out sc_lv 32 signal 128 } 
	{ imag_arr4_2_o_ap_vld sc_out sc_logic 1 outvld 128 } 
	{ imag_arr4_3_i sc_in sc_lv 32 signal 129 } 
	{ imag_arr4_3_o sc_out sc_lv 32 signal 129 } 
	{ imag_arr4_3_o_ap_vld sc_out sc_logic 1 outvld 129 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "real_i_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_0", "role": "address0" }} , 
 	{ "name": "real_i_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "ce0" }} , 
 	{ "name": "real_i_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "q0" }} , 
 	{ "name": "real_i_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_1", "role": "address0" }} , 
 	{ "name": "real_i_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "ce0" }} , 
 	{ "name": "real_i_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "q0" }} , 
 	{ "name": "real_i_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_2", "role": "address0" }} , 
 	{ "name": "real_i_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_2", "role": "ce0" }} , 
 	{ "name": "real_i_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_2", "role": "q0" }} , 
 	{ "name": "real_i_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_3", "role": "address0" }} , 
 	{ "name": "real_i_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_3", "role": "ce0" }} , 
 	{ "name": "real_i_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_3", "role": "q0" }} , 
 	{ "name": "real_i_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_4", "role": "address0" }} , 
 	{ "name": "real_i_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_4", "role": "ce0" }} , 
 	{ "name": "real_i_4_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_4", "role": "q0" }} , 
 	{ "name": "real_i_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_5", "role": "address0" }} , 
 	{ "name": "real_i_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_5", "role": "ce0" }} , 
 	{ "name": "real_i_5_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_5", "role": "q0" }} , 
 	{ "name": "real_i_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_6", "role": "address0" }} , 
 	{ "name": "real_i_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_6", "role": "ce0" }} , 
 	{ "name": "real_i_6_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_6", "role": "q0" }} , 
 	{ "name": "real_i_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_7", "role": "address0" }} , 
 	{ "name": "real_i_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_7", "role": "ce0" }} , 
 	{ "name": "real_i_7_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_7", "role": "q0" }} , 
 	{ "name": "real_i_8_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_8", "role": "address0" }} , 
 	{ "name": "real_i_8_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_8", "role": "ce0" }} , 
 	{ "name": "real_i_8_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_8", "role": "q0" }} , 
 	{ "name": "real_i_9_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_9", "role": "address0" }} , 
 	{ "name": "real_i_9_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_9", "role": "ce0" }} , 
 	{ "name": "real_i_9_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_9", "role": "q0" }} , 
 	{ "name": "real_i_10_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_10", "role": "address0" }} , 
 	{ "name": "real_i_10_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_10", "role": "ce0" }} , 
 	{ "name": "real_i_10_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_10", "role": "q0" }} , 
 	{ "name": "real_i_11_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_11", "role": "address0" }} , 
 	{ "name": "real_i_11_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_11", "role": "ce0" }} , 
 	{ "name": "real_i_11_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_11", "role": "q0" }} , 
 	{ "name": "real_i_12_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_12", "role": "address0" }} , 
 	{ "name": "real_i_12_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_12", "role": "ce0" }} , 
 	{ "name": "real_i_12_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_12", "role": "q0" }} , 
 	{ "name": "real_i_13_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_13", "role": "address0" }} , 
 	{ "name": "real_i_13_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_13", "role": "ce0" }} , 
 	{ "name": "real_i_13_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_13", "role": "q0" }} , 
 	{ "name": "real_i_14_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_14", "role": "address0" }} , 
 	{ "name": "real_i_14_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_14", "role": "ce0" }} , 
 	{ "name": "real_i_14_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_14", "role": "q0" }} , 
 	{ "name": "real_i_15_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_15", "role": "address0" }} , 
 	{ "name": "real_i_15_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_15", "role": "ce0" }} , 
 	{ "name": "real_i_15_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_15", "role": "q0" }} , 
 	{ "name": "real_i_16_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_16", "role": "address0" }} , 
 	{ "name": "real_i_16_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_16", "role": "ce0" }} , 
 	{ "name": "real_i_16_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_16", "role": "q0" }} , 
 	{ "name": "real_i_17_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_17", "role": "address0" }} , 
 	{ "name": "real_i_17_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_17", "role": "ce0" }} , 
 	{ "name": "real_i_17_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_17", "role": "q0" }} , 
 	{ "name": "real_i_18_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_18", "role": "address0" }} , 
 	{ "name": "real_i_18_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_18", "role": "ce0" }} , 
 	{ "name": "real_i_18_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_18", "role": "q0" }} , 
 	{ "name": "real_i_19_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_19", "role": "address0" }} , 
 	{ "name": "real_i_19_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_19", "role": "ce0" }} , 
 	{ "name": "real_i_19_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_19", "role": "q0" }} , 
 	{ "name": "real_i_20_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_20", "role": "address0" }} , 
 	{ "name": "real_i_20_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_20", "role": "ce0" }} , 
 	{ "name": "real_i_20_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_20", "role": "q0" }} , 
 	{ "name": "real_i_21_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_21", "role": "address0" }} , 
 	{ "name": "real_i_21_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_21", "role": "ce0" }} , 
 	{ "name": "real_i_21_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_21", "role": "q0" }} , 
 	{ "name": "real_i_22_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_22", "role": "address0" }} , 
 	{ "name": "real_i_22_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_22", "role": "ce0" }} , 
 	{ "name": "real_i_22_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_22", "role": "q0" }} , 
 	{ "name": "real_i_23_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_23", "role": "address0" }} , 
 	{ "name": "real_i_23_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_23", "role": "ce0" }} , 
 	{ "name": "real_i_23_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_23", "role": "q0" }} , 
 	{ "name": "real_i_24_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_24", "role": "address0" }} , 
 	{ "name": "real_i_24_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_24", "role": "ce0" }} , 
 	{ "name": "real_i_24_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_24", "role": "q0" }} , 
 	{ "name": "real_i_25_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_25", "role": "address0" }} , 
 	{ "name": "real_i_25_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_25", "role": "ce0" }} , 
 	{ "name": "real_i_25_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_25", "role": "q0" }} , 
 	{ "name": "real_i_26_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_26", "role": "address0" }} , 
 	{ "name": "real_i_26_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_26", "role": "ce0" }} , 
 	{ "name": "real_i_26_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_26", "role": "q0" }} , 
 	{ "name": "real_i_27_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_27", "role": "address0" }} , 
 	{ "name": "real_i_27_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_27", "role": "ce0" }} , 
 	{ "name": "real_i_27_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_27", "role": "q0" }} , 
 	{ "name": "real_i_28_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_28", "role": "address0" }} , 
 	{ "name": "real_i_28_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_28", "role": "ce0" }} , 
 	{ "name": "real_i_28_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_28", "role": "q0" }} , 
 	{ "name": "real_i_29_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_29", "role": "address0" }} , 
 	{ "name": "real_i_29_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_29", "role": "ce0" }} , 
 	{ "name": "real_i_29_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_29", "role": "q0" }} , 
 	{ "name": "real_i_30_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_30", "role": "address0" }} , 
 	{ "name": "real_i_30_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_30", "role": "ce0" }} , 
 	{ "name": "real_i_30_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_30", "role": "q0" }} , 
 	{ "name": "real_i_31_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_i_31", "role": "address0" }} , 
 	{ "name": "real_i_31_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_31", "role": "ce0" }} , 
 	{ "name": "real_i_31_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_31", "role": "q0" }} , 
 	{ "name": "imag_i_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_0", "role": "address0" }} , 
 	{ "name": "imag_i_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_0", "role": "ce0" }} , 
 	{ "name": "imag_i_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_0", "role": "q0" }} , 
 	{ "name": "imag_i_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_1", "role": "address0" }} , 
 	{ "name": "imag_i_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_1", "role": "ce0" }} , 
 	{ "name": "imag_i_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_1", "role": "q0" }} , 
 	{ "name": "imag_i_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_2", "role": "address0" }} , 
 	{ "name": "imag_i_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_2", "role": "ce0" }} , 
 	{ "name": "imag_i_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_2", "role": "q0" }} , 
 	{ "name": "imag_i_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_3", "role": "address0" }} , 
 	{ "name": "imag_i_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_3", "role": "ce0" }} , 
 	{ "name": "imag_i_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_3", "role": "q0" }} , 
 	{ "name": "imag_i_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_4", "role": "address0" }} , 
 	{ "name": "imag_i_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_4", "role": "ce0" }} , 
 	{ "name": "imag_i_4_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_4", "role": "q0" }} , 
 	{ "name": "imag_i_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_5", "role": "address0" }} , 
 	{ "name": "imag_i_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_5", "role": "ce0" }} , 
 	{ "name": "imag_i_5_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_5", "role": "q0" }} , 
 	{ "name": "imag_i_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_6", "role": "address0" }} , 
 	{ "name": "imag_i_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_6", "role": "ce0" }} , 
 	{ "name": "imag_i_6_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_6", "role": "q0" }} , 
 	{ "name": "imag_i_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_7", "role": "address0" }} , 
 	{ "name": "imag_i_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_7", "role": "ce0" }} , 
 	{ "name": "imag_i_7_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_7", "role": "q0" }} , 
 	{ "name": "imag_i_8_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_8", "role": "address0" }} , 
 	{ "name": "imag_i_8_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_8", "role": "ce0" }} , 
 	{ "name": "imag_i_8_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_8", "role": "q0" }} , 
 	{ "name": "imag_i_9_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_9", "role": "address0" }} , 
 	{ "name": "imag_i_9_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_9", "role": "ce0" }} , 
 	{ "name": "imag_i_9_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_9", "role": "q0" }} , 
 	{ "name": "imag_i_10_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_10", "role": "address0" }} , 
 	{ "name": "imag_i_10_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_10", "role": "ce0" }} , 
 	{ "name": "imag_i_10_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_10", "role": "q0" }} , 
 	{ "name": "imag_i_11_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_11", "role": "address0" }} , 
 	{ "name": "imag_i_11_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_11", "role": "ce0" }} , 
 	{ "name": "imag_i_11_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_11", "role": "q0" }} , 
 	{ "name": "imag_i_12_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_12", "role": "address0" }} , 
 	{ "name": "imag_i_12_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_12", "role": "ce0" }} , 
 	{ "name": "imag_i_12_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_12", "role": "q0" }} , 
 	{ "name": "imag_i_13_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_13", "role": "address0" }} , 
 	{ "name": "imag_i_13_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_13", "role": "ce0" }} , 
 	{ "name": "imag_i_13_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_13", "role": "q0" }} , 
 	{ "name": "imag_i_14_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_14", "role": "address0" }} , 
 	{ "name": "imag_i_14_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_14", "role": "ce0" }} , 
 	{ "name": "imag_i_14_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_14", "role": "q0" }} , 
 	{ "name": "imag_i_15_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_15", "role": "address0" }} , 
 	{ "name": "imag_i_15_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_15", "role": "ce0" }} , 
 	{ "name": "imag_i_15_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_15", "role": "q0" }} , 
 	{ "name": "imag_i_16_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_16", "role": "address0" }} , 
 	{ "name": "imag_i_16_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_16", "role": "ce0" }} , 
 	{ "name": "imag_i_16_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_16", "role": "q0" }} , 
 	{ "name": "imag_i_17_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_17", "role": "address0" }} , 
 	{ "name": "imag_i_17_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_17", "role": "ce0" }} , 
 	{ "name": "imag_i_17_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_17", "role": "q0" }} , 
 	{ "name": "imag_i_18_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_18", "role": "address0" }} , 
 	{ "name": "imag_i_18_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_18", "role": "ce0" }} , 
 	{ "name": "imag_i_18_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_18", "role": "q0" }} , 
 	{ "name": "imag_i_19_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_19", "role": "address0" }} , 
 	{ "name": "imag_i_19_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_19", "role": "ce0" }} , 
 	{ "name": "imag_i_19_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_19", "role": "q0" }} , 
 	{ "name": "imag_i_20_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_20", "role": "address0" }} , 
 	{ "name": "imag_i_20_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_20", "role": "ce0" }} , 
 	{ "name": "imag_i_20_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_20", "role": "q0" }} , 
 	{ "name": "imag_i_21_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_21", "role": "address0" }} , 
 	{ "name": "imag_i_21_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_21", "role": "ce0" }} , 
 	{ "name": "imag_i_21_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_21", "role": "q0" }} , 
 	{ "name": "imag_i_22_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_22", "role": "address0" }} , 
 	{ "name": "imag_i_22_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_22", "role": "ce0" }} , 
 	{ "name": "imag_i_22_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_22", "role": "q0" }} , 
 	{ "name": "imag_i_23_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_23", "role": "address0" }} , 
 	{ "name": "imag_i_23_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_23", "role": "ce0" }} , 
 	{ "name": "imag_i_23_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_23", "role": "q0" }} , 
 	{ "name": "imag_i_24_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_24", "role": "address0" }} , 
 	{ "name": "imag_i_24_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_24", "role": "ce0" }} , 
 	{ "name": "imag_i_24_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_24", "role": "q0" }} , 
 	{ "name": "imag_i_25_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_25", "role": "address0" }} , 
 	{ "name": "imag_i_25_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_25", "role": "ce0" }} , 
 	{ "name": "imag_i_25_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_25", "role": "q0" }} , 
 	{ "name": "imag_i_26_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_26", "role": "address0" }} , 
 	{ "name": "imag_i_26_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_26", "role": "ce0" }} , 
 	{ "name": "imag_i_26_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_26", "role": "q0" }} , 
 	{ "name": "imag_i_27_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_27", "role": "address0" }} , 
 	{ "name": "imag_i_27_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_27", "role": "ce0" }} , 
 	{ "name": "imag_i_27_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_27", "role": "q0" }} , 
 	{ "name": "imag_i_28_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_28", "role": "address0" }} , 
 	{ "name": "imag_i_28_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_28", "role": "ce0" }} , 
 	{ "name": "imag_i_28_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_28", "role": "q0" }} , 
 	{ "name": "imag_i_29_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_29", "role": "address0" }} , 
 	{ "name": "imag_i_29_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_29", "role": "ce0" }} , 
 	{ "name": "imag_i_29_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_29", "role": "q0" }} , 
 	{ "name": "imag_i_30_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_30", "role": "address0" }} , 
 	{ "name": "imag_i_30_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_30", "role": "ce0" }} , 
 	{ "name": "imag_i_30_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_30", "role": "q0" }} , 
 	{ "name": "imag_i_31_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_i_31", "role": "address0" }} , 
 	{ "name": "imag_i_31_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_31", "role": "ce0" }} , 
 	{ "name": "imag_i_31_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_31", "role": "q0" }} , 
 	{ "name": "real_o_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_o", "role": "address0" }} , 
 	{ "name": "real_o_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o", "role": "ce0" }} , 
 	{ "name": "real_o_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o", "role": "we0" }} , 
 	{ "name": "real_o_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o", "role": "d0" }} , 
 	{ "name": "imag_o_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "imag_o", "role": "address0" }} , 
 	{ "name": "imag_o_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o", "role": "ce0" }} , 
 	{ "name": "imag_o_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o", "role": "we0" }} , 
 	{ "name": "imag_o_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o", "role": "d0" }} , 
 	{ "name": "real_arr256_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "address0" }} , 
 	{ "name": "real_arr256_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "ce0" }} , 
 	{ "name": "real_arr256_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "we0" }} , 
 	{ "name": "real_arr256_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "d0" }} , 
 	{ "name": "real_arr256_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "q0" }} , 
 	{ "name": "real_arr256_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "address1" }} , 
 	{ "name": "real_arr256_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "ce1" }} , 
 	{ "name": "real_arr256_0_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "we1" }} , 
 	{ "name": "real_arr256_0_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "d1" }} , 
 	{ "name": "real_arr256_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "q1" }} , 
 	{ "name": "imag_arr256_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "address0" }} , 
 	{ "name": "imag_arr256_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "ce0" }} , 
 	{ "name": "imag_arr256_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "we0" }} , 
 	{ "name": "imag_arr256_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "d0" }} , 
 	{ "name": "imag_arr256_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "q0" }} , 
 	{ "name": "imag_arr256_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "address1" }} , 
 	{ "name": "imag_arr256_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "ce1" }} , 
 	{ "name": "imag_arr256_0_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "we1" }} , 
 	{ "name": "imag_arr256_0_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "d1" }} , 
 	{ "name": "imag_arr256_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "q1" }} , 
 	{ "name": "real_arr256_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "address0" }} , 
 	{ "name": "real_arr256_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "ce0" }} , 
 	{ "name": "real_arr256_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "we0" }} , 
 	{ "name": "real_arr256_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "d0" }} , 
 	{ "name": "real_arr256_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "q0" }} , 
 	{ "name": "real_arr256_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "address1" }} , 
 	{ "name": "real_arr256_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "ce1" }} , 
 	{ "name": "real_arr256_1_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "we1" }} , 
 	{ "name": "real_arr256_1_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "d1" }} , 
 	{ "name": "real_arr256_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "q1" }} , 
 	{ "name": "imag_arr256_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "address0" }} , 
 	{ "name": "imag_arr256_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "ce0" }} , 
 	{ "name": "imag_arr256_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "we0" }} , 
 	{ "name": "imag_arr256_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "d0" }} , 
 	{ "name": "imag_arr256_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "q0" }} , 
 	{ "name": "imag_arr256_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "address1" }} , 
 	{ "name": "imag_arr256_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "ce1" }} , 
 	{ "name": "imag_arr256_1_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "we1" }} , 
 	{ "name": "imag_arr256_1_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "d1" }} , 
 	{ "name": "imag_arr256_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "q1" }} , 
 	{ "name": "real_arr256_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "address0" }} , 
 	{ "name": "real_arr256_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "ce0" }} , 
 	{ "name": "real_arr256_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "we0" }} , 
 	{ "name": "real_arr256_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "d0" }} , 
 	{ "name": "real_arr256_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "q0" }} , 
 	{ "name": "real_arr256_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "address1" }} , 
 	{ "name": "real_arr256_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "ce1" }} , 
 	{ "name": "real_arr256_2_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "we1" }} , 
 	{ "name": "real_arr256_2_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "d1" }} , 
 	{ "name": "real_arr256_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "q1" }} , 
 	{ "name": "imag_arr256_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "address0" }} , 
 	{ "name": "imag_arr256_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "ce0" }} , 
 	{ "name": "imag_arr256_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "we0" }} , 
 	{ "name": "imag_arr256_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "d0" }} , 
 	{ "name": "imag_arr256_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "q0" }} , 
 	{ "name": "imag_arr256_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "address1" }} , 
 	{ "name": "imag_arr256_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "ce1" }} , 
 	{ "name": "imag_arr256_2_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "we1" }} , 
 	{ "name": "imag_arr256_2_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "d1" }} , 
 	{ "name": "imag_arr256_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "q1" }} , 
 	{ "name": "real_arr256_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "address0" }} , 
 	{ "name": "real_arr256_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "ce0" }} , 
 	{ "name": "real_arr256_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "we0" }} , 
 	{ "name": "real_arr256_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "d0" }} , 
 	{ "name": "real_arr256_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "q0" }} , 
 	{ "name": "real_arr256_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "address1" }} , 
 	{ "name": "real_arr256_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "ce1" }} , 
 	{ "name": "real_arr256_3_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "we1" }} , 
 	{ "name": "real_arr256_3_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "d1" }} , 
 	{ "name": "real_arr256_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "q1" }} , 
 	{ "name": "imag_arr256_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "address0" }} , 
 	{ "name": "imag_arr256_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "ce0" }} , 
 	{ "name": "imag_arr256_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "we0" }} , 
 	{ "name": "imag_arr256_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "d0" }} , 
 	{ "name": "imag_arr256_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "q0" }} , 
 	{ "name": "imag_arr256_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "address1" }} , 
 	{ "name": "imag_arr256_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "ce1" }} , 
 	{ "name": "imag_arr256_3_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "we1" }} , 
 	{ "name": "imag_arr256_3_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "d1" }} , 
 	{ "name": "imag_arr256_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "q1" }} , 
 	{ "name": "real_arr128_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "address0" }} , 
 	{ "name": "real_arr128_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "ce0" }} , 
 	{ "name": "real_arr128_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "we0" }} , 
 	{ "name": "real_arr128_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "d0" }} , 
 	{ "name": "real_arr128_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "q0" }} , 
 	{ "name": "real_arr128_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "address1" }} , 
 	{ "name": "real_arr128_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "ce1" }} , 
 	{ "name": "real_arr128_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "q1" }} , 
 	{ "name": "imag_arr128_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "address0" }} , 
 	{ "name": "imag_arr128_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "ce0" }} , 
 	{ "name": "imag_arr128_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "we0" }} , 
 	{ "name": "imag_arr128_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "d0" }} , 
 	{ "name": "imag_arr128_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "q0" }} , 
 	{ "name": "imag_arr128_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "address1" }} , 
 	{ "name": "imag_arr128_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "ce1" }} , 
 	{ "name": "imag_arr128_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "q1" }} , 
 	{ "name": "real_arr128_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "address0" }} , 
 	{ "name": "real_arr128_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "ce0" }} , 
 	{ "name": "real_arr128_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "we0" }} , 
 	{ "name": "real_arr128_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "d0" }} , 
 	{ "name": "real_arr128_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "q0" }} , 
 	{ "name": "real_arr128_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "address1" }} , 
 	{ "name": "real_arr128_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "ce1" }} , 
 	{ "name": "real_arr128_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "q1" }} , 
 	{ "name": "imag_arr128_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "address0" }} , 
 	{ "name": "imag_arr128_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "ce0" }} , 
 	{ "name": "imag_arr128_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "we0" }} , 
 	{ "name": "imag_arr128_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "d0" }} , 
 	{ "name": "imag_arr128_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "q0" }} , 
 	{ "name": "imag_arr128_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "address1" }} , 
 	{ "name": "imag_arr128_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "ce1" }} , 
 	{ "name": "imag_arr128_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "q1" }} , 
 	{ "name": "real_arr128_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "address0" }} , 
 	{ "name": "real_arr128_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "ce0" }} , 
 	{ "name": "real_arr128_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "we0" }} , 
 	{ "name": "real_arr128_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "d0" }} , 
 	{ "name": "real_arr128_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "q0" }} , 
 	{ "name": "real_arr128_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "address1" }} , 
 	{ "name": "real_arr128_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "ce1" }} , 
 	{ "name": "real_arr128_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "q1" }} , 
 	{ "name": "imag_arr128_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "address0" }} , 
 	{ "name": "imag_arr128_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "ce0" }} , 
 	{ "name": "imag_arr128_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "we0" }} , 
 	{ "name": "imag_arr128_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "d0" }} , 
 	{ "name": "imag_arr128_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "q0" }} , 
 	{ "name": "imag_arr128_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "address1" }} , 
 	{ "name": "imag_arr128_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "ce1" }} , 
 	{ "name": "imag_arr128_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "q1" }} , 
 	{ "name": "real_arr128_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "address0" }} , 
 	{ "name": "real_arr128_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "ce0" }} , 
 	{ "name": "real_arr128_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "we0" }} , 
 	{ "name": "real_arr128_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "d0" }} , 
 	{ "name": "real_arr128_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "q0" }} , 
 	{ "name": "real_arr128_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "address1" }} , 
 	{ "name": "real_arr128_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "ce1" }} , 
 	{ "name": "real_arr128_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "q1" }} , 
 	{ "name": "imag_arr128_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "address0" }} , 
 	{ "name": "imag_arr128_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "ce0" }} , 
 	{ "name": "imag_arr128_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "we0" }} , 
 	{ "name": "imag_arr128_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "d0" }} , 
 	{ "name": "imag_arr128_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "q0" }} , 
 	{ "name": "imag_arr128_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "address1" }} , 
 	{ "name": "imag_arr128_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "ce1" }} , 
 	{ "name": "imag_arr128_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "q1" }} , 
 	{ "name": "real_arr64_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_arr64_0", "role": "address0" }} , 
 	{ "name": "real_arr64_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_0", "role": "ce0" }} , 
 	{ "name": "real_arr64_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_0", "role": "we0" }} , 
 	{ "name": "real_arr64_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_0", "role": "d0" }} , 
 	{ "name": "real_arr64_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_0", "role": "q0" }} , 
 	{ "name": "real_arr64_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_arr64_0", "role": "address1" }} , 
 	{ "name": "real_arr64_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_0", "role": "ce1" }} , 
 	{ "name": "real_arr64_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_0", "role": "q1" }} , 
 	{ "name": "imag_arr64_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_arr64_0", "role": "address0" }} , 
 	{ "name": "imag_arr64_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_0", "role": "ce0" }} , 
 	{ "name": "imag_arr64_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_0", "role": "we0" }} , 
 	{ "name": "imag_arr64_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_0", "role": "d0" }} , 
 	{ "name": "imag_arr64_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_0", "role": "q0" }} , 
 	{ "name": "imag_arr64_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_arr64_0", "role": "address1" }} , 
 	{ "name": "imag_arr64_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_0", "role": "ce1" }} , 
 	{ "name": "imag_arr64_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_0", "role": "q1" }} , 
 	{ "name": "real_arr64_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_arr64_1", "role": "address0" }} , 
 	{ "name": "real_arr64_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_1", "role": "ce0" }} , 
 	{ "name": "real_arr64_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_1", "role": "we0" }} , 
 	{ "name": "real_arr64_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_1", "role": "d0" }} , 
 	{ "name": "real_arr64_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_1", "role": "q0" }} , 
 	{ "name": "real_arr64_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_arr64_1", "role": "address1" }} , 
 	{ "name": "real_arr64_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_1", "role": "ce1" }} , 
 	{ "name": "real_arr64_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_1", "role": "q1" }} , 
 	{ "name": "imag_arr64_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_arr64_1", "role": "address0" }} , 
 	{ "name": "imag_arr64_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_1", "role": "ce0" }} , 
 	{ "name": "imag_arr64_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_1", "role": "we0" }} , 
 	{ "name": "imag_arr64_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_1", "role": "d0" }} , 
 	{ "name": "imag_arr64_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_1", "role": "q0" }} , 
 	{ "name": "imag_arr64_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_arr64_1", "role": "address1" }} , 
 	{ "name": "imag_arr64_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_1", "role": "ce1" }} , 
 	{ "name": "imag_arr64_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_1", "role": "q1" }} , 
 	{ "name": "real_arr64_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_arr64_2", "role": "address0" }} , 
 	{ "name": "real_arr64_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_2", "role": "ce0" }} , 
 	{ "name": "real_arr64_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_2", "role": "we0" }} , 
 	{ "name": "real_arr64_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_2", "role": "d0" }} , 
 	{ "name": "real_arr64_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_2", "role": "q0" }} , 
 	{ "name": "real_arr64_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_arr64_2", "role": "address1" }} , 
 	{ "name": "real_arr64_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_2", "role": "ce1" }} , 
 	{ "name": "real_arr64_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_2", "role": "q1" }} , 
 	{ "name": "imag_arr64_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_arr64_2", "role": "address0" }} , 
 	{ "name": "imag_arr64_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_2", "role": "ce0" }} , 
 	{ "name": "imag_arr64_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_2", "role": "we0" }} , 
 	{ "name": "imag_arr64_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_2", "role": "d0" }} , 
 	{ "name": "imag_arr64_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_2", "role": "q0" }} , 
 	{ "name": "imag_arr64_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_arr64_2", "role": "address1" }} , 
 	{ "name": "imag_arr64_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_2", "role": "ce1" }} , 
 	{ "name": "imag_arr64_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_2", "role": "q1" }} , 
 	{ "name": "real_arr64_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_arr64_3", "role": "address0" }} , 
 	{ "name": "real_arr64_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_3", "role": "ce0" }} , 
 	{ "name": "real_arr64_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_3", "role": "we0" }} , 
 	{ "name": "real_arr64_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_3", "role": "d0" }} , 
 	{ "name": "real_arr64_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_3", "role": "q0" }} , 
 	{ "name": "real_arr64_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_arr64_3", "role": "address1" }} , 
 	{ "name": "real_arr64_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_3", "role": "ce1" }} , 
 	{ "name": "real_arr64_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_3", "role": "q1" }} , 
 	{ "name": "imag_arr64_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_arr64_3", "role": "address0" }} , 
 	{ "name": "imag_arr64_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_3", "role": "ce0" }} , 
 	{ "name": "imag_arr64_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_3", "role": "we0" }} , 
 	{ "name": "imag_arr64_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_3", "role": "d0" }} , 
 	{ "name": "imag_arr64_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_3", "role": "q0" }} , 
 	{ "name": "imag_arr64_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_arr64_3", "role": "address1" }} , 
 	{ "name": "imag_arr64_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_3", "role": "ce1" }} , 
 	{ "name": "imag_arr64_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_3", "role": "q1" }} , 
 	{ "name": "real_arr32_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_arr32_0", "role": "address0" }} , 
 	{ "name": "real_arr32_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_0", "role": "ce0" }} , 
 	{ "name": "real_arr32_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_0", "role": "we0" }} , 
 	{ "name": "real_arr32_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_0", "role": "d0" }} , 
 	{ "name": "real_arr32_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_0", "role": "q0" }} , 
 	{ "name": "imag_arr32_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_arr32_0", "role": "address0" }} , 
 	{ "name": "imag_arr32_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_0", "role": "ce0" }} , 
 	{ "name": "imag_arr32_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_0", "role": "we0" }} , 
 	{ "name": "imag_arr32_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_0", "role": "d0" }} , 
 	{ "name": "imag_arr32_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_0", "role": "q0" }} , 
 	{ "name": "real_arr32_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_arr32_1", "role": "address0" }} , 
 	{ "name": "real_arr32_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_1", "role": "ce0" }} , 
 	{ "name": "real_arr32_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_1", "role": "we0" }} , 
 	{ "name": "real_arr32_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_1", "role": "d0" }} , 
 	{ "name": "real_arr32_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_1", "role": "q0" }} , 
 	{ "name": "imag_arr32_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_arr32_1", "role": "address0" }} , 
 	{ "name": "imag_arr32_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_1", "role": "ce0" }} , 
 	{ "name": "imag_arr32_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_1", "role": "we0" }} , 
 	{ "name": "imag_arr32_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_1", "role": "d0" }} , 
 	{ "name": "imag_arr32_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_1", "role": "q0" }} , 
 	{ "name": "real_arr32_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_arr32_2", "role": "address0" }} , 
 	{ "name": "real_arr32_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_2", "role": "ce0" }} , 
 	{ "name": "real_arr32_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_2", "role": "we0" }} , 
 	{ "name": "real_arr32_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_2", "role": "d0" }} , 
 	{ "name": "real_arr32_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_2", "role": "q0" }} , 
 	{ "name": "imag_arr32_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_arr32_2", "role": "address0" }} , 
 	{ "name": "imag_arr32_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_2", "role": "ce0" }} , 
 	{ "name": "imag_arr32_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_2", "role": "we0" }} , 
 	{ "name": "imag_arr32_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_2", "role": "d0" }} , 
 	{ "name": "imag_arr32_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_2", "role": "q0" }} , 
 	{ "name": "real_arr32_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_arr32_3", "role": "address0" }} , 
 	{ "name": "real_arr32_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_3", "role": "ce0" }} , 
 	{ "name": "real_arr32_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_3", "role": "we0" }} , 
 	{ "name": "real_arr32_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_3", "role": "d0" }} , 
 	{ "name": "real_arr32_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_3", "role": "q0" }} , 
 	{ "name": "imag_arr32_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_arr32_3", "role": "address0" }} , 
 	{ "name": "imag_arr32_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_3", "role": "ce0" }} , 
 	{ "name": "imag_arr32_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_3", "role": "we0" }} , 
 	{ "name": "imag_arr32_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_3", "role": "d0" }} , 
 	{ "name": "imag_arr32_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_3", "role": "q0" }} , 
 	{ "name": "imag_arr16_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "imag_arr16_1", "role": "address0" }} , 
 	{ "name": "imag_arr16_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_1", "role": "ce0" }} , 
 	{ "name": "imag_arr16_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_1", "role": "we0" }} , 
 	{ "name": "imag_arr16_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_1", "role": "d0" }} , 
 	{ "name": "imag_arr16_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_1", "role": "q0" }} , 
 	{ "name": "real_arr16_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "real_arr16_1", "role": "address0" }} , 
 	{ "name": "real_arr16_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_1", "role": "ce0" }} , 
 	{ "name": "real_arr16_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_1", "role": "we0" }} , 
 	{ "name": "real_arr16_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_1", "role": "d0" }} , 
 	{ "name": "real_arr16_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_1", "role": "q0" }} , 
 	{ "name": "imag_arr16_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "imag_arr16_0", "role": "address0" }} , 
 	{ "name": "imag_arr16_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_0", "role": "ce0" }} , 
 	{ "name": "imag_arr16_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_0", "role": "we0" }} , 
 	{ "name": "imag_arr16_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_0", "role": "d0" }} , 
 	{ "name": "imag_arr16_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_0", "role": "q0" }} , 
 	{ "name": "real_arr16_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "real_arr16_0", "role": "address0" }} , 
 	{ "name": "real_arr16_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_0", "role": "ce0" }} , 
 	{ "name": "real_arr16_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_0", "role": "we0" }} , 
 	{ "name": "real_arr16_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_0", "role": "d0" }} , 
 	{ "name": "real_arr16_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_0", "role": "q0" }} , 
 	{ "name": "real_arr16_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "real_arr16_2", "role": "address0" }} , 
 	{ "name": "real_arr16_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_2", "role": "ce0" }} , 
 	{ "name": "real_arr16_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_2", "role": "we0" }} , 
 	{ "name": "real_arr16_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_2", "role": "d0" }} , 
 	{ "name": "real_arr16_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_2", "role": "q0" }} , 
 	{ "name": "imag_arr16_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "imag_arr16_2", "role": "address0" }} , 
 	{ "name": "imag_arr16_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_2", "role": "ce0" }} , 
 	{ "name": "imag_arr16_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_2", "role": "we0" }} , 
 	{ "name": "imag_arr16_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_2", "role": "d0" }} , 
 	{ "name": "imag_arr16_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_2", "role": "q0" }} , 
 	{ "name": "real_arr16_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "real_arr16_3", "role": "address0" }} , 
 	{ "name": "real_arr16_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_3", "role": "ce0" }} , 
 	{ "name": "real_arr16_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_3", "role": "we0" }} , 
 	{ "name": "real_arr16_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_3", "role": "d0" }} , 
 	{ "name": "real_arr16_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_3", "role": "q0" }} , 
 	{ "name": "imag_arr16_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "imag_arr16_3", "role": "address0" }} , 
 	{ "name": "imag_arr16_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_3", "role": "ce0" }} , 
 	{ "name": "imag_arr16_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_3", "role": "we0" }} , 
 	{ "name": "imag_arr16_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_3", "role": "d0" }} , 
 	{ "name": "imag_arr16_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_3", "role": "q0" }} , 
 	{ "name": "imag_arr8_1_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_1", "role": "i" }} , 
 	{ "name": "imag_arr8_1_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_1", "role": "o" }} , 
 	{ "name": "imag_arr8_1_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_1", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr8_1_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_1", "role": "i" }} , 
 	{ "name": "real_arr8_1_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_1", "role": "o" }} , 
 	{ "name": "real_arr8_1_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_1", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr8_0_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_0", "role": "i" }} , 
 	{ "name": "imag_arr8_0_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_0", "role": "o" }} , 
 	{ "name": "imag_arr8_0_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_0", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr8_0_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_0", "role": "i" }} , 
 	{ "name": "real_arr8_0_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_0", "role": "o" }} , 
 	{ "name": "real_arr8_0_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_0", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr8_2_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_2", "role": "i" }} , 
 	{ "name": "real_arr8_2_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_2", "role": "o" }} , 
 	{ "name": "real_arr8_2_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_2", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr8_4_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_4", "role": "i" }} , 
 	{ "name": "real_arr8_4_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_4", "role": "o" }} , 
 	{ "name": "real_arr8_4_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_4", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr8_6_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_6", "role": "i" }} , 
 	{ "name": "real_arr8_6_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_6", "role": "o" }} , 
 	{ "name": "real_arr8_6_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_6", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr8_2_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_2", "role": "i" }} , 
 	{ "name": "imag_arr8_2_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_2", "role": "o" }} , 
 	{ "name": "imag_arr8_2_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_2", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr8_4_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_4", "role": "i" }} , 
 	{ "name": "imag_arr8_4_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_4", "role": "o" }} , 
 	{ "name": "imag_arr8_4_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_4", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr8_6_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_6", "role": "i" }} , 
 	{ "name": "imag_arr8_6_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_6", "role": "o" }} , 
 	{ "name": "imag_arr8_6_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_6", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr8_3_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_3", "role": "i" }} , 
 	{ "name": "real_arr8_3_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_3", "role": "o" }} , 
 	{ "name": "real_arr8_3_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_3", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr8_5_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_5", "role": "i" }} , 
 	{ "name": "real_arr8_5_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_5", "role": "o" }} , 
 	{ "name": "real_arr8_5_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_5", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr8_7_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_7", "role": "i" }} , 
 	{ "name": "real_arr8_7_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_7", "role": "o" }} , 
 	{ "name": "real_arr8_7_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_7", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr8_3_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_3", "role": "i" }} , 
 	{ "name": "imag_arr8_3_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_3", "role": "o" }} , 
 	{ "name": "imag_arr8_3_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_3", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr8_5_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_5", "role": "i" }} , 
 	{ "name": "imag_arr8_5_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_5", "role": "o" }} , 
 	{ "name": "imag_arr8_5_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_5", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr8_7_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_7", "role": "i" }} , 
 	{ "name": "imag_arr8_7_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_7", "role": "o" }} , 
 	{ "name": "imag_arr8_7_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_7", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr4_0_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_0", "role": "i" }} , 
 	{ "name": "imag_arr4_0_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_0", "role": "o" }} , 
 	{ "name": "imag_arr4_0_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr4_0", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr4_0_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_0", "role": "i" }} , 
 	{ "name": "real_arr4_0_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_0", "role": "o" }} , 
 	{ "name": "real_arr4_0_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr4_0", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr4_1_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_1", "role": "i" }} , 
 	{ "name": "real_arr4_1_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_1", "role": "o" }} , 
 	{ "name": "real_arr4_1_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr4_1", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr4_2_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_2", "role": "i" }} , 
 	{ "name": "real_arr4_2_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_2", "role": "o" }} , 
 	{ "name": "real_arr4_2_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr4_2", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr4_3_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_3", "role": "i" }} , 
 	{ "name": "real_arr4_3_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_3", "role": "o" }} , 
 	{ "name": "real_arr4_3_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr4_3", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr4_1_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_1", "role": "i" }} , 
 	{ "name": "imag_arr4_1_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_1", "role": "o" }} , 
 	{ "name": "imag_arr4_1_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr4_1", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr4_2_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_2", "role": "i" }} , 
 	{ "name": "imag_arr4_2_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_2", "role": "o" }} , 
 	{ "name": "imag_arr4_2_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr4_2", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr4_3_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_3", "role": "i" }} , 
 	{ "name": "imag_arr4_3_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_3", "role": "o" }} , 
 	{ "name": "imag_arr4_3_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr4_3", "role": "o_ap_vld" }}  ]}
set Spec2ImplPortList { 
	real_i_0 { ap_memory {  { real_i_0_address0 mem_address 1 3 }  { real_i_0_ce0 mem_ce 1 1 }  { real_i_0_q0 mem_dout 0 32 } } }
	real_i_1 { ap_memory {  { real_i_1_address0 mem_address 1 3 }  { real_i_1_ce0 mem_ce 1 1 }  { real_i_1_q0 mem_dout 0 32 } } }
	real_i_2 { ap_memory {  { real_i_2_address0 mem_address 1 3 }  { real_i_2_ce0 mem_ce 1 1 }  { real_i_2_q0 mem_dout 0 32 } } }
	real_i_3 { ap_memory {  { real_i_3_address0 mem_address 1 3 }  { real_i_3_ce0 mem_ce 1 1 }  { real_i_3_q0 mem_dout 0 32 } } }
	real_i_4 { ap_memory {  { real_i_4_address0 mem_address 1 3 }  { real_i_4_ce0 mem_ce 1 1 }  { real_i_4_q0 mem_dout 0 32 } } }
	real_i_5 { ap_memory {  { real_i_5_address0 mem_address 1 3 }  { real_i_5_ce0 mem_ce 1 1 }  { real_i_5_q0 mem_dout 0 32 } } }
	real_i_6 { ap_memory {  { real_i_6_address0 mem_address 1 3 }  { real_i_6_ce0 mem_ce 1 1 }  { real_i_6_q0 mem_dout 0 32 } } }
	real_i_7 { ap_memory {  { real_i_7_address0 mem_address 1 3 }  { real_i_7_ce0 mem_ce 1 1 }  { real_i_7_q0 mem_dout 0 32 } } }
	real_i_8 { ap_memory {  { real_i_8_address0 mem_address 1 3 }  { real_i_8_ce0 mem_ce 1 1 }  { real_i_8_q0 mem_dout 0 32 } } }
	real_i_9 { ap_memory {  { real_i_9_address0 mem_address 1 3 }  { real_i_9_ce0 mem_ce 1 1 }  { real_i_9_q0 mem_dout 0 32 } } }
	real_i_10 { ap_memory {  { real_i_10_address0 mem_address 1 3 }  { real_i_10_ce0 mem_ce 1 1 }  { real_i_10_q0 mem_dout 0 32 } } }
	real_i_11 { ap_memory {  { real_i_11_address0 mem_address 1 3 }  { real_i_11_ce0 mem_ce 1 1 }  { real_i_11_q0 mem_dout 0 32 } } }
	real_i_12 { ap_memory {  { real_i_12_address0 mem_address 1 3 }  { real_i_12_ce0 mem_ce 1 1 }  { real_i_12_q0 mem_dout 0 32 } } }
	real_i_13 { ap_memory {  { real_i_13_address0 mem_address 1 3 }  { real_i_13_ce0 mem_ce 1 1 }  { real_i_13_q0 mem_dout 0 32 } } }
	real_i_14 { ap_memory {  { real_i_14_address0 mem_address 1 3 }  { real_i_14_ce0 mem_ce 1 1 }  { real_i_14_q0 mem_dout 0 32 } } }
	real_i_15 { ap_memory {  { real_i_15_address0 mem_address 1 3 }  { real_i_15_ce0 mem_ce 1 1 }  { real_i_15_q0 mem_dout 0 32 } } }
	real_i_16 { ap_memory {  { real_i_16_address0 mem_address 1 3 }  { real_i_16_ce0 mem_ce 1 1 }  { real_i_16_q0 mem_dout 0 32 } } }
	real_i_17 { ap_memory {  { real_i_17_address0 mem_address 1 3 }  { real_i_17_ce0 mem_ce 1 1 }  { real_i_17_q0 mem_dout 0 32 } } }
	real_i_18 { ap_memory {  { real_i_18_address0 mem_address 1 3 }  { real_i_18_ce0 mem_ce 1 1 }  { real_i_18_q0 mem_dout 0 32 } } }
	real_i_19 { ap_memory {  { real_i_19_address0 mem_address 1 3 }  { real_i_19_ce0 mem_ce 1 1 }  { real_i_19_q0 mem_dout 0 32 } } }
	real_i_20 { ap_memory {  { real_i_20_address0 mem_address 1 3 }  { real_i_20_ce0 mem_ce 1 1 }  { real_i_20_q0 mem_dout 0 32 } } }
	real_i_21 { ap_memory {  { real_i_21_address0 mem_address 1 3 }  { real_i_21_ce0 mem_ce 1 1 }  { real_i_21_q0 mem_dout 0 32 } } }
	real_i_22 { ap_memory {  { real_i_22_address0 mem_address 1 3 }  { real_i_22_ce0 mem_ce 1 1 }  { real_i_22_q0 mem_dout 0 32 } } }
	real_i_23 { ap_memory {  { real_i_23_address0 mem_address 1 3 }  { real_i_23_ce0 mem_ce 1 1 }  { real_i_23_q0 mem_dout 0 32 } } }
	real_i_24 { ap_memory {  { real_i_24_address0 mem_address 1 3 }  { real_i_24_ce0 mem_ce 1 1 }  { real_i_24_q0 mem_dout 0 32 } } }
	real_i_25 { ap_memory {  { real_i_25_address0 mem_address 1 3 }  { real_i_25_ce0 mem_ce 1 1 }  { real_i_25_q0 mem_dout 0 32 } } }
	real_i_26 { ap_memory {  { real_i_26_address0 mem_address 1 3 }  { real_i_26_ce0 mem_ce 1 1 }  { real_i_26_q0 mem_dout 0 32 } } }
	real_i_27 { ap_memory {  { real_i_27_address0 mem_address 1 3 }  { real_i_27_ce0 mem_ce 1 1 }  { real_i_27_q0 mem_dout 0 32 } } }
	real_i_28 { ap_memory {  { real_i_28_address0 mem_address 1 3 }  { real_i_28_ce0 mem_ce 1 1 }  { real_i_28_q0 mem_dout 0 32 } } }
	real_i_29 { ap_memory {  { real_i_29_address0 mem_address 1 3 }  { real_i_29_ce0 mem_ce 1 1 }  { real_i_29_q0 mem_dout 0 32 } } }
	real_i_30 { ap_memory {  { real_i_30_address0 mem_address 1 3 }  { real_i_30_ce0 mem_ce 1 1 }  { real_i_30_q0 mem_dout 0 32 } } }
	real_i_31 { ap_memory {  { real_i_31_address0 mem_address 1 3 }  { real_i_31_ce0 mem_ce 1 1 }  { real_i_31_q0 mem_dout 0 32 } } }
	imag_i_0 { ap_memory {  { imag_i_0_address0 mem_address 1 3 }  { imag_i_0_ce0 mem_ce 1 1 }  { imag_i_0_q0 mem_dout 0 32 } } }
	imag_i_1 { ap_memory {  { imag_i_1_address0 mem_address 1 3 }  { imag_i_1_ce0 mem_ce 1 1 }  { imag_i_1_q0 mem_dout 0 32 } } }
	imag_i_2 { ap_memory {  { imag_i_2_address0 mem_address 1 3 }  { imag_i_2_ce0 mem_ce 1 1 }  { imag_i_2_q0 mem_dout 0 32 } } }
	imag_i_3 { ap_memory {  { imag_i_3_address0 mem_address 1 3 }  { imag_i_3_ce0 mem_ce 1 1 }  { imag_i_3_q0 mem_dout 0 32 } } }
	imag_i_4 { ap_memory {  { imag_i_4_address0 mem_address 1 3 }  { imag_i_4_ce0 mem_ce 1 1 }  { imag_i_4_q0 mem_dout 0 32 } } }
	imag_i_5 { ap_memory {  { imag_i_5_address0 mem_address 1 3 }  { imag_i_5_ce0 mem_ce 1 1 }  { imag_i_5_q0 mem_dout 0 32 } } }
	imag_i_6 { ap_memory {  { imag_i_6_address0 mem_address 1 3 }  { imag_i_6_ce0 mem_ce 1 1 }  { imag_i_6_q0 mem_dout 0 32 } } }
	imag_i_7 { ap_memory {  { imag_i_7_address0 mem_address 1 3 }  { imag_i_7_ce0 mem_ce 1 1 }  { imag_i_7_q0 mem_dout 0 32 } } }
	imag_i_8 { ap_memory {  { imag_i_8_address0 mem_address 1 3 }  { imag_i_8_ce0 mem_ce 1 1 }  { imag_i_8_q0 mem_dout 0 32 } } }
	imag_i_9 { ap_memory {  { imag_i_9_address0 mem_address 1 3 }  { imag_i_9_ce0 mem_ce 1 1 }  { imag_i_9_q0 mem_dout 0 32 } } }
	imag_i_10 { ap_memory {  { imag_i_10_address0 mem_address 1 3 }  { imag_i_10_ce0 mem_ce 1 1 }  { imag_i_10_q0 mem_dout 0 32 } } }
	imag_i_11 { ap_memory {  { imag_i_11_address0 mem_address 1 3 }  { imag_i_11_ce0 mem_ce 1 1 }  { imag_i_11_q0 mem_dout 0 32 } } }
	imag_i_12 { ap_memory {  { imag_i_12_address0 mem_address 1 3 }  { imag_i_12_ce0 mem_ce 1 1 }  { imag_i_12_q0 mem_dout 0 32 } } }
	imag_i_13 { ap_memory {  { imag_i_13_address0 mem_address 1 3 }  { imag_i_13_ce0 mem_ce 1 1 }  { imag_i_13_q0 mem_dout 0 32 } } }
	imag_i_14 { ap_memory {  { imag_i_14_address0 mem_address 1 3 }  { imag_i_14_ce0 mem_ce 1 1 }  { imag_i_14_q0 mem_dout 0 32 } } }
	imag_i_15 { ap_memory {  { imag_i_15_address0 mem_address 1 3 }  { imag_i_15_ce0 mem_ce 1 1 }  { imag_i_15_q0 mem_dout 0 32 } } }
	imag_i_16 { ap_memory {  { imag_i_16_address0 mem_address 1 3 }  { imag_i_16_ce0 mem_ce 1 1 }  { imag_i_16_q0 mem_dout 0 32 } } }
	imag_i_17 { ap_memory {  { imag_i_17_address0 mem_address 1 3 }  { imag_i_17_ce0 mem_ce 1 1 }  { imag_i_17_q0 mem_dout 0 32 } } }
	imag_i_18 { ap_memory {  { imag_i_18_address0 mem_address 1 3 }  { imag_i_18_ce0 mem_ce 1 1 }  { imag_i_18_q0 mem_dout 0 32 } } }
	imag_i_19 { ap_memory {  { imag_i_19_address0 mem_address 1 3 }  { imag_i_19_ce0 mem_ce 1 1 }  { imag_i_19_q0 mem_dout 0 32 } } }
	imag_i_20 { ap_memory {  { imag_i_20_address0 mem_address 1 3 }  { imag_i_20_ce0 mem_ce 1 1 }  { imag_i_20_q0 mem_dout 0 32 } } }
	imag_i_21 { ap_memory {  { imag_i_21_address0 mem_address 1 3 }  { imag_i_21_ce0 mem_ce 1 1 }  { imag_i_21_q0 mem_dout 0 32 } } }
	imag_i_22 { ap_memory {  { imag_i_22_address0 mem_address 1 3 }  { imag_i_22_ce0 mem_ce 1 1 }  { imag_i_22_q0 mem_dout 0 32 } } }
	imag_i_23 { ap_memory {  { imag_i_23_address0 mem_address 1 3 }  { imag_i_23_ce0 mem_ce 1 1 }  { imag_i_23_q0 mem_dout 0 32 } } }
	imag_i_24 { ap_memory {  { imag_i_24_address0 mem_address 1 3 }  { imag_i_24_ce0 mem_ce 1 1 }  { imag_i_24_q0 mem_dout 0 32 } } }
	imag_i_25 { ap_memory {  { imag_i_25_address0 mem_address 1 3 }  { imag_i_25_ce0 mem_ce 1 1 }  { imag_i_25_q0 mem_dout 0 32 } } }
	imag_i_26 { ap_memory {  { imag_i_26_address0 mem_address 1 3 }  { imag_i_26_ce0 mem_ce 1 1 }  { imag_i_26_q0 mem_dout 0 32 } } }
	imag_i_27 { ap_memory {  { imag_i_27_address0 mem_address 1 3 }  { imag_i_27_ce0 mem_ce 1 1 }  { imag_i_27_q0 mem_dout 0 32 } } }
	imag_i_28 { ap_memory {  { imag_i_28_address0 mem_address 1 3 }  { imag_i_28_ce0 mem_ce 1 1 }  { imag_i_28_q0 mem_dout 0 32 } } }
	imag_i_29 { ap_memory {  { imag_i_29_address0 mem_address 1 3 }  { imag_i_29_ce0 mem_ce 1 1 }  { imag_i_29_q0 mem_dout 0 32 } } }
	imag_i_30 { ap_memory {  { imag_i_30_address0 mem_address 1 3 }  { imag_i_30_ce0 mem_ce 1 1 }  { imag_i_30_q0 mem_dout 0 32 } } }
	imag_i_31 { ap_memory {  { imag_i_31_address0 mem_address 1 3 }  { imag_i_31_ce0 mem_ce 1 1 }  { imag_i_31_q0 mem_dout 0 32 } } }
	real_o { ap_memory {  { real_o_address0 mem_address 1 8 }  { real_o_ce0 mem_ce 1 1 }  { real_o_we0 mem_we 1 1 }  { real_o_d0 mem_din 1 32 } } }
	imag_o { ap_memory {  { imag_o_address0 mem_address 1 8 }  { imag_o_ce0 mem_ce 1 1 }  { imag_o_we0 mem_we 1 1 }  { imag_o_d0 mem_din 1 32 } } }
	real_arr256_0 { ap_memory {  { real_arr256_0_address0 mem_address 1 6 }  { real_arr256_0_ce0 mem_ce 1 1 }  { real_arr256_0_we0 mem_we 1 1 }  { real_arr256_0_d0 mem_din 1 32 }  { real_arr256_0_q0 mem_dout 0 32 }  { real_arr256_0_address1 mem_address 1 6 }  { real_arr256_0_ce1 mem_ce 1 1 }  { real_arr256_0_we1 mem_we 1 1 }  { real_arr256_0_d1 mem_din 1 32 }  { real_arr256_0_q1 mem_dout 0 32 } } }
	imag_arr256_0 { ap_memory {  { imag_arr256_0_address0 mem_address 1 6 }  { imag_arr256_0_ce0 mem_ce 1 1 }  { imag_arr256_0_we0 mem_we 1 1 }  { imag_arr256_0_d0 mem_din 1 32 }  { imag_arr256_0_q0 mem_dout 0 32 }  { imag_arr256_0_address1 mem_address 1 6 }  { imag_arr256_0_ce1 mem_ce 1 1 }  { imag_arr256_0_we1 mem_we 1 1 }  { imag_arr256_0_d1 mem_din 1 32 }  { imag_arr256_0_q1 mem_dout 0 32 } } }
	real_arr256_1 { ap_memory {  { real_arr256_1_address0 mem_address 1 6 }  { real_arr256_1_ce0 mem_ce 1 1 }  { real_arr256_1_we0 mem_we 1 1 }  { real_arr256_1_d0 mem_din 1 32 }  { real_arr256_1_q0 mem_dout 0 32 }  { real_arr256_1_address1 mem_address 1 6 }  { real_arr256_1_ce1 mem_ce 1 1 }  { real_arr256_1_we1 mem_we 1 1 }  { real_arr256_1_d1 mem_din 1 32 }  { real_arr256_1_q1 mem_dout 0 32 } } }
	imag_arr256_1 { ap_memory {  { imag_arr256_1_address0 mem_address 1 6 }  { imag_arr256_1_ce0 mem_ce 1 1 }  { imag_arr256_1_we0 mem_we 1 1 }  { imag_arr256_1_d0 mem_din 1 32 }  { imag_arr256_1_q0 mem_dout 0 32 }  { imag_arr256_1_address1 mem_address 1 6 }  { imag_arr256_1_ce1 mem_ce 1 1 }  { imag_arr256_1_we1 mem_we 1 1 }  { imag_arr256_1_d1 mem_din 1 32 }  { imag_arr256_1_q1 mem_dout 0 32 } } }
	real_arr256_2 { ap_memory {  { real_arr256_2_address0 mem_address 1 6 }  { real_arr256_2_ce0 mem_ce 1 1 }  { real_arr256_2_we0 mem_we 1 1 }  { real_arr256_2_d0 mem_din 1 32 }  { real_arr256_2_q0 mem_dout 0 32 }  { real_arr256_2_address1 mem_address 1 6 }  { real_arr256_2_ce1 mem_ce 1 1 }  { real_arr256_2_we1 mem_we 1 1 }  { real_arr256_2_d1 mem_din 1 32 }  { real_arr256_2_q1 mem_dout 0 32 } } }
	imag_arr256_2 { ap_memory {  { imag_arr256_2_address0 mem_address 1 6 }  { imag_arr256_2_ce0 mem_ce 1 1 }  { imag_arr256_2_we0 mem_we 1 1 }  { imag_arr256_2_d0 mem_din 1 32 }  { imag_arr256_2_q0 mem_dout 0 32 }  { imag_arr256_2_address1 mem_address 1 6 }  { imag_arr256_2_ce1 mem_ce 1 1 }  { imag_arr256_2_we1 mem_we 1 1 }  { imag_arr256_2_d1 mem_din 1 32 }  { imag_arr256_2_q1 mem_dout 0 32 } } }
	real_arr256_3 { ap_memory {  { real_arr256_3_address0 mem_address 1 6 }  { real_arr256_3_ce0 mem_ce 1 1 }  { real_arr256_3_we0 mem_we 1 1 }  { real_arr256_3_d0 mem_din 1 32 }  { real_arr256_3_q0 mem_dout 0 32 }  { real_arr256_3_address1 mem_address 1 6 }  { real_arr256_3_ce1 mem_ce 1 1 }  { real_arr256_3_we1 mem_we 1 1 }  { real_arr256_3_d1 mem_din 1 32 }  { real_arr256_3_q1 mem_dout 0 32 } } }
	imag_arr256_3 { ap_memory {  { imag_arr256_3_address0 mem_address 1 6 }  { imag_arr256_3_ce0 mem_ce 1 1 }  { imag_arr256_3_we0 mem_we 1 1 }  { imag_arr256_3_d0 mem_din 1 32 }  { imag_arr256_3_q0 mem_dout 0 32 }  { imag_arr256_3_address1 mem_address 1 6 }  { imag_arr256_3_ce1 mem_ce 1 1 }  { imag_arr256_3_we1 mem_we 1 1 }  { imag_arr256_3_d1 mem_din 1 32 }  { imag_arr256_3_q1 mem_dout 0 32 } } }
	real_arr128_0 { ap_memory {  { real_arr128_0_address0 mem_address 1 5 }  { real_arr128_0_ce0 mem_ce 1 1 }  { real_arr128_0_we0 mem_we 1 1 }  { real_arr128_0_d0 mem_din 1 32 }  { real_arr128_0_q0 mem_dout 0 32 }  { real_arr128_0_address1 mem_address 1 5 }  { real_arr128_0_ce1 mem_ce 1 1 }  { real_arr128_0_q1 mem_dout 0 32 } } }
	imag_arr128_0 { ap_memory {  { imag_arr128_0_address0 mem_address 1 5 }  { imag_arr128_0_ce0 mem_ce 1 1 }  { imag_arr128_0_we0 mem_we 1 1 }  { imag_arr128_0_d0 mem_din 1 32 }  { imag_arr128_0_q0 mem_dout 0 32 }  { imag_arr128_0_address1 mem_address 1 5 }  { imag_arr128_0_ce1 mem_ce 1 1 }  { imag_arr128_0_q1 mem_dout 0 32 } } }
	real_arr128_1 { ap_memory {  { real_arr128_1_address0 mem_address 1 5 }  { real_arr128_1_ce0 mem_ce 1 1 }  { real_arr128_1_we0 mem_we 1 1 }  { real_arr128_1_d0 mem_din 1 32 }  { real_arr128_1_q0 mem_dout 0 32 }  { real_arr128_1_address1 mem_address 1 5 }  { real_arr128_1_ce1 mem_ce 1 1 }  { real_arr128_1_q1 mem_dout 0 32 } } }
	imag_arr128_1 { ap_memory {  { imag_arr128_1_address0 mem_address 1 5 }  { imag_arr128_1_ce0 mem_ce 1 1 }  { imag_arr128_1_we0 mem_we 1 1 }  { imag_arr128_1_d0 mem_din 1 32 }  { imag_arr128_1_q0 mem_dout 0 32 }  { imag_arr128_1_address1 mem_address 1 5 }  { imag_arr128_1_ce1 mem_ce 1 1 }  { imag_arr128_1_q1 mem_dout 0 32 } } }
	real_arr128_2 { ap_memory {  { real_arr128_2_address0 mem_address 1 5 }  { real_arr128_2_ce0 mem_ce 1 1 }  { real_arr128_2_we0 mem_we 1 1 }  { real_arr128_2_d0 mem_din 1 32 }  { real_arr128_2_q0 mem_dout 0 32 }  { real_arr128_2_address1 mem_address 1 5 }  { real_arr128_2_ce1 mem_ce 1 1 }  { real_arr128_2_q1 mem_dout 0 32 } } }
	imag_arr128_2 { ap_memory {  { imag_arr128_2_address0 mem_address 1 5 }  { imag_arr128_2_ce0 mem_ce 1 1 }  { imag_arr128_2_we0 mem_we 1 1 }  { imag_arr128_2_d0 mem_din 1 32 }  { imag_arr128_2_q0 mem_dout 0 32 }  { imag_arr128_2_address1 mem_address 1 5 }  { imag_arr128_2_ce1 mem_ce 1 1 }  { imag_arr128_2_q1 mem_dout 0 32 } } }
	real_arr128_3 { ap_memory {  { real_arr128_3_address0 mem_address 1 5 }  { real_arr128_3_ce0 mem_ce 1 1 }  { real_arr128_3_we0 mem_we 1 1 }  { real_arr128_3_d0 mem_din 1 32 }  { real_arr128_3_q0 mem_dout 0 32 }  { real_arr128_3_address1 mem_address 1 5 }  { real_arr128_3_ce1 mem_ce 1 1 }  { real_arr128_3_q1 mem_dout 0 32 } } }
	imag_arr128_3 { ap_memory {  { imag_arr128_3_address0 mem_address 1 5 }  { imag_arr128_3_ce0 mem_ce 1 1 }  { imag_arr128_3_we0 mem_we 1 1 }  { imag_arr128_3_d0 mem_din 1 32 }  { imag_arr128_3_q0 mem_dout 0 32 }  { imag_arr128_3_address1 mem_address 1 5 }  { imag_arr128_3_ce1 mem_ce 1 1 }  { imag_arr128_3_q1 mem_dout 0 32 } } }
	real_arr64_0 { ap_memory {  { real_arr64_0_address0 mem_address 1 4 }  { real_arr64_0_ce0 mem_ce 1 1 }  { real_arr64_0_we0 mem_we 1 1 }  { real_arr64_0_d0 mem_din 1 32 }  { real_arr64_0_q0 mem_dout 0 32 }  { real_arr64_0_address1 mem_address 1 4 }  { real_arr64_0_ce1 mem_ce 1 1 }  { real_arr64_0_q1 mem_dout 0 32 } } }
	imag_arr64_0 { ap_memory {  { imag_arr64_0_address0 mem_address 1 4 }  { imag_arr64_0_ce0 mem_ce 1 1 }  { imag_arr64_0_we0 mem_we 1 1 }  { imag_arr64_0_d0 mem_din 1 32 }  { imag_arr64_0_q0 mem_dout 0 32 }  { imag_arr64_0_address1 mem_address 1 4 }  { imag_arr64_0_ce1 mem_ce 1 1 }  { imag_arr64_0_q1 mem_dout 0 32 } } }
	real_arr64_1 { ap_memory {  { real_arr64_1_address0 mem_address 1 4 }  { real_arr64_1_ce0 mem_ce 1 1 }  { real_arr64_1_we0 mem_we 1 1 }  { real_arr64_1_d0 mem_din 1 32 }  { real_arr64_1_q0 mem_dout 0 32 }  { real_arr64_1_address1 mem_address 1 4 }  { real_arr64_1_ce1 mem_ce 1 1 }  { real_arr64_1_q1 mem_dout 0 32 } } }
	imag_arr64_1 { ap_memory {  { imag_arr64_1_address0 mem_address 1 4 }  { imag_arr64_1_ce0 mem_ce 1 1 }  { imag_arr64_1_we0 mem_we 1 1 }  { imag_arr64_1_d0 mem_din 1 32 }  { imag_arr64_1_q0 mem_dout 0 32 }  { imag_arr64_1_address1 mem_address 1 4 }  { imag_arr64_1_ce1 mem_ce 1 1 }  { imag_arr64_1_q1 mem_dout 0 32 } } }
	real_arr64_2 { ap_memory {  { real_arr64_2_address0 mem_address 1 4 }  { real_arr64_2_ce0 mem_ce 1 1 }  { real_arr64_2_we0 mem_we 1 1 }  { real_arr64_2_d0 mem_din 1 32 }  { real_arr64_2_q0 mem_dout 0 32 }  { real_arr64_2_address1 mem_address 1 4 }  { real_arr64_2_ce1 mem_ce 1 1 }  { real_arr64_2_q1 mem_dout 0 32 } } }
	imag_arr64_2 { ap_memory {  { imag_arr64_2_address0 mem_address 1 4 }  { imag_arr64_2_ce0 mem_ce 1 1 }  { imag_arr64_2_we0 mem_we 1 1 }  { imag_arr64_2_d0 mem_din 1 32 }  { imag_arr64_2_q0 mem_dout 0 32 }  { imag_arr64_2_address1 mem_address 1 4 }  { imag_arr64_2_ce1 mem_ce 1 1 }  { imag_arr64_2_q1 mem_dout 0 32 } } }
	real_arr64_3 { ap_memory {  { real_arr64_3_address0 mem_address 1 4 }  { real_arr64_3_ce0 mem_ce 1 1 }  { real_arr64_3_we0 mem_we 1 1 }  { real_arr64_3_d0 mem_din 1 32 }  { real_arr64_3_q0 mem_dout 0 32 }  { real_arr64_3_address1 mem_address 1 4 }  { real_arr64_3_ce1 mem_ce 1 1 }  { real_arr64_3_q1 mem_dout 0 32 } } }
	imag_arr64_3 { ap_memory {  { imag_arr64_3_address0 mem_address 1 4 }  { imag_arr64_3_ce0 mem_ce 1 1 }  { imag_arr64_3_we0 mem_we 1 1 }  { imag_arr64_3_d0 mem_din 1 32 }  { imag_arr64_3_q0 mem_dout 0 32 }  { imag_arr64_3_address1 mem_address 1 4 }  { imag_arr64_3_ce1 mem_ce 1 1 }  { imag_arr64_3_q1 mem_dout 0 32 } } }
	real_arr32_0 { ap_memory {  { real_arr32_0_address0 mem_address 1 3 }  { real_arr32_0_ce0 mem_ce 1 1 }  { real_arr32_0_we0 mem_we 1 1 }  { real_arr32_0_d0 mem_din 1 32 }  { real_arr32_0_q0 mem_dout 0 32 } } }
	imag_arr32_0 { ap_memory {  { imag_arr32_0_address0 mem_address 1 3 }  { imag_arr32_0_ce0 mem_ce 1 1 }  { imag_arr32_0_we0 mem_we 1 1 }  { imag_arr32_0_d0 mem_din 1 32 }  { imag_arr32_0_q0 mem_dout 0 32 } } }
	real_arr32_1 { ap_memory {  { real_arr32_1_address0 mem_address 1 3 }  { real_arr32_1_ce0 mem_ce 1 1 }  { real_arr32_1_we0 mem_we 1 1 }  { real_arr32_1_d0 mem_din 1 32 }  { real_arr32_1_q0 mem_dout 0 32 } } }
	imag_arr32_1 { ap_memory {  { imag_arr32_1_address0 mem_address 1 3 }  { imag_arr32_1_ce0 mem_ce 1 1 }  { imag_arr32_1_we0 mem_we 1 1 }  { imag_arr32_1_d0 mem_din 1 32 }  { imag_arr32_1_q0 mem_dout 0 32 } } }
	real_arr32_2 { ap_memory {  { real_arr32_2_address0 mem_address 1 3 }  { real_arr32_2_ce0 mem_ce 1 1 }  { real_arr32_2_we0 mem_we 1 1 }  { real_arr32_2_d0 mem_din 1 32 }  { real_arr32_2_q0 mem_dout 0 32 } } }
	imag_arr32_2 { ap_memory {  { imag_arr32_2_address0 mem_address 1 3 }  { imag_arr32_2_ce0 mem_ce 1 1 }  { imag_arr32_2_we0 mem_we 1 1 }  { imag_arr32_2_d0 mem_din 1 32 }  { imag_arr32_2_q0 mem_dout 0 32 } } }
	real_arr32_3 { ap_memory {  { real_arr32_3_address0 mem_address 1 3 }  { real_arr32_3_ce0 mem_ce 1 1 }  { real_arr32_3_we0 mem_we 1 1 }  { real_arr32_3_d0 mem_din 1 32 }  { real_arr32_3_q0 mem_dout 0 32 } } }
	imag_arr32_3 { ap_memory {  { imag_arr32_3_address0 mem_address 1 3 }  { imag_arr32_3_ce0 mem_ce 1 1 }  { imag_arr32_3_we0 mem_we 1 1 }  { imag_arr32_3_d0 mem_din 1 32 }  { imag_arr32_3_q0 mem_dout 0 32 } } }
	imag_arr16_1 { ap_memory {  { imag_arr16_1_address0 mem_address 1 2 }  { imag_arr16_1_ce0 mem_ce 1 1 }  { imag_arr16_1_we0 mem_we 1 1 }  { imag_arr16_1_d0 mem_din 1 32 }  { imag_arr16_1_q0 mem_dout 0 32 } } }
	real_arr16_1 { ap_memory {  { real_arr16_1_address0 mem_address 1 2 }  { real_arr16_1_ce0 mem_ce 1 1 }  { real_arr16_1_we0 mem_we 1 1 }  { real_arr16_1_d0 mem_din 1 32 }  { real_arr16_1_q0 mem_dout 0 32 } } }
	imag_arr16_0 { ap_memory {  { imag_arr16_0_address0 mem_address 1 2 }  { imag_arr16_0_ce0 mem_ce 1 1 }  { imag_arr16_0_we0 mem_we 1 1 }  { imag_arr16_0_d0 mem_din 1 32 }  { imag_arr16_0_q0 mem_dout 0 32 } } }
	real_arr16_0 { ap_memory {  { real_arr16_0_address0 mem_address 1 2 }  { real_arr16_0_ce0 mem_ce 1 1 }  { real_arr16_0_we0 mem_we 1 1 }  { real_arr16_0_d0 mem_din 1 32 }  { real_arr16_0_q0 mem_dout 0 32 } } }
	real_arr16_2 { ap_memory {  { real_arr16_2_address0 mem_address 1 2 }  { real_arr16_2_ce0 mem_ce 1 1 }  { real_arr16_2_we0 mem_we 1 1 }  { real_arr16_2_d0 mem_din 1 32 }  { real_arr16_2_q0 mem_dout 0 32 } } }
	imag_arr16_2 { ap_memory {  { imag_arr16_2_address0 mem_address 1 2 }  { imag_arr16_2_ce0 mem_ce 1 1 }  { imag_arr16_2_we0 mem_we 1 1 }  { imag_arr16_2_d0 mem_din 1 32 }  { imag_arr16_2_q0 mem_dout 0 32 } } }
	real_arr16_3 { ap_memory {  { real_arr16_3_address0 mem_address 1 2 }  { real_arr16_3_ce0 mem_ce 1 1 }  { real_arr16_3_we0 mem_we 1 1 }  { real_arr16_3_d0 mem_din 1 32 }  { real_arr16_3_q0 mem_dout 0 32 } } }
	imag_arr16_3 { ap_memory {  { imag_arr16_3_address0 mem_address 1 2 }  { imag_arr16_3_ce0 mem_ce 1 1 }  { imag_arr16_3_we0 mem_we 1 1 }  { imag_arr16_3_d0 mem_din 1 32 }  { imag_arr16_3_q0 mem_dout 0 32 } } }
	imag_arr8_1 { ap_ovld {  { imag_arr8_1_i in_data 0 32 }  { imag_arr8_1_o out_data 1 32 }  { imag_arr8_1_o_ap_vld out_vld 1 1 } } }
	real_arr8_1 { ap_ovld {  { real_arr8_1_i in_data 0 32 }  { real_arr8_1_o out_data 1 32 }  { real_arr8_1_o_ap_vld out_vld 1 1 } } }
	imag_arr8_0 { ap_ovld {  { imag_arr8_0_i in_data 0 32 }  { imag_arr8_0_o out_data 1 32 }  { imag_arr8_0_o_ap_vld out_vld 1 1 } } }
	real_arr8_0 { ap_ovld {  { real_arr8_0_i in_data 0 32 }  { real_arr8_0_o out_data 1 32 }  { real_arr8_0_o_ap_vld out_vld 1 1 } } }
	real_arr8_2 { ap_ovld {  { real_arr8_2_i in_data 0 32 }  { real_arr8_2_o out_data 1 32 }  { real_arr8_2_o_ap_vld out_vld 1 1 } } }
	real_arr8_4 { ap_ovld {  { real_arr8_4_i in_data 0 32 }  { real_arr8_4_o out_data 1 32 }  { real_arr8_4_o_ap_vld out_vld 1 1 } } }
	real_arr8_6 { ap_ovld {  { real_arr8_6_i in_data 0 32 }  { real_arr8_6_o out_data 1 32 }  { real_arr8_6_o_ap_vld out_vld 1 1 } } }
	imag_arr8_2 { ap_ovld {  { imag_arr8_2_i in_data 0 32 }  { imag_arr8_2_o out_data 1 32 }  { imag_arr8_2_o_ap_vld out_vld 1 1 } } }
	imag_arr8_4 { ap_ovld {  { imag_arr8_4_i in_data 0 32 }  { imag_arr8_4_o out_data 1 32 }  { imag_arr8_4_o_ap_vld out_vld 1 1 } } }
	imag_arr8_6 { ap_ovld {  { imag_arr8_6_i in_data 0 32 }  { imag_arr8_6_o out_data 1 32 }  { imag_arr8_6_o_ap_vld out_vld 1 1 } } }
	real_arr8_3 { ap_ovld {  { real_arr8_3_i in_data 0 32 }  { real_arr8_3_o out_data 1 32 }  { real_arr8_3_o_ap_vld out_vld 1 1 } } }
	real_arr8_5 { ap_ovld {  { real_arr8_5_i in_data 0 32 }  { real_arr8_5_o out_data 1 32 }  { real_arr8_5_o_ap_vld out_vld 1 1 } } }
	real_arr8_7 { ap_ovld {  { real_arr8_7_i in_data 0 32 }  { real_arr8_7_o out_data 1 32 }  { real_arr8_7_o_ap_vld out_vld 1 1 } } }
	imag_arr8_3 { ap_ovld {  { imag_arr8_3_i in_data 0 32 }  { imag_arr8_3_o out_data 1 32 }  { imag_arr8_3_o_ap_vld out_vld 1 1 } } }
	imag_arr8_5 { ap_ovld {  { imag_arr8_5_i in_data 0 32 }  { imag_arr8_5_o out_data 1 32 }  { imag_arr8_5_o_ap_vld out_vld 1 1 } } }
	imag_arr8_7 { ap_ovld {  { imag_arr8_7_i in_data 0 32 }  { imag_arr8_7_o out_data 1 32 }  { imag_arr8_7_o_ap_vld out_vld 1 1 } } }
	imag_arr4_0 { ap_ovld {  { imag_arr4_0_i in_data 0 32 }  { imag_arr4_0_o out_data 1 32 }  { imag_arr4_0_o_ap_vld out_vld 1 1 } } }
	real_arr4_0 { ap_ovld {  { real_arr4_0_i in_data 0 32 }  { real_arr4_0_o out_data 1 32 }  { real_arr4_0_o_ap_vld out_vld 1 1 } } }
	real_arr4_1 { ap_ovld {  { real_arr4_1_i in_data 0 32 }  { real_arr4_1_o out_data 1 32 }  { real_arr4_1_o_ap_vld out_vld 1 1 } } }
	real_arr4_2 { ap_ovld {  { real_arr4_2_i in_data 0 32 }  { real_arr4_2_o out_data 1 32 }  { real_arr4_2_o_ap_vld out_vld 1 1 } } }
	real_arr4_3 { ap_ovld {  { real_arr4_3_i in_data 0 32 }  { real_arr4_3_o out_data 1 32 }  { real_arr4_3_o_ap_vld out_vld 1 1 } } }
	imag_arr4_1 { ap_ovld {  { imag_arr4_1_i in_data 0 32 }  { imag_arr4_1_o out_data 1 32 }  { imag_arr4_1_o_ap_vld out_vld 1 1 } } }
	imag_arr4_2 { ap_ovld {  { imag_arr4_2_i in_data 0 32 }  { imag_arr4_2_o out_data 1 32 }  { imag_arr4_2_o_ap_vld out_vld 1 1 } } }
	imag_arr4_3 { ap_ovld {  { imag_arr4_3_i in_data 0 32 }  { imag_arr4_3_o out_data 1 32 }  { imag_arr4_3_o_ap_vld out_vld 1 1 } } }
}
