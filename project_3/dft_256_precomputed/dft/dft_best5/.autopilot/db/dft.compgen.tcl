# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 680 \
    name real_i_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_0 \
    op interface \
    ports { real_i_0_address0 { O 3 vector } real_i_0_ce0 { O 1 bit } real_i_0_d0 { O 32 vector } real_i_0_q0 { I 32 vector } real_i_0_we0 { O 1 bit } real_i_0_address1 { O 3 vector } real_i_0_ce1 { O 1 bit } real_i_0_d1 { O 32 vector } real_i_0_q1 { I 32 vector } real_i_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 681 \
    name real_i_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_1 \
    op interface \
    ports { real_i_1_address0 { O 3 vector } real_i_1_ce0 { O 1 bit } real_i_1_d0 { O 32 vector } real_i_1_q0 { I 32 vector } real_i_1_we0 { O 1 bit } real_i_1_address1 { O 3 vector } real_i_1_ce1 { O 1 bit } real_i_1_d1 { O 32 vector } real_i_1_q1 { I 32 vector } real_i_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 682 \
    name real_i_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_2 \
    op interface \
    ports { real_i_2_address0 { O 3 vector } real_i_2_ce0 { O 1 bit } real_i_2_d0 { O 32 vector } real_i_2_q0 { I 32 vector } real_i_2_we0 { O 1 bit } real_i_2_address1 { O 3 vector } real_i_2_ce1 { O 1 bit } real_i_2_d1 { O 32 vector } real_i_2_q1 { I 32 vector } real_i_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 683 \
    name real_i_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_3 \
    op interface \
    ports { real_i_3_address0 { O 3 vector } real_i_3_ce0 { O 1 bit } real_i_3_d0 { O 32 vector } real_i_3_q0 { I 32 vector } real_i_3_we0 { O 1 bit } real_i_3_address1 { O 3 vector } real_i_3_ce1 { O 1 bit } real_i_3_d1 { O 32 vector } real_i_3_q1 { I 32 vector } real_i_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 684 \
    name real_i_4 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_4 \
    op interface \
    ports { real_i_4_address0 { O 3 vector } real_i_4_ce0 { O 1 bit } real_i_4_d0 { O 32 vector } real_i_4_q0 { I 32 vector } real_i_4_we0 { O 1 bit } real_i_4_address1 { O 3 vector } real_i_4_ce1 { O 1 bit } real_i_4_d1 { O 32 vector } real_i_4_q1 { I 32 vector } real_i_4_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_4'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 685 \
    name real_i_5 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_5 \
    op interface \
    ports { real_i_5_address0 { O 3 vector } real_i_5_ce0 { O 1 bit } real_i_5_d0 { O 32 vector } real_i_5_q0 { I 32 vector } real_i_5_we0 { O 1 bit } real_i_5_address1 { O 3 vector } real_i_5_ce1 { O 1 bit } real_i_5_d1 { O 32 vector } real_i_5_q1 { I 32 vector } real_i_5_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_5'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 686 \
    name real_i_6 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_6 \
    op interface \
    ports { real_i_6_address0 { O 3 vector } real_i_6_ce0 { O 1 bit } real_i_6_d0 { O 32 vector } real_i_6_q0 { I 32 vector } real_i_6_we0 { O 1 bit } real_i_6_address1 { O 3 vector } real_i_6_ce1 { O 1 bit } real_i_6_d1 { O 32 vector } real_i_6_q1 { I 32 vector } real_i_6_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_6'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 687 \
    name real_i_7 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_7 \
    op interface \
    ports { real_i_7_address0 { O 3 vector } real_i_7_ce0 { O 1 bit } real_i_7_d0 { O 32 vector } real_i_7_q0 { I 32 vector } real_i_7_we0 { O 1 bit } real_i_7_address1 { O 3 vector } real_i_7_ce1 { O 1 bit } real_i_7_d1 { O 32 vector } real_i_7_q1 { I 32 vector } real_i_7_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_7'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 688 \
    name real_i_8 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_8 \
    op interface \
    ports { real_i_8_address0 { O 3 vector } real_i_8_ce0 { O 1 bit } real_i_8_d0 { O 32 vector } real_i_8_q0 { I 32 vector } real_i_8_we0 { O 1 bit } real_i_8_address1 { O 3 vector } real_i_8_ce1 { O 1 bit } real_i_8_d1 { O 32 vector } real_i_8_q1 { I 32 vector } real_i_8_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_8'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 689 \
    name real_i_9 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_9 \
    op interface \
    ports { real_i_9_address0 { O 3 vector } real_i_9_ce0 { O 1 bit } real_i_9_d0 { O 32 vector } real_i_9_q0 { I 32 vector } real_i_9_we0 { O 1 bit } real_i_9_address1 { O 3 vector } real_i_9_ce1 { O 1 bit } real_i_9_d1 { O 32 vector } real_i_9_q1 { I 32 vector } real_i_9_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_9'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 690 \
    name real_i_10 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_10 \
    op interface \
    ports { real_i_10_address0 { O 3 vector } real_i_10_ce0 { O 1 bit } real_i_10_d0 { O 32 vector } real_i_10_q0 { I 32 vector } real_i_10_we0 { O 1 bit } real_i_10_address1 { O 3 vector } real_i_10_ce1 { O 1 bit } real_i_10_d1 { O 32 vector } real_i_10_q1 { I 32 vector } real_i_10_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_10'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 691 \
    name real_i_11 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_11 \
    op interface \
    ports { real_i_11_address0 { O 3 vector } real_i_11_ce0 { O 1 bit } real_i_11_d0 { O 32 vector } real_i_11_q0 { I 32 vector } real_i_11_we0 { O 1 bit } real_i_11_address1 { O 3 vector } real_i_11_ce1 { O 1 bit } real_i_11_d1 { O 32 vector } real_i_11_q1 { I 32 vector } real_i_11_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_11'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 692 \
    name real_i_12 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_12 \
    op interface \
    ports { real_i_12_address0 { O 3 vector } real_i_12_ce0 { O 1 bit } real_i_12_d0 { O 32 vector } real_i_12_q0 { I 32 vector } real_i_12_we0 { O 1 bit } real_i_12_address1 { O 3 vector } real_i_12_ce1 { O 1 bit } real_i_12_d1 { O 32 vector } real_i_12_q1 { I 32 vector } real_i_12_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_12'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 693 \
    name real_i_13 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_13 \
    op interface \
    ports { real_i_13_address0 { O 3 vector } real_i_13_ce0 { O 1 bit } real_i_13_d0 { O 32 vector } real_i_13_q0 { I 32 vector } real_i_13_we0 { O 1 bit } real_i_13_address1 { O 3 vector } real_i_13_ce1 { O 1 bit } real_i_13_d1 { O 32 vector } real_i_13_q1 { I 32 vector } real_i_13_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_13'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 694 \
    name real_i_14 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_14 \
    op interface \
    ports { real_i_14_address0 { O 3 vector } real_i_14_ce0 { O 1 bit } real_i_14_d0 { O 32 vector } real_i_14_q0 { I 32 vector } real_i_14_we0 { O 1 bit } real_i_14_address1 { O 3 vector } real_i_14_ce1 { O 1 bit } real_i_14_d1 { O 32 vector } real_i_14_q1 { I 32 vector } real_i_14_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_14'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 695 \
    name real_i_15 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_15 \
    op interface \
    ports { real_i_15_address0 { O 3 vector } real_i_15_ce0 { O 1 bit } real_i_15_d0 { O 32 vector } real_i_15_q0 { I 32 vector } real_i_15_we0 { O 1 bit } real_i_15_address1 { O 3 vector } real_i_15_ce1 { O 1 bit } real_i_15_d1 { O 32 vector } real_i_15_q1 { I 32 vector } real_i_15_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_15'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 696 \
    name real_i_16 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_16 \
    op interface \
    ports { real_i_16_address0 { O 3 vector } real_i_16_ce0 { O 1 bit } real_i_16_d0 { O 32 vector } real_i_16_q0 { I 32 vector } real_i_16_we0 { O 1 bit } real_i_16_address1 { O 3 vector } real_i_16_ce1 { O 1 bit } real_i_16_d1 { O 32 vector } real_i_16_q1 { I 32 vector } real_i_16_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_16'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 697 \
    name real_i_17 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_17 \
    op interface \
    ports { real_i_17_address0 { O 3 vector } real_i_17_ce0 { O 1 bit } real_i_17_d0 { O 32 vector } real_i_17_q0 { I 32 vector } real_i_17_we0 { O 1 bit } real_i_17_address1 { O 3 vector } real_i_17_ce1 { O 1 bit } real_i_17_d1 { O 32 vector } real_i_17_q1 { I 32 vector } real_i_17_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_17'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 698 \
    name real_i_18 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_18 \
    op interface \
    ports { real_i_18_address0 { O 3 vector } real_i_18_ce0 { O 1 bit } real_i_18_d0 { O 32 vector } real_i_18_q0 { I 32 vector } real_i_18_we0 { O 1 bit } real_i_18_address1 { O 3 vector } real_i_18_ce1 { O 1 bit } real_i_18_d1 { O 32 vector } real_i_18_q1 { I 32 vector } real_i_18_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_18'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 699 \
    name real_i_19 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_19 \
    op interface \
    ports { real_i_19_address0 { O 3 vector } real_i_19_ce0 { O 1 bit } real_i_19_d0 { O 32 vector } real_i_19_q0 { I 32 vector } real_i_19_we0 { O 1 bit } real_i_19_address1 { O 3 vector } real_i_19_ce1 { O 1 bit } real_i_19_d1 { O 32 vector } real_i_19_q1 { I 32 vector } real_i_19_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_19'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 700 \
    name real_i_20 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_20 \
    op interface \
    ports { real_i_20_address0 { O 3 vector } real_i_20_ce0 { O 1 bit } real_i_20_d0 { O 32 vector } real_i_20_q0 { I 32 vector } real_i_20_we0 { O 1 bit } real_i_20_address1 { O 3 vector } real_i_20_ce1 { O 1 bit } real_i_20_d1 { O 32 vector } real_i_20_q1 { I 32 vector } real_i_20_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_20'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 701 \
    name real_i_21 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_21 \
    op interface \
    ports { real_i_21_address0 { O 3 vector } real_i_21_ce0 { O 1 bit } real_i_21_d0 { O 32 vector } real_i_21_q0 { I 32 vector } real_i_21_we0 { O 1 bit } real_i_21_address1 { O 3 vector } real_i_21_ce1 { O 1 bit } real_i_21_d1 { O 32 vector } real_i_21_q1 { I 32 vector } real_i_21_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_21'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 702 \
    name real_i_22 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_22 \
    op interface \
    ports { real_i_22_address0 { O 3 vector } real_i_22_ce0 { O 1 bit } real_i_22_d0 { O 32 vector } real_i_22_q0 { I 32 vector } real_i_22_we0 { O 1 bit } real_i_22_address1 { O 3 vector } real_i_22_ce1 { O 1 bit } real_i_22_d1 { O 32 vector } real_i_22_q1 { I 32 vector } real_i_22_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_22'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 703 \
    name real_i_23 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_23 \
    op interface \
    ports { real_i_23_address0 { O 3 vector } real_i_23_ce0 { O 1 bit } real_i_23_d0 { O 32 vector } real_i_23_q0 { I 32 vector } real_i_23_we0 { O 1 bit } real_i_23_address1 { O 3 vector } real_i_23_ce1 { O 1 bit } real_i_23_d1 { O 32 vector } real_i_23_q1 { I 32 vector } real_i_23_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_23'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 704 \
    name real_i_24 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_24 \
    op interface \
    ports { real_i_24_address0 { O 3 vector } real_i_24_ce0 { O 1 bit } real_i_24_d0 { O 32 vector } real_i_24_q0 { I 32 vector } real_i_24_we0 { O 1 bit } real_i_24_address1 { O 3 vector } real_i_24_ce1 { O 1 bit } real_i_24_d1 { O 32 vector } real_i_24_q1 { I 32 vector } real_i_24_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_24'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 705 \
    name real_i_25 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_25 \
    op interface \
    ports { real_i_25_address0 { O 3 vector } real_i_25_ce0 { O 1 bit } real_i_25_d0 { O 32 vector } real_i_25_q0 { I 32 vector } real_i_25_we0 { O 1 bit } real_i_25_address1 { O 3 vector } real_i_25_ce1 { O 1 bit } real_i_25_d1 { O 32 vector } real_i_25_q1 { I 32 vector } real_i_25_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_25'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 706 \
    name real_i_26 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_26 \
    op interface \
    ports { real_i_26_address0 { O 3 vector } real_i_26_ce0 { O 1 bit } real_i_26_d0 { O 32 vector } real_i_26_q0 { I 32 vector } real_i_26_we0 { O 1 bit } real_i_26_address1 { O 3 vector } real_i_26_ce1 { O 1 bit } real_i_26_d1 { O 32 vector } real_i_26_q1 { I 32 vector } real_i_26_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_26'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 707 \
    name real_i_27 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_27 \
    op interface \
    ports { real_i_27_address0 { O 3 vector } real_i_27_ce0 { O 1 bit } real_i_27_d0 { O 32 vector } real_i_27_q0 { I 32 vector } real_i_27_we0 { O 1 bit } real_i_27_address1 { O 3 vector } real_i_27_ce1 { O 1 bit } real_i_27_d1 { O 32 vector } real_i_27_q1 { I 32 vector } real_i_27_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_27'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 708 \
    name real_i_28 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_28 \
    op interface \
    ports { real_i_28_address0 { O 3 vector } real_i_28_ce0 { O 1 bit } real_i_28_d0 { O 32 vector } real_i_28_q0 { I 32 vector } real_i_28_we0 { O 1 bit } real_i_28_address1 { O 3 vector } real_i_28_ce1 { O 1 bit } real_i_28_d1 { O 32 vector } real_i_28_q1 { I 32 vector } real_i_28_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_28'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 709 \
    name real_i_29 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_29 \
    op interface \
    ports { real_i_29_address0 { O 3 vector } real_i_29_ce0 { O 1 bit } real_i_29_d0 { O 32 vector } real_i_29_q0 { I 32 vector } real_i_29_we0 { O 1 bit } real_i_29_address1 { O 3 vector } real_i_29_ce1 { O 1 bit } real_i_29_d1 { O 32 vector } real_i_29_q1 { I 32 vector } real_i_29_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_29'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 710 \
    name real_i_30 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_30 \
    op interface \
    ports { real_i_30_address0 { O 3 vector } real_i_30_ce0 { O 1 bit } real_i_30_d0 { O 32 vector } real_i_30_q0 { I 32 vector } real_i_30_we0 { O 1 bit } real_i_30_address1 { O 3 vector } real_i_30_ce1 { O 1 bit } real_i_30_d1 { O 32 vector } real_i_30_q1 { I 32 vector } real_i_30_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_30'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 711 \
    name real_i_31 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_31 \
    op interface \
    ports { real_i_31_address0 { O 3 vector } real_i_31_ce0 { O 1 bit } real_i_31_d0 { O 32 vector } real_i_31_q0 { I 32 vector } real_i_31_we0 { O 1 bit } real_i_31_address1 { O 3 vector } real_i_31_ce1 { O 1 bit } real_i_31_d1 { O 32 vector } real_i_31_q1 { I 32 vector } real_i_31_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_31'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 712 \
    name imag_i_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_0 \
    op interface \
    ports { imag_i_0_address0 { O 3 vector } imag_i_0_ce0 { O 1 bit } imag_i_0_d0 { O 32 vector } imag_i_0_q0 { I 32 vector } imag_i_0_we0 { O 1 bit } imag_i_0_address1 { O 3 vector } imag_i_0_ce1 { O 1 bit } imag_i_0_d1 { O 32 vector } imag_i_0_q1 { I 32 vector } imag_i_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 713 \
    name imag_i_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_1 \
    op interface \
    ports { imag_i_1_address0 { O 3 vector } imag_i_1_ce0 { O 1 bit } imag_i_1_d0 { O 32 vector } imag_i_1_q0 { I 32 vector } imag_i_1_we0 { O 1 bit } imag_i_1_address1 { O 3 vector } imag_i_1_ce1 { O 1 bit } imag_i_1_d1 { O 32 vector } imag_i_1_q1 { I 32 vector } imag_i_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 714 \
    name imag_i_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_2 \
    op interface \
    ports { imag_i_2_address0 { O 3 vector } imag_i_2_ce0 { O 1 bit } imag_i_2_d0 { O 32 vector } imag_i_2_q0 { I 32 vector } imag_i_2_we0 { O 1 bit } imag_i_2_address1 { O 3 vector } imag_i_2_ce1 { O 1 bit } imag_i_2_d1 { O 32 vector } imag_i_2_q1 { I 32 vector } imag_i_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 715 \
    name imag_i_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_3 \
    op interface \
    ports { imag_i_3_address0 { O 3 vector } imag_i_3_ce0 { O 1 bit } imag_i_3_d0 { O 32 vector } imag_i_3_q0 { I 32 vector } imag_i_3_we0 { O 1 bit } imag_i_3_address1 { O 3 vector } imag_i_3_ce1 { O 1 bit } imag_i_3_d1 { O 32 vector } imag_i_3_q1 { I 32 vector } imag_i_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 716 \
    name imag_i_4 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_4 \
    op interface \
    ports { imag_i_4_address0 { O 3 vector } imag_i_4_ce0 { O 1 bit } imag_i_4_d0 { O 32 vector } imag_i_4_q0 { I 32 vector } imag_i_4_we0 { O 1 bit } imag_i_4_address1 { O 3 vector } imag_i_4_ce1 { O 1 bit } imag_i_4_d1 { O 32 vector } imag_i_4_q1 { I 32 vector } imag_i_4_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_4'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 717 \
    name imag_i_5 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_5 \
    op interface \
    ports { imag_i_5_address0 { O 3 vector } imag_i_5_ce0 { O 1 bit } imag_i_5_d0 { O 32 vector } imag_i_5_q0 { I 32 vector } imag_i_5_we0 { O 1 bit } imag_i_5_address1 { O 3 vector } imag_i_5_ce1 { O 1 bit } imag_i_5_d1 { O 32 vector } imag_i_5_q1 { I 32 vector } imag_i_5_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_5'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 718 \
    name imag_i_6 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_6 \
    op interface \
    ports { imag_i_6_address0 { O 3 vector } imag_i_6_ce0 { O 1 bit } imag_i_6_d0 { O 32 vector } imag_i_6_q0 { I 32 vector } imag_i_6_we0 { O 1 bit } imag_i_6_address1 { O 3 vector } imag_i_6_ce1 { O 1 bit } imag_i_6_d1 { O 32 vector } imag_i_6_q1 { I 32 vector } imag_i_6_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_6'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 719 \
    name imag_i_7 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_7 \
    op interface \
    ports { imag_i_7_address0 { O 3 vector } imag_i_7_ce0 { O 1 bit } imag_i_7_d0 { O 32 vector } imag_i_7_q0 { I 32 vector } imag_i_7_we0 { O 1 bit } imag_i_7_address1 { O 3 vector } imag_i_7_ce1 { O 1 bit } imag_i_7_d1 { O 32 vector } imag_i_7_q1 { I 32 vector } imag_i_7_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_7'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 720 \
    name imag_i_8 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_8 \
    op interface \
    ports { imag_i_8_address0 { O 3 vector } imag_i_8_ce0 { O 1 bit } imag_i_8_d0 { O 32 vector } imag_i_8_q0 { I 32 vector } imag_i_8_we0 { O 1 bit } imag_i_8_address1 { O 3 vector } imag_i_8_ce1 { O 1 bit } imag_i_8_d1 { O 32 vector } imag_i_8_q1 { I 32 vector } imag_i_8_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_8'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 721 \
    name imag_i_9 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_9 \
    op interface \
    ports { imag_i_9_address0 { O 3 vector } imag_i_9_ce0 { O 1 bit } imag_i_9_d0 { O 32 vector } imag_i_9_q0 { I 32 vector } imag_i_9_we0 { O 1 bit } imag_i_9_address1 { O 3 vector } imag_i_9_ce1 { O 1 bit } imag_i_9_d1 { O 32 vector } imag_i_9_q1 { I 32 vector } imag_i_9_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_9'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 722 \
    name imag_i_10 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_10 \
    op interface \
    ports { imag_i_10_address0 { O 3 vector } imag_i_10_ce0 { O 1 bit } imag_i_10_d0 { O 32 vector } imag_i_10_q0 { I 32 vector } imag_i_10_we0 { O 1 bit } imag_i_10_address1 { O 3 vector } imag_i_10_ce1 { O 1 bit } imag_i_10_d1 { O 32 vector } imag_i_10_q1 { I 32 vector } imag_i_10_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_10'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 723 \
    name imag_i_11 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_11 \
    op interface \
    ports { imag_i_11_address0 { O 3 vector } imag_i_11_ce0 { O 1 bit } imag_i_11_d0 { O 32 vector } imag_i_11_q0 { I 32 vector } imag_i_11_we0 { O 1 bit } imag_i_11_address1 { O 3 vector } imag_i_11_ce1 { O 1 bit } imag_i_11_d1 { O 32 vector } imag_i_11_q1 { I 32 vector } imag_i_11_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_11'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 724 \
    name imag_i_12 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_12 \
    op interface \
    ports { imag_i_12_address0 { O 3 vector } imag_i_12_ce0 { O 1 bit } imag_i_12_d0 { O 32 vector } imag_i_12_q0 { I 32 vector } imag_i_12_we0 { O 1 bit } imag_i_12_address1 { O 3 vector } imag_i_12_ce1 { O 1 bit } imag_i_12_d1 { O 32 vector } imag_i_12_q1 { I 32 vector } imag_i_12_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_12'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 725 \
    name imag_i_13 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_13 \
    op interface \
    ports { imag_i_13_address0 { O 3 vector } imag_i_13_ce0 { O 1 bit } imag_i_13_d0 { O 32 vector } imag_i_13_q0 { I 32 vector } imag_i_13_we0 { O 1 bit } imag_i_13_address1 { O 3 vector } imag_i_13_ce1 { O 1 bit } imag_i_13_d1 { O 32 vector } imag_i_13_q1 { I 32 vector } imag_i_13_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_13'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 726 \
    name imag_i_14 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_14 \
    op interface \
    ports { imag_i_14_address0 { O 3 vector } imag_i_14_ce0 { O 1 bit } imag_i_14_d0 { O 32 vector } imag_i_14_q0 { I 32 vector } imag_i_14_we0 { O 1 bit } imag_i_14_address1 { O 3 vector } imag_i_14_ce1 { O 1 bit } imag_i_14_d1 { O 32 vector } imag_i_14_q1 { I 32 vector } imag_i_14_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_14'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 727 \
    name imag_i_15 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_15 \
    op interface \
    ports { imag_i_15_address0 { O 3 vector } imag_i_15_ce0 { O 1 bit } imag_i_15_d0 { O 32 vector } imag_i_15_q0 { I 32 vector } imag_i_15_we0 { O 1 bit } imag_i_15_address1 { O 3 vector } imag_i_15_ce1 { O 1 bit } imag_i_15_d1 { O 32 vector } imag_i_15_q1 { I 32 vector } imag_i_15_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_15'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 728 \
    name imag_i_16 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_16 \
    op interface \
    ports { imag_i_16_address0 { O 3 vector } imag_i_16_ce0 { O 1 bit } imag_i_16_d0 { O 32 vector } imag_i_16_q0 { I 32 vector } imag_i_16_we0 { O 1 bit } imag_i_16_address1 { O 3 vector } imag_i_16_ce1 { O 1 bit } imag_i_16_d1 { O 32 vector } imag_i_16_q1 { I 32 vector } imag_i_16_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_16'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 729 \
    name imag_i_17 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_17 \
    op interface \
    ports { imag_i_17_address0 { O 3 vector } imag_i_17_ce0 { O 1 bit } imag_i_17_d0 { O 32 vector } imag_i_17_q0 { I 32 vector } imag_i_17_we0 { O 1 bit } imag_i_17_address1 { O 3 vector } imag_i_17_ce1 { O 1 bit } imag_i_17_d1 { O 32 vector } imag_i_17_q1 { I 32 vector } imag_i_17_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_17'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 730 \
    name imag_i_18 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_18 \
    op interface \
    ports { imag_i_18_address0 { O 3 vector } imag_i_18_ce0 { O 1 bit } imag_i_18_d0 { O 32 vector } imag_i_18_q0 { I 32 vector } imag_i_18_we0 { O 1 bit } imag_i_18_address1 { O 3 vector } imag_i_18_ce1 { O 1 bit } imag_i_18_d1 { O 32 vector } imag_i_18_q1 { I 32 vector } imag_i_18_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_18'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 731 \
    name imag_i_19 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_19 \
    op interface \
    ports { imag_i_19_address0 { O 3 vector } imag_i_19_ce0 { O 1 bit } imag_i_19_d0 { O 32 vector } imag_i_19_q0 { I 32 vector } imag_i_19_we0 { O 1 bit } imag_i_19_address1 { O 3 vector } imag_i_19_ce1 { O 1 bit } imag_i_19_d1 { O 32 vector } imag_i_19_q1 { I 32 vector } imag_i_19_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_19'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 732 \
    name imag_i_20 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_20 \
    op interface \
    ports { imag_i_20_address0 { O 3 vector } imag_i_20_ce0 { O 1 bit } imag_i_20_d0 { O 32 vector } imag_i_20_q0 { I 32 vector } imag_i_20_we0 { O 1 bit } imag_i_20_address1 { O 3 vector } imag_i_20_ce1 { O 1 bit } imag_i_20_d1 { O 32 vector } imag_i_20_q1 { I 32 vector } imag_i_20_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_20'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 733 \
    name imag_i_21 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_21 \
    op interface \
    ports { imag_i_21_address0 { O 3 vector } imag_i_21_ce0 { O 1 bit } imag_i_21_d0 { O 32 vector } imag_i_21_q0 { I 32 vector } imag_i_21_we0 { O 1 bit } imag_i_21_address1 { O 3 vector } imag_i_21_ce1 { O 1 bit } imag_i_21_d1 { O 32 vector } imag_i_21_q1 { I 32 vector } imag_i_21_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_21'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 734 \
    name imag_i_22 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_22 \
    op interface \
    ports { imag_i_22_address0 { O 3 vector } imag_i_22_ce0 { O 1 bit } imag_i_22_d0 { O 32 vector } imag_i_22_q0 { I 32 vector } imag_i_22_we0 { O 1 bit } imag_i_22_address1 { O 3 vector } imag_i_22_ce1 { O 1 bit } imag_i_22_d1 { O 32 vector } imag_i_22_q1 { I 32 vector } imag_i_22_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_22'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 735 \
    name imag_i_23 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_23 \
    op interface \
    ports { imag_i_23_address0 { O 3 vector } imag_i_23_ce0 { O 1 bit } imag_i_23_d0 { O 32 vector } imag_i_23_q0 { I 32 vector } imag_i_23_we0 { O 1 bit } imag_i_23_address1 { O 3 vector } imag_i_23_ce1 { O 1 bit } imag_i_23_d1 { O 32 vector } imag_i_23_q1 { I 32 vector } imag_i_23_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_23'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 736 \
    name imag_i_24 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_24 \
    op interface \
    ports { imag_i_24_address0 { O 3 vector } imag_i_24_ce0 { O 1 bit } imag_i_24_d0 { O 32 vector } imag_i_24_q0 { I 32 vector } imag_i_24_we0 { O 1 bit } imag_i_24_address1 { O 3 vector } imag_i_24_ce1 { O 1 bit } imag_i_24_d1 { O 32 vector } imag_i_24_q1 { I 32 vector } imag_i_24_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_24'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 737 \
    name imag_i_25 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_25 \
    op interface \
    ports { imag_i_25_address0 { O 3 vector } imag_i_25_ce0 { O 1 bit } imag_i_25_d0 { O 32 vector } imag_i_25_q0 { I 32 vector } imag_i_25_we0 { O 1 bit } imag_i_25_address1 { O 3 vector } imag_i_25_ce1 { O 1 bit } imag_i_25_d1 { O 32 vector } imag_i_25_q1 { I 32 vector } imag_i_25_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_25'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 738 \
    name imag_i_26 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_26 \
    op interface \
    ports { imag_i_26_address0 { O 3 vector } imag_i_26_ce0 { O 1 bit } imag_i_26_d0 { O 32 vector } imag_i_26_q0 { I 32 vector } imag_i_26_we0 { O 1 bit } imag_i_26_address1 { O 3 vector } imag_i_26_ce1 { O 1 bit } imag_i_26_d1 { O 32 vector } imag_i_26_q1 { I 32 vector } imag_i_26_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_26'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 739 \
    name imag_i_27 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_27 \
    op interface \
    ports { imag_i_27_address0 { O 3 vector } imag_i_27_ce0 { O 1 bit } imag_i_27_d0 { O 32 vector } imag_i_27_q0 { I 32 vector } imag_i_27_we0 { O 1 bit } imag_i_27_address1 { O 3 vector } imag_i_27_ce1 { O 1 bit } imag_i_27_d1 { O 32 vector } imag_i_27_q1 { I 32 vector } imag_i_27_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_27'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 740 \
    name imag_i_28 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_28 \
    op interface \
    ports { imag_i_28_address0 { O 3 vector } imag_i_28_ce0 { O 1 bit } imag_i_28_d0 { O 32 vector } imag_i_28_q0 { I 32 vector } imag_i_28_we0 { O 1 bit } imag_i_28_address1 { O 3 vector } imag_i_28_ce1 { O 1 bit } imag_i_28_d1 { O 32 vector } imag_i_28_q1 { I 32 vector } imag_i_28_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_28'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 741 \
    name imag_i_29 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_29 \
    op interface \
    ports { imag_i_29_address0 { O 3 vector } imag_i_29_ce0 { O 1 bit } imag_i_29_d0 { O 32 vector } imag_i_29_q0 { I 32 vector } imag_i_29_we0 { O 1 bit } imag_i_29_address1 { O 3 vector } imag_i_29_ce1 { O 1 bit } imag_i_29_d1 { O 32 vector } imag_i_29_q1 { I 32 vector } imag_i_29_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_29'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 742 \
    name imag_i_30 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_30 \
    op interface \
    ports { imag_i_30_address0 { O 3 vector } imag_i_30_ce0 { O 1 bit } imag_i_30_d0 { O 32 vector } imag_i_30_q0 { I 32 vector } imag_i_30_we0 { O 1 bit } imag_i_30_address1 { O 3 vector } imag_i_30_ce1 { O 1 bit } imag_i_30_d1 { O 32 vector } imag_i_30_q1 { I 32 vector } imag_i_30_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_30'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 743 \
    name imag_i_31 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_31 \
    op interface \
    ports { imag_i_31_address0 { O 3 vector } imag_i_31_ce0 { O 1 bit } imag_i_31_d0 { O 32 vector } imag_i_31_q0 { I 32 vector } imag_i_31_we0 { O 1 bit } imag_i_31_address1 { O 3 vector } imag_i_31_ce1 { O 1 bit } imag_i_31_d1 { O 32 vector } imag_i_31_q1 { I 32 vector } imag_i_31_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_31'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 744 \
    name real_o \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o \
    op interface \
    ports { real_o_address0 { O 8 vector } real_o_ce0 { O 1 bit } real_o_d0 { O 32 vector } real_o_q0 { I 32 vector } real_o_we0 { O 1 bit } real_o_address1 { O 8 vector } real_o_ce1 { O 1 bit } real_o_d1 { O 32 vector } real_o_q1 { I 32 vector } real_o_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 745 \
    name imag_o \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o \
    op interface \
    ports { imag_o_address0 { O 8 vector } imag_o_ce0 { O 1 bit } imag_o_d0 { O 32 vector } imag_o_q0 { I 32 vector } imag_o_we0 { O 1 bit } imag_o_address1 { O 8 vector } imag_o_ce1 { O 1 bit } imag_o_d1 { O 32 vector } imag_o_q1 { I 32 vector } imag_o_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


