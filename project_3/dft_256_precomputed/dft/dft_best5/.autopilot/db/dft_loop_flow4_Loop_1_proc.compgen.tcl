# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 484 \
    name real_arr8_6 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_6 \
    op interface \
    ports { real_arr8_6 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 485 \
    name real_arr8_7 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_7 \
    op interface \
    ports { real_arr8_7 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 486 \
    name imag_arr8_6 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_6 \
    op interface \
    ports { imag_arr8_6 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 487 \
    name imag_arr8_7 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_7 \
    op interface \
    ports { imag_arr8_7 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 488 \
    name imag_arr8_1 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_1 \
    op interface \
    ports { imag_arr8_1 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 489 \
    name imag_arr8_3 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_3 \
    op interface \
    ports { imag_arr8_3 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 490 \
    name imag_arr8_5 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_5 \
    op interface \
    ports { imag_arr8_5 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 491 \
    name imag_arr8_0 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_0 \
    op interface \
    ports { imag_arr8_0 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 492 \
    name imag_arr8_2 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_2 \
    op interface \
    ports { imag_arr8_2 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 493 \
    name imag_arr8_4 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_4 \
    op interface \
    ports { imag_arr8_4 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 494 \
    name real_arr8_1 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_1 \
    op interface \
    ports { real_arr8_1 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 495 \
    name real_arr8_3 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_3 \
    op interface \
    ports { real_arr8_3 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 496 \
    name real_arr8_5 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_5 \
    op interface \
    ports { real_arr8_5 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 497 \
    name real_arr8_0 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_0 \
    op interface \
    ports { real_arr8_0 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 498 \
    name real_arr8_2 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_2 \
    op interface \
    ports { real_arr8_2 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 499 \
    name real_arr8_4 \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_4 \
    op interface \
    ports { real_arr8_4 { I 32 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 500 \
    name imag_arr4_0 \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr4_0 \
    op interface \
    ports { imag_arr4_0 { O 32 vector } imag_arr4_0_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 501 \
    name real_arr4_0 \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr4_0 \
    op interface \
    ports { real_arr4_0 { O 32 vector } real_arr4_0_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 502 \
    name real_arr4_1 \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr4_1 \
    op interface \
    ports { real_arr4_1 { O 32 vector } real_arr4_1_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 503 \
    name real_arr4_2 \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr4_2 \
    op interface \
    ports { real_arr4_2 { O 32 vector } real_arr4_2_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 504 \
    name real_arr4_3 \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr4_3 \
    op interface \
    ports { real_arr4_3 { O 32 vector } real_arr4_3_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 505 \
    name imag_arr4_1 \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr4_1 \
    op interface \
    ports { imag_arr4_1 { O 32 vector } imag_arr4_1_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 506 \
    name imag_arr4_2 \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr4_2 \
    op interface \
    ports { imag_arr4_2 { O 32 vector } imag_arr4_2_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 507 \
    name imag_arr4_3 \
    type other \
    dir O \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr4_3 \
    op interface \
    ports { imag_arr4_3 { O 32 vector } imag_arr4_3_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


