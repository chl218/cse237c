# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 550 \
    name real_i_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_0 \
    op interface \
    ports { real_i_0_address0 { O 3 vector } real_i_0_ce0 { O 1 bit } real_i_0_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 551 \
    name real_i_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_1 \
    op interface \
    ports { real_i_1_address0 { O 3 vector } real_i_1_ce0 { O 1 bit } real_i_1_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 552 \
    name real_i_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_2 \
    op interface \
    ports { real_i_2_address0 { O 3 vector } real_i_2_ce0 { O 1 bit } real_i_2_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 553 \
    name real_i_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_3 \
    op interface \
    ports { real_i_3_address0 { O 3 vector } real_i_3_ce0 { O 1 bit } real_i_3_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 554 \
    name real_i_4 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_4 \
    op interface \
    ports { real_i_4_address0 { O 3 vector } real_i_4_ce0 { O 1 bit } real_i_4_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_4'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 555 \
    name real_i_5 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_5 \
    op interface \
    ports { real_i_5_address0 { O 3 vector } real_i_5_ce0 { O 1 bit } real_i_5_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_5'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 556 \
    name real_i_6 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_6 \
    op interface \
    ports { real_i_6_address0 { O 3 vector } real_i_6_ce0 { O 1 bit } real_i_6_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_6'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 557 \
    name real_i_7 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_7 \
    op interface \
    ports { real_i_7_address0 { O 3 vector } real_i_7_ce0 { O 1 bit } real_i_7_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_7'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 558 \
    name real_i_8 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_8 \
    op interface \
    ports { real_i_8_address0 { O 3 vector } real_i_8_ce0 { O 1 bit } real_i_8_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_8'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 559 \
    name real_i_9 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_9 \
    op interface \
    ports { real_i_9_address0 { O 3 vector } real_i_9_ce0 { O 1 bit } real_i_9_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_9'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 560 \
    name real_i_10 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_10 \
    op interface \
    ports { real_i_10_address0 { O 3 vector } real_i_10_ce0 { O 1 bit } real_i_10_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_10'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 561 \
    name real_i_11 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_11 \
    op interface \
    ports { real_i_11_address0 { O 3 vector } real_i_11_ce0 { O 1 bit } real_i_11_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_11'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 562 \
    name real_i_12 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_12 \
    op interface \
    ports { real_i_12_address0 { O 3 vector } real_i_12_ce0 { O 1 bit } real_i_12_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_12'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 563 \
    name real_i_13 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_13 \
    op interface \
    ports { real_i_13_address0 { O 3 vector } real_i_13_ce0 { O 1 bit } real_i_13_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_13'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 564 \
    name real_i_14 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_14 \
    op interface \
    ports { real_i_14_address0 { O 3 vector } real_i_14_ce0 { O 1 bit } real_i_14_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_14'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 565 \
    name real_i_15 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_15 \
    op interface \
    ports { real_i_15_address0 { O 3 vector } real_i_15_ce0 { O 1 bit } real_i_15_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_15'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 566 \
    name real_i_16 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_16 \
    op interface \
    ports { real_i_16_address0 { O 3 vector } real_i_16_ce0 { O 1 bit } real_i_16_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_16'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 567 \
    name real_i_17 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_17 \
    op interface \
    ports { real_i_17_address0 { O 3 vector } real_i_17_ce0 { O 1 bit } real_i_17_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_17'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 568 \
    name real_i_18 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_18 \
    op interface \
    ports { real_i_18_address0 { O 3 vector } real_i_18_ce0 { O 1 bit } real_i_18_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_18'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 569 \
    name real_i_19 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_19 \
    op interface \
    ports { real_i_19_address0 { O 3 vector } real_i_19_ce0 { O 1 bit } real_i_19_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_19'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 570 \
    name real_i_20 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_20 \
    op interface \
    ports { real_i_20_address0 { O 3 vector } real_i_20_ce0 { O 1 bit } real_i_20_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_20'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 571 \
    name real_i_21 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_21 \
    op interface \
    ports { real_i_21_address0 { O 3 vector } real_i_21_ce0 { O 1 bit } real_i_21_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_21'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 572 \
    name real_i_22 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_22 \
    op interface \
    ports { real_i_22_address0 { O 3 vector } real_i_22_ce0 { O 1 bit } real_i_22_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_22'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 573 \
    name real_i_23 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_23 \
    op interface \
    ports { real_i_23_address0 { O 3 vector } real_i_23_ce0 { O 1 bit } real_i_23_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_23'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 574 \
    name real_i_24 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_24 \
    op interface \
    ports { real_i_24_address0 { O 3 vector } real_i_24_ce0 { O 1 bit } real_i_24_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_24'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 575 \
    name real_i_25 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_25 \
    op interface \
    ports { real_i_25_address0 { O 3 vector } real_i_25_ce0 { O 1 bit } real_i_25_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_25'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 576 \
    name real_i_26 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_26 \
    op interface \
    ports { real_i_26_address0 { O 3 vector } real_i_26_ce0 { O 1 bit } real_i_26_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_26'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 577 \
    name real_i_27 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_27 \
    op interface \
    ports { real_i_27_address0 { O 3 vector } real_i_27_ce0 { O 1 bit } real_i_27_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_27'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 578 \
    name real_i_28 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_28 \
    op interface \
    ports { real_i_28_address0 { O 3 vector } real_i_28_ce0 { O 1 bit } real_i_28_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_28'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 579 \
    name real_i_29 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_29 \
    op interface \
    ports { real_i_29_address0 { O 3 vector } real_i_29_ce0 { O 1 bit } real_i_29_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_29'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 580 \
    name real_i_30 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_30 \
    op interface \
    ports { real_i_30_address0 { O 3 vector } real_i_30_ce0 { O 1 bit } real_i_30_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_30'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 581 \
    name real_i_31 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_31 \
    op interface \
    ports { real_i_31_address0 { O 3 vector } real_i_31_ce0 { O 1 bit } real_i_31_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_31'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 582 \
    name imag_i_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_0 \
    op interface \
    ports { imag_i_0_address0 { O 3 vector } imag_i_0_ce0 { O 1 bit } imag_i_0_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 583 \
    name imag_i_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_1 \
    op interface \
    ports { imag_i_1_address0 { O 3 vector } imag_i_1_ce0 { O 1 bit } imag_i_1_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 584 \
    name imag_i_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_2 \
    op interface \
    ports { imag_i_2_address0 { O 3 vector } imag_i_2_ce0 { O 1 bit } imag_i_2_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 585 \
    name imag_i_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_3 \
    op interface \
    ports { imag_i_3_address0 { O 3 vector } imag_i_3_ce0 { O 1 bit } imag_i_3_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 586 \
    name imag_i_4 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_4 \
    op interface \
    ports { imag_i_4_address0 { O 3 vector } imag_i_4_ce0 { O 1 bit } imag_i_4_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_4'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 587 \
    name imag_i_5 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_5 \
    op interface \
    ports { imag_i_5_address0 { O 3 vector } imag_i_5_ce0 { O 1 bit } imag_i_5_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_5'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 588 \
    name imag_i_6 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_6 \
    op interface \
    ports { imag_i_6_address0 { O 3 vector } imag_i_6_ce0 { O 1 bit } imag_i_6_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_6'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 589 \
    name imag_i_7 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_7 \
    op interface \
    ports { imag_i_7_address0 { O 3 vector } imag_i_7_ce0 { O 1 bit } imag_i_7_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_7'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 590 \
    name imag_i_8 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_8 \
    op interface \
    ports { imag_i_8_address0 { O 3 vector } imag_i_8_ce0 { O 1 bit } imag_i_8_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_8'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 591 \
    name imag_i_9 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_9 \
    op interface \
    ports { imag_i_9_address0 { O 3 vector } imag_i_9_ce0 { O 1 bit } imag_i_9_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_9'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 592 \
    name imag_i_10 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_10 \
    op interface \
    ports { imag_i_10_address0 { O 3 vector } imag_i_10_ce0 { O 1 bit } imag_i_10_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_10'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 593 \
    name imag_i_11 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_11 \
    op interface \
    ports { imag_i_11_address0 { O 3 vector } imag_i_11_ce0 { O 1 bit } imag_i_11_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_11'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 594 \
    name imag_i_12 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_12 \
    op interface \
    ports { imag_i_12_address0 { O 3 vector } imag_i_12_ce0 { O 1 bit } imag_i_12_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_12'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 595 \
    name imag_i_13 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_13 \
    op interface \
    ports { imag_i_13_address0 { O 3 vector } imag_i_13_ce0 { O 1 bit } imag_i_13_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_13'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 596 \
    name imag_i_14 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_14 \
    op interface \
    ports { imag_i_14_address0 { O 3 vector } imag_i_14_ce0 { O 1 bit } imag_i_14_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_14'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 597 \
    name imag_i_15 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_15 \
    op interface \
    ports { imag_i_15_address0 { O 3 vector } imag_i_15_ce0 { O 1 bit } imag_i_15_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_15'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 598 \
    name imag_i_16 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_16 \
    op interface \
    ports { imag_i_16_address0 { O 3 vector } imag_i_16_ce0 { O 1 bit } imag_i_16_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_16'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 599 \
    name imag_i_17 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_17 \
    op interface \
    ports { imag_i_17_address0 { O 3 vector } imag_i_17_ce0 { O 1 bit } imag_i_17_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_17'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 600 \
    name imag_i_18 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_18 \
    op interface \
    ports { imag_i_18_address0 { O 3 vector } imag_i_18_ce0 { O 1 bit } imag_i_18_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_18'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 601 \
    name imag_i_19 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_19 \
    op interface \
    ports { imag_i_19_address0 { O 3 vector } imag_i_19_ce0 { O 1 bit } imag_i_19_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_19'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 602 \
    name imag_i_20 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_20 \
    op interface \
    ports { imag_i_20_address0 { O 3 vector } imag_i_20_ce0 { O 1 bit } imag_i_20_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_20'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 603 \
    name imag_i_21 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_21 \
    op interface \
    ports { imag_i_21_address0 { O 3 vector } imag_i_21_ce0 { O 1 bit } imag_i_21_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_21'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 604 \
    name imag_i_22 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_22 \
    op interface \
    ports { imag_i_22_address0 { O 3 vector } imag_i_22_ce0 { O 1 bit } imag_i_22_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_22'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 605 \
    name imag_i_23 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_23 \
    op interface \
    ports { imag_i_23_address0 { O 3 vector } imag_i_23_ce0 { O 1 bit } imag_i_23_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_23'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 606 \
    name imag_i_24 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_24 \
    op interface \
    ports { imag_i_24_address0 { O 3 vector } imag_i_24_ce0 { O 1 bit } imag_i_24_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_24'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 607 \
    name imag_i_25 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_25 \
    op interface \
    ports { imag_i_25_address0 { O 3 vector } imag_i_25_ce0 { O 1 bit } imag_i_25_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_25'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 608 \
    name imag_i_26 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_26 \
    op interface \
    ports { imag_i_26_address0 { O 3 vector } imag_i_26_ce0 { O 1 bit } imag_i_26_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_26'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 609 \
    name imag_i_27 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_27 \
    op interface \
    ports { imag_i_27_address0 { O 3 vector } imag_i_27_ce0 { O 1 bit } imag_i_27_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_27'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 610 \
    name imag_i_28 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_28 \
    op interface \
    ports { imag_i_28_address0 { O 3 vector } imag_i_28_ce0 { O 1 bit } imag_i_28_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_28'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 611 \
    name imag_i_29 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_29 \
    op interface \
    ports { imag_i_29_address0 { O 3 vector } imag_i_29_ce0 { O 1 bit } imag_i_29_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_29'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 612 \
    name imag_i_30 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_30 \
    op interface \
    ports { imag_i_30_address0 { O 3 vector } imag_i_30_ce0 { O 1 bit } imag_i_30_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_30'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 613 \
    name imag_i_31 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_31 \
    op interface \
    ports { imag_i_31_address0 { O 3 vector } imag_i_31_ce0 { O 1 bit } imag_i_31_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_31'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 614 \
    name real_o \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o \
    op interface \
    ports { real_o_address0 { O 8 vector } real_o_ce0 { O 1 bit } real_o_we0 { O 1 bit } real_o_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 615 \
    name imag_o \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o \
    op interface \
    ports { imag_o_address0 { O 8 vector } imag_o_ce0 { O 1 bit } imag_o_we0 { O 1 bit } imag_o_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 616 \
    name real_arr256_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr256_0 \
    op interface \
    ports { real_arr256_0_address0 { O 6 vector } real_arr256_0_ce0 { O 1 bit } real_arr256_0_we0 { O 1 bit } real_arr256_0_d0 { O 32 vector } real_arr256_0_q0 { I 32 vector } real_arr256_0_address1 { O 6 vector } real_arr256_0_ce1 { O 1 bit } real_arr256_0_we1 { O 1 bit } real_arr256_0_d1 { O 32 vector } real_arr256_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr256_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 617 \
    name imag_arr256_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr256_0 \
    op interface \
    ports { imag_arr256_0_address0 { O 6 vector } imag_arr256_0_ce0 { O 1 bit } imag_arr256_0_we0 { O 1 bit } imag_arr256_0_d0 { O 32 vector } imag_arr256_0_q0 { I 32 vector } imag_arr256_0_address1 { O 6 vector } imag_arr256_0_ce1 { O 1 bit } imag_arr256_0_we1 { O 1 bit } imag_arr256_0_d1 { O 32 vector } imag_arr256_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr256_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 618 \
    name real_arr256_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr256_1 \
    op interface \
    ports { real_arr256_1_address0 { O 6 vector } real_arr256_1_ce0 { O 1 bit } real_arr256_1_we0 { O 1 bit } real_arr256_1_d0 { O 32 vector } real_arr256_1_q0 { I 32 vector } real_arr256_1_address1 { O 6 vector } real_arr256_1_ce1 { O 1 bit } real_arr256_1_we1 { O 1 bit } real_arr256_1_d1 { O 32 vector } real_arr256_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr256_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 619 \
    name imag_arr256_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr256_1 \
    op interface \
    ports { imag_arr256_1_address0 { O 6 vector } imag_arr256_1_ce0 { O 1 bit } imag_arr256_1_we0 { O 1 bit } imag_arr256_1_d0 { O 32 vector } imag_arr256_1_q0 { I 32 vector } imag_arr256_1_address1 { O 6 vector } imag_arr256_1_ce1 { O 1 bit } imag_arr256_1_we1 { O 1 bit } imag_arr256_1_d1 { O 32 vector } imag_arr256_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr256_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 620 \
    name real_arr256_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr256_2 \
    op interface \
    ports { real_arr256_2_address0 { O 6 vector } real_arr256_2_ce0 { O 1 bit } real_arr256_2_we0 { O 1 bit } real_arr256_2_d0 { O 32 vector } real_arr256_2_q0 { I 32 vector } real_arr256_2_address1 { O 6 vector } real_arr256_2_ce1 { O 1 bit } real_arr256_2_we1 { O 1 bit } real_arr256_2_d1 { O 32 vector } real_arr256_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr256_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 621 \
    name imag_arr256_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr256_2 \
    op interface \
    ports { imag_arr256_2_address0 { O 6 vector } imag_arr256_2_ce0 { O 1 bit } imag_arr256_2_we0 { O 1 bit } imag_arr256_2_d0 { O 32 vector } imag_arr256_2_q0 { I 32 vector } imag_arr256_2_address1 { O 6 vector } imag_arr256_2_ce1 { O 1 bit } imag_arr256_2_we1 { O 1 bit } imag_arr256_2_d1 { O 32 vector } imag_arr256_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr256_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 622 \
    name real_arr256_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr256_3 \
    op interface \
    ports { real_arr256_3_address0 { O 6 vector } real_arr256_3_ce0 { O 1 bit } real_arr256_3_we0 { O 1 bit } real_arr256_3_d0 { O 32 vector } real_arr256_3_q0 { I 32 vector } real_arr256_3_address1 { O 6 vector } real_arr256_3_ce1 { O 1 bit } real_arr256_3_we1 { O 1 bit } real_arr256_3_d1 { O 32 vector } real_arr256_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr256_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 623 \
    name imag_arr256_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr256_3 \
    op interface \
    ports { imag_arr256_3_address0 { O 6 vector } imag_arr256_3_ce0 { O 1 bit } imag_arr256_3_we0 { O 1 bit } imag_arr256_3_d0 { O 32 vector } imag_arr256_3_q0 { I 32 vector } imag_arr256_3_address1 { O 6 vector } imag_arr256_3_ce1 { O 1 bit } imag_arr256_3_we1 { O 1 bit } imag_arr256_3_d1 { O 32 vector } imag_arr256_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr256_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 624 \
    name real_arr128_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr128_0 \
    op interface \
    ports { real_arr128_0_address0 { O 5 vector } real_arr128_0_ce0 { O 1 bit } real_arr128_0_we0 { O 1 bit } real_arr128_0_d0 { O 32 vector } real_arr128_0_q0 { I 32 vector } real_arr128_0_address1 { O 5 vector } real_arr128_0_ce1 { O 1 bit } real_arr128_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr128_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 625 \
    name imag_arr128_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr128_0 \
    op interface \
    ports { imag_arr128_0_address0 { O 5 vector } imag_arr128_0_ce0 { O 1 bit } imag_arr128_0_we0 { O 1 bit } imag_arr128_0_d0 { O 32 vector } imag_arr128_0_q0 { I 32 vector } imag_arr128_0_address1 { O 5 vector } imag_arr128_0_ce1 { O 1 bit } imag_arr128_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr128_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 626 \
    name real_arr128_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr128_1 \
    op interface \
    ports { real_arr128_1_address0 { O 5 vector } real_arr128_1_ce0 { O 1 bit } real_arr128_1_we0 { O 1 bit } real_arr128_1_d0 { O 32 vector } real_arr128_1_q0 { I 32 vector } real_arr128_1_address1 { O 5 vector } real_arr128_1_ce1 { O 1 bit } real_arr128_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr128_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 627 \
    name imag_arr128_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr128_1 \
    op interface \
    ports { imag_arr128_1_address0 { O 5 vector } imag_arr128_1_ce0 { O 1 bit } imag_arr128_1_we0 { O 1 bit } imag_arr128_1_d0 { O 32 vector } imag_arr128_1_q0 { I 32 vector } imag_arr128_1_address1 { O 5 vector } imag_arr128_1_ce1 { O 1 bit } imag_arr128_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr128_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 628 \
    name real_arr128_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr128_2 \
    op interface \
    ports { real_arr128_2_address0 { O 5 vector } real_arr128_2_ce0 { O 1 bit } real_arr128_2_we0 { O 1 bit } real_arr128_2_d0 { O 32 vector } real_arr128_2_q0 { I 32 vector } real_arr128_2_address1 { O 5 vector } real_arr128_2_ce1 { O 1 bit } real_arr128_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr128_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 629 \
    name imag_arr128_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr128_2 \
    op interface \
    ports { imag_arr128_2_address0 { O 5 vector } imag_arr128_2_ce0 { O 1 bit } imag_arr128_2_we0 { O 1 bit } imag_arr128_2_d0 { O 32 vector } imag_arr128_2_q0 { I 32 vector } imag_arr128_2_address1 { O 5 vector } imag_arr128_2_ce1 { O 1 bit } imag_arr128_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr128_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 630 \
    name real_arr128_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr128_3 \
    op interface \
    ports { real_arr128_3_address0 { O 5 vector } real_arr128_3_ce0 { O 1 bit } real_arr128_3_we0 { O 1 bit } real_arr128_3_d0 { O 32 vector } real_arr128_3_q0 { I 32 vector } real_arr128_3_address1 { O 5 vector } real_arr128_3_ce1 { O 1 bit } real_arr128_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr128_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 631 \
    name imag_arr128_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr128_3 \
    op interface \
    ports { imag_arr128_3_address0 { O 5 vector } imag_arr128_3_ce0 { O 1 bit } imag_arr128_3_we0 { O 1 bit } imag_arr128_3_d0 { O 32 vector } imag_arr128_3_q0 { I 32 vector } imag_arr128_3_address1 { O 5 vector } imag_arr128_3_ce1 { O 1 bit } imag_arr128_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr128_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 632 \
    name real_arr64_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr64_0 \
    op interface \
    ports { real_arr64_0_address0 { O 4 vector } real_arr64_0_ce0 { O 1 bit } real_arr64_0_we0 { O 1 bit } real_arr64_0_d0 { O 32 vector } real_arr64_0_q0 { I 32 vector } real_arr64_0_address1 { O 4 vector } real_arr64_0_ce1 { O 1 bit } real_arr64_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr64_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 633 \
    name imag_arr64_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr64_0 \
    op interface \
    ports { imag_arr64_0_address0 { O 4 vector } imag_arr64_0_ce0 { O 1 bit } imag_arr64_0_we0 { O 1 bit } imag_arr64_0_d0 { O 32 vector } imag_arr64_0_q0 { I 32 vector } imag_arr64_0_address1 { O 4 vector } imag_arr64_0_ce1 { O 1 bit } imag_arr64_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr64_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 634 \
    name real_arr64_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr64_1 \
    op interface \
    ports { real_arr64_1_address0 { O 4 vector } real_arr64_1_ce0 { O 1 bit } real_arr64_1_we0 { O 1 bit } real_arr64_1_d0 { O 32 vector } real_arr64_1_q0 { I 32 vector } real_arr64_1_address1 { O 4 vector } real_arr64_1_ce1 { O 1 bit } real_arr64_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr64_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 635 \
    name imag_arr64_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr64_1 \
    op interface \
    ports { imag_arr64_1_address0 { O 4 vector } imag_arr64_1_ce0 { O 1 bit } imag_arr64_1_we0 { O 1 bit } imag_arr64_1_d0 { O 32 vector } imag_arr64_1_q0 { I 32 vector } imag_arr64_1_address1 { O 4 vector } imag_arr64_1_ce1 { O 1 bit } imag_arr64_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr64_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 636 \
    name real_arr64_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr64_2 \
    op interface \
    ports { real_arr64_2_address0 { O 4 vector } real_arr64_2_ce0 { O 1 bit } real_arr64_2_we0 { O 1 bit } real_arr64_2_d0 { O 32 vector } real_arr64_2_q0 { I 32 vector } real_arr64_2_address1 { O 4 vector } real_arr64_2_ce1 { O 1 bit } real_arr64_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr64_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 637 \
    name imag_arr64_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr64_2 \
    op interface \
    ports { imag_arr64_2_address0 { O 4 vector } imag_arr64_2_ce0 { O 1 bit } imag_arr64_2_we0 { O 1 bit } imag_arr64_2_d0 { O 32 vector } imag_arr64_2_q0 { I 32 vector } imag_arr64_2_address1 { O 4 vector } imag_arr64_2_ce1 { O 1 bit } imag_arr64_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr64_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 638 \
    name real_arr64_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr64_3 \
    op interface \
    ports { real_arr64_3_address0 { O 4 vector } real_arr64_3_ce0 { O 1 bit } real_arr64_3_we0 { O 1 bit } real_arr64_3_d0 { O 32 vector } real_arr64_3_q0 { I 32 vector } real_arr64_3_address1 { O 4 vector } real_arr64_3_ce1 { O 1 bit } real_arr64_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr64_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 639 \
    name imag_arr64_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr64_3 \
    op interface \
    ports { imag_arr64_3_address0 { O 4 vector } imag_arr64_3_ce0 { O 1 bit } imag_arr64_3_we0 { O 1 bit } imag_arr64_3_d0 { O 32 vector } imag_arr64_3_q0 { I 32 vector } imag_arr64_3_address1 { O 4 vector } imag_arr64_3_ce1 { O 1 bit } imag_arr64_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr64_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 640 \
    name real_arr32_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr32_0 \
    op interface \
    ports { real_arr32_0_address0 { O 3 vector } real_arr32_0_ce0 { O 1 bit } real_arr32_0_we0 { O 1 bit } real_arr32_0_d0 { O 32 vector } real_arr32_0_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr32_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 641 \
    name imag_arr32_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr32_0 \
    op interface \
    ports { imag_arr32_0_address0 { O 3 vector } imag_arr32_0_ce0 { O 1 bit } imag_arr32_0_we0 { O 1 bit } imag_arr32_0_d0 { O 32 vector } imag_arr32_0_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr32_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 642 \
    name real_arr32_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr32_1 \
    op interface \
    ports { real_arr32_1_address0 { O 3 vector } real_arr32_1_ce0 { O 1 bit } real_arr32_1_we0 { O 1 bit } real_arr32_1_d0 { O 32 vector } real_arr32_1_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr32_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 643 \
    name imag_arr32_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr32_1 \
    op interface \
    ports { imag_arr32_1_address0 { O 3 vector } imag_arr32_1_ce0 { O 1 bit } imag_arr32_1_we0 { O 1 bit } imag_arr32_1_d0 { O 32 vector } imag_arr32_1_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr32_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 644 \
    name real_arr32_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr32_2 \
    op interface \
    ports { real_arr32_2_address0 { O 3 vector } real_arr32_2_ce0 { O 1 bit } real_arr32_2_we0 { O 1 bit } real_arr32_2_d0 { O 32 vector } real_arr32_2_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr32_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 645 \
    name imag_arr32_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr32_2 \
    op interface \
    ports { imag_arr32_2_address0 { O 3 vector } imag_arr32_2_ce0 { O 1 bit } imag_arr32_2_we0 { O 1 bit } imag_arr32_2_d0 { O 32 vector } imag_arr32_2_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr32_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 646 \
    name real_arr32_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr32_3 \
    op interface \
    ports { real_arr32_3_address0 { O 3 vector } real_arr32_3_ce0 { O 1 bit } real_arr32_3_we0 { O 1 bit } real_arr32_3_d0 { O 32 vector } real_arr32_3_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr32_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 647 \
    name imag_arr32_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr32_3 \
    op interface \
    ports { imag_arr32_3_address0 { O 3 vector } imag_arr32_3_ce0 { O 1 bit } imag_arr32_3_we0 { O 1 bit } imag_arr32_3_d0 { O 32 vector } imag_arr32_3_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr32_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 648 \
    name imag_arr16_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr16_1 \
    op interface \
    ports { imag_arr16_1_address0 { O 2 vector } imag_arr16_1_ce0 { O 1 bit } imag_arr16_1_we0 { O 1 bit } imag_arr16_1_d0 { O 32 vector } imag_arr16_1_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr16_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 649 \
    name real_arr16_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr16_1 \
    op interface \
    ports { real_arr16_1_address0 { O 2 vector } real_arr16_1_ce0 { O 1 bit } real_arr16_1_we0 { O 1 bit } real_arr16_1_d0 { O 32 vector } real_arr16_1_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr16_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 650 \
    name imag_arr16_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr16_0 \
    op interface \
    ports { imag_arr16_0_address0 { O 2 vector } imag_arr16_0_ce0 { O 1 bit } imag_arr16_0_we0 { O 1 bit } imag_arr16_0_d0 { O 32 vector } imag_arr16_0_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr16_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 651 \
    name real_arr16_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr16_0 \
    op interface \
    ports { real_arr16_0_address0 { O 2 vector } real_arr16_0_ce0 { O 1 bit } real_arr16_0_we0 { O 1 bit } real_arr16_0_d0 { O 32 vector } real_arr16_0_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr16_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 652 \
    name real_arr16_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr16_2 \
    op interface \
    ports { real_arr16_2_address0 { O 2 vector } real_arr16_2_ce0 { O 1 bit } real_arr16_2_we0 { O 1 bit } real_arr16_2_d0 { O 32 vector } real_arr16_2_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr16_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 653 \
    name imag_arr16_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr16_2 \
    op interface \
    ports { imag_arr16_2_address0 { O 2 vector } imag_arr16_2_ce0 { O 1 bit } imag_arr16_2_we0 { O 1 bit } imag_arr16_2_d0 { O 32 vector } imag_arr16_2_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr16_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 654 \
    name real_arr16_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr16_3 \
    op interface \
    ports { real_arr16_3_address0 { O 2 vector } real_arr16_3_ce0 { O 1 bit } real_arr16_3_we0 { O 1 bit } real_arr16_3_d0 { O 32 vector } real_arr16_3_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr16_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 655 \
    name imag_arr16_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr16_3 \
    op interface \
    ports { imag_arr16_3_address0 { O 2 vector } imag_arr16_3_ce0 { O 1 bit } imag_arr16_3_we0 { O 1 bit } imag_arr16_3_d0 { O 32 vector } imag_arr16_3_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr16_3'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 656 \
    name imag_arr8_1 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_1 \
    op interface \
    ports { imag_arr8_1_i { I 32 vector } imag_arr8_1_o { O 32 vector } imag_arr8_1_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 657 \
    name real_arr8_1 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_1 \
    op interface \
    ports { real_arr8_1_i { I 32 vector } real_arr8_1_o { O 32 vector } real_arr8_1_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 658 \
    name imag_arr8_0 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_0 \
    op interface \
    ports { imag_arr8_0_i { I 32 vector } imag_arr8_0_o { O 32 vector } imag_arr8_0_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 659 \
    name real_arr8_0 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_0 \
    op interface \
    ports { real_arr8_0_i { I 32 vector } real_arr8_0_o { O 32 vector } real_arr8_0_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 660 \
    name real_arr8_2 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_2 \
    op interface \
    ports { real_arr8_2_i { I 32 vector } real_arr8_2_o { O 32 vector } real_arr8_2_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 661 \
    name real_arr8_4 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_4 \
    op interface \
    ports { real_arr8_4_i { I 32 vector } real_arr8_4_o { O 32 vector } real_arr8_4_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 662 \
    name real_arr8_6 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_6 \
    op interface \
    ports { real_arr8_6_i { I 32 vector } real_arr8_6_o { O 32 vector } real_arr8_6_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 663 \
    name imag_arr8_2 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_2 \
    op interface \
    ports { imag_arr8_2_i { I 32 vector } imag_arr8_2_o { O 32 vector } imag_arr8_2_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 664 \
    name imag_arr8_4 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_4 \
    op interface \
    ports { imag_arr8_4_i { I 32 vector } imag_arr8_4_o { O 32 vector } imag_arr8_4_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 665 \
    name imag_arr8_6 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_6 \
    op interface \
    ports { imag_arr8_6_i { I 32 vector } imag_arr8_6_o { O 32 vector } imag_arr8_6_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 666 \
    name real_arr8_3 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_3 \
    op interface \
    ports { real_arr8_3_i { I 32 vector } real_arr8_3_o { O 32 vector } real_arr8_3_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 667 \
    name real_arr8_5 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_5 \
    op interface \
    ports { real_arr8_5_i { I 32 vector } real_arr8_5_o { O 32 vector } real_arr8_5_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 668 \
    name real_arr8_7 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_7 \
    op interface \
    ports { real_arr8_7_i { I 32 vector } real_arr8_7_o { O 32 vector } real_arr8_7_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 669 \
    name imag_arr8_3 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_3 \
    op interface \
    ports { imag_arr8_3_i { I 32 vector } imag_arr8_3_o { O 32 vector } imag_arr8_3_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 670 \
    name imag_arr8_5 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_5 \
    op interface \
    ports { imag_arr8_5_i { I 32 vector } imag_arr8_5_o { O 32 vector } imag_arr8_5_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 671 \
    name imag_arr8_7 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_7 \
    op interface \
    ports { imag_arr8_7_i { I 32 vector } imag_arr8_7_o { O 32 vector } imag_arr8_7_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 672 \
    name imag_arr4_0 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr4_0 \
    op interface \
    ports { imag_arr4_0_i { I 32 vector } imag_arr4_0_o { O 32 vector } imag_arr4_0_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 673 \
    name real_arr4_0 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr4_0 \
    op interface \
    ports { real_arr4_0_i { I 32 vector } real_arr4_0_o { O 32 vector } real_arr4_0_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 674 \
    name real_arr4_1 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr4_1 \
    op interface \
    ports { real_arr4_1_i { I 32 vector } real_arr4_1_o { O 32 vector } real_arr4_1_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 675 \
    name real_arr4_2 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr4_2 \
    op interface \
    ports { real_arr4_2_i { I 32 vector } real_arr4_2_o { O 32 vector } real_arr4_2_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 676 \
    name real_arr4_3 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr4_3 \
    op interface \
    ports { real_arr4_3_i { I 32 vector } real_arr4_3_o { O 32 vector } real_arr4_3_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 677 \
    name imag_arr4_1 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr4_1 \
    op interface \
    ports { imag_arr4_1_i { I 32 vector } imag_arr4_1_o { O 32 vector } imag_arr4_1_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 678 \
    name imag_arr4_2 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr4_2 \
    op interface \
    ports { imag_arr4_2_i { I 32 vector } imag_arr4_2_o { O 32 vector } imag_arr4_2_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 679 \
    name imag_arr4_3 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr4_3 \
    op interface \
    ports { imag_arr4_3_i { I 32 vector } imag_arr4_3_o { O 32 vector } imag_arr4_3_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


