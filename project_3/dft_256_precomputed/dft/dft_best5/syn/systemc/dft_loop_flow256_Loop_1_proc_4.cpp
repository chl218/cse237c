#include "dft_loop_flow256_Loop_1_proc.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void dft_loop_flow256_Loop_1_proc::thread_sct_20_address7() {
    sct_20_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_20_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_20_ce0 = ap_const_logic_1;
    } else {
        sct_20_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_20_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_20_ce1 = ap_const_logic_1;
    } else {
        sct_20_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_20_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_20_ce2 = ap_const_logic_1;
    } else {
        sct_20_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_20_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_20_ce3 = ap_const_logic_1;
    } else {
        sct_20_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_20_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_20_ce4 = ap_const_logic_1;
    } else {
        sct_20_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_20_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_20_ce5 = ap_const_logic_1;
    } else {
        sct_20_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_20_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_20_ce6 = ap_const_logic_1;
    } else {
        sct_20_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_20_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_20_ce7 = ap_const_logic_1;
    } else {
        sct_20_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_21_address0() {
    sct_21_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_21_address1() {
    sct_21_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_21_address2() {
    sct_21_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_21_address3() {
    sct_21_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_21_address4() {
    sct_21_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_21_address5() {
    sct_21_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_21_address6() {
    sct_21_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_21_address7() {
    sct_21_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_21_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_21_ce0 = ap_const_logic_1;
    } else {
        sct_21_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_21_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_21_ce1 = ap_const_logic_1;
    } else {
        sct_21_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_21_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_21_ce2 = ap_const_logic_1;
    } else {
        sct_21_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_21_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_21_ce3 = ap_const_logic_1;
    } else {
        sct_21_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_21_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_21_ce4 = ap_const_logic_1;
    } else {
        sct_21_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_21_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_21_ce5 = ap_const_logic_1;
    } else {
        sct_21_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_21_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_21_ce6 = ap_const_logic_1;
    } else {
        sct_21_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_21_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_21_ce7 = ap_const_logic_1;
    } else {
        sct_21_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_22_address0() {
    sct_22_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_22_address1() {
    sct_22_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_22_address2() {
    sct_22_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_22_address3() {
    sct_22_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_22_address4() {
    sct_22_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_22_address5() {
    sct_22_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_22_address6() {
    sct_22_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_22_address7() {
    sct_22_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_22_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_22_ce0 = ap_const_logic_1;
    } else {
        sct_22_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_22_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_22_ce1 = ap_const_logic_1;
    } else {
        sct_22_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_22_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_22_ce2 = ap_const_logic_1;
    } else {
        sct_22_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_22_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_22_ce3 = ap_const_logic_1;
    } else {
        sct_22_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_22_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_22_ce4 = ap_const_logic_1;
    } else {
        sct_22_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_22_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_22_ce5 = ap_const_logic_1;
    } else {
        sct_22_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_22_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_22_ce6 = ap_const_logic_1;
    } else {
        sct_22_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_22_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_22_ce7 = ap_const_logic_1;
    } else {
        sct_22_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_23_address0() {
    sct_23_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_23_address1() {
    sct_23_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_23_address2() {
    sct_23_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_23_address3() {
    sct_23_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_23_address4() {
    sct_23_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_23_address5() {
    sct_23_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_23_address6() {
    sct_23_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_23_address7() {
    sct_23_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_23_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_23_ce0 = ap_const_logic_1;
    } else {
        sct_23_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_23_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_23_ce1 = ap_const_logic_1;
    } else {
        sct_23_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_23_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_23_ce2 = ap_const_logic_1;
    } else {
        sct_23_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_23_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_23_ce3 = ap_const_logic_1;
    } else {
        sct_23_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_23_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_23_ce4 = ap_const_logic_1;
    } else {
        sct_23_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_23_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_23_ce5 = ap_const_logic_1;
    } else {
        sct_23_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_23_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_23_ce6 = ap_const_logic_1;
    } else {
        sct_23_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_23_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_23_ce7 = ap_const_logic_1;
    } else {
        sct_23_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_24_address0() {
    sct_24_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_24_address1() {
    sct_24_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_24_address2() {
    sct_24_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_24_address3() {
    sct_24_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_24_address4() {
    sct_24_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_24_address5() {
    sct_24_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_24_address6() {
    sct_24_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_24_address7() {
    sct_24_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_24_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_24_ce0 = ap_const_logic_1;
    } else {
        sct_24_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_24_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_24_ce1 = ap_const_logic_1;
    } else {
        sct_24_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_24_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_24_ce2 = ap_const_logic_1;
    } else {
        sct_24_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_24_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_24_ce3 = ap_const_logic_1;
    } else {
        sct_24_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_24_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_24_ce4 = ap_const_logic_1;
    } else {
        sct_24_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_24_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_24_ce5 = ap_const_logic_1;
    } else {
        sct_24_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_24_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_24_ce6 = ap_const_logic_1;
    } else {
        sct_24_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_24_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_24_ce7 = ap_const_logic_1;
    } else {
        sct_24_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_25_address0() {
    sct_25_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_25_address1() {
    sct_25_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_25_address2() {
    sct_25_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_25_address3() {
    sct_25_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_25_address4() {
    sct_25_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_25_address5() {
    sct_25_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_25_address6() {
    sct_25_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_25_address7() {
    sct_25_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_25_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_25_ce0 = ap_const_logic_1;
    } else {
        sct_25_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_25_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_25_ce1 = ap_const_logic_1;
    } else {
        sct_25_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_25_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_25_ce2 = ap_const_logic_1;
    } else {
        sct_25_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_25_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_25_ce3 = ap_const_logic_1;
    } else {
        sct_25_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_25_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_25_ce4 = ap_const_logic_1;
    } else {
        sct_25_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_25_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_25_ce5 = ap_const_logic_1;
    } else {
        sct_25_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_25_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_25_ce6 = ap_const_logic_1;
    } else {
        sct_25_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_25_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_25_ce7 = ap_const_logic_1;
    } else {
        sct_25_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_26_address0() {
    sct_26_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_26_address1() {
    sct_26_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_26_address2() {
    sct_26_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_26_address3() {
    sct_26_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_26_address4() {
    sct_26_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_26_address5() {
    sct_26_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_26_address6() {
    sct_26_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_26_address7() {
    sct_26_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_26_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_26_ce0 = ap_const_logic_1;
    } else {
        sct_26_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_26_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_26_ce1 = ap_const_logic_1;
    } else {
        sct_26_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_26_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_26_ce2 = ap_const_logic_1;
    } else {
        sct_26_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_26_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_26_ce3 = ap_const_logic_1;
    } else {
        sct_26_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_26_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_26_ce4 = ap_const_logic_1;
    } else {
        sct_26_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_26_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_26_ce5 = ap_const_logic_1;
    } else {
        sct_26_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_26_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_26_ce6 = ap_const_logic_1;
    } else {
        sct_26_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_26_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_26_ce7 = ap_const_logic_1;
    } else {
        sct_26_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_27_address0() {
    sct_27_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_27_address1() {
    sct_27_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_27_address2() {
    sct_27_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_27_address3() {
    sct_27_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_27_address4() {
    sct_27_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_27_address5() {
    sct_27_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_27_address6() {
    sct_27_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_27_address7() {
    sct_27_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_27_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_27_ce0 = ap_const_logic_1;
    } else {
        sct_27_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_27_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_27_ce1 = ap_const_logic_1;
    } else {
        sct_27_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_27_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_27_ce2 = ap_const_logic_1;
    } else {
        sct_27_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_27_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_27_ce3 = ap_const_logic_1;
    } else {
        sct_27_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_27_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_27_ce4 = ap_const_logic_1;
    } else {
        sct_27_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_27_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_27_ce5 = ap_const_logic_1;
    } else {
        sct_27_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_27_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_27_ce6 = ap_const_logic_1;
    } else {
        sct_27_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_27_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_27_ce7 = ap_const_logic_1;
    } else {
        sct_27_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_28_address0() {
    sct_28_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_28_address1() {
    sct_28_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_28_address2() {
    sct_28_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_28_address3() {
    sct_28_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_28_address4() {
    sct_28_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_28_address5() {
    sct_28_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_28_address6() {
    sct_28_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_28_address7() {
    sct_28_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_28_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_28_ce0 = ap_const_logic_1;
    } else {
        sct_28_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_28_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_28_ce1 = ap_const_logic_1;
    } else {
        sct_28_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_28_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_28_ce2 = ap_const_logic_1;
    } else {
        sct_28_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_28_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_28_ce3 = ap_const_logic_1;
    } else {
        sct_28_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_28_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_28_ce4 = ap_const_logic_1;
    } else {
        sct_28_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_28_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_28_ce5 = ap_const_logic_1;
    } else {
        sct_28_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_28_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_28_ce6 = ap_const_logic_1;
    } else {
        sct_28_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_28_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_28_ce7 = ap_const_logic_1;
    } else {
        sct_28_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_29_address0() {
    sct_29_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_29_address1() {
    sct_29_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_29_address2() {
    sct_29_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_29_address3() {
    sct_29_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_29_address4() {
    sct_29_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_29_address5() {
    sct_29_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_29_address6() {
    sct_29_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_29_address7() {
    sct_29_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_29_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_29_ce0 = ap_const_logic_1;
    } else {
        sct_29_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_29_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_29_ce1 = ap_const_logic_1;
    } else {
        sct_29_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_29_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_29_ce2 = ap_const_logic_1;
    } else {
        sct_29_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_29_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_29_ce3 = ap_const_logic_1;
    } else {
        sct_29_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_29_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_29_ce4 = ap_const_logic_1;
    } else {
        sct_29_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_29_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_29_ce5 = ap_const_logic_1;
    } else {
        sct_29_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_29_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_29_ce6 = ap_const_logic_1;
    } else {
        sct_29_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_29_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_29_ce7 = ap_const_logic_1;
    } else {
        sct_29_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_2_address0() {
    sct_2_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_2_address1() {
    sct_2_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_2_address2() {
    sct_2_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_2_address3() {
    sct_2_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_2_address4() {
    sct_2_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_2_address5() {
    sct_2_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_2_address6() {
    sct_2_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_2_address7() {
    sct_2_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_2_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_2_ce0 = ap_const_logic_1;
    } else {
        sct_2_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_2_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_2_ce1 = ap_const_logic_1;
    } else {
        sct_2_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_2_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_2_ce2 = ap_const_logic_1;
    } else {
        sct_2_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_2_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_2_ce3 = ap_const_logic_1;
    } else {
        sct_2_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_2_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_2_ce4 = ap_const_logic_1;
    } else {
        sct_2_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_2_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_2_ce5 = ap_const_logic_1;
    } else {
        sct_2_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_2_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_2_ce6 = ap_const_logic_1;
    } else {
        sct_2_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_2_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_2_ce7 = ap_const_logic_1;
    } else {
        sct_2_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_30_address0() {
    sct_30_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_30_address1() {
    sct_30_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_30_address2() {
    sct_30_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_30_address3() {
    sct_30_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_30_address4() {
    sct_30_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_30_address5() {
    sct_30_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_30_address6() {
    sct_30_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_30_address7() {
    sct_30_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_30_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_30_ce0 = ap_const_logic_1;
    } else {
        sct_30_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_30_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_30_ce1 = ap_const_logic_1;
    } else {
        sct_30_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_30_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_30_ce2 = ap_const_logic_1;
    } else {
        sct_30_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_30_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_30_ce3 = ap_const_logic_1;
    } else {
        sct_30_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_30_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_30_ce4 = ap_const_logic_1;
    } else {
        sct_30_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_30_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_30_ce5 = ap_const_logic_1;
    } else {
        sct_30_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_30_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_30_ce6 = ap_const_logic_1;
    } else {
        sct_30_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_30_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_30_ce7 = ap_const_logic_1;
    } else {
        sct_30_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_31_address0() {
    sct_31_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_31_address1() {
    sct_31_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_31_address2() {
    sct_31_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_31_address3() {
    sct_31_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_31_address4() {
    sct_31_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_31_address5() {
    sct_31_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_31_address6() {
    sct_31_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_31_address7() {
    sct_31_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_31_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_31_ce0 = ap_const_logic_1;
    } else {
        sct_31_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_31_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_31_ce1 = ap_const_logic_1;
    } else {
        sct_31_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_31_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_31_ce2 = ap_const_logic_1;
    } else {
        sct_31_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_31_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_31_ce3 = ap_const_logic_1;
    } else {
        sct_31_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_31_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_31_ce4 = ap_const_logic_1;
    } else {
        sct_31_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_31_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_31_ce5 = ap_const_logic_1;
    } else {
        sct_31_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_31_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_31_ce6 = ap_const_logic_1;
    } else {
        sct_31_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_31_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_31_ce7 = ap_const_logic_1;
    } else {
        sct_31_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_3_address0() {
    sct_3_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_3_address1() {
    sct_3_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_3_address2() {
    sct_3_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_3_address3() {
    sct_3_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_3_address4() {
    sct_3_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_3_address5() {
    sct_3_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_3_address6() {
    sct_3_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_3_address7() {
    sct_3_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_3_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_3_ce0 = ap_const_logic_1;
    } else {
        sct_3_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_3_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_3_ce1 = ap_const_logic_1;
    } else {
        sct_3_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_3_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_3_ce2 = ap_const_logic_1;
    } else {
        sct_3_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_3_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_3_ce3 = ap_const_logic_1;
    } else {
        sct_3_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_3_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_3_ce4 = ap_const_logic_1;
    } else {
        sct_3_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_3_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_3_ce5 = ap_const_logic_1;
    } else {
        sct_3_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_3_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_3_ce6 = ap_const_logic_1;
    } else {
        sct_3_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_3_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_3_ce7 = ap_const_logic_1;
    } else {
        sct_3_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_4_address0() {
    sct_4_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_4_address1() {
    sct_4_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_4_address2() {
    sct_4_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_4_address3() {
    sct_4_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_4_address4() {
    sct_4_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_4_address5() {
    sct_4_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_4_address6() {
    sct_4_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_4_address7() {
    sct_4_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_4_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_4_ce0 = ap_const_logic_1;
    } else {
        sct_4_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_4_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_4_ce1 = ap_const_logic_1;
    } else {
        sct_4_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_4_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_4_ce2 = ap_const_logic_1;
    } else {
        sct_4_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_4_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_4_ce3 = ap_const_logic_1;
    } else {
        sct_4_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_4_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_4_ce4 = ap_const_logic_1;
    } else {
        sct_4_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_4_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_4_ce5 = ap_const_logic_1;
    } else {
        sct_4_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_4_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_4_ce6 = ap_const_logic_1;
    } else {
        sct_4_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_4_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_4_ce7 = ap_const_logic_1;
    } else {
        sct_4_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_5_address0() {
    sct_5_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_5_address1() {
    sct_5_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_5_address2() {
    sct_5_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_5_address3() {
    sct_5_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_5_address4() {
    sct_5_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_5_address5() {
    sct_5_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_5_address6() {
    sct_5_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_5_address7() {
    sct_5_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_5_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_5_ce0 = ap_const_logic_1;
    } else {
        sct_5_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_5_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_5_ce1 = ap_const_logic_1;
    } else {
        sct_5_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_5_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_5_ce2 = ap_const_logic_1;
    } else {
        sct_5_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_5_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_5_ce3 = ap_const_logic_1;
    } else {
        sct_5_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_5_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_5_ce4 = ap_const_logic_1;
    } else {
        sct_5_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_5_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_5_ce5 = ap_const_logic_1;
    } else {
        sct_5_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_5_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_5_ce6 = ap_const_logic_1;
    } else {
        sct_5_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_5_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_5_ce7 = ap_const_logic_1;
    } else {
        sct_5_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_6_address0() {
    sct_6_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_6_address1() {
    sct_6_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_6_address2() {
    sct_6_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_6_address3() {
    sct_6_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_6_address4() {
    sct_6_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_6_address5() {
    sct_6_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_6_address6() {
    sct_6_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_6_address7() {
    sct_6_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_6_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_6_ce0 = ap_const_logic_1;
    } else {
        sct_6_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_6_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_6_ce1 = ap_const_logic_1;
    } else {
        sct_6_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_6_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_6_ce2 = ap_const_logic_1;
    } else {
        sct_6_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_6_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_6_ce3 = ap_const_logic_1;
    } else {
        sct_6_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_6_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_6_ce4 = ap_const_logic_1;
    } else {
        sct_6_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_6_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_6_ce5 = ap_const_logic_1;
    } else {
        sct_6_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_6_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_6_ce6 = ap_const_logic_1;
    } else {
        sct_6_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_6_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_6_ce7 = ap_const_logic_1;
    } else {
        sct_6_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_7_address0() {
    sct_7_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_7_address1() {
    sct_7_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_7_address2() {
    sct_7_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_7_address3() {
    sct_7_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_7_address4() {
    sct_7_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_7_address5() {
    sct_7_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_7_address6() {
    sct_7_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_7_address7() {
    sct_7_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_7_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_7_ce0 = ap_const_logic_1;
    } else {
        sct_7_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_7_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_7_ce1 = ap_const_logic_1;
    } else {
        sct_7_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_7_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_7_ce2 = ap_const_logic_1;
    } else {
        sct_7_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_7_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_7_ce3 = ap_const_logic_1;
    } else {
        sct_7_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_7_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_7_ce4 = ap_const_logic_1;
    } else {
        sct_7_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_7_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_7_ce5 = ap_const_logic_1;
    } else {
        sct_7_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_7_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_7_ce6 = ap_const_logic_1;
    } else {
        sct_7_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_7_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_7_ce7 = ap_const_logic_1;
    } else {
        sct_7_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_8_address0() {
    sct_8_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_8_address1() {
    sct_8_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_8_address2() {
    sct_8_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_8_address3() {
    sct_8_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_8_address4() {
    sct_8_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_8_address5() {
    sct_8_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_8_address6() {
    sct_8_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_8_address7() {
    sct_8_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_8_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_8_ce0 = ap_const_logic_1;
    } else {
        sct_8_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_8_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_8_ce1 = ap_const_logic_1;
    } else {
        sct_8_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_8_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_8_ce2 = ap_const_logic_1;
    } else {
        sct_8_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_8_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_8_ce3 = ap_const_logic_1;
    } else {
        sct_8_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_8_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_8_ce4 = ap_const_logic_1;
    } else {
        sct_8_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_8_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_8_ce5 = ap_const_logic_1;
    } else {
        sct_8_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_8_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_8_ce6 = ap_const_logic_1;
    } else {
        sct_8_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_8_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_8_ce7 = ap_const_logic_1;
    } else {
        sct_8_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_9_address0() {
    sct_9_address0 =  (sc_lv<3>) (newIndex22_fu_7746_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_9_address1() {
    sct_9_address1 =  (sc_lv<3>) (newIndex25_fu_7813_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_9_address2() {
    sct_9_address2 =  (sc_lv<3>) (newIndex26_fu_7880_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_9_address3() {
    sct_9_address3 =  (sc_lv<3>) (newIndex27_fu_7947_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_9_address4() {
    sct_9_address4 =  (sc_lv<3>) (newIndex28_fu_8014_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_9_address5() {
    sct_9_address5 =  (sc_lv<3>) (newIndex31_fu_8081_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_9_address6() {
    sct_9_address6 =  (sc_lv<3>) (newIndex34_fu_8148_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_9_address7() {
    sct_9_address7 =  (sc_lv<3>) (newIndex37_fu_8215_p1.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sct_9_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_9_ce0 = ap_const_logic_1;
    } else {
        sct_9_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_9_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_9_ce1 = ap_const_logic_1;
    } else {
        sct_9_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_9_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_9_ce2 = ap_const_logic_1;
    } else {
        sct_9_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_9_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_9_ce3 = ap_const_logic_1;
    } else {
        sct_9_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_9_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_9_ce4 = ap_const_logic_1;
    } else {
        sct_9_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_9_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_9_ce5 = ap_const_logic_1;
    } else {
        sct_9_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_9_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_9_ce6 = ap_const_logic_1;
    } else {
        sct_9_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sct_9_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_9_ce7 = ap_const_logic_1;
    } else {
        sct_9_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp12_fu_7319_p2() {
    sel_tmp12_fu_7319_p2 = (!tmp_18_fu_7310_p1.read().is_01() || !ap_const_lv5_1.is_01())? sc_lv<1>(): sc_lv<1>(tmp_18_fu_7310_p1.read() == ap_const_lv5_1);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp13_fu_8462_p3() {
    sel_tmp13_fu_8462_p3 = (!ap_reg_ppstg_sel_tmp12_reg_9808_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp12_reg_9808_pp0_it1.read()[0].to_bool())? real_i_1_q0.read(): real_i_25_q0.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp14_fu_7325_p2() {
    sel_tmp14_fu_7325_p2 = (!tmp_18_fu_7310_p1.read().is_01() || !ap_const_lv5_9.is_01())? sc_lv<1>(): sc_lv<1>(tmp_18_fu_7310_p1.read() == ap_const_lv5_9);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp15_fu_8469_p3() {
    sel_tmp15_fu_8469_p3 = (!ap_reg_ppstg_sel_tmp14_reg_9814_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp14_reg_9814_pp0_it1.read()[0].to_bool())? real_i_9_q0.read(): sel_tmp13_fu_8462_p3.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp16_fu_7331_p2() {
    sel_tmp16_fu_7331_p2 = (!tmp_18_fu_7310_p1.read().is_01() || !ap_const_lv5_11.is_01())? sc_lv<1>(): sc_lv<1>(tmp_18_fu_7310_p1.read() == ap_const_lv5_11);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp17_fu_8552_p3() {
    sel_tmp17_fu_8552_p3 = (!ap_reg_ppstg_sel_tmp12_reg_9808_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp12_reg_9808_pp0_it1.read()[0].to_bool())? imag_i_1_q0.read(): imag_i_25_q0.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp18_fu_8559_p3() {
    sel_tmp18_fu_8559_p3 = (!ap_reg_ppstg_sel_tmp14_reg_9814_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp14_reg_9814_pp0_it1.read()[0].to_bool())? imag_i_9_q0.read(): sel_tmp17_fu_8552_p3.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp19_fu_7366_p2() {
    sel_tmp19_fu_7366_p2 = (!tmp_20_fu_7357_p1.read().is_01() || !ap_const_lv5_2.is_01())? sc_lv<1>(): sc_lv<1>(tmp_20_fu_7357_p1.read() == ap_const_lv5_2);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp1_fu_8282_p3() {
    sel_tmp1_fu_8282_p3 = (!ap_reg_ppstg_sel_tmp_reg_9774_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp_reg_9774_pp0_it1.read()[0].to_bool())? real_i_0_q0.read(): real_i_24_q0.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp20_fu_8642_p3() {
    sel_tmp20_fu_8642_p3 = (!ap_reg_ppstg_sel_tmp19_reg_9837_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp19_reg_9837_pp0_it1.read()[0].to_bool())? real_i_2_q0.read(): real_i_26_q0.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp21_fu_7372_p2() {
    sel_tmp21_fu_7372_p2 = (!tmp_20_fu_7357_p1.read().is_01() || !ap_const_lv5_A.is_01())? sc_lv<1>(): sc_lv<1>(tmp_20_fu_7357_p1.read() == ap_const_lv5_A);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp22_fu_8649_p3() {
    sel_tmp22_fu_8649_p3 = (!ap_reg_ppstg_sel_tmp21_reg_9843_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp21_reg_9843_pp0_it1.read()[0].to_bool())? real_i_10_q0.read(): sel_tmp20_fu_8642_p3.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp23_fu_7378_p2() {
    sel_tmp23_fu_7378_p2 = (!tmp_20_fu_7357_p1.read().is_01() || !ap_const_lv5_12.is_01())? sc_lv<1>(): sc_lv<1>(tmp_20_fu_7357_p1.read() == ap_const_lv5_12);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp24_fu_8732_p3() {
    sel_tmp24_fu_8732_p3 = (!ap_reg_ppstg_sel_tmp19_reg_9837_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp19_reg_9837_pp0_it1.read()[0].to_bool())? imag_i_2_q0.read(): imag_i_26_q0.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp25_fu_8739_p3() {
    sel_tmp25_fu_8739_p3 = (!ap_reg_ppstg_sel_tmp21_reg_9843_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp21_reg_9843_pp0_it1.read()[0].to_bool())? imag_i_10_q0.read(): sel_tmp24_fu_8732_p3.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp26_fu_7413_p2() {
    sel_tmp26_fu_7413_p2 = (!tmp_22_fu_7404_p1.read().is_01() || !ap_const_lv5_3.is_01())? sc_lv<1>(): sc_lv<1>(tmp_22_fu_7404_p1.read() == ap_const_lv5_3);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp27_fu_8822_p3() {
    sel_tmp27_fu_8822_p3 = (!ap_reg_ppstg_sel_tmp26_reg_9866_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp26_reg_9866_pp0_it1.read()[0].to_bool())? real_i_3_q0.read(): real_i_27_q0.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp28_fu_7419_p2() {
    sel_tmp28_fu_7419_p2 = (!tmp_22_fu_7404_p1.read().is_01() || !ap_const_lv5_B.is_01())? sc_lv<1>(): sc_lv<1>(tmp_22_fu_7404_p1.read() == ap_const_lv5_B);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp29_fu_8829_p3() {
    sel_tmp29_fu_8829_p3 = (!ap_reg_ppstg_sel_tmp28_reg_9872_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp28_reg_9872_pp0_it1.read()[0].to_bool())? real_i_11_q0.read(): sel_tmp27_fu_8822_p3.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp2_fu_7268_p2() {
    sel_tmp2_fu_7268_p2 = (!tmp_16_fu_7248_p1.read().is_01() || !ap_const_lv5_8.is_01())? sc_lv<1>(): sc_lv<1>(tmp_16_fu_7248_p1.read() == ap_const_lv5_8);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp30_fu_7425_p2() {
    sel_tmp30_fu_7425_p2 = (!tmp_22_fu_7404_p1.read().is_01() || !ap_const_lv5_13.is_01())? sc_lv<1>(): sc_lv<1>(tmp_22_fu_7404_p1.read() == ap_const_lv5_13);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp31_fu_8912_p3() {
    sel_tmp31_fu_8912_p3 = (!ap_reg_ppstg_sel_tmp26_reg_9866_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp26_reg_9866_pp0_it1.read()[0].to_bool())? imag_i_3_q0.read(): imag_i_27_q0.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp32_fu_8919_p3() {
    sel_tmp32_fu_8919_p3 = (!ap_reg_ppstg_sel_tmp28_reg_9872_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp28_reg_9872_pp0_it1.read()[0].to_bool())? imag_i_11_q0.read(): sel_tmp31_fu_8912_p3.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp33_fu_7460_p2() {
    sel_tmp33_fu_7460_p2 = (!tmp_24_fu_7451_p1.read().is_01() || !ap_const_lv5_4.is_01())? sc_lv<1>(): sc_lv<1>(tmp_24_fu_7451_p1.read() == ap_const_lv5_4);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp34_fu_9002_p3() {
    sel_tmp34_fu_9002_p3 = (!ap_reg_ppstg_sel_tmp33_reg_9895_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp33_reg_9895_pp0_it1.read()[0].to_bool())? real_i_4_q0.read(): real_i_28_q0.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp35_fu_7466_p2() {
    sel_tmp35_fu_7466_p2 = (!tmp_24_fu_7451_p1.read().is_01() || !ap_const_lv5_C.is_01())? sc_lv<1>(): sc_lv<1>(tmp_24_fu_7451_p1.read() == ap_const_lv5_C);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp36_fu_9009_p3() {
    sel_tmp36_fu_9009_p3 = (!ap_reg_ppstg_sel_tmp35_reg_9901_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp35_reg_9901_pp0_it1.read()[0].to_bool())? real_i_12_q0.read(): sel_tmp34_fu_9002_p3.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp37_fu_7472_p2() {
    sel_tmp37_fu_7472_p2 = (!tmp_24_fu_7451_p1.read().is_01() || !ap_const_lv5_14.is_01())? sc_lv<1>(): sc_lv<1>(tmp_24_fu_7451_p1.read() == ap_const_lv5_14);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp38_fu_9092_p3() {
    sel_tmp38_fu_9092_p3 = (!ap_reg_ppstg_sel_tmp33_reg_9895_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp33_reg_9895_pp0_it1.read()[0].to_bool())? imag_i_4_q0.read(): imag_i_28_q0.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp39_fu_9099_p3() {
    sel_tmp39_fu_9099_p3 = (!ap_reg_ppstg_sel_tmp35_reg_9901_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp35_reg_9901_pp0_it1.read()[0].to_bool())? imag_i_12_q0.read(): sel_tmp38_fu_9092_p3.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp3_fu_8289_p3() {
    sel_tmp3_fu_8289_p3 = (!ap_reg_ppstg_sel_tmp2_reg_9780_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp2_reg_9780_pp0_it1.read()[0].to_bool())? real_i_8_q0.read(): sel_tmp1_fu_8282_p3.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp40_fu_7517_p2() {
    sel_tmp40_fu_7517_p2 = (!tmp_26_fu_7508_p1.read().is_01() || !ap_const_lv5_5.is_01())? sc_lv<1>(): sc_lv<1>(tmp_26_fu_7508_p1.read() == ap_const_lv5_5);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp41_fu_9182_p3() {
    sel_tmp41_fu_9182_p3 = (!ap_reg_ppstg_sel_tmp40_reg_9929_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp40_reg_9929_pp0_it1.read()[0].to_bool())? real_i_5_q0.read(): real_i_29_q0.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp42_fu_7523_p2() {
    sel_tmp42_fu_7523_p2 = (!tmp_26_fu_7508_p1.read().is_01() || !ap_const_lv5_D.is_01())? sc_lv<1>(): sc_lv<1>(tmp_26_fu_7508_p1.read() == ap_const_lv5_D);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp43_fu_9189_p3() {
    sel_tmp43_fu_9189_p3 = (!ap_reg_ppstg_sel_tmp42_reg_9935_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp42_reg_9935_pp0_it1.read()[0].to_bool())? real_i_13_q0.read(): sel_tmp41_fu_9182_p3.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp44_fu_7529_p2() {
    sel_tmp44_fu_7529_p2 = (!tmp_26_fu_7508_p1.read().is_01() || !ap_const_lv5_15.is_01())? sc_lv<1>(): sc_lv<1>(tmp_26_fu_7508_p1.read() == ap_const_lv5_15);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp45_fu_9272_p3() {
    sel_tmp45_fu_9272_p3 = (!ap_reg_ppstg_sel_tmp40_reg_9929_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp40_reg_9929_pp0_it1.read()[0].to_bool())? imag_i_5_q0.read(): imag_i_29_q0.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp46_fu_9279_p3() {
    sel_tmp46_fu_9279_p3 = (!ap_reg_ppstg_sel_tmp42_reg_9935_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp42_reg_9935_pp0_it1.read()[0].to_bool())? imag_i_13_q0.read(): sel_tmp45_fu_9272_p3.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp47_fu_7574_p2() {
    sel_tmp47_fu_7574_p2 = (!tmp_28_fu_7565_p1.read().is_01() || !ap_const_lv5_6.is_01())? sc_lv<1>(): sc_lv<1>(tmp_28_fu_7565_p1.read() == ap_const_lv5_6);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp48_fu_9362_p3() {
    sel_tmp48_fu_9362_p3 = (!ap_reg_ppstg_sel_tmp47_reg_9963_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp47_reg_9963_pp0_it1.read()[0].to_bool())? real_i_6_q0.read(): real_i_30_q0.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp49_fu_7580_p2() {
    sel_tmp49_fu_7580_p2 = (!tmp_28_fu_7565_p1.read().is_01() || !ap_const_lv5_E.is_01())? sc_lv<1>(): sc_lv<1>(tmp_28_fu_7565_p1.read() == ap_const_lv5_E);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp4_fu_7274_p2() {
    sel_tmp4_fu_7274_p2 = (!tmp_16_fu_7248_p1.read().is_01() || !ap_const_lv5_10.is_01())? sc_lv<1>(): sc_lv<1>(tmp_16_fu_7248_p1.read() == ap_const_lv5_10);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp50_fu_9369_p3() {
    sel_tmp50_fu_9369_p3 = (!ap_reg_ppstg_sel_tmp49_reg_9969_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp49_reg_9969_pp0_it1.read()[0].to_bool())? real_i_14_q0.read(): sel_tmp48_fu_9362_p3.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp51_fu_7586_p2() {
    sel_tmp51_fu_7586_p2 = (!tmp_28_fu_7565_p1.read().is_01() || !ap_const_lv5_16.is_01())? sc_lv<1>(): sc_lv<1>(tmp_28_fu_7565_p1.read() == ap_const_lv5_16);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp52_fu_9452_p3() {
    sel_tmp52_fu_9452_p3 = (!ap_reg_ppstg_sel_tmp47_reg_9963_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp47_reg_9963_pp0_it1.read()[0].to_bool())? imag_i_6_q0.read(): imag_i_30_q0.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp53_fu_9459_p3() {
    sel_tmp53_fu_9459_p3 = (!ap_reg_ppstg_sel_tmp49_reg_9969_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp49_reg_9969_pp0_it1.read()[0].to_bool())? imag_i_14_q0.read(): sel_tmp52_fu_9452_p3.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp54_fu_7631_p2() {
    sel_tmp54_fu_7631_p2 = (!tmp_30_fu_7622_p1.read().is_01() || !ap_const_lv5_7.is_01())? sc_lv<1>(): sc_lv<1>(tmp_30_fu_7622_p1.read() == ap_const_lv5_7);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp55_fu_9542_p3() {
    sel_tmp55_fu_9542_p3 = (!ap_reg_ppstg_sel_tmp54_reg_9997_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp54_reg_9997_pp0_it1.read()[0].to_bool())? real_i_7_q0.read(): real_i_31_q0.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp56_fu_7637_p2() {
    sel_tmp56_fu_7637_p2 = (!tmp_30_fu_7622_p1.read().is_01() || !ap_const_lv5_F.is_01())? sc_lv<1>(): sc_lv<1>(tmp_30_fu_7622_p1.read() == ap_const_lv5_F);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp57_fu_9549_p3() {
    sel_tmp57_fu_9549_p3 = (!ap_reg_ppstg_sel_tmp56_reg_10003_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp56_reg_10003_pp0_it1.read()[0].to_bool())? real_i_15_q0.read(): sel_tmp55_fu_9542_p3.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp58_fu_7643_p2() {
    sel_tmp58_fu_7643_p2 = (!tmp_30_fu_7622_p1.read().is_01() || !ap_const_lv5_17.is_01())? sc_lv<1>(): sc_lv<1>(tmp_30_fu_7622_p1.read() == ap_const_lv5_17);
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp59_fu_9632_p3() {
    sel_tmp59_fu_9632_p3 = (!ap_reg_ppstg_sel_tmp54_reg_9997_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp54_reg_9997_pp0_it1.read()[0].to_bool())? imag_i_7_q0.read(): imag_i_31_q0.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp60_fu_9639_p3() {
    sel_tmp60_fu_9639_p3 = (!ap_reg_ppstg_sel_tmp56_reg_10003_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp56_reg_10003_pp0_it1.read()[0].to_bool())? imag_i_15_q0.read(): sel_tmp59_fu_9632_p3.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp7_fu_8372_p3() {
    sel_tmp7_fu_8372_p3 = (!ap_reg_ppstg_sel_tmp_reg_9774_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp_reg_9774_pp0_it1.read()[0].to_bool())? imag_i_0_q0.read(): imag_i_24_q0.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp9_fu_8379_p3() {
    sel_tmp9_fu_8379_p3 = (!ap_reg_ppstg_sel_tmp2_reg_9780_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp2_reg_9780_pp0_it1.read()[0].to_bool())? imag_i_8_q0.read(): sel_tmp7_fu_8372_p3.read());
}

void dft_loop_flow256_Loop_1_proc::thread_sel_tmp_fu_7262_p2() {
    sel_tmp_fu_7262_p2 = (!tmp_16_fu_7248_p1.read().is_01() || !ap_const_lv5_0.is_01())? sc_lv<1>(): sc_lv<1>(tmp_16_fu_7248_p1.read() == ap_const_lv5_0);
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_16_fu_7248_p1() {
    tmp_16_fu_7248_p1 = p_02_0_i_i_reg_7034.read().range(5-1, 0);
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_17_fu_7280_p1() {
    tmp_17_fu_7280_p1 = tmp_i_fu_7243_p2.read().range(5-1, 0);
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_18_fu_7310_p1() {
    tmp_18_fu_7310_p1 = j_V_i_1_fu_7304_p2.read().range(5-1, 0);
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_19_fu_7337_p1() {
    tmp_19_fu_7337_p1 = tmp_i_1_fu_7314_p2.read().range(5-1, 0);
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_20_fu_7357_p1() {
    tmp_20_fu_7357_p1 = j_V_i_s_fu_7351_p2.read().range(5-1, 0);
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_21_fu_7384_p1() {
    tmp_21_fu_7384_p1 = tmp_i_2_fu_7361_p2.read().range(5-1, 0);
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_22_fu_7404_p1() {
    tmp_22_fu_7404_p1 = j_V_i_8_fu_7398_p2.read().range(5-1, 0);
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_23_fu_7431_p1() {
    tmp_23_fu_7431_p1 = tmp_i_3_fu_7408_p2.read().range(5-1, 0);
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_24_fu_7451_p1() {
    tmp_24_fu_7451_p1 = j_V_i_9_fu_7445_p2.read().range(5-1, 0);
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_25_fu_7478_p1() {
    tmp_25_fu_7478_p1 = tmp_i_4_fu_7455_p2.read().range(5-1, 0);
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_26_fu_7508_p1() {
    tmp_26_fu_7508_p1 = j_V_i_2_fu_7502_p2.read().range(5-1, 0);
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_27_fu_7535_p1() {
    tmp_27_fu_7535_p1 = tmp_i_5_fu_7512_p2.read().range(5-1, 0);
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_28_fu_7565_p1() {
    tmp_28_fu_7565_p1 = j_V_i_3_fu_7559_p2.read().range(5-1, 0);
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_29_fu_7592_p1() {
    tmp_29_fu_7592_p1 = tmp_i_6_fu_7569_p2.read().range(5-1, 0);
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_30_fu_7622_p1() {
    tmp_30_fu_7622_p1 = j_V_i_4_fu_7616_p2.read().range(5-1, 0);
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_31_fu_7649_p1() {
    tmp_31_fu_7649_p1 = tmp_i_7_fu_7626_p2.read().range(5-1, 0);
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_i_1_fu_7314_p0() {
    tmp_i_1_fu_7314_p0 = i_V_read.read();
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_i_1_fu_7314_p2() {
    tmp_i_1_fu_7314_p2 = (!tmp_i_1_fu_7314_p0.read().is_01() || !j_V_i_1_fu_7304_p2.read().is_01())? sc_lv<9>(): sc_bigint<9>(tmp_i_1_fu_7314_p0.read()) * sc_bigint<9>(j_V_i_1_fu_7304_p2.read());
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_i_2_fu_7361_p0() {
    tmp_i_2_fu_7361_p0 = i_V_read.read();
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_i_2_fu_7361_p2() {
    tmp_i_2_fu_7361_p2 = (!tmp_i_2_fu_7361_p0.read().is_01() || !j_V_i_s_fu_7351_p2.read().is_01())? sc_lv<9>(): sc_bigint<9>(tmp_i_2_fu_7361_p0.read()) * sc_bigint<9>(j_V_i_s_fu_7351_p2.read());
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_i_3_fu_7408_p0() {
    tmp_i_3_fu_7408_p0 = i_V_read.read();
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_i_3_fu_7408_p2() {
    tmp_i_3_fu_7408_p2 = (!tmp_i_3_fu_7408_p0.read().is_01() || !j_V_i_8_fu_7398_p2.read().is_01())? sc_lv<9>(): sc_bigint<9>(tmp_i_3_fu_7408_p0.read()) * sc_bigint<9>(j_V_i_8_fu_7398_p2.read());
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_i_4_fu_7455_p0() {
    tmp_i_4_fu_7455_p0 = i_V_read.read();
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_i_4_fu_7455_p2() {
    tmp_i_4_fu_7455_p2 = (!tmp_i_4_fu_7455_p0.read().is_01() || !j_V_i_9_fu_7445_p2.read().is_01())? sc_lv<9>(): sc_bigint<9>(tmp_i_4_fu_7455_p0.read()) * sc_bigint<9>(j_V_i_9_fu_7445_p2.read());
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_i_5_fu_7512_p0() {
    tmp_i_5_fu_7512_p0 = i_V_read.read();
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_i_5_fu_7512_p2() {
    tmp_i_5_fu_7512_p2 = (!tmp_i_5_fu_7512_p0.read().is_01() || !j_V_i_2_fu_7502_p2.read().is_01())? sc_lv<9>(): sc_bigint<9>(tmp_i_5_fu_7512_p0.read()) * sc_bigint<9>(j_V_i_2_fu_7502_p2.read());
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_i_6_fu_7569_p0() {
    tmp_i_6_fu_7569_p0 = i_V_read.read();
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_i_6_fu_7569_p2() {
    tmp_i_6_fu_7569_p2 = (!tmp_i_6_fu_7569_p0.read().is_01() || !j_V_i_3_fu_7559_p2.read().is_01())? sc_lv<9>(): sc_bigint<9>(tmp_i_6_fu_7569_p0.read()) * sc_bigint<9>(j_V_i_3_fu_7559_p2.read());
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_i_7_fu_7626_p0() {
    tmp_i_7_fu_7626_p0 = i_V_read.read();
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_i_7_fu_7626_p2() {
    tmp_i_7_fu_7626_p2 = (!tmp_i_7_fu_7626_p0.read().is_01() || !j_V_i_4_fu_7616_p2.read().is_01())? sc_lv<9>(): sc_bigint<9>(tmp_i_7_fu_7626_p0.read()) * sc_bigint<9>(j_V_i_4_fu_7616_p2.read());
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_i_fu_7243_p0() {
    tmp_i_fu_7243_p0 = i_V_read.read();
}

void dft_loop_flow256_Loop_1_proc::thread_tmp_i_fu_7243_p2() {
    tmp_i_fu_7243_p2 = (!tmp_i_fu_7243_p0.read().is_01() || !p_02_0_i_i_reg_7034.read().is_01())? sc_lv<9>(): sc_bigint<9>(tmp_i_fu_7243_p0.read()) * sc_bigint<9>(p_02_0_i_i_reg_7034.read());
}

}

