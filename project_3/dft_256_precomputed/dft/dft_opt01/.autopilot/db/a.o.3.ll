; ModuleID = 'D:/Projects/vivado/project_3/dft_256_precomputed/dft/dft_opt1/.autopilot/db/a.o.3.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@sin_coefficients_table = internal unnamed_addr constant [256 x float] [float 0.000000e+00, float 0xBF992146A0000000, float 0xBFA91F70E0000000, float 0xBFB2D527E0000000, float 0xBFB917A460000000, float 0xBFBF5653C0000000, float 0xBFC2C80C80000000, float 0xBFC5E21540000000, float 0xBFC8F8B580000000, float 0xBFCC0B8060000000, float 0xBFCF19F800000000, float 0xBFD111D360000000, float 0xBFD2940780000000, float 0xBFD4135DA0000000, float 0xBFD58F9B20000000, float 0xBFD7088500000000, float 0xBFD87DE0E0000000, float 0xBFD9EF7800000000, float 0xBFDB5D0FA0000000, float 0xBFDCC66D40000000, float 0xBFDE2B5E60000000, float 0xBFDF8BA400000000, float 0xBFE0738820000000, float 0xBFE11EB420000000, float 0xBFE1C73AC0000000, float 0xBFE26D04E0000000, float 0xBFE30FF760000000, float 0xBFE3AFFB00000000, float 0xBFE44CF280000000, float 0xBFE4E6CB20000000, float 0xBFE57D6940000000, float 0xBFE610B840000000, float 0xBFE6A09EE0000000, float 0xBFE72D0800000000, float 0xBFE7B5DEE0000000, float 0xBFE83B0E60000000, float 0xBFE8BC7F80000000, float 0xBFE93A2160000000, float 0xBFE9B3E140000000, float 0xBFEA29A800000000, float 0xBFEA9B6700000000, float 0xBFEB090B40000000, float 0xBFEB728420000000, float 0xBFEBD7C0C0000000, float 0xBFEC38B260000000, float 0xBFEC954A80000000, float 0xBFECED7A60000000, float 0xBFED413560000000, float 0xBFED906CC0000000, float 0xBFEDDB1420000000, float 0xBFEE2120E0000000, float 0xBFEE628880000000, float 0xBFEE9F40A0000000, float 0xBFEED740C0000000, float 0xBFEF0A7E80000000, float 0xBFEF38F360000000, float 0xBFEF629740000000, float 0xBFEF8765C0000000, float 0xBFEFA75680000000, float 0xBFEFC26560000000, float 0xBFEFD88E40000000, float 0xBFEFE9CCC0000000, float 0xBFEFF620E0000000, float 0xBFEFFD88C0000000, float -1.000000e+00, float 0xBFEFFD88C0000000, float 0xBFEFF620E0000000, float 0xBFEFE9CCC0000000, float 0xBFEFD88E40000000, float 0xBFEFC26560000000, float 0xBFEFA75680000000, float 0xBFEF8765C0000000, float 0xBFEF629740000000, float 0xBFEF38F360000000, float 0xBFEF0A7E80000000, float 0xBFEED740C0000000, float 0xBFEE9F40A0000000, float 0xBFEE628880000000, float 0xBFEE2120E0000000, float 0xBFEDDB1420000000, float 0xBFED906CC0000000, float 0xBFED413560000000, float 0xBFECED7A60000000, float 0xBFEC954A80000000, float 0xBFEC38B260000000, float 0xBFEBD7C0C0000000, float 0xBFEB728420000000, float 0xBFEB090B40000000, float 0xBFEA9B6700000000, float 0xBFEA29A800000000, float 0xBFE9B3E140000000, float 0xBFE93A2160000000, float 0xBFE8BC7F80000000, float 0xBFE83B0E60000000, float 0xBFE7B5DEE0000000, float 0xBFE72D0800000000, float 0xBFE6A09EE0000000, float 0xBFE610B840000000, float 0xBFE57D6940000000, float 0xBFE4E6CB20000000, float 0xBFE44CF280000000, float 0xBFE3AFFB00000000, float 0xBFE30FF760000000, float 0xBFE26D04E0000000, float 0xBFE1C73AC0000000, float 0xBFE11EB420000000, float 0xBFE0738820000000, float 0xBFDF8BA400000000, float 0xBFDE2B5E60000000, float 0xBFDCC66D40000000, float 0xBFDB5D0FA0000000, float 0xBFD9EF7800000000, float 0xBFD87DE0E0000000, float 0xBFD7088500000000, float 0xBFD58F9B20000000, float 0xBFD4135DA0000000, float 0xBFD2940780000000, float 0xBFD111D360000000, float 0xBFCF19F800000000, float 0xBFCC0B8060000000, float 0xBFC8F8B580000000, float 0xBFC5E21540000000, float 0xBFC2C80C80000000, float 0xBFBF5653C0000000, float 0xBFB917A460000000, float 0xBFB2D527E0000000, float 0xBFA91F70E0000000, float 0xBF992146A0000000, float -0.000000e+00, float 0x3F992146A0000000, float 0x3FA91F70E0000000, float 0x3FB2D527E0000000, float 0x3FB917A460000000, float 0x3FBF5653C0000000, float 0x3FC2C80C80000000, float 0x3FC5E21540000000, float 0x3FC8F8B580000000, float 0x3FCC0B8060000000, float 0x3FCF19F800000000, float 0x3FD111D360000000, float 0x3FD2940780000000, float 0x3FD4135DA0000000, float 0x3FD58F9B20000000, float 0x3FD7088500000000, float 0x3FD87DE0E0000000, float 0x3FD9EF7800000000, float 0x3FDB5D0FA0000000, float 0x3FDCC66D40000000, float 0x3FDE2B5E60000000, float 0x3FDF8BA400000000, float 0x3FE0738820000000, float 0x3FE11EB420000000, float 0x3FE1C73AC0000000, float 0x3FE26D04E0000000, float 0x3FE30FF760000000, float 0x3FE3AFFB00000000, float 0x3FE44CF280000000, float 0x3FE4E6CB20000000, float 0x3FE57D6940000000, float 0x3FE610B840000000, float 0x3FE6A09EE0000000, float 0x3FE72D0800000000, float 0x3FE7B5DEE0000000, float 0x3FE83B0E60000000, float 0x3FE8BC7F80000000, float 0x3FE93A2160000000, float 0x3FE9B3E140000000, float 0x3FEA29A800000000, float 0x3FEA9B6700000000, float 0x3FEB090B40000000, float 0x3FEB728420000000, float 0x3FEBD7C0C0000000, float 0x3FEC38B260000000, float 0x3FEC954A80000000, float 0x3FECED7A60000000, float 0x3FED413560000000, float 0x3FED906CC0000000, float 0x3FEDDB1420000000, float 0x3FEE2120E0000000, float 0x3FEE628880000000, float 0x3FEE9F40A0000000, float 0x3FEED740C0000000, float 0x3FEF0A7E80000000, float 0x3FEF38F360000000, float 0x3FEF629740000000, float 0x3FEF8765C0000000, float 0x3FEFA75680000000, float 0x3FEFC26560000000, float 0x3FEFD88E40000000, float 0x3FEFE9CCC0000000, float 0x3FEFF620E0000000, float 0x3FEFFD88C0000000, float 1.000000e+00, float 0x3FEFFD88C0000000, float 0x3FEFF620E0000000, float 0x3FEFE9CCC0000000, float 0x3FEFD88E40000000, float 0x3FEFC26560000000, float 0x3FEFA75680000000, float 0x3FEF8765C0000000, float 0x3FEF629740000000, float 0x3FEF38F360000000, float 0x3FEF0A7E80000000, float 0x3FEED740C0000000, float 0x3FEE9F40A0000000, float 0x3FEE628880000000, float 0x3FEE2120E0000000, float 0x3FEDDB1420000000, float 0x3FED906CC0000000, float 0x3FED413560000000, float 0x3FECED7A60000000, float 0x3FEC954A80000000, float 0x3FEC38B260000000, float 0x3FEBD7C0C0000000, float 0x3FEB728420000000, float 0x3FEB090B40000000, float 0x3FEA9B6700000000, float 0x3FEA29A800000000, float 0x3FE9B3E140000000, float 0x3FE93A2160000000, float 0x3FE8BC7F80000000, float 0x3FE83B0E60000000, float 0x3FE7B5DEE0000000, float 0x3FE72D0800000000, float 0x3FE6A09EE0000000, float 0x3FE610B840000000, float 0x3FE57D6940000000, float 0x3FE4E6CB20000000, float 0x3FE44CF280000000, float 0x3FE3AFFB00000000, float 0x3FE30FF760000000, float 0x3FE26D04E0000000, float 0x3FE1C73AC0000000, float 0x3FE11EB420000000, float 0x3FE0738820000000, float 0x3FDF8BA400000000, float 0x3FDE2B5E60000000, float 0x3FDCC66D40000000, float 0x3FDB5D0FA0000000, float 0x3FD9EF7800000000, float 0x3FD87DE0E0000000, float 0x3FD7088500000000, float 0x3FD58F9B20000000, float 0x3FD4135DA0000000, float 0x3FD2940780000000, float 0x3FD111D360000000, float 0x3FCF19F800000000, float 0x3FCC0B8060000000, float 0x3FC8F8B580000000, float 0x3FC5E21540000000, float 0x3FC2C80C80000000, float 0x3FBF5653C0000000, float 0x3FB917A460000000, float 0x3FB2D527E0000000, float 0x3FA91F70E0000000, float 0x3F992146A0000000], align 16 ; [#uses=1 type=[256 x float]*]
@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a] ; [#uses=0 type=[1 x void ()*]*]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535] ; [#uses=0 type=[1 x i32]*]
@dft_str = internal unnamed_addr constant [4 x i8] c"dft\00" ; [#uses=1 type=[4 x i8]*]
@cos_coefficients_table = internal unnamed_addr constant [256 x float] [float 1.000000e+00, float 0x3FEFFD88C0000000, float 0x3FEFF620E0000000, float 0x3FEFE9CCC0000000, float 0x3FEFD88E40000000, float 0x3FEFC26560000000, float 0x3FEFA75680000000, float 0x3FEF8765C0000000, float 0x3FEF629740000000, float 0x3FEF38F360000000, float 0x3FEF0A7E80000000, float 0x3FEED740C0000000, float 0x3FEE9F40A0000000, float 0x3FEE628880000000, float 0x3FEE2120E0000000, float 0x3FEDDB1420000000, float 0x3FED906CC0000000, float 0x3FED413560000000, float 0x3FECED7A60000000, float 0x3FEC954A80000000, float 0x3FEC38B260000000, float 0x3FEBD7C0C0000000, float 0x3FEB728420000000, float 0x3FEB090B40000000, float 0x3FEA9B6700000000, float 0x3FEA29A800000000, float 0x3FE9B3E140000000, float 0x3FE93A2160000000, float 0x3FE8BC7F80000000, float 0x3FE83B0E60000000, float 0x3FE7B5DEE0000000, float 0x3FE72D0800000000, float 0x3FE6A09EE0000000, float 0x3FE610B840000000, float 0x3FE57D6940000000, float 0x3FE4E6CB20000000, float 0x3FE44CF280000000, float 0x3FE3AFFB00000000, float 0x3FE30FF760000000, float 0x3FE26D04E0000000, float 0x3FE1C73AC0000000, float 0x3FE11EB420000000, float 0x3FE0738820000000, float 0x3FDF8BA400000000, float 0x3FDE2B5E60000000, float 0x3FDCC66D40000000, float 0x3FDB5D0FA0000000, float 0x3FD9EF7800000000, float 0x3FD87DE0E0000000, float 0x3FD7088500000000, float 0x3FD58F9B20000000, float 0x3FD4135DA0000000, float 0x3FD2940780000000, float 0x3FD111D360000000, float 0x3FCF19F800000000, float 0x3FCC0B8060000000, float 0x3FC8F8B580000000, float 0x3FC5E21540000000, float 0x3FC2C80C80000000, float 0x3FBF5653C0000000, float 0x3FB917A460000000, float 0x3FB2D527E0000000, float 0x3FA91F70E0000000, float 0x3F992146A0000000, float 0.000000e+00, float 0xBF992146A0000000, float 0xBFA91F70E0000000, float 0xBFB2D527E0000000, float 0xBFB917A460000000, float 0xBFBF5653C0000000, float 0xBFC2C80C80000000, float 0xBFC5E21540000000, float 0xBFC8F8B580000000, float 0xBFCC0B8060000000, float 0xBFCF19F800000000, float 0xBFD111D360000000, float 0xBFD2940780000000, float 0xBFD4135DA0000000, float 0xBFD58F9B20000000, float 0xBFD7088500000000, float 0xBFD87DE0E0000000, float 0xBFD9EF7800000000, float 0xBFDB5D0FA0000000, float 0xBFDCC66D40000000, float 0xBFDE2B5E60000000, float 0xBFDF8BA400000000, float 0xBFE0738820000000, float 0xBFE11EB420000000, float 0xBFE1C73AC0000000, float 0xBFE26D04E0000000, float 0xBFE30FF760000000, float 0xBFE3AFFB00000000, float 0xBFE44CF280000000, float 0xBFE4E6CB20000000, float 0xBFE57D6940000000, float 0xBFE610B840000000, float 0xBFE6A09EE0000000, float 0xBFE72D0800000000, float 0xBFE7B5DEE0000000, float 0xBFE83B0E60000000, float 0xBFE8BC7F80000000, float 0xBFE93A2160000000, float 0xBFE9B3E140000000, float 0xBFEA29A800000000, float 0xBFEA9B6700000000, float 0xBFEB090B40000000, float 0xBFEB728420000000, float 0xBFEBD7C0C0000000, float 0xBFEC38B260000000, float 0xBFEC954A80000000, float 0xBFECED7A60000000, float 0xBFED413560000000, float 0xBFED906CC0000000, float 0xBFEDDB1420000000, float 0xBFEE2120E0000000, float 0xBFEE628880000000, float 0xBFEE9F40A0000000, float 0xBFEED740C0000000, float 0xBFEF0A7E80000000, float 0xBFEF38F360000000, float 0xBFEF629740000000, float 0xBFEF8765C0000000, float 0xBFEFA75680000000, float 0xBFEFC26560000000, float 0xBFEFD88E40000000, float 0xBFEFE9CCC0000000, float 0xBFEFF620E0000000, float 0xBFEFFD88C0000000, float -1.000000e+00, float 0xBFEFFD88C0000000, float 0xBFEFF620E0000000, float 0xBFEFE9CCC0000000, float 0xBFEFD88E40000000, float 0xBFEFC26560000000, float 0xBFEFA75680000000, float 0xBFEF8765C0000000, float 0xBFEF629740000000, float 0xBFEF38F360000000, float 0xBFEF0A7E80000000, float 0xBFEED740C0000000, float 0xBFEE9F40A0000000, float 0xBFEE628880000000, float 0xBFEE2120E0000000, float 0xBFEDDB1420000000, float 0xBFED906CC0000000, float 0xBFED413560000000, float 0xBFECED7A60000000, float 0xBFEC954A80000000, float 0xBFEC38B260000000, float 0xBFEBD7C0C0000000, float 0xBFEB728420000000, float 0xBFEB090B40000000, float 0xBFEA9B6700000000, float 0xBFEA29A800000000, float 0xBFE9B3E140000000, float 0xBFE93A2160000000, float 0xBFE8BC7F80000000, float 0xBFE83B0E60000000, float 0xBFE7B5DEE0000000, float 0xBFE72D0800000000, float 0xBFE6A09EE0000000, float 0xBFE610B840000000, float 0xBFE57D6940000000, float 0xBFE4E6CB20000000, float 0xBFE44CF280000000, float 0xBFE3AFFB00000000, float 0xBFE30FF760000000, float 0xBFE26D04E0000000, float 0xBFE1C73AC0000000, float 0xBFE11EB420000000, float 0xBFE0738820000000, float 0xBFDF8BA400000000, float 0xBFDE2B5E60000000, float 0xBFDCC66D40000000, float 0xBFDB5D0FA0000000, float 0xBFD9EF7800000000, float 0xBFD87DE0E0000000, float 0xBFD7088500000000, float 0xBFD58F9B20000000, float 0xBFD4135DA0000000, float 0xBFD2940780000000, float 0xBFD111D360000000, float 0xBFCF19F800000000, float 0xBFCC0B8060000000, float 0xBFC8F8B580000000, float 0xBFC5E21540000000, float 0xBFC2C80C80000000, float 0xBFBF5653C0000000, float 0xBFB917A460000000, float 0xBFB2D527E0000000, float 0xBFA91F70E0000000, float 0xBF992146A0000000, float -0.000000e+00, float 0x3F992146A0000000, float 0x3FA91F70E0000000, float 0x3FB2D527E0000000, float 0x3FB917A460000000, float 0x3FBF5653C0000000, float 0x3FC2C80C80000000, float 0x3FC5E21540000000, float 0x3FC8F8B580000000, float 0x3FCC0B8060000000, float 0x3FCF19F800000000, float 0x3FD111D360000000, float 0x3FD2940780000000, float 0x3FD4135DA0000000, float 0x3FD58F9B20000000, float 0x3FD7088500000000, float 0x3FD87DE0E0000000, float 0x3FD9EF7800000000, float 0x3FDB5D0FA0000000, float 0x3FDCC66D40000000, float 0x3FDE2B5E60000000, float 0x3FDF8BA400000000, float 0x3FE0738820000000, float 0x3FE11EB420000000, float 0x3FE1C73AC0000000, float 0x3FE26D04E0000000, float 0x3FE30FF760000000, float 0x3FE3AFFB00000000, float 0x3FE44CF280000000, float 0x3FE4E6CB20000000, float 0x3FE57D6940000000, float 0x3FE610B840000000, float 0x3FE6A09EE0000000, float 0x3FE72D0800000000, float 0x3FE7B5DEE0000000, float 0x3FE83B0E60000000, float 0x3FE8BC7F80000000, float 0x3FE93A2160000000, float 0x3FE9B3E140000000, float 0x3FEA29A800000000, float 0x3FEA9B6700000000, float 0x3FEB090B40000000, float 0x3FEB728420000000, float 0x3FEBD7C0C0000000, float 0x3FEC38B260000000, float 0x3FEC954A80000000, float 0x3FECED7A60000000, float 0x3FED413560000000, float 0x3FED906CC0000000, float 0x3FEDDB1420000000, float 0x3FEE2120E0000000, float 0x3FEE628880000000, float 0x3FEE9F40A0000000, float 0x3FEED740C0000000, float 0x3FEF0A7E80000000, float 0x3FEF38F360000000, float 0x3FEF629740000000, float 0x3FEF8765C0000000, float 0x3FEFA75680000000, float 0x3FEFC26560000000, float 0x3FEFD88E40000000, float 0x3FEFE9CCC0000000, float 0x3FEFF620E0000000, float 0x3FEFFD88C0000000], align 16 ; [#uses=1 type=[256 x float]*]

; [#uses=7]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=2]
declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

; [#uses=0]
define void @dft([256 x float]* %sample_real, [256 x float]* %sample_imag) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap([256 x float]* %sample_real) nounwind, !map !7
  call void (...)* @_ssdm_op_SpecBitsMap([256 x float]* %sample_imag) nounwind, !map !13
  call void (...)* @_ssdm_op_SpecTopModule([4 x i8]* @dft_str) nounwind
  %tmp_real = alloca [256 x float], align 16      ; [#uses=2 type=[256 x float]*]
  %tmp_imag = alloca [256 x float], align 16      ; [#uses=2 type=[256 x float]*]
  call void @llvm.dbg.value(metadata !{[256 x float]* %sample_real}, i64 0, metadata !17), !dbg !30 ; [debug line = 7:16] [debug variable = sample_real]
  call void @llvm.dbg.value(metadata !{[256 x float]* %sample_imag}, i64 0, metadata !31), !dbg !32 ; [debug line = 7:58] [debug variable = sample_imag]
  call void @llvm.dbg.declare(metadata !{[256 x float]* %tmp_real}, metadata !33), !dbg !36 ; [debug line = 11:8] [debug variable = tmp_real]
  call void @llvm.dbg.declare(metadata !{[256 x float]* %tmp_imag}, metadata !37), !dbg !38 ; [debug line = 12:8] [debug variable = tmp_imag]
  br label %.loopexit, !dbg !39                   ; [debug line = 14:15]

.loopexit:                                        ; preds = %2, %0
  %i = phi i9 [ 0, %0 ], [ %i_2, %2 ]             ; [#uses=4 type=i9]
  %tmp_1 = trunc i9 %i to i8, !dbg !41            ; [#uses=1 type=i8] [debug line = 25:4]
  %exitcond2 = icmp eq i9 %i, -256, !dbg !39      ; [#uses=1 type=i1] [debug line = 14:15]
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 256, i64 256, i64 256) nounwind ; [#uses=0 type=i32]
  %i_2 = add i9 1, %i, !dbg !45                   ; [#uses=1 type=i9] [debug line = 14:44]
  call void @llvm.dbg.value(metadata !{i9 %i_2}, i64 0, metadata !46), !dbg !45 ; [debug line = 14:44] [debug variable = i]
  br i1 %exitcond2, label %.preheader, label %1, !dbg !39 ; [debug line = 14:15]

; <label>:1                                       ; preds = %.loopexit
  %tmp = zext i9 %i to i64, !dbg !48              ; [#uses=2 type=i64] [debug line = 15:3]
  %tmp_real_addr = getelementptr inbounds [256 x float]* %tmp_real, i64 0, i64 %tmp, !dbg !48 ; [#uses=2 type=float*] [debug line = 15:3]
  store float 0.000000e+00, float* %tmp_real_addr, align 4, !dbg !48 ; [debug line = 15:3]
  %tmp_imag_addr = getelementptr inbounds [256 x float]* %tmp_imag, i64 0, i64 %tmp, !dbg !49 ; [#uses=1 type=float*] [debug line = 16:3]
  br label %2, !dbg !50                           ; [debug line = 21:16]

; <label>:2                                       ; preds = %3, %1
  %storemerge = phi float [ 0.000000e+00, %1 ], [ %tmp_13, %3 ] ; [#uses=2 type=float]
  %tmp_2 = phi float [ 0.000000e+00, %1 ], [ %tmp_7, %3 ] ; [#uses=1 type=float]
  %j = phi i9 [ 0, %1 ], [ %j_1, %3 ]             ; [#uses=4 type=i9]
  store float %storemerge, float* %tmp_imag_addr, align 4, !dbg !51 ; [debug line = 30:4]
  %tmp_14 = trunc i9 %j to i8, !dbg !41           ; [#uses=1 type=i8] [debug line = 25:4]
  %exitcond1 = icmp eq i9 %j, -256, !dbg !50      ; [#uses=1 type=i1] [debug line = 21:16]
  %empty_3 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 256, i64 256, i64 256) nounwind ; [#uses=0 type=i32]
  %j_1 = add i9 1, %j, !dbg !52                   ; [#uses=1 type=i9] [debug line = 21:45]
  br i1 %exitcond1, label %.loopexit, label %3, !dbg !50 ; [debug line = 21:16]

; <label>:3                                       ; preds = %2
  %tmp_6 = mul i8 %tmp_1, %tmp_14, !dbg !41       ; [#uses=1 type=i8] [debug line = 25:4]
  %tmp_8 = zext i8 %tmp_6 to i64, !dbg !41        ; [#uses=2 type=i64] [debug line = 25:4]
  %cos_coefficients_table_addr = getelementptr inbounds [256 x float]* @cos_coefficients_table, i64 0, i64 %tmp_8, !dbg !41 ; [#uses=1 type=float*] [debug line = 25:4]
  %c = load float* %cos_coefficients_table_addr, align 4, !dbg !41 ; [#uses=2 type=float] [debug line = 25:4]
  call void @llvm.dbg.value(metadata !{float %c}, i64 0, metadata !53), !dbg !41 ; [debug line = 25:4] [debug variable = c]
  %sin_coefficients_table_addr = getelementptr inbounds [256 x float]* @sin_coefficients_table, i64 0, i64 %tmp_8, !dbg !54 ; [#uses=1 type=float*] [debug line = 26:4]
  %s = load float* %sin_coefficients_table_addr, align 4, !dbg !54 ; [#uses=2 type=float] [debug line = 26:4]
  call void @llvm.dbg.value(metadata !{float %s}, i64 0, metadata !55), !dbg !54 ; [debug line = 26:4] [debug variable = s]
  %tmp_9 = zext i9 %j to i64, !dbg !56            ; [#uses=2 type=i64] [debug line = 29:4]
  %sample_real_addr_1 = getelementptr [256 x float]* %sample_real, i64 0, i64 %tmp_9, !dbg !56 ; [#uses=1 type=float*] [debug line = 29:4]
  %sample_real_load = load float* %sample_real_addr_1, align 4, !dbg !56 ; [#uses=2 type=float] [debug line = 29:4]
  %tmp_s = fmul float %sample_real_load, %c, !dbg !56 ; [#uses=1 type=float] [debug line = 29:4]
  %sample_imag_addr_1 = getelementptr [256 x float]* %sample_imag, i64 0, i64 %tmp_9, !dbg !56 ; [#uses=1 type=float*] [debug line = 29:4]
  %sample_imag_load = load float* %sample_imag_addr_1, align 4, !dbg !56 ; [#uses=2 type=float] [debug line = 29:4]
  %tmp_4 = fmul float %sample_imag_load, %s, !dbg !56 ; [#uses=1 type=float] [debug line = 29:4]
  %tmp_5 = fsub float %tmp_s, %tmp_4, !dbg !56    ; [#uses=1 type=float] [debug line = 29:4]
  %tmp_7 = fadd float %tmp_2, %tmp_5, !dbg !56    ; [#uses=2 type=float] [debug line = 29:4]
  store float %tmp_7, float* %tmp_real_addr, align 4, !dbg !56 ; [debug line = 29:4]
  %tmp_10 = fmul float %sample_real_load, %s, !dbg !51 ; [#uses=1 type=float] [debug line = 30:4]
  %tmp_11 = fmul float %sample_imag_load, %c, !dbg !51 ; [#uses=1 type=float] [debug line = 30:4]
  %tmp_12 = fadd float %tmp_10, %tmp_11, !dbg !51 ; [#uses=1 type=float] [debug line = 30:4]
  %tmp_13 = fadd float %storemerge, %tmp_12, !dbg !51 ; [#uses=1 type=float] [debug line = 30:4]
  call void @llvm.dbg.value(metadata !{i9 %j_1}, i64 0, metadata !57), !dbg !52 ; [debug line = 21:45] [debug variable = j]
  br label %2, !dbg !52                           ; [debug line = 21:45]

.preheader:                                       ; preds = %4, %.loopexit
  %i1 = phi i9 [ %i_1, %4 ], [ 0, %.loopexit ]    ; [#uses=3 type=i9]
  %exitcond = icmp eq i9 %i1, -256, !dbg !58      ; [#uses=1 type=i1] [debug line = 34:15]
  %empty_4 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 256, i64 256, i64 256) nounwind ; [#uses=0 type=i32]
  %i_1 = add i9 %i1, 1, !dbg !60                  ; [#uses=1 type=i9] [debug line = 34:44]
  br i1 %exitcond, label %5, label %4, !dbg !58   ; [debug line = 34:15]

; <label>:4                                       ; preds = %.preheader
  %tmp_3 = zext i9 %i1 to i64, !dbg !61           ; [#uses=4 type=i64] [debug line = 35:3]
  %tmp_real_addr_1 = getelementptr inbounds [256 x float]* %tmp_real, i64 0, i64 %tmp_3, !dbg !61 ; [#uses=1 type=float*] [debug line = 35:3]
  %tmp_real_load = load float* %tmp_real_addr_1, align 4, !dbg !61 ; [#uses=1 type=float] [debug line = 35:3]
  %sample_real_addr = getelementptr [256 x float]* %sample_real, i64 0, i64 %tmp_3, !dbg !61 ; [#uses=1 type=float*] [debug line = 35:3]
  store float %tmp_real_load, float* %sample_real_addr, align 4, !dbg !61 ; [debug line = 35:3]
  %tmp_imag_addr_1 = getelementptr inbounds [256 x float]* %tmp_imag, i64 0, i64 %tmp_3, !dbg !63 ; [#uses=1 type=float*] [debug line = 36:3]
  %tmp_imag_load = load float* %tmp_imag_addr_1, align 4, !dbg !63 ; [#uses=1 type=float] [debug line = 36:3]
  %sample_imag_addr = getelementptr [256 x float]* %sample_imag, i64 0, i64 %tmp_3, !dbg !63 ; [#uses=1 type=float*] [debug line = 36:3]
  store float %tmp_imag_load, float* %sample_imag_addr, align 4, !dbg !63 ; [debug line = 36:3]
  call void @llvm.dbg.value(metadata !{i9 %i_1}, i64 0, metadata !64), !dbg !60 ; [debug line = 34:44] [debug variable = i]
  br label %.preheader, !dbg !60                  ; [debug line = 34:44]

; <label>:5                                       ; preds = %.preheader
  ret void, !dbg !65                              ; [debug line = 39:1]
}

; [#uses=1]
define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

; [#uses=3]
define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

; [#uses=2]
define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

; [#uses=0]
declare i8 @_ssdm_op_PartSelect.i8.i9.i32.i32(i9, i32, i32) nounwind readnone

; [#uses=0]
declare i16 @_ssdm_op_HSub(...)

; [#uses=0]
declare i16 @_ssdm_op_HMul(...)

; [#uses=0]
declare i16 @_ssdm_op_HDiv(...)

; [#uses=0]
declare i16 @_ssdm_op_HAdd(...)

; [#uses=1]
declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [1 x i32]* @llvm_global_ctors_0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 0, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 31, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"sample_real", metadata !11, metadata !"float", i32 0, i32 31}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 255, i32 1}
!13 = metadata !{metadata !14}
!14 = metadata !{i32 0, i32 31, metadata !15}
!15 = metadata !{metadata !16}
!16 = metadata !{metadata !"sample_imag", metadata !11, metadata !"float", i32 0, i32 31}
!17 = metadata !{i32 786689, metadata !18, metadata !"sample_real", null, i32 7, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!18 = metadata !{i32 786478, i32 0, metadata !19, metadata !"dft", metadata !"dft", metadata !"_Z3dftPfS_", metadata !19, i32 7, metadata !20, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !25, i32 8} ; [ DW_TAG_subprogram ]
!19 = metadata !{i32 786473, metadata !"dft.cpp", metadata !"d:/Projects/vivado/project_3/dft_256_precomputed", null} ; [ DW_TAG_file_type ]
!20 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !21, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!21 = metadata !{null, metadata !22, metadata !22}
!22 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !23} ; [ DW_TAG_pointer_type ]
!23 = metadata !{i32 786454, null, metadata !"DTYPE", metadata !19, i32 9, i64 0, i64 0, i64 0, i32 0, metadata !24} ; [ DW_TAG_typedef ]
!24 = metadata !{i32 786468, null, metadata !"float", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 4} ; [ DW_TAG_base_type ]
!25 = metadata !{metadata !26}
!26 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!27 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 0, i64 0, i32 0, i32 0, metadata !23, metadata !28, i32 0, i32 0} ; [ DW_TAG_array_type ]
!28 = metadata !{metadata !29}
!29 = metadata !{i32 786465, i64 0, i64 255}      ; [ DW_TAG_subrange_type ]
!30 = metadata !{i32 7, i32 16, metadata !18, null}
!31 = metadata !{i32 786689, metadata !18, metadata !"sample_imag", null, i32 7, metadata !27, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!32 = metadata !{i32 7, i32 58, metadata !18, null}
!33 = metadata !{i32 786688, metadata !34, metadata !"tmp_real", metadata !19, i32 11, metadata !35, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!34 = metadata !{i32 786443, metadata !18, i32 8, i32 1, metadata !19, i32 0} ; [ DW_TAG_lexical_block ]
!35 = metadata !{i32 786433, null, metadata !"", null, i32 0, i64 8192, i64 32, i32 0, i32 0, metadata !23, metadata !28, i32 0, i32 0} ; [ DW_TAG_array_type ]
!36 = metadata !{i32 11, i32 8, metadata !34, null}
!37 = metadata !{i32 786688, metadata !34, metadata !"tmp_imag", metadata !19, i32 12, metadata !35, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!38 = metadata !{i32 12, i32 8, metadata !34, null}
!39 = metadata !{i32 14, i32 15, metadata !40, null}
!40 = metadata !{i32 786443, metadata !34, i32 14, i32 2, metadata !19, i32 1} ; [ DW_TAG_lexical_block ]
!41 = metadata !{i32 25, i32 4, metadata !42, null}
!42 = metadata !{i32 786443, metadata !43, i32 21, i32 50, metadata !19, i32 4} ; [ DW_TAG_lexical_block ]
!43 = metadata !{i32 786443, metadata !44, i32 21, i32 3, metadata !19, i32 3} ; [ DW_TAG_lexical_block ]
!44 = metadata !{i32 786443, metadata !40, i32 14, i32 49, metadata !19, i32 2} ; [ DW_TAG_lexical_block ]
!45 = metadata !{i32 14, i32 44, metadata !40, null}
!46 = metadata !{i32 786688, metadata !40, metadata !"i", metadata !19, i32 14, metadata !47, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!47 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!48 = metadata !{i32 15, i32 3, metadata !44, null}
!49 = metadata !{i32 16, i32 3, metadata !44, null}
!50 = metadata !{i32 21, i32 16, metadata !43, null}
!51 = metadata !{i32 30, i32 4, metadata !42, null}
!52 = metadata !{i32 21, i32 45, metadata !43, null}
!53 = metadata !{i32 786688, metadata !34, metadata !"c", metadata !19, i32 9, metadata !23, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!54 = metadata !{i32 26, i32 4, metadata !42, null}
!55 = metadata !{i32 786688, metadata !34, metadata !"s", metadata !19, i32 9, metadata !23, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!56 = metadata !{i32 29, i32 4, metadata !42, null}
!57 = metadata !{i32 786688, metadata !43, metadata !"j", metadata !19, i32 21, metadata !47, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!58 = metadata !{i32 34, i32 15, metadata !59, null}
!59 = metadata !{i32 786443, metadata !34, i32 34, i32 2, metadata !19, i32 5} ; [ DW_TAG_lexical_block ]
!60 = metadata !{i32 34, i32 44, metadata !59, null}
!61 = metadata !{i32 35, i32 3, metadata !62, null}
!62 = metadata !{i32 786443, metadata !59, i32 34, i32 49, metadata !19, i32 6} ; [ DW_TAG_lexical_block ]
!63 = metadata !{i32 36, i32 3, metadata !62, null}
!64 = metadata !{i32 786688, metadata !59, metadata !"i", metadata !19, i32 34, metadata !47, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!65 = metadata !{i32 39, i32 1, metadata !34, null}
