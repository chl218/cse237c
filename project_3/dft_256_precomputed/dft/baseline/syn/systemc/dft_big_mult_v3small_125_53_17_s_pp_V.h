// ==============================================================
// File generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ==============================================================

#ifndef __dft_big_mult_v3small_125_53_17_s_pp_V_H__
#define __dft_big_mult_v3small_125_53_17_s_pp_V_H__


#include <systemc>
using namespace sc_core;
using namespace sc_dt;




#include <iostream>
#include <fstream>

struct dft_big_mult_v3small_125_53_17_s_pp_V_ram : public sc_core::sc_module {

  static const unsigned DataWidth = 70;
  static const unsigned AddressRange = 8;
  static const unsigned AddressWidth = 3;

//latency = 1
//input_reg = 1
//output_reg = 0
sc_core::sc_in <sc_lv<AddressWidth> > address0;
sc_core::sc_in <sc_logic> ce0;
sc_core::sc_out <sc_lv<DataWidth> > q0;
sc_core::sc_in<sc_logic> reset;
sc_core::sc_in<bool> clk;


sc_lv<DataWidth> ram[AddressRange];


   SC_CTOR(dft_big_mult_v3small_125_53_17_s_pp_V_ram) {
        ram[0] = "0b1011011100111110000100001100001010011010100000000000000000000000000000";
        ram[1] = "0b0000001111010000100001001110110001011110000000000000000000000000000000";
        ram[2] = "0b1001000110000111001101111110011111111001100000000000000000000000000000";
        ram[3] = "0b0110110111000111000011100001110011000011100000000000000000000000000000";
        ram[4] = "0b0000001110101100000110010001110010100110100000000000000000000000000000";
        ram[5] = "0b0000000000000000000000000000000000000000000000000000000000000000000000";
        ram[6] = "0b0000000000000000000000000000000000000000000000000000000000000000000000";
        ram[7] = "0b0000000000000000000000000000000000000000000000000000000000000000000000";


SC_METHOD(prc_write_0);
  sensitive<<clk.pos();
   }


void prc_write_0()
{
    if (ce0.read() == sc_dt::Log_1) 
    {
            if(address0.read().is_01() && address0.read().to_uint()<AddressRange)
              q0 = ram[address0.read().to_uint()];
            else
              q0 = sc_lv<DataWidth>();
    }
}


}; //endmodule


SC_MODULE(dft_big_mult_v3small_125_53_17_s_pp_V) {


static const unsigned DataWidth = 70;
static const unsigned AddressRange = 8;
static const unsigned AddressWidth = 3;

sc_core::sc_in <sc_lv<AddressWidth> > address0;
sc_core::sc_in<sc_logic> ce0;
sc_core::sc_out <sc_lv<DataWidth> > q0;
sc_core::sc_in<sc_logic> reset;
sc_core::sc_in<bool> clk;


dft_big_mult_v3small_125_53_17_s_pp_V_ram* meminst;


SC_CTOR(dft_big_mult_v3small_125_53_17_s_pp_V) {
meminst = new dft_big_mult_v3small_125_53_17_s_pp_V_ram("dft_big_mult_v3small_125_53_17_s_pp_V_ram");
meminst->address0(address0);
meminst->ce0(ce0);
meminst->q0(q0);

meminst->reset(reset);
meminst->clk(clk);
}
~dft_big_mult_v3small_125_53_17_s_pp_V() {
    delete meminst;
}


};//endmodule
#endif
