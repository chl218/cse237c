; ModuleID = 'D:/Projects/vivado/project_3/dft_256_precomputed/dft/baseline/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@pp_V = internal constant [8 x i70] [i70 -335535007245497532416, i70 17591406591603638272, i70 -509461270201035128832, i70 506259634380888604672, i70 16935301692876914688, i70 0, i70 0, i70 0]
@llvm_global_ctors_1 = appending global [3 x void ()*] [void ()* @_GLOBAL__I_a, void ()* @_GLOBAL__I_a8, void ()* @_GLOBAL__I_a1942]
@llvm_global_ctors_0 = appending global [3 x i32] [i32 65535, i32 65535, i32 65535]
@hls_KD_KD_cordic_KD_KD_addsu_2 = internal unnamed_addr constant [142 x i8] c"hls::cordic::addsub<0, 0, ap_fixed<79, 3, (ap_q_mode)5, (ap_o_mode)3, 0>, ap_fixed<79, 3, (ap_q_mode)0, (ap_o_mode)3, 0>, ap_uint<1> >.region\00"
@hls_KD_KD_cordic_KD_KD_addsu_1 = internal unnamed_addr constant [142 x i8] c"hls::cordic::addsub<0, 0, ap_fixed<79, 3, (ap_q_mode)5, (ap_o_mode)3, 0>, ap_fixed<79, 3, (ap_q_mode)5, (ap_o_mode)3, 0>, ap_uint<1> >.region\00"
@hls_KD_KD_cordic_KD_KD_addsu = internal unnamed_addr constant [135 x i8] c"hls::cordic::addsub<0, 0, ap_fixed<79, 3, (ap_q_mode)5, (ap_o_mode)3, 0>, ap_fixed<79, 3, (ap_q_mode)5, (ap_o_mode)3, 0>, bool>.region\00"
@hls_cordic_ctab_table_128_V = internal unnamed_addr constant [128 x i126] [i126 -18256305226174194124613628535325128908, i126 39442776452007029438581988151662170528, i126 20840479833484141535907734465581556094, i126 10578952970703094087978879161197675639, i126 5310005101453174985675347574176397365, i126 2657591115466887314119419599185582276, i126 1329119838776566165767415771090515808, i126 664600476781209930782809735563259991, i126 332305308760902745757206114308150239, i126 166153288198498015084967484089993543, i126 83076723327184848864159991914295098, i126 41538371567105655170464436460279954, i126 20769187021492645509723191025817333, i126 10384593665488820748005411471848144, i126 5192296852087223271663992833358155, i126 2596148428461463268305855343910053, i126 1298074214532963088845478733246034, i126 649037107304260476279099458567081, i126 324518553656852604622213216067037, i126 162259276829016598121458871234286, i126 81129638414582086037024072497015, i126 40564819207300266390548884732859, i126 20282409603651286116779049016797, i126 10141204801825787173577600358126, i126 5070602400912911601187309660855, i126 2535301200456458052393468515669, i126 1267650600228229307671710968490, i126 633825300114114689020227573077, i126 316912650057057348908160297642, i126 158456325028528675003835962709, i126 79228162514264337570637458090, i126 39614081257132168793908663637, i126 19807040628566084398028073642, i126 9903520314283042199148254549, i126 4951760157141521099590904490, i126 2475880078570760549797549397, i126 1237940039285380274899036842, i126 618970019642690137449551189, i126 309485009821345068724779690, i126 154742504910672534362390357, i126 77371252455336267181195242, i126 38685626227668133590597629, i126 19342813113834066795298815, i126 9671406556917033397649407, i126 4835703278458516698824703, i126 2417851639229258349412351, i126 1208925819614629174706175, i126 604462909807314587353087, i126 302231454903657293676543, i126 151115727451828646838271, i126 75557863725914323419135, i126 37778931862957161709567, i126 18889465931478580854783, i126 9444732965739290427391, i126 4722366482869645213695, i126 2361183241434822606847, i126 1180591620717411303423, i126 590295810358705651711, i126 295147905179352825855, i126 147573952589676412927, i126 73786976294838206463, i126 36893488147419103231, i126 18446744073709551615, i126 9223372036854775807, i126 4611686018427387903, i126 2305843009213693951, i126 1152921504606846975, i126 576460752303423487, i126 288230376151711743, i126 144115188075855871, i126 72057594037927935, i126 36028797018963967, i126 18014398509481983, i126 9007199254740991, i126 4503599627370495, i126 2251799813685247, i126 1125899906842623, i126 562949953421311, i126 281474976710655, i126 140737488355327, i126 70368744177663, i126 35184372088831, i126 17592186044415, i126 8796093022207, i126 4398046511103, i126 2199023255551, i126 1099511627775, i126 549755813887, i126 274877906943, i126 137438953471, i126 68719476735, i126 34359738367, i126 17179869183, i126 8589934591, i126 4294967295, i126 2147483647, i126 1073741823, i126 536870911, i126 268435455, i126 134217727, i126 67108863, i126 33554431, i126 16777215, i126 8388607, i126 4194303, i126 2097151, i126 1048575, i126 524287, i126 262143, i126 131071, i126 65535, i126 32767, i126 16383, i126 8191, i126 4095, i126 2047, i126 1023, i126 511, i126 255, i126 127, i126 63, i126 31, i126 15, i126 7, i126 3, i126 1, i126 0, i126 0]
@dft_str = internal unnamed_addr constant [4 x i8] c"dft\00"
@p_str1804 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1

define internal fastcc { double, double } @dft_sin_cos_range_redux_cordic(double %t_in) readnone {
_ZrsILi78ELb0EE11ap_int_baseIXT_EXT0_EXleT_Li64EEERKS1_i.exit:
  %t_in_read = call double @_ssdm_op_Read.ap_auto.double(double %t_in)
  %p_Val2_3 = bitcast double %t_in_read to i64
  %p_Result_s = call i1 @_ssdm_op_BitSelect.i1.i64.i32(i64 %p_Val2_3, i32 63)
  %loc_V = call i11 @_ssdm_op_PartSelect.i11.i64.i32.i32(i64 %p_Val2_3, i32 52, i32 62)
  %loc_V_1 = trunc i64 %p_Val2_3 to i52
  %tmp_i_cast = zext i11 %loc_V to i12
  %exp = add i12 -1023, %tmp_i_cast
  %tmp_6 = call i1 @_ssdm_op_BitSelect.i1.i12.i32(i12 %exp, i32 11)
  br i1 %tmp_6, label %0, label %_ZlsILi53ELb0EE11ap_int_baseIXT_EXT0_EXleT_Li64EEERKS1_i.exit.i

_ZlsILi53ELb0EE11ap_int_baseIXT_EXT0_EXleT_Li64EEERKS1_i.exit.i: ; preds = %_ZrsILi78ELb0EE11ap_int_baseIXT_EXT0_EXleT_Li64EEERKS1_i.exit
  %p_Val2_4 = call fastcc i178 @"dft_big_mult_v3small<125, 53, 17>"()
  %h_fract_V = call i78 @_ssdm_op_PartSelect.i78.i178.i32.i32(i178 %p_Val2_4, i32 44, i32 121)
  %p_Val2_8 = call i3 @_ssdm_op_PartSelect.i3.i178.i32.i32(i178 %p_Val2_4, i32 122, i32 124)
  %m_m_V = call fastcc i188 @"dft_big_mult_v3<94, 17>"(i78 %h_fract_V)
  %prod_adj_V = call i79 @_ssdm_op_PartSelect.i79.i188.i32.i32(i188 %m_m_V, i32 77, i32 155)
  %tmp_10 = call i1 @_ssdm_op_BitSelect.i1.i178.i32(i178 %p_Val2_4, i32 122)
  %tmp_4_i = sub i79 -129718850602772264754589, %prod_adj_V
  %tmp_1_i = call i78 @_ssdm_op_PartSelect.i78.i188.i32.i32(i188 %m_m_V, i32 78, i32 155)
  %tmp_2_i = call i78 @_ssdm_op_PartSelect.i78.i79.i32.i32(i79 %tmp_4_i, i32 1, i32 78)
  %tmp_3_i = select i1 %tmp_10, i78 %tmp_2_i, i78 %tmp_1_i
  br label %"range_redux_payne_hanek<16, 78, double>.exit"

; <label>:0                                       ; preds = %_ZrsILi78ELb0EE11ap_int_baseIXT_EXT0_EXleT_Li64EEERKS1_i.exit
  %r_V = call i78 @_ssdm_op_BitConcatenate.i78.i1.i52.i25(i1 true, i52 %loc_V_1, i25 0)
  %op2_assign = sub i12 1022, %tmp_i_cast
  %op2_assign_i_cast = sext i12 %op2_assign to i32
  %tmp_27_i_i = zext i32 %op2_assign_i_cast to i78
  %tmp_28_i_i = lshr i78 %r_V, %tmp_27_i_i
  br label %"range_redux_payne_hanek<16, 78, double>.exit"

"range_redux_payne_hanek<16, 78, double>.exit":   ; preds = %0, %_ZlsILi53ELb0EE11ap_int_baseIXT_EXT0_EXleT_Li64EEERKS1_i.exit.i
  %k_V = phi i3 [ %p_Val2_8, %_ZlsILi53ELb0EE11ap_int_baseIXT_EXT0_EXleT_Li64EEERKS1_i.exit.i ], [ 0, %0 ]
  %dout_V = phi i78 [ %tmp_3_i, %_ZlsILi53ELb0EE11ap_int_baseIXT_EXT0_EXleT_Li64EEERKS1_i.exit.i ], [ %tmp_28_i_i, %0 ]
  %r_V_7 = call i76 @_ssdm_op_PartSelect.i76.i78.i32.i32(i78 %dout_V, i32 2, i32 77)
  %loc_V_2 = zext i76 %r_V_7 to i79
  br label %1

; <label>:1                                       ; preds = %"operator>>.exit100.i", %"range_redux_payne_hanek<16, 78, double>.exit"
  %p_Val2_7 = phi i79 [ %loc_V_2, %"range_redux_payne_hanek<16, 78, double>.exit" ], [ %tz_V, %"operator>>.exit100.i" ]
  %p_Val2_6 = phi i79 [ 0, %"range_redux_payne_hanek<16, 78, double>.exit" ], [ %ty_V, %"operator>>.exit100.i" ]
  %p_Val2_5 = phi i79 [ 45882734510562557198175, %"range_redux_payne_hanek<16, 78, double>.exit" ], [ %tx_V, %"operator>>.exit100.i" ]
  %sh_assign = phi i7 [ 0, %"range_redux_payne_hanek<16, 78, double>.exit" ], [ %k, %"operator>>.exit100.i" ]
  %exitcond_i = icmp eq i7 %sh_assign, -50
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 78, i64 78, i64 78)
  %k = add i7 %sh_assign, 1
  br i1 %exitcond_i, label %"cordic_circ_v1<78, 0, 0, 0, 79, 3, 79, 3>.exit_ifconv", label %"operator>>.exit100.i"

"operator>>.exit100.i":                           ; preds = %1
  %d_V = call i1 @_ssdm_op_BitSelect.i1.i79.i32(i79 %p_Val2_7, i32 77)
  %tmp_9 = zext i7 %sh_assign to i79
  %y_s_V = ashr i79 %p_Val2_6, %tmp_9
  %rbegin = call i32 (...)* @_ssdm_op_SpecRegionBegin([142 x i8]* @hls_KD_KD_cordic_KD_KD_addsu_1)
  call void (...)* @_ssdm_op_SpecLatency(i32 0, i32 0, [1 x i8]* @p_str1804) nounwind
  %r_V_4_i = sub i79 %p_Val2_5, %y_s_V
  %r_V_i = add i79 %p_Val2_5, %y_s_V
  %tx_V = select i1 %d_V, i79 %r_V_i, i79 %r_V_4_i
  %rend = call i32 (...)* @_ssdm_op_SpecRegionEnd([142 x i8]* @hls_KD_KD_cordic_KD_KD_addsu_1, i32 %rbegin)
  %x_s_V = ashr i79 %p_Val2_5, %tmp_9
  %rbegin1 = call i32 (...)* @_ssdm_op_SpecRegionBegin([135 x i8]* @hls_KD_KD_cordic_KD_KD_addsu)
  call void (...)* @_ssdm_op_SpecLatency(i32 0, i32 0, [1 x i8]* @p_str1804) nounwind
  %r_V_3_i = sub i79 %p_Val2_6, %x_s_V
  %r_V_i5 = add i79 %p_Val2_6, %x_s_V
  %ty_V = select i1 %d_V, i79 %r_V_3_i, i79 %r_V_i5
  %rend2 = call i32 (...)* @_ssdm_op_SpecRegionEnd([135 x i8]* @hls_KD_KD_cordic_KD_KD_addsu, i32 %rbegin1)
  %tmp_1 = zext i7 %sh_assign to i64
  %hls_cordic_ctab_table_128_V_a = getelementptr [128 x i126]* @hls_cordic_ctab_table_128_V, i64 0, i64 %tmp_1
  %p_Val2_18 = load i126* %hls_cordic_ctab_table_128_V_a, align 16
  %p_Val2_14_cast = call i76 @_ssdm_op_PartSelect.i76.i126.i32.i32(i126 %p_Val2_18, i32 50, i32 125)
  %tmp_36 = call i1 @_ssdm_op_BitSelect.i1.i126.i32(i126 %p_Val2_18, i32 49)
  %tmp_3_cast = zext i1 %tmp_36 to i76
  %p_Val2_s = add i76 %tmp_3_cast, %p_Val2_14_cast
  %p_Val2_cast = zext i76 %p_Val2_s to i79
  %rbegin7 = call i32 (...)* @_ssdm_op_SpecRegionBegin([142 x i8]* @hls_KD_KD_cordic_KD_KD_addsu_2)
  call void (...)* @_ssdm_op_SpecLatency(i32 0, i32 0, [1 x i8]* @p_str1804) nounwind
  %r_V_5_i = sub i79 %p_Val2_7, %p_Val2_cast
  %r_V_i1 = add i79 %p_Val2_7, %p_Val2_cast
  %tz_V = select i1 %d_V, i79 %r_V_i1, i79 %r_V_5_i
  %rend8 = call i32 (...)* @_ssdm_op_SpecRegionEnd([142 x i8]* @hls_KD_KD_cordic_KD_KD_addsu_2, i32 %rbegin7)
  br label %1

"cordic_circ_v1<78, 0, 0, 0, 79, 3, 79, 3>.exit_ifconv": ; preds = %1
  %tmp = icmp eq i76 %r_V_7, 0
  %tmp_12 = trunc i79 %p_Val2_6 to i77
  %r_V_3 = call i78 @_ssdm_op_BitConcatenate.i78.i77.i1(i77 %tmp_12, i1 false)
  %tmp_14 = trunc i79 %p_Val2_5 to i77
  %r_V_4 = call i78 @_ssdm_op_BitConcatenate.i78.i77.i1(i77 %tmp_14, i1 false)
  %p_Val2_10 = select i1 %tmp, i78 0, i78 %r_V_3
  %p_Val2_13 = select i1 %tmp, i78 -151115727451828646838272, i78 %r_V_4
  %p_Result_i1 = call i16 @_ssdm_op_PartSelect.i16.i78.i32.i32(i78 %p_Val2_10, i32 62, i32 77)
  %p_Result_2 = call i32 @_ssdm_op_BitConcatenate.i32.i16.i16(i16 %p_Result_i1, i16 -32768)
  %p_Result_4_i = call i16 @_ssdm_op_PartSelect.i16.i78.i32.i32(i78 %p_Val2_10, i32 46, i32 61)
  %p_Result_3 = call i32 @_ssdm_op_BitConcatenate.i32.i16.i16(i16 %p_Result_4_i, i16 -32768)
  %p_Result_6_i = call i16 @_ssdm_op_PartSelect.i16.i78.i32.i32(i78 %p_Val2_10, i32 30, i32 45)
  %p_Result_4 = call i32 @_ssdm_op_BitConcatenate.i32.i16.i16(i16 %p_Result_6_i, i16 -32768)
  %p_Result_8_i = call i16 @_ssdm_op_PartSelect.i16.i78.i32.i32(i78 %p_Val2_10, i32 14, i32 29)
  %p_Result_5 = call i32 @_ssdm_op_BitConcatenate.i32.i16.i16(i16 %p_Result_8_i, i16 -32768)
  %c1 = call i32 @llvm.ctlz.i32(i32 %p_Result_2, i1 true)
  %c2 = call i32 @llvm.ctlz.i32(i32 %p_Result_3, i1 true)
  %c3 = call i32 @llvm.ctlz.i32(i32 %p_Result_4, i1 true)
  %c4 = call i32 @llvm.ctlz.i32(i32 %p_Result_5, i1 true)
  %tmp_i = zext i32 %c1 to i78
  %tmp_i_25 = shl i78 %p_Val2_10, %tmp_i
  %tmp_17_i = icmp eq i32 %c1, 16
  %shift = add nsw i32 %c2, 16
  %tmp_18_i = zext i32 %c2 to i78
  %tmp_19_i = shl i78 %tmp_i_25, %tmp_18_i
  %tmp_20_i = icmp eq i32 %c2, 16
  %shift_1 = add nsw i32 %c3, %shift
  %tmp_21_i = zext i32 %c3 to i78
  %tmp_22_i = shl i78 %tmp_19_i, %tmp_21_i
  %tmp_23_i = icmp eq i32 %c3, 16
  %shift_2 = add nsw i32 %c4, %shift_1
  %tmp_24_i = zext i32 %c4 to i78
  %tmp_25_i = shl i78 %tmp_22_i, %tmp_24_i
  %tmp1 = and i1 %tmp_20_i, %tmp_23_i
  %sel_tmp1_i = and i1 %tmp1, %tmp_17_i
  %sel_tmp5_i = xor i1 %tmp_20_i, true
  %sel_tmp6_i = and i1 %tmp_17_i, %sel_tmp5_i
  %p_v = select i1 %sel_tmp1_i, i32 %shift_2, i32 %shift_1
  %tmp_15 = trunc i32 %shift to i11
  %tmp_16 = trunc i32 %p_v to i11
  %tmp_19 = trunc i32 %c1 to i11
  %tmp_20 = select i1 %tmp_17_i, i11 %tmp_16, i11 %tmp_19
  %tmp_21 = select i1 %sel_tmp6_i, i11 %tmp_15, i11 %tmp_20
  %tmp_4 = call i52 @_ssdm_op_PartSelect.i52.i78.i32.i32(i78 %tmp_19_i, i32 25, i32 76)
  %tmp_22 = call i52 @_ssdm_op_PartSelect.i52.i78.i32.i32(i78 %tmp_25_i, i32 25, i32 76)
  %tmp_25 = call i52 @_ssdm_op_PartSelect.i52.i78.i32.i32(i78 %tmp_22_i, i32 25, i32 76)
  %tmp_26 = select i1 %sel_tmp1_i, i52 %tmp_22, i52 %tmp_25
  %tmp_2 = call i52 @_ssdm_op_PartSelect.i52.i78.i32.i32(i78 %tmp_i_25, i32 25, i32 76)
  %tmp_3 = select i1 %tmp_17_i, i52 %tmp_26, i52 %tmp_2
  %loc_V_3 = select i1 %sel_tmp6_i, i52 %tmp_4, i52 %tmp_3
  %tmp_26_i = icmp eq i78 %p_Val2_10, 0
  %phitmp_i = sub i11 1023, %tmp_21
  %out_exp_V = select i1 %tmp_26_i, i11 0, i11 %phitmp_i
  %p_Result_6 = call i64 @_ssdm_op_BitConcatenate.i64.i1.i11.i52(i1 false, i11 %out_exp_V, i52 %loc_V_3)
  %tmp_s_out = bitcast i64 %p_Result_6 to double
  %p_Result_i2 = call i16 @_ssdm_op_PartSelect.i16.i78.i32.i32(i78 %p_Val2_13, i32 62, i32 77)
  %p_Result_7 = call i32 @_ssdm_op_BitConcatenate.i32.i16.i16(i16 %p_Result_i2, i16 -32768)
  %p_Result_4_i1 = call i16 @_ssdm_op_PartSelect.i16.i78.i32.i32(i78 %p_Val2_13, i32 46, i32 61)
  %p_Result_8 = call i32 @_ssdm_op_BitConcatenate.i32.i16.i16(i16 %p_Result_4_i1, i16 -32768)
  %p_Result_6_i1 = call i16 @_ssdm_op_PartSelect.i16.i78.i32.i32(i78 %p_Val2_13, i32 30, i32 45)
  %p_Result_9 = call i32 @_ssdm_op_BitConcatenate.i32.i16.i16(i16 %p_Result_6_i1, i16 -32768)
  %p_Result_8_i1 = call i16 @_ssdm_op_PartSelect.i16.i78.i32.i32(i78 %p_Val2_13, i32 14, i32 29)
  %p_Result_10 = call i32 @_ssdm_op_BitConcatenate.i32.i16.i16(i16 %p_Result_8_i1, i16 -32768)
  %c1_1 = call i32 @llvm.ctlz.i32(i32 %p_Result_7, i1 true)
  %c2_1 = call i32 @llvm.ctlz.i32(i32 %p_Result_8, i1 true)
  %c3_1 = call i32 @llvm.ctlz.i32(i32 %p_Result_9, i1 true)
  %c4_1 = call i32 @llvm.ctlz.i32(i32 %p_Result_10, i1 true)
  %tmp_i1 = zext i32 %c1_1 to i78
  %tmp_i1_26 = shl i78 %p_Val2_13, %tmp_i1
  %tmp_17_i1 = icmp eq i32 %c1_1, 16
  %shift_5 = add nsw i32 %c2_1, 16
  %tmp_18_i1 = zext i32 %c2_1 to i78
  %tmp_19_i1 = shl i78 %tmp_i1_26, %tmp_18_i1
  %tmp_20_i1 = icmp eq i32 %c2_1, 16
  %shift_6 = add nsw i32 %c3_1, %shift_5
  %tmp_21_i1 = zext i32 %c3_1 to i78
  %tmp_22_i1 = shl i78 %tmp_19_i1, %tmp_21_i1
  %tmp_23_i1 = icmp eq i32 %c3_1, 16
  %shift_7 = add nsw i32 %c4_1, %shift_6
  %tmp_24_i1 = zext i32 %c4_1 to i78
  %tmp_25_i1 = shl i78 %tmp_22_i1, %tmp_24_i1
  %tmp8 = and i1 %tmp_20_i1, %tmp_23_i1
  %sel_tmp1_i1 = and i1 %tmp8, %tmp_17_i1
  %sel_tmp5_i1 = xor i1 %tmp_20_i1, true
  %sel_tmp6_i1 = and i1 %tmp_17_i1, %sel_tmp5_i1
  %p_v4 = select i1 %sel_tmp1_i1, i32 %shift_7, i32 %shift_6
  %tmp_27 = trunc i32 %shift_5 to i11
  %tmp_28 = trunc i32 %p_v4 to i11
  %tmp_29 = trunc i32 %c1_1 to i11
  %tmp_30 = select i1 %tmp_17_i1, i11 %tmp_28, i11 %tmp_29
  %tmp_31 = select i1 %sel_tmp6_i1, i11 %tmp_27, i11 %tmp_30
  %tmp_11 = call i52 @_ssdm_op_PartSelect.i52.i78.i32.i32(i78 %tmp_19_i1, i32 25, i32 76)
  %tmp_32 = call i52 @_ssdm_op_PartSelect.i52.i78.i32.i32(i78 %tmp_25_i1, i32 25, i32 76)
  %tmp_33 = call i52 @_ssdm_op_PartSelect.i52.i78.i32.i32(i78 %tmp_22_i1, i32 25, i32 76)
  %tmp_34 = select i1 %sel_tmp1_i1, i52 %tmp_32, i52 %tmp_33
  %tmp_17 = call i52 @_ssdm_op_PartSelect.i52.i78.i32.i32(i78 %tmp_i1_26, i32 25, i32 76)
  %tmp_18 = select i1 %tmp_17_i1, i52 %tmp_34, i52 %tmp_17
  %loc_V_4 = select i1 %sel_tmp6_i1, i52 %tmp_11, i52 %tmp_18
  %tmp_26_i1 = icmp eq i78 %p_Val2_13, 0
  %phitmp_i1 = sub i11 1023, %tmp_31
  %out_exp_V_1 = select i1 %tmp_26_i1, i11 0, i11 %phitmp_i1
  %p_Result_11 = call i64 @_ssdm_op_BitConcatenate.i64.i1.i11.i52(i1 false, i11 %out_exp_V_1, i52 %loc_V_4)
  %tmp_c_out = bitcast i64 %p_Result_11 to double
  %tmp_8 = xor i3 %k_V, -1
  %sel = select i1 %p_Result_s, i3 %tmp_8, i3 %k_V
  %tmp_34_neg = xor i64 %p_Result_6, -9223372036854775808
  %tmp_s = bitcast i64 %tmp_34_neg to double
  %tmp_28_neg = xor i64 %p_Result_11, -9223372036854775808
  %tmp_5 = bitcast i64 %tmp_28_neg to double
  %sel_tmp = icmp eq i3 %sel, -1
  %sel_tmp1 = select i1 %sel_tmp, double %tmp_s, double %tmp_s_out
  %sel_tmp2 = icmp eq i3 %sel, -2
  %sel_tmp3 = select i1 %sel_tmp2, double %tmp_5, double %sel_tmp1
  %sel_tmp4 = icmp eq i3 %sel, -3
  %sel_tmp5 = select i1 %sel_tmp4, double %tmp_5, double %sel_tmp3
  %sel_tmp6 = icmp eq i3 %sel, -4
  %sel_tmp7 = select i1 %sel_tmp6, double %tmp_s, double %sel_tmp5
  %sel_tmp8 = icmp eq i3 %sel, 3
  %sel_tmp9 = select i1 %sel_tmp8, double %tmp_s_out, double %sel_tmp7
  %sel_tmp10 = icmp eq i3 %sel, 2
  %sel_tmp11 = select i1 %sel_tmp10, double %tmp_c_out, double %sel_tmp9
  %sel_tmp12 = icmp eq i3 %sel, 1
  %s_out = select i1 %sel_tmp12, double %tmp_c_out, double %sel_tmp11
  %sel_tmp13 = select i1 %sel_tmp2, double %tmp_s_out, double %tmp_c_out
  %sel_tmp14 = select i1 %sel_tmp4, double %tmp_s, double %sel_tmp13
  %sel_tmp15 = select i1 %sel_tmp6, double %tmp_5, double %sel_tmp14
  %sel_tmp16 = select i1 %sel_tmp8, double %tmp_5, double %sel_tmp15
  %sel_tmp17 = select i1 %sel_tmp10, double %tmp_s, double %sel_tmp16
  %c_out = select i1 %sel_tmp12, double %tmp_s_out, double %sel_tmp17
  %tmp_7 = icmp eq i11 %loc_V, -1
  %tmp_13 = icmp ult i11 %loc_V, 999
  %t_in_s_out = select i1 %tmp_13, double %t_in_read, double %s_out
  %s_out_write_assign = select i1 %tmp_7, double 0x7FFFFFFFFFFFFFFF, double %t_in_s_out
  %c_out_write_assign = select i1 %tmp_7, double 0x7FFFFFFFFFFFFFFF, double %c_out
  %mrv = insertvalue { double, double } undef, double %s_out_write_assign, 0
  %mrv_1 = insertvalue { double, double } %mrv, double %c_out_write_assign, 1
  ret { double, double } %mrv_1
}

declare i178 @llvm.part.set.i178.i59(i178, i59, i32, i32) nounwind readnone

declare i94 @llvm.part.select.i94(i94, i32, i32) nounwind readnone

declare i79 @llvm.part.select.i79(i79, i32, i32) nounwind readnone

declare i78 @llvm.part.select.i78(i78, i32, i32) nounwind readnone

declare i70 @llvm.part.select.i70(i70, i32, i32) nounwind readnone

declare i64 @llvm.part.select.i64(i64, i32, i32) nounwind readnone

declare i48 @llvm.part.select.i48(i48, i32, i32) nounwind readnone

declare i188 @llvm.part.select.i188(i188, i32, i32) nounwind readnone

declare i178 @llvm.part.select.i178(i178, i32, i32) nounwind readnone

declare i126 @llvm.part.select.i126(i126, i32, i32) nounwind readnone

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

declare void @llvm.dbg.declare(metadata, metadata) nounwind readnone

declare i32 @llvm.ctlz.i32(i32, i1) nounwind readnone

define void @dft([256 x float]* %sample_real, [256 x float]* %sample_imag) nounwind uwtable {
  call void (...)* @_ssdm_op_SpecBitsMap([256 x float]* %sample_real) nounwind, !map !7
  call void (...)* @_ssdm_op_SpecBitsMap([256 x float]* %sample_imag) nounwind, !map !13
  call void (...)* @_ssdm_op_SpecTopModule([4 x i8]* @dft_str) nounwind
  %tmp_real = alloca [256 x float], align 16
  %tmp_imag = alloca [256 x float], align 16
  br label %.loopexit

.loopexit:                                        ; preds = %2, %0
  %i = phi i9 [ 0, %0 ], [ %i_2, %2 ]
  %i_cast3 = zext i9 %i to i32
  %exitcond2 = icmp eq i9 %i, -256
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 256, i64 256, i64 256) nounwind
  %i_2 = add i9 %i, 1
  br i1 %exitcond2, label %.preheader, label %1

; <label>:1                                       ; preds = %.loopexit
  %tmp = zext i9 %i to i64
  %tmp_real_addr = getelementptr inbounds [256 x float]* %tmp_real, i64 0, i64 %tmp
  store float 0.000000e+00, float* %tmp_real_addr, align 4
  %tmp_imag_addr = getelementptr inbounds [256 x float]* %tmp_imag, i64 0, i64 %tmp
  store float 0.000000e+00, float* %tmp_imag_addr, align 4
  %tmp_s = sitofp i32 %i_cast3 to float
  %tmp_14 = fpext float %tmp_s to double
  %tmp_15 = fmul double %tmp_14, 0x401921FB5444261E
  %tmp_16 = fmul double %tmp_15, 3.906250e-03
  %w = fptrunc double %tmp_16 to float
  br label %2

; <label>:2                                       ; preds = %3, %1
  %tmp_17 = phi float [ 0.000000e+00, %1 ], [ %tmp_31, %3 ]
  %tmp_18 = phi float [ 0.000000e+00, %1 ], [ %tmp_27, %3 ]
  %j = phi i9 [ 0, %1 ], [ %j_1, %3 ]
  %j_cast2 = zext i9 %j to i32
  %exitcond1 = icmp eq i9 %j, -256
  %empty_27 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 256, i64 256, i64 256) nounwind
  %j_1 = add i9 %j, 1
  br i1 %exitcond1, label %.loopexit, label %3

; <label>:3                                       ; preds = %2
  %tmp_20 = sitofp i32 %j_cast2 to float
  %tmp_21 = fmul float %tmp_20, %w
  %x_assign = fpext float %tmp_21 to double
  %call_ret_i_i_i = call fastcc { double, double } @dft_sin_cos_range_redux_cordic(double %x_assign) nounwind
  %c_out = extractvalue { double, double } %call_ret_i_i_i, 1
  %c = fptrunc double %c_out to float
  %s_out = extractvalue { double, double } %call_ret_i_i_i, 0
  %tmp_108_to_int = bitcast double %s_out to i64
  %tmp_108_neg = xor i64 %tmp_108_to_int, -9223372036854775808
  %tmp_22 = bitcast i64 %tmp_108_neg to double
  %s = fptrunc double %tmp_22 to float
  %tmp_23 = zext i9 %j to i64
  %sample_real_addr_1 = getelementptr [256 x float]* %sample_real, i64 0, i64 %tmp_23
  %sample_real_load = load float* %sample_real_addr_1, align 4
  %tmp_24 = fmul float %sample_real_load, %c
  %sample_imag_addr_1 = getelementptr [256 x float]* %sample_imag, i64 0, i64 %tmp_23
  %sample_imag_load = load float* %sample_imag_addr_1, align 4
  %tmp_25 = fmul float %sample_imag_load, %s
  %tmp_26 = fsub float %tmp_24, %tmp_25
  %tmp_27 = fadd float %tmp_18, %tmp_26
  store float %tmp_27, float* %tmp_real_addr, align 4
  %tmp_28 = fmul float %sample_real_load, %s
  %tmp_29 = fmul float %sample_imag_load, %c
  %tmp_30 = fadd float %tmp_28, %tmp_29
  %tmp_31 = fadd float %tmp_17, %tmp_30
  store float %tmp_31, float* %tmp_imag_addr, align 4
  br label %2

.preheader:                                       ; preds = %.loopexit, %4
  %i1 = phi i9 [ %i_1, %4 ], [ 0, %.loopexit ]
  %exitcond = icmp eq i9 %i1, -256
  %empty_28 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 256, i64 256, i64 256) nounwind
  %i_1 = add i9 %i1, 1
  br i1 %exitcond, label %5, label %4

; <label>:4                                       ; preds = %.preheader
  %tmp_19 = zext i9 %i1 to i64
  %tmp_real_addr_1 = getelementptr inbounds [256 x float]* %tmp_real, i64 0, i64 %tmp_19
  %tmp_real_load = load float* %tmp_real_addr_1, align 4
  %sample_real_addr = getelementptr [256 x float]* %sample_real, i64 0, i64 %tmp_19
  store float %tmp_real_load, float* %sample_real_addr, align 4
  %tmp_imag_addr_1 = getelementptr inbounds [256 x float]* %tmp_imag, i64 0, i64 %tmp_19
  %tmp_imag_load = load float* %tmp_imag_addr_1, align 4
  %sample_imag_addr = getelementptr [256 x float]* %sample_imag, i64 0, i64 %tmp_19
  store float %tmp_imag_load, float* %sample_imag_addr, align 4
  br label %.preheader

; <label>:5                                       ; preds = %.preheader
  ret void
}

define internal fastcc i178 @"dft_big_mult_v3small<125, 53, 17>"() readnone {
.preheader142.preheader:
  %pps_V = alloca [8 x i70], align 8
  br label %.preheader142

.preheader142:                                    ; preds = %.preheader142, %.preheader142.preheader
  %i = phi i4 [ 0, %.preheader142.preheader ], [ %i_4, %.preheader142 ]
  %exitcond1 = icmp eq i4 %i, -8
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 8, i64 8, i64 8)
  %i_4 = add i4 %i, 1
  br i1 %exitcond1, label %arrayctor.loop1.preheader, label %.preheader142

arrayctor.loop1.preheader:                        ; preds = %.preheader142
  %pps_V_addr = getelementptr [8 x i70]* %pps_V, i64 0, i64 0
  store i70 -335535007245497532416, i70* %pps_V_addr, align 16
  br label %0

; <label>:0                                       ; preds = %_ZrsILi70ELb0EE11ap_int_baseIXT_EXT0_EXleT_Li64EEERKS1_i.exit, %arrayctor.loop1.preheader
  %i_1 = phi i4 [ 1, %arrayctor.loop1.preheader ], [ %i_3, %_ZrsILi70ELb0EE11ap_int_baseIXT_EXT0_EXleT_Li64EEERKS1_i.exit ]
  %exitcond2 = icmp eq i4 %i_1, -8
  %empty_29 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 7, i64 7, i64 7)
  br i1 %exitcond2, label %.preheader, label %_ZrsILi70ELb0EE11ap_int_baseIXT_EXT0_EXleT_Li64EEERKS1_i.exit

_ZrsILi70ELb0EE11ap_int_baseIXT_EXT0_EXleT_Li64EEERKS1_i.exit: ; preds = %0
  %tmp_20 = zext i4 %i_1 to i64
  %tmp_21 = add i4 %i_1, -1
  %tmp_22 = zext i4 %tmp_21 to i64
  %pps_V_addr_1 = getelementptr [8 x i70]* %pps_V, i64 0, i64 %tmp_22
  %pps_V_load = load i70* %pps_V_addr_1, align 16
  %r_V = call i53 @_ssdm_op_PartSelect.i53.i70.i32.i32(i70 %pps_V_load, i32 17, i32 69)
  %r_V_5 = zext i53 %r_V to i70
  %pps_V_addr_2 = getelementptr [8 x i70]* %pps_V, i64 0, i64 %tmp_20
  %pp_V_addr = getelementptr [8 x i70]* @pp_V, i64 0, i64 %tmp_20
  %pp_V_load = load i70* %pp_V_addr, align 16
  %tmp_23 = add i70 %pp_V_load, %r_V_5
  store i70 %tmp_23, i70* %pps_V_addr_2, align 16
  %i_3 = add i4 %i_1, 1
  br label %0

.preheader:                                       ; preds = %0, %._crit_edge143
  %p_Val2_s = phi i178 [ %p_Result_12, %._crit_edge143 ], [ undef, %0 ]
  %i_2 = phi i3 [ %tmp_25, %._crit_edge143 ], [ 0, %0 ]
  %exitcond = icmp eq i3 %i_2, -1
  %empty_30 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 7, i64 7, i64 7)
  %tmp_25 = add i3 %i_2, 1
  br i1 %exitcond, label %1, label %._crit_edge143

._crit_edge143:                                   ; preds = %.preheader
  %Li = call i7 @_ssdm_op_BitConcatenate.i7.i3.i1.i3(i3 %i_2, i1 false, i3 %i_2)
  %Ui = add i7 16, %Li
  %tmp_24 = zext i3 %i_2 to i64
  %pps_V_addr_4 = getelementptr [8 x i70]* %pps_V, i64 0, i64 %tmp_24
  %p_Val2_19 = load i70* %pps_V_addr_4, align 16
  %tmp_37 = trunc i70 %p_Val2_19 to i17
  %loc_V = zext i17 %tmp_37 to i178
  %tmp_38 = icmp ugt i7 %Li, -17
  %tmp_39 = zext i7 %Li to i8
  %tmp_40 = zext i7 %Ui to i8
  %tmp_41 = sub i8 -79, %tmp_39
  %tmp_42 = select i1 %tmp_38, i8 %tmp_39, i8 %tmp_40
  %tmp_43 = select i1 %tmp_38, i8 %tmp_40, i8 %tmp_39
  %tmp_44 = select i1 %tmp_38, i8 %tmp_41, i8 %tmp_39
  %tmp_45 = sub i8 -79, %tmp_42
  %tmp_46 = zext i8 %tmp_44 to i178
  %tmp_47 = zext i8 %tmp_43 to i178
  %tmp_48 = zext i8 %tmp_45 to i178
  %tmp_49 = shl i178 %loc_V, %tmp_46
  %tmp_50 = call i178 @llvm.part.select.i178(i178 %tmp_49, i32 177, i32 0)
  %tmp_51 = select i1 %tmp_38, i178 %tmp_50, i178 %tmp_49
  %tmp_52 = shl i178 -1, %tmp_47
  %tmp_53 = lshr i178 -1, %tmp_48
  %p_demorgan = and i178 %tmp_52, %tmp_53
  %tmp_54 = xor i178 %p_demorgan, -1
  %tmp_55 = and i178 %p_Val2_s, %tmp_54
  %tmp_56 = and i178 %tmp_51, %p_demorgan
  %p_Result_12 = or i178 %tmp_55, %tmp_56
  br label %.preheader

; <label>:1                                       ; preds = %.preheader
  %pps_V_addr_3 = getelementptr [8 x i70]* %pps_V, i64 0, i64 7
  %pps_V_load_1 = load i70* %pps_V_addr_3, align 16
  %tmp = trunc i70 %pps_V_load_1 to i59
  %p_Result_s = call i178 @llvm.part.set.i178.i59(i178 %p_Val2_s, i59 %tmp, i32 119, i32 177)
  ret i178 %p_Result_s
}

define internal fastcc i188 @"dft_big_mult_v3<94, 17>"(i78 %a_V) readnone {
.preheader163.preheader:
  %a_V_read = call i78 @_ssdm_op_Read.ap_auto.i78(i78 %a_V)
  %a_V_cast = zext i78 %a_V_read to i94
  %pp_V_1 = alloca [36 x i34], align 8
  %pp_V_addr = getelementptr [36 x i34]* %pp_V_1, i64 0, i64 0
  %pps_V = alloca [12 x i48], align 8
  br label %.preheader163

.preheader163:                                    ; preds = %.preheader162, %.preheader163.preheader
  %i = phi i3 [ 0, %.preheader163.preheader ], [ %i_5, %.preheader162 ]
  %exitcond3 = icmp eq i3 %i, -2
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 6, i64 6, i64 6)
  %i_5 = add i3 %i, 1
  br i1 %exitcond3, label %arrayctor.loop4.preheader, label %.preheader162.preheader

.preheader162.preheader:                          ; preds = %.preheader163
  %tmp_s = icmp eq i3 %i, -3
  %tmp_32 = call i6 @_ssdm_op_BitConcatenate.i6.i3.i3(i3 %i, i3 0)
  %p_shl_cast = zext i6 %tmp_32 to i7
  %tmp_34 = call i4 @_ssdm_op_BitConcatenate.i4.i3.i1(i3 %i, i1 false)
  %p_shl1_cast = zext i4 %tmp_34 to i7
  %tmp_35 = sub i7 %p_shl_cast, %p_shl1_cast
  %Lo_assign = call i7 @_ssdm_op_BitConcatenate.i7.i3.i1.i3(i3 %i, i1 false, i3 %i)
  %tmp_27 = add i7 %Lo_assign, 16
  %Ui_1 = select i1 %tmp_s, i7 -35, i7 %tmp_27
  br label %.preheader162

.preheader162:                                    ; preds = %_ifconv, %.preheader162.preheader
  %j = phi i3 [ %j_2, %_ifconv ], [ 0, %.preheader162.preheader ]
  %exitcond5 = icmp eq i3 %j, -2
  %empty_31 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 6, i64 6, i64 6)
  %j_2 = add i3 %j, 1
  br i1 %exitcond5, label %.preheader163, label %_ifconv

_ifconv:                                          ; preds = %.preheader162
  %tmp_31 = icmp eq i3 %j, -3
  %Lo_assign_1 = call i7 @_ssdm_op_BitConcatenate.i7.i3.i1.i3(i3 %j, i1 false, i3 %j)
  %tmp_33 = add i7 16, %Lo_assign_1
  %Uj = select i1 %tmp_31, i7 -35, i7 %tmp_33
  %tmp_34_cast = zext i3 %j to i7
  %tmp_36 = add i7 %tmp_35, %tmp_34_cast
  %tmp_49_cast = sext i7 %tmp_36 to i64
  %pp_V_addr_1 = getelementptr [36 x i34]* %pp_V_1, i64 0, i64 %tmp_49_cast
  %tmp_58 = icmp ugt i7 %Lo_assign, %Ui_1
  %tmp_59 = call i94 @llvm.part.select.i94(i94 %a_V_cast, i32 93, i32 0)
  %tmp_60 = sub i7 %Lo_assign, %Ui_1
  %tmp_61 = sub i7 -35, %Lo_assign
  %tmp_62 = sub i7 %Ui_1, %Lo_assign
  %tmp_63 = select i1 %tmp_58, i7 %tmp_60, i7 %tmp_62
  %tmp_64 = select i1 %tmp_58, i94 %tmp_59, i94 %a_V_cast
  %tmp_65 = select i1 %tmp_58, i7 %tmp_61, i7 %Lo_assign
  %tmp_66 = sub i7 -35, %tmp_63
  %tmp_67 = zext i7 %tmp_65 to i94
  %tmp_68 = zext i7 %tmp_66 to i94
  %tmp_69 = lshr i94 %tmp_64, %tmp_67
  %tmp_70 = lshr i94 -1, %tmp_68
  %p_Result_13 = and i94 %tmp_69, %tmp_70
  %tmp_72 = icmp ugt i7 %Lo_assign_1, %Uj
  %tmp_73 = sub i7 %Lo_assign_1, %Uj
  %tmp_74 = sub i7 -35, %Lo_assign_1
  %tmp_75 = sub i7 %Uj, %Lo_assign_1
  %tmp_76 = select i1 %tmp_72, i7 %tmp_73, i7 %tmp_75
  %tmp_77 = select i1 %tmp_72, i94 -8932543859452897742033846272, i94 237372029602271161299249
  %tmp_78 = select i1 %tmp_72, i7 %tmp_74, i7 %Lo_assign_1
  %tmp_79 = sub i7 -35, %tmp_76
  %tmp_80 = zext i7 %tmp_78 to i94
  %tmp_81 = zext i7 %tmp_79 to i94
  %tmp_82 = lshr i94 %tmp_77, %tmp_80
  %tmp_83 = lshr i94 -1, %tmp_81
  %p_Result_14 = and i94 %tmp_82, %tmp_83
  %tmp_85 = trunc i94 %p_Result_13 to i34
  %tmp_86 = trunc i94 %p_Result_14 to i34
  %tmp_37 = mul i34 %tmp_86, %tmp_85
  store i34 %tmp_37, i34* %pp_V_addr_1, align 8
  br label %.preheader162

arrayctor.loop4.preheader:                        ; preds = %.preheader163
  %pp_V_load = load i34* %pp_V_addr, align 16
  %tmp = zext i34 %pp_V_load to i48
  %pps_V_addr = getelementptr [12 x i48]* %pps_V, i64 0, i64 0
  store i48 %tmp, i48* %pps_V_addr, align 16
  br label %0

; <label>:0                                       ; preds = %4, %arrayctor.loop4.preheader
  %i_1 = phi i4 [ 1, %arrayctor.loop4.preheader ], [ %i_6, %4 ]
  %i_1_cast = zext i4 %i_1 to i5
  %exitcond4 = icmp eq i4 %i_1, -4
  %empty_32 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 11, i64 11, i64 11)
  br i1 %exitcond4, label %.preheader, label %_ZrsILi48ELb0EE11ap_int_baseIXT_EXT0_EXleT_Li64EEERKS1_i.exit

_ZrsILi48ELb0EE11ap_int_baseIXT_EXT0_EXleT_Li64EEERKS1_i.exit: ; preds = %0
  %tmp_28 = zext i4 %i_1 to i64
  %tmp_29 = add i4 %i_1, -1
  %tmp_30 = zext i4 %tmp_29 to i64
  %pps_V_addr_5 = getelementptr [12 x i48]* %pps_V, i64 0, i64 %tmp_30
  %pps_V_load = load i48* %pps_V_addr_5, align 8
  %r_V = call i31 @_ssdm_op_PartSelect.i31.i48.i32.i32(i48 %pps_V_load, i32 17, i32 47)
  %r_V_6 = zext i31 %r_V to i48
  %pps_V_addr_6 = getelementptr [12 x i48]* %pps_V, i64 0, i64 %tmp_28
  store i48 %r_V_6, i48* %pps_V_addr_6, align 8
  br label %1

; <label>:1                                       ; preds = %._crit_edge165, %_ZrsILi48ELb0EE11ap_int_baseIXT_EXT0_EXleT_Li64EEERKS1_i.exit
  %j_1 = phi i3 [ 0, %_ZrsILi48ELb0EE11ap_int_baseIXT_EXT0_EXleT_Li64EEERKS1_i.exit ], [ %j_3, %._crit_edge165 ]
  %j_1_cast3 = zext i3 %j_1 to i5
  %j_1_cast3_cast = zext i3 %j_1 to i4
  %exitcond6 = icmp eq i3 %j_1, -2
  %empty_33 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 6, i64 6, i64 6)
  %j_3 = add i3 %j_1, 1
  br i1 %exitcond6, label %4, label %2

; <label>:2                                       ; preds = %1
  %ult = icmp ult i4 %i_1, %j_1_cast3_cast
  %rev5 = xor i1 %ult, true
  %tmp_40 = sub i5 %i_1_cast, %j_1_cast3
  %tmp_41 = icmp slt i5 %tmp_40, 6
  %or_cond = and i1 %rev5, %tmp_41
  br i1 %or_cond, label %3, label %._crit_edge165

; <label>:3                                       ; preds = %2
  %tmp_43_cast = sext i5 %tmp_40 to i7
  %tmp_42 = call i6 @_ssdm_op_BitConcatenate.i6.i3.i3(i3 %j_1, i3 0)
  %p_shl2_cast = zext i6 %tmp_42 to i7
  %tmp_43 = call i4 @_ssdm_op_BitConcatenate.i4.i3.i1(i3 %j_1, i1 false)
  %p_shl3_cast = zext i4 %tmp_43 to i7
  %tmp_44 = sub i7 %p_shl2_cast, %p_shl3_cast
  %tmp_47 = add i7 %tmp_44, %tmp_43_cast
  %tmp_53_cast = sext i7 %tmp_47 to i64
  %pp_V_addr_2 = getelementptr [36 x i34]* %pp_V_1, i64 0, i64 %tmp_53_cast
  %pp_V_load_1 = load i34* %pp_V_addr_2, align 8
  %tmp_45 = zext i34 %pp_V_load_1 to i48
  %pps_V_load_4 = load i48* %pps_V_addr_6, align 8
  %tmp_46 = add i48 %tmp_45, %pps_V_load_4
  store i48 %tmp_46, i48* %pps_V_addr_6, align 8
  br label %._crit_edge165

._crit_edge165:                                   ; preds = %3, %2
  br label %1

; <label>:4                                       ; preds = %1
  %i_6 = add i4 %i_1, 1
  br label %0

.preheader:                                       ; preds = %0, %._crit_edge166
  %p_Val2_20 = phi i188 [ %p_Result_s, %._crit_edge166 ], [ undef, %0 ]
  %i_2 = phi i4 [ %i_7, %._crit_edge166 ], [ 0, %0 ]
  %exitcond = icmp eq i4 %i_2, -4
  %empty_34 = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 12, i64 12, i64 12)
  %i_7 = add i4 %i_2, 1
  br i1 %exitcond, label %5, label %._crit_edge166

._crit_edge166:                                   ; preds = %.preheader
  %Li = call i8 @_ssdm_op_BitConcatenate.i8.i4.i4(i4 %i_2, i4 %i_2)
  %Ui = add i8 16, %Li
  %tmp_38 = icmp ugt i8 %Ui, -69
  %Hi_assign = select i1 %tmp_38, i8 -69, i8 %Ui
  %tmp_39 = zext i4 %i_2 to i64
  %pps_V_addr_7 = getelementptr [12 x i48]* %pps_V, i64 0, i64 %tmp_39
  %p_Val2_s = load i48* %pps_V_addr_7, align 8
  %tmp_87 = trunc i48 %p_Val2_s to i17
  %loc_V = zext i17 %tmp_87 to i188
  %tmp_88 = icmp ugt i8 %Li, %Hi_assign
  %tmp_89 = sub i8 -69, %Li
  %tmp_90 = select i1 %tmp_88, i8 %Li, i8 %Hi_assign
  %tmp_91 = select i1 %tmp_88, i8 %Hi_assign, i8 %Li
  %tmp_92 = select i1 %tmp_88, i8 %tmp_89, i8 %Li
  %tmp_93 = sub i8 -69, %tmp_90
  %tmp_94 = zext i8 %tmp_92 to i188
  %tmp_95 = zext i8 %tmp_91 to i188
  %tmp_96 = zext i8 %tmp_93 to i188
  %tmp_97 = shl i188 %loc_V, %tmp_94
  %tmp_98 = call i188 @llvm.part.select.i188(i188 %tmp_97, i32 187, i32 0)
  %tmp_99 = select i1 %tmp_88, i188 %tmp_98, i188 %tmp_97
  %tmp_100 = shl i188 -1, %tmp_95
  %tmp_101 = lshr i188 -1, %tmp_96
  %p_demorgan = and i188 %tmp_100, %tmp_101
  %tmp_102 = xor i188 %p_demorgan, -1
  %tmp_103 = and i188 %p_Val2_20, %tmp_102
  %tmp_104 = and i188 %tmp_99, %p_demorgan
  %p_Result_s = or i188 %tmp_103, %tmp_104
  br label %.preheader

; <label>:5                                       ; preds = %.preheader
  ret i188 %p_Val2_20
}

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecRegionEnd(...) {
entry:
  ret i32 0
}

define weak i32 @_ssdm_op_SpecRegionBegin(...) {
entry:
  ret i32 0
}

define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecLatency(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak i78 @_ssdm_op_Read.ap_auto.i78(i78) {
entry:
  ret i78 %0
}

define weak double @_ssdm_op_Read.ap_auto.double(double) {
entry:
  ret double %0
}

define weak i79 @_ssdm_op_PartSelect.i79.i188.i32.i32(i188, i32, i32) nounwind readnone {
entry:
  %empty = call i188 @llvm.part.select.i188(i188 %0, i32 %1, i32 %2)
  %empty_35 = trunc i188 %empty to i79
  ret i79 %empty_35
}

define weak i78 @_ssdm_op_PartSelect.i78.i79.i32.i32(i79, i32, i32) nounwind readnone {
entry:
  %empty = call i79 @llvm.part.select.i79(i79 %0, i32 %1, i32 %2)
  %empty_36 = trunc i79 %empty to i78
  ret i78 %empty_36
}

define weak i78 @_ssdm_op_PartSelect.i78.i188.i32.i32(i188, i32, i32) nounwind readnone {
entry:
  %empty = call i188 @llvm.part.select.i188(i188 %0, i32 %1, i32 %2)
  %empty_37 = trunc i188 %empty to i78
  ret i78 %empty_37
}

define weak i78 @_ssdm_op_PartSelect.i78.i178.i32.i32(i178, i32, i32) nounwind readnone {
entry:
  %empty = call i178 @llvm.part.select.i178(i178 %0, i32 %1, i32 %2)
  %empty_38 = trunc i178 %empty to i78
  ret i78 %empty_38
}

declare i77 @_ssdm_op_PartSelect.i77.i79.i32.i32(i79, i32, i32) nounwind readnone

define weak i76 @_ssdm_op_PartSelect.i76.i78.i32.i32(i78, i32, i32) nounwind readnone {
entry:
  %empty = call i78 @llvm.part.select.i78(i78 %0, i32 %1, i32 %2)
  %empty_39 = trunc i78 %empty to i76
  ret i76 %empty_39
}

define weak i76 @_ssdm_op_PartSelect.i76.i126.i32.i32(i126, i32, i32) nounwind readnone {
entry:
  %empty = call i126 @llvm.part.select.i126(i126 %0, i32 %1, i32 %2)
  %empty_40 = trunc i126 %empty to i76
  ret i76 %empty_40
}

declare i59 @_ssdm_op_PartSelect.i59.i70.i32.i32(i70, i32, i32) nounwind readnone

define weak i53 @_ssdm_op_PartSelect.i53.i70.i32.i32(i70, i32, i32) nounwind readnone {
entry:
  %empty = call i70 @llvm.part.select.i70(i70 %0, i32 %1, i32 %2)
  %empty_41 = trunc i70 %empty to i53
  ret i53 %empty_41
}

define weak i52 @_ssdm_op_PartSelect.i52.i78.i32.i32(i78, i32, i32) nounwind readnone {
entry:
  %empty = call i78 @llvm.part.select.i78(i78 %0, i32 %1, i32 %2)
  %empty_42 = trunc i78 %empty to i52
  ret i52 %empty_42
}

declare i52 @_ssdm_op_PartSelect.i52.i64.i32.i32(i64, i32, i32) nounwind readnone

declare i34 @_ssdm_op_PartSelect.i34.i94.i32.i32(i94, i32, i32) nounwind readnone

define weak i31 @_ssdm_op_PartSelect.i31.i48.i32.i32(i48, i32, i32) nounwind readnone {
entry:
  %empty = call i48 @llvm.part.select.i48(i48 %0, i32 %1, i32 %2)
  %empty_43 = trunc i48 %empty to i31
  ret i31 %empty_43
}

define weak i3 @_ssdm_op_PartSelect.i3.i178.i32.i32(i178, i32, i32) nounwind readnone {
entry:
  %empty = call i178 @llvm.part.select.i178(i178 %0, i32 %1, i32 %2)
  %empty_44 = trunc i178 %empty to i3
  ret i3 %empty_44
}

declare i17 @_ssdm_op_PartSelect.i17.i70.i32.i32(i70, i32, i32) nounwind readnone

declare i17 @_ssdm_op_PartSelect.i17.i48.i32.i32(i48, i32, i32) nounwind readnone

define weak i16 @_ssdm_op_PartSelect.i16.i78.i32.i32(i78, i32, i32) nounwind readnone {
entry:
  %empty = call i78 @llvm.part.select.i78(i78 %0, i32 %1, i32 %2)
  %empty_45 = trunc i78 %empty to i16
  ret i16 %empty_45
}

define weak i11 @_ssdm_op_PartSelect.i11.i64.i32.i32(i64, i32, i32) nounwind readnone {
entry:
  %empty = call i64 @llvm.part.select.i64(i64 %0, i32 %1, i32 %2)
  %empty_46 = trunc i64 %empty to i11
  ret i11 %empty_46
}

declare i11 @_ssdm_op_PartSelect.i11.i32.i32.i32(i32, i32, i32) nounwind readnone

declare i16 @_ssdm_op_HSub(...)

declare i16 @_ssdm_op_HMul(...)

declare i16 @_ssdm_op_HDiv(...)

declare i16 @_ssdm_op_HAdd(...)

define weak i1 @_ssdm_op_BitSelect.i1.i79.i32(i79, i32) nounwind readnone {
entry:
  %empty = zext i32 %1 to i79
  %empty_47 = shl i79 1, %empty
  %empty_48 = and i79 %0, %empty_47
  %empty_49 = icmp ne i79 %empty_48, 0
  ret i1 %empty_49
}

define weak i1 @_ssdm_op_BitSelect.i1.i64.i32(i64, i32) nounwind readnone {
entry:
  %empty = zext i32 %1 to i64
  %empty_50 = shl i64 1, %empty
  %empty_51 = and i64 %0, %empty_50
  %empty_52 = icmp ne i64 %empty_51, 0
  ret i1 %empty_52
}

define weak i1 @_ssdm_op_BitSelect.i1.i178.i32(i178, i32) nounwind readnone {
entry:
  %empty = zext i32 %1 to i178
  %empty_53 = shl i178 1, %empty
  %empty_54 = and i178 %0, %empty_53
  %empty_55 = icmp ne i178 %empty_54, 0
  ret i1 %empty_55
}

define weak i1 @_ssdm_op_BitSelect.i1.i126.i32(i126, i32) nounwind readnone {
entry:
  %empty = zext i32 %1 to i126
  %empty_56 = shl i126 1, %empty
  %empty_57 = and i126 %0, %empty_56
  %empty_58 = icmp ne i126 %empty_57, 0
  ret i1 %empty_58
}

define weak i1 @_ssdm_op_BitSelect.i1.i12.i32(i12, i32) nounwind readnone {
entry:
  %empty = trunc i32 %1 to i12
  %empty_59 = shl i12 1, %empty
  %empty_60 = and i12 %0, %empty_59
  %empty_61 = icmp ne i12 %empty_60, 0
  ret i1 %empty_61
}

define weak i8 @_ssdm_op_BitConcatenate.i8.i4.i4(i4, i4) nounwind readnone {
entry:
  %empty = zext i4 %0 to i8
  %empty_62 = zext i4 %1 to i8
  %empty_63 = shl i8 %empty, 4
  %empty_64 = or i8 %empty_63, %empty_62
  ret i8 %empty_64
}

define weak i78 @_ssdm_op_BitConcatenate.i78.i77.i1(i77, i1) nounwind readnone {
entry:
  %empty = zext i77 %0 to i78
  %empty_65 = zext i1 %1 to i78
  %empty_66 = shl i78 %empty, 1
  %empty_67 = or i78 %empty_66, %empty_65
  ret i78 %empty_67
}

define weak i78 @_ssdm_op_BitConcatenate.i78.i1.i52.i25(i1, i52, i25) nounwind readnone {
entry:
  %empty = zext i52 %1 to i77
  %empty_68 = zext i25 %2 to i77
  %empty_69 = shl i77 %empty, 25
  %empty_70 = or i77 %empty_69, %empty_68
  %empty_71 = zext i1 %0 to i78
  %empty_72 = zext i77 %empty_70 to i78
  %empty_73 = shl i78 %empty_71, 77
  %empty_74 = or i78 %empty_73, %empty_72
  ret i78 %empty_74
}

define weak i7 @_ssdm_op_BitConcatenate.i7.i3.i1.i3(i3, i1, i3) nounwind readnone {
entry:
  %empty = zext i1 %1 to i4
  %empty_75 = zext i3 %2 to i4
  %empty_76 = shl i4 %empty, 3
  %empty_77 = or i4 %empty_76, %empty_75
  %empty_78 = zext i3 %0 to i7
  %empty_79 = zext i4 %empty_77 to i7
  %empty_80 = shl i7 %empty_78, 4
  %empty_81 = or i7 %empty_80, %empty_79
  ret i7 %empty_81
}

define weak i64 @_ssdm_op_BitConcatenate.i64.i1.i11.i52(i1, i11, i52) nounwind readnone {
entry:
  %empty = zext i11 %1 to i63
  %empty_82 = zext i52 %2 to i63
  %empty_83 = shl i63 %empty, 52
  %empty_84 = or i63 %empty_83, %empty_82
  %empty_85 = zext i1 %0 to i64
  %empty_86 = zext i63 %empty_84 to i64
  %empty_87 = shl i64 %empty_85, 63
  %empty_88 = or i64 %empty_87, %empty_86
  ret i64 %empty_88
}

define weak i6 @_ssdm_op_BitConcatenate.i6.i3.i3(i3, i3) nounwind readnone {
entry:
  %empty = zext i3 %0 to i6
  %empty_89 = zext i3 %1 to i6
  %empty_90 = shl i6 %empty, 3
  %empty_91 = or i6 %empty_90, %empty_89
  ret i6 %empty_91
}

define weak i4 @_ssdm_op_BitConcatenate.i4.i3.i1(i3, i1) nounwind readnone {
entry:
  %empty = zext i3 %0 to i4
  %empty_92 = zext i1 %1 to i4
  %empty_93 = shl i4 %empty, 1
  %empty_94 = or i4 %empty_93, %empty_92
  ret i4 %empty_94
}

define weak i32 @_ssdm_op_BitConcatenate.i32.i16.i16(i16, i16) nounwind readnone {
entry:
  %empty = zext i16 %0 to i32
  %empty_95 = zext i16 %1 to i32
  %empty_96 = shl i32 %empty, 16
  %empty_97 = or i32 %empty_96, %empty_95
  ret i32 %empty_97
}

declare void @_GLOBAL__I_a8() nounwind

declare void @_GLOBAL__I_a1942() nounwind

declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [3 x i32]* @llvm_global_ctors_0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 2, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 31, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"sample_real", metadata !11, metadata !"float", i32 0, i32 31}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 255, i32 1}
!13 = metadata !{metadata !14}
!14 = metadata !{i32 0, i32 31, metadata !15}
!15 = metadata !{metadata !16}
!16 = metadata !{metadata !"sample_imag", metadata !11, metadata !"float", i32 0, i32 31}
