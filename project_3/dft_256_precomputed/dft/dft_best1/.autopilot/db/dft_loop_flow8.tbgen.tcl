set moduleName dft_loop_flow8
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {dft_loop_flow8}
set C_modelType { void 0 }
set C_modelArgList { 
	{ real_arr16_0 float 32 regular {array 4 { 1 3 } 1 1 } {global 0}  }
	{ real_arr16_1 float 32 regular {array 4 { 1 3 } 1 1 } {global 0}  }
	{ imag_arr16_0 float 32 regular {array 4 { 1 3 } 1 1 } {global 0}  }
	{ imag_arr16_1 float 32 regular {array 4 { 1 3 } 1 1 } {global 0}  }
	{ real_arr16_2 float 32 regular {array 4 { 1 3 } 1 1 } {global 0}  }
	{ real_arr16_3 float 32 regular {array 4 { 1 3 } 1 1 } {global 0}  }
	{ imag_arr16_2 float 32 regular {array 4 { 1 3 } 1 1 } {global 0}  }
	{ imag_arr16_3 float 32 regular {array 4 { 1 3 } 1 1 } {global 0}  }
	{ imag_arr8_3 float 32 regular {pointer 1} {global 1}  }
	{ imag_arr8_7 float 32 regular {pointer 1} {global 1}  }
	{ imag_arr8_2 float 32 regular {pointer 1} {global 1}  }
	{ imag_arr8_6 float 32 regular {pointer 1} {global 1}  }
	{ imag_arr8_1 float 32 regular {pointer 1} {global 1}  }
	{ imag_arr8_5 float 32 regular {pointer 1} {global 1}  }
	{ imag_arr8_0 float 32 regular {pointer 1} {global 1}  }
	{ imag_arr8_4 float 32 regular {pointer 1} {global 1}  }
	{ real_arr8_3 float 32 regular {pointer 1} {global 1}  }
	{ real_arr8_7 float 32 regular {pointer 1} {global 1}  }
	{ real_arr8_2 float 32 regular {pointer 1} {global 1}  }
	{ real_arr8_6 float 32 regular {pointer 1} {global 1}  }
	{ real_arr8_1 float 32 regular {pointer 1} {global 1}  }
	{ real_arr8_5 float 32 regular {pointer 1} {global 1}  }
	{ real_arr8_0 float 32 regular {pointer 1} {global 1}  }
	{ real_arr8_4 float 32 regular {pointer 1} {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "real_arr16_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 12,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr16_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 13,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr16_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 12,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr16_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 13,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr16_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 14,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr16_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 15,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr16_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 14,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr16_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 15,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_3", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 3,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_7", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 7,"up" : 7,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_2", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 2,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_6", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 6,"up" : 6,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_1", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_5", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 5,"up" : 5,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_0", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_4", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 4,"up" : 4,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_3", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 3,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_7", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 7,"up" : 7,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_2", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 2,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_6", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 6,"up" : 6,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_1", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_5", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 5,"up" : 5,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_0", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_4", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 4,"up" : 4,"step" : 2}]}]}], "extern" : 0} ]}
# RTL Port declarations: 
set portNum 62
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ real_arr16_0_address0 sc_out sc_lv 2 signal 0 } 
	{ real_arr16_0_ce0 sc_out sc_logic 1 signal 0 } 
	{ real_arr16_0_q0 sc_in sc_lv 32 signal 0 } 
	{ real_arr16_1_address0 sc_out sc_lv 2 signal 1 } 
	{ real_arr16_1_ce0 sc_out sc_logic 1 signal 1 } 
	{ real_arr16_1_q0 sc_in sc_lv 32 signal 1 } 
	{ imag_arr16_0_address0 sc_out sc_lv 2 signal 2 } 
	{ imag_arr16_0_ce0 sc_out sc_logic 1 signal 2 } 
	{ imag_arr16_0_q0 sc_in sc_lv 32 signal 2 } 
	{ imag_arr16_1_address0 sc_out sc_lv 2 signal 3 } 
	{ imag_arr16_1_ce0 sc_out sc_logic 1 signal 3 } 
	{ imag_arr16_1_q0 sc_in sc_lv 32 signal 3 } 
	{ real_arr16_2_address0 sc_out sc_lv 2 signal 4 } 
	{ real_arr16_2_ce0 sc_out sc_logic 1 signal 4 } 
	{ real_arr16_2_q0 sc_in sc_lv 32 signal 4 } 
	{ real_arr16_3_address0 sc_out sc_lv 2 signal 5 } 
	{ real_arr16_3_ce0 sc_out sc_logic 1 signal 5 } 
	{ real_arr16_3_q0 sc_in sc_lv 32 signal 5 } 
	{ imag_arr16_2_address0 sc_out sc_lv 2 signal 6 } 
	{ imag_arr16_2_ce0 sc_out sc_logic 1 signal 6 } 
	{ imag_arr16_2_q0 sc_in sc_lv 32 signal 6 } 
	{ imag_arr16_3_address0 sc_out sc_lv 2 signal 7 } 
	{ imag_arr16_3_ce0 sc_out sc_logic 1 signal 7 } 
	{ imag_arr16_3_q0 sc_in sc_lv 32 signal 7 } 
	{ imag_arr8_3 sc_out sc_lv 32 signal 8 } 
	{ imag_arr8_3_ap_vld sc_out sc_logic 1 outvld 8 } 
	{ imag_arr8_7 sc_out sc_lv 32 signal 9 } 
	{ imag_arr8_7_ap_vld sc_out sc_logic 1 outvld 9 } 
	{ imag_arr8_2 sc_out sc_lv 32 signal 10 } 
	{ imag_arr8_2_ap_vld sc_out sc_logic 1 outvld 10 } 
	{ imag_arr8_6 sc_out sc_lv 32 signal 11 } 
	{ imag_arr8_6_ap_vld sc_out sc_logic 1 outvld 11 } 
	{ imag_arr8_1 sc_out sc_lv 32 signal 12 } 
	{ imag_arr8_1_ap_vld sc_out sc_logic 1 outvld 12 } 
	{ imag_arr8_5 sc_out sc_lv 32 signal 13 } 
	{ imag_arr8_5_ap_vld sc_out sc_logic 1 outvld 13 } 
	{ imag_arr8_0 sc_out sc_lv 32 signal 14 } 
	{ imag_arr8_0_ap_vld sc_out sc_logic 1 outvld 14 } 
	{ imag_arr8_4 sc_out sc_lv 32 signal 15 } 
	{ imag_arr8_4_ap_vld sc_out sc_logic 1 outvld 15 } 
	{ real_arr8_3 sc_out sc_lv 32 signal 16 } 
	{ real_arr8_3_ap_vld sc_out sc_logic 1 outvld 16 } 
	{ real_arr8_7 sc_out sc_lv 32 signal 17 } 
	{ real_arr8_7_ap_vld sc_out sc_logic 1 outvld 17 } 
	{ real_arr8_2 sc_out sc_lv 32 signal 18 } 
	{ real_arr8_2_ap_vld sc_out sc_logic 1 outvld 18 } 
	{ real_arr8_6 sc_out sc_lv 32 signal 19 } 
	{ real_arr8_6_ap_vld sc_out sc_logic 1 outvld 19 } 
	{ real_arr8_1 sc_out sc_lv 32 signal 20 } 
	{ real_arr8_1_ap_vld sc_out sc_logic 1 outvld 20 } 
	{ real_arr8_5 sc_out sc_lv 32 signal 21 } 
	{ real_arr8_5_ap_vld sc_out sc_logic 1 outvld 21 } 
	{ real_arr8_0 sc_out sc_lv 32 signal 22 } 
	{ real_arr8_0_ap_vld sc_out sc_logic 1 outvld 22 } 
	{ real_arr8_4 sc_out sc_lv 32 signal 23 } 
	{ real_arr8_4_ap_vld sc_out sc_logic 1 outvld 23 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "real_arr16_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "real_arr16_0", "role": "address0" }} , 
 	{ "name": "real_arr16_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_0", "role": "ce0" }} , 
 	{ "name": "real_arr16_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_0", "role": "q0" }} , 
 	{ "name": "real_arr16_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "real_arr16_1", "role": "address0" }} , 
 	{ "name": "real_arr16_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_1", "role": "ce0" }} , 
 	{ "name": "real_arr16_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_1", "role": "q0" }} , 
 	{ "name": "imag_arr16_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "imag_arr16_0", "role": "address0" }} , 
 	{ "name": "imag_arr16_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_0", "role": "ce0" }} , 
 	{ "name": "imag_arr16_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_0", "role": "q0" }} , 
 	{ "name": "imag_arr16_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "imag_arr16_1", "role": "address0" }} , 
 	{ "name": "imag_arr16_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_1", "role": "ce0" }} , 
 	{ "name": "imag_arr16_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_1", "role": "q0" }} , 
 	{ "name": "real_arr16_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "real_arr16_2", "role": "address0" }} , 
 	{ "name": "real_arr16_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_2", "role": "ce0" }} , 
 	{ "name": "real_arr16_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_2", "role": "q0" }} , 
 	{ "name": "real_arr16_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "real_arr16_3", "role": "address0" }} , 
 	{ "name": "real_arr16_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_3", "role": "ce0" }} , 
 	{ "name": "real_arr16_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_3", "role": "q0" }} , 
 	{ "name": "imag_arr16_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "imag_arr16_2", "role": "address0" }} , 
 	{ "name": "imag_arr16_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_2", "role": "ce0" }} , 
 	{ "name": "imag_arr16_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_2", "role": "q0" }} , 
 	{ "name": "imag_arr16_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "imag_arr16_3", "role": "address0" }} , 
 	{ "name": "imag_arr16_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_3", "role": "ce0" }} , 
 	{ "name": "imag_arr16_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_3", "role": "q0" }} , 
 	{ "name": "imag_arr8_3", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_3", "role": "default" }} , 
 	{ "name": "imag_arr8_3_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_3", "role": "ap_vld" }} , 
 	{ "name": "imag_arr8_7", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_7", "role": "default" }} , 
 	{ "name": "imag_arr8_7_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_7", "role": "ap_vld" }} , 
 	{ "name": "imag_arr8_2", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_2", "role": "default" }} , 
 	{ "name": "imag_arr8_2_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_2", "role": "ap_vld" }} , 
 	{ "name": "imag_arr8_6", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_6", "role": "default" }} , 
 	{ "name": "imag_arr8_6_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_6", "role": "ap_vld" }} , 
 	{ "name": "imag_arr8_1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_1", "role": "default" }} , 
 	{ "name": "imag_arr8_1_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_1", "role": "ap_vld" }} , 
 	{ "name": "imag_arr8_5", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_5", "role": "default" }} , 
 	{ "name": "imag_arr8_5_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_5", "role": "ap_vld" }} , 
 	{ "name": "imag_arr8_0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_0", "role": "default" }} , 
 	{ "name": "imag_arr8_0_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_0", "role": "ap_vld" }} , 
 	{ "name": "imag_arr8_4", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_4", "role": "default" }} , 
 	{ "name": "imag_arr8_4_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_4", "role": "ap_vld" }} , 
 	{ "name": "real_arr8_3", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_3", "role": "default" }} , 
 	{ "name": "real_arr8_3_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_3", "role": "ap_vld" }} , 
 	{ "name": "real_arr8_7", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_7", "role": "default" }} , 
 	{ "name": "real_arr8_7_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_7", "role": "ap_vld" }} , 
 	{ "name": "real_arr8_2", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_2", "role": "default" }} , 
 	{ "name": "real_arr8_2_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_2", "role": "ap_vld" }} , 
 	{ "name": "real_arr8_6", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_6", "role": "default" }} , 
 	{ "name": "real_arr8_6_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_6", "role": "ap_vld" }} , 
 	{ "name": "real_arr8_1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_1", "role": "default" }} , 
 	{ "name": "real_arr8_1_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_1", "role": "ap_vld" }} , 
 	{ "name": "real_arr8_5", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_5", "role": "default" }} , 
 	{ "name": "real_arr8_5_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_5", "role": "ap_vld" }} , 
 	{ "name": "real_arr8_0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_0", "role": "default" }} , 
 	{ "name": "real_arr8_0_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_0", "role": "ap_vld" }} , 
 	{ "name": "real_arr8_4", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_4", "role": "default" }} , 
 	{ "name": "real_arr8_4_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_4", "role": "ap_vld" }}  ]}
set Spec2ImplPortList { 
	real_arr16_0 { ap_memory {  { real_arr16_0_address0 mem_address 1 2 }  { real_arr16_0_ce0 mem_ce 1 1 }  { real_arr16_0_q0 mem_dout 0 32 } } }
	real_arr16_1 { ap_memory {  { real_arr16_1_address0 mem_address 1 2 }  { real_arr16_1_ce0 mem_ce 1 1 }  { real_arr16_1_q0 mem_dout 0 32 } } }
	imag_arr16_0 { ap_memory {  { imag_arr16_0_address0 mem_address 1 2 }  { imag_arr16_0_ce0 mem_ce 1 1 }  { imag_arr16_0_q0 mem_dout 0 32 } } }
	imag_arr16_1 { ap_memory {  { imag_arr16_1_address0 mem_address 1 2 }  { imag_arr16_1_ce0 mem_ce 1 1 }  { imag_arr16_1_q0 mem_dout 0 32 } } }
	real_arr16_2 { ap_memory {  { real_arr16_2_address0 mem_address 1 2 }  { real_arr16_2_ce0 mem_ce 1 1 }  { real_arr16_2_q0 mem_dout 0 32 } } }
	real_arr16_3 { ap_memory {  { real_arr16_3_address0 mem_address 1 2 }  { real_arr16_3_ce0 mem_ce 1 1 }  { real_arr16_3_q0 mem_dout 0 32 } } }
	imag_arr16_2 { ap_memory {  { imag_arr16_2_address0 mem_address 1 2 }  { imag_arr16_2_ce0 mem_ce 1 1 }  { imag_arr16_2_q0 mem_dout 0 32 } } }
	imag_arr16_3 { ap_memory {  { imag_arr16_3_address0 mem_address 1 2 }  { imag_arr16_3_ce0 mem_ce 1 1 }  { imag_arr16_3_q0 mem_dout 0 32 } } }
	imag_arr8_3 { ap_vld {  { imag_arr8_3 out_data 1 32 }  { imag_arr8_3_ap_vld out_vld 1 1 } } }
	imag_arr8_7 { ap_vld {  { imag_arr8_7 out_data 1 32 }  { imag_arr8_7_ap_vld out_vld 1 1 } } }
	imag_arr8_2 { ap_vld {  { imag_arr8_2 out_data 1 32 }  { imag_arr8_2_ap_vld out_vld 1 1 } } }
	imag_arr8_6 { ap_vld {  { imag_arr8_6 out_data 1 32 }  { imag_arr8_6_ap_vld out_vld 1 1 } } }
	imag_arr8_1 { ap_vld {  { imag_arr8_1 out_data 1 32 }  { imag_arr8_1_ap_vld out_vld 1 1 } } }
	imag_arr8_5 { ap_vld {  { imag_arr8_5 out_data 1 32 }  { imag_arr8_5_ap_vld out_vld 1 1 } } }
	imag_arr8_0 { ap_vld {  { imag_arr8_0 out_data 1 32 }  { imag_arr8_0_ap_vld out_vld 1 1 } } }
	imag_arr8_4 { ap_vld {  { imag_arr8_4 out_data 1 32 }  { imag_arr8_4_ap_vld out_vld 1 1 } } }
	real_arr8_3 { ap_vld {  { real_arr8_3 out_data 1 32 }  { real_arr8_3_ap_vld out_vld 1 1 } } }
	real_arr8_7 { ap_vld {  { real_arr8_7 out_data 1 32 }  { real_arr8_7_ap_vld out_vld 1 1 } } }
	real_arr8_2 { ap_vld {  { real_arr8_2 out_data 1 32 }  { real_arr8_2_ap_vld out_vld 1 1 } } }
	real_arr8_6 { ap_vld {  { real_arr8_6 out_data 1 32 }  { real_arr8_6_ap_vld out_vld 1 1 } } }
	real_arr8_1 { ap_vld {  { real_arr8_1 out_data 1 32 }  { real_arr8_1_ap_vld out_vld 1 1 } } }
	real_arr8_5 { ap_vld {  { real_arr8_5 out_data 1 32 }  { real_arr8_5_ap_vld out_vld 1 1 } } }
	real_arr8_0 { ap_vld {  { real_arr8_0 out_data 1 32 }  { real_arr8_0_ap_vld out_vld 1 1 } } }
	real_arr8_4 { ap_vld {  { real_arr8_4 out_data 1 32 }  { real_arr8_4_ap_vld out_vld 1 1 } } }
}
