#include "dft_loop_flow256.h"
#include "AESL_pkg.h"

using namespace std;

namespace ap_rtl {

void dft_loop_flow256::thread_ap_done() {
    if (((!esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
          esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st1_fsm_0.read())) || 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st15_fsm_2.read()))) {
        ap_done = ap_const_logic_1;
    } else {
        ap_done = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_ap_idle() {
    if ((!esl_seteq<1,1,1>(ap_const_logic_1, ap_start.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st1_fsm_0.read()))) {
        ap_idle = ap_const_logic_1;
    } else {
        ap_idle = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_ap_ready() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_st15_fsm_2.read())) {
        ap_ready = ap_const_logic_1;
    } else {
        ap_ready = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_ap_sig_bdd_21() {
    ap_sig_bdd_21 = esl_seteq<1,1,1>(ap_CS_fsm.read().range(0, 0), ap_const_lv1_1);
}

void dft_loop_flow256::thread_ap_sig_bdd_2753() {
    ap_sig_bdd_2753 = esl_seteq<1,1,1>(ap_const_lv1_1, ap_CS_fsm.read().range(1, 1));
}

void dft_loop_flow256::thread_ap_sig_bdd_6108() {
    ap_sig_bdd_6108 = esl_seteq<1,1,1>(ap_const_lv1_1, ap_CS_fsm.read().range(2, 2));
}

void dft_loop_flow256::thread_ap_sig_cseq_ST_pp0_stg0_fsm_1() {
    if (ap_sig_bdd_2753.read()) {
        ap_sig_cseq_ST_pp0_stg0_fsm_1 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_pp0_stg0_fsm_1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_ap_sig_cseq_ST_st15_fsm_2() {
    if (ap_sig_bdd_6108.read()) {
        ap_sig_cseq_ST_st15_fsm_2 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st15_fsm_2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_ap_sig_cseq_ST_st1_fsm_0() {
    if (ap_sig_bdd_21.read()) {
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_1;
    } else {
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_0_address0() {
    cct_0_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_0_address1() {
    cct_0_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_0_address2() {
    cct_0_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_0_address3() {
    cct_0_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_0_address4() {
    cct_0_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_0_address5() {
    cct_0_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_0_address6() {
    cct_0_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_0_address7() {
    cct_0_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_0_ce0 = ap_const_logic_1;
    } else {
        cct_0_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_0_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_0_ce1 = ap_const_logic_1;
    } else {
        cct_0_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_0_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_0_ce2 = ap_const_logic_1;
    } else {
        cct_0_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_0_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_0_ce3 = ap_const_logic_1;
    } else {
        cct_0_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_0_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_0_ce4 = ap_const_logic_1;
    } else {
        cct_0_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_0_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_0_ce5 = ap_const_logic_1;
    } else {
        cct_0_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_0_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_0_ce6 = ap_const_logic_1;
    } else {
        cct_0_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_0_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_0_ce7 = ap_const_logic_1;
    } else {
        cct_0_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_10_address0() {
    cct_10_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_10_address1() {
    cct_10_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_10_address2() {
    cct_10_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_10_address3() {
    cct_10_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_10_address4() {
    cct_10_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_10_address5() {
    cct_10_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_10_address6() {
    cct_10_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_10_address7() {
    cct_10_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_10_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_10_ce0 = ap_const_logic_1;
    } else {
        cct_10_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_10_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_10_ce1 = ap_const_logic_1;
    } else {
        cct_10_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_10_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_10_ce2 = ap_const_logic_1;
    } else {
        cct_10_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_10_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_10_ce3 = ap_const_logic_1;
    } else {
        cct_10_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_10_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_10_ce4 = ap_const_logic_1;
    } else {
        cct_10_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_10_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_10_ce5 = ap_const_logic_1;
    } else {
        cct_10_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_10_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_10_ce6 = ap_const_logic_1;
    } else {
        cct_10_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_10_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_10_ce7 = ap_const_logic_1;
    } else {
        cct_10_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_11_address0() {
    cct_11_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_11_address1() {
    cct_11_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_11_address2() {
    cct_11_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_11_address3() {
    cct_11_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_11_address4() {
    cct_11_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_11_address5() {
    cct_11_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_11_address6() {
    cct_11_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_11_address7() {
    cct_11_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_11_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_11_ce0 = ap_const_logic_1;
    } else {
        cct_11_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_11_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_11_ce1 = ap_const_logic_1;
    } else {
        cct_11_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_11_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_11_ce2 = ap_const_logic_1;
    } else {
        cct_11_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_11_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_11_ce3 = ap_const_logic_1;
    } else {
        cct_11_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_11_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_11_ce4 = ap_const_logic_1;
    } else {
        cct_11_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_11_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_11_ce5 = ap_const_logic_1;
    } else {
        cct_11_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_11_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_11_ce6 = ap_const_logic_1;
    } else {
        cct_11_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_11_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_11_ce7 = ap_const_logic_1;
    } else {
        cct_11_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_12_address0() {
    cct_12_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_12_address1() {
    cct_12_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_12_address2() {
    cct_12_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_12_address3() {
    cct_12_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_12_address4() {
    cct_12_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_12_address5() {
    cct_12_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_12_address6() {
    cct_12_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_12_address7() {
    cct_12_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_12_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_12_ce0 = ap_const_logic_1;
    } else {
        cct_12_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_12_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_12_ce1 = ap_const_logic_1;
    } else {
        cct_12_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_12_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_12_ce2 = ap_const_logic_1;
    } else {
        cct_12_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_12_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_12_ce3 = ap_const_logic_1;
    } else {
        cct_12_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_12_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_12_ce4 = ap_const_logic_1;
    } else {
        cct_12_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_12_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_12_ce5 = ap_const_logic_1;
    } else {
        cct_12_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_12_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_12_ce6 = ap_const_logic_1;
    } else {
        cct_12_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_12_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_12_ce7 = ap_const_logic_1;
    } else {
        cct_12_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_13_address0() {
    cct_13_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_13_address1() {
    cct_13_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_13_address2() {
    cct_13_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_13_address3() {
    cct_13_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_13_address4() {
    cct_13_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_13_address5() {
    cct_13_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_13_address6() {
    cct_13_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_13_address7() {
    cct_13_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_13_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_13_ce0 = ap_const_logic_1;
    } else {
        cct_13_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_13_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_13_ce1 = ap_const_logic_1;
    } else {
        cct_13_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_13_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_13_ce2 = ap_const_logic_1;
    } else {
        cct_13_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_13_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_13_ce3 = ap_const_logic_1;
    } else {
        cct_13_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_13_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_13_ce4 = ap_const_logic_1;
    } else {
        cct_13_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_13_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_13_ce5 = ap_const_logic_1;
    } else {
        cct_13_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_13_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_13_ce6 = ap_const_logic_1;
    } else {
        cct_13_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_13_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_13_ce7 = ap_const_logic_1;
    } else {
        cct_13_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_14_address0() {
    cct_14_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_14_address1() {
    cct_14_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_14_address2() {
    cct_14_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_14_address3() {
    cct_14_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_14_address4() {
    cct_14_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_14_address5() {
    cct_14_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_14_address6() {
    cct_14_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_14_address7() {
    cct_14_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_14_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_14_ce0 = ap_const_logic_1;
    } else {
        cct_14_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_14_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_14_ce1 = ap_const_logic_1;
    } else {
        cct_14_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_14_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_14_ce2 = ap_const_logic_1;
    } else {
        cct_14_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_14_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_14_ce3 = ap_const_logic_1;
    } else {
        cct_14_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_14_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_14_ce4 = ap_const_logic_1;
    } else {
        cct_14_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_14_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_14_ce5 = ap_const_logic_1;
    } else {
        cct_14_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_14_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_14_ce6 = ap_const_logic_1;
    } else {
        cct_14_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_14_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_14_ce7 = ap_const_logic_1;
    } else {
        cct_14_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_15_address0() {
    cct_15_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_15_address1() {
    cct_15_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_15_address2() {
    cct_15_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_15_address3() {
    cct_15_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_15_address4() {
    cct_15_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_15_address5() {
    cct_15_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_15_address6() {
    cct_15_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_15_address7() {
    cct_15_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_15_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_15_ce0 = ap_const_logic_1;
    } else {
        cct_15_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_15_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_15_ce1 = ap_const_logic_1;
    } else {
        cct_15_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_15_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_15_ce2 = ap_const_logic_1;
    } else {
        cct_15_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_15_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_15_ce3 = ap_const_logic_1;
    } else {
        cct_15_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_15_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_15_ce4 = ap_const_logic_1;
    } else {
        cct_15_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_15_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_15_ce5 = ap_const_logic_1;
    } else {
        cct_15_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_15_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_15_ce6 = ap_const_logic_1;
    } else {
        cct_15_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_15_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_15_ce7 = ap_const_logic_1;
    } else {
        cct_15_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_16_address0() {
    cct_16_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_16_address1() {
    cct_16_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_16_address2() {
    cct_16_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_16_address3() {
    cct_16_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_16_address4() {
    cct_16_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_16_address5() {
    cct_16_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_16_address6() {
    cct_16_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_16_address7() {
    cct_16_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_16_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_16_ce0 = ap_const_logic_1;
    } else {
        cct_16_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_16_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_16_ce1 = ap_const_logic_1;
    } else {
        cct_16_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_16_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_16_ce2 = ap_const_logic_1;
    } else {
        cct_16_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_16_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_16_ce3 = ap_const_logic_1;
    } else {
        cct_16_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_16_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_16_ce4 = ap_const_logic_1;
    } else {
        cct_16_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_16_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_16_ce5 = ap_const_logic_1;
    } else {
        cct_16_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_16_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_16_ce6 = ap_const_logic_1;
    } else {
        cct_16_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_16_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_16_ce7 = ap_const_logic_1;
    } else {
        cct_16_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_17_address0() {
    cct_17_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_17_address1() {
    cct_17_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_17_address2() {
    cct_17_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_17_address3() {
    cct_17_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_17_address4() {
    cct_17_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_17_address5() {
    cct_17_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_17_address6() {
    cct_17_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_17_address7() {
    cct_17_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_17_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_17_ce0 = ap_const_logic_1;
    } else {
        cct_17_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_17_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_17_ce1 = ap_const_logic_1;
    } else {
        cct_17_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_17_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_17_ce2 = ap_const_logic_1;
    } else {
        cct_17_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_17_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_17_ce3 = ap_const_logic_1;
    } else {
        cct_17_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_17_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_17_ce4 = ap_const_logic_1;
    } else {
        cct_17_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_17_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_17_ce5 = ap_const_logic_1;
    } else {
        cct_17_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_17_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_17_ce6 = ap_const_logic_1;
    } else {
        cct_17_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_17_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_17_ce7 = ap_const_logic_1;
    } else {
        cct_17_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_18_address0() {
    cct_18_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_18_address1() {
    cct_18_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_18_address2() {
    cct_18_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_18_address3() {
    cct_18_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_18_address4() {
    cct_18_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_18_address5() {
    cct_18_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_18_address6() {
    cct_18_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_18_address7() {
    cct_18_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_18_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_18_ce0 = ap_const_logic_1;
    } else {
        cct_18_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_18_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_18_ce1 = ap_const_logic_1;
    } else {
        cct_18_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_18_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_18_ce2 = ap_const_logic_1;
    } else {
        cct_18_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_18_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_18_ce3 = ap_const_logic_1;
    } else {
        cct_18_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_18_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_18_ce4 = ap_const_logic_1;
    } else {
        cct_18_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_18_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_18_ce5 = ap_const_logic_1;
    } else {
        cct_18_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_18_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_18_ce6 = ap_const_logic_1;
    } else {
        cct_18_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_18_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_18_ce7 = ap_const_logic_1;
    } else {
        cct_18_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_19_address0() {
    cct_19_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_19_address1() {
    cct_19_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_19_address2() {
    cct_19_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_19_address3() {
    cct_19_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_19_address4() {
    cct_19_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_19_address5() {
    cct_19_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_19_address6() {
    cct_19_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_19_address7() {
    cct_19_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_19_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_19_ce0 = ap_const_logic_1;
    } else {
        cct_19_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_19_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_19_ce1 = ap_const_logic_1;
    } else {
        cct_19_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_19_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_19_ce2 = ap_const_logic_1;
    } else {
        cct_19_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_19_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_19_ce3 = ap_const_logic_1;
    } else {
        cct_19_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_19_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_19_ce4 = ap_const_logic_1;
    } else {
        cct_19_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_19_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_19_ce5 = ap_const_logic_1;
    } else {
        cct_19_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_19_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_19_ce6 = ap_const_logic_1;
    } else {
        cct_19_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_19_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_19_ce7 = ap_const_logic_1;
    } else {
        cct_19_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_1_address0() {
    cct_1_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_1_address1() {
    cct_1_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_1_address2() {
    cct_1_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_1_address3() {
    cct_1_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_1_address4() {
    cct_1_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_1_address5() {
    cct_1_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_1_address6() {
    cct_1_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_1_address7() {
    cct_1_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_1_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_1_ce0 = ap_const_logic_1;
    } else {
        cct_1_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_1_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_1_ce1 = ap_const_logic_1;
    } else {
        cct_1_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_1_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_1_ce2 = ap_const_logic_1;
    } else {
        cct_1_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_1_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_1_ce3 = ap_const_logic_1;
    } else {
        cct_1_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_1_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_1_ce4 = ap_const_logic_1;
    } else {
        cct_1_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_1_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_1_ce5 = ap_const_logic_1;
    } else {
        cct_1_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_1_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_1_ce6 = ap_const_logic_1;
    } else {
        cct_1_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_1_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_1_ce7 = ap_const_logic_1;
    } else {
        cct_1_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_20_address0() {
    cct_20_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_20_address1() {
    cct_20_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_20_address2() {
    cct_20_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_20_address3() {
    cct_20_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_20_address4() {
    cct_20_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_20_address5() {
    cct_20_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_20_address6() {
    cct_20_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_20_address7() {
    cct_20_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_20_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_20_ce0 = ap_const_logic_1;
    } else {
        cct_20_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_20_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_20_ce1 = ap_const_logic_1;
    } else {
        cct_20_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_20_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_20_ce2 = ap_const_logic_1;
    } else {
        cct_20_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_20_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_20_ce3 = ap_const_logic_1;
    } else {
        cct_20_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_20_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_20_ce4 = ap_const_logic_1;
    } else {
        cct_20_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_20_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_20_ce5 = ap_const_logic_1;
    } else {
        cct_20_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_20_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_20_ce6 = ap_const_logic_1;
    } else {
        cct_20_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_20_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_20_ce7 = ap_const_logic_1;
    } else {
        cct_20_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_21_address0() {
    cct_21_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_21_address1() {
    cct_21_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_21_address2() {
    cct_21_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_21_address3() {
    cct_21_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_21_address4() {
    cct_21_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_21_address5() {
    cct_21_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_21_address6() {
    cct_21_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_21_address7() {
    cct_21_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_21_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_21_ce0 = ap_const_logic_1;
    } else {
        cct_21_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_21_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_21_ce1 = ap_const_logic_1;
    } else {
        cct_21_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_21_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_21_ce2 = ap_const_logic_1;
    } else {
        cct_21_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_21_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_21_ce3 = ap_const_logic_1;
    } else {
        cct_21_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_21_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_21_ce4 = ap_const_logic_1;
    } else {
        cct_21_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_21_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_21_ce5 = ap_const_logic_1;
    } else {
        cct_21_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_21_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_21_ce6 = ap_const_logic_1;
    } else {
        cct_21_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_21_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_21_ce7 = ap_const_logic_1;
    } else {
        cct_21_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_22_address0() {
    cct_22_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_22_address1() {
    cct_22_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_22_address2() {
    cct_22_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_22_address3() {
    cct_22_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_22_address4() {
    cct_22_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_22_address5() {
    cct_22_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_22_address6() {
    cct_22_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_22_address7() {
    cct_22_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_22_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_22_ce0 = ap_const_logic_1;
    } else {
        cct_22_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_22_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_22_ce1 = ap_const_logic_1;
    } else {
        cct_22_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_22_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_22_ce2 = ap_const_logic_1;
    } else {
        cct_22_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_22_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_22_ce3 = ap_const_logic_1;
    } else {
        cct_22_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_22_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_22_ce4 = ap_const_logic_1;
    } else {
        cct_22_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_22_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_22_ce5 = ap_const_logic_1;
    } else {
        cct_22_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_22_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_22_ce6 = ap_const_logic_1;
    } else {
        cct_22_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_22_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_22_ce7 = ap_const_logic_1;
    } else {
        cct_22_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_23_address0() {
    cct_23_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_23_address1() {
    cct_23_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_23_address2() {
    cct_23_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_23_address3() {
    cct_23_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_23_address4() {
    cct_23_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_23_address5() {
    cct_23_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_23_address6() {
    cct_23_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_23_address7() {
    cct_23_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_23_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_23_ce0 = ap_const_logic_1;
    } else {
        cct_23_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_23_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_23_ce1 = ap_const_logic_1;
    } else {
        cct_23_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_23_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_23_ce2 = ap_const_logic_1;
    } else {
        cct_23_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_23_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_23_ce3 = ap_const_logic_1;
    } else {
        cct_23_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_23_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_23_ce4 = ap_const_logic_1;
    } else {
        cct_23_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_23_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_23_ce5 = ap_const_logic_1;
    } else {
        cct_23_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_23_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_23_ce6 = ap_const_logic_1;
    } else {
        cct_23_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_23_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_23_ce7 = ap_const_logic_1;
    } else {
        cct_23_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_24_address0() {
    cct_24_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_24_address1() {
    cct_24_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_24_address2() {
    cct_24_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_24_address3() {
    cct_24_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_24_address4() {
    cct_24_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_24_address5() {
    cct_24_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_24_address6() {
    cct_24_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_24_address7() {
    cct_24_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_24_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_24_ce0 = ap_const_logic_1;
    } else {
        cct_24_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_24_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_24_ce1 = ap_const_logic_1;
    } else {
        cct_24_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_24_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_24_ce2 = ap_const_logic_1;
    } else {
        cct_24_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_24_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_24_ce3 = ap_const_logic_1;
    } else {
        cct_24_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_24_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_24_ce4 = ap_const_logic_1;
    } else {
        cct_24_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_24_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_24_ce5 = ap_const_logic_1;
    } else {
        cct_24_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_24_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_24_ce6 = ap_const_logic_1;
    } else {
        cct_24_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_24_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_24_ce7 = ap_const_logic_1;
    } else {
        cct_24_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_25_address0() {
    cct_25_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_25_address1() {
    cct_25_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_25_address2() {
    cct_25_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_25_address3() {
    cct_25_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_25_address4() {
    cct_25_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_25_address5() {
    cct_25_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_25_address6() {
    cct_25_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_25_address7() {
    cct_25_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_25_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_25_ce0 = ap_const_logic_1;
    } else {
        cct_25_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_25_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_25_ce1 = ap_const_logic_1;
    } else {
        cct_25_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_25_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_25_ce2 = ap_const_logic_1;
    } else {
        cct_25_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_25_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_25_ce3 = ap_const_logic_1;
    } else {
        cct_25_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_25_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_25_ce4 = ap_const_logic_1;
    } else {
        cct_25_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_25_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_25_ce5 = ap_const_logic_1;
    } else {
        cct_25_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_25_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_25_ce6 = ap_const_logic_1;
    } else {
        cct_25_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_25_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_25_ce7 = ap_const_logic_1;
    } else {
        cct_25_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_26_address0() {
    cct_26_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_26_address1() {
    cct_26_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_26_address2() {
    cct_26_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_26_address3() {
    cct_26_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_26_address4() {
    cct_26_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_26_address5() {
    cct_26_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_26_address6() {
    cct_26_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_26_address7() {
    cct_26_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_26_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_26_ce0 = ap_const_logic_1;
    } else {
        cct_26_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_26_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_26_ce1 = ap_const_logic_1;
    } else {
        cct_26_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_26_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_26_ce2 = ap_const_logic_1;
    } else {
        cct_26_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_26_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_26_ce3 = ap_const_logic_1;
    } else {
        cct_26_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_26_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_26_ce4 = ap_const_logic_1;
    } else {
        cct_26_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_26_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_26_ce5 = ap_const_logic_1;
    } else {
        cct_26_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_26_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_26_ce6 = ap_const_logic_1;
    } else {
        cct_26_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_26_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_26_ce7 = ap_const_logic_1;
    } else {
        cct_26_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_27_address0() {
    cct_27_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_27_address1() {
    cct_27_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_27_address2() {
    cct_27_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_27_address3() {
    cct_27_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_27_address4() {
    cct_27_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_27_address5() {
    cct_27_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_27_address6() {
    cct_27_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_27_address7() {
    cct_27_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_27_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_27_ce0 = ap_const_logic_1;
    } else {
        cct_27_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_27_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_27_ce1 = ap_const_logic_1;
    } else {
        cct_27_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_27_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_27_ce2 = ap_const_logic_1;
    } else {
        cct_27_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_27_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_27_ce3 = ap_const_logic_1;
    } else {
        cct_27_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_27_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_27_ce4 = ap_const_logic_1;
    } else {
        cct_27_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_27_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_27_ce5 = ap_const_logic_1;
    } else {
        cct_27_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_27_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_27_ce6 = ap_const_logic_1;
    } else {
        cct_27_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_27_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_27_ce7 = ap_const_logic_1;
    } else {
        cct_27_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_28_address0() {
    cct_28_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_28_address1() {
    cct_28_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_28_address2() {
    cct_28_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_28_address3() {
    cct_28_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_28_address4() {
    cct_28_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_28_address5() {
    cct_28_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_28_address6() {
    cct_28_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_28_address7() {
    cct_28_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_28_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_28_ce0 = ap_const_logic_1;
    } else {
        cct_28_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_28_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_28_ce1 = ap_const_logic_1;
    } else {
        cct_28_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_28_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_28_ce2 = ap_const_logic_1;
    } else {
        cct_28_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_28_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_28_ce3 = ap_const_logic_1;
    } else {
        cct_28_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_28_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_28_ce4 = ap_const_logic_1;
    } else {
        cct_28_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_28_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_28_ce5 = ap_const_logic_1;
    } else {
        cct_28_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_28_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_28_ce6 = ap_const_logic_1;
    } else {
        cct_28_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_28_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_28_ce7 = ap_const_logic_1;
    } else {
        cct_28_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_29_address0() {
    cct_29_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_29_address1() {
    cct_29_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_29_address2() {
    cct_29_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_29_address3() {
    cct_29_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_29_address4() {
    cct_29_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_29_address5() {
    cct_29_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_29_address6() {
    cct_29_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_29_address7() {
    cct_29_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_29_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_29_ce0 = ap_const_logic_1;
    } else {
        cct_29_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_29_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_29_ce1 = ap_const_logic_1;
    } else {
        cct_29_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_29_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_29_ce2 = ap_const_logic_1;
    } else {
        cct_29_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_29_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_29_ce3 = ap_const_logic_1;
    } else {
        cct_29_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_29_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_29_ce4 = ap_const_logic_1;
    } else {
        cct_29_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_29_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_29_ce5 = ap_const_logic_1;
    } else {
        cct_29_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_29_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_29_ce6 = ap_const_logic_1;
    } else {
        cct_29_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_29_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_29_ce7 = ap_const_logic_1;
    } else {
        cct_29_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_2_address0() {
    cct_2_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_2_address1() {
    cct_2_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_2_address2() {
    cct_2_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_2_address3() {
    cct_2_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_2_address4() {
    cct_2_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_2_address5() {
    cct_2_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_2_address6() {
    cct_2_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_2_address7() {
    cct_2_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_2_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_2_ce0 = ap_const_logic_1;
    } else {
        cct_2_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_2_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_2_ce1 = ap_const_logic_1;
    } else {
        cct_2_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_2_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_2_ce2 = ap_const_logic_1;
    } else {
        cct_2_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_2_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_2_ce3 = ap_const_logic_1;
    } else {
        cct_2_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_2_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_2_ce4 = ap_const_logic_1;
    } else {
        cct_2_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_2_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_2_ce5 = ap_const_logic_1;
    } else {
        cct_2_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_2_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_2_ce6 = ap_const_logic_1;
    } else {
        cct_2_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_2_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_2_ce7 = ap_const_logic_1;
    } else {
        cct_2_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_30_address0() {
    cct_30_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_30_address1() {
    cct_30_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_30_address2() {
    cct_30_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_30_address3() {
    cct_30_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_30_address4() {
    cct_30_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_30_address5() {
    cct_30_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_30_address6() {
    cct_30_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_30_address7() {
    cct_30_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_30_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_30_ce0 = ap_const_logic_1;
    } else {
        cct_30_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_30_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_30_ce1 = ap_const_logic_1;
    } else {
        cct_30_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_30_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_30_ce2 = ap_const_logic_1;
    } else {
        cct_30_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_30_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_30_ce3 = ap_const_logic_1;
    } else {
        cct_30_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_30_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_30_ce4 = ap_const_logic_1;
    } else {
        cct_30_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_30_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_30_ce5 = ap_const_logic_1;
    } else {
        cct_30_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_30_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_30_ce6 = ap_const_logic_1;
    } else {
        cct_30_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_30_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_30_ce7 = ap_const_logic_1;
    } else {
        cct_30_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_31_address0() {
    cct_31_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_31_address1() {
    cct_31_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_31_address2() {
    cct_31_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_31_address3() {
    cct_31_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_31_address4() {
    cct_31_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_31_address5() {
    cct_31_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_31_address6() {
    cct_31_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_31_address7() {
    cct_31_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_31_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_31_ce0 = ap_const_logic_1;
    } else {
        cct_31_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_31_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_31_ce1 = ap_const_logic_1;
    } else {
        cct_31_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_31_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_31_ce2 = ap_const_logic_1;
    } else {
        cct_31_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_31_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_31_ce3 = ap_const_logic_1;
    } else {
        cct_31_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_31_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_31_ce4 = ap_const_logic_1;
    } else {
        cct_31_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_31_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_31_ce5 = ap_const_logic_1;
    } else {
        cct_31_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_31_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_31_ce6 = ap_const_logic_1;
    } else {
        cct_31_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_31_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_31_ce7 = ap_const_logic_1;
    } else {
        cct_31_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_3_address0() {
    cct_3_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_3_address1() {
    cct_3_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_3_address2() {
    cct_3_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_3_address3() {
    cct_3_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_3_address4() {
    cct_3_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_3_address5() {
    cct_3_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_3_address6() {
    cct_3_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_3_address7() {
    cct_3_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_3_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_3_ce0 = ap_const_logic_1;
    } else {
        cct_3_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_3_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_3_ce1 = ap_const_logic_1;
    } else {
        cct_3_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_3_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_3_ce2 = ap_const_logic_1;
    } else {
        cct_3_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_3_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_3_ce3 = ap_const_logic_1;
    } else {
        cct_3_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_3_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_3_ce4 = ap_const_logic_1;
    } else {
        cct_3_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_3_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_3_ce5 = ap_const_logic_1;
    } else {
        cct_3_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_3_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_3_ce6 = ap_const_logic_1;
    } else {
        cct_3_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_3_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_3_ce7 = ap_const_logic_1;
    } else {
        cct_3_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_4_address0() {
    cct_4_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_4_address1() {
    cct_4_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_4_address2() {
    cct_4_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_4_address3() {
    cct_4_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_4_address4() {
    cct_4_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_4_address5() {
    cct_4_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_4_address6() {
    cct_4_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_4_address7() {
    cct_4_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_4_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_4_ce0 = ap_const_logic_1;
    } else {
        cct_4_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_4_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_4_ce1 = ap_const_logic_1;
    } else {
        cct_4_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_4_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_4_ce2 = ap_const_logic_1;
    } else {
        cct_4_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_4_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_4_ce3 = ap_const_logic_1;
    } else {
        cct_4_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_4_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_4_ce4 = ap_const_logic_1;
    } else {
        cct_4_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_4_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_4_ce5 = ap_const_logic_1;
    } else {
        cct_4_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_4_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_4_ce6 = ap_const_logic_1;
    } else {
        cct_4_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_4_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_4_ce7 = ap_const_logic_1;
    } else {
        cct_4_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_5_address0() {
    cct_5_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_5_address1() {
    cct_5_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_5_address2() {
    cct_5_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_5_address3() {
    cct_5_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_5_address4() {
    cct_5_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_5_address5() {
    cct_5_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_5_address6() {
    cct_5_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_5_address7() {
    cct_5_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_5_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_5_ce0 = ap_const_logic_1;
    } else {
        cct_5_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_5_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_5_ce1 = ap_const_logic_1;
    } else {
        cct_5_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_5_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_5_ce2 = ap_const_logic_1;
    } else {
        cct_5_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_5_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_5_ce3 = ap_const_logic_1;
    } else {
        cct_5_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_5_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_5_ce4 = ap_const_logic_1;
    } else {
        cct_5_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_5_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_5_ce5 = ap_const_logic_1;
    } else {
        cct_5_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_5_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_5_ce6 = ap_const_logic_1;
    } else {
        cct_5_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_5_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_5_ce7 = ap_const_logic_1;
    } else {
        cct_5_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_6_address0() {
    cct_6_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_6_address1() {
    cct_6_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_6_address2() {
    cct_6_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_6_address3() {
    cct_6_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_6_address4() {
    cct_6_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_6_address5() {
    cct_6_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_6_address6() {
    cct_6_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_6_address7() {
    cct_6_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_6_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_6_ce0 = ap_const_logic_1;
    } else {
        cct_6_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_6_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_6_ce1 = ap_const_logic_1;
    } else {
        cct_6_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_6_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_6_ce2 = ap_const_logic_1;
    } else {
        cct_6_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_6_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_6_ce3 = ap_const_logic_1;
    } else {
        cct_6_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_6_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_6_ce4 = ap_const_logic_1;
    } else {
        cct_6_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_6_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_6_ce5 = ap_const_logic_1;
    } else {
        cct_6_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_6_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_6_ce6 = ap_const_logic_1;
    } else {
        cct_6_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_6_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_6_ce7 = ap_const_logic_1;
    } else {
        cct_6_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_7_address0() {
    cct_7_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_7_address1() {
    cct_7_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_7_address2() {
    cct_7_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_7_address3() {
    cct_7_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_7_address4() {
    cct_7_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_7_address5() {
    cct_7_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_7_address6() {
    cct_7_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_7_address7() {
    cct_7_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_7_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_7_ce0 = ap_const_logic_1;
    } else {
        cct_7_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_7_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_7_ce1 = ap_const_logic_1;
    } else {
        cct_7_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_7_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_7_ce2 = ap_const_logic_1;
    } else {
        cct_7_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_7_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_7_ce3 = ap_const_logic_1;
    } else {
        cct_7_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_7_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_7_ce4 = ap_const_logic_1;
    } else {
        cct_7_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_7_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_7_ce5 = ap_const_logic_1;
    } else {
        cct_7_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_7_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_7_ce6 = ap_const_logic_1;
    } else {
        cct_7_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_7_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_7_ce7 = ap_const_logic_1;
    } else {
        cct_7_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_8_address0() {
    cct_8_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_8_address1() {
    cct_8_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_8_address2() {
    cct_8_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_8_address3() {
    cct_8_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_8_address4() {
    cct_8_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_8_address5() {
    cct_8_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_8_address6() {
    cct_8_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_8_address7() {
    cct_8_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_8_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_8_ce0 = ap_const_logic_1;
    } else {
        cct_8_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_8_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_8_ce1 = ap_const_logic_1;
    } else {
        cct_8_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_8_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_8_ce2 = ap_const_logic_1;
    } else {
        cct_8_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_8_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_8_ce3 = ap_const_logic_1;
    } else {
        cct_8_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_8_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_8_ce4 = ap_const_logic_1;
    } else {
        cct_8_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_8_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_8_ce5 = ap_const_logic_1;
    } else {
        cct_8_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_8_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_8_ce6 = ap_const_logic_1;
    } else {
        cct_8_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_8_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_8_ce7 = ap_const_logic_1;
    } else {
        cct_8_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_9_address0() {
    cct_9_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_cct_9_address1() {
    cct_9_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_cct_9_address2() {
    cct_9_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_cct_9_address3() {
    cct_9_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_cct_9_address4() {
    cct_9_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_cct_9_address5() {
    cct_9_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_cct_9_address6() {
    cct_9_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_cct_9_address7() {
    cct_9_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_cct_9_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_9_ce0 = ap_const_logic_1;
    } else {
        cct_9_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_9_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_9_ce1 = ap_const_logic_1;
    } else {
        cct_9_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_9_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_9_ce2 = ap_const_logic_1;
    } else {
        cct_9_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_9_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_9_ce3 = ap_const_logic_1;
    } else {
        cct_9_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_9_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_9_ce4 = ap_const_logic_1;
    } else {
        cct_9_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_9_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_9_ce5 = ap_const_logic_1;
    } else {
        cct_9_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_9_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_9_ce6 = ap_const_logic_1;
    } else {
        cct_9_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_cct_9_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        cct_9_ce7 = ap_const_logic_1;
    } else {
        cct_9_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_exitcond_fu_7237_p2() {
    exitcond_fu_7237_p2 = (!p_s_reg_7034.read().is_01() || !ap_const_lv9_100.is_01())? sc_lv<1>(): sc_lv<1>(p_s_reg_7034.read() == ap_const_lv9_100);
}

void dft_loop_flow256::thread_grp_fu_7045_ce() {
    grp_fu_7045_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7049_ce() {
    grp_fu_7049_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7053_ce() {
    grp_fu_7053_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7057_ce() {
    grp_fu_7057_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7061_ce() {
    grp_fu_7061_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7065_ce() {
    grp_fu_7065_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7069_ce() {
    grp_fu_7069_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7073_ce() {
    grp_fu_7073_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7077_ce() {
    grp_fu_7077_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7081_ce() {
    grp_fu_7081_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7085_ce() {
    grp_fu_7085_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7089_ce() {
    grp_fu_7089_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7093_ce() {
    grp_fu_7093_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7097_ce() {
    grp_fu_7097_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7101_ce() {
    grp_fu_7101_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7105_ce() {
    grp_fu_7105_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7109_ce() {
    grp_fu_7109_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7113_ce() {
    grp_fu_7113_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7117_ce() {
    grp_fu_7117_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7121_ce() {
    grp_fu_7121_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7125_ce() {
    grp_fu_7125_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7129_ce() {
    grp_fu_7129_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7133_ce() {
    grp_fu_7133_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7137_ce() {
    grp_fu_7137_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7141_ce() {
    grp_fu_7141_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7145_ce() {
    grp_fu_7145_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7149_ce() {
    grp_fu_7149_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7153_ce() {
    grp_fu_7153_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7157_ce() {
    grp_fu_7157_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7161_ce() {
    grp_fu_7161_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7165_ce() {
    grp_fu_7165_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7169_ce() {
    grp_fu_7169_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7173_ce() {
    grp_fu_7173_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7177_ce() {
    grp_fu_7177_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7181_ce() {
    grp_fu_7181_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7185_ce() {
    grp_fu_7185_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7189_ce() {
    grp_fu_7189_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7193_ce() {
    grp_fu_7193_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7197_ce() {
    grp_fu_7197_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7201_ce() {
    grp_fu_7201_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7205_ce() {
    grp_fu_7205_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7209_ce() {
    grp_fu_7209_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7213_ce() {
    grp_fu_7213_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7217_ce() {
    grp_fu_7217_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7221_ce() {
    grp_fu_7221_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7225_ce() {
    grp_fu_7225_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7229_ce() {
    grp_fu_7229_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_grp_fu_7233_ce() {
    grp_fu_7233_ce = ap_const_logic_1;
}

void dft_loop_flow256::thread_imag_arr256_0_address0() {
    imag_arr256_0_address0 =  (sc_lv<6>) (newIndex3_fu_9722_p1.read());
}

void dft_loop_flow256::thread_imag_arr256_0_address1() {
    imag_arr256_0_address1 =  (sc_lv<6>) (newIndex10_fu_9733_p1.read());
}

void dft_loop_flow256::thread_imag_arr256_0_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read())) {
        imag_arr256_0_ce0 = ap_const_logic_1;
    } else {
        imag_arr256_0_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_arr256_0_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read())) {
        imag_arr256_0_ce1 = ap_const_logic_1;
    } else {
        imag_arr256_0_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_arr256_0_d0() {
    imag_arr256_0_d0 = tmp_14_reg_13273.read();
}

void dft_loop_flow256::thread_imag_arr256_0_d1() {
    imag_arr256_0_d1 = tmp_16_4_reg_13313.read();
}

void dft_loop_flow256::thread_imag_arr256_0_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read()) && 
          esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it11.read(), ap_const_lv1_0)))) {
        imag_arr256_0_we0 = ap_const_logic_1;
    } else {
        imag_arr256_0_we0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_arr256_0_we1() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read()) && 
          esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it11.read(), ap_const_lv1_0)))) {
        imag_arr256_0_we1 = ap_const_logic_1;
    } else {
        imag_arr256_0_we1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_arr256_1_address0() {
    imag_arr256_1_address0 =  (sc_lv<6>) (newIndex3_fu_9722_p1.read());
}

void dft_loop_flow256::thread_imag_arr256_1_address1() {
    imag_arr256_1_address1 =  (sc_lv<6>) (newIndex13_fu_9738_p1.read());
}

void dft_loop_flow256::thread_imag_arr256_1_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read())) {
        imag_arr256_1_ce0 = ap_const_logic_1;
    } else {
        imag_arr256_1_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_arr256_1_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read())) {
        imag_arr256_1_ce1 = ap_const_logic_1;
    } else {
        imag_arr256_1_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_arr256_1_d0() {
    imag_arr256_1_d0 = tmp_16_1_reg_13283.read();
}

void dft_loop_flow256::thread_imag_arr256_1_d1() {
    imag_arr256_1_d1 = tmp_16_5_reg_13323.read();
}

void dft_loop_flow256::thread_imag_arr256_1_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read()) && 
          esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it11.read(), ap_const_lv1_0)))) {
        imag_arr256_1_we0 = ap_const_logic_1;
    } else {
        imag_arr256_1_we0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_arr256_1_we1() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read()) && 
          esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it11.read(), ap_const_lv1_0)))) {
        imag_arr256_1_we1 = ap_const_logic_1;
    } else {
        imag_arr256_1_we1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_arr256_2_address0() {
    imag_arr256_2_address0 =  (sc_lv<6>) (newIndex3_fu_9722_p1.read());
}

void dft_loop_flow256::thread_imag_arr256_2_address1() {
    imag_arr256_2_address1 =  (sc_lv<6>) (newIndex16_fu_9743_p1.read());
}

void dft_loop_flow256::thread_imag_arr256_2_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read())) {
        imag_arr256_2_ce0 = ap_const_logic_1;
    } else {
        imag_arr256_2_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_arr256_2_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read())) {
        imag_arr256_2_ce1 = ap_const_logic_1;
    } else {
        imag_arr256_2_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_arr256_2_d0() {
    imag_arr256_2_d0 = tmp_16_2_reg_13293.read();
}

void dft_loop_flow256::thread_imag_arr256_2_d1() {
    imag_arr256_2_d1 = tmp_16_6_reg_13333.read();
}

void dft_loop_flow256::thread_imag_arr256_2_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read()) && 
          esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it11.read(), ap_const_lv1_0)))) {
        imag_arr256_2_we0 = ap_const_logic_1;
    } else {
        imag_arr256_2_we0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_arr256_2_we1() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read()) && 
          esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it11.read(), ap_const_lv1_0)))) {
        imag_arr256_2_we1 = ap_const_logic_1;
    } else {
        imag_arr256_2_we1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_arr256_3_address0() {
    imag_arr256_3_address0 =  (sc_lv<6>) (newIndex3_fu_9722_p1.read());
}

void dft_loop_flow256::thread_imag_arr256_3_address1() {
    imag_arr256_3_address1 =  (sc_lv<6>) (newIndex19_fu_9748_p1.read());
}

void dft_loop_flow256::thread_imag_arr256_3_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read())) {
        imag_arr256_3_ce0 = ap_const_logic_1;
    } else {
        imag_arr256_3_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_arr256_3_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read())) {
        imag_arr256_3_ce1 = ap_const_logic_1;
    } else {
        imag_arr256_3_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_arr256_3_d0() {
    imag_arr256_3_d0 = tmp_16_3_reg_13303.read();
}

void dft_loop_flow256::thread_imag_arr256_3_d1() {
    imag_arr256_3_d1 = tmp_16_7_reg_13343.read();
}

void dft_loop_flow256::thread_imag_arr256_3_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read()) && 
          esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it11.read(), ap_const_lv1_0)))) {
        imag_arr256_3_we0 = ap_const_logic_1;
    } else {
        imag_arr256_3_we0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_arr256_3_we1() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read()) && 
          esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it11.read(), ap_const_lv1_0)))) {
        imag_arr256_3_we1 = ap_const_logic_1;
    } else {
        imag_arr256_3_we1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_0_address0() {
    imag_i_0_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_0_ce0 = ap_const_logic_1;
    } else {
        imag_i_0_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_10_address0() {
    imag_i_10_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_10_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_10_ce0 = ap_const_logic_1;
    } else {
        imag_i_10_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_11_address0() {
    imag_i_11_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_11_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_11_ce0 = ap_const_logic_1;
    } else {
        imag_i_11_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_12_address0() {
    imag_i_12_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_12_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_12_ce0 = ap_const_logic_1;
    } else {
        imag_i_12_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_13_address0() {
    imag_i_13_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_13_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_13_ce0 = ap_const_logic_1;
    } else {
        imag_i_13_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_14_address0() {
    imag_i_14_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_14_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_14_ce0 = ap_const_logic_1;
    } else {
        imag_i_14_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_15_address0() {
    imag_i_15_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_15_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_15_ce0 = ap_const_logic_1;
    } else {
        imag_i_15_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_16_address0() {
    imag_i_16_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_16_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_16_ce0 = ap_const_logic_1;
    } else {
        imag_i_16_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_17_address0() {
    imag_i_17_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_17_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_17_ce0 = ap_const_logic_1;
    } else {
        imag_i_17_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_18_address0() {
    imag_i_18_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_18_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_18_ce0 = ap_const_logic_1;
    } else {
        imag_i_18_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_19_address0() {
    imag_i_19_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_19_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_19_ce0 = ap_const_logic_1;
    } else {
        imag_i_19_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_1_address0() {
    imag_i_1_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_1_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_1_ce0 = ap_const_logic_1;
    } else {
        imag_i_1_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_20_address0() {
    imag_i_20_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_20_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_20_ce0 = ap_const_logic_1;
    } else {
        imag_i_20_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_21_address0() {
    imag_i_21_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_21_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_21_ce0 = ap_const_logic_1;
    } else {
        imag_i_21_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_22_address0() {
    imag_i_22_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_22_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_22_ce0 = ap_const_logic_1;
    } else {
        imag_i_22_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_23_address0() {
    imag_i_23_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_23_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_23_ce0 = ap_const_logic_1;
    } else {
        imag_i_23_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_24_address0() {
    imag_i_24_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_24_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_24_ce0 = ap_const_logic_1;
    } else {
        imag_i_24_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_25_address0() {
    imag_i_25_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_25_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_25_ce0 = ap_const_logic_1;
    } else {
        imag_i_25_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_26_address0() {
    imag_i_26_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_26_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_26_ce0 = ap_const_logic_1;
    } else {
        imag_i_26_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_27_address0() {
    imag_i_27_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_27_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_27_ce0 = ap_const_logic_1;
    } else {
        imag_i_27_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_28_address0() {
    imag_i_28_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_28_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_28_ce0 = ap_const_logic_1;
    } else {
        imag_i_28_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_29_address0() {
    imag_i_29_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_29_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_29_ce0 = ap_const_logic_1;
    } else {
        imag_i_29_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_2_address0() {
    imag_i_2_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_2_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_2_ce0 = ap_const_logic_1;
    } else {
        imag_i_2_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_30_address0() {
    imag_i_30_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_30_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_30_ce0 = ap_const_logic_1;
    } else {
        imag_i_30_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_31_address0() {
    imag_i_31_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_31_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_31_ce0 = ap_const_logic_1;
    } else {
        imag_i_31_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_3_address0() {
    imag_i_3_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_3_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_3_ce0 = ap_const_logic_1;
    } else {
        imag_i_3_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_4_address0() {
    imag_i_4_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_4_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_4_ce0 = ap_const_logic_1;
    } else {
        imag_i_4_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_5_address0() {
    imag_i_5_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_5_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_5_ce0 = ap_const_logic_1;
    } else {
        imag_i_5_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_6_address0() {
    imag_i_6_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_6_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_6_ce0 = ap_const_logic_1;
    } else {
        imag_i_6_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_7_address0() {
    imag_i_7_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_7_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_7_ce0 = ap_const_logic_1;
    } else {
        imag_i_7_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_8_address0() {
    imag_i_8_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_8_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_8_ce0 = ap_const_logic_1;
    } else {
        imag_i_8_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_9_address0() {
    imag_i_9_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_imag_i_9_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        imag_i_9_ce0 = ap_const_logic_1;
    } else {
        imag_i_9_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_imag_i_load_0_phi_fu_8386_p3() {
    imag_i_load_0_phi_fu_8386_p3 = (!ap_reg_ppstg_sel_tmp4_reg_9786_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp4_reg_9786_pp0_it1.read()[0].to_bool())? imag_i_16_q0.read(): sel_tmp9_fu_8379_p3.read());
}

void dft_loop_flow256::thread_imag_i_load_1_phi_fu_8566_p3() {
    imag_i_load_1_phi_fu_8566_p3 = (!ap_reg_ppstg_sel_tmp16_reg_9820_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp16_reg_9820_pp0_it1.read()[0].to_bool())? imag_i_17_q0.read(): sel_tmp18_fu_8559_p3.read());
}

void dft_loop_flow256::thread_imag_i_load_2_phi_fu_8746_p3() {
    imag_i_load_2_phi_fu_8746_p3 = (!ap_reg_ppstg_sel_tmp23_reg_9849_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp23_reg_9849_pp0_it1.read()[0].to_bool())? imag_i_18_q0.read(): sel_tmp25_fu_8739_p3.read());
}

void dft_loop_flow256::thread_imag_i_load_3_phi_fu_8926_p3() {
    imag_i_load_3_phi_fu_8926_p3 = (!ap_reg_ppstg_sel_tmp30_reg_9878_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp30_reg_9878_pp0_it1.read()[0].to_bool())? imag_i_19_q0.read(): sel_tmp32_fu_8919_p3.read());
}

void dft_loop_flow256::thread_imag_i_load_4_phi_fu_9106_p3() {
    imag_i_load_4_phi_fu_9106_p3 = (!ap_reg_ppstg_sel_tmp37_reg_9907_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp37_reg_9907_pp0_it1.read()[0].to_bool())? imag_i_20_q0.read(): sel_tmp39_fu_9099_p3.read());
}

void dft_loop_flow256::thread_imag_i_load_5_phi_fu_9286_p3() {
    imag_i_load_5_phi_fu_9286_p3 = (!ap_reg_ppstg_sel_tmp44_reg_9941_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp44_reg_9941_pp0_it1.read()[0].to_bool())? imag_i_21_q0.read(): sel_tmp46_fu_9279_p3.read());
}

void dft_loop_flow256::thread_imag_i_load_6_phi_fu_9466_p3() {
    imag_i_load_6_phi_fu_9466_p3 = (!ap_reg_ppstg_sel_tmp51_reg_9975_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp51_reg_9975_pp0_it1.read()[0].to_bool())? imag_i_22_q0.read(): sel_tmp53_fu_9459_p3.read());
}

void dft_loop_flow256::thread_imag_i_load_7_phi_fu_9646_p3() {
    imag_i_load_7_phi_fu_9646_p3 = (!ap_reg_ppstg_sel_tmp58_reg_10009_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp58_reg_10009_pp0_it1.read()[0].to_bool())? imag_i_23_q0.read(): sel_tmp60_fu_9639_p3.read());
}

void dft_loop_flow256::thread_j_V_1_fu_7304_p2() {
    j_V_1_fu_7304_p2 = (p_s_reg_7034.read() | ap_const_lv9_1);
}

void dft_loop_flow256::thread_j_V_2_fu_7559_p2() {
    j_V_2_fu_7559_p2 = (p_s_reg_7034.read() | ap_const_lv9_6);
}

void dft_loop_flow256::thread_j_V_3_fu_7616_p2() {
    j_V_3_fu_7616_p2 = (p_s_reg_7034.read() | ap_const_lv9_7);
}

void dft_loop_flow256::thread_j_V_6_fu_7398_p2() {
    j_V_6_fu_7398_p2 = (p_s_reg_7034.read() | ap_const_lv9_3);
}

void dft_loop_flow256::thread_j_V_7_fu_7673_p2() {
    j_V_7_fu_7673_p2 = (!ap_const_lv9_8.is_01() || !p_s_reg_7034.read().is_01())? sc_lv<9>(): (sc_biguint<9>(ap_const_lv9_8) + sc_bigint<9>(p_s_reg_7034.read()));
}

void dft_loop_flow256::thread_j_V_8_fu_7445_p2() {
    j_V_8_fu_7445_p2 = (p_s_reg_7034.read() | ap_const_lv9_4);
}

void dft_loop_flow256::thread_j_V_9_fu_7502_p2() {
    j_V_9_fu_7502_p2 = (p_s_reg_7034.read() | ap_const_lv9_5);
}

void dft_loop_flow256::thread_j_V_s_fu_7351_p2() {
    j_V_s_fu_7351_p2 = (p_s_reg_7034.read() | ap_const_lv9_2);
}

void dft_loop_flow256::thread_newIndex10_fu_9733_p1() {
    newIndex10_fu_9733_p1 = esl_zext<64,7>(ap_reg_ppstg_newIndex8_reg_9924_pp0_it11.read());
}

void dft_loop_flow256::thread_newIndex11_fu_8081_p1() {
    newIndex11_fu_8081_p1 = esl_zext<64,3>(index0_V_5_cast_reg_9953.read());
}

void dft_loop_flow256::thread_newIndex13_fu_9738_p1() {
    newIndex13_fu_9738_p1 = esl_zext<64,7>(ap_reg_ppstg_newIndex12_reg_9958_pp0_it11.read());
}

void dft_loop_flow256::thread_newIndex14_fu_8148_p1() {
    newIndex14_fu_8148_p1 = esl_zext<64,3>(index0_V_6_cast_reg_9987.read());
}

void dft_loop_flow256::thread_newIndex16_fu_9743_p1() {
    newIndex16_fu_9743_p1 = esl_zext<64,7>(ap_reg_ppstg_newIndex15_reg_9992_pp0_it11.read());
}

void dft_loop_flow256::thread_newIndex17_fu_8215_p1() {
    newIndex17_fu_8215_p1 = esl_zext<64,3>(index0_V_7_cast_reg_10021.read());
}

void dft_loop_flow256::thread_newIndex19_fu_9748_p1() {
    newIndex19_fu_9748_p1 = esl_zext<64,7>(ap_reg_ppstg_newIndex18_reg_10026_pp0_it11.read());
}

void dft_loop_flow256::thread_newIndex1_fu_7746_p1() {
    newIndex1_fu_7746_p1 = esl_zext<64,3>(index0_V_cast_reg_9798.read());
}

void dft_loop_flow256::thread_newIndex3_fu_9722_p1() {
    newIndex3_fu_9722_p1 = esl_zext<64,7>(ap_reg_ppstg_newIndex2_reg_9803_pp0_it11.read());
}

void dft_loop_flow256::thread_newIndex4_fu_7813_p1() {
    newIndex4_fu_7813_p1 = esl_zext<64,3>(index0_V_1_cast_reg_9832.read());
}

void dft_loop_flow256::thread_newIndex5_fu_7880_p1() {
    newIndex5_fu_7880_p1 = esl_zext<64,3>(index0_V_2_cast_reg_9861.read());
}

void dft_loop_flow256::thread_newIndex6_fu_7947_p1() {
    newIndex6_fu_7947_p1 = esl_zext<64,3>(index0_V_3_cast_reg_9890.read());
}

void dft_loop_flow256::thread_newIndex7_fu_8014_p1() {
    newIndex7_fu_8014_p1 = esl_zext<64,3>(index0_V_4_cast_reg_9919.read());
}

void dft_loop_flow256::thread_newIndex9_fu_7679_p1() {
    newIndex9_fu_7679_p1 = esl_zext<64,4>(newIndex_reg_9769.read());
}

void dft_loop_flow256::thread_real_arr256_0_address0() {
    real_arr256_0_address0 =  (sc_lv<6>) (newIndex3_fu_9722_p1.read());
}

void dft_loop_flow256::thread_real_arr256_0_address1() {
    real_arr256_0_address1 =  (sc_lv<6>) (newIndex10_fu_9733_p1.read());
}

void dft_loop_flow256::thread_real_arr256_0_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read())) {
        real_arr256_0_ce0 = ap_const_logic_1;
    } else {
        real_arr256_0_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_arr256_0_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read())) {
        real_arr256_0_ce1 = ap_const_logic_1;
    } else {
        real_arr256_0_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_arr256_0_d0() {
    real_arr256_0_d0 = tmp_11_reg_13268.read();
}

void dft_loop_flow256::thread_real_arr256_0_d1() {
    real_arr256_0_d1 = tmp_13_4_reg_13308.read();
}

void dft_loop_flow256::thread_real_arr256_0_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read()) && 
          esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it11.read(), ap_const_lv1_0)))) {
        real_arr256_0_we0 = ap_const_logic_1;
    } else {
        real_arr256_0_we0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_arr256_0_we1() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read()) && 
          esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it11.read(), ap_const_lv1_0)))) {
        real_arr256_0_we1 = ap_const_logic_1;
    } else {
        real_arr256_0_we1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_arr256_1_address0() {
    real_arr256_1_address0 =  (sc_lv<6>) (newIndex3_fu_9722_p1.read());
}

void dft_loop_flow256::thread_real_arr256_1_address1() {
    real_arr256_1_address1 =  (sc_lv<6>) (newIndex13_fu_9738_p1.read());
}

void dft_loop_flow256::thread_real_arr256_1_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read())) {
        real_arr256_1_ce0 = ap_const_logic_1;
    } else {
        real_arr256_1_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_arr256_1_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read())) {
        real_arr256_1_ce1 = ap_const_logic_1;
    } else {
        real_arr256_1_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_arr256_1_d0() {
    real_arr256_1_d0 = tmp_13_1_reg_13278.read();
}

void dft_loop_flow256::thread_real_arr256_1_d1() {
    real_arr256_1_d1 = tmp_13_5_reg_13318.read();
}

void dft_loop_flow256::thread_real_arr256_1_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read()) && 
          esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it11.read(), ap_const_lv1_0)))) {
        real_arr256_1_we0 = ap_const_logic_1;
    } else {
        real_arr256_1_we0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_arr256_1_we1() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read()) && 
          esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it11.read(), ap_const_lv1_0)))) {
        real_arr256_1_we1 = ap_const_logic_1;
    } else {
        real_arr256_1_we1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_arr256_2_address0() {
    real_arr256_2_address0 =  (sc_lv<6>) (newIndex3_fu_9722_p1.read());
}

void dft_loop_flow256::thread_real_arr256_2_address1() {
    real_arr256_2_address1 =  (sc_lv<6>) (newIndex16_fu_9743_p1.read());
}

void dft_loop_flow256::thread_real_arr256_2_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read())) {
        real_arr256_2_ce0 = ap_const_logic_1;
    } else {
        real_arr256_2_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_arr256_2_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read())) {
        real_arr256_2_ce1 = ap_const_logic_1;
    } else {
        real_arr256_2_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_arr256_2_d0() {
    real_arr256_2_d0 = tmp_13_2_reg_13288.read();
}

void dft_loop_flow256::thread_real_arr256_2_d1() {
    real_arr256_2_d1 = tmp_13_6_reg_13328.read();
}

void dft_loop_flow256::thread_real_arr256_2_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read()) && 
          esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it11.read(), ap_const_lv1_0)))) {
        real_arr256_2_we0 = ap_const_logic_1;
    } else {
        real_arr256_2_we0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_arr256_2_we1() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read()) && 
          esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it11.read(), ap_const_lv1_0)))) {
        real_arr256_2_we1 = ap_const_logic_1;
    } else {
        real_arr256_2_we1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_arr256_3_address0() {
    real_arr256_3_address0 =  (sc_lv<6>) (newIndex3_fu_9722_p1.read());
}

void dft_loop_flow256::thread_real_arr256_3_address1() {
    real_arr256_3_address1 =  (sc_lv<6>) (newIndex19_fu_9748_p1.read());
}

void dft_loop_flow256::thread_real_arr256_3_ce0() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read())) {
        real_arr256_3_ce0 = ap_const_logic_1;
    } else {
        real_arr256_3_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_arr256_3_ce1() {
    if (esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read())) {
        real_arr256_3_ce1 = ap_const_logic_1;
    } else {
        real_arr256_3_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_arr256_3_d0() {
    real_arr256_3_d0 = tmp_13_3_reg_13298.read();
}

void dft_loop_flow256::thread_real_arr256_3_d1() {
    real_arr256_3_d1 = tmp_13_7_reg_13338.read();
}

void dft_loop_flow256::thread_real_arr256_3_we0() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read()) && 
          esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it11.read(), ap_const_lv1_0)))) {
        real_arr256_3_we0 = ap_const_logic_1;
    } else {
        real_arr256_3_we0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_arr256_3_we1() {
    if (((esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it12.read()) && 
          esl_seteq<1,1,1>(ap_reg_ppstg_exitcond_reg_9765_pp0_it11.read(), ap_const_lv1_0)))) {
        real_arr256_3_we1 = ap_const_logic_1;
    } else {
        real_arr256_3_we1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_0_address0() {
    real_i_0_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_0_ce0 = ap_const_logic_1;
    } else {
        real_i_0_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_10_address0() {
    real_i_10_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_10_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_10_ce0 = ap_const_logic_1;
    } else {
        real_i_10_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_11_address0() {
    real_i_11_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_11_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_11_ce0 = ap_const_logic_1;
    } else {
        real_i_11_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_12_address0() {
    real_i_12_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_12_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_12_ce0 = ap_const_logic_1;
    } else {
        real_i_12_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_13_address0() {
    real_i_13_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_13_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_13_ce0 = ap_const_logic_1;
    } else {
        real_i_13_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_14_address0() {
    real_i_14_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_14_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_14_ce0 = ap_const_logic_1;
    } else {
        real_i_14_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_15_address0() {
    real_i_15_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_15_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_15_ce0 = ap_const_logic_1;
    } else {
        real_i_15_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_16_address0() {
    real_i_16_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_16_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_16_ce0 = ap_const_logic_1;
    } else {
        real_i_16_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_17_address0() {
    real_i_17_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_17_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_17_ce0 = ap_const_logic_1;
    } else {
        real_i_17_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_18_address0() {
    real_i_18_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_18_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_18_ce0 = ap_const_logic_1;
    } else {
        real_i_18_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_19_address0() {
    real_i_19_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_19_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_19_ce0 = ap_const_logic_1;
    } else {
        real_i_19_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_1_address0() {
    real_i_1_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_1_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_1_ce0 = ap_const_logic_1;
    } else {
        real_i_1_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_20_address0() {
    real_i_20_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_20_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_20_ce0 = ap_const_logic_1;
    } else {
        real_i_20_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_21_address0() {
    real_i_21_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_21_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_21_ce0 = ap_const_logic_1;
    } else {
        real_i_21_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_22_address0() {
    real_i_22_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_22_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_22_ce0 = ap_const_logic_1;
    } else {
        real_i_22_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_23_address0() {
    real_i_23_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_23_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_23_ce0 = ap_const_logic_1;
    } else {
        real_i_23_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_24_address0() {
    real_i_24_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_24_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_24_ce0 = ap_const_logic_1;
    } else {
        real_i_24_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_25_address0() {
    real_i_25_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_25_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_25_ce0 = ap_const_logic_1;
    } else {
        real_i_25_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_26_address0() {
    real_i_26_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_26_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_26_ce0 = ap_const_logic_1;
    } else {
        real_i_26_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_27_address0() {
    real_i_27_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_27_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_27_ce0 = ap_const_logic_1;
    } else {
        real_i_27_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_28_address0() {
    real_i_28_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_28_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_28_ce0 = ap_const_logic_1;
    } else {
        real_i_28_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_29_address0() {
    real_i_29_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_29_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_29_ce0 = ap_const_logic_1;
    } else {
        real_i_29_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_2_address0() {
    real_i_2_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_2_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_2_ce0 = ap_const_logic_1;
    } else {
        real_i_2_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_30_address0() {
    real_i_30_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_30_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_30_ce0 = ap_const_logic_1;
    } else {
        real_i_30_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_31_address0() {
    real_i_31_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_31_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_31_ce0 = ap_const_logic_1;
    } else {
        real_i_31_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_3_address0() {
    real_i_3_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_3_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_3_ce0 = ap_const_logic_1;
    } else {
        real_i_3_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_4_address0() {
    real_i_4_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_4_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_4_ce0 = ap_const_logic_1;
    } else {
        real_i_4_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_5_address0() {
    real_i_5_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_5_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_5_ce0 = ap_const_logic_1;
    } else {
        real_i_5_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_6_address0() {
    real_i_6_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_6_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_6_ce0 = ap_const_logic_1;
    } else {
        real_i_6_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_7_address0() {
    real_i_7_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_7_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_7_ce0 = ap_const_logic_1;
    } else {
        real_i_7_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_8_address0() {
    real_i_8_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_8_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_8_ce0 = ap_const_logic_1;
    } else {
        real_i_8_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_9_address0() {
    real_i_9_address0 =  (sc_lv<3>) (newIndex9_fu_7679_p1.read());
}

void dft_loop_flow256::thread_real_i_9_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        real_i_9_ce0 = ap_const_logic_1;
    } else {
        real_i_9_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_real_i_load_0_phi_fu_8296_p3() {
    real_i_load_0_phi_fu_8296_p3 = (!ap_reg_ppstg_sel_tmp4_reg_9786_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp4_reg_9786_pp0_it1.read()[0].to_bool())? real_i_16_q0.read(): sel_tmp3_fu_8289_p3.read());
}

void dft_loop_flow256::thread_real_i_load_1_phi_fu_8476_p3() {
    real_i_load_1_phi_fu_8476_p3 = (!ap_reg_ppstg_sel_tmp16_reg_9820_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp16_reg_9820_pp0_it1.read()[0].to_bool())? real_i_17_q0.read(): sel_tmp15_fu_8469_p3.read());
}

void dft_loop_flow256::thread_real_i_load_2_phi_fu_8656_p3() {
    real_i_load_2_phi_fu_8656_p3 = (!ap_reg_ppstg_sel_tmp23_reg_9849_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp23_reg_9849_pp0_it1.read()[0].to_bool())? real_i_18_q0.read(): sel_tmp22_fu_8649_p3.read());
}

void dft_loop_flow256::thread_real_i_load_3_phi_fu_8836_p3() {
    real_i_load_3_phi_fu_8836_p3 = (!ap_reg_ppstg_sel_tmp30_reg_9878_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp30_reg_9878_pp0_it1.read()[0].to_bool())? real_i_19_q0.read(): sel_tmp29_fu_8829_p3.read());
}

void dft_loop_flow256::thread_real_i_load_4_phi_fu_9016_p3() {
    real_i_load_4_phi_fu_9016_p3 = (!ap_reg_ppstg_sel_tmp37_reg_9907_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp37_reg_9907_pp0_it1.read()[0].to_bool())? real_i_20_q0.read(): sel_tmp36_fu_9009_p3.read());
}

void dft_loop_flow256::thread_real_i_load_5_phi_fu_9196_p3() {
    real_i_load_5_phi_fu_9196_p3 = (!ap_reg_ppstg_sel_tmp44_reg_9941_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp44_reg_9941_pp0_it1.read()[0].to_bool())? real_i_21_q0.read(): sel_tmp43_fu_9189_p3.read());
}

void dft_loop_flow256::thread_real_i_load_6_phi_fu_9376_p3() {
    real_i_load_6_phi_fu_9376_p3 = (!ap_reg_ppstg_sel_tmp51_reg_9975_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp51_reg_9975_pp0_it1.read()[0].to_bool())? real_i_22_q0.read(): sel_tmp50_fu_9369_p3.read());
}

void dft_loop_flow256::thread_real_i_load_7_phi_fu_9556_p3() {
    real_i_load_7_phi_fu_9556_p3 = (!ap_reg_ppstg_sel_tmp58_reg_10009_pp0_it1.read()[0].is_01())? sc_lv<32>(): ((ap_reg_ppstg_sel_tmp58_reg_10009_pp0_it1.read()[0].to_bool())? real_i_23_q0.read(): sel_tmp57_fu_9549_p3.read());
}

void dft_loop_flow256::thread_sct_0_address0() {
    sct_0_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_sct_0_address1() {
    sct_0_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_sct_0_address2() {
    sct_0_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_sct_0_address3() {
    sct_0_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_sct_0_address4() {
    sct_0_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_sct_0_address5() {
    sct_0_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_sct_0_address6() {
    sct_0_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_sct_0_address7() {
    sct_0_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_sct_0_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_0_ce0 = ap_const_logic_1;
    } else {
        sct_0_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_0_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_0_ce1 = ap_const_logic_1;
    } else {
        sct_0_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_0_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_0_ce2 = ap_const_logic_1;
    } else {
        sct_0_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_0_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_0_ce3 = ap_const_logic_1;
    } else {
        sct_0_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_0_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_0_ce4 = ap_const_logic_1;
    } else {
        sct_0_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_0_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_0_ce5 = ap_const_logic_1;
    } else {
        sct_0_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_0_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_0_ce6 = ap_const_logic_1;
    } else {
        sct_0_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_0_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_0_ce7 = ap_const_logic_1;
    } else {
        sct_0_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_10_address0() {
    sct_10_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_sct_10_address1() {
    sct_10_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_sct_10_address2() {
    sct_10_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_sct_10_address3() {
    sct_10_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_sct_10_address4() {
    sct_10_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_sct_10_address5() {
    sct_10_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_sct_10_address6() {
    sct_10_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_sct_10_address7() {
    sct_10_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_sct_10_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_10_ce0 = ap_const_logic_1;
    } else {
        sct_10_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_10_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_10_ce1 = ap_const_logic_1;
    } else {
        sct_10_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_10_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_10_ce2 = ap_const_logic_1;
    } else {
        sct_10_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_10_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_10_ce3 = ap_const_logic_1;
    } else {
        sct_10_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_10_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_10_ce4 = ap_const_logic_1;
    } else {
        sct_10_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_10_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_10_ce5 = ap_const_logic_1;
    } else {
        sct_10_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_10_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_10_ce6 = ap_const_logic_1;
    } else {
        sct_10_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_10_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_10_ce7 = ap_const_logic_1;
    } else {
        sct_10_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_11_address0() {
    sct_11_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_sct_11_address1() {
    sct_11_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_sct_11_address2() {
    sct_11_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_sct_11_address3() {
    sct_11_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_sct_11_address4() {
    sct_11_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_sct_11_address5() {
    sct_11_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_sct_11_address6() {
    sct_11_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_sct_11_address7() {
    sct_11_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_sct_11_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_11_ce0 = ap_const_logic_1;
    } else {
        sct_11_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_11_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_11_ce1 = ap_const_logic_1;
    } else {
        sct_11_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_11_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_11_ce2 = ap_const_logic_1;
    } else {
        sct_11_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_11_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_11_ce3 = ap_const_logic_1;
    } else {
        sct_11_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_11_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_11_ce4 = ap_const_logic_1;
    } else {
        sct_11_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_11_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_11_ce5 = ap_const_logic_1;
    } else {
        sct_11_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_11_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_11_ce6 = ap_const_logic_1;
    } else {
        sct_11_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_11_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_11_ce7 = ap_const_logic_1;
    } else {
        sct_11_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_12_address0() {
    sct_12_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_sct_12_address1() {
    sct_12_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_sct_12_address2() {
    sct_12_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_sct_12_address3() {
    sct_12_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_sct_12_address4() {
    sct_12_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_sct_12_address5() {
    sct_12_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_sct_12_address6() {
    sct_12_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_sct_12_address7() {
    sct_12_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_sct_12_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_12_ce0 = ap_const_logic_1;
    } else {
        sct_12_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_12_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_12_ce1 = ap_const_logic_1;
    } else {
        sct_12_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_12_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_12_ce2 = ap_const_logic_1;
    } else {
        sct_12_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_12_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_12_ce3 = ap_const_logic_1;
    } else {
        sct_12_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_12_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_12_ce4 = ap_const_logic_1;
    } else {
        sct_12_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_12_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_12_ce5 = ap_const_logic_1;
    } else {
        sct_12_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_12_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_12_ce6 = ap_const_logic_1;
    } else {
        sct_12_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_12_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_12_ce7 = ap_const_logic_1;
    } else {
        sct_12_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_13_address0() {
    sct_13_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_sct_13_address1() {
    sct_13_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_sct_13_address2() {
    sct_13_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_sct_13_address3() {
    sct_13_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_sct_13_address4() {
    sct_13_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_sct_13_address5() {
    sct_13_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_sct_13_address6() {
    sct_13_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_sct_13_address7() {
    sct_13_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_sct_13_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_13_ce0 = ap_const_logic_1;
    } else {
        sct_13_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_13_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_13_ce1 = ap_const_logic_1;
    } else {
        sct_13_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_13_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_13_ce2 = ap_const_logic_1;
    } else {
        sct_13_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_13_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_13_ce3 = ap_const_logic_1;
    } else {
        sct_13_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_13_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_13_ce4 = ap_const_logic_1;
    } else {
        sct_13_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_13_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_13_ce5 = ap_const_logic_1;
    } else {
        sct_13_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_13_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_13_ce6 = ap_const_logic_1;
    } else {
        sct_13_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_13_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_13_ce7 = ap_const_logic_1;
    } else {
        sct_13_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_14_address0() {
    sct_14_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_sct_14_address1() {
    sct_14_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_sct_14_address2() {
    sct_14_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_sct_14_address3() {
    sct_14_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_sct_14_address4() {
    sct_14_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_sct_14_address5() {
    sct_14_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_sct_14_address6() {
    sct_14_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_sct_14_address7() {
    sct_14_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_sct_14_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_14_ce0 = ap_const_logic_1;
    } else {
        sct_14_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_14_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_14_ce1 = ap_const_logic_1;
    } else {
        sct_14_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_14_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_14_ce2 = ap_const_logic_1;
    } else {
        sct_14_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_14_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_14_ce3 = ap_const_logic_1;
    } else {
        sct_14_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_14_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_14_ce4 = ap_const_logic_1;
    } else {
        sct_14_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_14_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_14_ce5 = ap_const_logic_1;
    } else {
        sct_14_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_14_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_14_ce6 = ap_const_logic_1;
    } else {
        sct_14_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_14_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_14_ce7 = ap_const_logic_1;
    } else {
        sct_14_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_15_address0() {
    sct_15_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_sct_15_address1() {
    sct_15_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_sct_15_address2() {
    sct_15_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_sct_15_address3() {
    sct_15_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_sct_15_address4() {
    sct_15_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_sct_15_address5() {
    sct_15_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_sct_15_address6() {
    sct_15_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_sct_15_address7() {
    sct_15_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_sct_15_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_15_ce0 = ap_const_logic_1;
    } else {
        sct_15_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_15_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_15_ce1 = ap_const_logic_1;
    } else {
        sct_15_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_15_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_15_ce2 = ap_const_logic_1;
    } else {
        sct_15_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_15_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_15_ce3 = ap_const_logic_1;
    } else {
        sct_15_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_15_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_15_ce4 = ap_const_logic_1;
    } else {
        sct_15_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_15_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_15_ce5 = ap_const_logic_1;
    } else {
        sct_15_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_15_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_15_ce6 = ap_const_logic_1;
    } else {
        sct_15_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_15_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_15_ce7 = ap_const_logic_1;
    } else {
        sct_15_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_16_address0() {
    sct_16_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_sct_16_address1() {
    sct_16_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_sct_16_address2() {
    sct_16_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_sct_16_address3() {
    sct_16_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_sct_16_address4() {
    sct_16_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_sct_16_address5() {
    sct_16_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_sct_16_address6() {
    sct_16_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_sct_16_address7() {
    sct_16_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_sct_16_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_16_ce0 = ap_const_logic_1;
    } else {
        sct_16_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_16_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_16_ce1 = ap_const_logic_1;
    } else {
        sct_16_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_16_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_16_ce2 = ap_const_logic_1;
    } else {
        sct_16_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_16_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_16_ce3 = ap_const_logic_1;
    } else {
        sct_16_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_16_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_16_ce4 = ap_const_logic_1;
    } else {
        sct_16_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_16_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_16_ce5 = ap_const_logic_1;
    } else {
        sct_16_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_16_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_16_ce6 = ap_const_logic_1;
    } else {
        sct_16_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_16_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_16_ce7 = ap_const_logic_1;
    } else {
        sct_16_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_17_address0() {
    sct_17_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_sct_17_address1() {
    sct_17_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_sct_17_address2() {
    sct_17_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_sct_17_address3() {
    sct_17_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_sct_17_address4() {
    sct_17_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_sct_17_address5() {
    sct_17_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_sct_17_address6() {
    sct_17_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_sct_17_address7() {
    sct_17_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_sct_17_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_17_ce0 = ap_const_logic_1;
    } else {
        sct_17_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_17_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_17_ce1 = ap_const_logic_1;
    } else {
        sct_17_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_17_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_17_ce2 = ap_const_logic_1;
    } else {
        sct_17_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_17_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_17_ce3 = ap_const_logic_1;
    } else {
        sct_17_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_17_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_17_ce4 = ap_const_logic_1;
    } else {
        sct_17_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_17_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_17_ce5 = ap_const_logic_1;
    } else {
        sct_17_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_17_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_17_ce6 = ap_const_logic_1;
    } else {
        sct_17_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_17_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_17_ce7 = ap_const_logic_1;
    } else {
        sct_17_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_18_address0() {
    sct_18_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_sct_18_address1() {
    sct_18_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_sct_18_address2() {
    sct_18_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_sct_18_address3() {
    sct_18_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_sct_18_address4() {
    sct_18_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_sct_18_address5() {
    sct_18_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_sct_18_address6() {
    sct_18_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_sct_18_address7() {
    sct_18_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_sct_18_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_18_ce0 = ap_const_logic_1;
    } else {
        sct_18_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_18_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_18_ce1 = ap_const_logic_1;
    } else {
        sct_18_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_18_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_18_ce2 = ap_const_logic_1;
    } else {
        sct_18_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_18_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_18_ce3 = ap_const_logic_1;
    } else {
        sct_18_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_18_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_18_ce4 = ap_const_logic_1;
    } else {
        sct_18_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_18_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_18_ce5 = ap_const_logic_1;
    } else {
        sct_18_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_18_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_18_ce6 = ap_const_logic_1;
    } else {
        sct_18_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_18_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_18_ce7 = ap_const_logic_1;
    } else {
        sct_18_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_19_address0() {
    sct_19_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_sct_19_address1() {
    sct_19_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_sct_19_address2() {
    sct_19_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_sct_19_address3() {
    sct_19_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_sct_19_address4() {
    sct_19_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_sct_19_address5() {
    sct_19_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_sct_19_address6() {
    sct_19_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_sct_19_address7() {
    sct_19_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_sct_19_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_19_ce0 = ap_const_logic_1;
    } else {
        sct_19_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_19_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_19_ce1 = ap_const_logic_1;
    } else {
        sct_19_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_19_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_19_ce2 = ap_const_logic_1;
    } else {
        sct_19_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_19_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_19_ce3 = ap_const_logic_1;
    } else {
        sct_19_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_19_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_19_ce4 = ap_const_logic_1;
    } else {
        sct_19_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_19_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_19_ce5 = ap_const_logic_1;
    } else {
        sct_19_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_19_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_19_ce6 = ap_const_logic_1;
    } else {
        sct_19_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_19_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_19_ce7 = ap_const_logic_1;
    } else {
        sct_19_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_1_address0() {
    sct_1_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_sct_1_address1() {
    sct_1_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_sct_1_address2() {
    sct_1_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_sct_1_address3() {
    sct_1_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_sct_1_address4() {
    sct_1_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_sct_1_address5() {
    sct_1_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_sct_1_address6() {
    sct_1_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_sct_1_address7() {
    sct_1_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

void dft_loop_flow256::thread_sct_1_ce0() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_1_ce0 = ap_const_logic_1;
    } else {
        sct_1_ce0 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_1_ce1() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_1_ce1 = ap_const_logic_1;
    } else {
        sct_1_ce1 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_1_ce2() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_1_ce2 = ap_const_logic_1;
    } else {
        sct_1_ce2 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_1_ce3() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_1_ce3 = ap_const_logic_1;
    } else {
        sct_1_ce3 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_1_ce4() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_1_ce4 = ap_const_logic_1;
    } else {
        sct_1_ce4 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_1_ce5() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_1_ce5 = ap_const_logic_1;
    } else {
        sct_1_ce5 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_1_ce6() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_1_ce6 = ap_const_logic_1;
    } else {
        sct_1_ce6 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_1_ce7() {
    if ((esl_seteq<1,1,1>(ap_const_logic_1, ap_sig_cseq_ST_pp0_stg0_fsm_1.read()) && 
         esl_seteq<1,1,1>(ap_const_logic_1, ap_reg_ppiten_pp0_it1.read()))) {
        sct_1_ce7 = ap_const_logic_1;
    } else {
        sct_1_ce7 = ap_const_logic_0;
    }
}

void dft_loop_flow256::thread_sct_20_address0() {
    sct_20_address0 =  (sc_lv<3>) (newIndex1_fu_7746_p1.read());
}

void dft_loop_flow256::thread_sct_20_address1() {
    sct_20_address1 =  (sc_lv<3>) (newIndex4_fu_7813_p1.read());
}

void dft_loop_flow256::thread_sct_20_address2() {
    sct_20_address2 =  (sc_lv<3>) (newIndex5_fu_7880_p1.read());
}

void dft_loop_flow256::thread_sct_20_address3() {
    sct_20_address3 =  (sc_lv<3>) (newIndex6_fu_7947_p1.read());
}

void dft_loop_flow256::thread_sct_20_address4() {
    sct_20_address4 =  (sc_lv<3>) (newIndex7_fu_8014_p1.read());
}

void dft_loop_flow256::thread_sct_20_address5() {
    sct_20_address5 =  (sc_lv<3>) (newIndex11_fu_8081_p1.read());
}

void dft_loop_flow256::thread_sct_20_address6() {
    sct_20_address6 =  (sc_lv<3>) (newIndex14_fu_8148_p1.read());
}

void dft_loop_flow256::thread_sct_20_address7() {
    sct_20_address7 =  (sc_lv<3>) (newIndex17_fu_8215_p1.read());
}

}

