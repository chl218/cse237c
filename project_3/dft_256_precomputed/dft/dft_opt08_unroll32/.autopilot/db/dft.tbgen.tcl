set C_TypeInfoList {{ 
"dft" : [[], { "return": [[], "void"]} , [{"ExternC" : 0}], [ {"real_i": [[], {"array": ["0", [256]]}] }, {"imag_i": [[], {"array": ["0", [256]]}] }, {"real_o": [[], {"array": ["0", [256]]}] }, {"imag_o": [[], {"array": ["0", [256]]}] }],[],""], 
"0": [ "DTYPE", {"typedef": [[[], {"scalar": "float"}],""]}]
}}
set moduleName dft
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {dft}
set C_modelType { void 0 }
set C_modelArgList { 
	{ real_i_0 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ real_i_1 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ real_i_2 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ real_i_3 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ real_i_4 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ real_i_5 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ real_i_6 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ real_i_7 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ real_i_8 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ real_i_9 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ real_i_10 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ real_i_11 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ real_i_12 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ real_i_13 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ real_i_14 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ real_i_15 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ imag_i_0 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ imag_i_1 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ imag_i_2 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ imag_i_3 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ imag_i_4 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ imag_i_5 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ imag_i_6 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ imag_i_7 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ imag_i_8 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ imag_i_9 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ imag_i_10 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ imag_i_11 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ imag_i_12 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ imag_i_13 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ imag_i_14 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ imag_i_15 float 32 regular {array 16 { 1 3 } 1 1 }  }
	{ real_o_0 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ real_o_1 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ real_o_2 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ real_o_3 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ real_o_4 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ real_o_5 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ real_o_6 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ real_o_7 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ real_o_8 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ real_o_9 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ real_o_10 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ real_o_11 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ real_o_12 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ real_o_13 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ real_o_14 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ real_o_15 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ imag_o_0 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ imag_o_1 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ imag_o_2 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ imag_o_3 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ imag_o_4 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ imag_o_5 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ imag_o_6 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ imag_o_7 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ imag_o_8 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ imag_o_9 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ imag_o_10 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ imag_o_11 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ imag_o_12 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ imag_o_13 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ imag_o_14 float 32 regular {array 16 { 0 3 } 0 1 }  }
	{ imag_o_15 float 32 regular {array 16 { 0 3 } 0 1 }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "real_i_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 240,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 241,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 242,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 243,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_4", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 4,"up" : 244,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_5", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 5,"up" : 245,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_6", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 6,"up" : 246,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_7", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 7,"up" : 247,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_8", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 8,"up" : 248,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_9", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 9,"up" : 249,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_10", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 10,"up" : 250,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_11", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 11,"up" : 251,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_12", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 12,"up" : 252,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_13", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 13,"up" : 253,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_14", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 14,"up" : 254,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_15", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 15,"up" : 255,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 240,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 241,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 242,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 243,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_4", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 4,"up" : 244,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_5", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 5,"up" : 245,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_6", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 6,"up" : 246,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_7", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 7,"up" : 247,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_8", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 8,"up" : 248,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_9", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 9,"up" : 249,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_10", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 10,"up" : 250,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_11", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 11,"up" : 251,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_12", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 12,"up" : 252,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_13", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 13,"up" : 253,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_14", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 14,"up" : 254,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_15", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 15,"up" : 255,"step" : 16}]}]}]} , 
 	{ "Name" : "real_o_0", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 240,"step" : 16}]}]}]} , 
 	{ "Name" : "real_o_1", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 241,"step" : 16}]}]}]} , 
 	{ "Name" : "real_o_2", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 242,"step" : 16}]}]}]} , 
 	{ "Name" : "real_o_3", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 243,"step" : 16}]}]}]} , 
 	{ "Name" : "real_o_4", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 4,"up" : 244,"step" : 16}]}]}]} , 
 	{ "Name" : "real_o_5", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 5,"up" : 245,"step" : 16}]}]}]} , 
 	{ "Name" : "real_o_6", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 6,"up" : 246,"step" : 16}]}]}]} , 
 	{ "Name" : "real_o_7", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 7,"up" : 247,"step" : 16}]}]}]} , 
 	{ "Name" : "real_o_8", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 8,"up" : 248,"step" : 16}]}]}]} , 
 	{ "Name" : "real_o_9", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 9,"up" : 249,"step" : 16}]}]}]} , 
 	{ "Name" : "real_o_10", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 10,"up" : 250,"step" : 16}]}]}]} , 
 	{ "Name" : "real_o_11", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 11,"up" : 251,"step" : 16}]}]}]} , 
 	{ "Name" : "real_o_12", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 12,"up" : 252,"step" : 16}]}]}]} , 
 	{ "Name" : "real_o_13", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 13,"up" : 253,"step" : 16}]}]}]} , 
 	{ "Name" : "real_o_14", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 14,"up" : 254,"step" : 16}]}]}]} , 
 	{ "Name" : "real_o_15", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 15,"up" : 255,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_o_0", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 240,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_o_1", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 241,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_o_2", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 242,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_o_3", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 243,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_o_4", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 4,"up" : 244,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_o_5", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 5,"up" : 245,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_o_6", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 6,"up" : 246,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_o_7", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 7,"up" : 247,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_o_8", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 8,"up" : 248,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_o_9", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 9,"up" : 249,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_o_10", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 10,"up" : 250,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_o_11", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 11,"up" : 251,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_o_12", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 12,"up" : 252,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_o_13", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 13,"up" : 253,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_o_14", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 14,"up" : 254,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_o_15", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 15,"up" : 255,"step" : 16}]}]}]} ]}
# RTL Port declarations: 
set portNum 230
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ real_i_0_address0 sc_out sc_lv 4 signal 0 } 
	{ real_i_0_ce0 sc_out sc_logic 1 signal 0 } 
	{ real_i_0_q0 sc_in sc_lv 32 signal 0 } 
	{ real_i_1_address0 sc_out sc_lv 4 signal 1 } 
	{ real_i_1_ce0 sc_out sc_logic 1 signal 1 } 
	{ real_i_1_q0 sc_in sc_lv 32 signal 1 } 
	{ real_i_2_address0 sc_out sc_lv 4 signal 2 } 
	{ real_i_2_ce0 sc_out sc_logic 1 signal 2 } 
	{ real_i_2_q0 sc_in sc_lv 32 signal 2 } 
	{ real_i_3_address0 sc_out sc_lv 4 signal 3 } 
	{ real_i_3_ce0 sc_out sc_logic 1 signal 3 } 
	{ real_i_3_q0 sc_in sc_lv 32 signal 3 } 
	{ real_i_4_address0 sc_out sc_lv 4 signal 4 } 
	{ real_i_4_ce0 sc_out sc_logic 1 signal 4 } 
	{ real_i_4_q0 sc_in sc_lv 32 signal 4 } 
	{ real_i_5_address0 sc_out sc_lv 4 signal 5 } 
	{ real_i_5_ce0 sc_out sc_logic 1 signal 5 } 
	{ real_i_5_q0 sc_in sc_lv 32 signal 5 } 
	{ real_i_6_address0 sc_out sc_lv 4 signal 6 } 
	{ real_i_6_ce0 sc_out sc_logic 1 signal 6 } 
	{ real_i_6_q0 sc_in sc_lv 32 signal 6 } 
	{ real_i_7_address0 sc_out sc_lv 4 signal 7 } 
	{ real_i_7_ce0 sc_out sc_logic 1 signal 7 } 
	{ real_i_7_q0 sc_in sc_lv 32 signal 7 } 
	{ real_i_8_address0 sc_out sc_lv 4 signal 8 } 
	{ real_i_8_ce0 sc_out sc_logic 1 signal 8 } 
	{ real_i_8_q0 sc_in sc_lv 32 signal 8 } 
	{ real_i_9_address0 sc_out sc_lv 4 signal 9 } 
	{ real_i_9_ce0 sc_out sc_logic 1 signal 9 } 
	{ real_i_9_q0 sc_in sc_lv 32 signal 9 } 
	{ real_i_10_address0 sc_out sc_lv 4 signal 10 } 
	{ real_i_10_ce0 sc_out sc_logic 1 signal 10 } 
	{ real_i_10_q0 sc_in sc_lv 32 signal 10 } 
	{ real_i_11_address0 sc_out sc_lv 4 signal 11 } 
	{ real_i_11_ce0 sc_out sc_logic 1 signal 11 } 
	{ real_i_11_q0 sc_in sc_lv 32 signal 11 } 
	{ real_i_12_address0 sc_out sc_lv 4 signal 12 } 
	{ real_i_12_ce0 sc_out sc_logic 1 signal 12 } 
	{ real_i_12_q0 sc_in sc_lv 32 signal 12 } 
	{ real_i_13_address0 sc_out sc_lv 4 signal 13 } 
	{ real_i_13_ce0 sc_out sc_logic 1 signal 13 } 
	{ real_i_13_q0 sc_in sc_lv 32 signal 13 } 
	{ real_i_14_address0 sc_out sc_lv 4 signal 14 } 
	{ real_i_14_ce0 sc_out sc_logic 1 signal 14 } 
	{ real_i_14_q0 sc_in sc_lv 32 signal 14 } 
	{ real_i_15_address0 sc_out sc_lv 4 signal 15 } 
	{ real_i_15_ce0 sc_out sc_logic 1 signal 15 } 
	{ real_i_15_q0 sc_in sc_lv 32 signal 15 } 
	{ imag_i_0_address0 sc_out sc_lv 4 signal 16 } 
	{ imag_i_0_ce0 sc_out sc_logic 1 signal 16 } 
	{ imag_i_0_q0 sc_in sc_lv 32 signal 16 } 
	{ imag_i_1_address0 sc_out sc_lv 4 signal 17 } 
	{ imag_i_1_ce0 sc_out sc_logic 1 signal 17 } 
	{ imag_i_1_q0 sc_in sc_lv 32 signal 17 } 
	{ imag_i_2_address0 sc_out sc_lv 4 signal 18 } 
	{ imag_i_2_ce0 sc_out sc_logic 1 signal 18 } 
	{ imag_i_2_q0 sc_in sc_lv 32 signal 18 } 
	{ imag_i_3_address0 sc_out sc_lv 4 signal 19 } 
	{ imag_i_3_ce0 sc_out sc_logic 1 signal 19 } 
	{ imag_i_3_q0 sc_in sc_lv 32 signal 19 } 
	{ imag_i_4_address0 sc_out sc_lv 4 signal 20 } 
	{ imag_i_4_ce0 sc_out sc_logic 1 signal 20 } 
	{ imag_i_4_q0 sc_in sc_lv 32 signal 20 } 
	{ imag_i_5_address0 sc_out sc_lv 4 signal 21 } 
	{ imag_i_5_ce0 sc_out sc_logic 1 signal 21 } 
	{ imag_i_5_q0 sc_in sc_lv 32 signal 21 } 
	{ imag_i_6_address0 sc_out sc_lv 4 signal 22 } 
	{ imag_i_6_ce0 sc_out sc_logic 1 signal 22 } 
	{ imag_i_6_q0 sc_in sc_lv 32 signal 22 } 
	{ imag_i_7_address0 sc_out sc_lv 4 signal 23 } 
	{ imag_i_7_ce0 sc_out sc_logic 1 signal 23 } 
	{ imag_i_7_q0 sc_in sc_lv 32 signal 23 } 
	{ imag_i_8_address0 sc_out sc_lv 4 signal 24 } 
	{ imag_i_8_ce0 sc_out sc_logic 1 signal 24 } 
	{ imag_i_8_q0 sc_in sc_lv 32 signal 24 } 
	{ imag_i_9_address0 sc_out sc_lv 4 signal 25 } 
	{ imag_i_9_ce0 sc_out sc_logic 1 signal 25 } 
	{ imag_i_9_q0 sc_in sc_lv 32 signal 25 } 
	{ imag_i_10_address0 sc_out sc_lv 4 signal 26 } 
	{ imag_i_10_ce0 sc_out sc_logic 1 signal 26 } 
	{ imag_i_10_q0 sc_in sc_lv 32 signal 26 } 
	{ imag_i_11_address0 sc_out sc_lv 4 signal 27 } 
	{ imag_i_11_ce0 sc_out sc_logic 1 signal 27 } 
	{ imag_i_11_q0 sc_in sc_lv 32 signal 27 } 
	{ imag_i_12_address0 sc_out sc_lv 4 signal 28 } 
	{ imag_i_12_ce0 sc_out sc_logic 1 signal 28 } 
	{ imag_i_12_q0 sc_in sc_lv 32 signal 28 } 
	{ imag_i_13_address0 sc_out sc_lv 4 signal 29 } 
	{ imag_i_13_ce0 sc_out sc_logic 1 signal 29 } 
	{ imag_i_13_q0 sc_in sc_lv 32 signal 29 } 
	{ imag_i_14_address0 sc_out sc_lv 4 signal 30 } 
	{ imag_i_14_ce0 sc_out sc_logic 1 signal 30 } 
	{ imag_i_14_q0 sc_in sc_lv 32 signal 30 } 
	{ imag_i_15_address0 sc_out sc_lv 4 signal 31 } 
	{ imag_i_15_ce0 sc_out sc_logic 1 signal 31 } 
	{ imag_i_15_q0 sc_in sc_lv 32 signal 31 } 
	{ real_o_0_address0 sc_out sc_lv 4 signal 32 } 
	{ real_o_0_ce0 sc_out sc_logic 1 signal 32 } 
	{ real_o_0_we0 sc_out sc_logic 1 signal 32 } 
	{ real_o_0_d0 sc_out sc_lv 32 signal 32 } 
	{ real_o_1_address0 sc_out sc_lv 4 signal 33 } 
	{ real_o_1_ce0 sc_out sc_logic 1 signal 33 } 
	{ real_o_1_we0 sc_out sc_logic 1 signal 33 } 
	{ real_o_1_d0 sc_out sc_lv 32 signal 33 } 
	{ real_o_2_address0 sc_out sc_lv 4 signal 34 } 
	{ real_o_2_ce0 sc_out sc_logic 1 signal 34 } 
	{ real_o_2_we0 sc_out sc_logic 1 signal 34 } 
	{ real_o_2_d0 sc_out sc_lv 32 signal 34 } 
	{ real_o_3_address0 sc_out sc_lv 4 signal 35 } 
	{ real_o_3_ce0 sc_out sc_logic 1 signal 35 } 
	{ real_o_3_we0 sc_out sc_logic 1 signal 35 } 
	{ real_o_3_d0 sc_out sc_lv 32 signal 35 } 
	{ real_o_4_address0 sc_out sc_lv 4 signal 36 } 
	{ real_o_4_ce0 sc_out sc_logic 1 signal 36 } 
	{ real_o_4_we0 sc_out sc_logic 1 signal 36 } 
	{ real_o_4_d0 sc_out sc_lv 32 signal 36 } 
	{ real_o_5_address0 sc_out sc_lv 4 signal 37 } 
	{ real_o_5_ce0 sc_out sc_logic 1 signal 37 } 
	{ real_o_5_we0 sc_out sc_logic 1 signal 37 } 
	{ real_o_5_d0 sc_out sc_lv 32 signal 37 } 
	{ real_o_6_address0 sc_out sc_lv 4 signal 38 } 
	{ real_o_6_ce0 sc_out sc_logic 1 signal 38 } 
	{ real_o_6_we0 sc_out sc_logic 1 signal 38 } 
	{ real_o_6_d0 sc_out sc_lv 32 signal 38 } 
	{ real_o_7_address0 sc_out sc_lv 4 signal 39 } 
	{ real_o_7_ce0 sc_out sc_logic 1 signal 39 } 
	{ real_o_7_we0 sc_out sc_logic 1 signal 39 } 
	{ real_o_7_d0 sc_out sc_lv 32 signal 39 } 
	{ real_o_8_address0 sc_out sc_lv 4 signal 40 } 
	{ real_o_8_ce0 sc_out sc_logic 1 signal 40 } 
	{ real_o_8_we0 sc_out sc_logic 1 signal 40 } 
	{ real_o_8_d0 sc_out sc_lv 32 signal 40 } 
	{ real_o_9_address0 sc_out sc_lv 4 signal 41 } 
	{ real_o_9_ce0 sc_out sc_logic 1 signal 41 } 
	{ real_o_9_we0 sc_out sc_logic 1 signal 41 } 
	{ real_o_9_d0 sc_out sc_lv 32 signal 41 } 
	{ real_o_10_address0 sc_out sc_lv 4 signal 42 } 
	{ real_o_10_ce0 sc_out sc_logic 1 signal 42 } 
	{ real_o_10_we0 sc_out sc_logic 1 signal 42 } 
	{ real_o_10_d0 sc_out sc_lv 32 signal 42 } 
	{ real_o_11_address0 sc_out sc_lv 4 signal 43 } 
	{ real_o_11_ce0 sc_out sc_logic 1 signal 43 } 
	{ real_o_11_we0 sc_out sc_logic 1 signal 43 } 
	{ real_o_11_d0 sc_out sc_lv 32 signal 43 } 
	{ real_o_12_address0 sc_out sc_lv 4 signal 44 } 
	{ real_o_12_ce0 sc_out sc_logic 1 signal 44 } 
	{ real_o_12_we0 sc_out sc_logic 1 signal 44 } 
	{ real_o_12_d0 sc_out sc_lv 32 signal 44 } 
	{ real_o_13_address0 sc_out sc_lv 4 signal 45 } 
	{ real_o_13_ce0 sc_out sc_logic 1 signal 45 } 
	{ real_o_13_we0 sc_out sc_logic 1 signal 45 } 
	{ real_o_13_d0 sc_out sc_lv 32 signal 45 } 
	{ real_o_14_address0 sc_out sc_lv 4 signal 46 } 
	{ real_o_14_ce0 sc_out sc_logic 1 signal 46 } 
	{ real_o_14_we0 sc_out sc_logic 1 signal 46 } 
	{ real_o_14_d0 sc_out sc_lv 32 signal 46 } 
	{ real_o_15_address0 sc_out sc_lv 4 signal 47 } 
	{ real_o_15_ce0 sc_out sc_logic 1 signal 47 } 
	{ real_o_15_we0 sc_out sc_logic 1 signal 47 } 
	{ real_o_15_d0 sc_out sc_lv 32 signal 47 } 
	{ imag_o_0_address0 sc_out sc_lv 4 signal 48 } 
	{ imag_o_0_ce0 sc_out sc_logic 1 signal 48 } 
	{ imag_o_0_we0 sc_out sc_logic 1 signal 48 } 
	{ imag_o_0_d0 sc_out sc_lv 32 signal 48 } 
	{ imag_o_1_address0 sc_out sc_lv 4 signal 49 } 
	{ imag_o_1_ce0 sc_out sc_logic 1 signal 49 } 
	{ imag_o_1_we0 sc_out sc_logic 1 signal 49 } 
	{ imag_o_1_d0 sc_out sc_lv 32 signal 49 } 
	{ imag_o_2_address0 sc_out sc_lv 4 signal 50 } 
	{ imag_o_2_ce0 sc_out sc_logic 1 signal 50 } 
	{ imag_o_2_we0 sc_out sc_logic 1 signal 50 } 
	{ imag_o_2_d0 sc_out sc_lv 32 signal 50 } 
	{ imag_o_3_address0 sc_out sc_lv 4 signal 51 } 
	{ imag_o_3_ce0 sc_out sc_logic 1 signal 51 } 
	{ imag_o_3_we0 sc_out sc_logic 1 signal 51 } 
	{ imag_o_3_d0 sc_out sc_lv 32 signal 51 } 
	{ imag_o_4_address0 sc_out sc_lv 4 signal 52 } 
	{ imag_o_4_ce0 sc_out sc_logic 1 signal 52 } 
	{ imag_o_4_we0 sc_out sc_logic 1 signal 52 } 
	{ imag_o_4_d0 sc_out sc_lv 32 signal 52 } 
	{ imag_o_5_address0 sc_out sc_lv 4 signal 53 } 
	{ imag_o_5_ce0 sc_out sc_logic 1 signal 53 } 
	{ imag_o_5_we0 sc_out sc_logic 1 signal 53 } 
	{ imag_o_5_d0 sc_out sc_lv 32 signal 53 } 
	{ imag_o_6_address0 sc_out sc_lv 4 signal 54 } 
	{ imag_o_6_ce0 sc_out sc_logic 1 signal 54 } 
	{ imag_o_6_we0 sc_out sc_logic 1 signal 54 } 
	{ imag_o_6_d0 sc_out sc_lv 32 signal 54 } 
	{ imag_o_7_address0 sc_out sc_lv 4 signal 55 } 
	{ imag_o_7_ce0 sc_out sc_logic 1 signal 55 } 
	{ imag_o_7_we0 sc_out sc_logic 1 signal 55 } 
	{ imag_o_7_d0 sc_out sc_lv 32 signal 55 } 
	{ imag_o_8_address0 sc_out sc_lv 4 signal 56 } 
	{ imag_o_8_ce0 sc_out sc_logic 1 signal 56 } 
	{ imag_o_8_we0 sc_out sc_logic 1 signal 56 } 
	{ imag_o_8_d0 sc_out sc_lv 32 signal 56 } 
	{ imag_o_9_address0 sc_out sc_lv 4 signal 57 } 
	{ imag_o_9_ce0 sc_out sc_logic 1 signal 57 } 
	{ imag_o_9_we0 sc_out sc_logic 1 signal 57 } 
	{ imag_o_9_d0 sc_out sc_lv 32 signal 57 } 
	{ imag_o_10_address0 sc_out sc_lv 4 signal 58 } 
	{ imag_o_10_ce0 sc_out sc_logic 1 signal 58 } 
	{ imag_o_10_we0 sc_out sc_logic 1 signal 58 } 
	{ imag_o_10_d0 sc_out sc_lv 32 signal 58 } 
	{ imag_o_11_address0 sc_out sc_lv 4 signal 59 } 
	{ imag_o_11_ce0 sc_out sc_logic 1 signal 59 } 
	{ imag_o_11_we0 sc_out sc_logic 1 signal 59 } 
	{ imag_o_11_d0 sc_out sc_lv 32 signal 59 } 
	{ imag_o_12_address0 sc_out sc_lv 4 signal 60 } 
	{ imag_o_12_ce0 sc_out sc_logic 1 signal 60 } 
	{ imag_o_12_we0 sc_out sc_logic 1 signal 60 } 
	{ imag_o_12_d0 sc_out sc_lv 32 signal 60 } 
	{ imag_o_13_address0 sc_out sc_lv 4 signal 61 } 
	{ imag_o_13_ce0 sc_out sc_logic 1 signal 61 } 
	{ imag_o_13_we0 sc_out sc_logic 1 signal 61 } 
	{ imag_o_13_d0 sc_out sc_lv 32 signal 61 } 
	{ imag_o_14_address0 sc_out sc_lv 4 signal 62 } 
	{ imag_o_14_ce0 sc_out sc_logic 1 signal 62 } 
	{ imag_o_14_we0 sc_out sc_logic 1 signal 62 } 
	{ imag_o_14_d0 sc_out sc_lv 32 signal 62 } 
	{ imag_o_15_address0 sc_out sc_lv 4 signal 63 } 
	{ imag_o_15_ce0 sc_out sc_logic 1 signal 63 } 
	{ imag_o_15_we0 sc_out sc_logic 1 signal 63 } 
	{ imag_o_15_d0 sc_out sc_lv 32 signal 63 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "real_i_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_i_0", "role": "address0" }} , 
 	{ "name": "real_i_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "ce0" }} , 
 	{ "name": "real_i_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "q0" }} , 
 	{ "name": "real_i_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_i_1", "role": "address0" }} , 
 	{ "name": "real_i_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "ce0" }} , 
 	{ "name": "real_i_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "q0" }} , 
 	{ "name": "real_i_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_i_2", "role": "address0" }} , 
 	{ "name": "real_i_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_2", "role": "ce0" }} , 
 	{ "name": "real_i_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_2", "role": "q0" }} , 
 	{ "name": "real_i_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_i_3", "role": "address0" }} , 
 	{ "name": "real_i_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_3", "role": "ce0" }} , 
 	{ "name": "real_i_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_3", "role": "q0" }} , 
 	{ "name": "real_i_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_i_4", "role": "address0" }} , 
 	{ "name": "real_i_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_4", "role": "ce0" }} , 
 	{ "name": "real_i_4_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_4", "role": "q0" }} , 
 	{ "name": "real_i_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_i_5", "role": "address0" }} , 
 	{ "name": "real_i_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_5", "role": "ce0" }} , 
 	{ "name": "real_i_5_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_5", "role": "q0" }} , 
 	{ "name": "real_i_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_i_6", "role": "address0" }} , 
 	{ "name": "real_i_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_6", "role": "ce0" }} , 
 	{ "name": "real_i_6_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_6", "role": "q0" }} , 
 	{ "name": "real_i_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_i_7", "role": "address0" }} , 
 	{ "name": "real_i_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_7", "role": "ce0" }} , 
 	{ "name": "real_i_7_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_7", "role": "q0" }} , 
 	{ "name": "real_i_8_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_i_8", "role": "address0" }} , 
 	{ "name": "real_i_8_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_8", "role": "ce0" }} , 
 	{ "name": "real_i_8_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_8", "role": "q0" }} , 
 	{ "name": "real_i_9_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_i_9", "role": "address0" }} , 
 	{ "name": "real_i_9_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_9", "role": "ce0" }} , 
 	{ "name": "real_i_9_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_9", "role": "q0" }} , 
 	{ "name": "real_i_10_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_i_10", "role": "address0" }} , 
 	{ "name": "real_i_10_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_10", "role": "ce0" }} , 
 	{ "name": "real_i_10_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_10", "role": "q0" }} , 
 	{ "name": "real_i_11_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_i_11", "role": "address0" }} , 
 	{ "name": "real_i_11_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_11", "role": "ce0" }} , 
 	{ "name": "real_i_11_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_11", "role": "q0" }} , 
 	{ "name": "real_i_12_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_i_12", "role": "address0" }} , 
 	{ "name": "real_i_12_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_12", "role": "ce0" }} , 
 	{ "name": "real_i_12_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_12", "role": "q0" }} , 
 	{ "name": "real_i_13_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_i_13", "role": "address0" }} , 
 	{ "name": "real_i_13_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_13", "role": "ce0" }} , 
 	{ "name": "real_i_13_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_13", "role": "q0" }} , 
 	{ "name": "real_i_14_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_i_14", "role": "address0" }} , 
 	{ "name": "real_i_14_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_14", "role": "ce0" }} , 
 	{ "name": "real_i_14_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_14", "role": "q0" }} , 
 	{ "name": "real_i_15_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_i_15", "role": "address0" }} , 
 	{ "name": "real_i_15_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_15", "role": "ce0" }} , 
 	{ "name": "real_i_15_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_15", "role": "q0" }} , 
 	{ "name": "imag_i_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_i_0", "role": "address0" }} , 
 	{ "name": "imag_i_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_0", "role": "ce0" }} , 
 	{ "name": "imag_i_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_0", "role": "q0" }} , 
 	{ "name": "imag_i_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_i_1", "role": "address0" }} , 
 	{ "name": "imag_i_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_1", "role": "ce0" }} , 
 	{ "name": "imag_i_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_1", "role": "q0" }} , 
 	{ "name": "imag_i_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_i_2", "role": "address0" }} , 
 	{ "name": "imag_i_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_2", "role": "ce0" }} , 
 	{ "name": "imag_i_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_2", "role": "q0" }} , 
 	{ "name": "imag_i_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_i_3", "role": "address0" }} , 
 	{ "name": "imag_i_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_3", "role": "ce0" }} , 
 	{ "name": "imag_i_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_3", "role": "q0" }} , 
 	{ "name": "imag_i_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_i_4", "role": "address0" }} , 
 	{ "name": "imag_i_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_4", "role": "ce0" }} , 
 	{ "name": "imag_i_4_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_4", "role": "q0" }} , 
 	{ "name": "imag_i_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_i_5", "role": "address0" }} , 
 	{ "name": "imag_i_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_5", "role": "ce0" }} , 
 	{ "name": "imag_i_5_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_5", "role": "q0" }} , 
 	{ "name": "imag_i_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_i_6", "role": "address0" }} , 
 	{ "name": "imag_i_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_6", "role": "ce0" }} , 
 	{ "name": "imag_i_6_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_6", "role": "q0" }} , 
 	{ "name": "imag_i_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_i_7", "role": "address0" }} , 
 	{ "name": "imag_i_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_7", "role": "ce0" }} , 
 	{ "name": "imag_i_7_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_7", "role": "q0" }} , 
 	{ "name": "imag_i_8_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_i_8", "role": "address0" }} , 
 	{ "name": "imag_i_8_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_8", "role": "ce0" }} , 
 	{ "name": "imag_i_8_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_8", "role": "q0" }} , 
 	{ "name": "imag_i_9_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_i_9", "role": "address0" }} , 
 	{ "name": "imag_i_9_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_9", "role": "ce0" }} , 
 	{ "name": "imag_i_9_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_9", "role": "q0" }} , 
 	{ "name": "imag_i_10_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_i_10", "role": "address0" }} , 
 	{ "name": "imag_i_10_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_10", "role": "ce0" }} , 
 	{ "name": "imag_i_10_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_10", "role": "q0" }} , 
 	{ "name": "imag_i_11_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_i_11", "role": "address0" }} , 
 	{ "name": "imag_i_11_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_11", "role": "ce0" }} , 
 	{ "name": "imag_i_11_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_11", "role": "q0" }} , 
 	{ "name": "imag_i_12_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_i_12", "role": "address0" }} , 
 	{ "name": "imag_i_12_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_12", "role": "ce0" }} , 
 	{ "name": "imag_i_12_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_12", "role": "q0" }} , 
 	{ "name": "imag_i_13_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_i_13", "role": "address0" }} , 
 	{ "name": "imag_i_13_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_13", "role": "ce0" }} , 
 	{ "name": "imag_i_13_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_13", "role": "q0" }} , 
 	{ "name": "imag_i_14_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_i_14", "role": "address0" }} , 
 	{ "name": "imag_i_14_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_14", "role": "ce0" }} , 
 	{ "name": "imag_i_14_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_14", "role": "q0" }} , 
 	{ "name": "imag_i_15_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_i_15", "role": "address0" }} , 
 	{ "name": "imag_i_15_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_15", "role": "ce0" }} , 
 	{ "name": "imag_i_15_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_15", "role": "q0" }} , 
 	{ "name": "real_o_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_o_0", "role": "address0" }} , 
 	{ "name": "real_o_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_0", "role": "ce0" }} , 
 	{ "name": "real_o_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_0", "role": "we0" }} , 
 	{ "name": "real_o_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_0", "role": "d0" }} , 
 	{ "name": "real_o_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_o_1", "role": "address0" }} , 
 	{ "name": "real_o_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_1", "role": "ce0" }} , 
 	{ "name": "real_o_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_1", "role": "we0" }} , 
 	{ "name": "real_o_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_1", "role": "d0" }} , 
 	{ "name": "real_o_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_o_2", "role": "address0" }} , 
 	{ "name": "real_o_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_2", "role": "ce0" }} , 
 	{ "name": "real_o_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_2", "role": "we0" }} , 
 	{ "name": "real_o_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_2", "role": "d0" }} , 
 	{ "name": "real_o_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_o_3", "role": "address0" }} , 
 	{ "name": "real_o_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_3", "role": "ce0" }} , 
 	{ "name": "real_o_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_3", "role": "we0" }} , 
 	{ "name": "real_o_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_3", "role": "d0" }} , 
 	{ "name": "real_o_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_o_4", "role": "address0" }} , 
 	{ "name": "real_o_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_4", "role": "ce0" }} , 
 	{ "name": "real_o_4_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_4", "role": "we0" }} , 
 	{ "name": "real_o_4_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_4", "role": "d0" }} , 
 	{ "name": "real_o_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_o_5", "role": "address0" }} , 
 	{ "name": "real_o_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_5", "role": "ce0" }} , 
 	{ "name": "real_o_5_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_5", "role": "we0" }} , 
 	{ "name": "real_o_5_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_5", "role": "d0" }} , 
 	{ "name": "real_o_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_o_6", "role": "address0" }} , 
 	{ "name": "real_o_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_6", "role": "ce0" }} , 
 	{ "name": "real_o_6_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_6", "role": "we0" }} , 
 	{ "name": "real_o_6_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_6", "role": "d0" }} , 
 	{ "name": "real_o_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_o_7", "role": "address0" }} , 
 	{ "name": "real_o_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_7", "role": "ce0" }} , 
 	{ "name": "real_o_7_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_7", "role": "we0" }} , 
 	{ "name": "real_o_7_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_7", "role": "d0" }} , 
 	{ "name": "real_o_8_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_o_8", "role": "address0" }} , 
 	{ "name": "real_o_8_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_8", "role": "ce0" }} , 
 	{ "name": "real_o_8_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_8", "role": "we0" }} , 
 	{ "name": "real_o_8_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_8", "role": "d0" }} , 
 	{ "name": "real_o_9_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_o_9", "role": "address0" }} , 
 	{ "name": "real_o_9_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_9", "role": "ce0" }} , 
 	{ "name": "real_o_9_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_9", "role": "we0" }} , 
 	{ "name": "real_o_9_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_9", "role": "d0" }} , 
 	{ "name": "real_o_10_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_o_10", "role": "address0" }} , 
 	{ "name": "real_o_10_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_10", "role": "ce0" }} , 
 	{ "name": "real_o_10_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_10", "role": "we0" }} , 
 	{ "name": "real_o_10_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_10", "role": "d0" }} , 
 	{ "name": "real_o_11_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_o_11", "role": "address0" }} , 
 	{ "name": "real_o_11_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_11", "role": "ce0" }} , 
 	{ "name": "real_o_11_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_11", "role": "we0" }} , 
 	{ "name": "real_o_11_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_11", "role": "d0" }} , 
 	{ "name": "real_o_12_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_o_12", "role": "address0" }} , 
 	{ "name": "real_o_12_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_12", "role": "ce0" }} , 
 	{ "name": "real_o_12_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_12", "role": "we0" }} , 
 	{ "name": "real_o_12_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_12", "role": "d0" }} , 
 	{ "name": "real_o_13_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_o_13", "role": "address0" }} , 
 	{ "name": "real_o_13_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_13", "role": "ce0" }} , 
 	{ "name": "real_o_13_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_13", "role": "we0" }} , 
 	{ "name": "real_o_13_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_13", "role": "d0" }} , 
 	{ "name": "real_o_14_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_o_14", "role": "address0" }} , 
 	{ "name": "real_o_14_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_14", "role": "ce0" }} , 
 	{ "name": "real_o_14_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_14", "role": "we0" }} , 
 	{ "name": "real_o_14_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_14", "role": "d0" }} , 
 	{ "name": "real_o_15_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_o_15", "role": "address0" }} , 
 	{ "name": "real_o_15_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_15", "role": "ce0" }} , 
 	{ "name": "real_o_15_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_15", "role": "we0" }} , 
 	{ "name": "real_o_15_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_15", "role": "d0" }} , 
 	{ "name": "imag_o_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_o_0", "role": "address0" }} , 
 	{ "name": "imag_o_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_0", "role": "ce0" }} , 
 	{ "name": "imag_o_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_0", "role": "we0" }} , 
 	{ "name": "imag_o_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_0", "role": "d0" }} , 
 	{ "name": "imag_o_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_o_1", "role": "address0" }} , 
 	{ "name": "imag_o_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_1", "role": "ce0" }} , 
 	{ "name": "imag_o_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_1", "role": "we0" }} , 
 	{ "name": "imag_o_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_1", "role": "d0" }} , 
 	{ "name": "imag_o_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_o_2", "role": "address0" }} , 
 	{ "name": "imag_o_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_2", "role": "ce0" }} , 
 	{ "name": "imag_o_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_2", "role": "we0" }} , 
 	{ "name": "imag_o_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_2", "role": "d0" }} , 
 	{ "name": "imag_o_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_o_3", "role": "address0" }} , 
 	{ "name": "imag_o_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_3", "role": "ce0" }} , 
 	{ "name": "imag_o_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_3", "role": "we0" }} , 
 	{ "name": "imag_o_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_3", "role": "d0" }} , 
 	{ "name": "imag_o_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_o_4", "role": "address0" }} , 
 	{ "name": "imag_o_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_4", "role": "ce0" }} , 
 	{ "name": "imag_o_4_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_4", "role": "we0" }} , 
 	{ "name": "imag_o_4_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_4", "role": "d0" }} , 
 	{ "name": "imag_o_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_o_5", "role": "address0" }} , 
 	{ "name": "imag_o_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_5", "role": "ce0" }} , 
 	{ "name": "imag_o_5_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_5", "role": "we0" }} , 
 	{ "name": "imag_o_5_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_5", "role": "d0" }} , 
 	{ "name": "imag_o_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_o_6", "role": "address0" }} , 
 	{ "name": "imag_o_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_6", "role": "ce0" }} , 
 	{ "name": "imag_o_6_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_6", "role": "we0" }} , 
 	{ "name": "imag_o_6_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_6", "role": "d0" }} , 
 	{ "name": "imag_o_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_o_7", "role": "address0" }} , 
 	{ "name": "imag_o_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_7", "role": "ce0" }} , 
 	{ "name": "imag_o_7_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_7", "role": "we0" }} , 
 	{ "name": "imag_o_7_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_7", "role": "d0" }} , 
 	{ "name": "imag_o_8_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_o_8", "role": "address0" }} , 
 	{ "name": "imag_o_8_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_8", "role": "ce0" }} , 
 	{ "name": "imag_o_8_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_8", "role": "we0" }} , 
 	{ "name": "imag_o_8_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_8", "role": "d0" }} , 
 	{ "name": "imag_o_9_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_o_9", "role": "address0" }} , 
 	{ "name": "imag_o_9_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_9", "role": "ce0" }} , 
 	{ "name": "imag_o_9_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_9", "role": "we0" }} , 
 	{ "name": "imag_o_9_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_9", "role": "d0" }} , 
 	{ "name": "imag_o_10_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_o_10", "role": "address0" }} , 
 	{ "name": "imag_o_10_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_10", "role": "ce0" }} , 
 	{ "name": "imag_o_10_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_10", "role": "we0" }} , 
 	{ "name": "imag_o_10_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_10", "role": "d0" }} , 
 	{ "name": "imag_o_11_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_o_11", "role": "address0" }} , 
 	{ "name": "imag_o_11_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_11", "role": "ce0" }} , 
 	{ "name": "imag_o_11_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_11", "role": "we0" }} , 
 	{ "name": "imag_o_11_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_11", "role": "d0" }} , 
 	{ "name": "imag_o_12_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_o_12", "role": "address0" }} , 
 	{ "name": "imag_o_12_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_12", "role": "ce0" }} , 
 	{ "name": "imag_o_12_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_12", "role": "we0" }} , 
 	{ "name": "imag_o_12_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_12", "role": "d0" }} , 
 	{ "name": "imag_o_13_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_o_13", "role": "address0" }} , 
 	{ "name": "imag_o_13_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_13", "role": "ce0" }} , 
 	{ "name": "imag_o_13_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_13", "role": "we0" }} , 
 	{ "name": "imag_o_13_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_13", "role": "d0" }} , 
 	{ "name": "imag_o_14_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_o_14", "role": "address0" }} , 
 	{ "name": "imag_o_14_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_14", "role": "ce0" }} , 
 	{ "name": "imag_o_14_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_14", "role": "we0" }} , 
 	{ "name": "imag_o_14_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_14", "role": "d0" }} , 
 	{ "name": "imag_o_15_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_o_15", "role": "address0" }} , 
 	{ "name": "imag_o_15_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_15", "role": "ce0" }} , 
 	{ "name": "imag_o_15_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_15", "role": "we0" }} , 
 	{ "name": "imag_o_15_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_15", "role": "d0" }}  ]}
set Spec2ImplPortList { 
	real_i_0 { ap_memory {  { real_i_0_address0 mem_address 1 4 }  { real_i_0_ce0 mem_ce 1 1 }  { real_i_0_q0 mem_dout 0 32 } } }
	real_i_1 { ap_memory {  { real_i_1_address0 mem_address 1 4 }  { real_i_1_ce0 mem_ce 1 1 }  { real_i_1_q0 mem_dout 0 32 } } }
	real_i_2 { ap_memory {  { real_i_2_address0 mem_address 1 4 }  { real_i_2_ce0 mem_ce 1 1 }  { real_i_2_q0 mem_dout 0 32 } } }
	real_i_3 { ap_memory {  { real_i_3_address0 mem_address 1 4 }  { real_i_3_ce0 mem_ce 1 1 }  { real_i_3_q0 mem_dout 0 32 } } }
	real_i_4 { ap_memory {  { real_i_4_address0 mem_address 1 4 }  { real_i_4_ce0 mem_ce 1 1 }  { real_i_4_q0 mem_dout 0 32 } } }
	real_i_5 { ap_memory {  { real_i_5_address0 mem_address 1 4 }  { real_i_5_ce0 mem_ce 1 1 }  { real_i_5_q0 mem_dout 0 32 } } }
	real_i_6 { ap_memory {  { real_i_6_address0 mem_address 1 4 }  { real_i_6_ce0 mem_ce 1 1 }  { real_i_6_q0 mem_dout 0 32 } } }
	real_i_7 { ap_memory {  { real_i_7_address0 mem_address 1 4 }  { real_i_7_ce0 mem_ce 1 1 }  { real_i_7_q0 mem_dout 0 32 } } }
	real_i_8 { ap_memory {  { real_i_8_address0 mem_address 1 4 }  { real_i_8_ce0 mem_ce 1 1 }  { real_i_8_q0 mem_dout 0 32 } } }
	real_i_9 { ap_memory {  { real_i_9_address0 mem_address 1 4 }  { real_i_9_ce0 mem_ce 1 1 }  { real_i_9_q0 mem_dout 0 32 } } }
	real_i_10 { ap_memory {  { real_i_10_address0 mem_address 1 4 }  { real_i_10_ce0 mem_ce 1 1 }  { real_i_10_q0 mem_dout 0 32 } } }
	real_i_11 { ap_memory {  { real_i_11_address0 mem_address 1 4 }  { real_i_11_ce0 mem_ce 1 1 }  { real_i_11_q0 mem_dout 0 32 } } }
	real_i_12 { ap_memory {  { real_i_12_address0 mem_address 1 4 }  { real_i_12_ce0 mem_ce 1 1 }  { real_i_12_q0 mem_dout 0 32 } } }
	real_i_13 { ap_memory {  { real_i_13_address0 mem_address 1 4 }  { real_i_13_ce0 mem_ce 1 1 }  { real_i_13_q0 mem_dout 0 32 } } }
	real_i_14 { ap_memory {  { real_i_14_address0 mem_address 1 4 }  { real_i_14_ce0 mem_ce 1 1 }  { real_i_14_q0 mem_dout 0 32 } } }
	real_i_15 { ap_memory {  { real_i_15_address0 mem_address 1 4 }  { real_i_15_ce0 mem_ce 1 1 }  { real_i_15_q0 mem_dout 0 32 } } }
	imag_i_0 { ap_memory {  { imag_i_0_address0 mem_address 1 4 }  { imag_i_0_ce0 mem_ce 1 1 }  { imag_i_0_q0 mem_dout 0 32 } } }
	imag_i_1 { ap_memory {  { imag_i_1_address0 mem_address 1 4 }  { imag_i_1_ce0 mem_ce 1 1 }  { imag_i_1_q0 mem_dout 0 32 } } }
	imag_i_2 { ap_memory {  { imag_i_2_address0 mem_address 1 4 }  { imag_i_2_ce0 mem_ce 1 1 }  { imag_i_2_q0 mem_dout 0 32 } } }
	imag_i_3 { ap_memory {  { imag_i_3_address0 mem_address 1 4 }  { imag_i_3_ce0 mem_ce 1 1 }  { imag_i_3_q0 mem_dout 0 32 } } }
	imag_i_4 { ap_memory {  { imag_i_4_address0 mem_address 1 4 }  { imag_i_4_ce0 mem_ce 1 1 }  { imag_i_4_q0 mem_dout 0 32 } } }
	imag_i_5 { ap_memory {  { imag_i_5_address0 mem_address 1 4 }  { imag_i_5_ce0 mem_ce 1 1 }  { imag_i_5_q0 mem_dout 0 32 } } }
	imag_i_6 { ap_memory {  { imag_i_6_address0 mem_address 1 4 }  { imag_i_6_ce0 mem_ce 1 1 }  { imag_i_6_q0 mem_dout 0 32 } } }
	imag_i_7 { ap_memory {  { imag_i_7_address0 mem_address 1 4 }  { imag_i_7_ce0 mem_ce 1 1 }  { imag_i_7_q0 mem_dout 0 32 } } }
	imag_i_8 { ap_memory {  { imag_i_8_address0 mem_address 1 4 }  { imag_i_8_ce0 mem_ce 1 1 }  { imag_i_8_q0 mem_dout 0 32 } } }
	imag_i_9 { ap_memory {  { imag_i_9_address0 mem_address 1 4 }  { imag_i_9_ce0 mem_ce 1 1 }  { imag_i_9_q0 mem_dout 0 32 } } }
	imag_i_10 { ap_memory {  { imag_i_10_address0 mem_address 1 4 }  { imag_i_10_ce0 mem_ce 1 1 }  { imag_i_10_q0 mem_dout 0 32 } } }
	imag_i_11 { ap_memory {  { imag_i_11_address0 mem_address 1 4 }  { imag_i_11_ce0 mem_ce 1 1 }  { imag_i_11_q0 mem_dout 0 32 } } }
	imag_i_12 { ap_memory {  { imag_i_12_address0 mem_address 1 4 }  { imag_i_12_ce0 mem_ce 1 1 }  { imag_i_12_q0 mem_dout 0 32 } } }
	imag_i_13 { ap_memory {  { imag_i_13_address0 mem_address 1 4 }  { imag_i_13_ce0 mem_ce 1 1 }  { imag_i_13_q0 mem_dout 0 32 } } }
	imag_i_14 { ap_memory {  { imag_i_14_address0 mem_address 1 4 }  { imag_i_14_ce0 mem_ce 1 1 }  { imag_i_14_q0 mem_dout 0 32 } } }
	imag_i_15 { ap_memory {  { imag_i_15_address0 mem_address 1 4 }  { imag_i_15_ce0 mem_ce 1 1 }  { imag_i_15_q0 mem_dout 0 32 } } }
	real_o_0 { ap_memory {  { real_o_0_address0 mem_address 1 4 }  { real_o_0_ce0 mem_ce 1 1 }  { real_o_0_we0 mem_we 1 1 }  { real_o_0_d0 mem_din 1 32 } } }
	real_o_1 { ap_memory {  { real_o_1_address0 mem_address 1 4 }  { real_o_1_ce0 mem_ce 1 1 }  { real_o_1_we0 mem_we 1 1 }  { real_o_1_d0 mem_din 1 32 } } }
	real_o_2 { ap_memory {  { real_o_2_address0 mem_address 1 4 }  { real_o_2_ce0 mem_ce 1 1 }  { real_o_2_we0 mem_we 1 1 }  { real_o_2_d0 mem_din 1 32 } } }
	real_o_3 { ap_memory {  { real_o_3_address0 mem_address 1 4 }  { real_o_3_ce0 mem_ce 1 1 }  { real_o_3_we0 mem_we 1 1 }  { real_o_3_d0 mem_din 1 32 } } }
	real_o_4 { ap_memory {  { real_o_4_address0 mem_address 1 4 }  { real_o_4_ce0 mem_ce 1 1 }  { real_o_4_we0 mem_we 1 1 }  { real_o_4_d0 mem_din 1 32 } } }
	real_o_5 { ap_memory {  { real_o_5_address0 mem_address 1 4 }  { real_o_5_ce0 mem_ce 1 1 }  { real_o_5_we0 mem_we 1 1 }  { real_o_5_d0 mem_din 1 32 } } }
	real_o_6 { ap_memory {  { real_o_6_address0 mem_address 1 4 }  { real_o_6_ce0 mem_ce 1 1 }  { real_o_6_we0 mem_we 1 1 }  { real_o_6_d0 mem_din 1 32 } } }
	real_o_7 { ap_memory {  { real_o_7_address0 mem_address 1 4 }  { real_o_7_ce0 mem_ce 1 1 }  { real_o_7_we0 mem_we 1 1 }  { real_o_7_d0 mem_din 1 32 } } }
	real_o_8 { ap_memory {  { real_o_8_address0 mem_address 1 4 }  { real_o_8_ce0 mem_ce 1 1 }  { real_o_8_we0 mem_we 1 1 }  { real_o_8_d0 mem_din 1 32 } } }
	real_o_9 { ap_memory {  { real_o_9_address0 mem_address 1 4 }  { real_o_9_ce0 mem_ce 1 1 }  { real_o_9_we0 mem_we 1 1 }  { real_o_9_d0 mem_din 1 32 } } }
	real_o_10 { ap_memory {  { real_o_10_address0 mem_address 1 4 }  { real_o_10_ce0 mem_ce 1 1 }  { real_o_10_we0 mem_we 1 1 }  { real_o_10_d0 mem_din 1 32 } } }
	real_o_11 { ap_memory {  { real_o_11_address0 mem_address 1 4 }  { real_o_11_ce0 mem_ce 1 1 }  { real_o_11_we0 mem_we 1 1 }  { real_o_11_d0 mem_din 1 32 } } }
	real_o_12 { ap_memory {  { real_o_12_address0 mem_address 1 4 }  { real_o_12_ce0 mem_ce 1 1 }  { real_o_12_we0 mem_we 1 1 }  { real_o_12_d0 mem_din 1 32 } } }
	real_o_13 { ap_memory {  { real_o_13_address0 mem_address 1 4 }  { real_o_13_ce0 mem_ce 1 1 }  { real_o_13_we0 mem_we 1 1 }  { real_o_13_d0 mem_din 1 32 } } }
	real_o_14 { ap_memory {  { real_o_14_address0 mem_address 1 4 }  { real_o_14_ce0 mem_ce 1 1 }  { real_o_14_we0 mem_we 1 1 }  { real_o_14_d0 mem_din 1 32 } } }
	real_o_15 { ap_memory {  { real_o_15_address0 mem_address 1 4 }  { real_o_15_ce0 mem_ce 1 1 }  { real_o_15_we0 mem_we 1 1 }  { real_o_15_d0 mem_din 1 32 } } }
	imag_o_0 { ap_memory {  { imag_o_0_address0 mem_address 1 4 }  { imag_o_0_ce0 mem_ce 1 1 }  { imag_o_0_we0 mem_we 1 1 }  { imag_o_0_d0 mem_din 1 32 } } }
	imag_o_1 { ap_memory {  { imag_o_1_address0 mem_address 1 4 }  { imag_o_1_ce0 mem_ce 1 1 }  { imag_o_1_we0 mem_we 1 1 }  { imag_o_1_d0 mem_din 1 32 } } }
	imag_o_2 { ap_memory {  { imag_o_2_address0 mem_address 1 4 }  { imag_o_2_ce0 mem_ce 1 1 }  { imag_o_2_we0 mem_we 1 1 }  { imag_o_2_d0 mem_din 1 32 } } }
	imag_o_3 { ap_memory {  { imag_o_3_address0 mem_address 1 4 }  { imag_o_3_ce0 mem_ce 1 1 }  { imag_o_3_we0 mem_we 1 1 }  { imag_o_3_d0 mem_din 1 32 } } }
	imag_o_4 { ap_memory {  { imag_o_4_address0 mem_address 1 4 }  { imag_o_4_ce0 mem_ce 1 1 }  { imag_o_4_we0 mem_we 1 1 }  { imag_o_4_d0 mem_din 1 32 } } }
	imag_o_5 { ap_memory {  { imag_o_5_address0 mem_address 1 4 }  { imag_o_5_ce0 mem_ce 1 1 }  { imag_o_5_we0 mem_we 1 1 }  { imag_o_5_d0 mem_din 1 32 } } }
	imag_o_6 { ap_memory {  { imag_o_6_address0 mem_address 1 4 }  { imag_o_6_ce0 mem_ce 1 1 }  { imag_o_6_we0 mem_we 1 1 }  { imag_o_6_d0 mem_din 1 32 } } }
	imag_o_7 { ap_memory {  { imag_o_7_address0 mem_address 1 4 }  { imag_o_7_ce0 mem_ce 1 1 }  { imag_o_7_we0 mem_we 1 1 }  { imag_o_7_d0 mem_din 1 32 } } }
	imag_o_8 { ap_memory {  { imag_o_8_address0 mem_address 1 4 }  { imag_o_8_ce0 mem_ce 1 1 }  { imag_o_8_we0 mem_we 1 1 }  { imag_o_8_d0 mem_din 1 32 } } }
	imag_o_9 { ap_memory {  { imag_o_9_address0 mem_address 1 4 }  { imag_o_9_ce0 mem_ce 1 1 }  { imag_o_9_we0 mem_we 1 1 }  { imag_o_9_d0 mem_din 1 32 } } }
	imag_o_10 { ap_memory {  { imag_o_10_address0 mem_address 1 4 }  { imag_o_10_ce0 mem_ce 1 1 }  { imag_o_10_we0 mem_we 1 1 }  { imag_o_10_d0 mem_din 1 32 } } }
	imag_o_11 { ap_memory {  { imag_o_11_address0 mem_address 1 4 }  { imag_o_11_ce0 mem_ce 1 1 }  { imag_o_11_we0 mem_we 1 1 }  { imag_o_11_d0 mem_din 1 32 } } }
	imag_o_12 { ap_memory {  { imag_o_12_address0 mem_address 1 4 }  { imag_o_12_ce0 mem_ce 1 1 }  { imag_o_12_we0 mem_we 1 1 }  { imag_o_12_d0 mem_din 1 32 } } }
	imag_o_13 { ap_memory {  { imag_o_13_address0 mem_address 1 4 }  { imag_o_13_ce0 mem_ce 1 1 }  { imag_o_13_we0 mem_we 1 1 }  { imag_o_13_d0 mem_din 1 32 } } }
	imag_o_14 { ap_memory {  { imag_o_14_address0 mem_address 1 4 }  { imag_o_14_ce0 mem_ce 1 1 }  { imag_o_14_we0 mem_we 1 1 }  { imag_o_14_d0 mem_din 1 32 } } }
	imag_o_15 { ap_memory {  { imag_o_15_address0 mem_address 1 4 }  { imag_o_15_ce0 mem_ce 1 1 }  { imag_o_15_we0 mem_we 1 1 }  { imag_o_15_d0 mem_din 1 32 } } }
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
