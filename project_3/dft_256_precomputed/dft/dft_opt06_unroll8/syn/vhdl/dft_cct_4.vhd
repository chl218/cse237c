-- ==============================================================
-- File generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
-- Version: 2015.4
-- Copyright (C) 2015 Xilinx Inc. All rights reserved.
-- 
-- ==============================================================

library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;

entity dft_cct_4_rom is 
    generic(
             dwidth     : integer := 32; 
             awidth     : integer := 5; 
             mem_size    : integer := 32
    ); 
    port (
          addr0      : in std_logic_vector(awidth-1 downto 0); 
          ce0       : in std_logic; 
          q0         : out std_logic_vector(dwidth-1 downto 0);
          addr1      : in std_logic_vector(awidth-1 downto 0); 
          ce1       : in std_logic; 
          q1         : out std_logic_vector(dwidth-1 downto 0);
          clk       : in std_logic
    ); 
end entity; 


architecture rtl of dft_cct_4_rom is 

signal addr0_tmp : std_logic_vector(awidth-1 downto 0); 
signal addr1_tmp : std_logic_vector(awidth-1 downto 0); 
type mem_array is array (0 to mem_size-1) of std_logic_vector (dwidth-1 downto 0); 
signal mem : mem_array := (
    0 => "00111111011111101100010001110010", 
    1 => "00111111011101001111101000000101", 
    2 => "00111111011000011100010110010011", 
    3 => "00111111010001011110001111111100", 
    4 => "00111111001000100110011110010100", 
    5 => "00111110111100010101101011110011", 
    6 => "00111110100101001010000000111100", 
    7 => "00111101110010001011110100100011", 
    8 => "10111101110010001011110100100011", 
    9 => "10111110100101001010000000111100", 
    10 => "10111110111100010101101011110011", 
    11 => "10111111001000100110011110010100", 
    12 => "10111111010001011110001111111100", 
    13 => "10111111011000011100010110010011", 
    14 => "10111111011101001111101000000101", 
    15 to 16=> "10111111011111101100010001110010", 
    17 => "10111111011101001111101000000101", 
    18 => "10111111011000011100010110010011", 
    19 => "10111111010001011110001111111100", 
    20 => "10111111001000100110011110010100", 
    21 => "10111110111100010101101011110011", 
    22 => "10111110100101001010000000111100", 
    23 => "10111101110010001011110100100011", 
    24 => "00111101110010001011110100100011", 
    25 => "00111110100101001010000000111100", 
    26 => "00111110111100010101101011110011", 
    27 => "00111111001000100110011110010100", 
    28 => "00111111010001011110001111111100", 
    29 => "00111111011000011100010110010011", 
    30 => "00111111011101001111101000000101", 
    31 => "00111111011111101100010001110010" );


attribute EQUIVALENT_REGISTER_REMOVAL : string;
begin 


memory_access_guard_0: process (addr0) 
begin
      addr0_tmp <= addr0;
--synthesis translate_off
      if (CONV_INTEGER(addr0) > mem_size-1) then
           addr0_tmp <= (others => '0');
      else 
           addr0_tmp <= addr0;
      end if;
--synthesis translate_on
end process;

memory_access_guard_1: process (addr1) 
begin
      addr1_tmp <= addr1;
--synthesis translate_off
      if (CONV_INTEGER(addr1) > mem_size-1) then
           addr1_tmp <= (others => '0');
      else 
           addr1_tmp <= addr1;
      end if;
--synthesis translate_on
end process;

p_rom_access: process (clk)  
begin 
    if (clk'event and clk = '1') then
        if (ce0 = '1') then 
            q0 <= mem(CONV_INTEGER(addr0_tmp)); 
        end if;
        if (ce1 = '1') then 
            q1 <= mem(CONV_INTEGER(addr1_tmp)); 
        end if;
    end if;
end process;

end rtl;


Library IEEE;
use IEEE.std_logic_1164.all;

entity dft_cct_4 is
    generic (
        DataWidth : INTEGER := 32;
        AddressRange : INTEGER := 32;
        AddressWidth : INTEGER := 5);
    port (
        reset : IN STD_LOGIC;
        clk : IN STD_LOGIC;
        address0 : IN STD_LOGIC_VECTOR(AddressWidth - 1 DOWNTO 0);
        ce0 : IN STD_LOGIC;
        q0 : OUT STD_LOGIC_VECTOR(DataWidth - 1 DOWNTO 0);
        address1 : IN STD_LOGIC_VECTOR(AddressWidth - 1 DOWNTO 0);
        ce1 : IN STD_LOGIC;
        q1 : OUT STD_LOGIC_VECTOR(DataWidth - 1 DOWNTO 0));
end entity;

architecture arch of dft_cct_4 is
    component dft_cct_4_rom is
        port (
            clk : IN STD_LOGIC;
            addr0 : IN STD_LOGIC_VECTOR;
            ce0 : IN STD_LOGIC;
            q0 : OUT STD_LOGIC_VECTOR;
            addr1 : IN STD_LOGIC_VECTOR;
            ce1 : IN STD_LOGIC;
            q1 : OUT STD_LOGIC_VECTOR);
    end component;



begin
    dft_cct_4_rom_U :  component dft_cct_4_rom
    port map (
        clk => clk,
        addr0 => address0,
        ce0 => ce0,
        q0 => q0,
        addr1 => address1,
        ce1 => ce1,
        q1 => q1);

end architecture;


