set C_TypeInfoList {{ 
"dft" : [[], { "return": [[], "void"]} , [{"ExternC" : 0}], [ {"real_i": [[], {"array": ["0", [256]]}] }, {"imag_i": [[], {"array": ["0", [256]]}] }, {"real_o": [[], {"array": ["0", [256]]}] }, {"imag_o": [[], {"array": ["0", [256]]}] }],[],""], 
"0": [ "DTYPE", {"typedef": [[[], {"scalar": "float"}],""]}]
}}
set moduleName dft
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {dft}
set C_modelType { void 0 }
set C_modelArgList { 
	{ real_i_0 float 32 regular {array 32 { 1 3 } 1 1 }  }
	{ real_i_1 float 32 regular {array 32 { 1 3 } 1 1 }  }
	{ real_i_2 float 32 regular {array 32 { 1 3 } 1 1 }  }
	{ real_i_3 float 32 regular {array 32 { 1 3 } 1 1 }  }
	{ real_i_4 float 32 regular {array 32 { 1 3 } 1 1 }  }
	{ real_i_5 float 32 regular {array 32 { 1 3 } 1 1 }  }
	{ real_i_6 float 32 regular {array 32 { 1 3 } 1 1 }  }
	{ real_i_7 float 32 regular {array 32 { 1 3 } 1 1 }  }
	{ imag_i_0 float 32 regular {array 32 { 1 3 } 1 1 }  }
	{ imag_i_1 float 32 regular {array 32 { 1 3 } 1 1 }  }
	{ imag_i_2 float 32 regular {array 32 { 1 3 } 1 1 }  }
	{ imag_i_3 float 32 regular {array 32 { 1 3 } 1 1 }  }
	{ imag_i_4 float 32 regular {array 32 { 1 3 } 1 1 }  }
	{ imag_i_5 float 32 regular {array 32 { 1 3 } 1 1 }  }
	{ imag_i_6 float 32 regular {array 32 { 1 3 } 1 1 }  }
	{ imag_i_7 float 32 regular {array 32 { 1 3 } 1 1 }  }
	{ real_o_0 float 32 regular {array 32 { 0 3 } 0 1 }  }
	{ real_o_1 float 32 regular {array 32 { 0 3 } 0 1 }  }
	{ real_o_2 float 32 regular {array 32 { 0 3 } 0 1 }  }
	{ real_o_3 float 32 regular {array 32 { 0 3 } 0 1 }  }
	{ real_o_4 float 32 regular {array 32 { 0 3 } 0 1 }  }
	{ real_o_5 float 32 regular {array 32 { 0 3 } 0 1 }  }
	{ real_o_6 float 32 regular {array 32 { 0 3 } 0 1 }  }
	{ real_o_7 float 32 regular {array 32 { 0 3 } 0 1 }  }
	{ imag_o_0 float 32 regular {array 32 { 0 3 } 0 1 }  }
	{ imag_o_1 float 32 regular {array 32 { 0 3 } 0 1 }  }
	{ imag_o_2 float 32 regular {array 32 { 0 3 } 0 1 }  }
	{ imag_o_3 float 32 regular {array 32 { 0 3 } 0 1 }  }
	{ imag_o_4 float 32 regular {array 32 { 0 3 } 0 1 }  }
	{ imag_o_5 float 32 regular {array 32 { 0 3 } 0 1 }  }
	{ imag_o_6 float 32 regular {array 32 { 0 3 } 0 1 }  }
	{ imag_o_7 float 32 regular {array 32 { 0 3 } 0 1 }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "real_i_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 248,"step" : 8}]}]}]} , 
 	{ "Name" : "real_i_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 249,"step" : 8}]}]}]} , 
 	{ "Name" : "real_i_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 250,"step" : 8}]}]}]} , 
 	{ "Name" : "real_i_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 251,"step" : 8}]}]}]} , 
 	{ "Name" : "real_i_4", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 4,"up" : 252,"step" : 8}]}]}]} , 
 	{ "Name" : "real_i_5", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 5,"up" : 253,"step" : 8}]}]}]} , 
 	{ "Name" : "real_i_6", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 6,"up" : 254,"step" : 8}]}]}]} , 
 	{ "Name" : "real_i_7", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 7,"up" : 255,"step" : 8}]}]}]} , 
 	{ "Name" : "imag_i_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 248,"step" : 8}]}]}]} , 
 	{ "Name" : "imag_i_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 249,"step" : 8}]}]}]} , 
 	{ "Name" : "imag_i_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 250,"step" : 8}]}]}]} , 
 	{ "Name" : "imag_i_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 251,"step" : 8}]}]}]} , 
 	{ "Name" : "imag_i_4", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 4,"up" : 252,"step" : 8}]}]}]} , 
 	{ "Name" : "imag_i_5", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 5,"up" : 253,"step" : 8}]}]}]} , 
 	{ "Name" : "imag_i_6", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 6,"up" : 254,"step" : 8}]}]}]} , 
 	{ "Name" : "imag_i_7", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 7,"up" : 255,"step" : 8}]}]}]} , 
 	{ "Name" : "real_o_0", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 248,"step" : 8}]}]}]} , 
 	{ "Name" : "real_o_1", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 249,"step" : 8}]}]}]} , 
 	{ "Name" : "real_o_2", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 250,"step" : 8}]}]}]} , 
 	{ "Name" : "real_o_3", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 251,"step" : 8}]}]}]} , 
 	{ "Name" : "real_o_4", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 4,"up" : 252,"step" : 8}]}]}]} , 
 	{ "Name" : "real_o_5", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 5,"up" : 253,"step" : 8}]}]}]} , 
 	{ "Name" : "real_o_6", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 6,"up" : 254,"step" : 8}]}]}]} , 
 	{ "Name" : "real_o_7", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 7,"up" : 255,"step" : 8}]}]}]} , 
 	{ "Name" : "imag_o_0", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 248,"step" : 8}]}]}]} , 
 	{ "Name" : "imag_o_1", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 249,"step" : 8}]}]}]} , 
 	{ "Name" : "imag_o_2", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 250,"step" : 8}]}]}]} , 
 	{ "Name" : "imag_o_3", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 251,"step" : 8}]}]}]} , 
 	{ "Name" : "imag_o_4", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 4,"up" : 252,"step" : 8}]}]}]} , 
 	{ "Name" : "imag_o_5", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 5,"up" : 253,"step" : 8}]}]}]} , 
 	{ "Name" : "imag_o_6", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 6,"up" : 254,"step" : 8}]}]}]} , 
 	{ "Name" : "imag_o_7", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 7,"up" : 255,"step" : 8}]}]}]} ]}
# RTL Port declarations: 
set portNum 118
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ real_i_0_address0 sc_out sc_lv 5 signal 0 } 
	{ real_i_0_ce0 sc_out sc_logic 1 signal 0 } 
	{ real_i_0_q0 sc_in sc_lv 32 signal 0 } 
	{ real_i_1_address0 sc_out sc_lv 5 signal 1 } 
	{ real_i_1_ce0 sc_out sc_logic 1 signal 1 } 
	{ real_i_1_q0 sc_in sc_lv 32 signal 1 } 
	{ real_i_2_address0 sc_out sc_lv 5 signal 2 } 
	{ real_i_2_ce0 sc_out sc_logic 1 signal 2 } 
	{ real_i_2_q0 sc_in sc_lv 32 signal 2 } 
	{ real_i_3_address0 sc_out sc_lv 5 signal 3 } 
	{ real_i_3_ce0 sc_out sc_logic 1 signal 3 } 
	{ real_i_3_q0 sc_in sc_lv 32 signal 3 } 
	{ real_i_4_address0 sc_out sc_lv 5 signal 4 } 
	{ real_i_4_ce0 sc_out sc_logic 1 signal 4 } 
	{ real_i_4_q0 sc_in sc_lv 32 signal 4 } 
	{ real_i_5_address0 sc_out sc_lv 5 signal 5 } 
	{ real_i_5_ce0 sc_out sc_logic 1 signal 5 } 
	{ real_i_5_q0 sc_in sc_lv 32 signal 5 } 
	{ real_i_6_address0 sc_out sc_lv 5 signal 6 } 
	{ real_i_6_ce0 sc_out sc_logic 1 signal 6 } 
	{ real_i_6_q0 sc_in sc_lv 32 signal 6 } 
	{ real_i_7_address0 sc_out sc_lv 5 signal 7 } 
	{ real_i_7_ce0 sc_out sc_logic 1 signal 7 } 
	{ real_i_7_q0 sc_in sc_lv 32 signal 7 } 
	{ imag_i_0_address0 sc_out sc_lv 5 signal 8 } 
	{ imag_i_0_ce0 sc_out sc_logic 1 signal 8 } 
	{ imag_i_0_q0 sc_in sc_lv 32 signal 8 } 
	{ imag_i_1_address0 sc_out sc_lv 5 signal 9 } 
	{ imag_i_1_ce0 sc_out sc_logic 1 signal 9 } 
	{ imag_i_1_q0 sc_in sc_lv 32 signal 9 } 
	{ imag_i_2_address0 sc_out sc_lv 5 signal 10 } 
	{ imag_i_2_ce0 sc_out sc_logic 1 signal 10 } 
	{ imag_i_2_q0 sc_in sc_lv 32 signal 10 } 
	{ imag_i_3_address0 sc_out sc_lv 5 signal 11 } 
	{ imag_i_3_ce0 sc_out sc_logic 1 signal 11 } 
	{ imag_i_3_q0 sc_in sc_lv 32 signal 11 } 
	{ imag_i_4_address0 sc_out sc_lv 5 signal 12 } 
	{ imag_i_4_ce0 sc_out sc_logic 1 signal 12 } 
	{ imag_i_4_q0 sc_in sc_lv 32 signal 12 } 
	{ imag_i_5_address0 sc_out sc_lv 5 signal 13 } 
	{ imag_i_5_ce0 sc_out sc_logic 1 signal 13 } 
	{ imag_i_5_q0 sc_in sc_lv 32 signal 13 } 
	{ imag_i_6_address0 sc_out sc_lv 5 signal 14 } 
	{ imag_i_6_ce0 sc_out sc_logic 1 signal 14 } 
	{ imag_i_6_q0 sc_in sc_lv 32 signal 14 } 
	{ imag_i_7_address0 sc_out sc_lv 5 signal 15 } 
	{ imag_i_7_ce0 sc_out sc_logic 1 signal 15 } 
	{ imag_i_7_q0 sc_in sc_lv 32 signal 15 } 
	{ real_o_0_address0 sc_out sc_lv 5 signal 16 } 
	{ real_o_0_ce0 sc_out sc_logic 1 signal 16 } 
	{ real_o_0_we0 sc_out sc_logic 1 signal 16 } 
	{ real_o_0_d0 sc_out sc_lv 32 signal 16 } 
	{ real_o_1_address0 sc_out sc_lv 5 signal 17 } 
	{ real_o_1_ce0 sc_out sc_logic 1 signal 17 } 
	{ real_o_1_we0 sc_out sc_logic 1 signal 17 } 
	{ real_o_1_d0 sc_out sc_lv 32 signal 17 } 
	{ real_o_2_address0 sc_out sc_lv 5 signal 18 } 
	{ real_o_2_ce0 sc_out sc_logic 1 signal 18 } 
	{ real_o_2_we0 sc_out sc_logic 1 signal 18 } 
	{ real_o_2_d0 sc_out sc_lv 32 signal 18 } 
	{ real_o_3_address0 sc_out sc_lv 5 signal 19 } 
	{ real_o_3_ce0 sc_out sc_logic 1 signal 19 } 
	{ real_o_3_we0 sc_out sc_logic 1 signal 19 } 
	{ real_o_3_d0 sc_out sc_lv 32 signal 19 } 
	{ real_o_4_address0 sc_out sc_lv 5 signal 20 } 
	{ real_o_4_ce0 sc_out sc_logic 1 signal 20 } 
	{ real_o_4_we0 sc_out sc_logic 1 signal 20 } 
	{ real_o_4_d0 sc_out sc_lv 32 signal 20 } 
	{ real_o_5_address0 sc_out sc_lv 5 signal 21 } 
	{ real_o_5_ce0 sc_out sc_logic 1 signal 21 } 
	{ real_o_5_we0 sc_out sc_logic 1 signal 21 } 
	{ real_o_5_d0 sc_out sc_lv 32 signal 21 } 
	{ real_o_6_address0 sc_out sc_lv 5 signal 22 } 
	{ real_o_6_ce0 sc_out sc_logic 1 signal 22 } 
	{ real_o_6_we0 sc_out sc_logic 1 signal 22 } 
	{ real_o_6_d0 sc_out sc_lv 32 signal 22 } 
	{ real_o_7_address0 sc_out sc_lv 5 signal 23 } 
	{ real_o_7_ce0 sc_out sc_logic 1 signal 23 } 
	{ real_o_7_we0 sc_out sc_logic 1 signal 23 } 
	{ real_o_7_d0 sc_out sc_lv 32 signal 23 } 
	{ imag_o_0_address0 sc_out sc_lv 5 signal 24 } 
	{ imag_o_0_ce0 sc_out sc_logic 1 signal 24 } 
	{ imag_o_0_we0 sc_out sc_logic 1 signal 24 } 
	{ imag_o_0_d0 sc_out sc_lv 32 signal 24 } 
	{ imag_o_1_address0 sc_out sc_lv 5 signal 25 } 
	{ imag_o_1_ce0 sc_out sc_logic 1 signal 25 } 
	{ imag_o_1_we0 sc_out sc_logic 1 signal 25 } 
	{ imag_o_1_d0 sc_out sc_lv 32 signal 25 } 
	{ imag_o_2_address0 sc_out sc_lv 5 signal 26 } 
	{ imag_o_2_ce0 sc_out sc_logic 1 signal 26 } 
	{ imag_o_2_we0 sc_out sc_logic 1 signal 26 } 
	{ imag_o_2_d0 sc_out sc_lv 32 signal 26 } 
	{ imag_o_3_address0 sc_out sc_lv 5 signal 27 } 
	{ imag_o_3_ce0 sc_out sc_logic 1 signal 27 } 
	{ imag_o_3_we0 sc_out sc_logic 1 signal 27 } 
	{ imag_o_3_d0 sc_out sc_lv 32 signal 27 } 
	{ imag_o_4_address0 sc_out sc_lv 5 signal 28 } 
	{ imag_o_4_ce0 sc_out sc_logic 1 signal 28 } 
	{ imag_o_4_we0 sc_out sc_logic 1 signal 28 } 
	{ imag_o_4_d0 sc_out sc_lv 32 signal 28 } 
	{ imag_o_5_address0 sc_out sc_lv 5 signal 29 } 
	{ imag_o_5_ce0 sc_out sc_logic 1 signal 29 } 
	{ imag_o_5_we0 sc_out sc_logic 1 signal 29 } 
	{ imag_o_5_d0 sc_out sc_lv 32 signal 29 } 
	{ imag_o_6_address0 sc_out sc_lv 5 signal 30 } 
	{ imag_o_6_ce0 sc_out sc_logic 1 signal 30 } 
	{ imag_o_6_we0 sc_out sc_logic 1 signal 30 } 
	{ imag_o_6_d0 sc_out sc_lv 32 signal 30 } 
	{ imag_o_7_address0 sc_out sc_lv 5 signal 31 } 
	{ imag_o_7_ce0 sc_out sc_logic 1 signal 31 } 
	{ imag_o_7_we0 sc_out sc_logic 1 signal 31 } 
	{ imag_o_7_d0 sc_out sc_lv 32 signal 31 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "real_i_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_i_0", "role": "address0" }} , 
 	{ "name": "real_i_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "ce0" }} , 
 	{ "name": "real_i_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "q0" }} , 
 	{ "name": "real_i_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_i_1", "role": "address0" }} , 
 	{ "name": "real_i_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "ce0" }} , 
 	{ "name": "real_i_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "q0" }} , 
 	{ "name": "real_i_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_i_2", "role": "address0" }} , 
 	{ "name": "real_i_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_2", "role": "ce0" }} , 
 	{ "name": "real_i_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_2", "role": "q0" }} , 
 	{ "name": "real_i_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_i_3", "role": "address0" }} , 
 	{ "name": "real_i_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_3", "role": "ce0" }} , 
 	{ "name": "real_i_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_3", "role": "q0" }} , 
 	{ "name": "real_i_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_i_4", "role": "address0" }} , 
 	{ "name": "real_i_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_4", "role": "ce0" }} , 
 	{ "name": "real_i_4_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_4", "role": "q0" }} , 
 	{ "name": "real_i_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_i_5", "role": "address0" }} , 
 	{ "name": "real_i_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_5", "role": "ce0" }} , 
 	{ "name": "real_i_5_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_5", "role": "q0" }} , 
 	{ "name": "real_i_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_i_6", "role": "address0" }} , 
 	{ "name": "real_i_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_6", "role": "ce0" }} , 
 	{ "name": "real_i_6_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_6", "role": "q0" }} , 
 	{ "name": "real_i_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_i_7", "role": "address0" }} , 
 	{ "name": "real_i_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_7", "role": "ce0" }} , 
 	{ "name": "real_i_7_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_7", "role": "q0" }} , 
 	{ "name": "imag_i_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_i_0", "role": "address0" }} , 
 	{ "name": "imag_i_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_0", "role": "ce0" }} , 
 	{ "name": "imag_i_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_0", "role": "q0" }} , 
 	{ "name": "imag_i_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_i_1", "role": "address0" }} , 
 	{ "name": "imag_i_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_1", "role": "ce0" }} , 
 	{ "name": "imag_i_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_1", "role": "q0" }} , 
 	{ "name": "imag_i_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_i_2", "role": "address0" }} , 
 	{ "name": "imag_i_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_2", "role": "ce0" }} , 
 	{ "name": "imag_i_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_2", "role": "q0" }} , 
 	{ "name": "imag_i_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_i_3", "role": "address0" }} , 
 	{ "name": "imag_i_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_3", "role": "ce0" }} , 
 	{ "name": "imag_i_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_3", "role": "q0" }} , 
 	{ "name": "imag_i_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_i_4", "role": "address0" }} , 
 	{ "name": "imag_i_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_4", "role": "ce0" }} , 
 	{ "name": "imag_i_4_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_4", "role": "q0" }} , 
 	{ "name": "imag_i_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_i_5", "role": "address0" }} , 
 	{ "name": "imag_i_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_5", "role": "ce0" }} , 
 	{ "name": "imag_i_5_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_5", "role": "q0" }} , 
 	{ "name": "imag_i_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_i_6", "role": "address0" }} , 
 	{ "name": "imag_i_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_6", "role": "ce0" }} , 
 	{ "name": "imag_i_6_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_6", "role": "q0" }} , 
 	{ "name": "imag_i_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_i_7", "role": "address0" }} , 
 	{ "name": "imag_i_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_7", "role": "ce0" }} , 
 	{ "name": "imag_i_7_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_7", "role": "q0" }} , 
 	{ "name": "real_o_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_o_0", "role": "address0" }} , 
 	{ "name": "real_o_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_0", "role": "ce0" }} , 
 	{ "name": "real_o_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_0", "role": "we0" }} , 
 	{ "name": "real_o_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_0", "role": "d0" }} , 
 	{ "name": "real_o_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_o_1", "role": "address0" }} , 
 	{ "name": "real_o_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_1", "role": "ce0" }} , 
 	{ "name": "real_o_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_1", "role": "we0" }} , 
 	{ "name": "real_o_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_1", "role": "d0" }} , 
 	{ "name": "real_o_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_o_2", "role": "address0" }} , 
 	{ "name": "real_o_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_2", "role": "ce0" }} , 
 	{ "name": "real_o_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_2", "role": "we0" }} , 
 	{ "name": "real_o_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_2", "role": "d0" }} , 
 	{ "name": "real_o_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_o_3", "role": "address0" }} , 
 	{ "name": "real_o_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_3", "role": "ce0" }} , 
 	{ "name": "real_o_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_3", "role": "we0" }} , 
 	{ "name": "real_o_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_3", "role": "d0" }} , 
 	{ "name": "real_o_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_o_4", "role": "address0" }} , 
 	{ "name": "real_o_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_4", "role": "ce0" }} , 
 	{ "name": "real_o_4_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_4", "role": "we0" }} , 
 	{ "name": "real_o_4_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_4", "role": "d0" }} , 
 	{ "name": "real_o_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_o_5", "role": "address0" }} , 
 	{ "name": "real_o_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_5", "role": "ce0" }} , 
 	{ "name": "real_o_5_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_5", "role": "we0" }} , 
 	{ "name": "real_o_5_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_5", "role": "d0" }} , 
 	{ "name": "real_o_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_o_6", "role": "address0" }} , 
 	{ "name": "real_o_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_6", "role": "ce0" }} , 
 	{ "name": "real_o_6_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_6", "role": "we0" }} , 
 	{ "name": "real_o_6_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_6", "role": "d0" }} , 
 	{ "name": "real_o_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_o_7", "role": "address0" }} , 
 	{ "name": "real_o_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_7", "role": "ce0" }} , 
 	{ "name": "real_o_7_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o_7", "role": "we0" }} , 
 	{ "name": "real_o_7_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o_7", "role": "d0" }} , 
 	{ "name": "imag_o_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_o_0", "role": "address0" }} , 
 	{ "name": "imag_o_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_0", "role": "ce0" }} , 
 	{ "name": "imag_o_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_0", "role": "we0" }} , 
 	{ "name": "imag_o_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_0", "role": "d0" }} , 
 	{ "name": "imag_o_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_o_1", "role": "address0" }} , 
 	{ "name": "imag_o_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_1", "role": "ce0" }} , 
 	{ "name": "imag_o_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_1", "role": "we0" }} , 
 	{ "name": "imag_o_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_1", "role": "d0" }} , 
 	{ "name": "imag_o_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_o_2", "role": "address0" }} , 
 	{ "name": "imag_o_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_2", "role": "ce0" }} , 
 	{ "name": "imag_o_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_2", "role": "we0" }} , 
 	{ "name": "imag_o_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_2", "role": "d0" }} , 
 	{ "name": "imag_o_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_o_3", "role": "address0" }} , 
 	{ "name": "imag_o_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_3", "role": "ce0" }} , 
 	{ "name": "imag_o_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_3", "role": "we0" }} , 
 	{ "name": "imag_o_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_3", "role": "d0" }} , 
 	{ "name": "imag_o_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_o_4", "role": "address0" }} , 
 	{ "name": "imag_o_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_4", "role": "ce0" }} , 
 	{ "name": "imag_o_4_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_4", "role": "we0" }} , 
 	{ "name": "imag_o_4_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_4", "role": "d0" }} , 
 	{ "name": "imag_o_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_o_5", "role": "address0" }} , 
 	{ "name": "imag_o_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_5", "role": "ce0" }} , 
 	{ "name": "imag_o_5_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_5", "role": "we0" }} , 
 	{ "name": "imag_o_5_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_5", "role": "d0" }} , 
 	{ "name": "imag_o_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_o_6", "role": "address0" }} , 
 	{ "name": "imag_o_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_6", "role": "ce0" }} , 
 	{ "name": "imag_o_6_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_6", "role": "we0" }} , 
 	{ "name": "imag_o_6_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_6", "role": "d0" }} , 
 	{ "name": "imag_o_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_o_7", "role": "address0" }} , 
 	{ "name": "imag_o_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_7", "role": "ce0" }} , 
 	{ "name": "imag_o_7_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o_7", "role": "we0" }} , 
 	{ "name": "imag_o_7_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o_7", "role": "d0" }}  ]}
set Spec2ImplPortList { 
	real_i_0 { ap_memory {  { real_i_0_address0 mem_address 1 5 }  { real_i_0_ce0 mem_ce 1 1 }  { real_i_0_q0 mem_dout 0 32 } } }
	real_i_1 { ap_memory {  { real_i_1_address0 mem_address 1 5 }  { real_i_1_ce0 mem_ce 1 1 }  { real_i_1_q0 mem_dout 0 32 } } }
	real_i_2 { ap_memory {  { real_i_2_address0 mem_address 1 5 }  { real_i_2_ce0 mem_ce 1 1 }  { real_i_2_q0 mem_dout 0 32 } } }
	real_i_3 { ap_memory {  { real_i_3_address0 mem_address 1 5 }  { real_i_3_ce0 mem_ce 1 1 }  { real_i_3_q0 mem_dout 0 32 } } }
	real_i_4 { ap_memory {  { real_i_4_address0 mem_address 1 5 }  { real_i_4_ce0 mem_ce 1 1 }  { real_i_4_q0 mem_dout 0 32 } } }
	real_i_5 { ap_memory {  { real_i_5_address0 mem_address 1 5 }  { real_i_5_ce0 mem_ce 1 1 }  { real_i_5_q0 mem_dout 0 32 } } }
	real_i_6 { ap_memory {  { real_i_6_address0 mem_address 1 5 }  { real_i_6_ce0 mem_ce 1 1 }  { real_i_6_q0 mem_dout 0 32 } } }
	real_i_7 { ap_memory {  { real_i_7_address0 mem_address 1 5 }  { real_i_7_ce0 mem_ce 1 1 }  { real_i_7_q0 mem_dout 0 32 } } }
	imag_i_0 { ap_memory {  { imag_i_0_address0 mem_address 1 5 }  { imag_i_0_ce0 mem_ce 1 1 }  { imag_i_0_q0 mem_dout 0 32 } } }
	imag_i_1 { ap_memory {  { imag_i_1_address0 mem_address 1 5 }  { imag_i_1_ce0 mem_ce 1 1 }  { imag_i_1_q0 mem_dout 0 32 } } }
	imag_i_2 { ap_memory {  { imag_i_2_address0 mem_address 1 5 }  { imag_i_2_ce0 mem_ce 1 1 }  { imag_i_2_q0 mem_dout 0 32 } } }
	imag_i_3 { ap_memory {  { imag_i_3_address0 mem_address 1 5 }  { imag_i_3_ce0 mem_ce 1 1 }  { imag_i_3_q0 mem_dout 0 32 } } }
	imag_i_4 { ap_memory {  { imag_i_4_address0 mem_address 1 5 }  { imag_i_4_ce0 mem_ce 1 1 }  { imag_i_4_q0 mem_dout 0 32 } } }
	imag_i_5 { ap_memory {  { imag_i_5_address0 mem_address 1 5 }  { imag_i_5_ce0 mem_ce 1 1 }  { imag_i_5_q0 mem_dout 0 32 } } }
	imag_i_6 { ap_memory {  { imag_i_6_address0 mem_address 1 5 }  { imag_i_6_ce0 mem_ce 1 1 }  { imag_i_6_q0 mem_dout 0 32 } } }
	imag_i_7 { ap_memory {  { imag_i_7_address0 mem_address 1 5 }  { imag_i_7_ce0 mem_ce 1 1 }  { imag_i_7_q0 mem_dout 0 32 } } }
	real_o_0 { ap_memory {  { real_o_0_address0 mem_address 1 5 }  { real_o_0_ce0 mem_ce 1 1 }  { real_o_0_we0 mem_we 1 1 }  { real_o_0_d0 mem_din 1 32 } } }
	real_o_1 { ap_memory {  { real_o_1_address0 mem_address 1 5 }  { real_o_1_ce0 mem_ce 1 1 }  { real_o_1_we0 mem_we 1 1 }  { real_o_1_d0 mem_din 1 32 } } }
	real_o_2 { ap_memory {  { real_o_2_address0 mem_address 1 5 }  { real_o_2_ce0 mem_ce 1 1 }  { real_o_2_we0 mem_we 1 1 }  { real_o_2_d0 mem_din 1 32 } } }
	real_o_3 { ap_memory {  { real_o_3_address0 mem_address 1 5 }  { real_o_3_ce0 mem_ce 1 1 }  { real_o_3_we0 mem_we 1 1 }  { real_o_3_d0 mem_din 1 32 } } }
	real_o_4 { ap_memory {  { real_o_4_address0 mem_address 1 5 }  { real_o_4_ce0 mem_ce 1 1 }  { real_o_4_we0 mem_we 1 1 }  { real_o_4_d0 mem_din 1 32 } } }
	real_o_5 { ap_memory {  { real_o_5_address0 mem_address 1 5 }  { real_o_5_ce0 mem_ce 1 1 }  { real_o_5_we0 mem_we 1 1 }  { real_o_5_d0 mem_din 1 32 } } }
	real_o_6 { ap_memory {  { real_o_6_address0 mem_address 1 5 }  { real_o_6_ce0 mem_ce 1 1 }  { real_o_6_we0 mem_we 1 1 }  { real_o_6_d0 mem_din 1 32 } } }
	real_o_7 { ap_memory {  { real_o_7_address0 mem_address 1 5 }  { real_o_7_ce0 mem_ce 1 1 }  { real_o_7_we0 mem_we 1 1 }  { real_o_7_d0 mem_din 1 32 } } }
	imag_o_0 { ap_memory {  { imag_o_0_address0 mem_address 1 5 }  { imag_o_0_ce0 mem_ce 1 1 }  { imag_o_0_we0 mem_we 1 1 }  { imag_o_0_d0 mem_din 1 32 } } }
	imag_o_1 { ap_memory {  { imag_o_1_address0 mem_address 1 5 }  { imag_o_1_ce0 mem_ce 1 1 }  { imag_o_1_we0 mem_we 1 1 }  { imag_o_1_d0 mem_din 1 32 } } }
	imag_o_2 { ap_memory {  { imag_o_2_address0 mem_address 1 5 }  { imag_o_2_ce0 mem_ce 1 1 }  { imag_o_2_we0 mem_we 1 1 }  { imag_o_2_d0 mem_din 1 32 } } }
	imag_o_3 { ap_memory {  { imag_o_3_address0 mem_address 1 5 }  { imag_o_3_ce0 mem_ce 1 1 }  { imag_o_3_we0 mem_we 1 1 }  { imag_o_3_d0 mem_din 1 32 } } }
	imag_o_4 { ap_memory {  { imag_o_4_address0 mem_address 1 5 }  { imag_o_4_ce0 mem_ce 1 1 }  { imag_o_4_we0 mem_we 1 1 }  { imag_o_4_d0 mem_din 1 32 } } }
	imag_o_5 { ap_memory {  { imag_o_5_address0 mem_address 1 5 }  { imag_o_5_ce0 mem_ce 1 1 }  { imag_o_5_we0 mem_we 1 1 }  { imag_o_5_d0 mem_din 1 32 } } }
	imag_o_6 { ap_memory {  { imag_o_6_address0 mem_address 1 5 }  { imag_o_6_ce0 mem_ce 1 1 }  { imag_o_6_we0 mem_we 1 1 }  { imag_o_6_d0 mem_din 1 32 } } }
	imag_o_7 { ap_memory {  { imag_o_7_address0 mem_address 1 5 }  { imag_o_7_ce0 mem_ce 1 1 }  { imag_o_7_we0 mem_we 1 1 }  { imag_o_7_d0 mem_din 1 32 } } }
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
