#include "dft.h"
#include "coefficients256.h"

float real_i[SIZE], imag_i[SIZE];


void save_stream(DTYPE &stream) {
	for(int i = 0; i < SIZE; i++) {
		real_i[i] = stream.real.read();
		imag_i[i] = stream.imag.read();
	}
}

void dft(DTYPE &stream_i, DTYPE &stream_o)
{

	save_stream(stream_i);

	for(int i = 0; i < SIZE; i++) {
		float real_tmp = 0;
		float imag_tmp = 0;
//		float rt = 0;
//		float it = 0;

		for(int j = 0; j < SIZE; j++) {
			int index = (i*j)%SIZE;

			real_tmp += (real_i[j]*cct[index] - imag_i[j]*sct[index]);
			imag_tmp += (real_i[j]*sct[index] + imag_i[j]*cct[index]);

		}
		stream_o.real.write(real_tmp);
		stream_o.imag.write(imag_tmp);

	}
}
