#ifndef DFT_H
#define DFT_H

#include <ap_int.h>
#include "hls_stream.h"
using namespace hls;

#define SIZE 256    	/* SIZE OF DFT */

typedef struct {
	hls::stream<float> real;
	hls::stream<float> imag;
} DTYPE;

void dft(DTYPE &stream_i, DTYPE &steram_o);

#endif
