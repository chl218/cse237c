
#include "dft.h"
#include "coefficients1024.h"

DTYPE real_arr1024[1024], real_arr512[512],
      real_arr256[256],   real_arr128[128], real_arr64[64],   real_arr32[32],
      real_arr16[16],     real_arr8[8],     real_arr4[4],     real_arr2[2];

DTYPE imag_arr1024[1024], imag_arr512[512],
      imag_arr256[256],   imag_arr128[128], imag_arr64[64],   imag_arr32[32],
      imag_arr16[16],     imag_arr8[8],     imag_arr4[4],     imag_arr2[2];

void loop_flow1024(ap_uint<11> *i, DTYPE real_i[SIZE], DTYPE imag_i[SIZE]) {
#pragma HLS DATAFLOW
   for(ap_uint<11> j = 0; j < SIZE; j++) {
#pragma HLS PIPELINE II=1
#pragma HLS UNROLL factor=6
      ap_uint<11> index0 =  ((*i)*j)%SIZE;
      real_arr1024[j] = (real_i[j]*cct[index0] - imag_i[j]*sct[index0]);
      imag_arr1024[j] = (real_i[j]*sct[index0] + imag_i[j]*cct[index0]);
   }
}

void loop_flow512() {
#pragma HLS DATAFLOW
   for(ap_uint<11> i = 0, j = 0; j < 1024; i++, j+=2) {
#pragma HLS PIPELINE II=1
#pragma HLS UNROLL factor=8
      real_arr512[i] = real_arr1024[j] + real_arr1024[j+1];
      imag_arr512[i] = imag_arr1024[j] + imag_arr1024[j+1];
   }
}

void loop_flow256() {
#pragma HLS DATAFLOW
   for(ap_uint<11> i = 0, j = 0; j < 512; i++, j+=2) {
#pragma HLS PIPELINE II=1
#pragma HLS UNROLL factor=8
      real_arr256[i] = real_arr512[j] + real_arr512[j+1];
      imag_arr256[i] = imag_arr512[j] + imag_arr512[j+1];
   }
}

void loop_flow128() {
#pragma HLS DATAFLOW
   for(ap_uint<11> i = 0, j = 0; j < 256; i++, j+=2) {
#pragma HLS PIPELINE II=1
#pragma HLS UNROLL factor=8
      real_arr128[i] = real_arr256[j] + real_arr256[j+1];
      imag_arr128[i] = imag_arr256[j] + imag_arr256[j+1];
   }
}

void loop_flow64() {
#pragma HLS DATAFLOW
   for(ap_uint<11> i = 0,  j = 0; j < 128; i++, j+=2) {
#pragma HLS PIPELINE II=1
#pragma HLS UNROLL factor=8
      real_arr64[i] = real_arr128[j] + real_arr128[j+1];
      imag_arr64[i] = imag_arr128[j] + imag_arr128[j+1];
   }
}

void loop_flow32() {
#pragma HLS DATAFLOW
   for(ap_uint<11> i = 0, j = 0; j < 64; i++, j+=2) {
#pragma HLS PIPELINE II=1
#pragma HLS UNROLL factor=8
      real_arr32[i] = real_arr64[j] + real_arr64[j+1];
      imag_arr32[i] = imag_arr64[j] + imag_arr64[j+1];
   }
}

void loop_flow16() {
#pragma HLS DATAFLOW
   for(ap_uint<11> i = 0, j = 0; j < 32; i++, j+=2) {
#pragma HLS PIPELINE II=1
#pragma HLS UNROLL factor=4
      real_arr16[i] = real_arr32[j] + real_arr32[j+1];
      imag_arr16[i] = imag_arr32[j] + imag_arr32[j+1];
   }
}

void loop_flow8() {
#pragma HLS DATAFLOW
   for(ap_uint<11>i = 0, j = 0; j < 16; i++, j+=2) {
#pragma HLS PIPELINE II=1
#pragma HLS UNROLL factor=2
      real_arr8[i] = real_arr16[j] + real_arr16[j+1];
      imag_arr8[i] = imag_arr16[j] + imag_arr16[j+1];
   }
}

void loop_flow4() {
#pragma HLS DATAFLOW
   for(ap_uint<11> i = 0, j = 0; j < 8; i++,j+=2) {
#pragma HLS PIPELINE II=1
#pragma HLS UNROLL factor=1
      real_arr4[i] = real_arr8[j] + real_arr8[j+1];
      imag_arr4[i] = imag_arr8[j] + imag_arr8[j+1];
   }
}

void loop_flow2() {
#pragma HLS PIPELINE II=1
   real_arr2[0] = real_arr4[0] + real_arr4[1];
   imag_arr2[0] = imag_arr4[0] + imag_arr4[1];

   real_arr2[1] = real_arr4[2] + real_arr4[3];
   imag_arr2[1] = imag_arr4[2] + imag_arr4[3];
}

void dft(DTYPE real_i[SIZE], DTYPE imag_i[SIZE], DTYPE real_o[SIZE], DTYPE imag_o[SIZE])
{
#pragma HLS DATAFLOW

#pragma HLS ARRAY_PARTITION variable=sct cyclic factor=8 dim=1
#pragma HLS ARRAY_PARTITION variable=cct cyclic factor=8 dim=1

#pragma HLS ARRAY_PARTITION variable=imag_i cyclic factor=8 dim=1
#pragma HLS ARRAY_PARTITION variable=real_i cyclic factor=8 dim=1

#pragma HLS ARRAY_PARTITION variable=imag_arr1024 cyclic factor=4 dim=1
#pragma HLS ARRAY_PARTITION variable=real_arr1024 cyclic factor=4 dim=1

#pragma HLS ARRAY_PARTITION variable=imag_arr512 cyclic factor=4 dim=1
#pragma HLS ARRAY_PARTITION variable=real_arr512 cyclic factor=4 dim=1

#pragma HLS ARRAY_PARTITION variable=imag_arr256 cyclic factor=4 dim=1
#pragma HLS ARRAY_PARTITION variable=real_arr256 cyclic factor=4 dim=1

#pragma HLS ARRAY_PARTITION variable=imag_arr128 cyclic factor=4 dim=1
#pragma HLS ARRAY_PARTITION variable=real_arr128 cyclic factor=4 dim=1

#pragma HLS ARRAY_PARTITION variable=imag_arr64 cyclic factor=4 dim=1
#pragma HLS ARRAY_PARTITION variable=real_arr64 cyclic factor=4 dim=1

#pragma HLS ARRAY_PARTITION variable=imag_arr32 cyclic factor=4 dim=1
#pragma HLS ARRAY_PARTITION variable=real_arr32 cyclic factor=4 dim=1

#pragma HLS ARRAY_PARTITION variable=imag_arr16 cyclic factor=4 dim=1
#pragma HLS ARRAY_PARTITION variable=real_arr16 cyclic factor=4 dim=1

#pragma HLS ARRAY_PARTITION variable=imag_arr8 complete factor=4 dim=1
#pragma HLS ARRAY_PARTITION variable=real_arr8 complete factor=4 dim=1

#pragma HLS ARRAY_PARTITION variable=imag_arr4 complete factor=4 dim=1
#pragma HLS ARRAY_PARTITION variable=real_arr4 complete factor=4 dim=1

#pragma HLS ARRAY_PARTITION variable=imag_arr2 complete factor=4 dim=1
#pragma HLS ARRAY_PARTITION variable=real_arr2 complete factor=4 dim=1

   Loop_Row: for(ap_uint<11> i = 0; i < SIZE; i++) {
#pragma HLS UNROLL factor=8

      loop_flow1024(&i, real_i, imag_i);
      loop_flow512();
      loop_flow256();
      loop_flow128();
      loop_flow64();
      loop_flow32();
      loop_flow16();
      loop_flow8();
      loop_flow4();
      loop_flow2();

      real_o[i] = real_arr2[0] + real_arr2[1];
      imag_o[i] = imag_arr2[0] + imag_arr2[1];
   }
}

