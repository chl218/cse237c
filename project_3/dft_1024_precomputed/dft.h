#ifndef DFT_H
#define DFT_H

#include <ap_int.h>

typedef float DTYPE;

#define SIZE 1024 		/* SIZE OF DFT */

void dft(DTYPE real_i[SIZE], DTYPE imag_i[SIZE], DTYPE real_o[SIZE], DTYPE imag_o[SIZE]);

#endif
