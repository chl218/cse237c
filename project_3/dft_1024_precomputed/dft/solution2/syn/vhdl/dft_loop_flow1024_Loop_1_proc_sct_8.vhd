-- ==============================================================
-- File generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
-- Version: 2015.4
-- Copyright (C) 2015 Xilinx Inc. All rights reserved.
-- 
-- ==============================================================

library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;

entity dft_loop_flow1024_Loop_1_proc_sct_8_rom is 
    generic(
             dwidth     : integer := 32; 
             awidth     : integer := 6; 
             mem_size    : integer := 64
    ); 
    port (
          addr0      : in std_logic_vector(awidth-1 downto 0); 
          ce0       : in std_logic; 
          q0         : out std_logic_vector(dwidth-1 downto 0);
          addr1      : in std_logic_vector(awidth-1 downto 0); 
          ce1       : in std_logic; 
          q1         : out std_logic_vector(dwidth-1 downto 0);
          addr2      : in std_logic_vector(awidth-1 downto 0); 
          ce2       : in std_logic; 
          q2         : out std_logic_vector(dwidth-1 downto 0);
          addr3      : in std_logic_vector(awidth-1 downto 0); 
          ce3       : in std_logic; 
          q3         : out std_logic_vector(dwidth-1 downto 0);
          clk       : in std_logic
    ); 
end entity; 


architecture rtl of dft_loop_flow1024_Loop_1_proc_sct_8_rom is 

signal addr0_tmp : std_logic_vector(awidth-1 downto 0); 
signal addr1_tmp : std_logic_vector(awidth-1 downto 0); 
signal addr2_tmp : std_logic_vector(awidth-1 downto 0); 
signal addr3_tmp : std_logic_vector(awidth-1 downto 0); 
type mem_array is array (0 to mem_size-1) of std_logic_vector (dwidth-1 downto 0); 
signal mem0 : mem_array := (
    0 => "10111101010010001111101110000111", 
    1 => "10111110000101100100000001100100", 
    2 => "10111110011110001100111111000000", 
    3 => "10111110101011000111110011011001", 
    4 => "10111110110110101110100001111101", 
    5 => "10111111000000111001110001000001", 
    6 => "10111111000110000111111110111011", 
    7 => "10111111001010111110101101001010", 
    8 => "10111111001111011010111011110111", 
    9 => "10111111010011011001111100001010", 
    10 => "10111111010110111001010000100001", 
    11 => "10111111011001110110101111010011", 
    12 => "10111111011100010000100100000111", 
    13 => "10111111011110000101001111110100", 
    14 => "10111111011111010011101010110100", 
    15 to 16=> "10111111011111111011000100000111", 
    17 => "10111111011111010011101010110100", 
    18 => "10111111011110000101001111110100", 
    19 => "10111111011100010000100100000111", 
    20 => "10111111011001110110101111010011", 
    21 => "10111111010110111001010000100001", 
    22 => "10111111010011011001111100001010", 
    23 => "10111111001111011010111011110111", 
    24 => "10111111001010111110101101001010", 
    25 => "10111111000110000111111110111011", 
    26 => "10111111000000111001110001000001", 
    27 => "10111110110110101110100001111101", 
    28 => "10111110101011000111110011011001", 
    29 => "10111110011110001100111111000000", 
    30 => "10111110000101100100000001100100", 
    31 => "10111101010010001111101110000111", 
    32 => "00111101010010001111101110000111", 
    33 => "00111110000101100100000001100100", 
    34 => "00111110011110001100111111000000", 
    35 => "00111110101011000111110011011001", 
    36 => "00111110110110101110100001111101", 
    37 => "00111111000000111001110001000001", 
    38 => "00111111000110000111111110111011", 
    39 => "00111111001010111110101101001010", 
    40 => "00111111001111011010111011110111", 
    41 => "00111111010011011001111100001010", 
    42 => "00111111010110111001010000100001", 
    43 => "00111111011001110110101111010011", 
    44 => "00111111011100010000100100000111", 
    45 => "00111111011110000101001111110100", 
    46 => "00111111011111010011101010110100", 
    47 to 48=> "00111111011111111011000100000111", 
    49 => "00111111011111010011101010110100", 
    50 => "00111111011110000101001111110100", 
    51 => "00111111011100010000100100000111", 
    52 => "00111111011001110110101111010011", 
    53 => "00111111010110111001010000100001", 
    54 => "00111111010011011001111100001010", 
    55 => "00111111001111011010111011110111", 
    56 => "00111111001010111110101101001010", 
    57 => "00111111000110000111111110111011", 
    58 => "00111111000000111001110001000001", 
    59 => "00111110110110101110100001111101", 
    60 => "00111110101011000111110011011001", 
    61 => "00111110011110001100111111000000", 
    62 => "00111110000101100100000001100100", 
    63 => "00111101010010001111101110000111" );
signal mem1 : mem_array := (
    0 => "10111101010010001111101110000111", 
    1 => "10111110000101100100000001100100", 
    2 => "10111110011110001100111111000000", 
    3 => "10111110101011000111110011011001", 
    4 => "10111110110110101110100001111101", 
    5 => "10111111000000111001110001000001", 
    6 => "10111111000110000111111110111011", 
    7 => "10111111001010111110101101001010", 
    8 => "10111111001111011010111011110111", 
    9 => "10111111010011011001111100001010", 
    10 => "10111111010110111001010000100001", 
    11 => "10111111011001110110101111010011", 
    12 => "10111111011100010000100100000111", 
    13 => "10111111011110000101001111110100", 
    14 => "10111111011111010011101010110100", 
    15 to 16=> "10111111011111111011000100000111", 
    17 => "10111111011111010011101010110100", 
    18 => "10111111011110000101001111110100", 
    19 => "10111111011100010000100100000111", 
    20 => "10111111011001110110101111010011", 
    21 => "10111111010110111001010000100001", 
    22 => "10111111010011011001111100001010", 
    23 => "10111111001111011010111011110111", 
    24 => "10111111001010111110101101001010", 
    25 => "10111111000110000111111110111011", 
    26 => "10111111000000111001110001000001", 
    27 => "10111110110110101110100001111101", 
    28 => "10111110101011000111110011011001", 
    29 => "10111110011110001100111111000000", 
    30 => "10111110000101100100000001100100", 
    31 => "10111101010010001111101110000111", 
    32 => "00111101010010001111101110000111", 
    33 => "00111110000101100100000001100100", 
    34 => "00111110011110001100111111000000", 
    35 => "00111110101011000111110011011001", 
    36 => "00111110110110101110100001111101", 
    37 => "00111111000000111001110001000001", 
    38 => "00111111000110000111111110111011", 
    39 => "00111111001010111110101101001010", 
    40 => "00111111001111011010111011110111", 
    41 => "00111111010011011001111100001010", 
    42 => "00111111010110111001010000100001", 
    43 => "00111111011001110110101111010011", 
    44 => "00111111011100010000100100000111", 
    45 => "00111111011110000101001111110100", 
    46 => "00111111011111010011101010110100", 
    47 to 48=> "00111111011111111011000100000111", 
    49 => "00111111011111010011101010110100", 
    50 => "00111111011110000101001111110100", 
    51 => "00111111011100010000100100000111", 
    52 => "00111111011001110110101111010011", 
    53 => "00111111010110111001010000100001", 
    54 => "00111111010011011001111100001010", 
    55 => "00111111001111011010111011110111", 
    56 => "00111111001010111110101101001010", 
    57 => "00111111000110000111111110111011", 
    58 => "00111111000000111001110001000001", 
    59 => "00111110110110101110100001111101", 
    60 => "00111110101011000111110011011001", 
    61 => "00111110011110001100111111000000", 
    62 => "00111110000101100100000001100100", 
    63 => "00111101010010001111101110000111" );

attribute syn_rom_style : string;
attribute syn_rom_style of mem0 : signal is "block_rom";
attribute syn_rom_style of mem1 : signal is "block_rom";
attribute ROM_STYLE : string;
attribute ROM_STYLE of mem0 : signal is "block";
attribute ROM_STYLE of mem1 : signal is "block";

attribute EQUIVALENT_REGISTER_REMOVAL : string;
begin 


memory_access_guard_0: process (addr0) 
begin
      addr0_tmp <= addr0;
--synthesis translate_off
      if (CONV_INTEGER(addr0) > mem_size-1) then
           addr0_tmp <= (others => '0');
      else 
           addr0_tmp <= addr0;
      end if;
--synthesis translate_on
end process;

memory_access_guard_1: process (addr1) 
begin
      addr1_tmp <= addr1;
--synthesis translate_off
      if (CONV_INTEGER(addr1) > mem_size-1) then
           addr1_tmp <= (others => '0');
      else 
           addr1_tmp <= addr1;
      end if;
--synthesis translate_on
end process;

memory_access_guard_2: process (addr2) 
begin
      addr2_tmp <= addr2;
--synthesis translate_off
      if (CONV_INTEGER(addr2) > mem_size-1) then
           addr2_tmp <= (others => '0');
      else 
           addr2_tmp <= addr2;
      end if;
--synthesis translate_on
end process;

memory_access_guard_3: process (addr3) 
begin
      addr3_tmp <= addr3;
--synthesis translate_off
      if (CONV_INTEGER(addr3) > mem_size-1) then
           addr3_tmp <= (others => '0');
      else 
           addr3_tmp <= addr3;
      end if;
--synthesis translate_on
end process;

p_rom_access: process (clk)  
begin 
    if (clk'event and clk = '1') then
        if (ce0 = '1') then 
            q0 <= mem0(CONV_INTEGER(addr0_tmp)); 
        end if;
        if (ce1 = '1') then 
            q1 <= mem0(CONV_INTEGER(addr1_tmp)); 
        end if;
        if (ce2 = '1') then 
            q2 <= mem1(CONV_INTEGER(addr2_tmp)); 
        end if;
        if (ce3 = '1') then 
            q3 <= mem1(CONV_INTEGER(addr3_tmp)); 
        end if;
    end if;
end process;

end rtl;


Library IEEE;
use IEEE.std_logic_1164.all;

entity dft_loop_flow1024_Loop_1_proc_sct_8 is
    generic (
        DataWidth : INTEGER := 32;
        AddressRange : INTEGER := 64;
        AddressWidth : INTEGER := 6);
    port (
        reset : IN STD_LOGIC;
        clk : IN STD_LOGIC;
        address0 : IN STD_LOGIC_VECTOR(AddressWidth - 1 DOWNTO 0);
        ce0 : IN STD_LOGIC;
        q0 : OUT STD_LOGIC_VECTOR(DataWidth - 1 DOWNTO 0);
        address1 : IN STD_LOGIC_VECTOR(AddressWidth - 1 DOWNTO 0);
        ce1 : IN STD_LOGIC;
        q1 : OUT STD_LOGIC_VECTOR(DataWidth - 1 DOWNTO 0);
        address2 : IN STD_LOGIC_VECTOR(AddressWidth - 1 DOWNTO 0);
        ce2 : IN STD_LOGIC;
        q2 : OUT STD_LOGIC_VECTOR(DataWidth - 1 DOWNTO 0);
        address3 : IN STD_LOGIC_VECTOR(AddressWidth - 1 DOWNTO 0);
        ce3 : IN STD_LOGIC;
        q3 : OUT STD_LOGIC_VECTOR(DataWidth - 1 DOWNTO 0));
end entity;

architecture arch of dft_loop_flow1024_Loop_1_proc_sct_8 is
    component dft_loop_flow1024_Loop_1_proc_sct_8_rom is
        port (
            clk : IN STD_LOGIC;
            addr0 : IN STD_LOGIC_VECTOR;
            ce0 : IN STD_LOGIC;
            q0 : OUT STD_LOGIC_VECTOR;
            addr1 : IN STD_LOGIC_VECTOR;
            ce1 : IN STD_LOGIC;
            q1 : OUT STD_LOGIC_VECTOR;
            addr2 : IN STD_LOGIC_VECTOR;
            ce2 : IN STD_LOGIC;
            q2 : OUT STD_LOGIC_VECTOR;
            addr3 : IN STD_LOGIC_VECTOR;
            ce3 : IN STD_LOGIC;
            q3 : OUT STD_LOGIC_VECTOR);
    end component;



begin
    dft_loop_flow1024_Loop_1_proc_sct_8_rom_U :  component dft_loop_flow1024_Loop_1_proc_sct_8_rom
    port map (
        clk => clk,
        addr0 => address0,
        ce0 => ce0,
        q0 => q0,
        addr1 => address1,
        ce1 => ce1,
        q1 => q1,
        addr2 => address2,
        ce2 => ce2,
        q2 => q2,
        addr3 => address3,
        ce3 => ce3,
        q3 => q3);

end architecture;


