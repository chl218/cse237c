// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module dft_loop_flow8_Loop_1_proc (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_continue,
        ap_idle,
        ap_ready,
        imag_arr8_1,
        imag_arr8_1_ap_vld,
        imag_arr16_2_address0,
        imag_arr16_2_ce0,
        imag_arr16_2_q0,
        imag_arr16_3_address0,
        imag_arr16_3_ce0,
        imag_arr16_3_q0,
        real_arr8_1,
        real_arr8_1_ap_vld,
        real_arr16_2_address0,
        real_arr16_2_ce0,
        real_arr16_2_q0,
        real_arr16_3_address0,
        real_arr16_3_ce0,
        real_arr16_3_q0,
        imag_arr8_0,
        imag_arr8_0_ap_vld,
        imag_arr16_0_address0,
        imag_arr16_0_ce0,
        imag_arr16_0_q0,
        imag_arr16_1_address0,
        imag_arr16_1_ce0,
        imag_arr16_1_q0,
        real_arr8_0,
        real_arr8_0_ap_vld,
        real_arr16_0_address0,
        real_arr16_0_ce0,
        real_arr16_0_q0,
        real_arr16_1_address0,
        real_arr16_1_ce0,
        real_arr16_1_q0,
        real_arr8_2,
        real_arr8_2_ap_vld,
        real_arr8_4,
        real_arr8_4_ap_vld,
        real_arr8_6,
        real_arr8_6_ap_vld,
        imag_arr8_2,
        imag_arr8_2_ap_vld,
        imag_arr8_4,
        imag_arr8_4_ap_vld,
        imag_arr8_6,
        imag_arr8_6_ap_vld,
        real_arr8_3,
        real_arr8_3_ap_vld,
        real_arr8_5,
        real_arr8_5_ap_vld,
        real_arr8_7,
        real_arr8_7_ap_vld,
        imag_arr8_3,
        imag_arr8_3_ap_vld,
        imag_arr8_5,
        imag_arr8_5_ap_vld,
        imag_arr8_7,
        imag_arr8_7_ap_vld
);

parameter    ap_const_logic_1 = 1'b1;
parameter    ap_const_logic_0 = 1'b0;
parameter    ap_ST_st1_fsm_0 = 3'b1;
parameter    ap_ST_pp0_stg0_fsm_1 = 3'b10;
parameter    ap_ST_st9_fsm_2 = 3'b100;
parameter    ap_const_lv32_0 = 32'b00000000000000000000000000000000;
parameter    ap_const_lv1_1 = 1'b1;
parameter    ap_true = 1'b1;
parameter    ap_const_lv32_1 = 32'b1;
parameter    ap_const_lv1_0 = 1'b0;
parameter    ap_const_lv5_0 = 5'b00000;
parameter    ap_const_lv4_0 = 4'b0000;
parameter    ap_const_lv3_4 = 3'b100;
parameter    ap_const_lv3_2 = 3'b10;
parameter    ap_const_lv3_0 = 3'b000;
parameter    ap_const_lv3_5 = 3'b101;
parameter    ap_const_lv3_3 = 3'b11;
parameter    ap_const_lv3_1 = 3'b1;
parameter    ap_const_lv4_8 = 4'b1000;
parameter    ap_const_lv32_2 = 32'b10;
parameter    ap_const_lv32_4 = 32'b100;
parameter    ap_const_lv32_3 = 32'b11;
parameter    ap_const_lv4_2 = 4'b10;
parameter    ap_const_lv5_4 = 5'b100;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
input   ap_continue;
output   ap_idle;
output   ap_ready;
output  [31:0] imag_arr8_1;
output   imag_arr8_1_ap_vld;
output  [1:0] imag_arr16_2_address0;
output   imag_arr16_2_ce0;
input  [31:0] imag_arr16_2_q0;
output  [1:0] imag_arr16_3_address0;
output   imag_arr16_3_ce0;
input  [31:0] imag_arr16_3_q0;
output  [31:0] real_arr8_1;
output   real_arr8_1_ap_vld;
output  [1:0] real_arr16_2_address0;
output   real_arr16_2_ce0;
input  [31:0] real_arr16_2_q0;
output  [1:0] real_arr16_3_address0;
output   real_arr16_3_ce0;
input  [31:0] real_arr16_3_q0;
output  [31:0] imag_arr8_0;
output   imag_arr8_0_ap_vld;
output  [1:0] imag_arr16_0_address0;
output   imag_arr16_0_ce0;
input  [31:0] imag_arr16_0_q0;
output  [1:0] imag_arr16_1_address0;
output   imag_arr16_1_ce0;
input  [31:0] imag_arr16_1_q0;
output  [31:0] real_arr8_0;
output   real_arr8_0_ap_vld;
output  [1:0] real_arr16_0_address0;
output   real_arr16_0_ce0;
input  [31:0] real_arr16_0_q0;
output  [1:0] real_arr16_1_address0;
output   real_arr16_1_ce0;
input  [31:0] real_arr16_1_q0;
output  [31:0] real_arr8_2;
output   real_arr8_2_ap_vld;
output  [31:0] real_arr8_4;
output   real_arr8_4_ap_vld;
output  [31:0] real_arr8_6;
output   real_arr8_6_ap_vld;
output  [31:0] imag_arr8_2;
output   imag_arr8_2_ap_vld;
output  [31:0] imag_arr8_4;
output   imag_arr8_4_ap_vld;
output  [31:0] imag_arr8_6;
output   imag_arr8_6_ap_vld;
output  [31:0] real_arr8_3;
output   real_arr8_3_ap_vld;
output  [31:0] real_arr8_5;
output   real_arr8_5_ap_vld;
output  [31:0] real_arr8_7;
output   real_arr8_7_ap_vld;
output  [31:0] imag_arr8_3;
output   imag_arr8_3_ap_vld;
output  [31:0] imag_arr8_5;
output   imag_arr8_5_ap_vld;
output  [31:0] imag_arr8_7;
output   imag_arr8_7_ap_vld;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg imag_arr8_1_ap_vld;
reg imag_arr16_2_ce0;
reg imag_arr16_3_ce0;
reg real_arr8_1_ap_vld;
reg real_arr16_2_ce0;
reg real_arr16_3_ce0;
reg imag_arr8_0_ap_vld;
reg imag_arr16_0_ce0;
reg imag_arr16_1_ce0;
reg real_arr8_0_ap_vld;
reg real_arr16_0_ce0;
reg real_arr16_1_ce0;
reg real_arr8_2_ap_vld;
reg real_arr8_4_ap_vld;
reg real_arr8_6_ap_vld;
reg imag_arr8_2_ap_vld;
reg imag_arr8_4_ap_vld;
reg imag_arr8_6_ap_vld;
reg real_arr8_3_ap_vld;
reg real_arr8_5_ap_vld;
reg real_arr8_7_ap_vld;
reg imag_arr8_3_ap_vld;
reg imag_arr8_5_ap_vld;
reg imag_arr8_7_ap_vld;
reg    ap_done_reg = 1'b0;
(* fsm_encoding = "none" *) reg   [2:0] ap_CS_fsm = 3'b1;
reg    ap_sig_cseq_ST_st1_fsm_0;
reg    ap_sig_bdd_22;
reg   [4:0] p_078_0_i_i_reg_196;
reg   [3:0] p_04_0_i_i_reg_207;
wire   [0:0] exitcond_i_i_fu_234_p2;
reg    ap_sig_cseq_ST_pp0_stg0_fsm_1;
reg    ap_sig_bdd_120;
reg    ap_reg_ppiten_pp0_it0 = 1'b0;
reg    ap_reg_ppiten_pp0_it1 = 1'b0;
reg    ap_reg_ppiten_pp0_it2 = 1'b0;
reg    ap_reg_ppiten_pp0_it3 = 1'b0;
reg    ap_reg_ppiten_pp0_it4 = 1'b0;
reg    ap_reg_ppiten_pp0_it5 = 1'b0;
reg    ap_reg_ppiten_pp0_it6 = 1'b0;
wire   [2:0] tmp_1_fu_276_p1;
reg   [2:0] tmp_1_reg_408;
reg   [2:0] ap_reg_ppstg_tmp_1_reg_408_pp0_it1;
reg   [2:0] ap_reg_ppstg_tmp_1_reg_408_pp0_it2;
reg   [2:0] ap_reg_ppstg_tmp_1_reg_408_pp0_it3;
reg   [2:0] ap_reg_ppstg_tmp_1_reg_408_pp0_it4;
reg   [2:0] ap_reg_ppstg_tmp_1_reg_408_pp0_it5;
wire   [2:0] i_V_i_03_t_fu_280_p2;
reg   [2:0] i_V_i_03_t_reg_432;
reg   [2:0] ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it1;
reg   [2:0] ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it2;
reg   [2:0] ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it3;
reg   [2:0] ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it4;
reg   [2:0] ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5;
wire   [3:0] i_V_i_1_fu_286_p2;
wire   [4:0] j_V_i_1_fu_292_p2;
reg   [31:0] real_arr16_0_load_reg_456;
reg   [31:0] real_arr16_1_load_reg_461;
reg   [31:0] imag_arr16_0_load_reg_466;
reg   [31:0] imag_arr16_1_load_reg_471;
reg   [31:0] real_arr16_2_load_reg_476;
reg   [31:0] real_arr16_3_load_reg_481;
reg   [31:0] imag_arr16_2_load_reg_486;
reg   [31:0] imag_arr16_3_load_reg_491;
reg    ap_sig_bdd_234;
wire   [63:0] newIndex1_fu_250_p1;
wire   [63:0] newIndex3_fu_268_p1;
wire   [31:0] grp_fu_218_p2;
wire   [31:0] grp_fu_222_p2;
wire   [31:0] grp_fu_226_p2;
wire   [31:0] grp_fu_230_p2;
wire   [2:0] newIndex_fu_240_p4;
wire   [1:0] newIndex2_fu_258_p4;
wire    grp_fu_218_ce;
wire    grp_fu_222_ce;
wire    grp_fu_226_ce;
wire    grp_fu_230_ce;
reg    ap_sig_cseq_ST_st9_fsm_2;
reg    ap_sig_bdd_335;
reg   [2:0] ap_NS_fsm;


dft_fadd_32ns_32ns_32_5_full_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 5 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
dft_fadd_32ns_32ns_32_5_full_dsp_U386(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( real_arr16_0_load_reg_456 ),
    .din1( real_arr16_1_load_reg_461 ),
    .ce( grp_fu_218_ce ),
    .dout( grp_fu_218_p2 )
);

dft_fadd_32ns_32ns_32_5_full_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 5 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
dft_fadd_32ns_32ns_32_5_full_dsp_U387(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( imag_arr16_0_load_reg_466 ),
    .din1( imag_arr16_1_load_reg_471 ),
    .ce( grp_fu_222_ce ),
    .dout( grp_fu_222_p2 )
);

dft_fadd_32ns_32ns_32_5_full_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 5 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
dft_fadd_32ns_32ns_32_5_full_dsp_U388(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( real_arr16_2_load_reg_476 ),
    .din1( real_arr16_3_load_reg_481 ),
    .ce( grp_fu_226_ce ),
    .dout( grp_fu_226_p2 )
);

dft_fadd_32ns_32ns_32_5_full_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 5 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
dft_fadd_32ns_32ns_32_5_full_dsp_U389(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( imag_arr16_2_load_reg_486 ),
    .din1( imag_arr16_3_load_reg_491 ),
    .ce( grp_fu_230_ce ),
    .dout( grp_fu_230_p2 )
);



always @ (posedge ap_clk) begin : ap_ret_ap_CS_fsm
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_st1_fsm_0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_done_reg
    if (ap_rst == 1'b1) begin
        ap_done_reg <= ap_const_logic_0;
    end else begin
        if ((ap_const_logic_1 == ap_continue)) begin
            ap_done_reg <= ap_const_logic_0;
        end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_2)) begin
            ap_done_reg <= ap_const_logic_1;
        end
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_reg_ppiten_pp0_it0
    if (ap_rst == 1'b1) begin
        ap_reg_ppiten_pp0_it0 <= ap_const_logic_0;
    end else begin
        if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & ~(exitcond_i_i_fu_234_p2 == ap_const_lv1_0))) begin
            ap_reg_ppiten_pp0_it0 <= ap_const_logic_0;
        end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~ap_sig_bdd_234)) begin
            ap_reg_ppiten_pp0_it0 <= ap_const_logic_1;
        end
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_reg_ppiten_pp0_it1
    if (ap_rst == 1'b1) begin
        ap_reg_ppiten_pp0_it1 <= ap_const_logic_0;
    end else begin
        if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (exitcond_i_i_fu_234_p2 == ap_const_lv1_0))) begin
            ap_reg_ppiten_pp0_it1 <= ap_const_logic_1;
        end else if ((((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~ap_sig_bdd_234) | ((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & ~(exitcond_i_i_fu_234_p2 == ap_const_lv1_0)))) begin
            ap_reg_ppiten_pp0_it1 <= ap_const_logic_0;
        end
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_reg_ppiten_pp0_it2
    if (ap_rst == 1'b1) begin
        ap_reg_ppiten_pp0_it2 <= ap_const_logic_0;
    end else begin
        ap_reg_ppiten_pp0_it2 <= ap_reg_ppiten_pp0_it1;
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_reg_ppiten_pp0_it3
    if (ap_rst == 1'b1) begin
        ap_reg_ppiten_pp0_it3 <= ap_const_logic_0;
    end else begin
        ap_reg_ppiten_pp0_it3 <= ap_reg_ppiten_pp0_it2;
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_reg_ppiten_pp0_it4
    if (ap_rst == 1'b1) begin
        ap_reg_ppiten_pp0_it4 <= ap_const_logic_0;
    end else begin
        ap_reg_ppiten_pp0_it4 <= ap_reg_ppiten_pp0_it3;
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_reg_ppiten_pp0_it5
    if (ap_rst == 1'b1) begin
        ap_reg_ppiten_pp0_it5 <= ap_const_logic_0;
    end else begin
        ap_reg_ppiten_pp0_it5 <= ap_reg_ppiten_pp0_it4;
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_reg_ppiten_pp0_it6
    if (ap_rst == 1'b1) begin
        ap_reg_ppiten_pp0_it6 <= ap_const_logic_0;
    end else begin
        ap_reg_ppiten_pp0_it6 <= ap_reg_ppiten_pp0_it5;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & (exitcond_i_i_fu_234_p2 == ap_const_lv1_0))) begin
        p_04_0_i_i_reg_207 <= i_V_i_1_fu_286_p2;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~ap_sig_bdd_234)) begin
        p_04_0_i_i_reg_207 <= ap_const_lv4_0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & (exitcond_i_i_fu_234_p2 == ap_const_lv1_0))) begin
        p_078_0_i_i_reg_196 <= j_V_i_1_fu_292_p2;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~ap_sig_bdd_234)) begin
        p_078_0_i_i_reg_196 <= ap_const_lv5_0;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1)) begin
        ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it1[2 : 1] <= i_V_i_03_t_reg_432[2 : 1];
        ap_reg_ppstg_tmp_1_reg_408_pp0_it1 <= tmp_1_reg_408;
        imag_arr16_0_load_reg_466 <= imag_arr16_0_q0;
        imag_arr16_1_load_reg_471 <= imag_arr16_1_q0;
        imag_arr16_2_load_reg_486 <= imag_arr16_2_q0;
        imag_arr16_3_load_reg_491 <= imag_arr16_3_q0;
        real_arr16_0_load_reg_456 <= real_arr16_0_q0;
        real_arr16_1_load_reg_461 <= real_arr16_1_q0;
        real_arr16_2_load_reg_476 <= real_arr16_2_q0;
        real_arr16_3_load_reg_481 <= real_arr16_3_q0;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_true == ap_true)) begin
        ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it2[2 : 1] <= ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it1[2 : 1];
        ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it3[2 : 1] <= ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it2[2 : 1];
        ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it4[2 : 1] <= ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it3[2 : 1];
        ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5[2 : 1] <= ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it4[2 : 1];
        ap_reg_ppstg_tmp_1_reg_408_pp0_it2 <= ap_reg_ppstg_tmp_1_reg_408_pp0_it1;
        ap_reg_ppstg_tmp_1_reg_408_pp0_it3 <= ap_reg_ppstg_tmp_1_reg_408_pp0_it2;
        ap_reg_ppstg_tmp_1_reg_408_pp0_it4 <= ap_reg_ppstg_tmp_1_reg_408_pp0_it3;
        ap_reg_ppstg_tmp_1_reg_408_pp0_it5 <= ap_reg_ppstg_tmp_1_reg_408_pp0_it4;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (exitcond_i_i_fu_234_p2 == ap_const_lv1_0))) begin
        i_V_i_03_t_reg_432[2 : 1] <= i_V_i_03_t_fu_280_p2[2 : 1];
        tmp_1_reg_408 <= tmp_1_fu_276_p1;
    end
end

always @ (ap_done_reg or ap_sig_cseq_ST_st9_fsm_2) begin
    if (((ap_const_logic_1 == ap_done_reg) | (ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_2))) begin
        ap_done = ap_const_logic_1;
    end else begin
        ap_done = ap_const_logic_0;
    end
end

always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if ((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0))) begin
        ap_idle = ap_const_logic_1;
    end else begin
        ap_idle = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st9_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st9_fsm_2)) begin
        ap_ready = ap_const_logic_1;
    end else begin
        ap_ready = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_120) begin
    if (ap_sig_bdd_120) begin
        ap_sig_cseq_ST_pp0_stg0_fsm_1 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_pp0_stg0_fsm_1 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_22) begin
    if (ap_sig_bdd_22) begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_335) begin
    if (ap_sig_bdd_335) begin
        ap_sig_cseq_ST_st9_fsm_2 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st9_fsm_2 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it0) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it0))) begin
        imag_arr16_0_ce0 = ap_const_logic_1;
    end else begin
        imag_arr16_0_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it0) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it0))) begin
        imag_arr16_1_ce0 = ap_const_logic_1;
    end else begin
        imag_arr16_1_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it0) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it0))) begin
        imag_arr16_2_ce0 = ap_const_logic_1;
    end else begin
        imag_arr16_2_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it0) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it0))) begin
        imag_arr16_3_ce0 = ap_const_logic_1;
    end else begin
        imag_arr16_3_ce0 = ap_const_logic_0;
    end
end

always @ (ap_reg_ppiten_pp0_it6 or ap_reg_ppstg_tmp_1_reg_408_pp0_it5) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it6) & (ap_reg_ppstg_tmp_1_reg_408_pp0_it5 == ap_const_lv3_0))) begin
        imag_arr8_0_ap_vld = ap_const_logic_1;
    end else begin
        imag_arr8_0_ap_vld = ap_const_logic_0;
    end
end

always @ (ap_reg_ppiten_pp0_it6 or ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it6) & (ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5 == ap_const_lv3_1))) begin
        imag_arr8_1_ap_vld = ap_const_logic_1;
    end else begin
        imag_arr8_1_ap_vld = ap_const_logic_0;
    end
end

always @ (ap_reg_ppiten_pp0_it6 or ap_reg_ppstg_tmp_1_reg_408_pp0_it5) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it6) & (ap_reg_ppstg_tmp_1_reg_408_pp0_it5 == ap_const_lv3_2))) begin
        imag_arr8_2_ap_vld = ap_const_logic_1;
    end else begin
        imag_arr8_2_ap_vld = ap_const_logic_0;
    end
end

always @ (ap_reg_ppiten_pp0_it6 or ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it6) & (ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5 == ap_const_lv3_3))) begin
        imag_arr8_3_ap_vld = ap_const_logic_1;
    end else begin
        imag_arr8_3_ap_vld = ap_const_logic_0;
    end
end

always @ (ap_reg_ppiten_pp0_it6 or ap_reg_ppstg_tmp_1_reg_408_pp0_it5) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it6) & (ap_reg_ppstg_tmp_1_reg_408_pp0_it5 == ap_const_lv3_4))) begin
        imag_arr8_4_ap_vld = ap_const_logic_1;
    end else begin
        imag_arr8_4_ap_vld = ap_const_logic_0;
    end
end

always @ (ap_reg_ppiten_pp0_it6 or ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it6) & (ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5 == ap_const_lv3_5))) begin
        imag_arr8_5_ap_vld = ap_const_logic_1;
    end else begin
        imag_arr8_5_ap_vld = ap_const_logic_0;
    end
end

always @ (ap_reg_ppiten_pp0_it6 or ap_reg_ppstg_tmp_1_reg_408_pp0_it5) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it6) & ~(ap_reg_ppstg_tmp_1_reg_408_pp0_it5 == ap_const_lv3_4) & ~(ap_reg_ppstg_tmp_1_reg_408_pp0_it5 == ap_const_lv3_2) & ~(ap_reg_ppstg_tmp_1_reg_408_pp0_it5 == ap_const_lv3_0))) begin
        imag_arr8_6_ap_vld = ap_const_logic_1;
    end else begin
        imag_arr8_6_ap_vld = ap_const_logic_0;
    end
end

always @ (ap_reg_ppiten_pp0_it6 or ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it6) & ~(ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5 == ap_const_lv3_5) & ~(ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5 == ap_const_lv3_3) & ~(ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5 == ap_const_lv3_1))) begin
        imag_arr8_7_ap_vld = ap_const_logic_1;
    end else begin
        imag_arr8_7_ap_vld = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it0) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it0))) begin
        real_arr16_0_ce0 = ap_const_logic_1;
    end else begin
        real_arr16_0_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it0) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it0))) begin
        real_arr16_1_ce0 = ap_const_logic_1;
    end else begin
        real_arr16_1_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it0) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it0))) begin
        real_arr16_2_ce0 = ap_const_logic_1;
    end else begin
        real_arr16_2_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it0) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it0))) begin
        real_arr16_3_ce0 = ap_const_logic_1;
    end else begin
        real_arr16_3_ce0 = ap_const_logic_0;
    end
end

always @ (ap_reg_ppiten_pp0_it6 or ap_reg_ppstg_tmp_1_reg_408_pp0_it5) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it6) & (ap_reg_ppstg_tmp_1_reg_408_pp0_it5 == ap_const_lv3_0))) begin
        real_arr8_0_ap_vld = ap_const_logic_1;
    end else begin
        real_arr8_0_ap_vld = ap_const_logic_0;
    end
end

always @ (ap_reg_ppiten_pp0_it6 or ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it6) & (ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5 == ap_const_lv3_1))) begin
        real_arr8_1_ap_vld = ap_const_logic_1;
    end else begin
        real_arr8_1_ap_vld = ap_const_logic_0;
    end
end

always @ (ap_reg_ppiten_pp0_it6 or ap_reg_ppstg_tmp_1_reg_408_pp0_it5) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it6) & (ap_reg_ppstg_tmp_1_reg_408_pp0_it5 == ap_const_lv3_2))) begin
        real_arr8_2_ap_vld = ap_const_logic_1;
    end else begin
        real_arr8_2_ap_vld = ap_const_logic_0;
    end
end

always @ (ap_reg_ppiten_pp0_it6 or ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it6) & (ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5 == ap_const_lv3_3))) begin
        real_arr8_3_ap_vld = ap_const_logic_1;
    end else begin
        real_arr8_3_ap_vld = ap_const_logic_0;
    end
end

always @ (ap_reg_ppiten_pp0_it6 or ap_reg_ppstg_tmp_1_reg_408_pp0_it5) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it6) & (ap_reg_ppstg_tmp_1_reg_408_pp0_it5 == ap_const_lv3_4))) begin
        real_arr8_4_ap_vld = ap_const_logic_1;
    end else begin
        real_arr8_4_ap_vld = ap_const_logic_0;
    end
end

always @ (ap_reg_ppiten_pp0_it6 or ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it6) & (ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5 == ap_const_lv3_5))) begin
        real_arr8_5_ap_vld = ap_const_logic_1;
    end else begin
        real_arr8_5_ap_vld = ap_const_logic_0;
    end
end

always @ (ap_reg_ppiten_pp0_it6 or ap_reg_ppstg_tmp_1_reg_408_pp0_it5) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it6) & ~(ap_reg_ppstg_tmp_1_reg_408_pp0_it5 == ap_const_lv3_4) & ~(ap_reg_ppstg_tmp_1_reg_408_pp0_it5 == ap_const_lv3_2) & ~(ap_reg_ppstg_tmp_1_reg_408_pp0_it5 == ap_const_lv3_0))) begin
        real_arr8_6_ap_vld = ap_const_logic_1;
    end else begin
        real_arr8_6_ap_vld = ap_const_logic_0;
    end
end

always @ (ap_reg_ppiten_pp0_it6 or ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it6) & ~(ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5 == ap_const_lv3_5) & ~(ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5 == ap_const_lv3_3) & ~(ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5 == ap_const_lv3_1))) begin
        real_arr8_7_ap_vld = ap_const_logic_1;
    end else begin
        real_arr8_7_ap_vld = ap_const_logic_0;
    end
end
always @ (ap_CS_fsm or exitcond_i_i_fu_234_p2 or ap_reg_ppiten_pp0_it0 or ap_reg_ppiten_pp0_it1 or ap_reg_ppiten_pp0_it5 or ap_reg_ppiten_pp0_it6 or ap_sig_bdd_234) begin
    case (ap_CS_fsm)
        ap_ST_st1_fsm_0 : 
        begin
            if (~ap_sig_bdd_234) begin
                ap_NS_fsm = ap_ST_pp0_stg0_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end
        end
        ap_ST_pp0_stg0_fsm_1 : 
        begin
            if ((~((ap_const_logic_1 == ap_reg_ppiten_pp0_it6) & ~(ap_const_logic_1 == ap_reg_ppiten_pp0_it5)) & ~((ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & ~(exitcond_i_i_fu_234_p2 == ap_const_lv1_0) & ~(ap_const_logic_1 == ap_reg_ppiten_pp0_it1)))) begin
                ap_NS_fsm = ap_ST_pp0_stg0_fsm_1;
            end else if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & ~(exitcond_i_i_fu_234_p2 == ap_const_lv1_0) & ~(ap_const_logic_1 == ap_reg_ppiten_pp0_it1))) begin
                ap_NS_fsm = ap_ST_st9_fsm_2;
            end else begin
                ap_NS_fsm = ap_ST_st9_fsm_2;
            end
        end
        ap_ST_st9_fsm_2 : 
        begin
            ap_NS_fsm = ap_ST_st1_fsm_0;
        end
        default : 
        begin
            ap_NS_fsm = 'bx;
        end
    endcase
end



always @ (ap_CS_fsm) begin
    ap_sig_bdd_120 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_1]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_22 = (ap_CS_fsm[ap_const_lv32_0] == ap_const_lv1_1);
end


always @ (ap_start or ap_done_reg) begin
    ap_sig_bdd_234 = ((ap_start == ap_const_logic_0) | (ap_done_reg == ap_const_logic_1));
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_335 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_2]);
end

assign exitcond_i_i_fu_234_p2 = (p_04_0_i_i_reg_207 == ap_const_lv4_8? 1'b1: 1'b0);

assign grp_fu_218_ce = ap_const_logic_1;

assign grp_fu_222_ce = ap_const_logic_1;

assign grp_fu_226_ce = ap_const_logic_1;

assign grp_fu_230_ce = ap_const_logic_1;

assign i_V_i_03_t_fu_280_p2 = (tmp_1_fu_276_p1 | ap_const_lv3_1);

assign i_V_i_1_fu_286_p2 = (p_04_0_i_i_reg_207 + ap_const_lv4_2);

assign imag_arr16_0_address0 = newIndex1_fu_250_p1;

assign imag_arr16_1_address0 = newIndex3_fu_268_p1;

assign imag_arr16_2_address0 = newIndex1_fu_250_p1;

assign imag_arr16_3_address0 = newIndex3_fu_268_p1;

assign imag_arr8_0 = grp_fu_222_p2;

assign imag_arr8_1 = grp_fu_230_p2;

assign imag_arr8_2 = grp_fu_222_p2;

assign imag_arr8_3 = grp_fu_230_p2;

assign imag_arr8_4 = grp_fu_222_p2;

assign imag_arr8_5 = grp_fu_230_p2;

assign imag_arr8_6 = grp_fu_222_p2;

assign imag_arr8_7 = grp_fu_230_p2;

assign j_V_i_1_fu_292_p2 = (p_078_0_i_i_reg_196 + ap_const_lv5_4);

assign newIndex1_fu_250_p1 = newIndex_fu_240_p4;

assign newIndex2_fu_258_p4 = {{p_078_0_i_i_reg_196[ap_const_lv32_3 : ap_const_lv32_2]}};

assign newIndex3_fu_268_p1 = newIndex2_fu_258_p4;

assign newIndex_fu_240_p4 = {{p_078_0_i_i_reg_196[ap_const_lv32_4 : ap_const_lv32_2]}};

assign real_arr16_0_address0 = newIndex1_fu_250_p1;

assign real_arr16_1_address0 = newIndex3_fu_268_p1;

assign real_arr16_2_address0 = newIndex1_fu_250_p1;

assign real_arr16_3_address0 = newIndex3_fu_268_p1;

assign real_arr8_0 = grp_fu_218_p2;

assign real_arr8_1 = grp_fu_226_p2;

assign real_arr8_2 = grp_fu_218_p2;

assign real_arr8_3 = grp_fu_226_p2;

assign real_arr8_4 = grp_fu_218_p2;

assign real_arr8_5 = grp_fu_226_p2;

assign real_arr8_6 = grp_fu_218_p2;

assign real_arr8_7 = grp_fu_226_p2;

assign tmp_1_fu_276_p1 = p_04_0_i_i_reg_207[2:0];
always @ (posedge ap_clk) begin
    i_V_i_03_t_reg_432[0] <= 1'b1;
    ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it1[0] <= 1'b1;
    ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it2[0] <= 1'b1;
    ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it3[0] <= 1'b1;
    ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it4[0] <= 1'b1;
    ap_reg_ppstg_i_V_i_03_t_reg_432_pp0_it5[0] <= 1'b1;
end



endmodule //dft_loop_flow8_Loop_1_proc

