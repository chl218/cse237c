// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

#ifndef _dft_loop_flow64_HH_
#define _dft_loop_flow64_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "dft_loop_flow64_Loop_1_proc.h"

namespace ap_rtl {

struct dft_loop_flow64 : public sc_module {
    // Port declarations 166
    sc_out< sc_lv<5> > real_arr128_0_address0;
    sc_out< sc_logic > real_arr128_0_ce0;
    sc_out< sc_lv<32> > real_arr128_0_d0;
    sc_in< sc_lv<32> > real_arr128_0_q0;
    sc_out< sc_logic > real_arr128_0_we0;
    sc_out< sc_lv<5> > real_arr128_0_address1;
    sc_out< sc_logic > real_arr128_0_ce1;
    sc_out< sc_lv<32> > real_arr128_0_d1;
    sc_in< sc_lv<32> > real_arr128_0_q1;
    sc_out< sc_logic > real_arr128_0_we1;
    sc_out< sc_lv<5> > real_arr128_1_address0;
    sc_out< sc_logic > real_arr128_1_ce0;
    sc_out< sc_lv<32> > real_arr128_1_d0;
    sc_in< sc_lv<32> > real_arr128_1_q0;
    sc_out< sc_logic > real_arr128_1_we0;
    sc_out< sc_lv<5> > real_arr128_1_address1;
    sc_out< sc_logic > real_arr128_1_ce1;
    sc_out< sc_lv<32> > real_arr128_1_d1;
    sc_in< sc_lv<32> > real_arr128_1_q1;
    sc_out< sc_logic > real_arr128_1_we1;
    sc_out< sc_lv<4> > real_arr64_0_address0;
    sc_out< sc_logic > real_arr64_0_ce0;
    sc_out< sc_lv<32> > real_arr64_0_d0;
    sc_in< sc_lv<32> > real_arr64_0_q0;
    sc_out< sc_logic > real_arr64_0_we0;
    sc_out< sc_lv<4> > real_arr64_0_address1;
    sc_out< sc_logic > real_arr64_0_ce1;
    sc_out< sc_lv<32> > real_arr64_0_d1;
    sc_in< sc_lv<32> > real_arr64_0_q1;
    sc_out< sc_logic > real_arr64_0_we1;
    sc_out< sc_lv<5> > imag_arr128_0_address0;
    sc_out< sc_logic > imag_arr128_0_ce0;
    sc_out< sc_lv<32> > imag_arr128_0_d0;
    sc_in< sc_lv<32> > imag_arr128_0_q0;
    sc_out< sc_logic > imag_arr128_0_we0;
    sc_out< sc_lv<5> > imag_arr128_0_address1;
    sc_out< sc_logic > imag_arr128_0_ce1;
    sc_out< sc_lv<32> > imag_arr128_0_d1;
    sc_in< sc_lv<32> > imag_arr128_0_q1;
    sc_out< sc_logic > imag_arr128_0_we1;
    sc_out< sc_lv<5> > imag_arr128_1_address0;
    sc_out< sc_logic > imag_arr128_1_ce0;
    sc_out< sc_lv<32> > imag_arr128_1_d0;
    sc_in< sc_lv<32> > imag_arr128_1_q0;
    sc_out< sc_logic > imag_arr128_1_we0;
    sc_out< sc_lv<5> > imag_arr128_1_address1;
    sc_out< sc_logic > imag_arr128_1_ce1;
    sc_out< sc_lv<32> > imag_arr128_1_d1;
    sc_in< sc_lv<32> > imag_arr128_1_q1;
    sc_out< sc_logic > imag_arr128_1_we1;
    sc_out< sc_lv<4> > imag_arr64_0_address0;
    sc_out< sc_logic > imag_arr64_0_ce0;
    sc_out< sc_lv<32> > imag_arr64_0_d0;
    sc_in< sc_lv<32> > imag_arr64_0_q0;
    sc_out< sc_logic > imag_arr64_0_we0;
    sc_out< sc_lv<4> > imag_arr64_0_address1;
    sc_out< sc_logic > imag_arr64_0_ce1;
    sc_out< sc_lv<32> > imag_arr64_0_d1;
    sc_in< sc_lv<32> > imag_arr64_0_q1;
    sc_out< sc_logic > imag_arr64_0_we1;
    sc_out< sc_lv<5> > real_arr128_2_address0;
    sc_out< sc_logic > real_arr128_2_ce0;
    sc_out< sc_lv<32> > real_arr128_2_d0;
    sc_in< sc_lv<32> > real_arr128_2_q0;
    sc_out< sc_logic > real_arr128_2_we0;
    sc_out< sc_lv<5> > real_arr128_2_address1;
    sc_out< sc_logic > real_arr128_2_ce1;
    sc_out< sc_lv<32> > real_arr128_2_d1;
    sc_in< sc_lv<32> > real_arr128_2_q1;
    sc_out< sc_logic > real_arr128_2_we1;
    sc_out< sc_lv<5> > real_arr128_3_address0;
    sc_out< sc_logic > real_arr128_3_ce0;
    sc_out< sc_lv<32> > real_arr128_3_d0;
    sc_in< sc_lv<32> > real_arr128_3_q0;
    sc_out< sc_logic > real_arr128_3_we0;
    sc_out< sc_lv<5> > real_arr128_3_address1;
    sc_out< sc_logic > real_arr128_3_ce1;
    sc_out< sc_lv<32> > real_arr128_3_d1;
    sc_in< sc_lv<32> > real_arr128_3_q1;
    sc_out< sc_logic > real_arr128_3_we1;
    sc_out< sc_lv<4> > real_arr64_1_address0;
    sc_out< sc_logic > real_arr64_1_ce0;
    sc_out< sc_lv<32> > real_arr64_1_d0;
    sc_in< sc_lv<32> > real_arr64_1_q0;
    sc_out< sc_logic > real_arr64_1_we0;
    sc_out< sc_lv<4> > real_arr64_1_address1;
    sc_out< sc_logic > real_arr64_1_ce1;
    sc_out< sc_lv<32> > real_arr64_1_d1;
    sc_in< sc_lv<32> > real_arr64_1_q1;
    sc_out< sc_logic > real_arr64_1_we1;
    sc_out< sc_lv<5> > imag_arr128_2_address0;
    sc_out< sc_logic > imag_arr128_2_ce0;
    sc_out< sc_lv<32> > imag_arr128_2_d0;
    sc_in< sc_lv<32> > imag_arr128_2_q0;
    sc_out< sc_logic > imag_arr128_2_we0;
    sc_out< sc_lv<5> > imag_arr128_2_address1;
    sc_out< sc_logic > imag_arr128_2_ce1;
    sc_out< sc_lv<32> > imag_arr128_2_d1;
    sc_in< sc_lv<32> > imag_arr128_2_q1;
    sc_out< sc_logic > imag_arr128_2_we1;
    sc_out< sc_lv<5> > imag_arr128_3_address0;
    sc_out< sc_logic > imag_arr128_3_ce0;
    sc_out< sc_lv<32> > imag_arr128_3_d0;
    sc_in< sc_lv<32> > imag_arr128_3_q0;
    sc_out< sc_logic > imag_arr128_3_we0;
    sc_out< sc_lv<5> > imag_arr128_3_address1;
    sc_out< sc_logic > imag_arr128_3_ce1;
    sc_out< sc_lv<32> > imag_arr128_3_d1;
    sc_in< sc_lv<32> > imag_arr128_3_q1;
    sc_out< sc_logic > imag_arr128_3_we1;
    sc_out< sc_lv<4> > imag_arr64_1_address0;
    sc_out< sc_logic > imag_arr64_1_ce0;
    sc_out< sc_lv<32> > imag_arr64_1_d0;
    sc_in< sc_lv<32> > imag_arr64_1_q0;
    sc_out< sc_logic > imag_arr64_1_we0;
    sc_out< sc_lv<4> > imag_arr64_1_address1;
    sc_out< sc_logic > imag_arr64_1_ce1;
    sc_out< sc_lv<32> > imag_arr64_1_d1;
    sc_in< sc_lv<32> > imag_arr64_1_q1;
    sc_out< sc_logic > imag_arr64_1_we1;
    sc_out< sc_lv<4> > real_arr64_2_address0;
    sc_out< sc_logic > real_arr64_2_ce0;
    sc_out< sc_lv<32> > real_arr64_2_d0;
    sc_in< sc_lv<32> > real_arr64_2_q0;
    sc_out< sc_logic > real_arr64_2_we0;
    sc_out< sc_lv<4> > real_arr64_2_address1;
    sc_out< sc_logic > real_arr64_2_ce1;
    sc_out< sc_lv<32> > real_arr64_2_d1;
    sc_in< sc_lv<32> > real_arr64_2_q1;
    sc_out< sc_logic > real_arr64_2_we1;
    sc_out< sc_lv<4> > imag_arr64_2_address0;
    sc_out< sc_logic > imag_arr64_2_ce0;
    sc_out< sc_lv<32> > imag_arr64_2_d0;
    sc_in< sc_lv<32> > imag_arr64_2_q0;
    sc_out< sc_logic > imag_arr64_2_we0;
    sc_out< sc_lv<4> > imag_arr64_2_address1;
    sc_out< sc_logic > imag_arr64_2_ce1;
    sc_out< sc_lv<32> > imag_arr64_2_d1;
    sc_in< sc_lv<32> > imag_arr64_2_q1;
    sc_out< sc_logic > imag_arr64_2_we1;
    sc_out< sc_lv<4> > real_arr64_3_address0;
    sc_out< sc_logic > real_arr64_3_ce0;
    sc_out< sc_lv<32> > real_arr64_3_d0;
    sc_in< sc_lv<32> > real_arr64_3_q0;
    sc_out< sc_logic > real_arr64_3_we0;
    sc_out< sc_lv<4> > real_arr64_3_address1;
    sc_out< sc_logic > real_arr64_3_ce1;
    sc_out< sc_lv<32> > real_arr64_3_d1;
    sc_in< sc_lv<32> > real_arr64_3_q1;
    sc_out< sc_logic > real_arr64_3_we1;
    sc_out< sc_lv<4> > imag_arr64_3_address0;
    sc_out< sc_logic > imag_arr64_3_ce0;
    sc_out< sc_lv<32> > imag_arr64_3_d0;
    sc_in< sc_lv<32> > imag_arr64_3_q0;
    sc_out< sc_logic > imag_arr64_3_we0;
    sc_out< sc_lv<4> > imag_arr64_3_address1;
    sc_out< sc_logic > imag_arr64_3_ce1;
    sc_out< sc_lv<32> > imag_arr64_3_d1;
    sc_in< sc_lv<32> > imag_arr64_3_q1;
    sc_out< sc_logic > imag_arr64_3_we1;
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_out< sc_logic > ap_done;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;


    // Module declarations
    dft_loop_flow64(sc_module_name name);
    SC_HAS_PROCESS(dft_loop_flow64);

    ~dft_loop_flow64();

    sc_trace_file* mVcdFile;

    dft_loop_flow64_Loop_1_proc* dft_loop_flow64_Loop_1_proc_U0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_ap_start;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_ap_done;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_ap_continue;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_ap_idle;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_ap_ready;
    sc_signal< sc_lv<5> > dft_loop_flow64_Loop_1_proc_U0_real_arr128_0_address0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_real_arr128_0_ce0;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_real_arr128_0_q0;
    sc_signal< sc_lv<5> > dft_loop_flow64_Loop_1_proc_U0_real_arr128_0_address1;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_real_arr128_0_ce1;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_real_arr128_0_q1;
    sc_signal< sc_lv<5> > dft_loop_flow64_Loop_1_proc_U0_real_arr128_1_address0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_real_arr128_1_ce0;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_real_arr128_1_q0;
    sc_signal< sc_lv<5> > dft_loop_flow64_Loop_1_proc_U0_real_arr128_1_address1;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_real_arr128_1_ce1;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_real_arr128_1_q1;
    sc_signal< sc_lv<4> > dft_loop_flow64_Loop_1_proc_U0_real_arr64_0_address0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_real_arr64_0_ce0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_real_arr64_0_we0;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_real_arr64_0_d0;
    sc_signal< sc_lv<5> > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_0_address0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_0_ce0;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_0_q0;
    sc_signal< sc_lv<5> > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_0_address1;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_0_ce1;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_0_q1;
    sc_signal< sc_lv<5> > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_1_address0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_1_ce0;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_1_q0;
    sc_signal< sc_lv<5> > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_1_address1;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_1_ce1;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_1_q1;
    sc_signal< sc_lv<4> > dft_loop_flow64_Loop_1_proc_U0_imag_arr64_0_address0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_imag_arr64_0_ce0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_imag_arr64_0_we0;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_imag_arr64_0_d0;
    sc_signal< sc_lv<5> > dft_loop_flow64_Loop_1_proc_U0_real_arr128_2_address0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_real_arr128_2_ce0;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_real_arr128_2_q0;
    sc_signal< sc_lv<5> > dft_loop_flow64_Loop_1_proc_U0_real_arr128_2_address1;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_real_arr128_2_ce1;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_real_arr128_2_q1;
    sc_signal< sc_lv<5> > dft_loop_flow64_Loop_1_proc_U0_real_arr128_3_address0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_real_arr128_3_ce0;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_real_arr128_3_q0;
    sc_signal< sc_lv<5> > dft_loop_flow64_Loop_1_proc_U0_real_arr128_3_address1;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_real_arr128_3_ce1;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_real_arr128_3_q1;
    sc_signal< sc_lv<4> > dft_loop_flow64_Loop_1_proc_U0_real_arr64_1_address0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_real_arr64_1_ce0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_real_arr64_1_we0;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_real_arr64_1_d0;
    sc_signal< sc_lv<5> > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_2_address0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_2_ce0;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_2_q0;
    sc_signal< sc_lv<5> > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_2_address1;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_2_ce1;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_2_q1;
    sc_signal< sc_lv<5> > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_3_address0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_3_ce0;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_3_q0;
    sc_signal< sc_lv<5> > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_3_address1;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_3_ce1;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_imag_arr128_3_q1;
    sc_signal< sc_lv<4> > dft_loop_flow64_Loop_1_proc_U0_imag_arr64_1_address0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_imag_arr64_1_ce0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_imag_arr64_1_we0;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_imag_arr64_1_d0;
    sc_signal< sc_lv<4> > dft_loop_flow64_Loop_1_proc_U0_real_arr64_2_address0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_real_arr64_2_ce0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_real_arr64_2_we0;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_real_arr64_2_d0;
    sc_signal< sc_lv<4> > dft_loop_flow64_Loop_1_proc_U0_imag_arr64_2_address0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_imag_arr64_2_ce0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_imag_arr64_2_we0;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_imag_arr64_2_d0;
    sc_signal< sc_lv<4> > dft_loop_flow64_Loop_1_proc_U0_real_arr64_3_address0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_real_arr64_3_ce0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_real_arr64_3_we0;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_real_arr64_3_d0;
    sc_signal< sc_lv<4> > dft_loop_flow64_Loop_1_proc_U0_imag_arr64_3_address0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_imag_arr64_3_ce0;
    sc_signal< sc_logic > dft_loop_flow64_Loop_1_proc_U0_imag_arr64_3_we0;
    sc_signal< sc_lv<32> > dft_loop_flow64_Loop_1_proc_U0_imag_arr64_3_d0;
    sc_signal< sc_logic > ap_sig_hs_continue;
    sc_signal< sc_logic > ap_reg_procdone_dft_loop_flow64_Loop_1_proc_U0;
    sc_signal< sc_logic > ap_sig_hs_done;
    sc_signal< sc_logic > ap_CS;
    sc_signal< sc_logic > ap_sig_top_allready;
    static const sc_lv<5> ap_const_lv5_0;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<32> ap_const_lv32_0;
    static const sc_lv<4> ap_const_lv4_0;
    static const sc_logic ap_const_logic_1;
    static const bool ap_true;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_ap_done();
    void thread_ap_idle();
    void thread_ap_ready();
    void thread_ap_sig_hs_continue();
    void thread_ap_sig_hs_done();
    void thread_ap_sig_top_allready();
    void thread_dft_loop_flow64_Loop_1_proc_U0_ap_continue();
    void thread_dft_loop_flow64_Loop_1_proc_U0_ap_start();
    void thread_dft_loop_flow64_Loop_1_proc_U0_imag_arr128_0_q0();
    void thread_dft_loop_flow64_Loop_1_proc_U0_imag_arr128_0_q1();
    void thread_dft_loop_flow64_Loop_1_proc_U0_imag_arr128_1_q0();
    void thread_dft_loop_flow64_Loop_1_proc_U0_imag_arr128_1_q1();
    void thread_dft_loop_flow64_Loop_1_proc_U0_imag_arr128_2_q0();
    void thread_dft_loop_flow64_Loop_1_proc_U0_imag_arr128_2_q1();
    void thread_dft_loop_flow64_Loop_1_proc_U0_imag_arr128_3_q0();
    void thread_dft_loop_flow64_Loop_1_proc_U0_imag_arr128_3_q1();
    void thread_dft_loop_flow64_Loop_1_proc_U0_real_arr128_0_q0();
    void thread_dft_loop_flow64_Loop_1_proc_U0_real_arr128_0_q1();
    void thread_dft_loop_flow64_Loop_1_proc_U0_real_arr128_1_q0();
    void thread_dft_loop_flow64_Loop_1_proc_U0_real_arr128_1_q1();
    void thread_dft_loop_flow64_Loop_1_proc_U0_real_arr128_2_q0();
    void thread_dft_loop_flow64_Loop_1_proc_U0_real_arr128_2_q1();
    void thread_dft_loop_flow64_Loop_1_proc_U0_real_arr128_3_q0();
    void thread_dft_loop_flow64_Loop_1_proc_U0_real_arr128_3_q1();
    void thread_imag_arr128_0_address0();
    void thread_imag_arr128_0_address1();
    void thread_imag_arr128_0_ce0();
    void thread_imag_arr128_0_ce1();
    void thread_imag_arr128_0_d0();
    void thread_imag_arr128_0_d1();
    void thread_imag_arr128_0_we0();
    void thread_imag_arr128_0_we1();
    void thread_imag_arr128_1_address0();
    void thread_imag_arr128_1_address1();
    void thread_imag_arr128_1_ce0();
    void thread_imag_arr128_1_ce1();
    void thread_imag_arr128_1_d0();
    void thread_imag_arr128_1_d1();
    void thread_imag_arr128_1_we0();
    void thread_imag_arr128_1_we1();
    void thread_imag_arr128_2_address0();
    void thread_imag_arr128_2_address1();
    void thread_imag_arr128_2_ce0();
    void thread_imag_arr128_2_ce1();
    void thread_imag_arr128_2_d0();
    void thread_imag_arr128_2_d1();
    void thread_imag_arr128_2_we0();
    void thread_imag_arr128_2_we1();
    void thread_imag_arr128_3_address0();
    void thread_imag_arr128_3_address1();
    void thread_imag_arr128_3_ce0();
    void thread_imag_arr128_3_ce1();
    void thread_imag_arr128_3_d0();
    void thread_imag_arr128_3_d1();
    void thread_imag_arr128_3_we0();
    void thread_imag_arr128_3_we1();
    void thread_imag_arr64_0_address0();
    void thread_imag_arr64_0_address1();
    void thread_imag_arr64_0_ce0();
    void thread_imag_arr64_0_ce1();
    void thread_imag_arr64_0_d0();
    void thread_imag_arr64_0_d1();
    void thread_imag_arr64_0_we0();
    void thread_imag_arr64_0_we1();
    void thread_imag_arr64_1_address0();
    void thread_imag_arr64_1_address1();
    void thread_imag_arr64_1_ce0();
    void thread_imag_arr64_1_ce1();
    void thread_imag_arr64_1_d0();
    void thread_imag_arr64_1_d1();
    void thread_imag_arr64_1_we0();
    void thread_imag_arr64_1_we1();
    void thread_imag_arr64_2_address0();
    void thread_imag_arr64_2_address1();
    void thread_imag_arr64_2_ce0();
    void thread_imag_arr64_2_ce1();
    void thread_imag_arr64_2_d0();
    void thread_imag_arr64_2_d1();
    void thread_imag_arr64_2_we0();
    void thread_imag_arr64_2_we1();
    void thread_imag_arr64_3_address0();
    void thread_imag_arr64_3_address1();
    void thread_imag_arr64_3_ce0();
    void thread_imag_arr64_3_ce1();
    void thread_imag_arr64_3_d0();
    void thread_imag_arr64_3_d1();
    void thread_imag_arr64_3_we0();
    void thread_imag_arr64_3_we1();
    void thread_real_arr128_0_address0();
    void thread_real_arr128_0_address1();
    void thread_real_arr128_0_ce0();
    void thread_real_arr128_0_ce1();
    void thread_real_arr128_0_d0();
    void thread_real_arr128_0_d1();
    void thread_real_arr128_0_we0();
    void thread_real_arr128_0_we1();
    void thread_real_arr128_1_address0();
    void thread_real_arr128_1_address1();
    void thread_real_arr128_1_ce0();
    void thread_real_arr128_1_ce1();
    void thread_real_arr128_1_d0();
    void thread_real_arr128_1_d1();
    void thread_real_arr128_1_we0();
    void thread_real_arr128_1_we1();
    void thread_real_arr128_2_address0();
    void thread_real_arr128_2_address1();
    void thread_real_arr128_2_ce0();
    void thread_real_arr128_2_ce1();
    void thread_real_arr128_2_d0();
    void thread_real_arr128_2_d1();
    void thread_real_arr128_2_we0();
    void thread_real_arr128_2_we1();
    void thread_real_arr128_3_address0();
    void thread_real_arr128_3_address1();
    void thread_real_arr128_3_ce0();
    void thread_real_arr128_3_ce1();
    void thread_real_arr128_3_d0();
    void thread_real_arr128_3_d1();
    void thread_real_arr128_3_we0();
    void thread_real_arr128_3_we1();
    void thread_real_arr64_0_address0();
    void thread_real_arr64_0_address1();
    void thread_real_arr64_0_ce0();
    void thread_real_arr64_0_ce1();
    void thread_real_arr64_0_d0();
    void thread_real_arr64_0_d1();
    void thread_real_arr64_0_we0();
    void thread_real_arr64_0_we1();
    void thread_real_arr64_1_address0();
    void thread_real_arr64_1_address1();
    void thread_real_arr64_1_ce0();
    void thread_real_arr64_1_ce1();
    void thread_real_arr64_1_d0();
    void thread_real_arr64_1_d1();
    void thread_real_arr64_1_we0();
    void thread_real_arr64_1_we1();
    void thread_real_arr64_2_address0();
    void thread_real_arr64_2_address1();
    void thread_real_arr64_2_ce0();
    void thread_real_arr64_2_ce1();
    void thread_real_arr64_2_d0();
    void thread_real_arr64_2_d1();
    void thread_real_arr64_2_we0();
    void thread_real_arr64_2_we1();
    void thread_real_arr64_3_address0();
    void thread_real_arr64_3_address1();
    void thread_real_arr64_3_ce0();
    void thread_real_arr64_3_ce1();
    void thread_real_arr64_3_d0();
    void thread_real_arr64_3_d1();
    void thread_real_arr64_3_we0();
    void thread_real_arr64_3_we1();
};

}

using namespace ap_rtl;

#endif
