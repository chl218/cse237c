set moduleName dft_loop_flow1024_Loop_1_proc
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {dft_loop_flow1024_Loop_1_proc}
set C_modelType { void 0 }
set C_modelArgList { 
	{ i_V_read int 11 regular  }
	{ real_i_0 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_4 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_8 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_12 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_0 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_4 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_8 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_12 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_1 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_5 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_9 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_13 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_1 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_5 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_9 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_13 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_2 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_6 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_10 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_14 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_2 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_6 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_10 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_14 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_3 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_7 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_11 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_15 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_3 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_7 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_11 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_15 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_arr1024_0 float 32 regular {array 256 { 0 3 } 0 1 } {global 1}  }
	{ imag_arr1024_0 float 32 regular {array 256 { 0 3 } 0 1 } {global 1}  }
	{ real_arr1024_1 float 32 regular {array 256 { 0 3 } 0 1 } {global 1}  }
	{ imag_arr1024_1 float 32 regular {array 256 { 0 3 } 0 1 } {global 1}  }
	{ real_arr1024_2 float 32 regular {array 256 { 0 3 } 0 1 } {global 1}  }
	{ imag_arr1024_2 float 32 regular {array 256 { 0 3 } 0 1 } {global 1}  }
	{ real_arr1024_3 float 32 regular {array 256 { 0 3 } 0 1 } {global 1}  }
	{ imag_arr1024_3 float 32 regular {array 256 { 0 3 } 0 1 } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "i_V_read", "interface" : "wire", "bitwidth" : 11, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_4", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_8", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_12", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_4", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_8", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_12", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_5", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_9", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_13", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_5", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_9", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_13", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_6", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_10", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_14", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_6", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_10", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_14", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_7", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_11", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_15", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_7", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_11", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_15", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_arr1024_0", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr1024","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 1020,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr1024_0", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr1024","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 1020,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr1024_1", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr1024","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1021,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr1024_1", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr1024","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1021,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr1024_2", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr1024","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 1022,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr1024_2", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr1024","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 1022,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr1024_3", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr1024","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 1023,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr1024_3", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr1024","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 1023,"step" : 4}]}]}], "extern" : 0} ]}
# RTL Port declarations: 
set portNum 136
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ i_V_read sc_in sc_lv 11 signal 0 } 
	{ real_i_0_address0 sc_out sc_lv 6 signal 1 } 
	{ real_i_0_ce0 sc_out sc_logic 1 signal 1 } 
	{ real_i_0_q0 sc_in sc_lv 32 signal 1 } 
	{ real_i_4_address0 sc_out sc_lv 6 signal 2 } 
	{ real_i_4_ce0 sc_out sc_logic 1 signal 2 } 
	{ real_i_4_q0 sc_in sc_lv 32 signal 2 } 
	{ real_i_8_address0 sc_out sc_lv 6 signal 3 } 
	{ real_i_8_ce0 sc_out sc_logic 1 signal 3 } 
	{ real_i_8_q0 sc_in sc_lv 32 signal 3 } 
	{ real_i_12_address0 sc_out sc_lv 6 signal 4 } 
	{ real_i_12_ce0 sc_out sc_logic 1 signal 4 } 
	{ real_i_12_q0 sc_in sc_lv 32 signal 4 } 
	{ imag_i_0_address0 sc_out sc_lv 6 signal 5 } 
	{ imag_i_0_ce0 sc_out sc_logic 1 signal 5 } 
	{ imag_i_0_q0 sc_in sc_lv 32 signal 5 } 
	{ imag_i_4_address0 sc_out sc_lv 6 signal 6 } 
	{ imag_i_4_ce0 sc_out sc_logic 1 signal 6 } 
	{ imag_i_4_q0 sc_in sc_lv 32 signal 6 } 
	{ imag_i_8_address0 sc_out sc_lv 6 signal 7 } 
	{ imag_i_8_ce0 sc_out sc_logic 1 signal 7 } 
	{ imag_i_8_q0 sc_in sc_lv 32 signal 7 } 
	{ imag_i_12_address0 sc_out sc_lv 6 signal 8 } 
	{ imag_i_12_ce0 sc_out sc_logic 1 signal 8 } 
	{ imag_i_12_q0 sc_in sc_lv 32 signal 8 } 
	{ real_i_1_address0 sc_out sc_lv 6 signal 9 } 
	{ real_i_1_ce0 sc_out sc_logic 1 signal 9 } 
	{ real_i_1_q0 sc_in sc_lv 32 signal 9 } 
	{ real_i_5_address0 sc_out sc_lv 6 signal 10 } 
	{ real_i_5_ce0 sc_out sc_logic 1 signal 10 } 
	{ real_i_5_q0 sc_in sc_lv 32 signal 10 } 
	{ real_i_9_address0 sc_out sc_lv 6 signal 11 } 
	{ real_i_9_ce0 sc_out sc_logic 1 signal 11 } 
	{ real_i_9_q0 sc_in sc_lv 32 signal 11 } 
	{ real_i_13_address0 sc_out sc_lv 6 signal 12 } 
	{ real_i_13_ce0 sc_out sc_logic 1 signal 12 } 
	{ real_i_13_q0 sc_in sc_lv 32 signal 12 } 
	{ imag_i_1_address0 sc_out sc_lv 6 signal 13 } 
	{ imag_i_1_ce0 sc_out sc_logic 1 signal 13 } 
	{ imag_i_1_q0 sc_in sc_lv 32 signal 13 } 
	{ imag_i_5_address0 sc_out sc_lv 6 signal 14 } 
	{ imag_i_5_ce0 sc_out sc_logic 1 signal 14 } 
	{ imag_i_5_q0 sc_in sc_lv 32 signal 14 } 
	{ imag_i_9_address0 sc_out sc_lv 6 signal 15 } 
	{ imag_i_9_ce0 sc_out sc_logic 1 signal 15 } 
	{ imag_i_9_q0 sc_in sc_lv 32 signal 15 } 
	{ imag_i_13_address0 sc_out sc_lv 6 signal 16 } 
	{ imag_i_13_ce0 sc_out sc_logic 1 signal 16 } 
	{ imag_i_13_q0 sc_in sc_lv 32 signal 16 } 
	{ real_i_2_address0 sc_out sc_lv 6 signal 17 } 
	{ real_i_2_ce0 sc_out sc_logic 1 signal 17 } 
	{ real_i_2_q0 sc_in sc_lv 32 signal 17 } 
	{ real_i_6_address0 sc_out sc_lv 6 signal 18 } 
	{ real_i_6_ce0 sc_out sc_logic 1 signal 18 } 
	{ real_i_6_q0 sc_in sc_lv 32 signal 18 } 
	{ real_i_10_address0 sc_out sc_lv 6 signal 19 } 
	{ real_i_10_ce0 sc_out sc_logic 1 signal 19 } 
	{ real_i_10_q0 sc_in sc_lv 32 signal 19 } 
	{ real_i_14_address0 sc_out sc_lv 6 signal 20 } 
	{ real_i_14_ce0 sc_out sc_logic 1 signal 20 } 
	{ real_i_14_q0 sc_in sc_lv 32 signal 20 } 
	{ imag_i_2_address0 sc_out sc_lv 6 signal 21 } 
	{ imag_i_2_ce0 sc_out sc_logic 1 signal 21 } 
	{ imag_i_2_q0 sc_in sc_lv 32 signal 21 } 
	{ imag_i_6_address0 sc_out sc_lv 6 signal 22 } 
	{ imag_i_6_ce0 sc_out sc_logic 1 signal 22 } 
	{ imag_i_6_q0 sc_in sc_lv 32 signal 22 } 
	{ imag_i_10_address0 sc_out sc_lv 6 signal 23 } 
	{ imag_i_10_ce0 sc_out sc_logic 1 signal 23 } 
	{ imag_i_10_q0 sc_in sc_lv 32 signal 23 } 
	{ imag_i_14_address0 sc_out sc_lv 6 signal 24 } 
	{ imag_i_14_ce0 sc_out sc_logic 1 signal 24 } 
	{ imag_i_14_q0 sc_in sc_lv 32 signal 24 } 
	{ real_i_3_address0 sc_out sc_lv 6 signal 25 } 
	{ real_i_3_ce0 sc_out sc_logic 1 signal 25 } 
	{ real_i_3_q0 sc_in sc_lv 32 signal 25 } 
	{ real_i_7_address0 sc_out sc_lv 6 signal 26 } 
	{ real_i_7_ce0 sc_out sc_logic 1 signal 26 } 
	{ real_i_7_q0 sc_in sc_lv 32 signal 26 } 
	{ real_i_11_address0 sc_out sc_lv 6 signal 27 } 
	{ real_i_11_ce0 sc_out sc_logic 1 signal 27 } 
	{ real_i_11_q0 sc_in sc_lv 32 signal 27 } 
	{ real_i_15_address0 sc_out sc_lv 6 signal 28 } 
	{ real_i_15_ce0 sc_out sc_logic 1 signal 28 } 
	{ real_i_15_q0 sc_in sc_lv 32 signal 28 } 
	{ imag_i_3_address0 sc_out sc_lv 6 signal 29 } 
	{ imag_i_3_ce0 sc_out sc_logic 1 signal 29 } 
	{ imag_i_3_q0 sc_in sc_lv 32 signal 29 } 
	{ imag_i_7_address0 sc_out sc_lv 6 signal 30 } 
	{ imag_i_7_ce0 sc_out sc_logic 1 signal 30 } 
	{ imag_i_7_q0 sc_in sc_lv 32 signal 30 } 
	{ imag_i_11_address0 sc_out sc_lv 6 signal 31 } 
	{ imag_i_11_ce0 sc_out sc_logic 1 signal 31 } 
	{ imag_i_11_q0 sc_in sc_lv 32 signal 31 } 
	{ imag_i_15_address0 sc_out sc_lv 6 signal 32 } 
	{ imag_i_15_ce0 sc_out sc_logic 1 signal 32 } 
	{ imag_i_15_q0 sc_in sc_lv 32 signal 32 } 
	{ real_arr1024_0_address0 sc_out sc_lv 8 signal 33 } 
	{ real_arr1024_0_ce0 sc_out sc_logic 1 signal 33 } 
	{ real_arr1024_0_we0 sc_out sc_logic 1 signal 33 } 
	{ real_arr1024_0_d0 sc_out sc_lv 32 signal 33 } 
	{ imag_arr1024_0_address0 sc_out sc_lv 8 signal 34 } 
	{ imag_arr1024_0_ce0 sc_out sc_logic 1 signal 34 } 
	{ imag_arr1024_0_we0 sc_out sc_logic 1 signal 34 } 
	{ imag_arr1024_0_d0 sc_out sc_lv 32 signal 34 } 
	{ real_arr1024_1_address0 sc_out sc_lv 8 signal 35 } 
	{ real_arr1024_1_ce0 sc_out sc_logic 1 signal 35 } 
	{ real_arr1024_1_we0 sc_out sc_logic 1 signal 35 } 
	{ real_arr1024_1_d0 sc_out sc_lv 32 signal 35 } 
	{ imag_arr1024_1_address0 sc_out sc_lv 8 signal 36 } 
	{ imag_arr1024_1_ce0 sc_out sc_logic 1 signal 36 } 
	{ imag_arr1024_1_we0 sc_out sc_logic 1 signal 36 } 
	{ imag_arr1024_1_d0 sc_out sc_lv 32 signal 36 } 
	{ real_arr1024_2_address0 sc_out sc_lv 8 signal 37 } 
	{ real_arr1024_2_ce0 sc_out sc_logic 1 signal 37 } 
	{ real_arr1024_2_we0 sc_out sc_logic 1 signal 37 } 
	{ real_arr1024_2_d0 sc_out sc_lv 32 signal 37 } 
	{ imag_arr1024_2_address0 sc_out sc_lv 8 signal 38 } 
	{ imag_arr1024_2_ce0 sc_out sc_logic 1 signal 38 } 
	{ imag_arr1024_2_we0 sc_out sc_logic 1 signal 38 } 
	{ imag_arr1024_2_d0 sc_out sc_lv 32 signal 38 } 
	{ real_arr1024_3_address0 sc_out sc_lv 8 signal 39 } 
	{ real_arr1024_3_ce0 sc_out sc_logic 1 signal 39 } 
	{ real_arr1024_3_we0 sc_out sc_logic 1 signal 39 } 
	{ real_arr1024_3_d0 sc_out sc_lv 32 signal 39 } 
	{ imag_arr1024_3_address0 sc_out sc_lv 8 signal 40 } 
	{ imag_arr1024_3_ce0 sc_out sc_logic 1 signal 40 } 
	{ imag_arr1024_3_we0 sc_out sc_logic 1 signal 40 } 
	{ imag_arr1024_3_d0 sc_out sc_lv 32 signal 40 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "i_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":11, "type": "signal", "bundle":{"name": "i_V_read", "role": "default" }} , 
 	{ "name": "real_i_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_0", "role": "address0" }} , 
 	{ "name": "real_i_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "ce0" }} , 
 	{ "name": "real_i_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "q0" }} , 
 	{ "name": "real_i_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_4", "role": "address0" }} , 
 	{ "name": "real_i_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_4", "role": "ce0" }} , 
 	{ "name": "real_i_4_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_4", "role": "q0" }} , 
 	{ "name": "real_i_8_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_8", "role": "address0" }} , 
 	{ "name": "real_i_8_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_8", "role": "ce0" }} , 
 	{ "name": "real_i_8_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_8", "role": "q0" }} , 
 	{ "name": "real_i_12_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_12", "role": "address0" }} , 
 	{ "name": "real_i_12_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_12", "role": "ce0" }} , 
 	{ "name": "real_i_12_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_12", "role": "q0" }} , 
 	{ "name": "imag_i_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_0", "role": "address0" }} , 
 	{ "name": "imag_i_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_0", "role": "ce0" }} , 
 	{ "name": "imag_i_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_0", "role": "q0" }} , 
 	{ "name": "imag_i_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_4", "role": "address0" }} , 
 	{ "name": "imag_i_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_4", "role": "ce0" }} , 
 	{ "name": "imag_i_4_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_4", "role": "q0" }} , 
 	{ "name": "imag_i_8_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_8", "role": "address0" }} , 
 	{ "name": "imag_i_8_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_8", "role": "ce0" }} , 
 	{ "name": "imag_i_8_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_8", "role": "q0" }} , 
 	{ "name": "imag_i_12_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_12", "role": "address0" }} , 
 	{ "name": "imag_i_12_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_12", "role": "ce0" }} , 
 	{ "name": "imag_i_12_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_12", "role": "q0" }} , 
 	{ "name": "real_i_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_1", "role": "address0" }} , 
 	{ "name": "real_i_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "ce0" }} , 
 	{ "name": "real_i_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "q0" }} , 
 	{ "name": "real_i_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_5", "role": "address0" }} , 
 	{ "name": "real_i_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_5", "role": "ce0" }} , 
 	{ "name": "real_i_5_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_5", "role": "q0" }} , 
 	{ "name": "real_i_9_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_9", "role": "address0" }} , 
 	{ "name": "real_i_9_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_9", "role": "ce0" }} , 
 	{ "name": "real_i_9_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_9", "role": "q0" }} , 
 	{ "name": "real_i_13_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_13", "role": "address0" }} , 
 	{ "name": "real_i_13_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_13", "role": "ce0" }} , 
 	{ "name": "real_i_13_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_13", "role": "q0" }} , 
 	{ "name": "imag_i_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_1", "role": "address0" }} , 
 	{ "name": "imag_i_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_1", "role": "ce0" }} , 
 	{ "name": "imag_i_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_1", "role": "q0" }} , 
 	{ "name": "imag_i_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_5", "role": "address0" }} , 
 	{ "name": "imag_i_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_5", "role": "ce0" }} , 
 	{ "name": "imag_i_5_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_5", "role": "q0" }} , 
 	{ "name": "imag_i_9_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_9", "role": "address0" }} , 
 	{ "name": "imag_i_9_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_9", "role": "ce0" }} , 
 	{ "name": "imag_i_9_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_9", "role": "q0" }} , 
 	{ "name": "imag_i_13_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_13", "role": "address0" }} , 
 	{ "name": "imag_i_13_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_13", "role": "ce0" }} , 
 	{ "name": "imag_i_13_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_13", "role": "q0" }} , 
 	{ "name": "real_i_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_2", "role": "address0" }} , 
 	{ "name": "real_i_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_2", "role": "ce0" }} , 
 	{ "name": "real_i_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_2", "role": "q0" }} , 
 	{ "name": "real_i_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_6", "role": "address0" }} , 
 	{ "name": "real_i_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_6", "role": "ce0" }} , 
 	{ "name": "real_i_6_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_6", "role": "q0" }} , 
 	{ "name": "real_i_10_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_10", "role": "address0" }} , 
 	{ "name": "real_i_10_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_10", "role": "ce0" }} , 
 	{ "name": "real_i_10_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_10", "role": "q0" }} , 
 	{ "name": "real_i_14_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_14", "role": "address0" }} , 
 	{ "name": "real_i_14_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_14", "role": "ce0" }} , 
 	{ "name": "real_i_14_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_14", "role": "q0" }} , 
 	{ "name": "imag_i_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_2", "role": "address0" }} , 
 	{ "name": "imag_i_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_2", "role": "ce0" }} , 
 	{ "name": "imag_i_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_2", "role": "q0" }} , 
 	{ "name": "imag_i_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_6", "role": "address0" }} , 
 	{ "name": "imag_i_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_6", "role": "ce0" }} , 
 	{ "name": "imag_i_6_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_6", "role": "q0" }} , 
 	{ "name": "imag_i_10_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_10", "role": "address0" }} , 
 	{ "name": "imag_i_10_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_10", "role": "ce0" }} , 
 	{ "name": "imag_i_10_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_10", "role": "q0" }} , 
 	{ "name": "imag_i_14_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_14", "role": "address0" }} , 
 	{ "name": "imag_i_14_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_14", "role": "ce0" }} , 
 	{ "name": "imag_i_14_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_14", "role": "q0" }} , 
 	{ "name": "real_i_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_3", "role": "address0" }} , 
 	{ "name": "real_i_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_3", "role": "ce0" }} , 
 	{ "name": "real_i_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_3", "role": "q0" }} , 
 	{ "name": "real_i_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_7", "role": "address0" }} , 
 	{ "name": "real_i_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_7", "role": "ce0" }} , 
 	{ "name": "real_i_7_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_7", "role": "q0" }} , 
 	{ "name": "real_i_11_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_11", "role": "address0" }} , 
 	{ "name": "real_i_11_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_11", "role": "ce0" }} , 
 	{ "name": "real_i_11_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_11", "role": "q0" }} , 
 	{ "name": "real_i_15_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_15", "role": "address0" }} , 
 	{ "name": "real_i_15_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_15", "role": "ce0" }} , 
 	{ "name": "real_i_15_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_15", "role": "q0" }} , 
 	{ "name": "imag_i_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_3", "role": "address0" }} , 
 	{ "name": "imag_i_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_3", "role": "ce0" }} , 
 	{ "name": "imag_i_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_3", "role": "q0" }} , 
 	{ "name": "imag_i_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_7", "role": "address0" }} , 
 	{ "name": "imag_i_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_7", "role": "ce0" }} , 
 	{ "name": "imag_i_7_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_7", "role": "q0" }} , 
 	{ "name": "imag_i_11_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_11", "role": "address0" }} , 
 	{ "name": "imag_i_11_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_11", "role": "ce0" }} , 
 	{ "name": "imag_i_11_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_11", "role": "q0" }} , 
 	{ "name": "imag_i_15_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_15", "role": "address0" }} , 
 	{ "name": "imag_i_15_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_15", "role": "ce0" }} , 
 	{ "name": "imag_i_15_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_15", "role": "q0" }} , 
 	{ "name": "real_arr1024_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_arr1024_0", "role": "address0" }} , 
 	{ "name": "real_arr1024_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_0", "role": "ce0" }} , 
 	{ "name": "real_arr1024_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_0", "role": "we0" }} , 
 	{ "name": "real_arr1024_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr1024_0", "role": "d0" }} , 
 	{ "name": "imag_arr1024_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "imag_arr1024_0", "role": "address0" }} , 
 	{ "name": "imag_arr1024_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_0", "role": "ce0" }} , 
 	{ "name": "imag_arr1024_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_0", "role": "we0" }} , 
 	{ "name": "imag_arr1024_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr1024_0", "role": "d0" }} , 
 	{ "name": "real_arr1024_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_arr1024_1", "role": "address0" }} , 
 	{ "name": "real_arr1024_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_1", "role": "ce0" }} , 
 	{ "name": "real_arr1024_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_1", "role": "we0" }} , 
 	{ "name": "real_arr1024_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr1024_1", "role": "d0" }} , 
 	{ "name": "imag_arr1024_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "imag_arr1024_1", "role": "address0" }} , 
 	{ "name": "imag_arr1024_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_1", "role": "ce0" }} , 
 	{ "name": "imag_arr1024_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_1", "role": "we0" }} , 
 	{ "name": "imag_arr1024_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr1024_1", "role": "d0" }} , 
 	{ "name": "real_arr1024_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_arr1024_2", "role": "address0" }} , 
 	{ "name": "real_arr1024_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_2", "role": "ce0" }} , 
 	{ "name": "real_arr1024_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_2", "role": "we0" }} , 
 	{ "name": "real_arr1024_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr1024_2", "role": "d0" }} , 
 	{ "name": "imag_arr1024_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "imag_arr1024_2", "role": "address0" }} , 
 	{ "name": "imag_arr1024_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_2", "role": "ce0" }} , 
 	{ "name": "imag_arr1024_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_2", "role": "we0" }} , 
 	{ "name": "imag_arr1024_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr1024_2", "role": "d0" }} , 
 	{ "name": "real_arr1024_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_arr1024_3", "role": "address0" }} , 
 	{ "name": "real_arr1024_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_3", "role": "ce0" }} , 
 	{ "name": "real_arr1024_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_3", "role": "we0" }} , 
 	{ "name": "real_arr1024_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr1024_3", "role": "d0" }} , 
 	{ "name": "imag_arr1024_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "imag_arr1024_3", "role": "address0" }} , 
 	{ "name": "imag_arr1024_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_3", "role": "ce0" }} , 
 	{ "name": "imag_arr1024_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_3", "role": "we0" }} , 
 	{ "name": "imag_arr1024_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr1024_3", "role": "d0" }}  ]}
set Spec2ImplPortList { 
	i_V_read { ap_none {  { i_V_read in_data 0 11 } } }
	real_i_0 { ap_memory {  { real_i_0_address0 mem_address 1 6 }  { real_i_0_ce0 mem_ce 1 1 }  { real_i_0_q0 mem_dout 0 32 } } }
	real_i_4 { ap_memory {  { real_i_4_address0 mem_address 1 6 }  { real_i_4_ce0 mem_ce 1 1 }  { real_i_4_q0 mem_dout 0 32 } } }
	real_i_8 { ap_memory {  { real_i_8_address0 mem_address 1 6 }  { real_i_8_ce0 mem_ce 1 1 }  { real_i_8_q0 mem_dout 0 32 } } }
	real_i_12 { ap_memory {  { real_i_12_address0 mem_address 1 6 }  { real_i_12_ce0 mem_ce 1 1 }  { real_i_12_q0 mem_dout 0 32 } } }
	imag_i_0 { ap_memory {  { imag_i_0_address0 mem_address 1 6 }  { imag_i_0_ce0 mem_ce 1 1 }  { imag_i_0_q0 mem_dout 0 32 } } }
	imag_i_4 { ap_memory {  { imag_i_4_address0 mem_address 1 6 }  { imag_i_4_ce0 mem_ce 1 1 }  { imag_i_4_q0 mem_dout 0 32 } } }
	imag_i_8 { ap_memory {  { imag_i_8_address0 mem_address 1 6 }  { imag_i_8_ce0 mem_ce 1 1 }  { imag_i_8_q0 mem_dout 0 32 } } }
	imag_i_12 { ap_memory {  { imag_i_12_address0 mem_address 1 6 }  { imag_i_12_ce0 mem_ce 1 1 }  { imag_i_12_q0 mem_dout 0 32 } } }
	real_i_1 { ap_memory {  { real_i_1_address0 mem_address 1 6 }  { real_i_1_ce0 mem_ce 1 1 }  { real_i_1_q0 mem_dout 0 32 } } }
	real_i_5 { ap_memory {  { real_i_5_address0 mem_address 1 6 }  { real_i_5_ce0 mem_ce 1 1 }  { real_i_5_q0 mem_dout 0 32 } } }
	real_i_9 { ap_memory {  { real_i_9_address0 mem_address 1 6 }  { real_i_9_ce0 mem_ce 1 1 }  { real_i_9_q0 mem_dout 0 32 } } }
	real_i_13 { ap_memory {  { real_i_13_address0 mem_address 1 6 }  { real_i_13_ce0 mem_ce 1 1 }  { real_i_13_q0 mem_dout 0 32 } } }
	imag_i_1 { ap_memory {  { imag_i_1_address0 mem_address 1 6 }  { imag_i_1_ce0 mem_ce 1 1 }  { imag_i_1_q0 mem_dout 0 32 } } }
	imag_i_5 { ap_memory {  { imag_i_5_address0 mem_address 1 6 }  { imag_i_5_ce0 mem_ce 1 1 }  { imag_i_5_q0 mem_dout 0 32 } } }
	imag_i_9 { ap_memory {  { imag_i_9_address0 mem_address 1 6 }  { imag_i_9_ce0 mem_ce 1 1 }  { imag_i_9_q0 mem_dout 0 32 } } }
	imag_i_13 { ap_memory {  { imag_i_13_address0 mem_address 1 6 }  { imag_i_13_ce0 mem_ce 1 1 }  { imag_i_13_q0 mem_dout 0 32 } } }
	real_i_2 { ap_memory {  { real_i_2_address0 mem_address 1 6 }  { real_i_2_ce0 mem_ce 1 1 }  { real_i_2_q0 mem_dout 0 32 } } }
	real_i_6 { ap_memory {  { real_i_6_address0 mem_address 1 6 }  { real_i_6_ce0 mem_ce 1 1 }  { real_i_6_q0 mem_dout 0 32 } } }
	real_i_10 { ap_memory {  { real_i_10_address0 mem_address 1 6 }  { real_i_10_ce0 mem_ce 1 1 }  { real_i_10_q0 mem_dout 0 32 } } }
	real_i_14 { ap_memory {  { real_i_14_address0 mem_address 1 6 }  { real_i_14_ce0 mem_ce 1 1 }  { real_i_14_q0 mem_dout 0 32 } } }
	imag_i_2 { ap_memory {  { imag_i_2_address0 mem_address 1 6 }  { imag_i_2_ce0 mem_ce 1 1 }  { imag_i_2_q0 mem_dout 0 32 } } }
	imag_i_6 { ap_memory {  { imag_i_6_address0 mem_address 1 6 }  { imag_i_6_ce0 mem_ce 1 1 }  { imag_i_6_q0 mem_dout 0 32 } } }
	imag_i_10 { ap_memory {  { imag_i_10_address0 mem_address 1 6 }  { imag_i_10_ce0 mem_ce 1 1 }  { imag_i_10_q0 mem_dout 0 32 } } }
	imag_i_14 { ap_memory {  { imag_i_14_address0 mem_address 1 6 }  { imag_i_14_ce0 mem_ce 1 1 }  { imag_i_14_q0 mem_dout 0 32 } } }
	real_i_3 { ap_memory {  { real_i_3_address0 mem_address 1 6 }  { real_i_3_ce0 mem_ce 1 1 }  { real_i_3_q0 mem_dout 0 32 } } }
	real_i_7 { ap_memory {  { real_i_7_address0 mem_address 1 6 }  { real_i_7_ce0 mem_ce 1 1 }  { real_i_7_q0 mem_dout 0 32 } } }
	real_i_11 { ap_memory {  { real_i_11_address0 mem_address 1 6 }  { real_i_11_ce0 mem_ce 1 1 }  { real_i_11_q0 mem_dout 0 32 } } }
	real_i_15 { ap_memory {  { real_i_15_address0 mem_address 1 6 }  { real_i_15_ce0 mem_ce 1 1 }  { real_i_15_q0 mem_dout 0 32 } } }
	imag_i_3 { ap_memory {  { imag_i_3_address0 mem_address 1 6 }  { imag_i_3_ce0 mem_ce 1 1 }  { imag_i_3_q0 mem_dout 0 32 } } }
	imag_i_7 { ap_memory {  { imag_i_7_address0 mem_address 1 6 }  { imag_i_7_ce0 mem_ce 1 1 }  { imag_i_7_q0 mem_dout 0 32 } } }
	imag_i_11 { ap_memory {  { imag_i_11_address0 mem_address 1 6 }  { imag_i_11_ce0 mem_ce 1 1 }  { imag_i_11_q0 mem_dout 0 32 } } }
	imag_i_15 { ap_memory {  { imag_i_15_address0 mem_address 1 6 }  { imag_i_15_ce0 mem_ce 1 1 }  { imag_i_15_q0 mem_dout 0 32 } } }
	real_arr1024_0 { ap_memory {  { real_arr1024_0_address0 mem_address 1 8 }  { real_arr1024_0_ce0 mem_ce 1 1 }  { real_arr1024_0_we0 mem_we 1 1 }  { real_arr1024_0_d0 mem_din 1 32 } } }
	imag_arr1024_0 { ap_memory {  { imag_arr1024_0_address0 mem_address 1 8 }  { imag_arr1024_0_ce0 mem_ce 1 1 }  { imag_arr1024_0_we0 mem_we 1 1 }  { imag_arr1024_0_d0 mem_din 1 32 } } }
	real_arr1024_1 { ap_memory {  { real_arr1024_1_address0 mem_address 1 8 }  { real_arr1024_1_ce0 mem_ce 1 1 }  { real_arr1024_1_we0 mem_we 1 1 }  { real_arr1024_1_d0 mem_din 1 32 } } }
	imag_arr1024_1 { ap_memory {  { imag_arr1024_1_address0 mem_address 1 8 }  { imag_arr1024_1_ce0 mem_ce 1 1 }  { imag_arr1024_1_we0 mem_we 1 1 }  { imag_arr1024_1_d0 mem_din 1 32 } } }
	real_arr1024_2 { ap_memory {  { real_arr1024_2_address0 mem_address 1 8 }  { real_arr1024_2_ce0 mem_ce 1 1 }  { real_arr1024_2_we0 mem_we 1 1 }  { real_arr1024_2_d0 mem_din 1 32 } } }
	imag_arr1024_2 { ap_memory {  { imag_arr1024_2_address0 mem_address 1 8 }  { imag_arr1024_2_ce0 mem_ce 1 1 }  { imag_arr1024_2_we0 mem_we 1 1 }  { imag_arr1024_2_d0 mem_din 1 32 } } }
	real_arr1024_3 { ap_memory {  { real_arr1024_3_address0 mem_address 1 8 }  { real_arr1024_3_ce0 mem_ce 1 1 }  { real_arr1024_3_we0 mem_we 1 1 }  { real_arr1024_3_d0 mem_din 1 32 } } }
	imag_arr1024_3 { ap_memory {  { imag_arr1024_3_address0 mem_address 1 8 }  { imag_arr1024_3_ce0 mem_ce 1 1 }  { imag_arr1024_3_we0 mem_we 1 1 }  { imag_arr1024_3_d0 mem_din 1 32 } } }
}
