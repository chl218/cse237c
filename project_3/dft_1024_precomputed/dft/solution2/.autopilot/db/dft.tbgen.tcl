set C_TypeInfoList {{ 
"dft" : [[], { "return": [[], "void"]} , [{"ExternC" : 0}], [ {"real_i": [[], {"array": ["0", [1024]]}] }, {"imag_i": [[], {"array": ["0", [1024]]}] }, {"real_o": [[], {"array": ["0", [1024]]}] }, {"imag_o": [[], {"array": ["0", [1024]]}] }],["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"],""],
 "1": [ "real_arr8", [[], {"array": ["0", [8]]}],""],
 "2": [ "real_arr64", [[], {"array": ["0", [64]]}],""],
 "3": [ "real_arr512", [[], {"array": ["0", [512]]}],""],
 "4": [ "real_arr4", [[], {"array": ["0", [4]]}],""],
 "5": [ "real_arr32", [[], {"array": ["0", [32]]}],""],
 "6": [ "real_arr256", [[], {"array": ["0", [256]]}],""],
 "7": [ "real_arr2", [[], {"array": ["0", [2]]}],""],
 "8": [ "real_arr16", [[], {"array": ["0", [16]]}],""],
 "9": [ "real_arr128", [[], {"array": ["0", [128]]}],""],
 "10": [ "real_arr1024", [[], {"array": ["0", [1024]]}],""],
 "11": [ "imag_arr8", [[], {"array": ["0", [8]]}],""],
 "12": [ "imag_arr64", [[], {"array": ["0", [64]]}],""],
 "13": [ "imag_arr512", [[], {"array": ["0", [512]]}],""],
 "14": [ "imag_arr4", [[], {"array": ["0", [4]]}],""],
 "15": [ "imag_arr32", [[], {"array": ["0", [32]]}],""],
 "16": [ "imag_arr256", [[], {"array": ["0", [256]]}],""],
 "17": [ "imag_arr2", [[], {"array": ["0", [2]]}],""],
 "18": [ "imag_arr16", [[], {"array": ["0", [16]]}],""],
 "19": [ "imag_arr128", [[], {"array": ["0", [128]]}],""],
 "20": [ "imag_arr1024", [[], {"array": ["0", [1024]]}],""], 
"0": [ "DTYPE", {"typedef": [[[], {"scalar": "float"}],""]}]
}}
set moduleName dft
set isCombinational 0
set isDatapathOnly 0
set isPipelined 1
set pipeline_type dataflow
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {dft}
set C_modelType { void 0 }
set C_modelArgList { 
	{ real_i_0 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_1 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_2 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_3 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_4 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_5 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_6 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_7 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_8 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_9 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_10 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_11 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_12 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_13 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_14 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_i_15 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_0 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_1 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_2 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_3 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_4 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_5 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_6 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_7 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_8 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_9 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_10 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_11 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_12 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_13 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_14 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ imag_i_15 float 32 regular {array 64 { 1 3 } 1 1 }  }
	{ real_o float 32 regular {array 1024 { 0 3 } 0 1 }  }
	{ imag_o float 32 regular {array 1024 { 0 3 } 0 1 }  }
}
set C_modelArgMapList {[ 
	{ "Name" : "real_i_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 1008,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1009,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 1010,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 1011,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_4", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 4,"up" : 1012,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_5", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 5,"up" : 1013,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_6", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 6,"up" : 1014,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_7", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 7,"up" : 1015,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_8", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 8,"up" : 1016,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_9", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 9,"up" : 1017,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_10", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 10,"up" : 1018,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_11", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 11,"up" : 1019,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_12", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 12,"up" : 1020,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_13", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 13,"up" : 1021,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_14", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 14,"up" : 1022,"step" : 16}]}]}]} , 
 	{ "Name" : "real_i_15", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 15,"up" : 1023,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 1008,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1009,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 1010,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 1011,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_4", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 4,"up" : 1012,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_5", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 5,"up" : 1013,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_6", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 6,"up" : 1014,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_7", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 7,"up" : 1015,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_8", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 8,"up" : 1016,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_9", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 9,"up" : 1017,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_10", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 10,"up" : 1018,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_11", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 11,"up" : 1019,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_12", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 12,"up" : 1020,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_13", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 13,"up" : 1021,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_14", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 14,"up" : 1022,"step" : 16}]}]}]} , 
 	{ "Name" : "imag_i_15", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_i","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 15,"up" : 1023,"step" : 16}]}]}]} , 
 	{ "Name" : "real_o", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 1023,"step" : 1}]}]}]} , 
 	{ "Name" : "imag_o", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_o","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 1023,"step" : 1}]}]}]} ]}
# RTL Port declarations: 
set portNum 346
set portList { 
	{ real_i_0_address0 sc_out sc_lv 6 signal 0 } 
	{ real_i_0_ce0 sc_out sc_logic 1 signal 0 } 
	{ real_i_0_d0 sc_out sc_lv 32 signal 0 } 
	{ real_i_0_q0 sc_in sc_lv 32 signal 0 } 
	{ real_i_0_we0 sc_out sc_logic 1 signal 0 } 
	{ real_i_0_address1 sc_out sc_lv 6 signal 0 } 
	{ real_i_0_ce1 sc_out sc_logic 1 signal 0 } 
	{ real_i_0_d1 sc_out sc_lv 32 signal 0 } 
	{ real_i_0_q1 sc_in sc_lv 32 signal 0 } 
	{ real_i_0_we1 sc_out sc_logic 1 signal 0 } 
	{ real_i_1_address0 sc_out sc_lv 6 signal 1 } 
	{ real_i_1_ce0 sc_out sc_logic 1 signal 1 } 
	{ real_i_1_d0 sc_out sc_lv 32 signal 1 } 
	{ real_i_1_q0 sc_in sc_lv 32 signal 1 } 
	{ real_i_1_we0 sc_out sc_logic 1 signal 1 } 
	{ real_i_1_address1 sc_out sc_lv 6 signal 1 } 
	{ real_i_1_ce1 sc_out sc_logic 1 signal 1 } 
	{ real_i_1_d1 sc_out sc_lv 32 signal 1 } 
	{ real_i_1_q1 sc_in sc_lv 32 signal 1 } 
	{ real_i_1_we1 sc_out sc_logic 1 signal 1 } 
	{ real_i_2_address0 sc_out sc_lv 6 signal 2 } 
	{ real_i_2_ce0 sc_out sc_logic 1 signal 2 } 
	{ real_i_2_d0 sc_out sc_lv 32 signal 2 } 
	{ real_i_2_q0 sc_in sc_lv 32 signal 2 } 
	{ real_i_2_we0 sc_out sc_logic 1 signal 2 } 
	{ real_i_2_address1 sc_out sc_lv 6 signal 2 } 
	{ real_i_2_ce1 sc_out sc_logic 1 signal 2 } 
	{ real_i_2_d1 sc_out sc_lv 32 signal 2 } 
	{ real_i_2_q1 sc_in sc_lv 32 signal 2 } 
	{ real_i_2_we1 sc_out sc_logic 1 signal 2 } 
	{ real_i_3_address0 sc_out sc_lv 6 signal 3 } 
	{ real_i_3_ce0 sc_out sc_logic 1 signal 3 } 
	{ real_i_3_d0 sc_out sc_lv 32 signal 3 } 
	{ real_i_3_q0 sc_in sc_lv 32 signal 3 } 
	{ real_i_3_we0 sc_out sc_logic 1 signal 3 } 
	{ real_i_3_address1 sc_out sc_lv 6 signal 3 } 
	{ real_i_3_ce1 sc_out sc_logic 1 signal 3 } 
	{ real_i_3_d1 sc_out sc_lv 32 signal 3 } 
	{ real_i_3_q1 sc_in sc_lv 32 signal 3 } 
	{ real_i_3_we1 sc_out sc_logic 1 signal 3 } 
	{ real_i_4_address0 sc_out sc_lv 6 signal 4 } 
	{ real_i_4_ce0 sc_out sc_logic 1 signal 4 } 
	{ real_i_4_d0 sc_out sc_lv 32 signal 4 } 
	{ real_i_4_q0 sc_in sc_lv 32 signal 4 } 
	{ real_i_4_we0 sc_out sc_logic 1 signal 4 } 
	{ real_i_4_address1 sc_out sc_lv 6 signal 4 } 
	{ real_i_4_ce1 sc_out sc_logic 1 signal 4 } 
	{ real_i_4_d1 sc_out sc_lv 32 signal 4 } 
	{ real_i_4_q1 sc_in sc_lv 32 signal 4 } 
	{ real_i_4_we1 sc_out sc_logic 1 signal 4 } 
	{ real_i_5_address0 sc_out sc_lv 6 signal 5 } 
	{ real_i_5_ce0 sc_out sc_logic 1 signal 5 } 
	{ real_i_5_d0 sc_out sc_lv 32 signal 5 } 
	{ real_i_5_q0 sc_in sc_lv 32 signal 5 } 
	{ real_i_5_we0 sc_out sc_logic 1 signal 5 } 
	{ real_i_5_address1 sc_out sc_lv 6 signal 5 } 
	{ real_i_5_ce1 sc_out sc_logic 1 signal 5 } 
	{ real_i_5_d1 sc_out sc_lv 32 signal 5 } 
	{ real_i_5_q1 sc_in sc_lv 32 signal 5 } 
	{ real_i_5_we1 sc_out sc_logic 1 signal 5 } 
	{ real_i_6_address0 sc_out sc_lv 6 signal 6 } 
	{ real_i_6_ce0 sc_out sc_logic 1 signal 6 } 
	{ real_i_6_d0 sc_out sc_lv 32 signal 6 } 
	{ real_i_6_q0 sc_in sc_lv 32 signal 6 } 
	{ real_i_6_we0 sc_out sc_logic 1 signal 6 } 
	{ real_i_6_address1 sc_out sc_lv 6 signal 6 } 
	{ real_i_6_ce1 sc_out sc_logic 1 signal 6 } 
	{ real_i_6_d1 sc_out sc_lv 32 signal 6 } 
	{ real_i_6_q1 sc_in sc_lv 32 signal 6 } 
	{ real_i_6_we1 sc_out sc_logic 1 signal 6 } 
	{ real_i_7_address0 sc_out sc_lv 6 signal 7 } 
	{ real_i_7_ce0 sc_out sc_logic 1 signal 7 } 
	{ real_i_7_d0 sc_out sc_lv 32 signal 7 } 
	{ real_i_7_q0 sc_in sc_lv 32 signal 7 } 
	{ real_i_7_we0 sc_out sc_logic 1 signal 7 } 
	{ real_i_7_address1 sc_out sc_lv 6 signal 7 } 
	{ real_i_7_ce1 sc_out sc_logic 1 signal 7 } 
	{ real_i_7_d1 sc_out sc_lv 32 signal 7 } 
	{ real_i_7_q1 sc_in sc_lv 32 signal 7 } 
	{ real_i_7_we1 sc_out sc_logic 1 signal 7 } 
	{ real_i_8_address0 sc_out sc_lv 6 signal 8 } 
	{ real_i_8_ce0 sc_out sc_logic 1 signal 8 } 
	{ real_i_8_d0 sc_out sc_lv 32 signal 8 } 
	{ real_i_8_q0 sc_in sc_lv 32 signal 8 } 
	{ real_i_8_we0 sc_out sc_logic 1 signal 8 } 
	{ real_i_8_address1 sc_out sc_lv 6 signal 8 } 
	{ real_i_8_ce1 sc_out sc_logic 1 signal 8 } 
	{ real_i_8_d1 sc_out sc_lv 32 signal 8 } 
	{ real_i_8_q1 sc_in sc_lv 32 signal 8 } 
	{ real_i_8_we1 sc_out sc_logic 1 signal 8 } 
	{ real_i_9_address0 sc_out sc_lv 6 signal 9 } 
	{ real_i_9_ce0 sc_out sc_logic 1 signal 9 } 
	{ real_i_9_d0 sc_out sc_lv 32 signal 9 } 
	{ real_i_9_q0 sc_in sc_lv 32 signal 9 } 
	{ real_i_9_we0 sc_out sc_logic 1 signal 9 } 
	{ real_i_9_address1 sc_out sc_lv 6 signal 9 } 
	{ real_i_9_ce1 sc_out sc_logic 1 signal 9 } 
	{ real_i_9_d1 sc_out sc_lv 32 signal 9 } 
	{ real_i_9_q1 sc_in sc_lv 32 signal 9 } 
	{ real_i_9_we1 sc_out sc_logic 1 signal 9 } 
	{ real_i_10_address0 sc_out sc_lv 6 signal 10 } 
	{ real_i_10_ce0 sc_out sc_logic 1 signal 10 } 
	{ real_i_10_d0 sc_out sc_lv 32 signal 10 } 
	{ real_i_10_q0 sc_in sc_lv 32 signal 10 } 
	{ real_i_10_we0 sc_out sc_logic 1 signal 10 } 
	{ real_i_10_address1 sc_out sc_lv 6 signal 10 } 
	{ real_i_10_ce1 sc_out sc_logic 1 signal 10 } 
	{ real_i_10_d1 sc_out sc_lv 32 signal 10 } 
	{ real_i_10_q1 sc_in sc_lv 32 signal 10 } 
	{ real_i_10_we1 sc_out sc_logic 1 signal 10 } 
	{ real_i_11_address0 sc_out sc_lv 6 signal 11 } 
	{ real_i_11_ce0 sc_out sc_logic 1 signal 11 } 
	{ real_i_11_d0 sc_out sc_lv 32 signal 11 } 
	{ real_i_11_q0 sc_in sc_lv 32 signal 11 } 
	{ real_i_11_we0 sc_out sc_logic 1 signal 11 } 
	{ real_i_11_address1 sc_out sc_lv 6 signal 11 } 
	{ real_i_11_ce1 sc_out sc_logic 1 signal 11 } 
	{ real_i_11_d1 sc_out sc_lv 32 signal 11 } 
	{ real_i_11_q1 sc_in sc_lv 32 signal 11 } 
	{ real_i_11_we1 sc_out sc_logic 1 signal 11 } 
	{ real_i_12_address0 sc_out sc_lv 6 signal 12 } 
	{ real_i_12_ce0 sc_out sc_logic 1 signal 12 } 
	{ real_i_12_d0 sc_out sc_lv 32 signal 12 } 
	{ real_i_12_q0 sc_in sc_lv 32 signal 12 } 
	{ real_i_12_we0 sc_out sc_logic 1 signal 12 } 
	{ real_i_12_address1 sc_out sc_lv 6 signal 12 } 
	{ real_i_12_ce1 sc_out sc_logic 1 signal 12 } 
	{ real_i_12_d1 sc_out sc_lv 32 signal 12 } 
	{ real_i_12_q1 sc_in sc_lv 32 signal 12 } 
	{ real_i_12_we1 sc_out sc_logic 1 signal 12 } 
	{ real_i_13_address0 sc_out sc_lv 6 signal 13 } 
	{ real_i_13_ce0 sc_out sc_logic 1 signal 13 } 
	{ real_i_13_d0 sc_out sc_lv 32 signal 13 } 
	{ real_i_13_q0 sc_in sc_lv 32 signal 13 } 
	{ real_i_13_we0 sc_out sc_logic 1 signal 13 } 
	{ real_i_13_address1 sc_out sc_lv 6 signal 13 } 
	{ real_i_13_ce1 sc_out sc_logic 1 signal 13 } 
	{ real_i_13_d1 sc_out sc_lv 32 signal 13 } 
	{ real_i_13_q1 sc_in sc_lv 32 signal 13 } 
	{ real_i_13_we1 sc_out sc_logic 1 signal 13 } 
	{ real_i_14_address0 sc_out sc_lv 6 signal 14 } 
	{ real_i_14_ce0 sc_out sc_logic 1 signal 14 } 
	{ real_i_14_d0 sc_out sc_lv 32 signal 14 } 
	{ real_i_14_q0 sc_in sc_lv 32 signal 14 } 
	{ real_i_14_we0 sc_out sc_logic 1 signal 14 } 
	{ real_i_14_address1 sc_out sc_lv 6 signal 14 } 
	{ real_i_14_ce1 sc_out sc_logic 1 signal 14 } 
	{ real_i_14_d1 sc_out sc_lv 32 signal 14 } 
	{ real_i_14_q1 sc_in sc_lv 32 signal 14 } 
	{ real_i_14_we1 sc_out sc_logic 1 signal 14 } 
	{ real_i_15_address0 sc_out sc_lv 6 signal 15 } 
	{ real_i_15_ce0 sc_out sc_logic 1 signal 15 } 
	{ real_i_15_d0 sc_out sc_lv 32 signal 15 } 
	{ real_i_15_q0 sc_in sc_lv 32 signal 15 } 
	{ real_i_15_we0 sc_out sc_logic 1 signal 15 } 
	{ real_i_15_address1 sc_out sc_lv 6 signal 15 } 
	{ real_i_15_ce1 sc_out sc_logic 1 signal 15 } 
	{ real_i_15_d1 sc_out sc_lv 32 signal 15 } 
	{ real_i_15_q1 sc_in sc_lv 32 signal 15 } 
	{ real_i_15_we1 sc_out sc_logic 1 signal 15 } 
	{ imag_i_0_address0 sc_out sc_lv 6 signal 16 } 
	{ imag_i_0_ce0 sc_out sc_logic 1 signal 16 } 
	{ imag_i_0_d0 sc_out sc_lv 32 signal 16 } 
	{ imag_i_0_q0 sc_in sc_lv 32 signal 16 } 
	{ imag_i_0_we0 sc_out sc_logic 1 signal 16 } 
	{ imag_i_0_address1 sc_out sc_lv 6 signal 16 } 
	{ imag_i_0_ce1 sc_out sc_logic 1 signal 16 } 
	{ imag_i_0_d1 sc_out sc_lv 32 signal 16 } 
	{ imag_i_0_q1 sc_in sc_lv 32 signal 16 } 
	{ imag_i_0_we1 sc_out sc_logic 1 signal 16 } 
	{ imag_i_1_address0 sc_out sc_lv 6 signal 17 } 
	{ imag_i_1_ce0 sc_out sc_logic 1 signal 17 } 
	{ imag_i_1_d0 sc_out sc_lv 32 signal 17 } 
	{ imag_i_1_q0 sc_in sc_lv 32 signal 17 } 
	{ imag_i_1_we0 sc_out sc_logic 1 signal 17 } 
	{ imag_i_1_address1 sc_out sc_lv 6 signal 17 } 
	{ imag_i_1_ce1 sc_out sc_logic 1 signal 17 } 
	{ imag_i_1_d1 sc_out sc_lv 32 signal 17 } 
	{ imag_i_1_q1 sc_in sc_lv 32 signal 17 } 
	{ imag_i_1_we1 sc_out sc_logic 1 signal 17 } 
	{ imag_i_2_address0 sc_out sc_lv 6 signal 18 } 
	{ imag_i_2_ce0 sc_out sc_logic 1 signal 18 } 
	{ imag_i_2_d0 sc_out sc_lv 32 signal 18 } 
	{ imag_i_2_q0 sc_in sc_lv 32 signal 18 } 
	{ imag_i_2_we0 sc_out sc_logic 1 signal 18 } 
	{ imag_i_2_address1 sc_out sc_lv 6 signal 18 } 
	{ imag_i_2_ce1 sc_out sc_logic 1 signal 18 } 
	{ imag_i_2_d1 sc_out sc_lv 32 signal 18 } 
	{ imag_i_2_q1 sc_in sc_lv 32 signal 18 } 
	{ imag_i_2_we1 sc_out sc_logic 1 signal 18 } 
	{ imag_i_3_address0 sc_out sc_lv 6 signal 19 } 
	{ imag_i_3_ce0 sc_out sc_logic 1 signal 19 } 
	{ imag_i_3_d0 sc_out sc_lv 32 signal 19 } 
	{ imag_i_3_q0 sc_in sc_lv 32 signal 19 } 
	{ imag_i_3_we0 sc_out sc_logic 1 signal 19 } 
	{ imag_i_3_address1 sc_out sc_lv 6 signal 19 } 
	{ imag_i_3_ce1 sc_out sc_logic 1 signal 19 } 
	{ imag_i_3_d1 sc_out sc_lv 32 signal 19 } 
	{ imag_i_3_q1 sc_in sc_lv 32 signal 19 } 
	{ imag_i_3_we1 sc_out sc_logic 1 signal 19 } 
	{ imag_i_4_address0 sc_out sc_lv 6 signal 20 } 
	{ imag_i_4_ce0 sc_out sc_logic 1 signal 20 } 
	{ imag_i_4_d0 sc_out sc_lv 32 signal 20 } 
	{ imag_i_4_q0 sc_in sc_lv 32 signal 20 } 
	{ imag_i_4_we0 sc_out sc_logic 1 signal 20 } 
	{ imag_i_4_address1 sc_out sc_lv 6 signal 20 } 
	{ imag_i_4_ce1 sc_out sc_logic 1 signal 20 } 
	{ imag_i_4_d1 sc_out sc_lv 32 signal 20 } 
	{ imag_i_4_q1 sc_in sc_lv 32 signal 20 } 
	{ imag_i_4_we1 sc_out sc_logic 1 signal 20 } 
	{ imag_i_5_address0 sc_out sc_lv 6 signal 21 } 
	{ imag_i_5_ce0 sc_out sc_logic 1 signal 21 } 
	{ imag_i_5_d0 sc_out sc_lv 32 signal 21 } 
	{ imag_i_5_q0 sc_in sc_lv 32 signal 21 } 
	{ imag_i_5_we0 sc_out sc_logic 1 signal 21 } 
	{ imag_i_5_address1 sc_out sc_lv 6 signal 21 } 
	{ imag_i_5_ce1 sc_out sc_logic 1 signal 21 } 
	{ imag_i_5_d1 sc_out sc_lv 32 signal 21 } 
	{ imag_i_5_q1 sc_in sc_lv 32 signal 21 } 
	{ imag_i_5_we1 sc_out sc_logic 1 signal 21 } 
	{ imag_i_6_address0 sc_out sc_lv 6 signal 22 } 
	{ imag_i_6_ce0 sc_out sc_logic 1 signal 22 } 
	{ imag_i_6_d0 sc_out sc_lv 32 signal 22 } 
	{ imag_i_6_q0 sc_in sc_lv 32 signal 22 } 
	{ imag_i_6_we0 sc_out sc_logic 1 signal 22 } 
	{ imag_i_6_address1 sc_out sc_lv 6 signal 22 } 
	{ imag_i_6_ce1 sc_out sc_logic 1 signal 22 } 
	{ imag_i_6_d1 sc_out sc_lv 32 signal 22 } 
	{ imag_i_6_q1 sc_in sc_lv 32 signal 22 } 
	{ imag_i_6_we1 sc_out sc_logic 1 signal 22 } 
	{ imag_i_7_address0 sc_out sc_lv 6 signal 23 } 
	{ imag_i_7_ce0 sc_out sc_logic 1 signal 23 } 
	{ imag_i_7_d0 sc_out sc_lv 32 signal 23 } 
	{ imag_i_7_q0 sc_in sc_lv 32 signal 23 } 
	{ imag_i_7_we0 sc_out sc_logic 1 signal 23 } 
	{ imag_i_7_address1 sc_out sc_lv 6 signal 23 } 
	{ imag_i_7_ce1 sc_out sc_logic 1 signal 23 } 
	{ imag_i_7_d1 sc_out sc_lv 32 signal 23 } 
	{ imag_i_7_q1 sc_in sc_lv 32 signal 23 } 
	{ imag_i_7_we1 sc_out sc_logic 1 signal 23 } 
	{ imag_i_8_address0 sc_out sc_lv 6 signal 24 } 
	{ imag_i_8_ce0 sc_out sc_logic 1 signal 24 } 
	{ imag_i_8_d0 sc_out sc_lv 32 signal 24 } 
	{ imag_i_8_q0 sc_in sc_lv 32 signal 24 } 
	{ imag_i_8_we0 sc_out sc_logic 1 signal 24 } 
	{ imag_i_8_address1 sc_out sc_lv 6 signal 24 } 
	{ imag_i_8_ce1 sc_out sc_logic 1 signal 24 } 
	{ imag_i_8_d1 sc_out sc_lv 32 signal 24 } 
	{ imag_i_8_q1 sc_in sc_lv 32 signal 24 } 
	{ imag_i_8_we1 sc_out sc_logic 1 signal 24 } 
	{ imag_i_9_address0 sc_out sc_lv 6 signal 25 } 
	{ imag_i_9_ce0 sc_out sc_logic 1 signal 25 } 
	{ imag_i_9_d0 sc_out sc_lv 32 signal 25 } 
	{ imag_i_9_q0 sc_in sc_lv 32 signal 25 } 
	{ imag_i_9_we0 sc_out sc_logic 1 signal 25 } 
	{ imag_i_9_address1 sc_out sc_lv 6 signal 25 } 
	{ imag_i_9_ce1 sc_out sc_logic 1 signal 25 } 
	{ imag_i_9_d1 sc_out sc_lv 32 signal 25 } 
	{ imag_i_9_q1 sc_in sc_lv 32 signal 25 } 
	{ imag_i_9_we1 sc_out sc_logic 1 signal 25 } 
	{ imag_i_10_address0 sc_out sc_lv 6 signal 26 } 
	{ imag_i_10_ce0 sc_out sc_logic 1 signal 26 } 
	{ imag_i_10_d0 sc_out sc_lv 32 signal 26 } 
	{ imag_i_10_q0 sc_in sc_lv 32 signal 26 } 
	{ imag_i_10_we0 sc_out sc_logic 1 signal 26 } 
	{ imag_i_10_address1 sc_out sc_lv 6 signal 26 } 
	{ imag_i_10_ce1 sc_out sc_logic 1 signal 26 } 
	{ imag_i_10_d1 sc_out sc_lv 32 signal 26 } 
	{ imag_i_10_q1 sc_in sc_lv 32 signal 26 } 
	{ imag_i_10_we1 sc_out sc_logic 1 signal 26 } 
	{ imag_i_11_address0 sc_out sc_lv 6 signal 27 } 
	{ imag_i_11_ce0 sc_out sc_logic 1 signal 27 } 
	{ imag_i_11_d0 sc_out sc_lv 32 signal 27 } 
	{ imag_i_11_q0 sc_in sc_lv 32 signal 27 } 
	{ imag_i_11_we0 sc_out sc_logic 1 signal 27 } 
	{ imag_i_11_address1 sc_out sc_lv 6 signal 27 } 
	{ imag_i_11_ce1 sc_out sc_logic 1 signal 27 } 
	{ imag_i_11_d1 sc_out sc_lv 32 signal 27 } 
	{ imag_i_11_q1 sc_in sc_lv 32 signal 27 } 
	{ imag_i_11_we1 sc_out sc_logic 1 signal 27 } 
	{ imag_i_12_address0 sc_out sc_lv 6 signal 28 } 
	{ imag_i_12_ce0 sc_out sc_logic 1 signal 28 } 
	{ imag_i_12_d0 sc_out sc_lv 32 signal 28 } 
	{ imag_i_12_q0 sc_in sc_lv 32 signal 28 } 
	{ imag_i_12_we0 sc_out sc_logic 1 signal 28 } 
	{ imag_i_12_address1 sc_out sc_lv 6 signal 28 } 
	{ imag_i_12_ce1 sc_out sc_logic 1 signal 28 } 
	{ imag_i_12_d1 sc_out sc_lv 32 signal 28 } 
	{ imag_i_12_q1 sc_in sc_lv 32 signal 28 } 
	{ imag_i_12_we1 sc_out sc_logic 1 signal 28 } 
	{ imag_i_13_address0 sc_out sc_lv 6 signal 29 } 
	{ imag_i_13_ce0 sc_out sc_logic 1 signal 29 } 
	{ imag_i_13_d0 sc_out sc_lv 32 signal 29 } 
	{ imag_i_13_q0 sc_in sc_lv 32 signal 29 } 
	{ imag_i_13_we0 sc_out sc_logic 1 signal 29 } 
	{ imag_i_13_address1 sc_out sc_lv 6 signal 29 } 
	{ imag_i_13_ce1 sc_out sc_logic 1 signal 29 } 
	{ imag_i_13_d1 sc_out sc_lv 32 signal 29 } 
	{ imag_i_13_q1 sc_in sc_lv 32 signal 29 } 
	{ imag_i_13_we1 sc_out sc_logic 1 signal 29 } 
	{ imag_i_14_address0 sc_out sc_lv 6 signal 30 } 
	{ imag_i_14_ce0 sc_out sc_logic 1 signal 30 } 
	{ imag_i_14_d0 sc_out sc_lv 32 signal 30 } 
	{ imag_i_14_q0 sc_in sc_lv 32 signal 30 } 
	{ imag_i_14_we0 sc_out sc_logic 1 signal 30 } 
	{ imag_i_14_address1 sc_out sc_lv 6 signal 30 } 
	{ imag_i_14_ce1 sc_out sc_logic 1 signal 30 } 
	{ imag_i_14_d1 sc_out sc_lv 32 signal 30 } 
	{ imag_i_14_q1 sc_in sc_lv 32 signal 30 } 
	{ imag_i_14_we1 sc_out sc_logic 1 signal 30 } 
	{ imag_i_15_address0 sc_out sc_lv 6 signal 31 } 
	{ imag_i_15_ce0 sc_out sc_logic 1 signal 31 } 
	{ imag_i_15_d0 sc_out sc_lv 32 signal 31 } 
	{ imag_i_15_q0 sc_in sc_lv 32 signal 31 } 
	{ imag_i_15_we0 sc_out sc_logic 1 signal 31 } 
	{ imag_i_15_address1 sc_out sc_lv 6 signal 31 } 
	{ imag_i_15_ce1 sc_out sc_logic 1 signal 31 } 
	{ imag_i_15_d1 sc_out sc_lv 32 signal 31 } 
	{ imag_i_15_q1 sc_in sc_lv 32 signal 31 } 
	{ imag_i_15_we1 sc_out sc_logic 1 signal 31 } 
	{ real_o_address0 sc_out sc_lv 10 signal 32 } 
	{ real_o_ce0 sc_out sc_logic 1 signal 32 } 
	{ real_o_d0 sc_out sc_lv 32 signal 32 } 
	{ real_o_q0 sc_in sc_lv 32 signal 32 } 
	{ real_o_we0 sc_out sc_logic 1 signal 32 } 
	{ real_o_address1 sc_out sc_lv 10 signal 32 } 
	{ real_o_ce1 sc_out sc_logic 1 signal 32 } 
	{ real_o_d1 sc_out sc_lv 32 signal 32 } 
	{ real_o_q1 sc_in sc_lv 32 signal 32 } 
	{ real_o_we1 sc_out sc_logic 1 signal 32 } 
	{ imag_o_address0 sc_out sc_lv 10 signal 33 } 
	{ imag_o_ce0 sc_out sc_logic 1 signal 33 } 
	{ imag_o_d0 sc_out sc_lv 32 signal 33 } 
	{ imag_o_q0 sc_in sc_lv 32 signal 33 } 
	{ imag_o_we0 sc_out sc_logic 1 signal 33 } 
	{ imag_o_address1 sc_out sc_lv 10 signal 33 } 
	{ imag_o_ce1 sc_out sc_logic 1 signal 33 } 
	{ imag_o_d1 sc_out sc_lv 32 signal 33 } 
	{ imag_o_q1 sc_in sc_lv 32 signal 33 } 
	{ imag_o_we1 sc_out sc_logic 1 signal 33 } 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
}
set NewPortList {[ 
	{ "name": "real_i_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_0", "role": "address0" }} , 
 	{ "name": "real_i_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "ce0" }} , 
 	{ "name": "real_i_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "d0" }} , 
 	{ "name": "real_i_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "q0" }} , 
 	{ "name": "real_i_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "we0" }} , 
 	{ "name": "real_i_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_0", "role": "address1" }} , 
 	{ "name": "real_i_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "ce1" }} , 
 	{ "name": "real_i_0_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "d1" }} , 
 	{ "name": "real_i_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "q1" }} , 
 	{ "name": "real_i_0_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "we1" }} , 
 	{ "name": "real_i_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_1", "role": "address0" }} , 
 	{ "name": "real_i_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "ce0" }} , 
 	{ "name": "real_i_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "d0" }} , 
 	{ "name": "real_i_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "q0" }} , 
 	{ "name": "real_i_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "we0" }} , 
 	{ "name": "real_i_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_1", "role": "address1" }} , 
 	{ "name": "real_i_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "ce1" }} , 
 	{ "name": "real_i_1_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "d1" }} , 
 	{ "name": "real_i_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "q1" }} , 
 	{ "name": "real_i_1_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "we1" }} , 
 	{ "name": "real_i_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_2", "role": "address0" }} , 
 	{ "name": "real_i_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_2", "role": "ce0" }} , 
 	{ "name": "real_i_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_2", "role": "d0" }} , 
 	{ "name": "real_i_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_2", "role": "q0" }} , 
 	{ "name": "real_i_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_2", "role": "we0" }} , 
 	{ "name": "real_i_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_2", "role": "address1" }} , 
 	{ "name": "real_i_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_2", "role": "ce1" }} , 
 	{ "name": "real_i_2_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_2", "role": "d1" }} , 
 	{ "name": "real_i_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_2", "role": "q1" }} , 
 	{ "name": "real_i_2_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_2", "role": "we1" }} , 
 	{ "name": "real_i_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_3", "role": "address0" }} , 
 	{ "name": "real_i_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_3", "role": "ce0" }} , 
 	{ "name": "real_i_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_3", "role": "d0" }} , 
 	{ "name": "real_i_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_3", "role": "q0" }} , 
 	{ "name": "real_i_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_3", "role": "we0" }} , 
 	{ "name": "real_i_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_3", "role": "address1" }} , 
 	{ "name": "real_i_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_3", "role": "ce1" }} , 
 	{ "name": "real_i_3_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_3", "role": "d1" }} , 
 	{ "name": "real_i_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_3", "role": "q1" }} , 
 	{ "name": "real_i_3_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_3", "role": "we1" }} , 
 	{ "name": "real_i_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_4", "role": "address0" }} , 
 	{ "name": "real_i_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_4", "role": "ce0" }} , 
 	{ "name": "real_i_4_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_4", "role": "d0" }} , 
 	{ "name": "real_i_4_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_4", "role": "q0" }} , 
 	{ "name": "real_i_4_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_4", "role": "we0" }} , 
 	{ "name": "real_i_4_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_4", "role": "address1" }} , 
 	{ "name": "real_i_4_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_4", "role": "ce1" }} , 
 	{ "name": "real_i_4_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_4", "role": "d1" }} , 
 	{ "name": "real_i_4_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_4", "role": "q1" }} , 
 	{ "name": "real_i_4_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_4", "role": "we1" }} , 
 	{ "name": "real_i_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_5", "role": "address0" }} , 
 	{ "name": "real_i_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_5", "role": "ce0" }} , 
 	{ "name": "real_i_5_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_5", "role": "d0" }} , 
 	{ "name": "real_i_5_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_5", "role": "q0" }} , 
 	{ "name": "real_i_5_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_5", "role": "we0" }} , 
 	{ "name": "real_i_5_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_5", "role": "address1" }} , 
 	{ "name": "real_i_5_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_5", "role": "ce1" }} , 
 	{ "name": "real_i_5_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_5", "role": "d1" }} , 
 	{ "name": "real_i_5_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_5", "role": "q1" }} , 
 	{ "name": "real_i_5_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_5", "role": "we1" }} , 
 	{ "name": "real_i_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_6", "role": "address0" }} , 
 	{ "name": "real_i_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_6", "role": "ce0" }} , 
 	{ "name": "real_i_6_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_6", "role": "d0" }} , 
 	{ "name": "real_i_6_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_6", "role": "q0" }} , 
 	{ "name": "real_i_6_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_6", "role": "we0" }} , 
 	{ "name": "real_i_6_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_6", "role": "address1" }} , 
 	{ "name": "real_i_6_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_6", "role": "ce1" }} , 
 	{ "name": "real_i_6_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_6", "role": "d1" }} , 
 	{ "name": "real_i_6_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_6", "role": "q1" }} , 
 	{ "name": "real_i_6_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_6", "role": "we1" }} , 
 	{ "name": "real_i_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_7", "role": "address0" }} , 
 	{ "name": "real_i_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_7", "role": "ce0" }} , 
 	{ "name": "real_i_7_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_7", "role": "d0" }} , 
 	{ "name": "real_i_7_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_7", "role": "q0" }} , 
 	{ "name": "real_i_7_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_7", "role": "we0" }} , 
 	{ "name": "real_i_7_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_7", "role": "address1" }} , 
 	{ "name": "real_i_7_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_7", "role": "ce1" }} , 
 	{ "name": "real_i_7_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_7", "role": "d1" }} , 
 	{ "name": "real_i_7_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_7", "role": "q1" }} , 
 	{ "name": "real_i_7_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_7", "role": "we1" }} , 
 	{ "name": "real_i_8_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_8", "role": "address0" }} , 
 	{ "name": "real_i_8_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_8", "role": "ce0" }} , 
 	{ "name": "real_i_8_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_8", "role": "d0" }} , 
 	{ "name": "real_i_8_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_8", "role": "q0" }} , 
 	{ "name": "real_i_8_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_8", "role": "we0" }} , 
 	{ "name": "real_i_8_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_8", "role": "address1" }} , 
 	{ "name": "real_i_8_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_8", "role": "ce1" }} , 
 	{ "name": "real_i_8_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_8", "role": "d1" }} , 
 	{ "name": "real_i_8_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_8", "role": "q1" }} , 
 	{ "name": "real_i_8_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_8", "role": "we1" }} , 
 	{ "name": "real_i_9_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_9", "role": "address0" }} , 
 	{ "name": "real_i_9_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_9", "role": "ce0" }} , 
 	{ "name": "real_i_9_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_9", "role": "d0" }} , 
 	{ "name": "real_i_9_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_9", "role": "q0" }} , 
 	{ "name": "real_i_9_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_9", "role": "we0" }} , 
 	{ "name": "real_i_9_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_9", "role": "address1" }} , 
 	{ "name": "real_i_9_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_9", "role": "ce1" }} , 
 	{ "name": "real_i_9_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_9", "role": "d1" }} , 
 	{ "name": "real_i_9_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_9", "role": "q1" }} , 
 	{ "name": "real_i_9_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_9", "role": "we1" }} , 
 	{ "name": "real_i_10_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_10", "role": "address0" }} , 
 	{ "name": "real_i_10_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_10", "role": "ce0" }} , 
 	{ "name": "real_i_10_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_10", "role": "d0" }} , 
 	{ "name": "real_i_10_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_10", "role": "q0" }} , 
 	{ "name": "real_i_10_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_10", "role": "we0" }} , 
 	{ "name": "real_i_10_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_10", "role": "address1" }} , 
 	{ "name": "real_i_10_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_10", "role": "ce1" }} , 
 	{ "name": "real_i_10_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_10", "role": "d1" }} , 
 	{ "name": "real_i_10_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_10", "role": "q1" }} , 
 	{ "name": "real_i_10_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_10", "role": "we1" }} , 
 	{ "name": "real_i_11_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_11", "role": "address0" }} , 
 	{ "name": "real_i_11_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_11", "role": "ce0" }} , 
 	{ "name": "real_i_11_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_11", "role": "d0" }} , 
 	{ "name": "real_i_11_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_11", "role": "q0" }} , 
 	{ "name": "real_i_11_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_11", "role": "we0" }} , 
 	{ "name": "real_i_11_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_11", "role": "address1" }} , 
 	{ "name": "real_i_11_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_11", "role": "ce1" }} , 
 	{ "name": "real_i_11_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_11", "role": "d1" }} , 
 	{ "name": "real_i_11_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_11", "role": "q1" }} , 
 	{ "name": "real_i_11_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_11", "role": "we1" }} , 
 	{ "name": "real_i_12_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_12", "role": "address0" }} , 
 	{ "name": "real_i_12_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_12", "role": "ce0" }} , 
 	{ "name": "real_i_12_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_12", "role": "d0" }} , 
 	{ "name": "real_i_12_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_12", "role": "q0" }} , 
 	{ "name": "real_i_12_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_12", "role": "we0" }} , 
 	{ "name": "real_i_12_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_12", "role": "address1" }} , 
 	{ "name": "real_i_12_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_12", "role": "ce1" }} , 
 	{ "name": "real_i_12_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_12", "role": "d1" }} , 
 	{ "name": "real_i_12_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_12", "role": "q1" }} , 
 	{ "name": "real_i_12_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_12", "role": "we1" }} , 
 	{ "name": "real_i_13_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_13", "role": "address0" }} , 
 	{ "name": "real_i_13_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_13", "role": "ce0" }} , 
 	{ "name": "real_i_13_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_13", "role": "d0" }} , 
 	{ "name": "real_i_13_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_13", "role": "q0" }} , 
 	{ "name": "real_i_13_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_13", "role": "we0" }} , 
 	{ "name": "real_i_13_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_13", "role": "address1" }} , 
 	{ "name": "real_i_13_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_13", "role": "ce1" }} , 
 	{ "name": "real_i_13_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_13", "role": "d1" }} , 
 	{ "name": "real_i_13_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_13", "role": "q1" }} , 
 	{ "name": "real_i_13_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_13", "role": "we1" }} , 
 	{ "name": "real_i_14_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_14", "role": "address0" }} , 
 	{ "name": "real_i_14_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_14", "role": "ce0" }} , 
 	{ "name": "real_i_14_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_14", "role": "d0" }} , 
 	{ "name": "real_i_14_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_14", "role": "q0" }} , 
 	{ "name": "real_i_14_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_14", "role": "we0" }} , 
 	{ "name": "real_i_14_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_14", "role": "address1" }} , 
 	{ "name": "real_i_14_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_14", "role": "ce1" }} , 
 	{ "name": "real_i_14_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_14", "role": "d1" }} , 
 	{ "name": "real_i_14_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_14", "role": "q1" }} , 
 	{ "name": "real_i_14_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_14", "role": "we1" }} , 
 	{ "name": "real_i_15_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_15", "role": "address0" }} , 
 	{ "name": "real_i_15_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_15", "role": "ce0" }} , 
 	{ "name": "real_i_15_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_15", "role": "d0" }} , 
 	{ "name": "real_i_15_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_15", "role": "q0" }} , 
 	{ "name": "real_i_15_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_15", "role": "we0" }} , 
 	{ "name": "real_i_15_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_i_15", "role": "address1" }} , 
 	{ "name": "real_i_15_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_15", "role": "ce1" }} , 
 	{ "name": "real_i_15_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_15", "role": "d1" }} , 
 	{ "name": "real_i_15_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_15", "role": "q1" }} , 
 	{ "name": "real_i_15_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_15", "role": "we1" }} , 
 	{ "name": "imag_i_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_0", "role": "address0" }} , 
 	{ "name": "imag_i_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_0", "role": "ce0" }} , 
 	{ "name": "imag_i_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_0", "role": "d0" }} , 
 	{ "name": "imag_i_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_0", "role": "q0" }} , 
 	{ "name": "imag_i_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_0", "role": "we0" }} , 
 	{ "name": "imag_i_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_0", "role": "address1" }} , 
 	{ "name": "imag_i_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_0", "role": "ce1" }} , 
 	{ "name": "imag_i_0_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_0", "role": "d1" }} , 
 	{ "name": "imag_i_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_0", "role": "q1" }} , 
 	{ "name": "imag_i_0_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_0", "role": "we1" }} , 
 	{ "name": "imag_i_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_1", "role": "address0" }} , 
 	{ "name": "imag_i_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_1", "role": "ce0" }} , 
 	{ "name": "imag_i_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_1", "role": "d0" }} , 
 	{ "name": "imag_i_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_1", "role": "q0" }} , 
 	{ "name": "imag_i_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_1", "role": "we0" }} , 
 	{ "name": "imag_i_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_1", "role": "address1" }} , 
 	{ "name": "imag_i_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_1", "role": "ce1" }} , 
 	{ "name": "imag_i_1_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_1", "role": "d1" }} , 
 	{ "name": "imag_i_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_1", "role": "q1" }} , 
 	{ "name": "imag_i_1_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_1", "role": "we1" }} , 
 	{ "name": "imag_i_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_2", "role": "address0" }} , 
 	{ "name": "imag_i_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_2", "role": "ce0" }} , 
 	{ "name": "imag_i_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_2", "role": "d0" }} , 
 	{ "name": "imag_i_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_2", "role": "q0" }} , 
 	{ "name": "imag_i_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_2", "role": "we0" }} , 
 	{ "name": "imag_i_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_2", "role": "address1" }} , 
 	{ "name": "imag_i_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_2", "role": "ce1" }} , 
 	{ "name": "imag_i_2_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_2", "role": "d1" }} , 
 	{ "name": "imag_i_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_2", "role": "q1" }} , 
 	{ "name": "imag_i_2_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_2", "role": "we1" }} , 
 	{ "name": "imag_i_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_3", "role": "address0" }} , 
 	{ "name": "imag_i_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_3", "role": "ce0" }} , 
 	{ "name": "imag_i_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_3", "role": "d0" }} , 
 	{ "name": "imag_i_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_3", "role": "q0" }} , 
 	{ "name": "imag_i_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_3", "role": "we0" }} , 
 	{ "name": "imag_i_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_3", "role": "address1" }} , 
 	{ "name": "imag_i_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_3", "role": "ce1" }} , 
 	{ "name": "imag_i_3_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_3", "role": "d1" }} , 
 	{ "name": "imag_i_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_3", "role": "q1" }} , 
 	{ "name": "imag_i_3_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_3", "role": "we1" }} , 
 	{ "name": "imag_i_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_4", "role": "address0" }} , 
 	{ "name": "imag_i_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_4", "role": "ce0" }} , 
 	{ "name": "imag_i_4_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_4", "role": "d0" }} , 
 	{ "name": "imag_i_4_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_4", "role": "q0" }} , 
 	{ "name": "imag_i_4_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_4", "role": "we0" }} , 
 	{ "name": "imag_i_4_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_4", "role": "address1" }} , 
 	{ "name": "imag_i_4_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_4", "role": "ce1" }} , 
 	{ "name": "imag_i_4_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_4", "role": "d1" }} , 
 	{ "name": "imag_i_4_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_4", "role": "q1" }} , 
 	{ "name": "imag_i_4_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_4", "role": "we1" }} , 
 	{ "name": "imag_i_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_5", "role": "address0" }} , 
 	{ "name": "imag_i_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_5", "role": "ce0" }} , 
 	{ "name": "imag_i_5_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_5", "role": "d0" }} , 
 	{ "name": "imag_i_5_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_5", "role": "q0" }} , 
 	{ "name": "imag_i_5_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_5", "role": "we0" }} , 
 	{ "name": "imag_i_5_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_5", "role": "address1" }} , 
 	{ "name": "imag_i_5_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_5", "role": "ce1" }} , 
 	{ "name": "imag_i_5_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_5", "role": "d1" }} , 
 	{ "name": "imag_i_5_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_5", "role": "q1" }} , 
 	{ "name": "imag_i_5_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_5", "role": "we1" }} , 
 	{ "name": "imag_i_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_6", "role": "address0" }} , 
 	{ "name": "imag_i_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_6", "role": "ce0" }} , 
 	{ "name": "imag_i_6_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_6", "role": "d0" }} , 
 	{ "name": "imag_i_6_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_6", "role": "q0" }} , 
 	{ "name": "imag_i_6_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_6", "role": "we0" }} , 
 	{ "name": "imag_i_6_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_6", "role": "address1" }} , 
 	{ "name": "imag_i_6_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_6", "role": "ce1" }} , 
 	{ "name": "imag_i_6_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_6", "role": "d1" }} , 
 	{ "name": "imag_i_6_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_6", "role": "q1" }} , 
 	{ "name": "imag_i_6_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_6", "role": "we1" }} , 
 	{ "name": "imag_i_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_7", "role": "address0" }} , 
 	{ "name": "imag_i_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_7", "role": "ce0" }} , 
 	{ "name": "imag_i_7_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_7", "role": "d0" }} , 
 	{ "name": "imag_i_7_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_7", "role": "q0" }} , 
 	{ "name": "imag_i_7_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_7", "role": "we0" }} , 
 	{ "name": "imag_i_7_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_7", "role": "address1" }} , 
 	{ "name": "imag_i_7_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_7", "role": "ce1" }} , 
 	{ "name": "imag_i_7_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_7", "role": "d1" }} , 
 	{ "name": "imag_i_7_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_7", "role": "q1" }} , 
 	{ "name": "imag_i_7_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_7", "role": "we1" }} , 
 	{ "name": "imag_i_8_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_8", "role": "address0" }} , 
 	{ "name": "imag_i_8_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_8", "role": "ce0" }} , 
 	{ "name": "imag_i_8_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_8", "role": "d0" }} , 
 	{ "name": "imag_i_8_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_8", "role": "q0" }} , 
 	{ "name": "imag_i_8_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_8", "role": "we0" }} , 
 	{ "name": "imag_i_8_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_8", "role": "address1" }} , 
 	{ "name": "imag_i_8_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_8", "role": "ce1" }} , 
 	{ "name": "imag_i_8_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_8", "role": "d1" }} , 
 	{ "name": "imag_i_8_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_8", "role": "q1" }} , 
 	{ "name": "imag_i_8_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_8", "role": "we1" }} , 
 	{ "name": "imag_i_9_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_9", "role": "address0" }} , 
 	{ "name": "imag_i_9_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_9", "role": "ce0" }} , 
 	{ "name": "imag_i_9_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_9", "role": "d0" }} , 
 	{ "name": "imag_i_9_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_9", "role": "q0" }} , 
 	{ "name": "imag_i_9_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_9", "role": "we0" }} , 
 	{ "name": "imag_i_9_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_9", "role": "address1" }} , 
 	{ "name": "imag_i_9_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_9", "role": "ce1" }} , 
 	{ "name": "imag_i_9_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_9", "role": "d1" }} , 
 	{ "name": "imag_i_9_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_9", "role": "q1" }} , 
 	{ "name": "imag_i_9_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_9", "role": "we1" }} , 
 	{ "name": "imag_i_10_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_10", "role": "address0" }} , 
 	{ "name": "imag_i_10_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_10", "role": "ce0" }} , 
 	{ "name": "imag_i_10_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_10", "role": "d0" }} , 
 	{ "name": "imag_i_10_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_10", "role": "q0" }} , 
 	{ "name": "imag_i_10_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_10", "role": "we0" }} , 
 	{ "name": "imag_i_10_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_10", "role": "address1" }} , 
 	{ "name": "imag_i_10_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_10", "role": "ce1" }} , 
 	{ "name": "imag_i_10_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_10", "role": "d1" }} , 
 	{ "name": "imag_i_10_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_10", "role": "q1" }} , 
 	{ "name": "imag_i_10_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_10", "role": "we1" }} , 
 	{ "name": "imag_i_11_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_11", "role": "address0" }} , 
 	{ "name": "imag_i_11_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_11", "role": "ce0" }} , 
 	{ "name": "imag_i_11_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_11", "role": "d0" }} , 
 	{ "name": "imag_i_11_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_11", "role": "q0" }} , 
 	{ "name": "imag_i_11_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_11", "role": "we0" }} , 
 	{ "name": "imag_i_11_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_11", "role": "address1" }} , 
 	{ "name": "imag_i_11_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_11", "role": "ce1" }} , 
 	{ "name": "imag_i_11_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_11", "role": "d1" }} , 
 	{ "name": "imag_i_11_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_11", "role": "q1" }} , 
 	{ "name": "imag_i_11_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_11", "role": "we1" }} , 
 	{ "name": "imag_i_12_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_12", "role": "address0" }} , 
 	{ "name": "imag_i_12_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_12", "role": "ce0" }} , 
 	{ "name": "imag_i_12_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_12", "role": "d0" }} , 
 	{ "name": "imag_i_12_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_12", "role": "q0" }} , 
 	{ "name": "imag_i_12_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_12", "role": "we0" }} , 
 	{ "name": "imag_i_12_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_12", "role": "address1" }} , 
 	{ "name": "imag_i_12_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_12", "role": "ce1" }} , 
 	{ "name": "imag_i_12_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_12", "role": "d1" }} , 
 	{ "name": "imag_i_12_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_12", "role": "q1" }} , 
 	{ "name": "imag_i_12_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_12", "role": "we1" }} , 
 	{ "name": "imag_i_13_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_13", "role": "address0" }} , 
 	{ "name": "imag_i_13_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_13", "role": "ce0" }} , 
 	{ "name": "imag_i_13_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_13", "role": "d0" }} , 
 	{ "name": "imag_i_13_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_13", "role": "q0" }} , 
 	{ "name": "imag_i_13_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_13", "role": "we0" }} , 
 	{ "name": "imag_i_13_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_13", "role": "address1" }} , 
 	{ "name": "imag_i_13_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_13", "role": "ce1" }} , 
 	{ "name": "imag_i_13_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_13", "role": "d1" }} , 
 	{ "name": "imag_i_13_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_13", "role": "q1" }} , 
 	{ "name": "imag_i_13_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_13", "role": "we1" }} , 
 	{ "name": "imag_i_14_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_14", "role": "address0" }} , 
 	{ "name": "imag_i_14_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_14", "role": "ce0" }} , 
 	{ "name": "imag_i_14_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_14", "role": "d0" }} , 
 	{ "name": "imag_i_14_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_14", "role": "q0" }} , 
 	{ "name": "imag_i_14_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_14", "role": "we0" }} , 
 	{ "name": "imag_i_14_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_14", "role": "address1" }} , 
 	{ "name": "imag_i_14_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_14", "role": "ce1" }} , 
 	{ "name": "imag_i_14_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_14", "role": "d1" }} , 
 	{ "name": "imag_i_14_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_14", "role": "q1" }} , 
 	{ "name": "imag_i_14_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_14", "role": "we1" }} , 
 	{ "name": "imag_i_15_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_15", "role": "address0" }} , 
 	{ "name": "imag_i_15_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_15", "role": "ce0" }} , 
 	{ "name": "imag_i_15_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_15", "role": "d0" }} , 
 	{ "name": "imag_i_15_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_15", "role": "q0" }} , 
 	{ "name": "imag_i_15_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_15", "role": "we0" }} , 
 	{ "name": "imag_i_15_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_i_15", "role": "address1" }} , 
 	{ "name": "imag_i_15_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_15", "role": "ce1" }} , 
 	{ "name": "imag_i_15_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_15", "role": "d1" }} , 
 	{ "name": "imag_i_15_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_15", "role": "q1" }} , 
 	{ "name": "imag_i_15_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_15", "role": "we1" }} , 
 	{ "name": "real_o_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":10, "type": "signal", "bundle":{"name": "real_o", "role": "address0" }} , 
 	{ "name": "real_o_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o", "role": "ce0" }} , 
 	{ "name": "real_o_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o", "role": "d0" }} , 
 	{ "name": "real_o_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o", "role": "q0" }} , 
 	{ "name": "real_o_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o", "role": "we0" }} , 
 	{ "name": "real_o_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":10, "type": "signal", "bundle":{"name": "real_o", "role": "address1" }} , 
 	{ "name": "real_o_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o", "role": "ce1" }} , 
 	{ "name": "real_o_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o", "role": "d1" }} , 
 	{ "name": "real_o_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o", "role": "q1" }} , 
 	{ "name": "real_o_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o", "role": "we1" }} , 
 	{ "name": "imag_o_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":10, "type": "signal", "bundle":{"name": "imag_o", "role": "address0" }} , 
 	{ "name": "imag_o_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o", "role": "ce0" }} , 
 	{ "name": "imag_o_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o", "role": "d0" }} , 
 	{ "name": "imag_o_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o", "role": "q0" }} , 
 	{ "name": "imag_o_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o", "role": "we0" }} , 
 	{ "name": "imag_o_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":10, "type": "signal", "bundle":{"name": "imag_o", "role": "address1" }} , 
 	{ "name": "imag_o_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o", "role": "ce1" }} , 
 	{ "name": "imag_o_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o", "role": "d1" }} , 
 	{ "name": "imag_o_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o", "role": "q1" }} , 
 	{ "name": "imag_o_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o", "role": "we1" }} , 
 	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }}  ]}
set Spec2ImplPortList { 
	real_i_0 { ap_memory {  { real_i_0_address0 mem_address 1 6 }  { real_i_0_ce0 mem_ce 1 1 }  { real_i_0_d0 mem_din 1 32 }  { real_i_0_q0 mem_dout 0 32 }  { real_i_0_we0 mem_we 1 1 }  { real_i_0_address1 mem_address 1 6 }  { real_i_0_ce1 mem_ce 1 1 }  { real_i_0_d1 mem_din 1 32 }  { real_i_0_q1 mem_dout 0 32 }  { real_i_0_we1 mem_we 1 1 } } }
	real_i_1 { ap_memory {  { real_i_1_address0 mem_address 1 6 }  { real_i_1_ce0 mem_ce 1 1 }  { real_i_1_d0 mem_din 1 32 }  { real_i_1_q0 mem_dout 0 32 }  { real_i_1_we0 mem_we 1 1 }  { real_i_1_address1 mem_address 1 6 }  { real_i_1_ce1 mem_ce 1 1 }  { real_i_1_d1 mem_din 1 32 }  { real_i_1_q1 mem_dout 0 32 }  { real_i_1_we1 mem_we 1 1 } } }
	real_i_2 { ap_memory {  { real_i_2_address0 mem_address 1 6 }  { real_i_2_ce0 mem_ce 1 1 }  { real_i_2_d0 mem_din 1 32 }  { real_i_2_q0 mem_dout 0 32 }  { real_i_2_we0 mem_we 1 1 }  { real_i_2_address1 mem_address 1 6 }  { real_i_2_ce1 mem_ce 1 1 }  { real_i_2_d1 mem_din 1 32 }  { real_i_2_q1 mem_dout 0 32 }  { real_i_2_we1 mem_we 1 1 } } }
	real_i_3 { ap_memory {  { real_i_3_address0 mem_address 1 6 }  { real_i_3_ce0 mem_ce 1 1 }  { real_i_3_d0 mem_din 1 32 }  { real_i_3_q0 mem_dout 0 32 }  { real_i_3_we0 mem_we 1 1 }  { real_i_3_address1 mem_address 1 6 }  { real_i_3_ce1 mem_ce 1 1 }  { real_i_3_d1 mem_din 1 32 }  { real_i_3_q1 mem_dout 0 32 }  { real_i_3_we1 mem_we 1 1 } } }
	real_i_4 { ap_memory {  { real_i_4_address0 mem_address 1 6 }  { real_i_4_ce0 mem_ce 1 1 }  { real_i_4_d0 mem_din 1 32 }  { real_i_4_q0 mem_dout 0 32 }  { real_i_4_we0 mem_we 1 1 }  { real_i_4_address1 mem_address 1 6 }  { real_i_4_ce1 mem_ce 1 1 }  { real_i_4_d1 mem_din 1 32 }  { real_i_4_q1 mem_dout 0 32 }  { real_i_4_we1 mem_we 1 1 } } }
	real_i_5 { ap_memory {  { real_i_5_address0 mem_address 1 6 }  { real_i_5_ce0 mem_ce 1 1 }  { real_i_5_d0 mem_din 1 32 }  { real_i_5_q0 mem_dout 0 32 }  { real_i_5_we0 mem_we 1 1 }  { real_i_5_address1 mem_address 1 6 }  { real_i_5_ce1 mem_ce 1 1 }  { real_i_5_d1 mem_din 1 32 }  { real_i_5_q1 mem_dout 0 32 }  { real_i_5_we1 mem_we 1 1 } } }
	real_i_6 { ap_memory {  { real_i_6_address0 mem_address 1 6 }  { real_i_6_ce0 mem_ce 1 1 }  { real_i_6_d0 mem_din 1 32 }  { real_i_6_q0 mem_dout 0 32 }  { real_i_6_we0 mem_we 1 1 }  { real_i_6_address1 mem_address 1 6 }  { real_i_6_ce1 mem_ce 1 1 }  { real_i_6_d1 mem_din 1 32 }  { real_i_6_q1 mem_dout 0 32 }  { real_i_6_we1 mem_we 1 1 } } }
	real_i_7 { ap_memory {  { real_i_7_address0 mem_address 1 6 }  { real_i_7_ce0 mem_ce 1 1 }  { real_i_7_d0 mem_din 1 32 }  { real_i_7_q0 mem_dout 0 32 }  { real_i_7_we0 mem_we 1 1 }  { real_i_7_address1 mem_address 1 6 }  { real_i_7_ce1 mem_ce 1 1 }  { real_i_7_d1 mem_din 1 32 }  { real_i_7_q1 mem_dout 0 32 }  { real_i_7_we1 mem_we 1 1 } } }
	real_i_8 { ap_memory {  { real_i_8_address0 mem_address 1 6 }  { real_i_8_ce0 mem_ce 1 1 }  { real_i_8_d0 mem_din 1 32 }  { real_i_8_q0 mem_dout 0 32 }  { real_i_8_we0 mem_we 1 1 }  { real_i_8_address1 mem_address 1 6 }  { real_i_8_ce1 mem_ce 1 1 }  { real_i_8_d1 mem_din 1 32 }  { real_i_8_q1 mem_dout 0 32 }  { real_i_8_we1 mem_we 1 1 } } }
	real_i_9 { ap_memory {  { real_i_9_address0 mem_address 1 6 }  { real_i_9_ce0 mem_ce 1 1 }  { real_i_9_d0 mem_din 1 32 }  { real_i_9_q0 mem_dout 0 32 }  { real_i_9_we0 mem_we 1 1 }  { real_i_9_address1 mem_address 1 6 }  { real_i_9_ce1 mem_ce 1 1 }  { real_i_9_d1 mem_din 1 32 }  { real_i_9_q1 mem_dout 0 32 }  { real_i_9_we1 mem_we 1 1 } } }
	real_i_10 { ap_memory {  { real_i_10_address0 mem_address 1 6 }  { real_i_10_ce0 mem_ce 1 1 }  { real_i_10_d0 mem_din 1 32 }  { real_i_10_q0 mem_dout 0 32 }  { real_i_10_we0 mem_we 1 1 }  { real_i_10_address1 mem_address 1 6 }  { real_i_10_ce1 mem_ce 1 1 }  { real_i_10_d1 mem_din 1 32 }  { real_i_10_q1 mem_dout 0 32 }  { real_i_10_we1 mem_we 1 1 } } }
	real_i_11 { ap_memory {  { real_i_11_address0 mem_address 1 6 }  { real_i_11_ce0 mem_ce 1 1 }  { real_i_11_d0 mem_din 1 32 }  { real_i_11_q0 mem_dout 0 32 }  { real_i_11_we0 mem_we 1 1 }  { real_i_11_address1 mem_address 1 6 }  { real_i_11_ce1 mem_ce 1 1 }  { real_i_11_d1 mem_din 1 32 }  { real_i_11_q1 mem_dout 0 32 }  { real_i_11_we1 mem_we 1 1 } } }
	real_i_12 { ap_memory {  { real_i_12_address0 mem_address 1 6 }  { real_i_12_ce0 mem_ce 1 1 }  { real_i_12_d0 mem_din 1 32 }  { real_i_12_q0 mem_dout 0 32 }  { real_i_12_we0 mem_we 1 1 }  { real_i_12_address1 mem_address 1 6 }  { real_i_12_ce1 mem_ce 1 1 }  { real_i_12_d1 mem_din 1 32 }  { real_i_12_q1 mem_dout 0 32 }  { real_i_12_we1 mem_we 1 1 } } }
	real_i_13 { ap_memory {  { real_i_13_address0 mem_address 1 6 }  { real_i_13_ce0 mem_ce 1 1 }  { real_i_13_d0 mem_din 1 32 }  { real_i_13_q0 mem_dout 0 32 }  { real_i_13_we0 mem_we 1 1 }  { real_i_13_address1 mem_address 1 6 }  { real_i_13_ce1 mem_ce 1 1 }  { real_i_13_d1 mem_din 1 32 }  { real_i_13_q1 mem_dout 0 32 }  { real_i_13_we1 mem_we 1 1 } } }
	real_i_14 { ap_memory {  { real_i_14_address0 mem_address 1 6 }  { real_i_14_ce0 mem_ce 1 1 }  { real_i_14_d0 mem_din 1 32 }  { real_i_14_q0 mem_dout 0 32 }  { real_i_14_we0 mem_we 1 1 }  { real_i_14_address1 mem_address 1 6 }  { real_i_14_ce1 mem_ce 1 1 }  { real_i_14_d1 mem_din 1 32 }  { real_i_14_q1 mem_dout 0 32 }  { real_i_14_we1 mem_we 1 1 } } }
	real_i_15 { ap_memory {  { real_i_15_address0 mem_address 1 6 }  { real_i_15_ce0 mem_ce 1 1 }  { real_i_15_d0 mem_din 1 32 }  { real_i_15_q0 mem_dout 0 32 }  { real_i_15_we0 mem_we 1 1 }  { real_i_15_address1 mem_address 1 6 }  { real_i_15_ce1 mem_ce 1 1 }  { real_i_15_d1 mem_din 1 32 }  { real_i_15_q1 mem_dout 0 32 }  { real_i_15_we1 mem_we 1 1 } } }
	imag_i_0 { ap_memory {  { imag_i_0_address0 mem_address 1 6 }  { imag_i_0_ce0 mem_ce 1 1 }  { imag_i_0_d0 mem_din 1 32 }  { imag_i_0_q0 mem_dout 0 32 }  { imag_i_0_we0 mem_we 1 1 }  { imag_i_0_address1 mem_address 1 6 }  { imag_i_0_ce1 mem_ce 1 1 }  { imag_i_0_d1 mem_din 1 32 }  { imag_i_0_q1 mem_dout 0 32 }  { imag_i_0_we1 mem_we 1 1 } } }
	imag_i_1 { ap_memory {  { imag_i_1_address0 mem_address 1 6 }  { imag_i_1_ce0 mem_ce 1 1 }  { imag_i_1_d0 mem_din 1 32 }  { imag_i_1_q0 mem_dout 0 32 }  { imag_i_1_we0 mem_we 1 1 }  { imag_i_1_address1 mem_address 1 6 }  { imag_i_1_ce1 mem_ce 1 1 }  { imag_i_1_d1 mem_din 1 32 }  { imag_i_1_q1 mem_dout 0 32 }  { imag_i_1_we1 mem_we 1 1 } } }
	imag_i_2 { ap_memory {  { imag_i_2_address0 mem_address 1 6 }  { imag_i_2_ce0 mem_ce 1 1 }  { imag_i_2_d0 mem_din 1 32 }  { imag_i_2_q0 mem_dout 0 32 }  { imag_i_2_we0 mem_we 1 1 }  { imag_i_2_address1 mem_address 1 6 }  { imag_i_2_ce1 mem_ce 1 1 }  { imag_i_2_d1 mem_din 1 32 }  { imag_i_2_q1 mem_dout 0 32 }  { imag_i_2_we1 mem_we 1 1 } } }
	imag_i_3 { ap_memory {  { imag_i_3_address0 mem_address 1 6 }  { imag_i_3_ce0 mem_ce 1 1 }  { imag_i_3_d0 mem_din 1 32 }  { imag_i_3_q0 mem_dout 0 32 }  { imag_i_3_we0 mem_we 1 1 }  { imag_i_3_address1 mem_address 1 6 }  { imag_i_3_ce1 mem_ce 1 1 }  { imag_i_3_d1 mem_din 1 32 }  { imag_i_3_q1 mem_dout 0 32 }  { imag_i_3_we1 mem_we 1 1 } } }
	imag_i_4 { ap_memory {  { imag_i_4_address0 mem_address 1 6 }  { imag_i_4_ce0 mem_ce 1 1 }  { imag_i_4_d0 mem_din 1 32 }  { imag_i_4_q0 mem_dout 0 32 }  { imag_i_4_we0 mem_we 1 1 }  { imag_i_4_address1 mem_address 1 6 }  { imag_i_4_ce1 mem_ce 1 1 }  { imag_i_4_d1 mem_din 1 32 }  { imag_i_4_q1 mem_dout 0 32 }  { imag_i_4_we1 mem_we 1 1 } } }
	imag_i_5 { ap_memory {  { imag_i_5_address0 mem_address 1 6 }  { imag_i_5_ce0 mem_ce 1 1 }  { imag_i_5_d0 mem_din 1 32 }  { imag_i_5_q0 mem_dout 0 32 }  { imag_i_5_we0 mem_we 1 1 }  { imag_i_5_address1 mem_address 1 6 }  { imag_i_5_ce1 mem_ce 1 1 }  { imag_i_5_d1 mem_din 1 32 }  { imag_i_5_q1 mem_dout 0 32 }  { imag_i_5_we1 mem_we 1 1 } } }
	imag_i_6 { ap_memory {  { imag_i_6_address0 mem_address 1 6 }  { imag_i_6_ce0 mem_ce 1 1 }  { imag_i_6_d0 mem_din 1 32 }  { imag_i_6_q0 mem_dout 0 32 }  { imag_i_6_we0 mem_we 1 1 }  { imag_i_6_address1 mem_address 1 6 }  { imag_i_6_ce1 mem_ce 1 1 }  { imag_i_6_d1 mem_din 1 32 }  { imag_i_6_q1 mem_dout 0 32 }  { imag_i_6_we1 mem_we 1 1 } } }
	imag_i_7 { ap_memory {  { imag_i_7_address0 mem_address 1 6 }  { imag_i_7_ce0 mem_ce 1 1 }  { imag_i_7_d0 mem_din 1 32 }  { imag_i_7_q0 mem_dout 0 32 }  { imag_i_7_we0 mem_we 1 1 }  { imag_i_7_address1 mem_address 1 6 }  { imag_i_7_ce1 mem_ce 1 1 }  { imag_i_7_d1 mem_din 1 32 }  { imag_i_7_q1 mem_dout 0 32 }  { imag_i_7_we1 mem_we 1 1 } } }
	imag_i_8 { ap_memory {  { imag_i_8_address0 mem_address 1 6 }  { imag_i_8_ce0 mem_ce 1 1 }  { imag_i_8_d0 mem_din 1 32 }  { imag_i_8_q0 mem_dout 0 32 }  { imag_i_8_we0 mem_we 1 1 }  { imag_i_8_address1 mem_address 1 6 }  { imag_i_8_ce1 mem_ce 1 1 }  { imag_i_8_d1 mem_din 1 32 }  { imag_i_8_q1 mem_dout 0 32 }  { imag_i_8_we1 mem_we 1 1 } } }
	imag_i_9 { ap_memory {  { imag_i_9_address0 mem_address 1 6 }  { imag_i_9_ce0 mem_ce 1 1 }  { imag_i_9_d0 mem_din 1 32 }  { imag_i_9_q0 mem_dout 0 32 }  { imag_i_9_we0 mem_we 1 1 }  { imag_i_9_address1 mem_address 1 6 }  { imag_i_9_ce1 mem_ce 1 1 }  { imag_i_9_d1 mem_din 1 32 }  { imag_i_9_q1 mem_dout 0 32 }  { imag_i_9_we1 mem_we 1 1 } } }
	imag_i_10 { ap_memory {  { imag_i_10_address0 mem_address 1 6 }  { imag_i_10_ce0 mem_ce 1 1 }  { imag_i_10_d0 mem_din 1 32 }  { imag_i_10_q0 mem_dout 0 32 }  { imag_i_10_we0 mem_we 1 1 }  { imag_i_10_address1 mem_address 1 6 }  { imag_i_10_ce1 mem_ce 1 1 }  { imag_i_10_d1 mem_din 1 32 }  { imag_i_10_q1 mem_dout 0 32 }  { imag_i_10_we1 mem_we 1 1 } } }
	imag_i_11 { ap_memory {  { imag_i_11_address0 mem_address 1 6 }  { imag_i_11_ce0 mem_ce 1 1 }  { imag_i_11_d0 mem_din 1 32 }  { imag_i_11_q0 mem_dout 0 32 }  { imag_i_11_we0 mem_we 1 1 }  { imag_i_11_address1 mem_address 1 6 }  { imag_i_11_ce1 mem_ce 1 1 }  { imag_i_11_d1 mem_din 1 32 }  { imag_i_11_q1 mem_dout 0 32 }  { imag_i_11_we1 mem_we 1 1 } } }
	imag_i_12 { ap_memory {  { imag_i_12_address0 mem_address 1 6 }  { imag_i_12_ce0 mem_ce 1 1 }  { imag_i_12_d0 mem_din 1 32 }  { imag_i_12_q0 mem_dout 0 32 }  { imag_i_12_we0 mem_we 1 1 }  { imag_i_12_address1 mem_address 1 6 }  { imag_i_12_ce1 mem_ce 1 1 }  { imag_i_12_d1 mem_din 1 32 }  { imag_i_12_q1 mem_dout 0 32 }  { imag_i_12_we1 mem_we 1 1 } } }
	imag_i_13 { ap_memory {  { imag_i_13_address0 mem_address 1 6 }  { imag_i_13_ce0 mem_ce 1 1 }  { imag_i_13_d0 mem_din 1 32 }  { imag_i_13_q0 mem_dout 0 32 }  { imag_i_13_we0 mem_we 1 1 }  { imag_i_13_address1 mem_address 1 6 }  { imag_i_13_ce1 mem_ce 1 1 }  { imag_i_13_d1 mem_din 1 32 }  { imag_i_13_q1 mem_dout 0 32 }  { imag_i_13_we1 mem_we 1 1 } } }
	imag_i_14 { ap_memory {  { imag_i_14_address0 mem_address 1 6 }  { imag_i_14_ce0 mem_ce 1 1 }  { imag_i_14_d0 mem_din 1 32 }  { imag_i_14_q0 mem_dout 0 32 }  { imag_i_14_we0 mem_we 1 1 }  { imag_i_14_address1 mem_address 1 6 }  { imag_i_14_ce1 mem_ce 1 1 }  { imag_i_14_d1 mem_din 1 32 }  { imag_i_14_q1 mem_dout 0 32 }  { imag_i_14_we1 mem_we 1 1 } } }
	imag_i_15 { ap_memory {  { imag_i_15_address0 mem_address 1 6 }  { imag_i_15_ce0 mem_ce 1 1 }  { imag_i_15_d0 mem_din 1 32 }  { imag_i_15_q0 mem_dout 0 32 }  { imag_i_15_we0 mem_we 1 1 }  { imag_i_15_address1 mem_address 1 6 }  { imag_i_15_ce1 mem_ce 1 1 }  { imag_i_15_d1 mem_din 1 32 }  { imag_i_15_q1 mem_dout 0 32 }  { imag_i_15_we1 mem_we 1 1 } } }
	real_o { ap_memory {  { real_o_address0 mem_address 1 10 }  { real_o_ce0 mem_ce 1 1 }  { real_o_d0 mem_din 1 32 }  { real_o_q0 mem_dout 0 32 }  { real_o_we0 mem_we 1 1 }  { real_o_address1 mem_address 1 10 }  { real_o_ce1 mem_ce 1 1 }  { real_o_d1 mem_din 1 32 }  { real_o_q1 mem_dout 0 32 }  { real_o_we1 mem_we 1 1 } } }
	imag_o { ap_memory {  { imag_o_address0 mem_address 1 10 }  { imag_o_ce0 mem_ce 1 1 }  { imag_o_d0 mem_din 1 32 }  { imag_o_q0 mem_dout 0 32 }  { imag_o_we0 mem_we 1 1 }  { imag_o_address1 mem_address 1 10 }  { imag_o_ce1 mem_ce 1 1 }  { imag_o_d1 mem_din 1 32 }  { imag_o_q1 mem_dout 0 32 }  { imag_o_we1 mem_we 1 1 } } }
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
