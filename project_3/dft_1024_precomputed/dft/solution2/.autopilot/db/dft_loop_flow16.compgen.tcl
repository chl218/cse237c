# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 370 \
    name imag_arr16_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr16_1 \
    op interface \
    ports { imag_arr16_1_address0 { O 2 vector } imag_arr16_1_ce0 { O 1 bit } imag_arr16_1_d0 { O 32 vector } imag_arr16_1_q0 { I 32 vector } imag_arr16_1_we0 { O 1 bit } imag_arr16_1_address1 { O 2 vector } imag_arr16_1_ce1 { O 1 bit } imag_arr16_1_d1 { O 32 vector } imag_arr16_1_q1 { I 32 vector } imag_arr16_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr16_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 371 \
    name imag_arr32_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr32_2 \
    op interface \
    ports { imag_arr32_2_address0 { O 3 vector } imag_arr32_2_ce0 { O 1 bit } imag_arr32_2_d0 { O 32 vector } imag_arr32_2_q0 { I 32 vector } imag_arr32_2_we0 { O 1 bit } imag_arr32_2_address1 { O 3 vector } imag_arr32_2_ce1 { O 1 bit } imag_arr32_2_d1 { O 32 vector } imag_arr32_2_q1 { I 32 vector } imag_arr32_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr32_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 372 \
    name imag_arr32_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr32_3 \
    op interface \
    ports { imag_arr32_3_address0 { O 3 vector } imag_arr32_3_ce0 { O 1 bit } imag_arr32_3_d0 { O 32 vector } imag_arr32_3_q0 { I 32 vector } imag_arr32_3_we0 { O 1 bit } imag_arr32_3_address1 { O 3 vector } imag_arr32_3_ce1 { O 1 bit } imag_arr32_3_d1 { O 32 vector } imag_arr32_3_q1 { I 32 vector } imag_arr32_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr32_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 373 \
    name real_arr16_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr16_1 \
    op interface \
    ports { real_arr16_1_address0 { O 2 vector } real_arr16_1_ce0 { O 1 bit } real_arr16_1_d0 { O 32 vector } real_arr16_1_q0 { I 32 vector } real_arr16_1_we0 { O 1 bit } real_arr16_1_address1 { O 2 vector } real_arr16_1_ce1 { O 1 bit } real_arr16_1_d1 { O 32 vector } real_arr16_1_q1 { I 32 vector } real_arr16_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr16_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 374 \
    name real_arr32_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr32_2 \
    op interface \
    ports { real_arr32_2_address0 { O 3 vector } real_arr32_2_ce0 { O 1 bit } real_arr32_2_d0 { O 32 vector } real_arr32_2_q0 { I 32 vector } real_arr32_2_we0 { O 1 bit } real_arr32_2_address1 { O 3 vector } real_arr32_2_ce1 { O 1 bit } real_arr32_2_d1 { O 32 vector } real_arr32_2_q1 { I 32 vector } real_arr32_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr32_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 375 \
    name real_arr32_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr32_3 \
    op interface \
    ports { real_arr32_3_address0 { O 3 vector } real_arr32_3_ce0 { O 1 bit } real_arr32_3_d0 { O 32 vector } real_arr32_3_q0 { I 32 vector } real_arr32_3_we0 { O 1 bit } real_arr32_3_address1 { O 3 vector } real_arr32_3_ce1 { O 1 bit } real_arr32_3_d1 { O 32 vector } real_arr32_3_q1 { I 32 vector } real_arr32_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr32_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 376 \
    name imag_arr16_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr16_0 \
    op interface \
    ports { imag_arr16_0_address0 { O 2 vector } imag_arr16_0_ce0 { O 1 bit } imag_arr16_0_d0 { O 32 vector } imag_arr16_0_q0 { I 32 vector } imag_arr16_0_we0 { O 1 bit } imag_arr16_0_address1 { O 2 vector } imag_arr16_0_ce1 { O 1 bit } imag_arr16_0_d1 { O 32 vector } imag_arr16_0_q1 { I 32 vector } imag_arr16_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr16_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 377 \
    name imag_arr32_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr32_0 \
    op interface \
    ports { imag_arr32_0_address0 { O 3 vector } imag_arr32_0_ce0 { O 1 bit } imag_arr32_0_d0 { O 32 vector } imag_arr32_0_q0 { I 32 vector } imag_arr32_0_we0 { O 1 bit } imag_arr32_0_address1 { O 3 vector } imag_arr32_0_ce1 { O 1 bit } imag_arr32_0_d1 { O 32 vector } imag_arr32_0_q1 { I 32 vector } imag_arr32_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr32_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 378 \
    name imag_arr32_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr32_1 \
    op interface \
    ports { imag_arr32_1_address0 { O 3 vector } imag_arr32_1_ce0 { O 1 bit } imag_arr32_1_d0 { O 32 vector } imag_arr32_1_q0 { I 32 vector } imag_arr32_1_we0 { O 1 bit } imag_arr32_1_address1 { O 3 vector } imag_arr32_1_ce1 { O 1 bit } imag_arr32_1_d1 { O 32 vector } imag_arr32_1_q1 { I 32 vector } imag_arr32_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr32_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 379 \
    name real_arr16_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr16_0 \
    op interface \
    ports { real_arr16_0_address0 { O 2 vector } real_arr16_0_ce0 { O 1 bit } real_arr16_0_d0 { O 32 vector } real_arr16_0_q0 { I 32 vector } real_arr16_0_we0 { O 1 bit } real_arr16_0_address1 { O 2 vector } real_arr16_0_ce1 { O 1 bit } real_arr16_0_d1 { O 32 vector } real_arr16_0_q1 { I 32 vector } real_arr16_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr16_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 380 \
    name real_arr32_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr32_0 \
    op interface \
    ports { real_arr32_0_address0 { O 3 vector } real_arr32_0_ce0 { O 1 bit } real_arr32_0_d0 { O 32 vector } real_arr32_0_q0 { I 32 vector } real_arr32_0_we0 { O 1 bit } real_arr32_0_address1 { O 3 vector } real_arr32_0_ce1 { O 1 bit } real_arr32_0_d1 { O 32 vector } real_arr32_0_q1 { I 32 vector } real_arr32_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr32_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 381 \
    name real_arr32_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr32_1 \
    op interface \
    ports { real_arr32_1_address0 { O 3 vector } real_arr32_1_ce0 { O 1 bit } real_arr32_1_d0 { O 32 vector } real_arr32_1_q0 { I 32 vector } real_arr32_1_we0 { O 1 bit } real_arr32_1_address1 { O 3 vector } real_arr32_1_ce1 { O 1 bit } real_arr32_1_d1 { O 32 vector } real_arr32_1_q1 { I 32 vector } real_arr32_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr32_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 382 \
    name real_arr16_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr16_2 \
    op interface \
    ports { real_arr16_2_address0 { O 2 vector } real_arr16_2_ce0 { O 1 bit } real_arr16_2_d0 { O 32 vector } real_arr16_2_q0 { I 32 vector } real_arr16_2_we0 { O 1 bit } real_arr16_2_address1 { O 2 vector } real_arr16_2_ce1 { O 1 bit } real_arr16_2_d1 { O 32 vector } real_arr16_2_q1 { I 32 vector } real_arr16_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr16_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 383 \
    name imag_arr16_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr16_2 \
    op interface \
    ports { imag_arr16_2_address0 { O 2 vector } imag_arr16_2_ce0 { O 1 bit } imag_arr16_2_d0 { O 32 vector } imag_arr16_2_q0 { I 32 vector } imag_arr16_2_we0 { O 1 bit } imag_arr16_2_address1 { O 2 vector } imag_arr16_2_ce1 { O 1 bit } imag_arr16_2_d1 { O 32 vector } imag_arr16_2_q1 { I 32 vector } imag_arr16_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr16_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 384 \
    name real_arr16_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr16_3 \
    op interface \
    ports { real_arr16_3_address0 { O 2 vector } real_arr16_3_ce0 { O 1 bit } real_arr16_3_d0 { O 32 vector } real_arr16_3_q0 { I 32 vector } real_arr16_3_we0 { O 1 bit } real_arr16_3_address1 { O 2 vector } real_arr16_3_ce1 { O 1 bit } real_arr16_3_d1 { O 32 vector } real_arr16_3_q1 { I 32 vector } real_arr16_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr16_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 385 \
    name imag_arr16_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr16_3 \
    op interface \
    ports { imag_arr16_3_address0 { O 2 vector } imag_arr16_3_ce0 { O 1 bit } imag_arr16_3_d0 { O 32 vector } imag_arr16_3_q0 { I 32 vector } imag_arr16_3_we0 { O 1 bit } imag_arr16_3_address1 { O 2 vector } imag_arr16_3_ce1 { O 1 bit } imag_arr16_3_d1 { O 32 vector } imag_arr16_3_q1 { I 32 vector } imag_arr16_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr16_3'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


