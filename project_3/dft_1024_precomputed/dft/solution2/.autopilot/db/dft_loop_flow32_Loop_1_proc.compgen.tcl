# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 318 \
    name real_arr64_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr64_0 \
    op interface \
    ports { real_arr64_0_address0 { O 4 vector } real_arr64_0_ce0 { O 1 bit } real_arr64_0_q0 { I 32 vector } real_arr64_0_address1 { O 4 vector } real_arr64_0_ce1 { O 1 bit } real_arr64_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr64_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 319 \
    name real_arr64_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr64_1 \
    op interface \
    ports { real_arr64_1_address0 { O 4 vector } real_arr64_1_ce0 { O 1 bit } real_arr64_1_q0 { I 32 vector } real_arr64_1_address1 { O 4 vector } real_arr64_1_ce1 { O 1 bit } real_arr64_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr64_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 320 \
    name real_arr32_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr32_0 \
    op interface \
    ports { real_arr32_0_address0 { O 3 vector } real_arr32_0_ce0 { O 1 bit } real_arr32_0_we0 { O 1 bit } real_arr32_0_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr32_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 321 \
    name imag_arr64_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr64_0 \
    op interface \
    ports { imag_arr64_0_address0 { O 4 vector } imag_arr64_0_ce0 { O 1 bit } imag_arr64_0_q0 { I 32 vector } imag_arr64_0_address1 { O 4 vector } imag_arr64_0_ce1 { O 1 bit } imag_arr64_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr64_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 322 \
    name imag_arr64_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr64_1 \
    op interface \
    ports { imag_arr64_1_address0 { O 4 vector } imag_arr64_1_ce0 { O 1 bit } imag_arr64_1_q0 { I 32 vector } imag_arr64_1_address1 { O 4 vector } imag_arr64_1_ce1 { O 1 bit } imag_arr64_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr64_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 323 \
    name imag_arr32_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr32_0 \
    op interface \
    ports { imag_arr32_0_address0 { O 3 vector } imag_arr32_0_ce0 { O 1 bit } imag_arr32_0_we0 { O 1 bit } imag_arr32_0_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr32_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 324 \
    name real_arr64_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr64_2 \
    op interface \
    ports { real_arr64_2_address0 { O 4 vector } real_arr64_2_ce0 { O 1 bit } real_arr64_2_q0 { I 32 vector } real_arr64_2_address1 { O 4 vector } real_arr64_2_ce1 { O 1 bit } real_arr64_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr64_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 325 \
    name real_arr64_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr64_3 \
    op interface \
    ports { real_arr64_3_address0 { O 4 vector } real_arr64_3_ce0 { O 1 bit } real_arr64_3_q0 { I 32 vector } real_arr64_3_address1 { O 4 vector } real_arr64_3_ce1 { O 1 bit } real_arr64_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr64_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 326 \
    name real_arr32_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr32_1 \
    op interface \
    ports { real_arr32_1_address0 { O 3 vector } real_arr32_1_ce0 { O 1 bit } real_arr32_1_we0 { O 1 bit } real_arr32_1_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr32_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 327 \
    name imag_arr64_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr64_2 \
    op interface \
    ports { imag_arr64_2_address0 { O 4 vector } imag_arr64_2_ce0 { O 1 bit } imag_arr64_2_q0 { I 32 vector } imag_arr64_2_address1 { O 4 vector } imag_arr64_2_ce1 { O 1 bit } imag_arr64_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr64_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 328 \
    name imag_arr64_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr64_3 \
    op interface \
    ports { imag_arr64_3_address0 { O 4 vector } imag_arr64_3_ce0 { O 1 bit } imag_arr64_3_q0 { I 32 vector } imag_arr64_3_address1 { O 4 vector } imag_arr64_3_ce1 { O 1 bit } imag_arr64_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr64_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 329 \
    name imag_arr32_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr32_1 \
    op interface \
    ports { imag_arr32_1_address0 { O 3 vector } imag_arr32_1_ce0 { O 1 bit } imag_arr32_1_we0 { O 1 bit } imag_arr32_1_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr32_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 330 \
    name real_arr32_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr32_2 \
    op interface \
    ports { real_arr32_2_address0 { O 3 vector } real_arr32_2_ce0 { O 1 bit } real_arr32_2_we0 { O 1 bit } real_arr32_2_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr32_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 331 \
    name imag_arr32_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr32_2 \
    op interface \
    ports { imag_arr32_2_address0 { O 3 vector } imag_arr32_2_ce0 { O 1 bit } imag_arr32_2_we0 { O 1 bit } imag_arr32_2_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr32_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 332 \
    name real_arr32_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr32_3 \
    op interface \
    ports { real_arr32_3_address0 { O 3 vector } real_arr32_3_ce0 { O 1 bit } real_arr32_3_we0 { O 1 bit } real_arr32_3_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr32_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 333 \
    name imag_arr32_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr32_3 \
    op interface \
    ports { imag_arr32_3_address0 { O 3 vector } imag_arr32_3_ce0 { O 1 bit } imag_arr32_3_we0 { O 1 bit } imag_arr32_3_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr32_3'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


