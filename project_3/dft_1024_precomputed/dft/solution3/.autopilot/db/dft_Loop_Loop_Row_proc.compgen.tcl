# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 462 \
    name real_i_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_0 \
    op interface \
    ports { real_i_0_address0 { O 7 vector } real_i_0_ce0 { O 1 bit } real_i_0_q0 { I 32 vector } real_i_0_address1 { O 7 vector } real_i_0_ce1 { O 1 bit } real_i_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 463 \
    name real_i_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_1 \
    op interface \
    ports { real_i_1_address0 { O 7 vector } real_i_1_ce0 { O 1 bit } real_i_1_q0 { I 32 vector } real_i_1_address1 { O 7 vector } real_i_1_ce1 { O 1 bit } real_i_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 464 \
    name real_i_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_2 \
    op interface \
    ports { real_i_2_address0 { O 7 vector } real_i_2_ce0 { O 1 bit } real_i_2_q0 { I 32 vector } real_i_2_address1 { O 7 vector } real_i_2_ce1 { O 1 bit } real_i_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 465 \
    name real_i_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_3 \
    op interface \
    ports { real_i_3_address0 { O 7 vector } real_i_3_ce0 { O 1 bit } real_i_3_q0 { I 32 vector } real_i_3_address1 { O 7 vector } real_i_3_ce1 { O 1 bit } real_i_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 466 \
    name real_i_4 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_4 \
    op interface \
    ports { real_i_4_address0 { O 7 vector } real_i_4_ce0 { O 1 bit } real_i_4_q0 { I 32 vector } real_i_4_address1 { O 7 vector } real_i_4_ce1 { O 1 bit } real_i_4_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_4'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 467 \
    name real_i_5 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_5 \
    op interface \
    ports { real_i_5_address0 { O 7 vector } real_i_5_ce0 { O 1 bit } real_i_5_q0 { I 32 vector } real_i_5_address1 { O 7 vector } real_i_5_ce1 { O 1 bit } real_i_5_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_5'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 468 \
    name real_i_6 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_6 \
    op interface \
    ports { real_i_6_address0 { O 7 vector } real_i_6_ce0 { O 1 bit } real_i_6_q0 { I 32 vector } real_i_6_address1 { O 7 vector } real_i_6_ce1 { O 1 bit } real_i_6_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_6'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 469 \
    name real_i_7 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_7 \
    op interface \
    ports { real_i_7_address0 { O 7 vector } real_i_7_ce0 { O 1 bit } real_i_7_q0 { I 32 vector } real_i_7_address1 { O 7 vector } real_i_7_ce1 { O 1 bit } real_i_7_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_7'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 470 \
    name imag_i_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_0 \
    op interface \
    ports { imag_i_0_address0 { O 7 vector } imag_i_0_ce0 { O 1 bit } imag_i_0_q0 { I 32 vector } imag_i_0_address1 { O 7 vector } imag_i_0_ce1 { O 1 bit } imag_i_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 471 \
    name imag_i_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_1 \
    op interface \
    ports { imag_i_1_address0 { O 7 vector } imag_i_1_ce0 { O 1 bit } imag_i_1_q0 { I 32 vector } imag_i_1_address1 { O 7 vector } imag_i_1_ce1 { O 1 bit } imag_i_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 472 \
    name imag_i_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_2 \
    op interface \
    ports { imag_i_2_address0 { O 7 vector } imag_i_2_ce0 { O 1 bit } imag_i_2_q0 { I 32 vector } imag_i_2_address1 { O 7 vector } imag_i_2_ce1 { O 1 bit } imag_i_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 473 \
    name imag_i_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_3 \
    op interface \
    ports { imag_i_3_address0 { O 7 vector } imag_i_3_ce0 { O 1 bit } imag_i_3_q0 { I 32 vector } imag_i_3_address1 { O 7 vector } imag_i_3_ce1 { O 1 bit } imag_i_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 474 \
    name imag_i_4 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_4 \
    op interface \
    ports { imag_i_4_address0 { O 7 vector } imag_i_4_ce0 { O 1 bit } imag_i_4_q0 { I 32 vector } imag_i_4_address1 { O 7 vector } imag_i_4_ce1 { O 1 bit } imag_i_4_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_4'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 475 \
    name imag_i_5 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_5 \
    op interface \
    ports { imag_i_5_address0 { O 7 vector } imag_i_5_ce0 { O 1 bit } imag_i_5_q0 { I 32 vector } imag_i_5_address1 { O 7 vector } imag_i_5_ce1 { O 1 bit } imag_i_5_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_5'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 476 \
    name imag_i_6 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_6 \
    op interface \
    ports { imag_i_6_address0 { O 7 vector } imag_i_6_ce0 { O 1 bit } imag_i_6_q0 { I 32 vector } imag_i_6_address1 { O 7 vector } imag_i_6_ce1 { O 1 bit } imag_i_6_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_6'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 477 \
    name imag_i_7 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_7 \
    op interface \
    ports { imag_i_7_address0 { O 7 vector } imag_i_7_ce0 { O 1 bit } imag_i_7_q0 { I 32 vector } imag_i_7_address1 { O 7 vector } imag_i_7_ce1 { O 1 bit } imag_i_7_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_7'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 478 \
    name real_o \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_o \
    op interface \
    ports { real_o_address0 { O 10 vector } real_o_ce0 { O 1 bit } real_o_we0 { O 1 bit } real_o_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_o'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 479 \
    name imag_o \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_o \
    op interface \
    ports { imag_o_address0 { O 10 vector } imag_o_ce0 { O 1 bit } imag_o_we0 { O 1 bit } imag_o_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_o'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 480 \
    name imag_arr1024_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr1024_1 \
    op interface \
    ports { imag_arr1024_1_address0 { O 8 vector } imag_arr1024_1_ce0 { O 1 bit } imag_arr1024_1_we0 { O 1 bit } imag_arr1024_1_d0 { O 32 vector } imag_arr1024_1_q0 { I 32 vector } imag_arr1024_1_address1 { O 8 vector } imag_arr1024_1_ce1 { O 1 bit } imag_arr1024_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr1024_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 481 \
    name real_arr1024_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr1024_1 \
    op interface \
    ports { real_arr1024_1_address0 { O 8 vector } real_arr1024_1_ce0 { O 1 bit } real_arr1024_1_we0 { O 1 bit } real_arr1024_1_d0 { O 32 vector } real_arr1024_1_q0 { I 32 vector } real_arr1024_1_address1 { O 8 vector } real_arr1024_1_ce1 { O 1 bit } real_arr1024_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr1024_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 482 \
    name imag_arr1024_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr1024_0 \
    op interface \
    ports { imag_arr1024_0_address0 { O 8 vector } imag_arr1024_0_ce0 { O 1 bit } imag_arr1024_0_we0 { O 1 bit } imag_arr1024_0_d0 { O 32 vector } imag_arr1024_0_q0 { I 32 vector } imag_arr1024_0_address1 { O 8 vector } imag_arr1024_0_ce1 { O 1 bit } imag_arr1024_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr1024_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 483 \
    name real_arr1024_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr1024_0 \
    op interface \
    ports { real_arr1024_0_address0 { O 8 vector } real_arr1024_0_ce0 { O 1 bit } real_arr1024_0_we0 { O 1 bit } real_arr1024_0_d0 { O 32 vector } real_arr1024_0_q0 { I 32 vector } real_arr1024_0_address1 { O 8 vector } real_arr1024_0_ce1 { O 1 bit } real_arr1024_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr1024_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 484 \
    name imag_arr1024_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr1024_3 \
    op interface \
    ports { imag_arr1024_3_address0 { O 8 vector } imag_arr1024_3_ce0 { O 1 bit } imag_arr1024_3_we0 { O 1 bit } imag_arr1024_3_d0 { O 32 vector } imag_arr1024_3_q0 { I 32 vector } imag_arr1024_3_address1 { O 8 vector } imag_arr1024_3_ce1 { O 1 bit } imag_arr1024_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr1024_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 485 \
    name real_arr1024_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr1024_3 \
    op interface \
    ports { real_arr1024_3_address0 { O 8 vector } real_arr1024_3_ce0 { O 1 bit } real_arr1024_3_we0 { O 1 bit } real_arr1024_3_d0 { O 32 vector } real_arr1024_3_q0 { I 32 vector } real_arr1024_3_address1 { O 8 vector } real_arr1024_3_ce1 { O 1 bit } real_arr1024_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr1024_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 486 \
    name imag_arr1024_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr1024_2 \
    op interface \
    ports { imag_arr1024_2_address0 { O 8 vector } imag_arr1024_2_ce0 { O 1 bit } imag_arr1024_2_we0 { O 1 bit } imag_arr1024_2_d0 { O 32 vector } imag_arr1024_2_q0 { I 32 vector } imag_arr1024_2_address1 { O 8 vector } imag_arr1024_2_ce1 { O 1 bit } imag_arr1024_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr1024_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 487 \
    name real_arr1024_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr1024_2 \
    op interface \
    ports { real_arr1024_2_address0 { O 8 vector } real_arr1024_2_ce0 { O 1 bit } real_arr1024_2_we0 { O 1 bit } real_arr1024_2_d0 { O 32 vector } real_arr1024_2_q0 { I 32 vector } real_arr1024_2_address1 { O 8 vector } real_arr1024_2_ce1 { O 1 bit } real_arr1024_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr1024_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 488 \
    name real_arr512_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr512_0 \
    op interface \
    ports { real_arr512_0_address0 { O 7 vector } real_arr512_0_ce0 { O 1 bit } real_arr512_0_we0 { O 1 bit } real_arr512_0_d0 { O 32 vector } real_arr512_0_q0 { I 32 vector } real_arr512_0_address1 { O 7 vector } real_arr512_0_ce1 { O 1 bit } real_arr512_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr512_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 489 \
    name imag_arr512_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr512_0 \
    op interface \
    ports { imag_arr512_0_address0 { O 7 vector } imag_arr512_0_ce0 { O 1 bit } imag_arr512_0_we0 { O 1 bit } imag_arr512_0_d0 { O 32 vector } imag_arr512_0_q0 { I 32 vector } imag_arr512_0_address1 { O 7 vector } imag_arr512_0_ce1 { O 1 bit } imag_arr512_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr512_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 490 \
    name real_arr512_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr512_1 \
    op interface \
    ports { real_arr512_1_address0 { O 7 vector } real_arr512_1_ce0 { O 1 bit } real_arr512_1_we0 { O 1 bit } real_arr512_1_d0 { O 32 vector } real_arr512_1_q0 { I 32 vector } real_arr512_1_address1 { O 7 vector } real_arr512_1_ce1 { O 1 bit } real_arr512_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr512_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 491 \
    name imag_arr512_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr512_1 \
    op interface \
    ports { imag_arr512_1_address0 { O 7 vector } imag_arr512_1_ce0 { O 1 bit } imag_arr512_1_we0 { O 1 bit } imag_arr512_1_d0 { O 32 vector } imag_arr512_1_q0 { I 32 vector } imag_arr512_1_address1 { O 7 vector } imag_arr512_1_ce1 { O 1 bit } imag_arr512_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr512_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 492 \
    name real_arr512_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr512_2 \
    op interface \
    ports { real_arr512_2_address0 { O 7 vector } real_arr512_2_ce0 { O 1 bit } real_arr512_2_we0 { O 1 bit } real_arr512_2_d0 { O 32 vector } real_arr512_2_q0 { I 32 vector } real_arr512_2_address1 { O 7 vector } real_arr512_2_ce1 { O 1 bit } real_arr512_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr512_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 493 \
    name imag_arr512_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr512_2 \
    op interface \
    ports { imag_arr512_2_address0 { O 7 vector } imag_arr512_2_ce0 { O 1 bit } imag_arr512_2_we0 { O 1 bit } imag_arr512_2_d0 { O 32 vector } imag_arr512_2_q0 { I 32 vector } imag_arr512_2_address1 { O 7 vector } imag_arr512_2_ce1 { O 1 bit } imag_arr512_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr512_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 494 \
    name real_arr512_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr512_3 \
    op interface \
    ports { real_arr512_3_address0 { O 7 vector } real_arr512_3_ce0 { O 1 bit } real_arr512_3_we0 { O 1 bit } real_arr512_3_d0 { O 32 vector } real_arr512_3_q0 { I 32 vector } real_arr512_3_address1 { O 7 vector } real_arr512_3_ce1 { O 1 bit } real_arr512_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr512_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 495 \
    name imag_arr512_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr512_3 \
    op interface \
    ports { imag_arr512_3_address0 { O 7 vector } imag_arr512_3_ce0 { O 1 bit } imag_arr512_3_we0 { O 1 bit } imag_arr512_3_d0 { O 32 vector } imag_arr512_3_q0 { I 32 vector } imag_arr512_3_address1 { O 7 vector } imag_arr512_3_ce1 { O 1 bit } imag_arr512_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr512_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 496 \
    name real_arr256_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr256_0 \
    op interface \
    ports { real_arr256_0_address0 { O 6 vector } real_arr256_0_ce0 { O 1 bit } real_arr256_0_we0 { O 1 bit } real_arr256_0_d0 { O 32 vector } real_arr256_0_q0 { I 32 vector } real_arr256_0_address1 { O 6 vector } real_arr256_0_ce1 { O 1 bit } real_arr256_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr256_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 497 \
    name imag_arr256_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr256_0 \
    op interface \
    ports { imag_arr256_0_address0 { O 6 vector } imag_arr256_0_ce0 { O 1 bit } imag_arr256_0_we0 { O 1 bit } imag_arr256_0_d0 { O 32 vector } imag_arr256_0_q0 { I 32 vector } imag_arr256_0_address1 { O 6 vector } imag_arr256_0_ce1 { O 1 bit } imag_arr256_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr256_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 498 \
    name real_arr256_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr256_1 \
    op interface \
    ports { real_arr256_1_address0 { O 6 vector } real_arr256_1_ce0 { O 1 bit } real_arr256_1_we0 { O 1 bit } real_arr256_1_d0 { O 32 vector } real_arr256_1_q0 { I 32 vector } real_arr256_1_address1 { O 6 vector } real_arr256_1_ce1 { O 1 bit } real_arr256_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr256_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 499 \
    name imag_arr256_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr256_1 \
    op interface \
    ports { imag_arr256_1_address0 { O 6 vector } imag_arr256_1_ce0 { O 1 bit } imag_arr256_1_we0 { O 1 bit } imag_arr256_1_d0 { O 32 vector } imag_arr256_1_q0 { I 32 vector } imag_arr256_1_address1 { O 6 vector } imag_arr256_1_ce1 { O 1 bit } imag_arr256_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr256_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 500 \
    name real_arr256_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr256_2 \
    op interface \
    ports { real_arr256_2_address0 { O 6 vector } real_arr256_2_ce0 { O 1 bit } real_arr256_2_we0 { O 1 bit } real_arr256_2_d0 { O 32 vector } real_arr256_2_q0 { I 32 vector } real_arr256_2_address1 { O 6 vector } real_arr256_2_ce1 { O 1 bit } real_arr256_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr256_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 501 \
    name imag_arr256_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr256_2 \
    op interface \
    ports { imag_arr256_2_address0 { O 6 vector } imag_arr256_2_ce0 { O 1 bit } imag_arr256_2_we0 { O 1 bit } imag_arr256_2_d0 { O 32 vector } imag_arr256_2_q0 { I 32 vector } imag_arr256_2_address1 { O 6 vector } imag_arr256_2_ce1 { O 1 bit } imag_arr256_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr256_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 502 \
    name real_arr256_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr256_3 \
    op interface \
    ports { real_arr256_3_address0 { O 6 vector } real_arr256_3_ce0 { O 1 bit } real_arr256_3_we0 { O 1 bit } real_arr256_3_d0 { O 32 vector } real_arr256_3_q0 { I 32 vector } real_arr256_3_address1 { O 6 vector } real_arr256_3_ce1 { O 1 bit } real_arr256_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr256_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 503 \
    name imag_arr256_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr256_3 \
    op interface \
    ports { imag_arr256_3_address0 { O 6 vector } imag_arr256_3_ce0 { O 1 bit } imag_arr256_3_we0 { O 1 bit } imag_arr256_3_d0 { O 32 vector } imag_arr256_3_q0 { I 32 vector } imag_arr256_3_address1 { O 6 vector } imag_arr256_3_ce1 { O 1 bit } imag_arr256_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr256_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 504 \
    name real_arr128_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr128_0 \
    op interface \
    ports { real_arr128_0_address0 { O 5 vector } real_arr128_0_ce0 { O 1 bit } real_arr128_0_we0 { O 1 bit } real_arr128_0_d0 { O 32 vector } real_arr128_0_q0 { I 32 vector } real_arr128_0_address1 { O 5 vector } real_arr128_0_ce1 { O 1 bit } real_arr128_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr128_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 505 \
    name imag_arr128_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr128_0 \
    op interface \
    ports { imag_arr128_0_address0 { O 5 vector } imag_arr128_0_ce0 { O 1 bit } imag_arr128_0_we0 { O 1 bit } imag_arr128_0_d0 { O 32 vector } imag_arr128_0_q0 { I 32 vector } imag_arr128_0_address1 { O 5 vector } imag_arr128_0_ce1 { O 1 bit } imag_arr128_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr128_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 506 \
    name real_arr128_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr128_1 \
    op interface \
    ports { real_arr128_1_address0 { O 5 vector } real_arr128_1_ce0 { O 1 bit } real_arr128_1_we0 { O 1 bit } real_arr128_1_d0 { O 32 vector } real_arr128_1_q0 { I 32 vector } real_arr128_1_address1 { O 5 vector } real_arr128_1_ce1 { O 1 bit } real_arr128_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr128_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 507 \
    name imag_arr128_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr128_1 \
    op interface \
    ports { imag_arr128_1_address0 { O 5 vector } imag_arr128_1_ce0 { O 1 bit } imag_arr128_1_we0 { O 1 bit } imag_arr128_1_d0 { O 32 vector } imag_arr128_1_q0 { I 32 vector } imag_arr128_1_address1 { O 5 vector } imag_arr128_1_ce1 { O 1 bit } imag_arr128_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr128_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 508 \
    name real_arr128_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr128_2 \
    op interface \
    ports { real_arr128_2_address0 { O 5 vector } real_arr128_2_ce0 { O 1 bit } real_arr128_2_we0 { O 1 bit } real_arr128_2_d0 { O 32 vector } real_arr128_2_q0 { I 32 vector } real_arr128_2_address1 { O 5 vector } real_arr128_2_ce1 { O 1 bit } real_arr128_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr128_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 509 \
    name imag_arr128_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr128_2 \
    op interface \
    ports { imag_arr128_2_address0 { O 5 vector } imag_arr128_2_ce0 { O 1 bit } imag_arr128_2_we0 { O 1 bit } imag_arr128_2_d0 { O 32 vector } imag_arr128_2_q0 { I 32 vector } imag_arr128_2_address1 { O 5 vector } imag_arr128_2_ce1 { O 1 bit } imag_arr128_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr128_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 510 \
    name real_arr128_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr128_3 \
    op interface \
    ports { real_arr128_3_address0 { O 5 vector } real_arr128_3_ce0 { O 1 bit } real_arr128_3_we0 { O 1 bit } real_arr128_3_d0 { O 32 vector } real_arr128_3_q0 { I 32 vector } real_arr128_3_address1 { O 5 vector } real_arr128_3_ce1 { O 1 bit } real_arr128_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr128_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 511 \
    name imag_arr128_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr128_3 \
    op interface \
    ports { imag_arr128_3_address0 { O 5 vector } imag_arr128_3_ce0 { O 1 bit } imag_arr128_3_we0 { O 1 bit } imag_arr128_3_d0 { O 32 vector } imag_arr128_3_q0 { I 32 vector } imag_arr128_3_address1 { O 5 vector } imag_arr128_3_ce1 { O 1 bit } imag_arr128_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr128_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 512 \
    name real_arr64_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr64_0 \
    op interface \
    ports { real_arr64_0_address0 { O 4 vector } real_arr64_0_ce0 { O 1 bit } real_arr64_0_we0 { O 1 bit } real_arr64_0_d0 { O 32 vector } real_arr64_0_q0 { I 32 vector } real_arr64_0_address1 { O 4 vector } real_arr64_0_ce1 { O 1 bit } real_arr64_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr64_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 513 \
    name imag_arr64_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr64_0 \
    op interface \
    ports { imag_arr64_0_address0 { O 4 vector } imag_arr64_0_ce0 { O 1 bit } imag_arr64_0_we0 { O 1 bit } imag_arr64_0_d0 { O 32 vector } imag_arr64_0_q0 { I 32 vector } imag_arr64_0_address1 { O 4 vector } imag_arr64_0_ce1 { O 1 bit } imag_arr64_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr64_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 514 \
    name real_arr64_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr64_1 \
    op interface \
    ports { real_arr64_1_address0 { O 4 vector } real_arr64_1_ce0 { O 1 bit } real_arr64_1_we0 { O 1 bit } real_arr64_1_d0 { O 32 vector } real_arr64_1_q0 { I 32 vector } real_arr64_1_address1 { O 4 vector } real_arr64_1_ce1 { O 1 bit } real_arr64_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr64_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 515 \
    name imag_arr64_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr64_1 \
    op interface \
    ports { imag_arr64_1_address0 { O 4 vector } imag_arr64_1_ce0 { O 1 bit } imag_arr64_1_we0 { O 1 bit } imag_arr64_1_d0 { O 32 vector } imag_arr64_1_q0 { I 32 vector } imag_arr64_1_address1 { O 4 vector } imag_arr64_1_ce1 { O 1 bit } imag_arr64_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr64_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 516 \
    name real_arr64_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr64_2 \
    op interface \
    ports { real_arr64_2_address0 { O 4 vector } real_arr64_2_ce0 { O 1 bit } real_arr64_2_we0 { O 1 bit } real_arr64_2_d0 { O 32 vector } real_arr64_2_q0 { I 32 vector } real_arr64_2_address1 { O 4 vector } real_arr64_2_ce1 { O 1 bit } real_arr64_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr64_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 517 \
    name imag_arr64_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr64_2 \
    op interface \
    ports { imag_arr64_2_address0 { O 4 vector } imag_arr64_2_ce0 { O 1 bit } imag_arr64_2_we0 { O 1 bit } imag_arr64_2_d0 { O 32 vector } imag_arr64_2_q0 { I 32 vector } imag_arr64_2_address1 { O 4 vector } imag_arr64_2_ce1 { O 1 bit } imag_arr64_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr64_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 518 \
    name real_arr64_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr64_3 \
    op interface \
    ports { real_arr64_3_address0 { O 4 vector } real_arr64_3_ce0 { O 1 bit } real_arr64_3_we0 { O 1 bit } real_arr64_3_d0 { O 32 vector } real_arr64_3_q0 { I 32 vector } real_arr64_3_address1 { O 4 vector } real_arr64_3_ce1 { O 1 bit } real_arr64_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr64_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 519 \
    name imag_arr64_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr64_3 \
    op interface \
    ports { imag_arr64_3_address0 { O 4 vector } imag_arr64_3_ce0 { O 1 bit } imag_arr64_3_we0 { O 1 bit } imag_arr64_3_d0 { O 32 vector } imag_arr64_3_q0 { I 32 vector } imag_arr64_3_address1 { O 4 vector } imag_arr64_3_ce1 { O 1 bit } imag_arr64_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr64_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 520 \
    name real_arr32_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr32_0 \
    op interface \
    ports { real_arr32_0_address0 { O 3 vector } real_arr32_0_ce0 { O 1 bit } real_arr32_0_we0 { O 1 bit } real_arr32_0_d0 { O 32 vector } real_arr32_0_q0 { I 32 vector } real_arr32_0_address1 { O 3 vector } real_arr32_0_ce1 { O 1 bit } real_arr32_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr32_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 521 \
    name imag_arr32_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr32_0 \
    op interface \
    ports { imag_arr32_0_address0 { O 3 vector } imag_arr32_0_ce0 { O 1 bit } imag_arr32_0_we0 { O 1 bit } imag_arr32_0_d0 { O 32 vector } imag_arr32_0_q0 { I 32 vector } imag_arr32_0_address1 { O 3 vector } imag_arr32_0_ce1 { O 1 bit } imag_arr32_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr32_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 522 \
    name real_arr32_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr32_1 \
    op interface \
    ports { real_arr32_1_address0 { O 3 vector } real_arr32_1_ce0 { O 1 bit } real_arr32_1_we0 { O 1 bit } real_arr32_1_d0 { O 32 vector } real_arr32_1_q0 { I 32 vector } real_arr32_1_address1 { O 3 vector } real_arr32_1_ce1 { O 1 bit } real_arr32_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr32_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 523 \
    name imag_arr32_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr32_1 \
    op interface \
    ports { imag_arr32_1_address0 { O 3 vector } imag_arr32_1_ce0 { O 1 bit } imag_arr32_1_we0 { O 1 bit } imag_arr32_1_d0 { O 32 vector } imag_arr32_1_q0 { I 32 vector } imag_arr32_1_address1 { O 3 vector } imag_arr32_1_ce1 { O 1 bit } imag_arr32_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr32_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 524 \
    name real_arr32_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr32_2 \
    op interface \
    ports { real_arr32_2_address0 { O 3 vector } real_arr32_2_ce0 { O 1 bit } real_arr32_2_we0 { O 1 bit } real_arr32_2_d0 { O 32 vector } real_arr32_2_q0 { I 32 vector } real_arr32_2_address1 { O 3 vector } real_arr32_2_ce1 { O 1 bit } real_arr32_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr32_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 525 \
    name imag_arr32_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr32_2 \
    op interface \
    ports { imag_arr32_2_address0 { O 3 vector } imag_arr32_2_ce0 { O 1 bit } imag_arr32_2_we0 { O 1 bit } imag_arr32_2_d0 { O 32 vector } imag_arr32_2_q0 { I 32 vector } imag_arr32_2_address1 { O 3 vector } imag_arr32_2_ce1 { O 1 bit } imag_arr32_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr32_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 526 \
    name real_arr32_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr32_3 \
    op interface \
    ports { real_arr32_3_address0 { O 3 vector } real_arr32_3_ce0 { O 1 bit } real_arr32_3_we0 { O 1 bit } real_arr32_3_d0 { O 32 vector } real_arr32_3_q0 { I 32 vector } real_arr32_3_address1 { O 3 vector } real_arr32_3_ce1 { O 1 bit } real_arr32_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr32_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 527 \
    name imag_arr32_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr32_3 \
    op interface \
    ports { imag_arr32_3_address0 { O 3 vector } imag_arr32_3_ce0 { O 1 bit } imag_arr32_3_we0 { O 1 bit } imag_arr32_3_d0 { O 32 vector } imag_arr32_3_q0 { I 32 vector } imag_arr32_3_address1 { O 3 vector } imag_arr32_3_ce1 { O 1 bit } imag_arr32_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr32_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 528 \
    name real_arr16_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr16_0 \
    op interface \
    ports { real_arr16_0_address0 { O 2 vector } real_arr16_0_ce0 { O 1 bit } real_arr16_0_we0 { O 1 bit } real_arr16_0_d0 { O 32 vector } real_arr16_0_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr16_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 529 \
    name imag_arr16_0 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr16_0 \
    op interface \
    ports { imag_arr16_0_address0 { O 2 vector } imag_arr16_0_ce0 { O 1 bit } imag_arr16_0_we0 { O 1 bit } imag_arr16_0_d0 { O 32 vector } imag_arr16_0_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr16_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 530 \
    name real_arr16_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr16_1 \
    op interface \
    ports { real_arr16_1_address0 { O 2 vector } real_arr16_1_ce0 { O 1 bit } real_arr16_1_we0 { O 1 bit } real_arr16_1_d0 { O 32 vector } real_arr16_1_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr16_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 531 \
    name imag_arr16_1 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr16_1 \
    op interface \
    ports { imag_arr16_1_address0 { O 2 vector } imag_arr16_1_ce0 { O 1 bit } imag_arr16_1_we0 { O 1 bit } imag_arr16_1_d0 { O 32 vector } imag_arr16_1_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr16_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 532 \
    name real_arr16_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr16_2 \
    op interface \
    ports { real_arr16_2_address0 { O 2 vector } real_arr16_2_ce0 { O 1 bit } real_arr16_2_we0 { O 1 bit } real_arr16_2_d0 { O 32 vector } real_arr16_2_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr16_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 533 \
    name imag_arr16_2 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr16_2 \
    op interface \
    ports { imag_arr16_2_address0 { O 2 vector } imag_arr16_2_ce0 { O 1 bit } imag_arr16_2_we0 { O 1 bit } imag_arr16_2_d0 { O 32 vector } imag_arr16_2_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr16_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 534 \
    name real_arr16_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename real_arr16_3 \
    op interface \
    ports { real_arr16_3_address0 { O 2 vector } real_arr16_3_ce0 { O 1 bit } real_arr16_3_we0 { O 1 bit } real_arr16_3_d0 { O 32 vector } real_arr16_3_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr16_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 535 \
    name imag_arr16_3 \
    reset_level 1 \
    sync_rst true \
    dir IO \
    corename imag_arr16_3 \
    op interface \
    ports { imag_arr16_3_address0 { O 2 vector } imag_arr16_3_ce0 { O 1 bit } imag_arr16_3_we0 { O 1 bit } imag_arr16_3_d0 { O 32 vector } imag_arr16_3_q0 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr16_3'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 536 \
    name imag_arr8_1 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_1 \
    op interface \
    ports { imag_arr8_1_i { I 32 vector } imag_arr8_1_o { O 32 vector } imag_arr8_1_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 537 \
    name real_arr8_1 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_1 \
    op interface \
    ports { real_arr8_1_i { I 32 vector } real_arr8_1_o { O 32 vector } real_arr8_1_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 538 \
    name imag_arr8_0 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_0 \
    op interface \
    ports { imag_arr8_0_i { I 32 vector } imag_arr8_0_o { O 32 vector } imag_arr8_0_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 539 \
    name real_arr8_0 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_0 \
    op interface \
    ports { real_arr8_0_i { I 32 vector } real_arr8_0_o { O 32 vector } real_arr8_0_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 540 \
    name real_arr8_2 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_2 \
    op interface \
    ports { real_arr8_2_i { I 32 vector } real_arr8_2_o { O 32 vector } real_arr8_2_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 541 \
    name real_arr8_4 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_4 \
    op interface \
    ports { real_arr8_4_i { I 32 vector } real_arr8_4_o { O 32 vector } real_arr8_4_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 542 \
    name real_arr8_6 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_6 \
    op interface \
    ports { real_arr8_6_i { I 32 vector } real_arr8_6_o { O 32 vector } real_arr8_6_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 543 \
    name imag_arr8_2 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_2 \
    op interface \
    ports { imag_arr8_2_i { I 32 vector } imag_arr8_2_o { O 32 vector } imag_arr8_2_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 544 \
    name imag_arr8_4 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_4 \
    op interface \
    ports { imag_arr8_4_i { I 32 vector } imag_arr8_4_o { O 32 vector } imag_arr8_4_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 545 \
    name imag_arr8_6 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_6 \
    op interface \
    ports { imag_arr8_6_i { I 32 vector } imag_arr8_6_o { O 32 vector } imag_arr8_6_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 546 \
    name real_arr8_3 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_3 \
    op interface \
    ports { real_arr8_3_i { I 32 vector } real_arr8_3_o { O 32 vector } real_arr8_3_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 547 \
    name real_arr8_5 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_5 \
    op interface \
    ports { real_arr8_5_i { I 32 vector } real_arr8_5_o { O 32 vector } real_arr8_5_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 548 \
    name real_arr8_7 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr8_7 \
    op interface \
    ports { real_arr8_7_i { I 32 vector } real_arr8_7_o { O 32 vector } real_arr8_7_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 549 \
    name imag_arr8_3 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_3 \
    op interface \
    ports { imag_arr8_3_i { I 32 vector } imag_arr8_3_o { O 32 vector } imag_arr8_3_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 550 \
    name imag_arr8_5 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_5 \
    op interface \
    ports { imag_arr8_5_i { I 32 vector } imag_arr8_5_o { O 32 vector } imag_arr8_5_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 551 \
    name imag_arr8_7 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr8_7 \
    op interface \
    ports { imag_arr8_7_i { I 32 vector } imag_arr8_7_o { O 32 vector } imag_arr8_7_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 552 \
    name imag_arr4_0 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr4_0 \
    op interface \
    ports { imag_arr4_0_i { I 32 vector } imag_arr4_0_o { O 32 vector } imag_arr4_0_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 553 \
    name real_arr4_0 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr4_0 \
    op interface \
    ports { real_arr4_0_i { I 32 vector } real_arr4_0_o { O 32 vector } real_arr4_0_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 554 \
    name real_arr4_1 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr4_1 \
    op interface \
    ports { real_arr4_1_i { I 32 vector } real_arr4_1_o { O 32 vector } real_arr4_1_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 555 \
    name real_arr4_2 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr4_2 \
    op interface \
    ports { real_arr4_2_i { I 32 vector } real_arr4_2_o { O 32 vector } real_arr4_2_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 556 \
    name real_arr4_3 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_real_arr4_3 \
    op interface \
    ports { real_arr4_3_i { I 32 vector } real_arr4_3_o { O 32 vector } real_arr4_3_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 557 \
    name imag_arr4_1 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr4_1 \
    op interface \
    ports { imag_arr4_1_i { I 32 vector } imag_arr4_1_o { O 32 vector } imag_arr4_1_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 558 \
    name imag_arr4_2 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr4_2 \
    op interface \
    ports { imag_arr4_2_i { I 32 vector } imag_arr4_2_o { O 32 vector } imag_arr4_2_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 559 \
    name imag_arr4_3 \
    type other \
    dir IO \
    reset_level 1 \
    sync_rst true \
    corename dc_imag_arr4_3 \
    op interface \
    ports { imag_arr4_3_i { I 32 vector } imag_arr4_3_o { O 32 vector } imag_arr4_3_o_ap_vld { O 1 bit } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


