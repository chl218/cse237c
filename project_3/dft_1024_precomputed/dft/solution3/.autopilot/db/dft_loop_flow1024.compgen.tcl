# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 78 \
    name real_i_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_0 \
    op interface \
    ports { real_i_0_address0 { O 7 vector } real_i_0_ce0 { O 1 bit } real_i_0_d0 { O 32 vector } real_i_0_q0 { I 32 vector } real_i_0_we0 { O 1 bit } real_i_0_address1 { O 7 vector } real_i_0_ce1 { O 1 bit } real_i_0_d1 { O 32 vector } real_i_0_q1 { I 32 vector } real_i_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 79 \
    name real_i_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_1 \
    op interface \
    ports { real_i_1_address0 { O 7 vector } real_i_1_ce0 { O 1 bit } real_i_1_d0 { O 32 vector } real_i_1_q0 { I 32 vector } real_i_1_we0 { O 1 bit } real_i_1_address1 { O 7 vector } real_i_1_ce1 { O 1 bit } real_i_1_d1 { O 32 vector } real_i_1_q1 { I 32 vector } real_i_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 80 \
    name real_i_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_2 \
    op interface \
    ports { real_i_2_address0 { O 7 vector } real_i_2_ce0 { O 1 bit } real_i_2_d0 { O 32 vector } real_i_2_q0 { I 32 vector } real_i_2_we0 { O 1 bit } real_i_2_address1 { O 7 vector } real_i_2_ce1 { O 1 bit } real_i_2_d1 { O 32 vector } real_i_2_q1 { I 32 vector } real_i_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 81 \
    name real_i_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_3 \
    op interface \
    ports { real_i_3_address0 { O 7 vector } real_i_3_ce0 { O 1 bit } real_i_3_d0 { O 32 vector } real_i_3_q0 { I 32 vector } real_i_3_we0 { O 1 bit } real_i_3_address1 { O 7 vector } real_i_3_ce1 { O 1 bit } real_i_3_d1 { O 32 vector } real_i_3_q1 { I 32 vector } real_i_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 82 \
    name real_i_4 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_4 \
    op interface \
    ports { real_i_4_address0 { O 7 vector } real_i_4_ce0 { O 1 bit } real_i_4_d0 { O 32 vector } real_i_4_q0 { I 32 vector } real_i_4_we0 { O 1 bit } real_i_4_address1 { O 7 vector } real_i_4_ce1 { O 1 bit } real_i_4_d1 { O 32 vector } real_i_4_q1 { I 32 vector } real_i_4_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_4'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 83 \
    name real_i_5 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_5 \
    op interface \
    ports { real_i_5_address0 { O 7 vector } real_i_5_ce0 { O 1 bit } real_i_5_d0 { O 32 vector } real_i_5_q0 { I 32 vector } real_i_5_we0 { O 1 bit } real_i_5_address1 { O 7 vector } real_i_5_ce1 { O 1 bit } real_i_5_d1 { O 32 vector } real_i_5_q1 { I 32 vector } real_i_5_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_5'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 84 \
    name real_i_6 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_6 \
    op interface \
    ports { real_i_6_address0 { O 7 vector } real_i_6_ce0 { O 1 bit } real_i_6_d0 { O 32 vector } real_i_6_q0 { I 32 vector } real_i_6_we0 { O 1 bit } real_i_6_address1 { O 7 vector } real_i_6_ce1 { O 1 bit } real_i_6_d1 { O 32 vector } real_i_6_q1 { I 32 vector } real_i_6_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_6'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 85 \
    name real_i_7 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_i_7 \
    op interface \
    ports { real_i_7_address0 { O 7 vector } real_i_7_ce0 { O 1 bit } real_i_7_d0 { O 32 vector } real_i_7_q0 { I 32 vector } real_i_7_we0 { O 1 bit } real_i_7_address1 { O 7 vector } real_i_7_ce1 { O 1 bit } real_i_7_d1 { O 32 vector } real_i_7_q1 { I 32 vector } real_i_7_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_i_7'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 86 \
    name imag_i_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_0 \
    op interface \
    ports { imag_i_0_address0 { O 7 vector } imag_i_0_ce0 { O 1 bit } imag_i_0_d0 { O 32 vector } imag_i_0_q0 { I 32 vector } imag_i_0_we0 { O 1 bit } imag_i_0_address1 { O 7 vector } imag_i_0_ce1 { O 1 bit } imag_i_0_d1 { O 32 vector } imag_i_0_q1 { I 32 vector } imag_i_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 87 \
    name imag_i_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_1 \
    op interface \
    ports { imag_i_1_address0 { O 7 vector } imag_i_1_ce0 { O 1 bit } imag_i_1_d0 { O 32 vector } imag_i_1_q0 { I 32 vector } imag_i_1_we0 { O 1 bit } imag_i_1_address1 { O 7 vector } imag_i_1_ce1 { O 1 bit } imag_i_1_d1 { O 32 vector } imag_i_1_q1 { I 32 vector } imag_i_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 88 \
    name imag_i_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_2 \
    op interface \
    ports { imag_i_2_address0 { O 7 vector } imag_i_2_ce0 { O 1 bit } imag_i_2_d0 { O 32 vector } imag_i_2_q0 { I 32 vector } imag_i_2_we0 { O 1 bit } imag_i_2_address1 { O 7 vector } imag_i_2_ce1 { O 1 bit } imag_i_2_d1 { O 32 vector } imag_i_2_q1 { I 32 vector } imag_i_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 89 \
    name imag_i_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_3 \
    op interface \
    ports { imag_i_3_address0 { O 7 vector } imag_i_3_ce0 { O 1 bit } imag_i_3_d0 { O 32 vector } imag_i_3_q0 { I 32 vector } imag_i_3_we0 { O 1 bit } imag_i_3_address1 { O 7 vector } imag_i_3_ce1 { O 1 bit } imag_i_3_d1 { O 32 vector } imag_i_3_q1 { I 32 vector } imag_i_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 90 \
    name imag_i_4 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_4 \
    op interface \
    ports { imag_i_4_address0 { O 7 vector } imag_i_4_ce0 { O 1 bit } imag_i_4_d0 { O 32 vector } imag_i_4_q0 { I 32 vector } imag_i_4_we0 { O 1 bit } imag_i_4_address1 { O 7 vector } imag_i_4_ce1 { O 1 bit } imag_i_4_d1 { O 32 vector } imag_i_4_q1 { I 32 vector } imag_i_4_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_4'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 91 \
    name imag_i_5 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_5 \
    op interface \
    ports { imag_i_5_address0 { O 7 vector } imag_i_5_ce0 { O 1 bit } imag_i_5_d0 { O 32 vector } imag_i_5_q0 { I 32 vector } imag_i_5_we0 { O 1 bit } imag_i_5_address1 { O 7 vector } imag_i_5_ce1 { O 1 bit } imag_i_5_d1 { O 32 vector } imag_i_5_q1 { I 32 vector } imag_i_5_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_5'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 92 \
    name imag_i_6 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_6 \
    op interface \
    ports { imag_i_6_address0 { O 7 vector } imag_i_6_ce0 { O 1 bit } imag_i_6_d0 { O 32 vector } imag_i_6_q0 { I 32 vector } imag_i_6_we0 { O 1 bit } imag_i_6_address1 { O 7 vector } imag_i_6_ce1 { O 1 bit } imag_i_6_d1 { O 32 vector } imag_i_6_q1 { I 32 vector } imag_i_6_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_6'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 93 \
    name imag_i_7 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_i_7 \
    op interface \
    ports { imag_i_7_address0 { O 7 vector } imag_i_7_ce0 { O 1 bit } imag_i_7_d0 { O 32 vector } imag_i_7_q0 { I 32 vector } imag_i_7_we0 { O 1 bit } imag_i_7_address1 { O 7 vector } imag_i_7_ce1 { O 1 bit } imag_i_7_d1 { O 32 vector } imag_i_7_q1 { I 32 vector } imag_i_7_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_i_7'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 94 \
    name imag_arr1024_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr1024_1 \
    op interface \
    ports { imag_arr1024_1_address0 { O 8 vector } imag_arr1024_1_ce0 { O 1 bit } imag_arr1024_1_d0 { O 32 vector } imag_arr1024_1_q0 { I 32 vector } imag_arr1024_1_we0 { O 1 bit } imag_arr1024_1_address1 { O 8 vector } imag_arr1024_1_ce1 { O 1 bit } imag_arr1024_1_d1 { O 32 vector } imag_arr1024_1_q1 { I 32 vector } imag_arr1024_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr1024_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 95 \
    name real_arr1024_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr1024_1 \
    op interface \
    ports { real_arr1024_1_address0 { O 8 vector } real_arr1024_1_ce0 { O 1 bit } real_arr1024_1_d0 { O 32 vector } real_arr1024_1_q0 { I 32 vector } real_arr1024_1_we0 { O 1 bit } real_arr1024_1_address1 { O 8 vector } real_arr1024_1_ce1 { O 1 bit } real_arr1024_1_d1 { O 32 vector } real_arr1024_1_q1 { I 32 vector } real_arr1024_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr1024_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 96 \
    name imag_arr1024_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr1024_0 \
    op interface \
    ports { imag_arr1024_0_address0 { O 8 vector } imag_arr1024_0_ce0 { O 1 bit } imag_arr1024_0_d0 { O 32 vector } imag_arr1024_0_q0 { I 32 vector } imag_arr1024_0_we0 { O 1 bit } imag_arr1024_0_address1 { O 8 vector } imag_arr1024_0_ce1 { O 1 bit } imag_arr1024_0_d1 { O 32 vector } imag_arr1024_0_q1 { I 32 vector } imag_arr1024_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr1024_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 97 \
    name real_arr1024_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr1024_0 \
    op interface \
    ports { real_arr1024_0_address0 { O 8 vector } real_arr1024_0_ce0 { O 1 bit } real_arr1024_0_d0 { O 32 vector } real_arr1024_0_q0 { I 32 vector } real_arr1024_0_we0 { O 1 bit } real_arr1024_0_address1 { O 8 vector } real_arr1024_0_ce1 { O 1 bit } real_arr1024_0_d1 { O 32 vector } real_arr1024_0_q1 { I 32 vector } real_arr1024_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr1024_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 98 \
    name imag_arr1024_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr1024_3 \
    op interface \
    ports { imag_arr1024_3_address0 { O 8 vector } imag_arr1024_3_ce0 { O 1 bit } imag_arr1024_3_d0 { O 32 vector } imag_arr1024_3_q0 { I 32 vector } imag_arr1024_3_we0 { O 1 bit } imag_arr1024_3_address1 { O 8 vector } imag_arr1024_3_ce1 { O 1 bit } imag_arr1024_3_d1 { O 32 vector } imag_arr1024_3_q1 { I 32 vector } imag_arr1024_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr1024_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 99 \
    name real_arr1024_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr1024_3 \
    op interface \
    ports { real_arr1024_3_address0 { O 8 vector } real_arr1024_3_ce0 { O 1 bit } real_arr1024_3_d0 { O 32 vector } real_arr1024_3_q0 { I 32 vector } real_arr1024_3_we0 { O 1 bit } real_arr1024_3_address1 { O 8 vector } real_arr1024_3_ce1 { O 1 bit } real_arr1024_3_d1 { O 32 vector } real_arr1024_3_q1 { I 32 vector } real_arr1024_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr1024_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 100 \
    name imag_arr1024_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr1024_2 \
    op interface \
    ports { imag_arr1024_2_address0 { O 8 vector } imag_arr1024_2_ce0 { O 1 bit } imag_arr1024_2_d0 { O 32 vector } imag_arr1024_2_q0 { I 32 vector } imag_arr1024_2_we0 { O 1 bit } imag_arr1024_2_address1 { O 8 vector } imag_arr1024_2_ce1 { O 1 bit } imag_arr1024_2_d1 { O 32 vector } imag_arr1024_2_q1 { I 32 vector } imag_arr1024_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr1024_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 101 \
    name real_arr1024_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr1024_2 \
    op interface \
    ports { real_arr1024_2_address0 { O 8 vector } real_arr1024_2_ce0 { O 1 bit } real_arr1024_2_d0 { O 32 vector } real_arr1024_2_q0 { I 32 vector } real_arr1024_2_we0 { O 1 bit } real_arr1024_2_address1 { O 8 vector } real_arr1024_2_ce1 { O 1 bit } real_arr1024_2_d1 { O 32 vector } real_arr1024_2_q1 { I 32 vector } real_arr1024_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr1024_2'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id 77 \
    name i_V_read \
    type other \
    dir I \
    reset_level 1 \
    sync_rst true \
    corename dc_i_V_read \
    op interface \
    ports { i_V_read { I 11 vector } } \
} "
}

# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


