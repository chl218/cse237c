set moduleName dft_Loop_Loop_Row_proc
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {dft_Loop_Loop_Row_proc}
set C_modelType { void 0 }
set C_modelArgList { 
	{ real_i_0 float 32 regular {array 128 { 1 1 } 1 1 }  }
	{ real_i_1 float 32 regular {array 128 { 1 1 } 1 1 }  }
	{ real_i_2 float 32 regular {array 128 { 1 1 } 1 1 }  }
	{ real_i_3 float 32 regular {array 128 { 1 1 } 1 1 }  }
	{ real_i_4 float 32 regular {array 128 { 1 1 } 1 1 }  }
	{ real_i_5 float 32 regular {array 128 { 1 1 } 1 1 }  }
	{ real_i_6 float 32 regular {array 128 { 1 1 } 1 1 }  }
	{ real_i_7 float 32 regular {array 128 { 1 1 } 1 1 }  }
	{ imag_i_0 float 32 regular {array 128 { 1 1 } 1 1 }  }
	{ imag_i_1 float 32 regular {array 128 { 1 1 } 1 1 }  }
	{ imag_i_2 float 32 regular {array 128 { 1 1 } 1 1 }  }
	{ imag_i_3 float 32 regular {array 128 { 1 1 } 1 1 }  }
	{ imag_i_4 float 32 regular {array 128 { 1 1 } 1 1 }  }
	{ imag_i_5 float 32 regular {array 128 { 1 1 } 1 1 }  }
	{ imag_i_6 float 32 regular {array 128 { 1 1 } 1 1 }  }
	{ imag_i_7 float 32 regular {array 128 { 1 1 } 1 1 }  }
	{ real_o float 32 regular {array 1024 { 0 3 } 0 1 }  }
	{ imag_o float 32 regular {array 1024 { 0 3 } 0 1 }  }
	{ imag_arr1024_1 float 32 regular {array 256 { 2 1 } 1 1 } {global 2}  }
	{ real_arr1024_1 float 32 regular {array 256 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr1024_0 float 32 regular {array 256 { 2 1 } 1 1 } {global 2}  }
	{ real_arr1024_0 float 32 regular {array 256 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr1024_3 float 32 regular {array 256 { 2 1 } 1 1 } {global 2}  }
	{ real_arr1024_3 float 32 regular {array 256 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr1024_2 float 32 regular {array 256 { 2 1 } 1 1 } {global 2}  }
	{ real_arr1024_2 float 32 regular {array 256 { 2 1 } 1 1 } {global 2}  }
	{ real_arr512_0 float 32 regular {array 128 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr512_0 float 32 regular {array 128 { 2 1 } 1 1 } {global 2}  }
	{ real_arr512_1 float 32 regular {array 128 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr512_1 float 32 regular {array 128 { 2 1 } 1 1 } {global 2}  }
	{ real_arr512_2 float 32 regular {array 128 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr512_2 float 32 regular {array 128 { 2 1 } 1 1 } {global 2}  }
	{ real_arr512_3 float 32 regular {array 128 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr512_3 float 32 regular {array 128 { 2 1 } 1 1 } {global 2}  }
	{ real_arr256_0 float 32 regular {array 64 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr256_0 float 32 regular {array 64 { 2 1 } 1 1 } {global 2}  }
	{ real_arr256_1 float 32 regular {array 64 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr256_1 float 32 regular {array 64 { 2 1 } 1 1 } {global 2}  }
	{ real_arr256_2 float 32 regular {array 64 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr256_2 float 32 regular {array 64 { 2 1 } 1 1 } {global 2}  }
	{ real_arr256_3 float 32 regular {array 64 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr256_3 float 32 regular {array 64 { 2 1 } 1 1 } {global 2}  }
	{ real_arr128_0 float 32 regular {array 32 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr128_0 float 32 regular {array 32 { 2 1 } 1 1 } {global 2}  }
	{ real_arr128_1 float 32 regular {array 32 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr128_1 float 32 regular {array 32 { 2 1 } 1 1 } {global 2}  }
	{ real_arr128_2 float 32 regular {array 32 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr128_2 float 32 regular {array 32 { 2 1 } 1 1 } {global 2}  }
	{ real_arr128_3 float 32 regular {array 32 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr128_3 float 32 regular {array 32 { 2 1 } 1 1 } {global 2}  }
	{ real_arr64_0 float 32 regular {array 16 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr64_0 float 32 regular {array 16 { 2 1 } 1 1 } {global 2}  }
	{ real_arr64_1 float 32 regular {array 16 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr64_1 float 32 regular {array 16 { 2 1 } 1 1 } {global 2}  }
	{ real_arr64_2 float 32 regular {array 16 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr64_2 float 32 regular {array 16 { 2 1 } 1 1 } {global 2}  }
	{ real_arr64_3 float 32 regular {array 16 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr64_3 float 32 regular {array 16 { 2 1 } 1 1 } {global 2}  }
	{ real_arr32_0 float 32 regular {array 8 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr32_0 float 32 regular {array 8 { 2 1 } 1 1 } {global 2}  }
	{ real_arr32_1 float 32 regular {array 8 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr32_1 float 32 regular {array 8 { 2 1 } 1 1 } {global 2}  }
	{ real_arr32_2 float 32 regular {array 8 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr32_2 float 32 regular {array 8 { 2 1 } 1 1 } {global 2}  }
	{ real_arr32_3 float 32 regular {array 8 { 2 1 } 1 1 } {global 2}  }
	{ imag_arr32_3 float 32 regular {array 8 { 2 1 } 1 1 } {global 2}  }
	{ real_arr16_0 float 32 regular {array 4 { 2 3 } 1 1 } {global 2}  }
	{ imag_arr16_0 float 32 regular {array 4 { 2 3 } 1 1 } {global 2}  }
	{ real_arr16_1 float 32 regular {array 4 { 2 3 } 1 1 } {global 2}  }
	{ imag_arr16_1 float 32 regular {array 4 { 2 3 } 1 1 } {global 2}  }
	{ real_arr16_2 float 32 regular {array 4 { 2 3 } 1 1 } {global 2}  }
	{ imag_arr16_2 float 32 regular {array 4 { 2 3 } 1 1 } {global 2}  }
	{ real_arr16_3 float 32 regular {array 4 { 2 3 } 1 1 } {global 2}  }
	{ imag_arr16_3 float 32 regular {array 4 { 2 3 } 1 1 } {global 2}  }
	{ imag_arr8_1 float 32 regular {pointer 2} {global 2}  }
	{ real_arr8_1 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr8_0 float 32 regular {pointer 2} {global 2}  }
	{ real_arr8_0 float 32 regular {pointer 2} {global 2}  }
	{ real_arr8_2 float 32 regular {pointer 2} {global 2}  }
	{ real_arr8_4 float 32 regular {pointer 2} {global 2}  }
	{ real_arr8_6 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr8_2 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr8_4 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr8_6 float 32 regular {pointer 2} {global 2}  }
	{ real_arr8_3 float 32 regular {pointer 2} {global 2}  }
	{ real_arr8_5 float 32 regular {pointer 2} {global 2}  }
	{ real_arr8_7 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr8_3 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr8_5 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr8_7 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr4_0 float 32 regular {pointer 2} {global 2}  }
	{ real_arr4_0 float 32 regular {pointer 2} {global 2}  }
	{ real_arr4_1 float 32 regular {pointer 2} {global 2}  }
	{ real_arr4_2 float 32 regular {pointer 2} {global 2}  }
	{ real_arr4_3 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr4_1 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr4_2 float 32 regular {pointer 2} {global 2}  }
	{ imag_arr4_3 float 32 regular {pointer 2} {global 2}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "real_i_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_4", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_5", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_6", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_i_7", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_4", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_5", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_6", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "imag_i_7", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY"} , 
 	{ "Name" : "real_o", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "imag_o", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY"} , 
 	{ "Name" : "imag_arr1024_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr1024","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1021,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr1024_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr1024","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1021,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr1024_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr1024","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 1020,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr1024_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr1024","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 1020,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr1024_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr1024","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 1023,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr1024_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr1024","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 1023,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr1024_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr1024","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 1022,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr1024_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr1024","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 1022,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr512_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr512","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 508,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr512_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr512","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 508,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr512_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr512","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 509,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr512_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr512","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 509,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr512_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr512","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 510,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr512_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr512","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 510,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr512_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr512","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 511,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr512_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr512","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 511,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr256_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 252,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr256_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 252,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr256_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 253,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr256_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 253,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr256_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 254,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr256_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 254,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr256_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 255,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr256_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 255,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr128_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 124,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr128_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 124,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr128_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 125,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr128_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 125,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr128_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 126,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr128_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 126,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr128_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 127,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr128_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 127,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr64_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr64","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 60,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr64_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr64","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 60,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr64_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr64","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 61,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr64_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr64","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 61,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr64_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr64","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 62,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr64_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr64","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 62,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr64_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr64","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 63,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr64_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr64","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 63,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr32_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr32","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 28,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr32_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr32","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 28,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr32_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr32","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 29,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr32_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr32","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 29,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr32_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr32","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 30,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr32_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr32","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 30,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr32_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr32","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 31,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr32_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr32","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 31,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr16_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 12,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr16_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 12,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr16_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 13,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr16_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 13,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr16_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 14,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr16_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 14,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr16_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 15,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr16_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr16","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 15,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_1", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_1", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_0", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_0", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_2", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 2,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_4", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 4,"up" : 4,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_6", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 6,"up" : 6,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_2", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 2,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_4", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 4,"up" : 4,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_6", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 6,"up" : 6,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_3", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 3,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_5", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 5,"up" : 5,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr8_7", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 7,"up" : 7,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_3", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 3,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_5", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 5,"up" : 5,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr8_7", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr8","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 7,"up" : 7,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr4_0", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr4_0", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr4_1", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr4_2", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 2,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr4_3", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 3,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr4_1", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr4_2", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 2,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr4_3", "interface" : "wire", "bitwidth" : 32, "direction" : "READWRITE", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 3,"step" : 2}]}]}], "extern" : 0} ]}
# RTL Port declarations: 
set portNum 607
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_continue sc_in sc_logic 1 continue -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ real_i_0_address0 sc_out sc_lv 7 signal 0 } 
	{ real_i_0_ce0 sc_out sc_logic 1 signal 0 } 
	{ real_i_0_q0 sc_in sc_lv 32 signal 0 } 
	{ real_i_0_address1 sc_out sc_lv 7 signal 0 } 
	{ real_i_0_ce1 sc_out sc_logic 1 signal 0 } 
	{ real_i_0_q1 sc_in sc_lv 32 signal 0 } 
	{ real_i_1_address0 sc_out sc_lv 7 signal 1 } 
	{ real_i_1_ce0 sc_out sc_logic 1 signal 1 } 
	{ real_i_1_q0 sc_in sc_lv 32 signal 1 } 
	{ real_i_1_address1 sc_out sc_lv 7 signal 1 } 
	{ real_i_1_ce1 sc_out sc_logic 1 signal 1 } 
	{ real_i_1_q1 sc_in sc_lv 32 signal 1 } 
	{ real_i_2_address0 sc_out sc_lv 7 signal 2 } 
	{ real_i_2_ce0 sc_out sc_logic 1 signal 2 } 
	{ real_i_2_q0 sc_in sc_lv 32 signal 2 } 
	{ real_i_2_address1 sc_out sc_lv 7 signal 2 } 
	{ real_i_2_ce1 sc_out sc_logic 1 signal 2 } 
	{ real_i_2_q1 sc_in sc_lv 32 signal 2 } 
	{ real_i_3_address0 sc_out sc_lv 7 signal 3 } 
	{ real_i_3_ce0 sc_out sc_logic 1 signal 3 } 
	{ real_i_3_q0 sc_in sc_lv 32 signal 3 } 
	{ real_i_3_address1 sc_out sc_lv 7 signal 3 } 
	{ real_i_3_ce1 sc_out sc_logic 1 signal 3 } 
	{ real_i_3_q1 sc_in sc_lv 32 signal 3 } 
	{ real_i_4_address0 sc_out sc_lv 7 signal 4 } 
	{ real_i_4_ce0 sc_out sc_logic 1 signal 4 } 
	{ real_i_4_q0 sc_in sc_lv 32 signal 4 } 
	{ real_i_4_address1 sc_out sc_lv 7 signal 4 } 
	{ real_i_4_ce1 sc_out sc_logic 1 signal 4 } 
	{ real_i_4_q1 sc_in sc_lv 32 signal 4 } 
	{ real_i_5_address0 sc_out sc_lv 7 signal 5 } 
	{ real_i_5_ce0 sc_out sc_logic 1 signal 5 } 
	{ real_i_5_q0 sc_in sc_lv 32 signal 5 } 
	{ real_i_5_address1 sc_out sc_lv 7 signal 5 } 
	{ real_i_5_ce1 sc_out sc_logic 1 signal 5 } 
	{ real_i_5_q1 sc_in sc_lv 32 signal 5 } 
	{ real_i_6_address0 sc_out sc_lv 7 signal 6 } 
	{ real_i_6_ce0 sc_out sc_logic 1 signal 6 } 
	{ real_i_6_q0 sc_in sc_lv 32 signal 6 } 
	{ real_i_6_address1 sc_out sc_lv 7 signal 6 } 
	{ real_i_6_ce1 sc_out sc_logic 1 signal 6 } 
	{ real_i_6_q1 sc_in sc_lv 32 signal 6 } 
	{ real_i_7_address0 sc_out sc_lv 7 signal 7 } 
	{ real_i_7_ce0 sc_out sc_logic 1 signal 7 } 
	{ real_i_7_q0 sc_in sc_lv 32 signal 7 } 
	{ real_i_7_address1 sc_out sc_lv 7 signal 7 } 
	{ real_i_7_ce1 sc_out sc_logic 1 signal 7 } 
	{ real_i_7_q1 sc_in sc_lv 32 signal 7 } 
	{ imag_i_0_address0 sc_out sc_lv 7 signal 8 } 
	{ imag_i_0_ce0 sc_out sc_logic 1 signal 8 } 
	{ imag_i_0_q0 sc_in sc_lv 32 signal 8 } 
	{ imag_i_0_address1 sc_out sc_lv 7 signal 8 } 
	{ imag_i_0_ce1 sc_out sc_logic 1 signal 8 } 
	{ imag_i_0_q1 sc_in sc_lv 32 signal 8 } 
	{ imag_i_1_address0 sc_out sc_lv 7 signal 9 } 
	{ imag_i_1_ce0 sc_out sc_logic 1 signal 9 } 
	{ imag_i_1_q0 sc_in sc_lv 32 signal 9 } 
	{ imag_i_1_address1 sc_out sc_lv 7 signal 9 } 
	{ imag_i_1_ce1 sc_out sc_logic 1 signal 9 } 
	{ imag_i_1_q1 sc_in sc_lv 32 signal 9 } 
	{ imag_i_2_address0 sc_out sc_lv 7 signal 10 } 
	{ imag_i_2_ce0 sc_out sc_logic 1 signal 10 } 
	{ imag_i_2_q0 sc_in sc_lv 32 signal 10 } 
	{ imag_i_2_address1 sc_out sc_lv 7 signal 10 } 
	{ imag_i_2_ce1 sc_out sc_logic 1 signal 10 } 
	{ imag_i_2_q1 sc_in sc_lv 32 signal 10 } 
	{ imag_i_3_address0 sc_out sc_lv 7 signal 11 } 
	{ imag_i_3_ce0 sc_out sc_logic 1 signal 11 } 
	{ imag_i_3_q0 sc_in sc_lv 32 signal 11 } 
	{ imag_i_3_address1 sc_out sc_lv 7 signal 11 } 
	{ imag_i_3_ce1 sc_out sc_logic 1 signal 11 } 
	{ imag_i_3_q1 sc_in sc_lv 32 signal 11 } 
	{ imag_i_4_address0 sc_out sc_lv 7 signal 12 } 
	{ imag_i_4_ce0 sc_out sc_logic 1 signal 12 } 
	{ imag_i_4_q0 sc_in sc_lv 32 signal 12 } 
	{ imag_i_4_address1 sc_out sc_lv 7 signal 12 } 
	{ imag_i_4_ce1 sc_out sc_logic 1 signal 12 } 
	{ imag_i_4_q1 sc_in sc_lv 32 signal 12 } 
	{ imag_i_5_address0 sc_out sc_lv 7 signal 13 } 
	{ imag_i_5_ce0 sc_out sc_logic 1 signal 13 } 
	{ imag_i_5_q0 sc_in sc_lv 32 signal 13 } 
	{ imag_i_5_address1 sc_out sc_lv 7 signal 13 } 
	{ imag_i_5_ce1 sc_out sc_logic 1 signal 13 } 
	{ imag_i_5_q1 sc_in sc_lv 32 signal 13 } 
	{ imag_i_6_address0 sc_out sc_lv 7 signal 14 } 
	{ imag_i_6_ce0 sc_out sc_logic 1 signal 14 } 
	{ imag_i_6_q0 sc_in sc_lv 32 signal 14 } 
	{ imag_i_6_address1 sc_out sc_lv 7 signal 14 } 
	{ imag_i_6_ce1 sc_out sc_logic 1 signal 14 } 
	{ imag_i_6_q1 sc_in sc_lv 32 signal 14 } 
	{ imag_i_7_address0 sc_out sc_lv 7 signal 15 } 
	{ imag_i_7_ce0 sc_out sc_logic 1 signal 15 } 
	{ imag_i_7_q0 sc_in sc_lv 32 signal 15 } 
	{ imag_i_7_address1 sc_out sc_lv 7 signal 15 } 
	{ imag_i_7_ce1 sc_out sc_logic 1 signal 15 } 
	{ imag_i_7_q1 sc_in sc_lv 32 signal 15 } 
	{ real_o_address0 sc_out sc_lv 10 signal 16 } 
	{ real_o_ce0 sc_out sc_logic 1 signal 16 } 
	{ real_o_we0 sc_out sc_logic 1 signal 16 } 
	{ real_o_d0 sc_out sc_lv 32 signal 16 } 
	{ imag_o_address0 sc_out sc_lv 10 signal 17 } 
	{ imag_o_ce0 sc_out sc_logic 1 signal 17 } 
	{ imag_o_we0 sc_out sc_logic 1 signal 17 } 
	{ imag_o_d0 sc_out sc_lv 32 signal 17 } 
	{ imag_arr1024_1_address0 sc_out sc_lv 8 signal 18 } 
	{ imag_arr1024_1_ce0 sc_out sc_logic 1 signal 18 } 
	{ imag_arr1024_1_we0 sc_out sc_logic 1 signal 18 } 
	{ imag_arr1024_1_d0 sc_out sc_lv 32 signal 18 } 
	{ imag_arr1024_1_q0 sc_in sc_lv 32 signal 18 } 
	{ imag_arr1024_1_address1 sc_out sc_lv 8 signal 18 } 
	{ imag_arr1024_1_ce1 sc_out sc_logic 1 signal 18 } 
	{ imag_arr1024_1_q1 sc_in sc_lv 32 signal 18 } 
	{ real_arr1024_1_address0 sc_out sc_lv 8 signal 19 } 
	{ real_arr1024_1_ce0 sc_out sc_logic 1 signal 19 } 
	{ real_arr1024_1_we0 sc_out sc_logic 1 signal 19 } 
	{ real_arr1024_1_d0 sc_out sc_lv 32 signal 19 } 
	{ real_arr1024_1_q0 sc_in sc_lv 32 signal 19 } 
	{ real_arr1024_1_address1 sc_out sc_lv 8 signal 19 } 
	{ real_arr1024_1_ce1 sc_out sc_logic 1 signal 19 } 
	{ real_arr1024_1_q1 sc_in sc_lv 32 signal 19 } 
	{ imag_arr1024_0_address0 sc_out sc_lv 8 signal 20 } 
	{ imag_arr1024_0_ce0 sc_out sc_logic 1 signal 20 } 
	{ imag_arr1024_0_we0 sc_out sc_logic 1 signal 20 } 
	{ imag_arr1024_0_d0 sc_out sc_lv 32 signal 20 } 
	{ imag_arr1024_0_q0 sc_in sc_lv 32 signal 20 } 
	{ imag_arr1024_0_address1 sc_out sc_lv 8 signal 20 } 
	{ imag_arr1024_0_ce1 sc_out sc_logic 1 signal 20 } 
	{ imag_arr1024_0_q1 sc_in sc_lv 32 signal 20 } 
	{ real_arr1024_0_address0 sc_out sc_lv 8 signal 21 } 
	{ real_arr1024_0_ce0 sc_out sc_logic 1 signal 21 } 
	{ real_arr1024_0_we0 sc_out sc_logic 1 signal 21 } 
	{ real_arr1024_0_d0 sc_out sc_lv 32 signal 21 } 
	{ real_arr1024_0_q0 sc_in sc_lv 32 signal 21 } 
	{ real_arr1024_0_address1 sc_out sc_lv 8 signal 21 } 
	{ real_arr1024_0_ce1 sc_out sc_logic 1 signal 21 } 
	{ real_arr1024_0_q1 sc_in sc_lv 32 signal 21 } 
	{ imag_arr1024_3_address0 sc_out sc_lv 8 signal 22 } 
	{ imag_arr1024_3_ce0 sc_out sc_logic 1 signal 22 } 
	{ imag_arr1024_3_we0 sc_out sc_logic 1 signal 22 } 
	{ imag_arr1024_3_d0 sc_out sc_lv 32 signal 22 } 
	{ imag_arr1024_3_q0 sc_in sc_lv 32 signal 22 } 
	{ imag_arr1024_3_address1 sc_out sc_lv 8 signal 22 } 
	{ imag_arr1024_3_ce1 sc_out sc_logic 1 signal 22 } 
	{ imag_arr1024_3_q1 sc_in sc_lv 32 signal 22 } 
	{ real_arr1024_3_address0 sc_out sc_lv 8 signal 23 } 
	{ real_arr1024_3_ce0 sc_out sc_logic 1 signal 23 } 
	{ real_arr1024_3_we0 sc_out sc_logic 1 signal 23 } 
	{ real_arr1024_3_d0 sc_out sc_lv 32 signal 23 } 
	{ real_arr1024_3_q0 sc_in sc_lv 32 signal 23 } 
	{ real_arr1024_3_address1 sc_out sc_lv 8 signal 23 } 
	{ real_arr1024_3_ce1 sc_out sc_logic 1 signal 23 } 
	{ real_arr1024_3_q1 sc_in sc_lv 32 signal 23 } 
	{ imag_arr1024_2_address0 sc_out sc_lv 8 signal 24 } 
	{ imag_arr1024_2_ce0 sc_out sc_logic 1 signal 24 } 
	{ imag_arr1024_2_we0 sc_out sc_logic 1 signal 24 } 
	{ imag_arr1024_2_d0 sc_out sc_lv 32 signal 24 } 
	{ imag_arr1024_2_q0 sc_in sc_lv 32 signal 24 } 
	{ imag_arr1024_2_address1 sc_out sc_lv 8 signal 24 } 
	{ imag_arr1024_2_ce1 sc_out sc_logic 1 signal 24 } 
	{ imag_arr1024_2_q1 sc_in sc_lv 32 signal 24 } 
	{ real_arr1024_2_address0 sc_out sc_lv 8 signal 25 } 
	{ real_arr1024_2_ce0 sc_out sc_logic 1 signal 25 } 
	{ real_arr1024_2_we0 sc_out sc_logic 1 signal 25 } 
	{ real_arr1024_2_d0 sc_out sc_lv 32 signal 25 } 
	{ real_arr1024_2_q0 sc_in sc_lv 32 signal 25 } 
	{ real_arr1024_2_address1 sc_out sc_lv 8 signal 25 } 
	{ real_arr1024_2_ce1 sc_out sc_logic 1 signal 25 } 
	{ real_arr1024_2_q1 sc_in sc_lv 32 signal 25 } 
	{ real_arr512_0_address0 sc_out sc_lv 7 signal 26 } 
	{ real_arr512_0_ce0 sc_out sc_logic 1 signal 26 } 
	{ real_arr512_0_we0 sc_out sc_logic 1 signal 26 } 
	{ real_arr512_0_d0 sc_out sc_lv 32 signal 26 } 
	{ real_arr512_0_q0 sc_in sc_lv 32 signal 26 } 
	{ real_arr512_0_address1 sc_out sc_lv 7 signal 26 } 
	{ real_arr512_0_ce1 sc_out sc_logic 1 signal 26 } 
	{ real_arr512_0_q1 sc_in sc_lv 32 signal 26 } 
	{ imag_arr512_0_address0 sc_out sc_lv 7 signal 27 } 
	{ imag_arr512_0_ce0 sc_out sc_logic 1 signal 27 } 
	{ imag_arr512_0_we0 sc_out sc_logic 1 signal 27 } 
	{ imag_arr512_0_d0 sc_out sc_lv 32 signal 27 } 
	{ imag_arr512_0_q0 sc_in sc_lv 32 signal 27 } 
	{ imag_arr512_0_address1 sc_out sc_lv 7 signal 27 } 
	{ imag_arr512_0_ce1 sc_out sc_logic 1 signal 27 } 
	{ imag_arr512_0_q1 sc_in sc_lv 32 signal 27 } 
	{ real_arr512_1_address0 sc_out sc_lv 7 signal 28 } 
	{ real_arr512_1_ce0 sc_out sc_logic 1 signal 28 } 
	{ real_arr512_1_we0 sc_out sc_logic 1 signal 28 } 
	{ real_arr512_1_d0 sc_out sc_lv 32 signal 28 } 
	{ real_arr512_1_q0 sc_in sc_lv 32 signal 28 } 
	{ real_arr512_1_address1 sc_out sc_lv 7 signal 28 } 
	{ real_arr512_1_ce1 sc_out sc_logic 1 signal 28 } 
	{ real_arr512_1_q1 sc_in sc_lv 32 signal 28 } 
	{ imag_arr512_1_address0 sc_out sc_lv 7 signal 29 } 
	{ imag_arr512_1_ce0 sc_out sc_logic 1 signal 29 } 
	{ imag_arr512_1_we0 sc_out sc_logic 1 signal 29 } 
	{ imag_arr512_1_d0 sc_out sc_lv 32 signal 29 } 
	{ imag_arr512_1_q0 sc_in sc_lv 32 signal 29 } 
	{ imag_arr512_1_address1 sc_out sc_lv 7 signal 29 } 
	{ imag_arr512_1_ce1 sc_out sc_logic 1 signal 29 } 
	{ imag_arr512_1_q1 sc_in sc_lv 32 signal 29 } 
	{ real_arr512_2_address0 sc_out sc_lv 7 signal 30 } 
	{ real_arr512_2_ce0 sc_out sc_logic 1 signal 30 } 
	{ real_arr512_2_we0 sc_out sc_logic 1 signal 30 } 
	{ real_arr512_2_d0 sc_out sc_lv 32 signal 30 } 
	{ real_arr512_2_q0 sc_in sc_lv 32 signal 30 } 
	{ real_arr512_2_address1 sc_out sc_lv 7 signal 30 } 
	{ real_arr512_2_ce1 sc_out sc_logic 1 signal 30 } 
	{ real_arr512_2_q1 sc_in sc_lv 32 signal 30 } 
	{ imag_arr512_2_address0 sc_out sc_lv 7 signal 31 } 
	{ imag_arr512_2_ce0 sc_out sc_logic 1 signal 31 } 
	{ imag_arr512_2_we0 sc_out sc_logic 1 signal 31 } 
	{ imag_arr512_2_d0 sc_out sc_lv 32 signal 31 } 
	{ imag_arr512_2_q0 sc_in sc_lv 32 signal 31 } 
	{ imag_arr512_2_address1 sc_out sc_lv 7 signal 31 } 
	{ imag_arr512_2_ce1 sc_out sc_logic 1 signal 31 } 
	{ imag_arr512_2_q1 sc_in sc_lv 32 signal 31 } 
	{ real_arr512_3_address0 sc_out sc_lv 7 signal 32 } 
	{ real_arr512_3_ce0 sc_out sc_logic 1 signal 32 } 
	{ real_arr512_3_we0 sc_out sc_logic 1 signal 32 } 
	{ real_arr512_3_d0 sc_out sc_lv 32 signal 32 } 
	{ real_arr512_3_q0 sc_in sc_lv 32 signal 32 } 
	{ real_arr512_3_address1 sc_out sc_lv 7 signal 32 } 
	{ real_arr512_3_ce1 sc_out sc_logic 1 signal 32 } 
	{ real_arr512_3_q1 sc_in sc_lv 32 signal 32 } 
	{ imag_arr512_3_address0 sc_out sc_lv 7 signal 33 } 
	{ imag_arr512_3_ce0 sc_out sc_logic 1 signal 33 } 
	{ imag_arr512_3_we0 sc_out sc_logic 1 signal 33 } 
	{ imag_arr512_3_d0 sc_out sc_lv 32 signal 33 } 
	{ imag_arr512_3_q0 sc_in sc_lv 32 signal 33 } 
	{ imag_arr512_3_address1 sc_out sc_lv 7 signal 33 } 
	{ imag_arr512_3_ce1 sc_out sc_logic 1 signal 33 } 
	{ imag_arr512_3_q1 sc_in sc_lv 32 signal 33 } 
	{ real_arr256_0_address0 sc_out sc_lv 6 signal 34 } 
	{ real_arr256_0_ce0 sc_out sc_logic 1 signal 34 } 
	{ real_arr256_0_we0 sc_out sc_logic 1 signal 34 } 
	{ real_arr256_0_d0 sc_out sc_lv 32 signal 34 } 
	{ real_arr256_0_q0 sc_in sc_lv 32 signal 34 } 
	{ real_arr256_0_address1 sc_out sc_lv 6 signal 34 } 
	{ real_arr256_0_ce1 sc_out sc_logic 1 signal 34 } 
	{ real_arr256_0_q1 sc_in sc_lv 32 signal 34 } 
	{ imag_arr256_0_address0 sc_out sc_lv 6 signal 35 } 
	{ imag_arr256_0_ce0 sc_out sc_logic 1 signal 35 } 
	{ imag_arr256_0_we0 sc_out sc_logic 1 signal 35 } 
	{ imag_arr256_0_d0 sc_out sc_lv 32 signal 35 } 
	{ imag_arr256_0_q0 sc_in sc_lv 32 signal 35 } 
	{ imag_arr256_0_address1 sc_out sc_lv 6 signal 35 } 
	{ imag_arr256_0_ce1 sc_out sc_logic 1 signal 35 } 
	{ imag_arr256_0_q1 sc_in sc_lv 32 signal 35 } 
	{ real_arr256_1_address0 sc_out sc_lv 6 signal 36 } 
	{ real_arr256_1_ce0 sc_out sc_logic 1 signal 36 } 
	{ real_arr256_1_we0 sc_out sc_logic 1 signal 36 } 
	{ real_arr256_1_d0 sc_out sc_lv 32 signal 36 } 
	{ real_arr256_1_q0 sc_in sc_lv 32 signal 36 } 
	{ real_arr256_1_address1 sc_out sc_lv 6 signal 36 } 
	{ real_arr256_1_ce1 sc_out sc_logic 1 signal 36 } 
	{ real_arr256_1_q1 sc_in sc_lv 32 signal 36 } 
	{ imag_arr256_1_address0 sc_out sc_lv 6 signal 37 } 
	{ imag_arr256_1_ce0 sc_out sc_logic 1 signal 37 } 
	{ imag_arr256_1_we0 sc_out sc_logic 1 signal 37 } 
	{ imag_arr256_1_d0 sc_out sc_lv 32 signal 37 } 
	{ imag_arr256_1_q0 sc_in sc_lv 32 signal 37 } 
	{ imag_arr256_1_address1 sc_out sc_lv 6 signal 37 } 
	{ imag_arr256_1_ce1 sc_out sc_logic 1 signal 37 } 
	{ imag_arr256_1_q1 sc_in sc_lv 32 signal 37 } 
	{ real_arr256_2_address0 sc_out sc_lv 6 signal 38 } 
	{ real_arr256_2_ce0 sc_out sc_logic 1 signal 38 } 
	{ real_arr256_2_we0 sc_out sc_logic 1 signal 38 } 
	{ real_arr256_2_d0 sc_out sc_lv 32 signal 38 } 
	{ real_arr256_2_q0 sc_in sc_lv 32 signal 38 } 
	{ real_arr256_2_address1 sc_out sc_lv 6 signal 38 } 
	{ real_arr256_2_ce1 sc_out sc_logic 1 signal 38 } 
	{ real_arr256_2_q1 sc_in sc_lv 32 signal 38 } 
	{ imag_arr256_2_address0 sc_out sc_lv 6 signal 39 } 
	{ imag_arr256_2_ce0 sc_out sc_logic 1 signal 39 } 
	{ imag_arr256_2_we0 sc_out sc_logic 1 signal 39 } 
	{ imag_arr256_2_d0 sc_out sc_lv 32 signal 39 } 
	{ imag_arr256_2_q0 sc_in sc_lv 32 signal 39 } 
	{ imag_arr256_2_address1 sc_out sc_lv 6 signal 39 } 
	{ imag_arr256_2_ce1 sc_out sc_logic 1 signal 39 } 
	{ imag_arr256_2_q1 sc_in sc_lv 32 signal 39 } 
	{ real_arr256_3_address0 sc_out sc_lv 6 signal 40 } 
	{ real_arr256_3_ce0 sc_out sc_logic 1 signal 40 } 
	{ real_arr256_3_we0 sc_out sc_logic 1 signal 40 } 
	{ real_arr256_3_d0 sc_out sc_lv 32 signal 40 } 
	{ real_arr256_3_q0 sc_in sc_lv 32 signal 40 } 
	{ real_arr256_3_address1 sc_out sc_lv 6 signal 40 } 
	{ real_arr256_3_ce1 sc_out sc_logic 1 signal 40 } 
	{ real_arr256_3_q1 sc_in sc_lv 32 signal 40 } 
	{ imag_arr256_3_address0 sc_out sc_lv 6 signal 41 } 
	{ imag_arr256_3_ce0 sc_out sc_logic 1 signal 41 } 
	{ imag_arr256_3_we0 sc_out sc_logic 1 signal 41 } 
	{ imag_arr256_3_d0 sc_out sc_lv 32 signal 41 } 
	{ imag_arr256_3_q0 sc_in sc_lv 32 signal 41 } 
	{ imag_arr256_3_address1 sc_out sc_lv 6 signal 41 } 
	{ imag_arr256_3_ce1 sc_out sc_logic 1 signal 41 } 
	{ imag_arr256_3_q1 sc_in sc_lv 32 signal 41 } 
	{ real_arr128_0_address0 sc_out sc_lv 5 signal 42 } 
	{ real_arr128_0_ce0 sc_out sc_logic 1 signal 42 } 
	{ real_arr128_0_we0 sc_out sc_logic 1 signal 42 } 
	{ real_arr128_0_d0 sc_out sc_lv 32 signal 42 } 
	{ real_arr128_0_q0 sc_in sc_lv 32 signal 42 } 
	{ real_arr128_0_address1 sc_out sc_lv 5 signal 42 } 
	{ real_arr128_0_ce1 sc_out sc_logic 1 signal 42 } 
	{ real_arr128_0_q1 sc_in sc_lv 32 signal 42 } 
	{ imag_arr128_0_address0 sc_out sc_lv 5 signal 43 } 
	{ imag_arr128_0_ce0 sc_out sc_logic 1 signal 43 } 
	{ imag_arr128_0_we0 sc_out sc_logic 1 signal 43 } 
	{ imag_arr128_0_d0 sc_out sc_lv 32 signal 43 } 
	{ imag_arr128_0_q0 sc_in sc_lv 32 signal 43 } 
	{ imag_arr128_0_address1 sc_out sc_lv 5 signal 43 } 
	{ imag_arr128_0_ce1 sc_out sc_logic 1 signal 43 } 
	{ imag_arr128_0_q1 sc_in sc_lv 32 signal 43 } 
	{ real_arr128_1_address0 sc_out sc_lv 5 signal 44 } 
	{ real_arr128_1_ce0 sc_out sc_logic 1 signal 44 } 
	{ real_arr128_1_we0 sc_out sc_logic 1 signal 44 } 
	{ real_arr128_1_d0 sc_out sc_lv 32 signal 44 } 
	{ real_arr128_1_q0 sc_in sc_lv 32 signal 44 } 
	{ real_arr128_1_address1 sc_out sc_lv 5 signal 44 } 
	{ real_arr128_1_ce1 sc_out sc_logic 1 signal 44 } 
	{ real_arr128_1_q1 sc_in sc_lv 32 signal 44 } 
	{ imag_arr128_1_address0 sc_out sc_lv 5 signal 45 } 
	{ imag_arr128_1_ce0 sc_out sc_logic 1 signal 45 } 
	{ imag_arr128_1_we0 sc_out sc_logic 1 signal 45 } 
	{ imag_arr128_1_d0 sc_out sc_lv 32 signal 45 } 
	{ imag_arr128_1_q0 sc_in sc_lv 32 signal 45 } 
	{ imag_arr128_1_address1 sc_out sc_lv 5 signal 45 } 
	{ imag_arr128_1_ce1 sc_out sc_logic 1 signal 45 } 
	{ imag_arr128_1_q1 sc_in sc_lv 32 signal 45 } 
	{ real_arr128_2_address0 sc_out sc_lv 5 signal 46 } 
	{ real_arr128_2_ce0 sc_out sc_logic 1 signal 46 } 
	{ real_arr128_2_we0 sc_out sc_logic 1 signal 46 } 
	{ real_arr128_2_d0 sc_out sc_lv 32 signal 46 } 
	{ real_arr128_2_q0 sc_in sc_lv 32 signal 46 } 
	{ real_arr128_2_address1 sc_out sc_lv 5 signal 46 } 
	{ real_arr128_2_ce1 sc_out sc_logic 1 signal 46 } 
	{ real_arr128_2_q1 sc_in sc_lv 32 signal 46 } 
	{ imag_arr128_2_address0 sc_out sc_lv 5 signal 47 } 
	{ imag_arr128_2_ce0 sc_out sc_logic 1 signal 47 } 
	{ imag_arr128_2_we0 sc_out sc_logic 1 signal 47 } 
	{ imag_arr128_2_d0 sc_out sc_lv 32 signal 47 } 
	{ imag_arr128_2_q0 sc_in sc_lv 32 signal 47 } 
	{ imag_arr128_2_address1 sc_out sc_lv 5 signal 47 } 
	{ imag_arr128_2_ce1 sc_out sc_logic 1 signal 47 } 
	{ imag_arr128_2_q1 sc_in sc_lv 32 signal 47 } 
	{ real_arr128_3_address0 sc_out sc_lv 5 signal 48 } 
	{ real_arr128_3_ce0 sc_out sc_logic 1 signal 48 } 
	{ real_arr128_3_we0 sc_out sc_logic 1 signal 48 } 
	{ real_arr128_3_d0 sc_out sc_lv 32 signal 48 } 
	{ real_arr128_3_q0 sc_in sc_lv 32 signal 48 } 
	{ real_arr128_3_address1 sc_out sc_lv 5 signal 48 } 
	{ real_arr128_3_ce1 sc_out sc_logic 1 signal 48 } 
	{ real_arr128_3_q1 sc_in sc_lv 32 signal 48 } 
	{ imag_arr128_3_address0 sc_out sc_lv 5 signal 49 } 
	{ imag_arr128_3_ce0 sc_out sc_logic 1 signal 49 } 
	{ imag_arr128_3_we0 sc_out sc_logic 1 signal 49 } 
	{ imag_arr128_3_d0 sc_out sc_lv 32 signal 49 } 
	{ imag_arr128_3_q0 sc_in sc_lv 32 signal 49 } 
	{ imag_arr128_3_address1 sc_out sc_lv 5 signal 49 } 
	{ imag_arr128_3_ce1 sc_out sc_logic 1 signal 49 } 
	{ imag_arr128_3_q1 sc_in sc_lv 32 signal 49 } 
	{ real_arr64_0_address0 sc_out sc_lv 4 signal 50 } 
	{ real_arr64_0_ce0 sc_out sc_logic 1 signal 50 } 
	{ real_arr64_0_we0 sc_out sc_logic 1 signal 50 } 
	{ real_arr64_0_d0 sc_out sc_lv 32 signal 50 } 
	{ real_arr64_0_q0 sc_in sc_lv 32 signal 50 } 
	{ real_arr64_0_address1 sc_out sc_lv 4 signal 50 } 
	{ real_arr64_0_ce1 sc_out sc_logic 1 signal 50 } 
	{ real_arr64_0_q1 sc_in sc_lv 32 signal 50 } 
	{ imag_arr64_0_address0 sc_out sc_lv 4 signal 51 } 
	{ imag_arr64_0_ce0 sc_out sc_logic 1 signal 51 } 
	{ imag_arr64_0_we0 sc_out sc_logic 1 signal 51 } 
	{ imag_arr64_0_d0 sc_out sc_lv 32 signal 51 } 
	{ imag_arr64_0_q0 sc_in sc_lv 32 signal 51 } 
	{ imag_arr64_0_address1 sc_out sc_lv 4 signal 51 } 
	{ imag_arr64_0_ce1 sc_out sc_logic 1 signal 51 } 
	{ imag_arr64_0_q1 sc_in sc_lv 32 signal 51 } 
	{ real_arr64_1_address0 sc_out sc_lv 4 signal 52 } 
	{ real_arr64_1_ce0 sc_out sc_logic 1 signal 52 } 
	{ real_arr64_1_we0 sc_out sc_logic 1 signal 52 } 
	{ real_arr64_1_d0 sc_out sc_lv 32 signal 52 } 
	{ real_arr64_1_q0 sc_in sc_lv 32 signal 52 } 
	{ real_arr64_1_address1 sc_out sc_lv 4 signal 52 } 
	{ real_arr64_1_ce1 sc_out sc_logic 1 signal 52 } 
	{ real_arr64_1_q1 sc_in sc_lv 32 signal 52 } 
	{ imag_arr64_1_address0 sc_out sc_lv 4 signal 53 } 
	{ imag_arr64_1_ce0 sc_out sc_logic 1 signal 53 } 
	{ imag_arr64_1_we0 sc_out sc_logic 1 signal 53 } 
	{ imag_arr64_1_d0 sc_out sc_lv 32 signal 53 } 
	{ imag_arr64_1_q0 sc_in sc_lv 32 signal 53 } 
	{ imag_arr64_1_address1 sc_out sc_lv 4 signal 53 } 
	{ imag_arr64_1_ce1 sc_out sc_logic 1 signal 53 } 
	{ imag_arr64_1_q1 sc_in sc_lv 32 signal 53 } 
	{ real_arr64_2_address0 sc_out sc_lv 4 signal 54 } 
	{ real_arr64_2_ce0 sc_out sc_logic 1 signal 54 } 
	{ real_arr64_2_we0 sc_out sc_logic 1 signal 54 } 
	{ real_arr64_2_d0 sc_out sc_lv 32 signal 54 } 
	{ real_arr64_2_q0 sc_in sc_lv 32 signal 54 } 
	{ real_arr64_2_address1 sc_out sc_lv 4 signal 54 } 
	{ real_arr64_2_ce1 sc_out sc_logic 1 signal 54 } 
	{ real_arr64_2_q1 sc_in sc_lv 32 signal 54 } 
	{ imag_arr64_2_address0 sc_out sc_lv 4 signal 55 } 
	{ imag_arr64_2_ce0 sc_out sc_logic 1 signal 55 } 
	{ imag_arr64_2_we0 sc_out sc_logic 1 signal 55 } 
	{ imag_arr64_2_d0 sc_out sc_lv 32 signal 55 } 
	{ imag_arr64_2_q0 sc_in sc_lv 32 signal 55 } 
	{ imag_arr64_2_address1 sc_out sc_lv 4 signal 55 } 
	{ imag_arr64_2_ce1 sc_out sc_logic 1 signal 55 } 
	{ imag_arr64_2_q1 sc_in sc_lv 32 signal 55 } 
	{ real_arr64_3_address0 sc_out sc_lv 4 signal 56 } 
	{ real_arr64_3_ce0 sc_out sc_logic 1 signal 56 } 
	{ real_arr64_3_we0 sc_out sc_logic 1 signal 56 } 
	{ real_arr64_3_d0 sc_out sc_lv 32 signal 56 } 
	{ real_arr64_3_q0 sc_in sc_lv 32 signal 56 } 
	{ real_arr64_3_address1 sc_out sc_lv 4 signal 56 } 
	{ real_arr64_3_ce1 sc_out sc_logic 1 signal 56 } 
	{ real_arr64_3_q1 sc_in sc_lv 32 signal 56 } 
	{ imag_arr64_3_address0 sc_out sc_lv 4 signal 57 } 
	{ imag_arr64_3_ce0 sc_out sc_logic 1 signal 57 } 
	{ imag_arr64_3_we0 sc_out sc_logic 1 signal 57 } 
	{ imag_arr64_3_d0 sc_out sc_lv 32 signal 57 } 
	{ imag_arr64_3_q0 sc_in sc_lv 32 signal 57 } 
	{ imag_arr64_3_address1 sc_out sc_lv 4 signal 57 } 
	{ imag_arr64_3_ce1 sc_out sc_logic 1 signal 57 } 
	{ imag_arr64_3_q1 sc_in sc_lv 32 signal 57 } 
	{ real_arr32_0_address0 sc_out sc_lv 3 signal 58 } 
	{ real_arr32_0_ce0 sc_out sc_logic 1 signal 58 } 
	{ real_arr32_0_we0 sc_out sc_logic 1 signal 58 } 
	{ real_arr32_0_d0 sc_out sc_lv 32 signal 58 } 
	{ real_arr32_0_q0 sc_in sc_lv 32 signal 58 } 
	{ real_arr32_0_address1 sc_out sc_lv 3 signal 58 } 
	{ real_arr32_0_ce1 sc_out sc_logic 1 signal 58 } 
	{ real_arr32_0_q1 sc_in sc_lv 32 signal 58 } 
	{ imag_arr32_0_address0 sc_out sc_lv 3 signal 59 } 
	{ imag_arr32_0_ce0 sc_out sc_logic 1 signal 59 } 
	{ imag_arr32_0_we0 sc_out sc_logic 1 signal 59 } 
	{ imag_arr32_0_d0 sc_out sc_lv 32 signal 59 } 
	{ imag_arr32_0_q0 sc_in sc_lv 32 signal 59 } 
	{ imag_arr32_0_address1 sc_out sc_lv 3 signal 59 } 
	{ imag_arr32_0_ce1 sc_out sc_logic 1 signal 59 } 
	{ imag_arr32_0_q1 sc_in sc_lv 32 signal 59 } 
	{ real_arr32_1_address0 sc_out sc_lv 3 signal 60 } 
	{ real_arr32_1_ce0 sc_out sc_logic 1 signal 60 } 
	{ real_arr32_1_we0 sc_out sc_logic 1 signal 60 } 
	{ real_arr32_1_d0 sc_out sc_lv 32 signal 60 } 
	{ real_arr32_1_q0 sc_in sc_lv 32 signal 60 } 
	{ real_arr32_1_address1 sc_out sc_lv 3 signal 60 } 
	{ real_arr32_1_ce1 sc_out sc_logic 1 signal 60 } 
	{ real_arr32_1_q1 sc_in sc_lv 32 signal 60 } 
	{ imag_arr32_1_address0 sc_out sc_lv 3 signal 61 } 
	{ imag_arr32_1_ce0 sc_out sc_logic 1 signal 61 } 
	{ imag_arr32_1_we0 sc_out sc_logic 1 signal 61 } 
	{ imag_arr32_1_d0 sc_out sc_lv 32 signal 61 } 
	{ imag_arr32_1_q0 sc_in sc_lv 32 signal 61 } 
	{ imag_arr32_1_address1 sc_out sc_lv 3 signal 61 } 
	{ imag_arr32_1_ce1 sc_out sc_logic 1 signal 61 } 
	{ imag_arr32_1_q1 sc_in sc_lv 32 signal 61 } 
	{ real_arr32_2_address0 sc_out sc_lv 3 signal 62 } 
	{ real_arr32_2_ce0 sc_out sc_logic 1 signal 62 } 
	{ real_arr32_2_we0 sc_out sc_logic 1 signal 62 } 
	{ real_arr32_2_d0 sc_out sc_lv 32 signal 62 } 
	{ real_arr32_2_q0 sc_in sc_lv 32 signal 62 } 
	{ real_arr32_2_address1 sc_out sc_lv 3 signal 62 } 
	{ real_arr32_2_ce1 sc_out sc_logic 1 signal 62 } 
	{ real_arr32_2_q1 sc_in sc_lv 32 signal 62 } 
	{ imag_arr32_2_address0 sc_out sc_lv 3 signal 63 } 
	{ imag_arr32_2_ce0 sc_out sc_logic 1 signal 63 } 
	{ imag_arr32_2_we0 sc_out sc_logic 1 signal 63 } 
	{ imag_arr32_2_d0 sc_out sc_lv 32 signal 63 } 
	{ imag_arr32_2_q0 sc_in sc_lv 32 signal 63 } 
	{ imag_arr32_2_address1 sc_out sc_lv 3 signal 63 } 
	{ imag_arr32_2_ce1 sc_out sc_logic 1 signal 63 } 
	{ imag_arr32_2_q1 sc_in sc_lv 32 signal 63 } 
	{ real_arr32_3_address0 sc_out sc_lv 3 signal 64 } 
	{ real_arr32_3_ce0 sc_out sc_logic 1 signal 64 } 
	{ real_arr32_3_we0 sc_out sc_logic 1 signal 64 } 
	{ real_arr32_3_d0 sc_out sc_lv 32 signal 64 } 
	{ real_arr32_3_q0 sc_in sc_lv 32 signal 64 } 
	{ real_arr32_3_address1 sc_out sc_lv 3 signal 64 } 
	{ real_arr32_3_ce1 sc_out sc_logic 1 signal 64 } 
	{ real_arr32_3_q1 sc_in sc_lv 32 signal 64 } 
	{ imag_arr32_3_address0 sc_out sc_lv 3 signal 65 } 
	{ imag_arr32_3_ce0 sc_out sc_logic 1 signal 65 } 
	{ imag_arr32_3_we0 sc_out sc_logic 1 signal 65 } 
	{ imag_arr32_3_d0 sc_out sc_lv 32 signal 65 } 
	{ imag_arr32_3_q0 sc_in sc_lv 32 signal 65 } 
	{ imag_arr32_3_address1 sc_out sc_lv 3 signal 65 } 
	{ imag_arr32_3_ce1 sc_out sc_logic 1 signal 65 } 
	{ imag_arr32_3_q1 sc_in sc_lv 32 signal 65 } 
	{ real_arr16_0_address0 sc_out sc_lv 2 signal 66 } 
	{ real_arr16_0_ce0 sc_out sc_logic 1 signal 66 } 
	{ real_arr16_0_we0 sc_out sc_logic 1 signal 66 } 
	{ real_arr16_0_d0 sc_out sc_lv 32 signal 66 } 
	{ real_arr16_0_q0 sc_in sc_lv 32 signal 66 } 
	{ imag_arr16_0_address0 sc_out sc_lv 2 signal 67 } 
	{ imag_arr16_0_ce0 sc_out sc_logic 1 signal 67 } 
	{ imag_arr16_0_we0 sc_out sc_logic 1 signal 67 } 
	{ imag_arr16_0_d0 sc_out sc_lv 32 signal 67 } 
	{ imag_arr16_0_q0 sc_in sc_lv 32 signal 67 } 
	{ real_arr16_1_address0 sc_out sc_lv 2 signal 68 } 
	{ real_arr16_1_ce0 sc_out sc_logic 1 signal 68 } 
	{ real_arr16_1_we0 sc_out sc_logic 1 signal 68 } 
	{ real_arr16_1_d0 sc_out sc_lv 32 signal 68 } 
	{ real_arr16_1_q0 sc_in sc_lv 32 signal 68 } 
	{ imag_arr16_1_address0 sc_out sc_lv 2 signal 69 } 
	{ imag_arr16_1_ce0 sc_out sc_logic 1 signal 69 } 
	{ imag_arr16_1_we0 sc_out sc_logic 1 signal 69 } 
	{ imag_arr16_1_d0 sc_out sc_lv 32 signal 69 } 
	{ imag_arr16_1_q0 sc_in sc_lv 32 signal 69 } 
	{ real_arr16_2_address0 sc_out sc_lv 2 signal 70 } 
	{ real_arr16_2_ce0 sc_out sc_logic 1 signal 70 } 
	{ real_arr16_2_we0 sc_out sc_logic 1 signal 70 } 
	{ real_arr16_2_d0 sc_out sc_lv 32 signal 70 } 
	{ real_arr16_2_q0 sc_in sc_lv 32 signal 70 } 
	{ imag_arr16_2_address0 sc_out sc_lv 2 signal 71 } 
	{ imag_arr16_2_ce0 sc_out sc_logic 1 signal 71 } 
	{ imag_arr16_2_we0 sc_out sc_logic 1 signal 71 } 
	{ imag_arr16_2_d0 sc_out sc_lv 32 signal 71 } 
	{ imag_arr16_2_q0 sc_in sc_lv 32 signal 71 } 
	{ real_arr16_3_address0 sc_out sc_lv 2 signal 72 } 
	{ real_arr16_3_ce0 sc_out sc_logic 1 signal 72 } 
	{ real_arr16_3_we0 sc_out sc_logic 1 signal 72 } 
	{ real_arr16_3_d0 sc_out sc_lv 32 signal 72 } 
	{ real_arr16_3_q0 sc_in sc_lv 32 signal 72 } 
	{ imag_arr16_3_address0 sc_out sc_lv 2 signal 73 } 
	{ imag_arr16_3_ce0 sc_out sc_logic 1 signal 73 } 
	{ imag_arr16_3_we0 sc_out sc_logic 1 signal 73 } 
	{ imag_arr16_3_d0 sc_out sc_lv 32 signal 73 } 
	{ imag_arr16_3_q0 sc_in sc_lv 32 signal 73 } 
	{ imag_arr8_1_i sc_in sc_lv 32 signal 74 } 
	{ imag_arr8_1_o sc_out sc_lv 32 signal 74 } 
	{ imag_arr8_1_o_ap_vld sc_out sc_logic 1 outvld 74 } 
	{ real_arr8_1_i sc_in sc_lv 32 signal 75 } 
	{ real_arr8_1_o sc_out sc_lv 32 signal 75 } 
	{ real_arr8_1_o_ap_vld sc_out sc_logic 1 outvld 75 } 
	{ imag_arr8_0_i sc_in sc_lv 32 signal 76 } 
	{ imag_arr8_0_o sc_out sc_lv 32 signal 76 } 
	{ imag_arr8_0_o_ap_vld sc_out sc_logic 1 outvld 76 } 
	{ real_arr8_0_i sc_in sc_lv 32 signal 77 } 
	{ real_arr8_0_o sc_out sc_lv 32 signal 77 } 
	{ real_arr8_0_o_ap_vld sc_out sc_logic 1 outvld 77 } 
	{ real_arr8_2_i sc_in sc_lv 32 signal 78 } 
	{ real_arr8_2_o sc_out sc_lv 32 signal 78 } 
	{ real_arr8_2_o_ap_vld sc_out sc_logic 1 outvld 78 } 
	{ real_arr8_4_i sc_in sc_lv 32 signal 79 } 
	{ real_arr8_4_o sc_out sc_lv 32 signal 79 } 
	{ real_arr8_4_o_ap_vld sc_out sc_logic 1 outvld 79 } 
	{ real_arr8_6_i sc_in sc_lv 32 signal 80 } 
	{ real_arr8_6_o sc_out sc_lv 32 signal 80 } 
	{ real_arr8_6_o_ap_vld sc_out sc_logic 1 outvld 80 } 
	{ imag_arr8_2_i sc_in sc_lv 32 signal 81 } 
	{ imag_arr8_2_o sc_out sc_lv 32 signal 81 } 
	{ imag_arr8_2_o_ap_vld sc_out sc_logic 1 outvld 81 } 
	{ imag_arr8_4_i sc_in sc_lv 32 signal 82 } 
	{ imag_arr8_4_o sc_out sc_lv 32 signal 82 } 
	{ imag_arr8_4_o_ap_vld sc_out sc_logic 1 outvld 82 } 
	{ imag_arr8_6_i sc_in sc_lv 32 signal 83 } 
	{ imag_arr8_6_o sc_out sc_lv 32 signal 83 } 
	{ imag_arr8_6_o_ap_vld sc_out sc_logic 1 outvld 83 } 
	{ real_arr8_3_i sc_in sc_lv 32 signal 84 } 
	{ real_arr8_3_o sc_out sc_lv 32 signal 84 } 
	{ real_arr8_3_o_ap_vld sc_out sc_logic 1 outvld 84 } 
	{ real_arr8_5_i sc_in sc_lv 32 signal 85 } 
	{ real_arr8_5_o sc_out sc_lv 32 signal 85 } 
	{ real_arr8_5_o_ap_vld sc_out sc_logic 1 outvld 85 } 
	{ real_arr8_7_i sc_in sc_lv 32 signal 86 } 
	{ real_arr8_7_o sc_out sc_lv 32 signal 86 } 
	{ real_arr8_7_o_ap_vld sc_out sc_logic 1 outvld 86 } 
	{ imag_arr8_3_i sc_in sc_lv 32 signal 87 } 
	{ imag_arr8_3_o sc_out sc_lv 32 signal 87 } 
	{ imag_arr8_3_o_ap_vld sc_out sc_logic 1 outvld 87 } 
	{ imag_arr8_5_i sc_in sc_lv 32 signal 88 } 
	{ imag_arr8_5_o sc_out sc_lv 32 signal 88 } 
	{ imag_arr8_5_o_ap_vld sc_out sc_logic 1 outvld 88 } 
	{ imag_arr8_7_i sc_in sc_lv 32 signal 89 } 
	{ imag_arr8_7_o sc_out sc_lv 32 signal 89 } 
	{ imag_arr8_7_o_ap_vld sc_out sc_logic 1 outvld 89 } 
	{ imag_arr4_0_i sc_in sc_lv 32 signal 90 } 
	{ imag_arr4_0_o sc_out sc_lv 32 signal 90 } 
	{ imag_arr4_0_o_ap_vld sc_out sc_logic 1 outvld 90 } 
	{ real_arr4_0_i sc_in sc_lv 32 signal 91 } 
	{ real_arr4_0_o sc_out sc_lv 32 signal 91 } 
	{ real_arr4_0_o_ap_vld sc_out sc_logic 1 outvld 91 } 
	{ real_arr4_1_i sc_in sc_lv 32 signal 92 } 
	{ real_arr4_1_o sc_out sc_lv 32 signal 92 } 
	{ real_arr4_1_o_ap_vld sc_out sc_logic 1 outvld 92 } 
	{ real_arr4_2_i sc_in sc_lv 32 signal 93 } 
	{ real_arr4_2_o sc_out sc_lv 32 signal 93 } 
	{ real_arr4_2_o_ap_vld sc_out sc_logic 1 outvld 93 } 
	{ real_arr4_3_i sc_in sc_lv 32 signal 94 } 
	{ real_arr4_3_o sc_out sc_lv 32 signal 94 } 
	{ real_arr4_3_o_ap_vld sc_out sc_logic 1 outvld 94 } 
	{ imag_arr4_1_i sc_in sc_lv 32 signal 95 } 
	{ imag_arr4_1_o sc_out sc_lv 32 signal 95 } 
	{ imag_arr4_1_o_ap_vld sc_out sc_logic 1 outvld 95 } 
	{ imag_arr4_2_i sc_in sc_lv 32 signal 96 } 
	{ imag_arr4_2_o sc_out sc_lv 32 signal 96 } 
	{ imag_arr4_2_o_ap_vld sc_out sc_logic 1 outvld 96 } 
	{ imag_arr4_3_i sc_in sc_lv 32 signal 97 } 
	{ imag_arr4_3_o sc_out sc_lv 32 signal 97 } 
	{ imag_arr4_3_o_ap_vld sc_out sc_logic 1 outvld 97 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_continue", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "continue", "bundle":{"name": "ap_continue", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "real_i_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_i_0", "role": "address0" }} , 
 	{ "name": "real_i_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "ce0" }} , 
 	{ "name": "real_i_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "q0" }} , 
 	{ "name": "real_i_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_i_0", "role": "address1" }} , 
 	{ "name": "real_i_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_0", "role": "ce1" }} , 
 	{ "name": "real_i_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_0", "role": "q1" }} , 
 	{ "name": "real_i_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_i_1", "role": "address0" }} , 
 	{ "name": "real_i_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "ce0" }} , 
 	{ "name": "real_i_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "q0" }} , 
 	{ "name": "real_i_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_i_1", "role": "address1" }} , 
 	{ "name": "real_i_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_1", "role": "ce1" }} , 
 	{ "name": "real_i_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_1", "role": "q1" }} , 
 	{ "name": "real_i_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_i_2", "role": "address0" }} , 
 	{ "name": "real_i_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_2", "role": "ce0" }} , 
 	{ "name": "real_i_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_2", "role": "q0" }} , 
 	{ "name": "real_i_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_i_2", "role": "address1" }} , 
 	{ "name": "real_i_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_2", "role": "ce1" }} , 
 	{ "name": "real_i_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_2", "role": "q1" }} , 
 	{ "name": "real_i_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_i_3", "role": "address0" }} , 
 	{ "name": "real_i_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_3", "role": "ce0" }} , 
 	{ "name": "real_i_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_3", "role": "q0" }} , 
 	{ "name": "real_i_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_i_3", "role": "address1" }} , 
 	{ "name": "real_i_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_3", "role": "ce1" }} , 
 	{ "name": "real_i_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_3", "role": "q1" }} , 
 	{ "name": "real_i_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_i_4", "role": "address0" }} , 
 	{ "name": "real_i_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_4", "role": "ce0" }} , 
 	{ "name": "real_i_4_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_4", "role": "q0" }} , 
 	{ "name": "real_i_4_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_i_4", "role": "address1" }} , 
 	{ "name": "real_i_4_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_4", "role": "ce1" }} , 
 	{ "name": "real_i_4_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_4", "role": "q1" }} , 
 	{ "name": "real_i_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_i_5", "role": "address0" }} , 
 	{ "name": "real_i_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_5", "role": "ce0" }} , 
 	{ "name": "real_i_5_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_5", "role": "q0" }} , 
 	{ "name": "real_i_5_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_i_5", "role": "address1" }} , 
 	{ "name": "real_i_5_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_5", "role": "ce1" }} , 
 	{ "name": "real_i_5_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_5", "role": "q1" }} , 
 	{ "name": "real_i_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_i_6", "role": "address0" }} , 
 	{ "name": "real_i_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_6", "role": "ce0" }} , 
 	{ "name": "real_i_6_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_6", "role": "q0" }} , 
 	{ "name": "real_i_6_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_i_6", "role": "address1" }} , 
 	{ "name": "real_i_6_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_6", "role": "ce1" }} , 
 	{ "name": "real_i_6_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_6", "role": "q1" }} , 
 	{ "name": "real_i_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_i_7", "role": "address0" }} , 
 	{ "name": "real_i_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_7", "role": "ce0" }} , 
 	{ "name": "real_i_7_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_7", "role": "q0" }} , 
 	{ "name": "real_i_7_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_i_7", "role": "address1" }} , 
 	{ "name": "real_i_7_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_i_7", "role": "ce1" }} , 
 	{ "name": "real_i_7_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_i_7", "role": "q1" }} , 
 	{ "name": "imag_i_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_i_0", "role": "address0" }} , 
 	{ "name": "imag_i_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_0", "role": "ce0" }} , 
 	{ "name": "imag_i_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_0", "role": "q0" }} , 
 	{ "name": "imag_i_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_i_0", "role": "address1" }} , 
 	{ "name": "imag_i_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_0", "role": "ce1" }} , 
 	{ "name": "imag_i_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_0", "role": "q1" }} , 
 	{ "name": "imag_i_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_i_1", "role": "address0" }} , 
 	{ "name": "imag_i_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_1", "role": "ce0" }} , 
 	{ "name": "imag_i_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_1", "role": "q0" }} , 
 	{ "name": "imag_i_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_i_1", "role": "address1" }} , 
 	{ "name": "imag_i_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_1", "role": "ce1" }} , 
 	{ "name": "imag_i_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_1", "role": "q1" }} , 
 	{ "name": "imag_i_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_i_2", "role": "address0" }} , 
 	{ "name": "imag_i_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_2", "role": "ce0" }} , 
 	{ "name": "imag_i_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_2", "role": "q0" }} , 
 	{ "name": "imag_i_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_i_2", "role": "address1" }} , 
 	{ "name": "imag_i_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_2", "role": "ce1" }} , 
 	{ "name": "imag_i_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_2", "role": "q1" }} , 
 	{ "name": "imag_i_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_i_3", "role": "address0" }} , 
 	{ "name": "imag_i_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_3", "role": "ce0" }} , 
 	{ "name": "imag_i_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_3", "role": "q0" }} , 
 	{ "name": "imag_i_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_i_3", "role": "address1" }} , 
 	{ "name": "imag_i_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_3", "role": "ce1" }} , 
 	{ "name": "imag_i_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_3", "role": "q1" }} , 
 	{ "name": "imag_i_4_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_i_4", "role": "address0" }} , 
 	{ "name": "imag_i_4_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_4", "role": "ce0" }} , 
 	{ "name": "imag_i_4_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_4", "role": "q0" }} , 
 	{ "name": "imag_i_4_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_i_4", "role": "address1" }} , 
 	{ "name": "imag_i_4_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_4", "role": "ce1" }} , 
 	{ "name": "imag_i_4_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_4", "role": "q1" }} , 
 	{ "name": "imag_i_5_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_i_5", "role": "address0" }} , 
 	{ "name": "imag_i_5_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_5", "role": "ce0" }} , 
 	{ "name": "imag_i_5_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_5", "role": "q0" }} , 
 	{ "name": "imag_i_5_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_i_5", "role": "address1" }} , 
 	{ "name": "imag_i_5_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_5", "role": "ce1" }} , 
 	{ "name": "imag_i_5_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_5", "role": "q1" }} , 
 	{ "name": "imag_i_6_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_i_6", "role": "address0" }} , 
 	{ "name": "imag_i_6_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_6", "role": "ce0" }} , 
 	{ "name": "imag_i_6_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_6", "role": "q0" }} , 
 	{ "name": "imag_i_6_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_i_6", "role": "address1" }} , 
 	{ "name": "imag_i_6_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_6", "role": "ce1" }} , 
 	{ "name": "imag_i_6_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_6", "role": "q1" }} , 
 	{ "name": "imag_i_7_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_i_7", "role": "address0" }} , 
 	{ "name": "imag_i_7_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_7", "role": "ce0" }} , 
 	{ "name": "imag_i_7_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_7", "role": "q0" }} , 
 	{ "name": "imag_i_7_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_i_7", "role": "address1" }} , 
 	{ "name": "imag_i_7_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_i_7", "role": "ce1" }} , 
 	{ "name": "imag_i_7_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_i_7", "role": "q1" }} , 
 	{ "name": "real_o_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":10, "type": "signal", "bundle":{"name": "real_o", "role": "address0" }} , 
 	{ "name": "real_o_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o", "role": "ce0" }} , 
 	{ "name": "real_o_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_o", "role": "we0" }} , 
 	{ "name": "real_o_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_o", "role": "d0" }} , 
 	{ "name": "imag_o_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":10, "type": "signal", "bundle":{"name": "imag_o", "role": "address0" }} , 
 	{ "name": "imag_o_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o", "role": "ce0" }} , 
 	{ "name": "imag_o_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_o", "role": "we0" }} , 
 	{ "name": "imag_o_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_o", "role": "d0" }} , 
 	{ "name": "imag_arr1024_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "imag_arr1024_1", "role": "address0" }} , 
 	{ "name": "imag_arr1024_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_1", "role": "ce0" }} , 
 	{ "name": "imag_arr1024_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_1", "role": "we0" }} , 
 	{ "name": "imag_arr1024_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr1024_1", "role": "d0" }} , 
 	{ "name": "imag_arr1024_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr1024_1", "role": "q0" }} , 
 	{ "name": "imag_arr1024_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "imag_arr1024_1", "role": "address1" }} , 
 	{ "name": "imag_arr1024_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_1", "role": "ce1" }} , 
 	{ "name": "imag_arr1024_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr1024_1", "role": "q1" }} , 
 	{ "name": "real_arr1024_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_arr1024_1", "role": "address0" }} , 
 	{ "name": "real_arr1024_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_1", "role": "ce0" }} , 
 	{ "name": "real_arr1024_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_1", "role": "we0" }} , 
 	{ "name": "real_arr1024_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr1024_1", "role": "d0" }} , 
 	{ "name": "real_arr1024_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr1024_1", "role": "q0" }} , 
 	{ "name": "real_arr1024_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_arr1024_1", "role": "address1" }} , 
 	{ "name": "real_arr1024_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_1", "role": "ce1" }} , 
 	{ "name": "real_arr1024_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr1024_1", "role": "q1" }} , 
 	{ "name": "imag_arr1024_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "imag_arr1024_0", "role": "address0" }} , 
 	{ "name": "imag_arr1024_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_0", "role": "ce0" }} , 
 	{ "name": "imag_arr1024_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_0", "role": "we0" }} , 
 	{ "name": "imag_arr1024_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr1024_0", "role": "d0" }} , 
 	{ "name": "imag_arr1024_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr1024_0", "role": "q0" }} , 
 	{ "name": "imag_arr1024_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "imag_arr1024_0", "role": "address1" }} , 
 	{ "name": "imag_arr1024_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_0", "role": "ce1" }} , 
 	{ "name": "imag_arr1024_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr1024_0", "role": "q1" }} , 
 	{ "name": "real_arr1024_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_arr1024_0", "role": "address0" }} , 
 	{ "name": "real_arr1024_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_0", "role": "ce0" }} , 
 	{ "name": "real_arr1024_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_0", "role": "we0" }} , 
 	{ "name": "real_arr1024_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr1024_0", "role": "d0" }} , 
 	{ "name": "real_arr1024_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr1024_0", "role": "q0" }} , 
 	{ "name": "real_arr1024_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_arr1024_0", "role": "address1" }} , 
 	{ "name": "real_arr1024_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_0", "role": "ce1" }} , 
 	{ "name": "real_arr1024_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr1024_0", "role": "q1" }} , 
 	{ "name": "imag_arr1024_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "imag_arr1024_3", "role": "address0" }} , 
 	{ "name": "imag_arr1024_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_3", "role": "ce0" }} , 
 	{ "name": "imag_arr1024_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_3", "role": "we0" }} , 
 	{ "name": "imag_arr1024_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr1024_3", "role": "d0" }} , 
 	{ "name": "imag_arr1024_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr1024_3", "role": "q0" }} , 
 	{ "name": "imag_arr1024_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "imag_arr1024_3", "role": "address1" }} , 
 	{ "name": "imag_arr1024_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_3", "role": "ce1" }} , 
 	{ "name": "imag_arr1024_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr1024_3", "role": "q1" }} , 
 	{ "name": "real_arr1024_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_arr1024_3", "role": "address0" }} , 
 	{ "name": "real_arr1024_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_3", "role": "ce0" }} , 
 	{ "name": "real_arr1024_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_3", "role": "we0" }} , 
 	{ "name": "real_arr1024_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr1024_3", "role": "d0" }} , 
 	{ "name": "real_arr1024_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr1024_3", "role": "q0" }} , 
 	{ "name": "real_arr1024_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_arr1024_3", "role": "address1" }} , 
 	{ "name": "real_arr1024_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_3", "role": "ce1" }} , 
 	{ "name": "real_arr1024_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr1024_3", "role": "q1" }} , 
 	{ "name": "imag_arr1024_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "imag_arr1024_2", "role": "address0" }} , 
 	{ "name": "imag_arr1024_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_2", "role": "ce0" }} , 
 	{ "name": "imag_arr1024_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_2", "role": "we0" }} , 
 	{ "name": "imag_arr1024_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr1024_2", "role": "d0" }} , 
 	{ "name": "imag_arr1024_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr1024_2", "role": "q0" }} , 
 	{ "name": "imag_arr1024_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "imag_arr1024_2", "role": "address1" }} , 
 	{ "name": "imag_arr1024_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr1024_2", "role": "ce1" }} , 
 	{ "name": "imag_arr1024_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr1024_2", "role": "q1" }} , 
 	{ "name": "real_arr1024_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_arr1024_2", "role": "address0" }} , 
 	{ "name": "real_arr1024_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_2", "role": "ce0" }} , 
 	{ "name": "real_arr1024_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_2", "role": "we0" }} , 
 	{ "name": "real_arr1024_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr1024_2", "role": "d0" }} , 
 	{ "name": "real_arr1024_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr1024_2", "role": "q0" }} , 
 	{ "name": "real_arr1024_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":8, "type": "signal", "bundle":{"name": "real_arr1024_2", "role": "address1" }} , 
 	{ "name": "real_arr1024_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr1024_2", "role": "ce1" }} , 
 	{ "name": "real_arr1024_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr1024_2", "role": "q1" }} , 
 	{ "name": "real_arr512_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_arr512_0", "role": "address0" }} , 
 	{ "name": "real_arr512_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr512_0", "role": "ce0" }} , 
 	{ "name": "real_arr512_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr512_0", "role": "we0" }} , 
 	{ "name": "real_arr512_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr512_0", "role": "d0" }} , 
 	{ "name": "real_arr512_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr512_0", "role": "q0" }} , 
 	{ "name": "real_arr512_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_arr512_0", "role": "address1" }} , 
 	{ "name": "real_arr512_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr512_0", "role": "ce1" }} , 
 	{ "name": "real_arr512_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr512_0", "role": "q1" }} , 
 	{ "name": "imag_arr512_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_arr512_0", "role": "address0" }} , 
 	{ "name": "imag_arr512_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr512_0", "role": "ce0" }} , 
 	{ "name": "imag_arr512_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr512_0", "role": "we0" }} , 
 	{ "name": "imag_arr512_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr512_0", "role": "d0" }} , 
 	{ "name": "imag_arr512_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr512_0", "role": "q0" }} , 
 	{ "name": "imag_arr512_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_arr512_0", "role": "address1" }} , 
 	{ "name": "imag_arr512_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr512_0", "role": "ce1" }} , 
 	{ "name": "imag_arr512_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr512_0", "role": "q1" }} , 
 	{ "name": "real_arr512_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_arr512_1", "role": "address0" }} , 
 	{ "name": "real_arr512_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr512_1", "role": "ce0" }} , 
 	{ "name": "real_arr512_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr512_1", "role": "we0" }} , 
 	{ "name": "real_arr512_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr512_1", "role": "d0" }} , 
 	{ "name": "real_arr512_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr512_1", "role": "q0" }} , 
 	{ "name": "real_arr512_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_arr512_1", "role": "address1" }} , 
 	{ "name": "real_arr512_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr512_1", "role": "ce1" }} , 
 	{ "name": "real_arr512_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr512_1", "role": "q1" }} , 
 	{ "name": "imag_arr512_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_arr512_1", "role": "address0" }} , 
 	{ "name": "imag_arr512_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr512_1", "role": "ce0" }} , 
 	{ "name": "imag_arr512_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr512_1", "role": "we0" }} , 
 	{ "name": "imag_arr512_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr512_1", "role": "d0" }} , 
 	{ "name": "imag_arr512_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr512_1", "role": "q0" }} , 
 	{ "name": "imag_arr512_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_arr512_1", "role": "address1" }} , 
 	{ "name": "imag_arr512_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr512_1", "role": "ce1" }} , 
 	{ "name": "imag_arr512_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr512_1", "role": "q1" }} , 
 	{ "name": "real_arr512_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_arr512_2", "role": "address0" }} , 
 	{ "name": "real_arr512_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr512_2", "role": "ce0" }} , 
 	{ "name": "real_arr512_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr512_2", "role": "we0" }} , 
 	{ "name": "real_arr512_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr512_2", "role": "d0" }} , 
 	{ "name": "real_arr512_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr512_2", "role": "q0" }} , 
 	{ "name": "real_arr512_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_arr512_2", "role": "address1" }} , 
 	{ "name": "real_arr512_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr512_2", "role": "ce1" }} , 
 	{ "name": "real_arr512_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr512_2", "role": "q1" }} , 
 	{ "name": "imag_arr512_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_arr512_2", "role": "address0" }} , 
 	{ "name": "imag_arr512_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr512_2", "role": "ce0" }} , 
 	{ "name": "imag_arr512_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr512_2", "role": "we0" }} , 
 	{ "name": "imag_arr512_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr512_2", "role": "d0" }} , 
 	{ "name": "imag_arr512_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr512_2", "role": "q0" }} , 
 	{ "name": "imag_arr512_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_arr512_2", "role": "address1" }} , 
 	{ "name": "imag_arr512_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr512_2", "role": "ce1" }} , 
 	{ "name": "imag_arr512_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr512_2", "role": "q1" }} , 
 	{ "name": "real_arr512_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_arr512_3", "role": "address0" }} , 
 	{ "name": "real_arr512_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr512_3", "role": "ce0" }} , 
 	{ "name": "real_arr512_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr512_3", "role": "we0" }} , 
 	{ "name": "real_arr512_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr512_3", "role": "d0" }} , 
 	{ "name": "real_arr512_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr512_3", "role": "q0" }} , 
 	{ "name": "real_arr512_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "real_arr512_3", "role": "address1" }} , 
 	{ "name": "real_arr512_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr512_3", "role": "ce1" }} , 
 	{ "name": "real_arr512_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr512_3", "role": "q1" }} , 
 	{ "name": "imag_arr512_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_arr512_3", "role": "address0" }} , 
 	{ "name": "imag_arr512_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr512_3", "role": "ce0" }} , 
 	{ "name": "imag_arr512_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr512_3", "role": "we0" }} , 
 	{ "name": "imag_arr512_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr512_3", "role": "d0" }} , 
 	{ "name": "imag_arr512_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr512_3", "role": "q0" }} , 
 	{ "name": "imag_arr512_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":7, "type": "signal", "bundle":{"name": "imag_arr512_3", "role": "address1" }} , 
 	{ "name": "imag_arr512_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr512_3", "role": "ce1" }} , 
 	{ "name": "imag_arr512_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr512_3", "role": "q1" }} , 
 	{ "name": "real_arr256_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "address0" }} , 
 	{ "name": "real_arr256_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "ce0" }} , 
 	{ "name": "real_arr256_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "we0" }} , 
 	{ "name": "real_arr256_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "d0" }} , 
 	{ "name": "real_arr256_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "q0" }} , 
 	{ "name": "real_arr256_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "address1" }} , 
 	{ "name": "real_arr256_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "ce1" }} , 
 	{ "name": "real_arr256_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "q1" }} , 
 	{ "name": "imag_arr256_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "address0" }} , 
 	{ "name": "imag_arr256_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "ce0" }} , 
 	{ "name": "imag_arr256_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "we0" }} , 
 	{ "name": "imag_arr256_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "d0" }} , 
 	{ "name": "imag_arr256_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "q0" }} , 
 	{ "name": "imag_arr256_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "address1" }} , 
 	{ "name": "imag_arr256_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "ce1" }} , 
 	{ "name": "imag_arr256_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "q1" }} , 
 	{ "name": "real_arr256_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "address0" }} , 
 	{ "name": "real_arr256_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "ce0" }} , 
 	{ "name": "real_arr256_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "we0" }} , 
 	{ "name": "real_arr256_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "d0" }} , 
 	{ "name": "real_arr256_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "q0" }} , 
 	{ "name": "real_arr256_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "address1" }} , 
 	{ "name": "real_arr256_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "ce1" }} , 
 	{ "name": "real_arr256_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "q1" }} , 
 	{ "name": "imag_arr256_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "address0" }} , 
 	{ "name": "imag_arr256_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "ce0" }} , 
 	{ "name": "imag_arr256_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "we0" }} , 
 	{ "name": "imag_arr256_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "d0" }} , 
 	{ "name": "imag_arr256_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "q0" }} , 
 	{ "name": "imag_arr256_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "address1" }} , 
 	{ "name": "imag_arr256_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "ce1" }} , 
 	{ "name": "imag_arr256_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "q1" }} , 
 	{ "name": "real_arr256_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "address0" }} , 
 	{ "name": "real_arr256_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "ce0" }} , 
 	{ "name": "real_arr256_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "we0" }} , 
 	{ "name": "real_arr256_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "d0" }} , 
 	{ "name": "real_arr256_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "q0" }} , 
 	{ "name": "real_arr256_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "address1" }} , 
 	{ "name": "real_arr256_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "ce1" }} , 
 	{ "name": "real_arr256_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "q1" }} , 
 	{ "name": "imag_arr256_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "address0" }} , 
 	{ "name": "imag_arr256_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "ce0" }} , 
 	{ "name": "imag_arr256_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "we0" }} , 
 	{ "name": "imag_arr256_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "d0" }} , 
 	{ "name": "imag_arr256_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "q0" }} , 
 	{ "name": "imag_arr256_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "address1" }} , 
 	{ "name": "imag_arr256_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "ce1" }} , 
 	{ "name": "imag_arr256_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "q1" }} , 
 	{ "name": "real_arr256_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "address0" }} , 
 	{ "name": "real_arr256_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "ce0" }} , 
 	{ "name": "real_arr256_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "we0" }} , 
 	{ "name": "real_arr256_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "d0" }} , 
 	{ "name": "real_arr256_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "q0" }} , 
 	{ "name": "real_arr256_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "address1" }} , 
 	{ "name": "real_arr256_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "ce1" }} , 
 	{ "name": "real_arr256_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "q1" }} , 
 	{ "name": "imag_arr256_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "address0" }} , 
 	{ "name": "imag_arr256_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "ce0" }} , 
 	{ "name": "imag_arr256_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "we0" }} , 
 	{ "name": "imag_arr256_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "d0" }} , 
 	{ "name": "imag_arr256_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "q0" }} , 
 	{ "name": "imag_arr256_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "address1" }} , 
 	{ "name": "imag_arr256_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "ce1" }} , 
 	{ "name": "imag_arr256_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "q1" }} , 
 	{ "name": "real_arr128_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "address0" }} , 
 	{ "name": "real_arr128_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "ce0" }} , 
 	{ "name": "real_arr128_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "we0" }} , 
 	{ "name": "real_arr128_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "d0" }} , 
 	{ "name": "real_arr128_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "q0" }} , 
 	{ "name": "real_arr128_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "address1" }} , 
 	{ "name": "real_arr128_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "ce1" }} , 
 	{ "name": "real_arr128_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "q1" }} , 
 	{ "name": "imag_arr128_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "address0" }} , 
 	{ "name": "imag_arr128_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "ce0" }} , 
 	{ "name": "imag_arr128_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "we0" }} , 
 	{ "name": "imag_arr128_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "d0" }} , 
 	{ "name": "imag_arr128_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "q0" }} , 
 	{ "name": "imag_arr128_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "address1" }} , 
 	{ "name": "imag_arr128_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "ce1" }} , 
 	{ "name": "imag_arr128_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "q1" }} , 
 	{ "name": "real_arr128_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "address0" }} , 
 	{ "name": "real_arr128_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "ce0" }} , 
 	{ "name": "real_arr128_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "we0" }} , 
 	{ "name": "real_arr128_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "d0" }} , 
 	{ "name": "real_arr128_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "q0" }} , 
 	{ "name": "real_arr128_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "address1" }} , 
 	{ "name": "real_arr128_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "ce1" }} , 
 	{ "name": "real_arr128_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "q1" }} , 
 	{ "name": "imag_arr128_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "address0" }} , 
 	{ "name": "imag_arr128_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "ce0" }} , 
 	{ "name": "imag_arr128_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "we0" }} , 
 	{ "name": "imag_arr128_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "d0" }} , 
 	{ "name": "imag_arr128_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "q0" }} , 
 	{ "name": "imag_arr128_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "address1" }} , 
 	{ "name": "imag_arr128_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "ce1" }} , 
 	{ "name": "imag_arr128_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "q1" }} , 
 	{ "name": "real_arr128_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "address0" }} , 
 	{ "name": "real_arr128_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "ce0" }} , 
 	{ "name": "real_arr128_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "we0" }} , 
 	{ "name": "real_arr128_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "d0" }} , 
 	{ "name": "real_arr128_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "q0" }} , 
 	{ "name": "real_arr128_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "address1" }} , 
 	{ "name": "real_arr128_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "ce1" }} , 
 	{ "name": "real_arr128_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "q1" }} , 
 	{ "name": "imag_arr128_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "address0" }} , 
 	{ "name": "imag_arr128_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "ce0" }} , 
 	{ "name": "imag_arr128_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "we0" }} , 
 	{ "name": "imag_arr128_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "d0" }} , 
 	{ "name": "imag_arr128_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "q0" }} , 
 	{ "name": "imag_arr128_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "address1" }} , 
 	{ "name": "imag_arr128_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "ce1" }} , 
 	{ "name": "imag_arr128_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "q1" }} , 
 	{ "name": "real_arr128_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "address0" }} , 
 	{ "name": "real_arr128_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "ce0" }} , 
 	{ "name": "real_arr128_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "we0" }} , 
 	{ "name": "real_arr128_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "d0" }} , 
 	{ "name": "real_arr128_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "q0" }} , 
 	{ "name": "real_arr128_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "address1" }} , 
 	{ "name": "real_arr128_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "ce1" }} , 
 	{ "name": "real_arr128_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "q1" }} , 
 	{ "name": "imag_arr128_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "address0" }} , 
 	{ "name": "imag_arr128_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "ce0" }} , 
 	{ "name": "imag_arr128_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "we0" }} , 
 	{ "name": "imag_arr128_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "d0" }} , 
 	{ "name": "imag_arr128_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "q0" }} , 
 	{ "name": "imag_arr128_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "address1" }} , 
 	{ "name": "imag_arr128_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "ce1" }} , 
 	{ "name": "imag_arr128_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "q1" }} , 
 	{ "name": "real_arr64_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_arr64_0", "role": "address0" }} , 
 	{ "name": "real_arr64_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_0", "role": "ce0" }} , 
 	{ "name": "real_arr64_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_0", "role": "we0" }} , 
 	{ "name": "real_arr64_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_0", "role": "d0" }} , 
 	{ "name": "real_arr64_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_0", "role": "q0" }} , 
 	{ "name": "real_arr64_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_arr64_0", "role": "address1" }} , 
 	{ "name": "real_arr64_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_0", "role": "ce1" }} , 
 	{ "name": "real_arr64_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_0", "role": "q1" }} , 
 	{ "name": "imag_arr64_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_arr64_0", "role": "address0" }} , 
 	{ "name": "imag_arr64_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_0", "role": "ce0" }} , 
 	{ "name": "imag_arr64_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_0", "role": "we0" }} , 
 	{ "name": "imag_arr64_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_0", "role": "d0" }} , 
 	{ "name": "imag_arr64_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_0", "role": "q0" }} , 
 	{ "name": "imag_arr64_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_arr64_0", "role": "address1" }} , 
 	{ "name": "imag_arr64_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_0", "role": "ce1" }} , 
 	{ "name": "imag_arr64_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_0", "role": "q1" }} , 
 	{ "name": "real_arr64_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_arr64_1", "role": "address0" }} , 
 	{ "name": "real_arr64_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_1", "role": "ce0" }} , 
 	{ "name": "real_arr64_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_1", "role": "we0" }} , 
 	{ "name": "real_arr64_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_1", "role": "d0" }} , 
 	{ "name": "real_arr64_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_1", "role": "q0" }} , 
 	{ "name": "real_arr64_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_arr64_1", "role": "address1" }} , 
 	{ "name": "real_arr64_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_1", "role": "ce1" }} , 
 	{ "name": "real_arr64_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_1", "role": "q1" }} , 
 	{ "name": "imag_arr64_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_arr64_1", "role": "address0" }} , 
 	{ "name": "imag_arr64_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_1", "role": "ce0" }} , 
 	{ "name": "imag_arr64_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_1", "role": "we0" }} , 
 	{ "name": "imag_arr64_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_1", "role": "d0" }} , 
 	{ "name": "imag_arr64_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_1", "role": "q0" }} , 
 	{ "name": "imag_arr64_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_arr64_1", "role": "address1" }} , 
 	{ "name": "imag_arr64_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_1", "role": "ce1" }} , 
 	{ "name": "imag_arr64_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_1", "role": "q1" }} , 
 	{ "name": "real_arr64_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_arr64_2", "role": "address0" }} , 
 	{ "name": "real_arr64_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_2", "role": "ce0" }} , 
 	{ "name": "real_arr64_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_2", "role": "we0" }} , 
 	{ "name": "real_arr64_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_2", "role": "d0" }} , 
 	{ "name": "real_arr64_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_2", "role": "q0" }} , 
 	{ "name": "real_arr64_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_arr64_2", "role": "address1" }} , 
 	{ "name": "real_arr64_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_2", "role": "ce1" }} , 
 	{ "name": "real_arr64_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_2", "role": "q1" }} , 
 	{ "name": "imag_arr64_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_arr64_2", "role": "address0" }} , 
 	{ "name": "imag_arr64_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_2", "role": "ce0" }} , 
 	{ "name": "imag_arr64_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_2", "role": "we0" }} , 
 	{ "name": "imag_arr64_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_2", "role": "d0" }} , 
 	{ "name": "imag_arr64_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_2", "role": "q0" }} , 
 	{ "name": "imag_arr64_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_arr64_2", "role": "address1" }} , 
 	{ "name": "imag_arr64_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_2", "role": "ce1" }} , 
 	{ "name": "imag_arr64_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_2", "role": "q1" }} , 
 	{ "name": "real_arr64_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_arr64_3", "role": "address0" }} , 
 	{ "name": "real_arr64_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_3", "role": "ce0" }} , 
 	{ "name": "real_arr64_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_3", "role": "we0" }} , 
 	{ "name": "real_arr64_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_3", "role": "d0" }} , 
 	{ "name": "real_arr64_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_3", "role": "q0" }} , 
 	{ "name": "real_arr64_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "real_arr64_3", "role": "address1" }} , 
 	{ "name": "real_arr64_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr64_3", "role": "ce1" }} , 
 	{ "name": "real_arr64_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr64_3", "role": "q1" }} , 
 	{ "name": "imag_arr64_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_arr64_3", "role": "address0" }} , 
 	{ "name": "imag_arr64_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_3", "role": "ce0" }} , 
 	{ "name": "imag_arr64_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_3", "role": "we0" }} , 
 	{ "name": "imag_arr64_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_3", "role": "d0" }} , 
 	{ "name": "imag_arr64_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_3", "role": "q0" }} , 
 	{ "name": "imag_arr64_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":4, "type": "signal", "bundle":{"name": "imag_arr64_3", "role": "address1" }} , 
 	{ "name": "imag_arr64_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr64_3", "role": "ce1" }} , 
 	{ "name": "imag_arr64_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr64_3", "role": "q1" }} , 
 	{ "name": "real_arr32_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_arr32_0", "role": "address0" }} , 
 	{ "name": "real_arr32_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_0", "role": "ce0" }} , 
 	{ "name": "real_arr32_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_0", "role": "we0" }} , 
 	{ "name": "real_arr32_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_0", "role": "d0" }} , 
 	{ "name": "real_arr32_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_0", "role": "q0" }} , 
 	{ "name": "real_arr32_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_arr32_0", "role": "address1" }} , 
 	{ "name": "real_arr32_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_0", "role": "ce1" }} , 
 	{ "name": "real_arr32_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_0", "role": "q1" }} , 
 	{ "name": "imag_arr32_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_arr32_0", "role": "address0" }} , 
 	{ "name": "imag_arr32_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_0", "role": "ce0" }} , 
 	{ "name": "imag_arr32_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_0", "role": "we0" }} , 
 	{ "name": "imag_arr32_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_0", "role": "d0" }} , 
 	{ "name": "imag_arr32_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_0", "role": "q0" }} , 
 	{ "name": "imag_arr32_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_arr32_0", "role": "address1" }} , 
 	{ "name": "imag_arr32_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_0", "role": "ce1" }} , 
 	{ "name": "imag_arr32_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_0", "role": "q1" }} , 
 	{ "name": "real_arr32_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_arr32_1", "role": "address0" }} , 
 	{ "name": "real_arr32_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_1", "role": "ce0" }} , 
 	{ "name": "real_arr32_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_1", "role": "we0" }} , 
 	{ "name": "real_arr32_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_1", "role": "d0" }} , 
 	{ "name": "real_arr32_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_1", "role": "q0" }} , 
 	{ "name": "real_arr32_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_arr32_1", "role": "address1" }} , 
 	{ "name": "real_arr32_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_1", "role": "ce1" }} , 
 	{ "name": "real_arr32_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_1", "role": "q1" }} , 
 	{ "name": "imag_arr32_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_arr32_1", "role": "address0" }} , 
 	{ "name": "imag_arr32_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_1", "role": "ce0" }} , 
 	{ "name": "imag_arr32_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_1", "role": "we0" }} , 
 	{ "name": "imag_arr32_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_1", "role": "d0" }} , 
 	{ "name": "imag_arr32_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_1", "role": "q0" }} , 
 	{ "name": "imag_arr32_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_arr32_1", "role": "address1" }} , 
 	{ "name": "imag_arr32_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_1", "role": "ce1" }} , 
 	{ "name": "imag_arr32_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_1", "role": "q1" }} , 
 	{ "name": "real_arr32_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_arr32_2", "role": "address0" }} , 
 	{ "name": "real_arr32_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_2", "role": "ce0" }} , 
 	{ "name": "real_arr32_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_2", "role": "we0" }} , 
 	{ "name": "real_arr32_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_2", "role": "d0" }} , 
 	{ "name": "real_arr32_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_2", "role": "q0" }} , 
 	{ "name": "real_arr32_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_arr32_2", "role": "address1" }} , 
 	{ "name": "real_arr32_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_2", "role": "ce1" }} , 
 	{ "name": "real_arr32_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_2", "role": "q1" }} , 
 	{ "name": "imag_arr32_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_arr32_2", "role": "address0" }} , 
 	{ "name": "imag_arr32_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_2", "role": "ce0" }} , 
 	{ "name": "imag_arr32_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_2", "role": "we0" }} , 
 	{ "name": "imag_arr32_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_2", "role": "d0" }} , 
 	{ "name": "imag_arr32_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_2", "role": "q0" }} , 
 	{ "name": "imag_arr32_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_arr32_2", "role": "address1" }} , 
 	{ "name": "imag_arr32_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_2", "role": "ce1" }} , 
 	{ "name": "imag_arr32_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_2", "role": "q1" }} , 
 	{ "name": "real_arr32_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_arr32_3", "role": "address0" }} , 
 	{ "name": "real_arr32_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_3", "role": "ce0" }} , 
 	{ "name": "real_arr32_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_3", "role": "we0" }} , 
 	{ "name": "real_arr32_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_3", "role": "d0" }} , 
 	{ "name": "real_arr32_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_3", "role": "q0" }} , 
 	{ "name": "real_arr32_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "real_arr32_3", "role": "address1" }} , 
 	{ "name": "real_arr32_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr32_3", "role": "ce1" }} , 
 	{ "name": "real_arr32_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr32_3", "role": "q1" }} , 
 	{ "name": "imag_arr32_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_arr32_3", "role": "address0" }} , 
 	{ "name": "imag_arr32_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_3", "role": "ce0" }} , 
 	{ "name": "imag_arr32_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_3", "role": "we0" }} , 
 	{ "name": "imag_arr32_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_3", "role": "d0" }} , 
 	{ "name": "imag_arr32_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_3", "role": "q0" }} , 
 	{ "name": "imag_arr32_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":3, "type": "signal", "bundle":{"name": "imag_arr32_3", "role": "address1" }} , 
 	{ "name": "imag_arr32_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr32_3", "role": "ce1" }} , 
 	{ "name": "imag_arr32_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr32_3", "role": "q1" }} , 
 	{ "name": "real_arr16_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "real_arr16_0", "role": "address0" }} , 
 	{ "name": "real_arr16_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_0", "role": "ce0" }} , 
 	{ "name": "real_arr16_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_0", "role": "we0" }} , 
 	{ "name": "real_arr16_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_0", "role": "d0" }} , 
 	{ "name": "real_arr16_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_0", "role": "q0" }} , 
 	{ "name": "imag_arr16_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "imag_arr16_0", "role": "address0" }} , 
 	{ "name": "imag_arr16_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_0", "role": "ce0" }} , 
 	{ "name": "imag_arr16_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_0", "role": "we0" }} , 
 	{ "name": "imag_arr16_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_0", "role": "d0" }} , 
 	{ "name": "imag_arr16_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_0", "role": "q0" }} , 
 	{ "name": "real_arr16_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "real_arr16_1", "role": "address0" }} , 
 	{ "name": "real_arr16_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_1", "role": "ce0" }} , 
 	{ "name": "real_arr16_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_1", "role": "we0" }} , 
 	{ "name": "real_arr16_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_1", "role": "d0" }} , 
 	{ "name": "real_arr16_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_1", "role": "q0" }} , 
 	{ "name": "imag_arr16_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "imag_arr16_1", "role": "address0" }} , 
 	{ "name": "imag_arr16_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_1", "role": "ce0" }} , 
 	{ "name": "imag_arr16_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_1", "role": "we0" }} , 
 	{ "name": "imag_arr16_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_1", "role": "d0" }} , 
 	{ "name": "imag_arr16_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_1", "role": "q0" }} , 
 	{ "name": "real_arr16_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "real_arr16_2", "role": "address0" }} , 
 	{ "name": "real_arr16_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_2", "role": "ce0" }} , 
 	{ "name": "real_arr16_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_2", "role": "we0" }} , 
 	{ "name": "real_arr16_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_2", "role": "d0" }} , 
 	{ "name": "real_arr16_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_2", "role": "q0" }} , 
 	{ "name": "imag_arr16_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "imag_arr16_2", "role": "address0" }} , 
 	{ "name": "imag_arr16_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_2", "role": "ce0" }} , 
 	{ "name": "imag_arr16_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_2", "role": "we0" }} , 
 	{ "name": "imag_arr16_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_2", "role": "d0" }} , 
 	{ "name": "imag_arr16_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_2", "role": "q0" }} , 
 	{ "name": "real_arr16_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "real_arr16_3", "role": "address0" }} , 
 	{ "name": "real_arr16_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_3", "role": "ce0" }} , 
 	{ "name": "real_arr16_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr16_3", "role": "we0" }} , 
 	{ "name": "real_arr16_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_3", "role": "d0" }} , 
 	{ "name": "real_arr16_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr16_3", "role": "q0" }} , 
 	{ "name": "imag_arr16_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":2, "type": "signal", "bundle":{"name": "imag_arr16_3", "role": "address0" }} , 
 	{ "name": "imag_arr16_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_3", "role": "ce0" }} , 
 	{ "name": "imag_arr16_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr16_3", "role": "we0" }} , 
 	{ "name": "imag_arr16_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_3", "role": "d0" }} , 
 	{ "name": "imag_arr16_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr16_3", "role": "q0" }} , 
 	{ "name": "imag_arr8_1_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_1", "role": "i" }} , 
 	{ "name": "imag_arr8_1_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_1", "role": "o" }} , 
 	{ "name": "imag_arr8_1_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_1", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr8_1_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_1", "role": "i" }} , 
 	{ "name": "real_arr8_1_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_1", "role": "o" }} , 
 	{ "name": "real_arr8_1_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_1", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr8_0_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_0", "role": "i" }} , 
 	{ "name": "imag_arr8_0_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_0", "role": "o" }} , 
 	{ "name": "imag_arr8_0_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_0", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr8_0_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_0", "role": "i" }} , 
 	{ "name": "real_arr8_0_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_0", "role": "o" }} , 
 	{ "name": "real_arr8_0_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_0", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr8_2_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_2", "role": "i" }} , 
 	{ "name": "real_arr8_2_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_2", "role": "o" }} , 
 	{ "name": "real_arr8_2_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_2", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr8_4_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_4", "role": "i" }} , 
 	{ "name": "real_arr8_4_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_4", "role": "o" }} , 
 	{ "name": "real_arr8_4_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_4", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr8_6_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_6", "role": "i" }} , 
 	{ "name": "real_arr8_6_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_6", "role": "o" }} , 
 	{ "name": "real_arr8_6_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_6", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr8_2_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_2", "role": "i" }} , 
 	{ "name": "imag_arr8_2_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_2", "role": "o" }} , 
 	{ "name": "imag_arr8_2_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_2", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr8_4_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_4", "role": "i" }} , 
 	{ "name": "imag_arr8_4_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_4", "role": "o" }} , 
 	{ "name": "imag_arr8_4_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_4", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr8_6_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_6", "role": "i" }} , 
 	{ "name": "imag_arr8_6_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_6", "role": "o" }} , 
 	{ "name": "imag_arr8_6_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_6", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr8_3_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_3", "role": "i" }} , 
 	{ "name": "real_arr8_3_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_3", "role": "o" }} , 
 	{ "name": "real_arr8_3_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_3", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr8_5_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_5", "role": "i" }} , 
 	{ "name": "real_arr8_5_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_5", "role": "o" }} , 
 	{ "name": "real_arr8_5_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_5", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr8_7_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_7", "role": "i" }} , 
 	{ "name": "real_arr8_7_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr8_7", "role": "o" }} , 
 	{ "name": "real_arr8_7_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr8_7", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr8_3_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_3", "role": "i" }} , 
 	{ "name": "imag_arr8_3_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_3", "role": "o" }} , 
 	{ "name": "imag_arr8_3_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_3", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr8_5_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_5", "role": "i" }} , 
 	{ "name": "imag_arr8_5_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_5", "role": "o" }} , 
 	{ "name": "imag_arr8_5_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_5", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr8_7_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_7", "role": "i" }} , 
 	{ "name": "imag_arr8_7_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr8_7", "role": "o" }} , 
 	{ "name": "imag_arr8_7_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr8_7", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr4_0_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_0", "role": "i" }} , 
 	{ "name": "imag_arr4_0_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_0", "role": "o" }} , 
 	{ "name": "imag_arr4_0_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr4_0", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr4_0_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_0", "role": "i" }} , 
 	{ "name": "real_arr4_0_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_0", "role": "o" }} , 
 	{ "name": "real_arr4_0_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr4_0", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr4_1_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_1", "role": "i" }} , 
 	{ "name": "real_arr4_1_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_1", "role": "o" }} , 
 	{ "name": "real_arr4_1_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr4_1", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr4_2_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_2", "role": "i" }} , 
 	{ "name": "real_arr4_2_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_2", "role": "o" }} , 
 	{ "name": "real_arr4_2_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr4_2", "role": "o_ap_vld" }} , 
 	{ "name": "real_arr4_3_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_3", "role": "i" }} , 
 	{ "name": "real_arr4_3_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_3", "role": "o" }} , 
 	{ "name": "real_arr4_3_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr4_3", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr4_1_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_1", "role": "i" }} , 
 	{ "name": "imag_arr4_1_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_1", "role": "o" }} , 
 	{ "name": "imag_arr4_1_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr4_1", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr4_2_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_2", "role": "i" }} , 
 	{ "name": "imag_arr4_2_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_2", "role": "o" }} , 
 	{ "name": "imag_arr4_2_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr4_2", "role": "o_ap_vld" }} , 
 	{ "name": "imag_arr4_3_i", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_3", "role": "i" }} , 
 	{ "name": "imag_arr4_3_o", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_3", "role": "o" }} , 
 	{ "name": "imag_arr4_3_o_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr4_3", "role": "o_ap_vld" }}  ]}
set Spec2ImplPortList { 
	real_i_0 { ap_memory {  { real_i_0_address0 mem_address 1 7 }  { real_i_0_ce0 mem_ce 1 1 }  { real_i_0_q0 mem_dout 0 32 }  { real_i_0_address1 mem_address 1 7 }  { real_i_0_ce1 mem_ce 1 1 }  { real_i_0_q1 mem_dout 0 32 } } }
	real_i_1 { ap_memory {  { real_i_1_address0 mem_address 1 7 }  { real_i_1_ce0 mem_ce 1 1 }  { real_i_1_q0 mem_dout 0 32 }  { real_i_1_address1 mem_address 1 7 }  { real_i_1_ce1 mem_ce 1 1 }  { real_i_1_q1 mem_dout 0 32 } } }
	real_i_2 { ap_memory {  { real_i_2_address0 mem_address 1 7 }  { real_i_2_ce0 mem_ce 1 1 }  { real_i_2_q0 mem_dout 0 32 }  { real_i_2_address1 mem_address 1 7 }  { real_i_2_ce1 mem_ce 1 1 }  { real_i_2_q1 mem_dout 0 32 } } }
	real_i_3 { ap_memory {  { real_i_3_address0 mem_address 1 7 }  { real_i_3_ce0 mem_ce 1 1 }  { real_i_3_q0 mem_dout 0 32 }  { real_i_3_address1 mem_address 1 7 }  { real_i_3_ce1 mem_ce 1 1 }  { real_i_3_q1 mem_dout 0 32 } } }
	real_i_4 { ap_memory {  { real_i_4_address0 mem_address 1 7 }  { real_i_4_ce0 mem_ce 1 1 }  { real_i_4_q0 mem_dout 0 32 }  { real_i_4_address1 mem_address 1 7 }  { real_i_4_ce1 mem_ce 1 1 }  { real_i_4_q1 mem_dout 0 32 } } }
	real_i_5 { ap_memory {  { real_i_5_address0 mem_address 1 7 }  { real_i_5_ce0 mem_ce 1 1 }  { real_i_5_q0 mem_dout 0 32 }  { real_i_5_address1 mem_address 1 7 }  { real_i_5_ce1 mem_ce 1 1 }  { real_i_5_q1 mem_dout 0 32 } } }
	real_i_6 { ap_memory {  { real_i_6_address0 mem_address 1 7 }  { real_i_6_ce0 mem_ce 1 1 }  { real_i_6_q0 mem_dout 0 32 }  { real_i_6_address1 mem_address 1 7 }  { real_i_6_ce1 mem_ce 1 1 }  { real_i_6_q1 mem_dout 0 32 } } }
	real_i_7 { ap_memory {  { real_i_7_address0 mem_address 1 7 }  { real_i_7_ce0 mem_ce 1 1 }  { real_i_7_q0 mem_dout 0 32 }  { real_i_7_address1 mem_address 1 7 }  { real_i_7_ce1 mem_ce 1 1 }  { real_i_7_q1 mem_dout 0 32 } } }
	imag_i_0 { ap_memory {  { imag_i_0_address0 mem_address 1 7 }  { imag_i_0_ce0 mem_ce 1 1 }  { imag_i_0_q0 mem_dout 0 32 }  { imag_i_0_address1 mem_address 1 7 }  { imag_i_0_ce1 mem_ce 1 1 }  { imag_i_0_q1 mem_dout 0 32 } } }
	imag_i_1 { ap_memory {  { imag_i_1_address0 mem_address 1 7 }  { imag_i_1_ce0 mem_ce 1 1 }  { imag_i_1_q0 mem_dout 0 32 }  { imag_i_1_address1 mem_address 1 7 }  { imag_i_1_ce1 mem_ce 1 1 }  { imag_i_1_q1 mem_dout 0 32 } } }
	imag_i_2 { ap_memory {  { imag_i_2_address0 mem_address 1 7 }  { imag_i_2_ce0 mem_ce 1 1 }  { imag_i_2_q0 mem_dout 0 32 }  { imag_i_2_address1 mem_address 1 7 }  { imag_i_2_ce1 mem_ce 1 1 }  { imag_i_2_q1 mem_dout 0 32 } } }
	imag_i_3 { ap_memory {  { imag_i_3_address0 mem_address 1 7 }  { imag_i_3_ce0 mem_ce 1 1 }  { imag_i_3_q0 mem_dout 0 32 }  { imag_i_3_address1 mem_address 1 7 }  { imag_i_3_ce1 mem_ce 1 1 }  { imag_i_3_q1 mem_dout 0 32 } } }
	imag_i_4 { ap_memory {  { imag_i_4_address0 mem_address 1 7 }  { imag_i_4_ce0 mem_ce 1 1 }  { imag_i_4_q0 mem_dout 0 32 }  { imag_i_4_address1 mem_address 1 7 }  { imag_i_4_ce1 mem_ce 1 1 }  { imag_i_4_q1 mem_dout 0 32 } } }
	imag_i_5 { ap_memory {  { imag_i_5_address0 mem_address 1 7 }  { imag_i_5_ce0 mem_ce 1 1 }  { imag_i_5_q0 mem_dout 0 32 }  { imag_i_5_address1 mem_address 1 7 }  { imag_i_5_ce1 mem_ce 1 1 }  { imag_i_5_q1 mem_dout 0 32 } } }
	imag_i_6 { ap_memory {  { imag_i_6_address0 mem_address 1 7 }  { imag_i_6_ce0 mem_ce 1 1 }  { imag_i_6_q0 mem_dout 0 32 }  { imag_i_6_address1 mem_address 1 7 }  { imag_i_6_ce1 mem_ce 1 1 }  { imag_i_6_q1 mem_dout 0 32 } } }
	imag_i_7 { ap_memory {  { imag_i_7_address0 mem_address 1 7 }  { imag_i_7_ce0 mem_ce 1 1 }  { imag_i_7_q0 mem_dout 0 32 }  { imag_i_7_address1 mem_address 1 7 }  { imag_i_7_ce1 mem_ce 1 1 }  { imag_i_7_q1 mem_dout 0 32 } } }
	real_o { ap_memory {  { real_o_address0 mem_address 1 10 }  { real_o_ce0 mem_ce 1 1 }  { real_o_we0 mem_we 1 1 }  { real_o_d0 mem_din 1 32 } } }
	imag_o { ap_memory {  { imag_o_address0 mem_address 1 10 }  { imag_o_ce0 mem_ce 1 1 }  { imag_o_we0 mem_we 1 1 }  { imag_o_d0 mem_din 1 32 } } }
	imag_arr1024_1 { ap_memory {  { imag_arr1024_1_address0 mem_address 1 8 }  { imag_arr1024_1_ce0 mem_ce 1 1 }  { imag_arr1024_1_we0 mem_we 1 1 }  { imag_arr1024_1_d0 mem_din 1 32 }  { imag_arr1024_1_q0 mem_dout 0 32 }  { imag_arr1024_1_address1 mem_address 1 8 }  { imag_arr1024_1_ce1 mem_ce 1 1 }  { imag_arr1024_1_q1 mem_dout 0 32 } } }
	real_arr1024_1 { ap_memory {  { real_arr1024_1_address0 mem_address 1 8 }  { real_arr1024_1_ce0 mem_ce 1 1 }  { real_arr1024_1_we0 mem_we 1 1 }  { real_arr1024_1_d0 mem_din 1 32 }  { real_arr1024_1_q0 mem_dout 0 32 }  { real_arr1024_1_address1 mem_address 1 8 }  { real_arr1024_1_ce1 mem_ce 1 1 }  { real_arr1024_1_q1 mem_dout 0 32 } } }
	imag_arr1024_0 { ap_memory {  { imag_arr1024_0_address0 mem_address 1 8 }  { imag_arr1024_0_ce0 mem_ce 1 1 }  { imag_arr1024_0_we0 mem_we 1 1 }  { imag_arr1024_0_d0 mem_din 1 32 }  { imag_arr1024_0_q0 mem_dout 0 32 }  { imag_arr1024_0_address1 mem_address 1 8 }  { imag_arr1024_0_ce1 mem_ce 1 1 }  { imag_arr1024_0_q1 mem_dout 0 32 } } }
	real_arr1024_0 { ap_memory {  { real_arr1024_0_address0 mem_address 1 8 }  { real_arr1024_0_ce0 mem_ce 1 1 }  { real_arr1024_0_we0 mem_we 1 1 }  { real_arr1024_0_d0 mem_din 1 32 }  { real_arr1024_0_q0 mem_dout 0 32 }  { real_arr1024_0_address1 mem_address 1 8 }  { real_arr1024_0_ce1 mem_ce 1 1 }  { real_arr1024_0_q1 mem_dout 0 32 } } }
	imag_arr1024_3 { ap_memory {  { imag_arr1024_3_address0 mem_address 1 8 }  { imag_arr1024_3_ce0 mem_ce 1 1 }  { imag_arr1024_3_we0 mem_we 1 1 }  { imag_arr1024_3_d0 mem_din 1 32 }  { imag_arr1024_3_q0 mem_dout 0 32 }  { imag_arr1024_3_address1 mem_address 1 8 }  { imag_arr1024_3_ce1 mem_ce 1 1 }  { imag_arr1024_3_q1 mem_dout 0 32 } } }
	real_arr1024_3 { ap_memory {  { real_arr1024_3_address0 mem_address 1 8 }  { real_arr1024_3_ce0 mem_ce 1 1 }  { real_arr1024_3_we0 mem_we 1 1 }  { real_arr1024_3_d0 mem_din 1 32 }  { real_arr1024_3_q0 mem_dout 0 32 }  { real_arr1024_3_address1 mem_address 1 8 }  { real_arr1024_3_ce1 mem_ce 1 1 }  { real_arr1024_3_q1 mem_dout 0 32 } } }
	imag_arr1024_2 { ap_memory {  { imag_arr1024_2_address0 mem_address 1 8 }  { imag_arr1024_2_ce0 mem_ce 1 1 }  { imag_arr1024_2_we0 mem_we 1 1 }  { imag_arr1024_2_d0 mem_din 1 32 }  { imag_arr1024_2_q0 mem_dout 0 32 }  { imag_arr1024_2_address1 mem_address 1 8 }  { imag_arr1024_2_ce1 mem_ce 1 1 }  { imag_arr1024_2_q1 mem_dout 0 32 } } }
	real_arr1024_2 { ap_memory {  { real_arr1024_2_address0 mem_address 1 8 }  { real_arr1024_2_ce0 mem_ce 1 1 }  { real_arr1024_2_we0 mem_we 1 1 }  { real_arr1024_2_d0 mem_din 1 32 }  { real_arr1024_2_q0 mem_dout 0 32 }  { real_arr1024_2_address1 mem_address 1 8 }  { real_arr1024_2_ce1 mem_ce 1 1 }  { real_arr1024_2_q1 mem_dout 0 32 } } }
	real_arr512_0 { ap_memory {  { real_arr512_0_address0 mem_address 1 7 }  { real_arr512_0_ce0 mem_ce 1 1 }  { real_arr512_0_we0 mem_we 1 1 }  { real_arr512_0_d0 mem_din 1 32 }  { real_arr512_0_q0 mem_dout 0 32 }  { real_arr512_0_address1 mem_address 1 7 }  { real_arr512_0_ce1 mem_ce 1 1 }  { real_arr512_0_q1 mem_dout 0 32 } } }
	imag_arr512_0 { ap_memory {  { imag_arr512_0_address0 mem_address 1 7 }  { imag_arr512_0_ce0 mem_ce 1 1 }  { imag_arr512_0_we0 mem_we 1 1 }  { imag_arr512_0_d0 mem_din 1 32 }  { imag_arr512_0_q0 mem_dout 0 32 }  { imag_arr512_0_address1 mem_address 1 7 }  { imag_arr512_0_ce1 mem_ce 1 1 }  { imag_arr512_0_q1 mem_dout 0 32 } } }
	real_arr512_1 { ap_memory {  { real_arr512_1_address0 mem_address 1 7 }  { real_arr512_1_ce0 mem_ce 1 1 }  { real_arr512_1_we0 mem_we 1 1 }  { real_arr512_1_d0 mem_din 1 32 }  { real_arr512_1_q0 mem_dout 0 32 }  { real_arr512_1_address1 mem_address 1 7 }  { real_arr512_1_ce1 mem_ce 1 1 }  { real_arr512_1_q1 mem_dout 0 32 } } }
	imag_arr512_1 { ap_memory {  { imag_arr512_1_address0 mem_address 1 7 }  { imag_arr512_1_ce0 mem_ce 1 1 }  { imag_arr512_1_we0 mem_we 1 1 }  { imag_arr512_1_d0 mem_din 1 32 }  { imag_arr512_1_q0 mem_dout 0 32 }  { imag_arr512_1_address1 mem_address 1 7 }  { imag_arr512_1_ce1 mem_ce 1 1 }  { imag_arr512_1_q1 mem_dout 0 32 } } }
	real_arr512_2 { ap_memory {  { real_arr512_2_address0 mem_address 1 7 }  { real_arr512_2_ce0 mem_ce 1 1 }  { real_arr512_2_we0 mem_we 1 1 }  { real_arr512_2_d0 mem_din 1 32 }  { real_arr512_2_q0 mem_dout 0 32 }  { real_arr512_2_address1 mem_address 1 7 }  { real_arr512_2_ce1 mem_ce 1 1 }  { real_arr512_2_q1 mem_dout 0 32 } } }
	imag_arr512_2 { ap_memory {  { imag_arr512_2_address0 mem_address 1 7 }  { imag_arr512_2_ce0 mem_ce 1 1 }  { imag_arr512_2_we0 mem_we 1 1 }  { imag_arr512_2_d0 mem_din 1 32 }  { imag_arr512_2_q0 mem_dout 0 32 }  { imag_arr512_2_address1 mem_address 1 7 }  { imag_arr512_2_ce1 mem_ce 1 1 }  { imag_arr512_2_q1 mem_dout 0 32 } } }
	real_arr512_3 { ap_memory {  { real_arr512_3_address0 mem_address 1 7 }  { real_arr512_3_ce0 mem_ce 1 1 }  { real_arr512_3_we0 mem_we 1 1 }  { real_arr512_3_d0 mem_din 1 32 }  { real_arr512_3_q0 mem_dout 0 32 }  { real_arr512_3_address1 mem_address 1 7 }  { real_arr512_3_ce1 mem_ce 1 1 }  { real_arr512_3_q1 mem_dout 0 32 } } }
	imag_arr512_3 { ap_memory {  { imag_arr512_3_address0 mem_address 1 7 }  { imag_arr512_3_ce0 mem_ce 1 1 }  { imag_arr512_3_we0 mem_we 1 1 }  { imag_arr512_3_d0 mem_din 1 32 }  { imag_arr512_3_q0 mem_dout 0 32 }  { imag_arr512_3_address1 mem_address 1 7 }  { imag_arr512_3_ce1 mem_ce 1 1 }  { imag_arr512_3_q1 mem_dout 0 32 } } }
	real_arr256_0 { ap_memory {  { real_arr256_0_address0 mem_address 1 6 }  { real_arr256_0_ce0 mem_ce 1 1 }  { real_arr256_0_we0 mem_we 1 1 }  { real_arr256_0_d0 mem_din 1 32 }  { real_arr256_0_q0 mem_dout 0 32 }  { real_arr256_0_address1 mem_address 1 6 }  { real_arr256_0_ce1 mem_ce 1 1 }  { real_arr256_0_q1 mem_dout 0 32 } } }
	imag_arr256_0 { ap_memory {  { imag_arr256_0_address0 mem_address 1 6 }  { imag_arr256_0_ce0 mem_ce 1 1 }  { imag_arr256_0_we0 mem_we 1 1 }  { imag_arr256_0_d0 mem_din 1 32 }  { imag_arr256_0_q0 mem_dout 0 32 }  { imag_arr256_0_address1 mem_address 1 6 }  { imag_arr256_0_ce1 mem_ce 1 1 }  { imag_arr256_0_q1 mem_dout 0 32 } } }
	real_arr256_1 { ap_memory {  { real_arr256_1_address0 mem_address 1 6 }  { real_arr256_1_ce0 mem_ce 1 1 }  { real_arr256_1_we0 mem_we 1 1 }  { real_arr256_1_d0 mem_din 1 32 }  { real_arr256_1_q0 mem_dout 0 32 }  { real_arr256_1_address1 mem_address 1 6 }  { real_arr256_1_ce1 mem_ce 1 1 }  { real_arr256_1_q1 mem_dout 0 32 } } }
	imag_arr256_1 { ap_memory {  { imag_arr256_1_address0 mem_address 1 6 }  { imag_arr256_1_ce0 mem_ce 1 1 }  { imag_arr256_1_we0 mem_we 1 1 }  { imag_arr256_1_d0 mem_din 1 32 }  { imag_arr256_1_q0 mem_dout 0 32 }  { imag_arr256_1_address1 mem_address 1 6 }  { imag_arr256_1_ce1 mem_ce 1 1 }  { imag_arr256_1_q1 mem_dout 0 32 } } }
	real_arr256_2 { ap_memory {  { real_arr256_2_address0 mem_address 1 6 }  { real_arr256_2_ce0 mem_ce 1 1 }  { real_arr256_2_we0 mem_we 1 1 }  { real_arr256_2_d0 mem_din 1 32 }  { real_arr256_2_q0 mem_dout 0 32 }  { real_arr256_2_address1 mem_address 1 6 }  { real_arr256_2_ce1 mem_ce 1 1 }  { real_arr256_2_q1 mem_dout 0 32 } } }
	imag_arr256_2 { ap_memory {  { imag_arr256_2_address0 mem_address 1 6 }  { imag_arr256_2_ce0 mem_ce 1 1 }  { imag_arr256_2_we0 mem_we 1 1 }  { imag_arr256_2_d0 mem_din 1 32 }  { imag_arr256_2_q0 mem_dout 0 32 }  { imag_arr256_2_address1 mem_address 1 6 }  { imag_arr256_2_ce1 mem_ce 1 1 }  { imag_arr256_2_q1 mem_dout 0 32 } } }
	real_arr256_3 { ap_memory {  { real_arr256_3_address0 mem_address 1 6 }  { real_arr256_3_ce0 mem_ce 1 1 }  { real_arr256_3_we0 mem_we 1 1 }  { real_arr256_3_d0 mem_din 1 32 }  { real_arr256_3_q0 mem_dout 0 32 }  { real_arr256_3_address1 mem_address 1 6 }  { real_arr256_3_ce1 mem_ce 1 1 }  { real_arr256_3_q1 mem_dout 0 32 } } }
	imag_arr256_3 { ap_memory {  { imag_arr256_3_address0 mem_address 1 6 }  { imag_arr256_3_ce0 mem_ce 1 1 }  { imag_arr256_3_we0 mem_we 1 1 }  { imag_arr256_3_d0 mem_din 1 32 }  { imag_arr256_3_q0 mem_dout 0 32 }  { imag_arr256_3_address1 mem_address 1 6 }  { imag_arr256_3_ce1 mem_ce 1 1 }  { imag_arr256_3_q1 mem_dout 0 32 } } }
	real_arr128_0 { ap_memory {  { real_arr128_0_address0 mem_address 1 5 }  { real_arr128_0_ce0 mem_ce 1 1 }  { real_arr128_0_we0 mem_we 1 1 }  { real_arr128_0_d0 mem_din 1 32 }  { real_arr128_0_q0 mem_dout 0 32 }  { real_arr128_0_address1 mem_address 1 5 }  { real_arr128_0_ce1 mem_ce 1 1 }  { real_arr128_0_q1 mem_dout 0 32 } } }
	imag_arr128_0 { ap_memory {  { imag_arr128_0_address0 mem_address 1 5 }  { imag_arr128_0_ce0 mem_ce 1 1 }  { imag_arr128_0_we0 mem_we 1 1 }  { imag_arr128_0_d0 mem_din 1 32 }  { imag_arr128_0_q0 mem_dout 0 32 }  { imag_arr128_0_address1 mem_address 1 5 }  { imag_arr128_0_ce1 mem_ce 1 1 }  { imag_arr128_0_q1 mem_dout 0 32 } } }
	real_arr128_1 { ap_memory {  { real_arr128_1_address0 mem_address 1 5 }  { real_arr128_1_ce0 mem_ce 1 1 }  { real_arr128_1_we0 mem_we 1 1 }  { real_arr128_1_d0 mem_din 1 32 }  { real_arr128_1_q0 mem_dout 0 32 }  { real_arr128_1_address1 mem_address 1 5 }  { real_arr128_1_ce1 mem_ce 1 1 }  { real_arr128_1_q1 mem_dout 0 32 } } }
	imag_arr128_1 { ap_memory {  { imag_arr128_1_address0 mem_address 1 5 }  { imag_arr128_1_ce0 mem_ce 1 1 }  { imag_arr128_1_we0 mem_we 1 1 }  { imag_arr128_1_d0 mem_din 1 32 }  { imag_arr128_1_q0 mem_dout 0 32 }  { imag_arr128_1_address1 mem_address 1 5 }  { imag_arr128_1_ce1 mem_ce 1 1 }  { imag_arr128_1_q1 mem_dout 0 32 } } }
	real_arr128_2 { ap_memory {  { real_arr128_2_address0 mem_address 1 5 }  { real_arr128_2_ce0 mem_ce 1 1 }  { real_arr128_2_we0 mem_we 1 1 }  { real_arr128_2_d0 mem_din 1 32 }  { real_arr128_2_q0 mem_dout 0 32 }  { real_arr128_2_address1 mem_address 1 5 }  { real_arr128_2_ce1 mem_ce 1 1 }  { real_arr128_2_q1 mem_dout 0 32 } } }
	imag_arr128_2 { ap_memory {  { imag_arr128_2_address0 mem_address 1 5 }  { imag_arr128_2_ce0 mem_ce 1 1 }  { imag_arr128_2_we0 mem_we 1 1 }  { imag_arr128_2_d0 mem_din 1 32 }  { imag_arr128_2_q0 mem_dout 0 32 }  { imag_arr128_2_address1 mem_address 1 5 }  { imag_arr128_2_ce1 mem_ce 1 1 }  { imag_arr128_2_q1 mem_dout 0 32 } } }
	real_arr128_3 { ap_memory {  { real_arr128_3_address0 mem_address 1 5 }  { real_arr128_3_ce0 mem_ce 1 1 }  { real_arr128_3_we0 mem_we 1 1 }  { real_arr128_3_d0 mem_din 1 32 }  { real_arr128_3_q0 mem_dout 0 32 }  { real_arr128_3_address1 mem_address 1 5 }  { real_arr128_3_ce1 mem_ce 1 1 }  { real_arr128_3_q1 mem_dout 0 32 } } }
	imag_arr128_3 { ap_memory {  { imag_arr128_3_address0 mem_address 1 5 }  { imag_arr128_3_ce0 mem_ce 1 1 }  { imag_arr128_3_we0 mem_we 1 1 }  { imag_arr128_3_d0 mem_din 1 32 }  { imag_arr128_3_q0 mem_dout 0 32 }  { imag_arr128_3_address1 mem_address 1 5 }  { imag_arr128_3_ce1 mem_ce 1 1 }  { imag_arr128_3_q1 mem_dout 0 32 } } }
	real_arr64_0 { ap_memory {  { real_arr64_0_address0 mem_address 1 4 }  { real_arr64_0_ce0 mem_ce 1 1 }  { real_arr64_0_we0 mem_we 1 1 }  { real_arr64_0_d0 mem_din 1 32 }  { real_arr64_0_q0 mem_dout 0 32 }  { real_arr64_0_address1 mem_address 1 4 }  { real_arr64_0_ce1 mem_ce 1 1 }  { real_arr64_0_q1 mem_dout 0 32 } } }
	imag_arr64_0 { ap_memory {  { imag_arr64_0_address0 mem_address 1 4 }  { imag_arr64_0_ce0 mem_ce 1 1 }  { imag_arr64_0_we0 mem_we 1 1 }  { imag_arr64_0_d0 mem_din 1 32 }  { imag_arr64_0_q0 mem_dout 0 32 }  { imag_arr64_0_address1 mem_address 1 4 }  { imag_arr64_0_ce1 mem_ce 1 1 }  { imag_arr64_0_q1 mem_dout 0 32 } } }
	real_arr64_1 { ap_memory {  { real_arr64_1_address0 mem_address 1 4 }  { real_arr64_1_ce0 mem_ce 1 1 }  { real_arr64_1_we0 mem_we 1 1 }  { real_arr64_1_d0 mem_din 1 32 }  { real_arr64_1_q0 mem_dout 0 32 }  { real_arr64_1_address1 mem_address 1 4 }  { real_arr64_1_ce1 mem_ce 1 1 }  { real_arr64_1_q1 mem_dout 0 32 } } }
	imag_arr64_1 { ap_memory {  { imag_arr64_1_address0 mem_address 1 4 }  { imag_arr64_1_ce0 mem_ce 1 1 }  { imag_arr64_1_we0 mem_we 1 1 }  { imag_arr64_1_d0 mem_din 1 32 }  { imag_arr64_1_q0 mem_dout 0 32 }  { imag_arr64_1_address1 mem_address 1 4 }  { imag_arr64_1_ce1 mem_ce 1 1 }  { imag_arr64_1_q1 mem_dout 0 32 } } }
	real_arr64_2 { ap_memory {  { real_arr64_2_address0 mem_address 1 4 }  { real_arr64_2_ce0 mem_ce 1 1 }  { real_arr64_2_we0 mem_we 1 1 }  { real_arr64_2_d0 mem_din 1 32 }  { real_arr64_2_q0 mem_dout 0 32 }  { real_arr64_2_address1 mem_address 1 4 }  { real_arr64_2_ce1 mem_ce 1 1 }  { real_arr64_2_q1 mem_dout 0 32 } } }
	imag_arr64_2 { ap_memory {  { imag_arr64_2_address0 mem_address 1 4 }  { imag_arr64_2_ce0 mem_ce 1 1 }  { imag_arr64_2_we0 mem_we 1 1 }  { imag_arr64_2_d0 mem_din 1 32 }  { imag_arr64_2_q0 mem_dout 0 32 }  { imag_arr64_2_address1 mem_address 1 4 }  { imag_arr64_2_ce1 mem_ce 1 1 }  { imag_arr64_2_q1 mem_dout 0 32 } } }
	real_arr64_3 { ap_memory {  { real_arr64_3_address0 mem_address 1 4 }  { real_arr64_3_ce0 mem_ce 1 1 }  { real_arr64_3_we0 mem_we 1 1 }  { real_arr64_3_d0 mem_din 1 32 }  { real_arr64_3_q0 mem_dout 0 32 }  { real_arr64_3_address1 mem_address 1 4 }  { real_arr64_3_ce1 mem_ce 1 1 }  { real_arr64_3_q1 mem_dout 0 32 } } }
	imag_arr64_3 { ap_memory {  { imag_arr64_3_address0 mem_address 1 4 }  { imag_arr64_3_ce0 mem_ce 1 1 }  { imag_arr64_3_we0 mem_we 1 1 }  { imag_arr64_3_d0 mem_din 1 32 }  { imag_arr64_3_q0 mem_dout 0 32 }  { imag_arr64_3_address1 mem_address 1 4 }  { imag_arr64_3_ce1 mem_ce 1 1 }  { imag_arr64_3_q1 mem_dout 0 32 } } }
	real_arr32_0 { ap_memory {  { real_arr32_0_address0 mem_address 1 3 }  { real_arr32_0_ce0 mem_ce 1 1 }  { real_arr32_0_we0 mem_we 1 1 }  { real_arr32_0_d0 mem_din 1 32 }  { real_arr32_0_q0 mem_dout 0 32 }  { real_arr32_0_address1 mem_address 1 3 }  { real_arr32_0_ce1 mem_ce 1 1 }  { real_arr32_0_q1 mem_dout 0 32 } } }
	imag_arr32_0 { ap_memory {  { imag_arr32_0_address0 mem_address 1 3 }  { imag_arr32_0_ce0 mem_ce 1 1 }  { imag_arr32_0_we0 mem_we 1 1 }  { imag_arr32_0_d0 mem_din 1 32 }  { imag_arr32_0_q0 mem_dout 0 32 }  { imag_arr32_0_address1 mem_address 1 3 }  { imag_arr32_0_ce1 mem_ce 1 1 }  { imag_arr32_0_q1 mem_dout 0 32 } } }
	real_arr32_1 { ap_memory {  { real_arr32_1_address0 mem_address 1 3 }  { real_arr32_1_ce0 mem_ce 1 1 }  { real_arr32_1_we0 mem_we 1 1 }  { real_arr32_1_d0 mem_din 1 32 }  { real_arr32_1_q0 mem_dout 0 32 }  { real_arr32_1_address1 mem_address 1 3 }  { real_arr32_1_ce1 mem_ce 1 1 }  { real_arr32_1_q1 mem_dout 0 32 } } }
	imag_arr32_1 { ap_memory {  { imag_arr32_1_address0 mem_address 1 3 }  { imag_arr32_1_ce0 mem_ce 1 1 }  { imag_arr32_1_we0 mem_we 1 1 }  { imag_arr32_1_d0 mem_din 1 32 }  { imag_arr32_1_q0 mem_dout 0 32 }  { imag_arr32_1_address1 mem_address 1 3 }  { imag_arr32_1_ce1 mem_ce 1 1 }  { imag_arr32_1_q1 mem_dout 0 32 } } }
	real_arr32_2 { ap_memory {  { real_arr32_2_address0 mem_address 1 3 }  { real_arr32_2_ce0 mem_ce 1 1 }  { real_arr32_2_we0 mem_we 1 1 }  { real_arr32_2_d0 mem_din 1 32 }  { real_arr32_2_q0 mem_dout 0 32 }  { real_arr32_2_address1 mem_address 1 3 }  { real_arr32_2_ce1 mem_ce 1 1 }  { real_arr32_2_q1 mem_dout 0 32 } } }
	imag_arr32_2 { ap_memory {  { imag_arr32_2_address0 mem_address 1 3 }  { imag_arr32_2_ce0 mem_ce 1 1 }  { imag_arr32_2_we0 mem_we 1 1 }  { imag_arr32_2_d0 mem_din 1 32 }  { imag_arr32_2_q0 mem_dout 0 32 }  { imag_arr32_2_address1 mem_address 1 3 }  { imag_arr32_2_ce1 mem_ce 1 1 }  { imag_arr32_2_q1 mem_dout 0 32 } } }
	real_arr32_3 { ap_memory {  { real_arr32_3_address0 mem_address 1 3 }  { real_arr32_3_ce0 mem_ce 1 1 }  { real_arr32_3_we0 mem_we 1 1 }  { real_arr32_3_d0 mem_din 1 32 }  { real_arr32_3_q0 mem_dout 0 32 }  { real_arr32_3_address1 mem_address 1 3 }  { real_arr32_3_ce1 mem_ce 1 1 }  { real_arr32_3_q1 mem_dout 0 32 } } }
	imag_arr32_3 { ap_memory {  { imag_arr32_3_address0 mem_address 1 3 }  { imag_arr32_3_ce0 mem_ce 1 1 }  { imag_arr32_3_we0 mem_we 1 1 }  { imag_arr32_3_d0 mem_din 1 32 }  { imag_arr32_3_q0 mem_dout 0 32 }  { imag_arr32_3_address1 mem_address 1 3 }  { imag_arr32_3_ce1 mem_ce 1 1 }  { imag_arr32_3_q1 mem_dout 0 32 } } }
	real_arr16_0 { ap_memory {  { real_arr16_0_address0 mem_address 1 2 }  { real_arr16_0_ce0 mem_ce 1 1 }  { real_arr16_0_we0 mem_we 1 1 }  { real_arr16_0_d0 mem_din 1 32 }  { real_arr16_0_q0 mem_dout 0 32 } } }
	imag_arr16_0 { ap_memory {  { imag_arr16_0_address0 mem_address 1 2 }  { imag_arr16_0_ce0 mem_ce 1 1 }  { imag_arr16_0_we0 mem_we 1 1 }  { imag_arr16_0_d0 mem_din 1 32 }  { imag_arr16_0_q0 mem_dout 0 32 } } }
	real_arr16_1 { ap_memory {  { real_arr16_1_address0 mem_address 1 2 }  { real_arr16_1_ce0 mem_ce 1 1 }  { real_arr16_1_we0 mem_we 1 1 }  { real_arr16_1_d0 mem_din 1 32 }  { real_arr16_1_q0 mem_dout 0 32 } } }
	imag_arr16_1 { ap_memory {  { imag_arr16_1_address0 mem_address 1 2 }  { imag_arr16_1_ce0 mem_ce 1 1 }  { imag_arr16_1_we0 mem_we 1 1 }  { imag_arr16_1_d0 mem_din 1 32 }  { imag_arr16_1_q0 mem_dout 0 32 } } }
	real_arr16_2 { ap_memory {  { real_arr16_2_address0 mem_address 1 2 }  { real_arr16_2_ce0 mem_ce 1 1 }  { real_arr16_2_we0 mem_we 1 1 }  { real_arr16_2_d0 mem_din 1 32 }  { real_arr16_2_q0 mem_dout 0 32 } } }
	imag_arr16_2 { ap_memory {  { imag_arr16_2_address0 mem_address 1 2 }  { imag_arr16_2_ce0 mem_ce 1 1 }  { imag_arr16_2_we0 mem_we 1 1 }  { imag_arr16_2_d0 mem_din 1 32 }  { imag_arr16_2_q0 mem_dout 0 32 } } }
	real_arr16_3 { ap_memory {  { real_arr16_3_address0 mem_address 1 2 }  { real_arr16_3_ce0 mem_ce 1 1 }  { real_arr16_3_we0 mem_we 1 1 }  { real_arr16_3_d0 mem_din 1 32 }  { real_arr16_3_q0 mem_dout 0 32 } } }
	imag_arr16_3 { ap_memory {  { imag_arr16_3_address0 mem_address 1 2 }  { imag_arr16_3_ce0 mem_ce 1 1 }  { imag_arr16_3_we0 mem_we 1 1 }  { imag_arr16_3_d0 mem_din 1 32 }  { imag_arr16_3_q0 mem_dout 0 32 } } }
	imag_arr8_1 { ap_ovld {  { imag_arr8_1_i in_data 0 32 }  { imag_arr8_1_o out_data 1 32 }  { imag_arr8_1_o_ap_vld out_vld 1 1 } } }
	real_arr8_1 { ap_ovld {  { real_arr8_1_i in_data 0 32 }  { real_arr8_1_o out_data 1 32 }  { real_arr8_1_o_ap_vld out_vld 1 1 } } }
	imag_arr8_0 { ap_ovld {  { imag_arr8_0_i in_data 0 32 }  { imag_arr8_0_o out_data 1 32 }  { imag_arr8_0_o_ap_vld out_vld 1 1 } } }
	real_arr8_0 { ap_ovld {  { real_arr8_0_i in_data 0 32 }  { real_arr8_0_o out_data 1 32 }  { real_arr8_0_o_ap_vld out_vld 1 1 } } }
	real_arr8_2 { ap_ovld {  { real_arr8_2_i in_data 0 32 }  { real_arr8_2_o out_data 1 32 }  { real_arr8_2_o_ap_vld out_vld 1 1 } } }
	real_arr8_4 { ap_ovld {  { real_arr8_4_i in_data 0 32 }  { real_arr8_4_o out_data 1 32 }  { real_arr8_4_o_ap_vld out_vld 1 1 } } }
	real_arr8_6 { ap_ovld {  { real_arr8_6_i in_data 0 32 }  { real_arr8_6_o out_data 1 32 }  { real_arr8_6_o_ap_vld out_vld 1 1 } } }
	imag_arr8_2 { ap_ovld {  { imag_arr8_2_i in_data 0 32 }  { imag_arr8_2_o out_data 1 32 }  { imag_arr8_2_o_ap_vld out_vld 1 1 } } }
	imag_arr8_4 { ap_ovld {  { imag_arr8_4_i in_data 0 32 }  { imag_arr8_4_o out_data 1 32 }  { imag_arr8_4_o_ap_vld out_vld 1 1 } } }
	imag_arr8_6 { ap_ovld {  { imag_arr8_6_i in_data 0 32 }  { imag_arr8_6_o out_data 1 32 }  { imag_arr8_6_o_ap_vld out_vld 1 1 } } }
	real_arr8_3 { ap_ovld {  { real_arr8_3_i in_data 0 32 }  { real_arr8_3_o out_data 1 32 }  { real_arr8_3_o_ap_vld out_vld 1 1 } } }
	real_arr8_5 { ap_ovld {  { real_arr8_5_i in_data 0 32 }  { real_arr8_5_o out_data 1 32 }  { real_arr8_5_o_ap_vld out_vld 1 1 } } }
	real_arr8_7 { ap_ovld {  { real_arr8_7_i in_data 0 32 }  { real_arr8_7_o out_data 1 32 }  { real_arr8_7_o_ap_vld out_vld 1 1 } } }
	imag_arr8_3 { ap_ovld {  { imag_arr8_3_i in_data 0 32 }  { imag_arr8_3_o out_data 1 32 }  { imag_arr8_3_o_ap_vld out_vld 1 1 } } }
	imag_arr8_5 { ap_ovld {  { imag_arr8_5_i in_data 0 32 }  { imag_arr8_5_o out_data 1 32 }  { imag_arr8_5_o_ap_vld out_vld 1 1 } } }
	imag_arr8_7 { ap_ovld {  { imag_arr8_7_i in_data 0 32 }  { imag_arr8_7_o out_data 1 32 }  { imag_arr8_7_o_ap_vld out_vld 1 1 } } }
	imag_arr4_0 { ap_ovld {  { imag_arr4_0_i in_data 0 32 }  { imag_arr4_0_o out_data 1 32 }  { imag_arr4_0_o_ap_vld out_vld 1 1 } } }
	real_arr4_0 { ap_ovld {  { real_arr4_0_i in_data 0 32 }  { real_arr4_0_o out_data 1 32 }  { real_arr4_0_o_ap_vld out_vld 1 1 } } }
	real_arr4_1 { ap_ovld {  { real_arr4_1_i in_data 0 32 }  { real_arr4_1_o out_data 1 32 }  { real_arr4_1_o_ap_vld out_vld 1 1 } } }
	real_arr4_2 { ap_ovld {  { real_arr4_2_i in_data 0 32 }  { real_arr4_2_o out_data 1 32 }  { real_arr4_2_o_ap_vld out_vld 1 1 } } }
	real_arr4_3 { ap_ovld {  { real_arr4_3_i in_data 0 32 }  { real_arr4_3_o out_data 1 32 }  { real_arr4_3_o_ap_vld out_vld 1 1 } } }
	imag_arr4_1 { ap_ovld {  { imag_arr4_1_i in_data 0 32 }  { imag_arr4_1_o out_data 1 32 }  { imag_arr4_1_o_ap_vld out_vld 1 1 } } }
	imag_arr4_2 { ap_ovld {  { imag_arr4_2_i in_data 0 32 }  { imag_arr4_2_o out_data 1 32 }  { imag_arr4_2_o_ap_vld out_vld 1 1 } } }
	imag_arr4_3 { ap_ovld {  { imag_arr4_3_i in_data 0 32 }  { imag_arr4_3_o out_data 1 32 }  { imag_arr4_3_o_ap_vld out_vld 1 1 } } }
}
