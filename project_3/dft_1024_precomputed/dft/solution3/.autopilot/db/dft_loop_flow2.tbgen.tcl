set moduleName dft_loop_flow2
set isCombinational 0
set isDatapathOnly 0
set isPipelined 1
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set pipeII 1
set pipeLatency 4
set C_modelName {dft_loop_flow2}
set C_modelType { void 0 }
set C_modelArgList { 
	{ real_arr4_0 float 32 regular {pointer 0} {global 0}  }
	{ real_arr4_1 float 32 regular {pointer 0} {global 0}  }
	{ real_arr2_0 float 32 regular {pointer 1} {global 1}  }
	{ imag_arr4_0 float 32 regular {pointer 0} {global 0}  }
	{ imag_arr4_1 float 32 regular {pointer 0} {global 0}  }
	{ imag_arr2_0 float 32 regular {pointer 1} {global 1}  }
	{ real_arr4_2 float 32 regular {pointer 0} {global 0}  }
	{ real_arr4_3 float 32 regular {pointer 0} {global 0}  }
	{ real_arr2_1 float 32 regular {pointer 1} {global 1}  }
	{ imag_arr4_2 float 32 regular {pointer 0} {global 0}  }
	{ imag_arr4_3 float 32 regular {pointer 0} {global 0}  }
	{ imag_arr2_1 float 32 regular {pointer 1} {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "real_arr4_0", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr4_1", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr2_0", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr2","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr4_0", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr4_1", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr2_0", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr2","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 0,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr4_2", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 2,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr4_3", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 3,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr2_1", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr2","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr4_2", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 2,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr4_3", "interface" : "wire", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr4","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 3,"step" : 2}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr2_1", "interface" : "wire", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr2","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 1,"step" : 2}]}]}], "extern" : 0} ]}
# RTL Port declarations: 
set portNum 23
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ ap_ce sc_in sc_logic 1 ce -1 } 
	{ real_arr4_0 sc_in sc_lv 32 signal 0 } 
	{ real_arr4_1 sc_in sc_lv 32 signal 1 } 
	{ real_arr2_0 sc_out sc_lv 32 signal 2 } 
	{ real_arr2_0_ap_vld sc_out sc_logic 1 outvld 2 } 
	{ imag_arr4_0 sc_in sc_lv 32 signal 3 } 
	{ imag_arr4_1 sc_in sc_lv 32 signal 4 } 
	{ imag_arr2_0 sc_out sc_lv 32 signal 5 } 
	{ imag_arr2_0_ap_vld sc_out sc_logic 1 outvld 5 } 
	{ real_arr4_2 sc_in sc_lv 32 signal 6 } 
	{ real_arr4_3 sc_in sc_lv 32 signal 7 } 
	{ real_arr2_1 sc_out sc_lv 32 signal 8 } 
	{ real_arr2_1_ap_vld sc_out sc_logic 1 outvld 8 } 
	{ imag_arr4_2 sc_in sc_lv 32 signal 9 } 
	{ imag_arr4_3 sc_in sc_lv 32 signal 10 } 
	{ imag_arr2_1 sc_out sc_lv 32 signal 11 } 
	{ imag_arr2_1_ap_vld sc_out sc_logic 1 outvld 11 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "ap_ce", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "ce", "bundle":{"name": "ap_ce", "role": "default" }} , 
 	{ "name": "real_arr4_0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_0", "role": "default" }} , 
 	{ "name": "real_arr4_1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_1", "role": "default" }} , 
 	{ "name": "real_arr2_0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr2_0", "role": "default" }} , 
 	{ "name": "real_arr2_0_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr2_0", "role": "ap_vld" }} , 
 	{ "name": "imag_arr4_0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_0", "role": "default" }} , 
 	{ "name": "imag_arr4_1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_1", "role": "default" }} , 
 	{ "name": "imag_arr2_0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr2_0", "role": "default" }} , 
 	{ "name": "imag_arr2_0_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr2_0", "role": "ap_vld" }} , 
 	{ "name": "real_arr4_2", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_2", "role": "default" }} , 
 	{ "name": "real_arr4_3", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr4_3", "role": "default" }} , 
 	{ "name": "real_arr2_1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr2_1", "role": "default" }} , 
 	{ "name": "real_arr2_1_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "real_arr2_1", "role": "ap_vld" }} , 
 	{ "name": "imag_arr4_2", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_2", "role": "default" }} , 
 	{ "name": "imag_arr4_3", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr4_3", "role": "default" }} , 
 	{ "name": "imag_arr2_1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr2_1", "role": "default" }} , 
 	{ "name": "imag_arr2_1_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "imag_arr2_1", "role": "ap_vld" }}  ]}
set Spec2ImplPortList { 
	real_arr4_0 { ap_none {  { real_arr4_0 in_data 0 32 } } }
	real_arr4_1 { ap_none {  { real_arr4_1 in_data 0 32 } } }
	real_arr2_0 { ap_vld {  { real_arr2_0 out_data 1 32 }  { real_arr2_0_ap_vld out_vld 1 1 } } }
	imag_arr4_0 { ap_none {  { imag_arr4_0 in_data 0 32 } } }
	imag_arr4_1 { ap_none {  { imag_arr4_1 in_data 0 32 } } }
	imag_arr2_0 { ap_vld {  { imag_arr2_0 out_data 1 32 }  { imag_arr2_0_ap_vld out_vld 1 1 } } }
	real_arr4_2 { ap_none {  { real_arr4_2 in_data 0 32 } } }
	real_arr4_3 { ap_none {  { real_arr4_3 in_data 0 32 } } }
	real_arr2_1 { ap_vld {  { real_arr2_1 out_data 1 32 }  { real_arr2_1_ap_vld out_vld 1 1 } } }
	imag_arr4_2 { ap_none {  { imag_arr4_2 in_data 0 32 } } }
	imag_arr4_3 { ap_none {  { imag_arr4_3 in_data 0 32 } } }
	imag_arr2_1 { ap_vld {  { imag_arr2_1 out_data 1 32 }  { imag_arr2_1_ap_vld out_vld 1 1 } } }
}
