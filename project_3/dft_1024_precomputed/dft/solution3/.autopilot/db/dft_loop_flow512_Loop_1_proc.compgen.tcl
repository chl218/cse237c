# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 110 \
    name real_arr1024_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr1024_0 \
    op interface \
    ports { real_arr1024_0_address0 { O 8 vector } real_arr1024_0_ce0 { O 1 bit } real_arr1024_0_q0 { I 32 vector } real_arr1024_0_address1 { O 8 vector } real_arr1024_0_ce1 { O 1 bit } real_arr1024_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr1024_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 111 \
    name real_arr1024_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr1024_1 \
    op interface \
    ports { real_arr1024_1_address0 { O 8 vector } real_arr1024_1_ce0 { O 1 bit } real_arr1024_1_q0 { I 32 vector } real_arr1024_1_address1 { O 8 vector } real_arr1024_1_ce1 { O 1 bit } real_arr1024_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr1024_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 112 \
    name real_arr512_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr512_0 \
    op interface \
    ports { real_arr512_0_address0 { O 7 vector } real_arr512_0_ce0 { O 1 bit } real_arr512_0_we0 { O 1 bit } real_arr512_0_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr512_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 113 \
    name imag_arr1024_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr1024_0 \
    op interface \
    ports { imag_arr1024_0_address0 { O 8 vector } imag_arr1024_0_ce0 { O 1 bit } imag_arr1024_0_q0 { I 32 vector } imag_arr1024_0_address1 { O 8 vector } imag_arr1024_0_ce1 { O 1 bit } imag_arr1024_0_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr1024_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 114 \
    name imag_arr1024_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr1024_1 \
    op interface \
    ports { imag_arr1024_1_address0 { O 8 vector } imag_arr1024_1_ce0 { O 1 bit } imag_arr1024_1_q0 { I 32 vector } imag_arr1024_1_address1 { O 8 vector } imag_arr1024_1_ce1 { O 1 bit } imag_arr1024_1_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr1024_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 115 \
    name imag_arr512_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr512_0 \
    op interface \
    ports { imag_arr512_0_address0 { O 7 vector } imag_arr512_0_ce0 { O 1 bit } imag_arr512_0_we0 { O 1 bit } imag_arr512_0_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr512_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 116 \
    name real_arr1024_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr1024_2 \
    op interface \
    ports { real_arr1024_2_address0 { O 8 vector } real_arr1024_2_ce0 { O 1 bit } real_arr1024_2_q0 { I 32 vector } real_arr1024_2_address1 { O 8 vector } real_arr1024_2_ce1 { O 1 bit } real_arr1024_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr1024_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 117 \
    name real_arr1024_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr1024_3 \
    op interface \
    ports { real_arr1024_3_address0 { O 8 vector } real_arr1024_3_ce0 { O 1 bit } real_arr1024_3_q0 { I 32 vector } real_arr1024_3_address1 { O 8 vector } real_arr1024_3_ce1 { O 1 bit } real_arr1024_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr1024_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 118 \
    name real_arr512_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr512_1 \
    op interface \
    ports { real_arr512_1_address0 { O 7 vector } real_arr512_1_ce0 { O 1 bit } real_arr512_1_we0 { O 1 bit } real_arr512_1_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr512_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 119 \
    name imag_arr1024_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr1024_2 \
    op interface \
    ports { imag_arr1024_2_address0 { O 8 vector } imag_arr1024_2_ce0 { O 1 bit } imag_arr1024_2_q0 { I 32 vector } imag_arr1024_2_address1 { O 8 vector } imag_arr1024_2_ce1 { O 1 bit } imag_arr1024_2_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr1024_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 120 \
    name imag_arr1024_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr1024_3 \
    op interface \
    ports { imag_arr1024_3_address0 { O 8 vector } imag_arr1024_3_ce0 { O 1 bit } imag_arr1024_3_q0 { I 32 vector } imag_arr1024_3_address1 { O 8 vector } imag_arr1024_3_ce1 { O 1 bit } imag_arr1024_3_q1 { I 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr1024_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 121 \
    name imag_arr512_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr512_1 \
    op interface \
    ports { imag_arr512_1_address0 { O 7 vector } imag_arr512_1_ce0 { O 1 bit } imag_arr512_1_we0 { O 1 bit } imag_arr512_1_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr512_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 122 \
    name real_arr512_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr512_2 \
    op interface \
    ports { real_arr512_2_address0 { O 7 vector } real_arr512_2_ce0 { O 1 bit } real_arr512_2_we0 { O 1 bit } real_arr512_2_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr512_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 123 \
    name imag_arr512_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr512_2 \
    op interface \
    ports { imag_arr512_2_address0 { O 7 vector } imag_arr512_2_ce0 { O 1 bit } imag_arr512_2_we0 { O 1 bit } imag_arr512_2_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr512_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 124 \
    name real_arr512_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr512_3 \
    op interface \
    ports { real_arr512_3_address0 { O 7 vector } real_arr512_3_ce0 { O 1 bit } real_arr512_3_we0 { O 1 bit } real_arr512_3_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr512_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 125 \
    name imag_arr512_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr512_3 \
    op interface \
    ports { imag_arr512_3_address0 { O 7 vector } imag_arr512_3_ce0 { O 1 bit } imag_arr512_3_we0 { O 1 bit } imag_arr512_3_d0 { O 32 vector } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr512_3'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } ap_continue { I 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


