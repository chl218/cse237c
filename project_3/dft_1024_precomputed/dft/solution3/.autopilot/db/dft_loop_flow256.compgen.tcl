# This script segment is generated automatically by AutoPilot

# clear list
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_begin
    cg_default_interface_gen_bundle_begin
    AESL_LIB_XILADAPTER::native_axis_begin
}

# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 166 \
    name real_arr512_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr512_0 \
    op interface \
    ports { real_arr512_0_address0 { O 7 vector } real_arr512_0_ce0 { O 1 bit } real_arr512_0_d0 { O 32 vector } real_arr512_0_q0 { I 32 vector } real_arr512_0_we0 { O 1 bit } real_arr512_0_address1 { O 7 vector } real_arr512_0_ce1 { O 1 bit } real_arr512_0_d1 { O 32 vector } real_arr512_0_q1 { I 32 vector } real_arr512_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr512_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 167 \
    name real_arr512_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr512_1 \
    op interface \
    ports { real_arr512_1_address0 { O 7 vector } real_arr512_1_ce0 { O 1 bit } real_arr512_1_d0 { O 32 vector } real_arr512_1_q0 { I 32 vector } real_arr512_1_we0 { O 1 bit } real_arr512_1_address1 { O 7 vector } real_arr512_1_ce1 { O 1 bit } real_arr512_1_d1 { O 32 vector } real_arr512_1_q1 { I 32 vector } real_arr512_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr512_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 168 \
    name real_arr256_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr256_0 \
    op interface \
    ports { real_arr256_0_address0 { O 6 vector } real_arr256_0_ce0 { O 1 bit } real_arr256_0_d0 { O 32 vector } real_arr256_0_q0 { I 32 vector } real_arr256_0_we0 { O 1 bit } real_arr256_0_address1 { O 6 vector } real_arr256_0_ce1 { O 1 bit } real_arr256_0_d1 { O 32 vector } real_arr256_0_q1 { I 32 vector } real_arr256_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr256_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 169 \
    name imag_arr512_0 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr512_0 \
    op interface \
    ports { imag_arr512_0_address0 { O 7 vector } imag_arr512_0_ce0 { O 1 bit } imag_arr512_0_d0 { O 32 vector } imag_arr512_0_q0 { I 32 vector } imag_arr512_0_we0 { O 1 bit } imag_arr512_0_address1 { O 7 vector } imag_arr512_0_ce1 { O 1 bit } imag_arr512_0_d1 { O 32 vector } imag_arr512_0_q1 { I 32 vector } imag_arr512_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr512_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 170 \
    name imag_arr512_1 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr512_1 \
    op interface \
    ports { imag_arr512_1_address0 { O 7 vector } imag_arr512_1_ce0 { O 1 bit } imag_arr512_1_d0 { O 32 vector } imag_arr512_1_q0 { I 32 vector } imag_arr512_1_we0 { O 1 bit } imag_arr512_1_address1 { O 7 vector } imag_arr512_1_ce1 { O 1 bit } imag_arr512_1_d1 { O 32 vector } imag_arr512_1_q1 { I 32 vector } imag_arr512_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr512_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 171 \
    name imag_arr256_0 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr256_0 \
    op interface \
    ports { imag_arr256_0_address0 { O 6 vector } imag_arr256_0_ce0 { O 1 bit } imag_arr256_0_d0 { O 32 vector } imag_arr256_0_q0 { I 32 vector } imag_arr256_0_we0 { O 1 bit } imag_arr256_0_address1 { O 6 vector } imag_arr256_0_ce1 { O 1 bit } imag_arr256_0_d1 { O 32 vector } imag_arr256_0_q1 { I 32 vector } imag_arr256_0_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr256_0'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 172 \
    name real_arr512_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr512_2 \
    op interface \
    ports { real_arr512_2_address0 { O 7 vector } real_arr512_2_ce0 { O 1 bit } real_arr512_2_d0 { O 32 vector } real_arr512_2_q0 { I 32 vector } real_arr512_2_we0 { O 1 bit } real_arr512_2_address1 { O 7 vector } real_arr512_2_ce1 { O 1 bit } real_arr512_2_d1 { O 32 vector } real_arr512_2_q1 { I 32 vector } real_arr512_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr512_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 173 \
    name real_arr512_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename real_arr512_3 \
    op interface \
    ports { real_arr512_3_address0 { O 7 vector } real_arr512_3_ce0 { O 1 bit } real_arr512_3_d0 { O 32 vector } real_arr512_3_q0 { I 32 vector } real_arr512_3_we0 { O 1 bit } real_arr512_3_address1 { O 7 vector } real_arr512_3_ce1 { O 1 bit } real_arr512_3_d1 { O 32 vector } real_arr512_3_q1 { I 32 vector } real_arr512_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr512_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 174 \
    name real_arr256_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr256_1 \
    op interface \
    ports { real_arr256_1_address0 { O 6 vector } real_arr256_1_ce0 { O 1 bit } real_arr256_1_d0 { O 32 vector } real_arr256_1_q0 { I 32 vector } real_arr256_1_we0 { O 1 bit } real_arr256_1_address1 { O 6 vector } real_arr256_1_ce1 { O 1 bit } real_arr256_1_d1 { O 32 vector } real_arr256_1_q1 { I 32 vector } real_arr256_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr256_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 175 \
    name imag_arr512_2 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr512_2 \
    op interface \
    ports { imag_arr512_2_address0 { O 7 vector } imag_arr512_2_ce0 { O 1 bit } imag_arr512_2_d0 { O 32 vector } imag_arr512_2_q0 { I 32 vector } imag_arr512_2_we0 { O 1 bit } imag_arr512_2_address1 { O 7 vector } imag_arr512_2_ce1 { O 1 bit } imag_arr512_2_d1 { O 32 vector } imag_arr512_2_q1 { I 32 vector } imag_arr512_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr512_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 176 \
    name imag_arr512_3 \
    reset_level 1 \
    sync_rst true \
    dir I \
    corename imag_arr512_3 \
    op interface \
    ports { imag_arr512_3_address0 { O 7 vector } imag_arr512_3_ce0 { O 1 bit } imag_arr512_3_d0 { O 32 vector } imag_arr512_3_q0 { I 32 vector } imag_arr512_3_we0 { O 1 bit } imag_arr512_3_address1 { O 7 vector } imag_arr512_3_ce1 { O 1 bit } imag_arr512_3_d1 { O 32 vector } imag_arr512_3_q1 { I 32 vector } imag_arr512_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr512_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 177 \
    name imag_arr256_1 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr256_1 \
    op interface \
    ports { imag_arr256_1_address0 { O 6 vector } imag_arr256_1_ce0 { O 1 bit } imag_arr256_1_d0 { O 32 vector } imag_arr256_1_q0 { I 32 vector } imag_arr256_1_we0 { O 1 bit } imag_arr256_1_address1 { O 6 vector } imag_arr256_1_ce1 { O 1 bit } imag_arr256_1_d1 { O 32 vector } imag_arr256_1_q1 { I 32 vector } imag_arr256_1_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr256_1'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 178 \
    name real_arr256_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr256_2 \
    op interface \
    ports { real_arr256_2_address0 { O 6 vector } real_arr256_2_ce0 { O 1 bit } real_arr256_2_d0 { O 32 vector } real_arr256_2_q0 { I 32 vector } real_arr256_2_we0 { O 1 bit } real_arr256_2_address1 { O 6 vector } real_arr256_2_ce1 { O 1 bit } real_arr256_2_d1 { O 32 vector } real_arr256_2_q1 { I 32 vector } real_arr256_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr256_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 179 \
    name imag_arr256_2 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr256_2 \
    op interface \
    ports { imag_arr256_2_address0 { O 6 vector } imag_arr256_2_ce0 { O 1 bit } imag_arr256_2_d0 { O 32 vector } imag_arr256_2_q0 { I 32 vector } imag_arr256_2_we0 { O 1 bit } imag_arr256_2_address1 { O 6 vector } imag_arr256_2_ce1 { O 1 bit } imag_arr256_2_d1 { O 32 vector } imag_arr256_2_q1 { I 32 vector } imag_arr256_2_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr256_2'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 180 \
    name real_arr256_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename real_arr256_3 \
    op interface \
    ports { real_arr256_3_address0 { O 6 vector } real_arr256_3_ce0 { O 1 bit } real_arr256_3_d0 { O 32 vector } real_arr256_3_q0 { I 32 vector } real_arr256_3_we0 { O 1 bit } real_arr256_3_address1 { O 6 vector } real_arr256_3_ce1 { O 1 bit } real_arr256_3_d1 { O 32 vector } real_arr256_3_q1 { I 32 vector } real_arr256_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'real_arr256_3'"
}
}


# XIL_BRAM:
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc ::AESL_LIB_XILADAPTER::xil_bram_gen] == "::AESL_LIB_XILADAPTER::xil_bram_gen"} {
eval "::AESL_LIB_XILADAPTER::xil_bram_gen { \
    id 181 \
    name imag_arr256_3 \
    reset_level 1 \
    sync_rst true \
    dir O \
    corename imag_arr256_3 \
    op interface \
    ports { imag_arr256_3_address0 { O 6 vector } imag_arr256_3_ce0 { O 1 bit } imag_arr256_3_d0 { O 32 vector } imag_arr256_3_q0 { I 32 vector } imag_arr256_3_we0 { O 1 bit } imag_arr256_3_address1 { O 6 vector } imag_arr256_3_ce1 { O 1 bit } imag_arr256_3_d1 { O 32 vector } imag_arr256_3_q1 { I 32 vector } imag_arr256_3_we1 { O 1 bit } } \
} "
} else {
puts "@W \[IMPL-110\] Cannot find bus interface model in the library. Ignored generation of bus interface for 'imag_arr256_3'"
}
}


# Direct connection:
if {${::AESL::PGuard_autoexp_gen}} {
eval "cg_default_interface_gen_dc { \
    id -1 \
    name ap_ctrl \
    type ap_ctrl \
    reset_level 1 \
    sync_rst true \
    corename ap_ctrl \
    op interface \
    ports { ap_start { I 1 bit } ap_ready { O 1 bit } ap_done { O 1 bit } ap_idle { O 1 bit } } \
} "
}


# Adapter definition:
set PortName ap_clk
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_clock] == "cg_default_interface_gen_clock"} {
eval "cg_default_interface_gen_clock { \
    id -2 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_clk \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-113\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}


# Adapter definition:
set PortName ap_rst
set DataWd 1 
if {${::AESL::PGuard_autoexp_gen}} {
if {[info proc cg_default_interface_gen_reset] == "cg_default_interface_gen_reset"} {
eval "cg_default_interface_gen_reset { \
    id -3 \
    name ${PortName} \
    reset_level 1 \
    sync_rst true \
    corename apif_ap_rst \
    data_wd ${DataWd} \
    op interface \
}"
} else {
puts "@W \[IMPL-114\] Cannot find bus interface model in the library. Ignored generation of bus interface for '${PortName}'"
}
}



# merge
if {${::AESL::PGuard_autoexp_gen}} {
    cg_default_interface_gen_dc_end
    cg_default_interface_gen_bundle_end
    AESL_LIB_XILADAPTER::native_axis_end
}


