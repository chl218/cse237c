set moduleName dft_loop_flow128
set isCombinational 0
set isDatapathOnly 0
set isPipelined 1
set pipeline_type dataflow
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {dft_loop_flow128}
set C_modelType { void 0 }
set C_modelArgList { 
	{ real_arr256_0 float 32 regular {array 64 { 1 1 } 1 1 } {global 0}  }
	{ real_arr256_1 float 32 regular {array 64 { 1 1 } 1 1 } {global 0}  }
	{ real_arr128_0 float 32 regular {array 32 { 0 3 } 0 1 } {global 1}  }
	{ imag_arr256_0 float 32 regular {array 64 { 1 1 } 1 1 } {global 0}  }
	{ imag_arr256_1 float 32 regular {array 64 { 1 1 } 1 1 } {global 0}  }
	{ imag_arr128_0 float 32 regular {array 32 { 0 3 } 0 1 } {global 1}  }
	{ real_arr256_2 float 32 regular {array 64 { 1 1 } 1 1 } {global 0}  }
	{ real_arr256_3 float 32 regular {array 64 { 1 1 } 1 1 } {global 0}  }
	{ real_arr128_1 float 32 regular {array 32 { 0 3 } 0 1 } {global 1}  }
	{ imag_arr256_2 float 32 regular {array 64 { 1 1 } 1 1 } {global 0}  }
	{ imag_arr256_3 float 32 regular {array 64 { 1 1 } 1 1 } {global 0}  }
	{ imag_arr128_1 float 32 regular {array 32 { 0 3 } 0 1 } {global 1}  }
	{ real_arr128_2 float 32 regular {array 32 { 0 3 } 0 1 } {global 1}  }
	{ imag_arr128_2 float 32 regular {array 32 { 0 3 } 0 1 } {global 1}  }
	{ real_arr128_3 float 32 regular {array 32 { 0 3 } 0 1 } {global 1}  }
	{ imag_arr128_3 float 32 regular {array 32 { 0 3 } 0 1 } {global 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "real_arr256_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 252,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr256_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 253,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr128_0", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 124,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr256_0", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 252,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr256_1", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 253,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr128_0", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 0,"up" : 124,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr256_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 254,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr256_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 255,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr128_1", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 125,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr256_2", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 254,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr256_3", "interface" : "memory", "bitwidth" : 32, "direction" : "READONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr256","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 255,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr128_1", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 1,"up" : 125,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr128_2", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 126,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr128_2", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 2,"up" : 126,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "real_arr128_3", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "real_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 127,"step" : 4}]}]}], "extern" : 0} , 
 	{ "Name" : "imag_arr128_3", "interface" : "memory", "bitwidth" : 32, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":31,"cElement": [{"cName": "imag_arr128","cData": "float","bit_use": { "low": 0,"up": 31},"cArray": [{"low" : 3,"up" : 127,"step" : 4}]}]}], "extern" : 0} ]}
# RTL Port declarations: 
set portNum 166
set portList { 
	{ real_arr256_0_address0 sc_out sc_lv 6 signal 0 } 
	{ real_arr256_0_ce0 sc_out sc_logic 1 signal 0 } 
	{ real_arr256_0_d0 sc_out sc_lv 32 signal 0 } 
	{ real_arr256_0_q0 sc_in sc_lv 32 signal 0 } 
	{ real_arr256_0_we0 sc_out sc_logic 1 signal 0 } 
	{ real_arr256_0_address1 sc_out sc_lv 6 signal 0 } 
	{ real_arr256_0_ce1 sc_out sc_logic 1 signal 0 } 
	{ real_arr256_0_d1 sc_out sc_lv 32 signal 0 } 
	{ real_arr256_0_q1 sc_in sc_lv 32 signal 0 } 
	{ real_arr256_0_we1 sc_out sc_logic 1 signal 0 } 
	{ real_arr256_1_address0 sc_out sc_lv 6 signal 1 } 
	{ real_arr256_1_ce0 sc_out sc_logic 1 signal 1 } 
	{ real_arr256_1_d0 sc_out sc_lv 32 signal 1 } 
	{ real_arr256_1_q0 sc_in sc_lv 32 signal 1 } 
	{ real_arr256_1_we0 sc_out sc_logic 1 signal 1 } 
	{ real_arr256_1_address1 sc_out sc_lv 6 signal 1 } 
	{ real_arr256_1_ce1 sc_out sc_logic 1 signal 1 } 
	{ real_arr256_1_d1 sc_out sc_lv 32 signal 1 } 
	{ real_arr256_1_q1 sc_in sc_lv 32 signal 1 } 
	{ real_arr256_1_we1 sc_out sc_logic 1 signal 1 } 
	{ real_arr128_0_address0 sc_out sc_lv 5 signal 2 } 
	{ real_arr128_0_ce0 sc_out sc_logic 1 signal 2 } 
	{ real_arr128_0_d0 sc_out sc_lv 32 signal 2 } 
	{ real_arr128_0_q0 sc_in sc_lv 32 signal 2 } 
	{ real_arr128_0_we0 sc_out sc_logic 1 signal 2 } 
	{ real_arr128_0_address1 sc_out sc_lv 5 signal 2 } 
	{ real_arr128_0_ce1 sc_out sc_logic 1 signal 2 } 
	{ real_arr128_0_d1 sc_out sc_lv 32 signal 2 } 
	{ real_arr128_0_q1 sc_in sc_lv 32 signal 2 } 
	{ real_arr128_0_we1 sc_out sc_logic 1 signal 2 } 
	{ imag_arr256_0_address0 sc_out sc_lv 6 signal 3 } 
	{ imag_arr256_0_ce0 sc_out sc_logic 1 signal 3 } 
	{ imag_arr256_0_d0 sc_out sc_lv 32 signal 3 } 
	{ imag_arr256_0_q0 sc_in sc_lv 32 signal 3 } 
	{ imag_arr256_0_we0 sc_out sc_logic 1 signal 3 } 
	{ imag_arr256_0_address1 sc_out sc_lv 6 signal 3 } 
	{ imag_arr256_0_ce1 sc_out sc_logic 1 signal 3 } 
	{ imag_arr256_0_d1 sc_out sc_lv 32 signal 3 } 
	{ imag_arr256_0_q1 sc_in sc_lv 32 signal 3 } 
	{ imag_arr256_0_we1 sc_out sc_logic 1 signal 3 } 
	{ imag_arr256_1_address0 sc_out sc_lv 6 signal 4 } 
	{ imag_arr256_1_ce0 sc_out sc_logic 1 signal 4 } 
	{ imag_arr256_1_d0 sc_out sc_lv 32 signal 4 } 
	{ imag_arr256_1_q0 sc_in sc_lv 32 signal 4 } 
	{ imag_arr256_1_we0 sc_out sc_logic 1 signal 4 } 
	{ imag_arr256_1_address1 sc_out sc_lv 6 signal 4 } 
	{ imag_arr256_1_ce1 sc_out sc_logic 1 signal 4 } 
	{ imag_arr256_1_d1 sc_out sc_lv 32 signal 4 } 
	{ imag_arr256_1_q1 sc_in sc_lv 32 signal 4 } 
	{ imag_arr256_1_we1 sc_out sc_logic 1 signal 4 } 
	{ imag_arr128_0_address0 sc_out sc_lv 5 signal 5 } 
	{ imag_arr128_0_ce0 sc_out sc_logic 1 signal 5 } 
	{ imag_arr128_0_d0 sc_out sc_lv 32 signal 5 } 
	{ imag_arr128_0_q0 sc_in sc_lv 32 signal 5 } 
	{ imag_arr128_0_we0 sc_out sc_logic 1 signal 5 } 
	{ imag_arr128_0_address1 sc_out sc_lv 5 signal 5 } 
	{ imag_arr128_0_ce1 sc_out sc_logic 1 signal 5 } 
	{ imag_arr128_0_d1 sc_out sc_lv 32 signal 5 } 
	{ imag_arr128_0_q1 sc_in sc_lv 32 signal 5 } 
	{ imag_arr128_0_we1 sc_out sc_logic 1 signal 5 } 
	{ real_arr256_2_address0 sc_out sc_lv 6 signal 6 } 
	{ real_arr256_2_ce0 sc_out sc_logic 1 signal 6 } 
	{ real_arr256_2_d0 sc_out sc_lv 32 signal 6 } 
	{ real_arr256_2_q0 sc_in sc_lv 32 signal 6 } 
	{ real_arr256_2_we0 sc_out sc_logic 1 signal 6 } 
	{ real_arr256_2_address1 sc_out sc_lv 6 signal 6 } 
	{ real_arr256_2_ce1 sc_out sc_logic 1 signal 6 } 
	{ real_arr256_2_d1 sc_out sc_lv 32 signal 6 } 
	{ real_arr256_2_q1 sc_in sc_lv 32 signal 6 } 
	{ real_arr256_2_we1 sc_out sc_logic 1 signal 6 } 
	{ real_arr256_3_address0 sc_out sc_lv 6 signal 7 } 
	{ real_arr256_3_ce0 sc_out sc_logic 1 signal 7 } 
	{ real_arr256_3_d0 sc_out sc_lv 32 signal 7 } 
	{ real_arr256_3_q0 sc_in sc_lv 32 signal 7 } 
	{ real_arr256_3_we0 sc_out sc_logic 1 signal 7 } 
	{ real_arr256_3_address1 sc_out sc_lv 6 signal 7 } 
	{ real_arr256_3_ce1 sc_out sc_logic 1 signal 7 } 
	{ real_arr256_3_d1 sc_out sc_lv 32 signal 7 } 
	{ real_arr256_3_q1 sc_in sc_lv 32 signal 7 } 
	{ real_arr256_3_we1 sc_out sc_logic 1 signal 7 } 
	{ real_arr128_1_address0 sc_out sc_lv 5 signal 8 } 
	{ real_arr128_1_ce0 sc_out sc_logic 1 signal 8 } 
	{ real_arr128_1_d0 sc_out sc_lv 32 signal 8 } 
	{ real_arr128_1_q0 sc_in sc_lv 32 signal 8 } 
	{ real_arr128_1_we0 sc_out sc_logic 1 signal 8 } 
	{ real_arr128_1_address1 sc_out sc_lv 5 signal 8 } 
	{ real_arr128_1_ce1 sc_out sc_logic 1 signal 8 } 
	{ real_arr128_1_d1 sc_out sc_lv 32 signal 8 } 
	{ real_arr128_1_q1 sc_in sc_lv 32 signal 8 } 
	{ real_arr128_1_we1 sc_out sc_logic 1 signal 8 } 
	{ imag_arr256_2_address0 sc_out sc_lv 6 signal 9 } 
	{ imag_arr256_2_ce0 sc_out sc_logic 1 signal 9 } 
	{ imag_arr256_2_d0 sc_out sc_lv 32 signal 9 } 
	{ imag_arr256_2_q0 sc_in sc_lv 32 signal 9 } 
	{ imag_arr256_2_we0 sc_out sc_logic 1 signal 9 } 
	{ imag_arr256_2_address1 sc_out sc_lv 6 signal 9 } 
	{ imag_arr256_2_ce1 sc_out sc_logic 1 signal 9 } 
	{ imag_arr256_2_d1 sc_out sc_lv 32 signal 9 } 
	{ imag_arr256_2_q1 sc_in sc_lv 32 signal 9 } 
	{ imag_arr256_2_we1 sc_out sc_logic 1 signal 9 } 
	{ imag_arr256_3_address0 sc_out sc_lv 6 signal 10 } 
	{ imag_arr256_3_ce0 sc_out sc_logic 1 signal 10 } 
	{ imag_arr256_3_d0 sc_out sc_lv 32 signal 10 } 
	{ imag_arr256_3_q0 sc_in sc_lv 32 signal 10 } 
	{ imag_arr256_3_we0 sc_out sc_logic 1 signal 10 } 
	{ imag_arr256_3_address1 sc_out sc_lv 6 signal 10 } 
	{ imag_arr256_3_ce1 sc_out sc_logic 1 signal 10 } 
	{ imag_arr256_3_d1 sc_out sc_lv 32 signal 10 } 
	{ imag_arr256_3_q1 sc_in sc_lv 32 signal 10 } 
	{ imag_arr256_3_we1 sc_out sc_logic 1 signal 10 } 
	{ imag_arr128_1_address0 sc_out sc_lv 5 signal 11 } 
	{ imag_arr128_1_ce0 sc_out sc_logic 1 signal 11 } 
	{ imag_arr128_1_d0 sc_out sc_lv 32 signal 11 } 
	{ imag_arr128_1_q0 sc_in sc_lv 32 signal 11 } 
	{ imag_arr128_1_we0 sc_out sc_logic 1 signal 11 } 
	{ imag_arr128_1_address1 sc_out sc_lv 5 signal 11 } 
	{ imag_arr128_1_ce1 sc_out sc_logic 1 signal 11 } 
	{ imag_arr128_1_d1 sc_out sc_lv 32 signal 11 } 
	{ imag_arr128_1_q1 sc_in sc_lv 32 signal 11 } 
	{ imag_arr128_1_we1 sc_out sc_logic 1 signal 11 } 
	{ real_arr128_2_address0 sc_out sc_lv 5 signal 12 } 
	{ real_arr128_2_ce0 sc_out sc_logic 1 signal 12 } 
	{ real_arr128_2_d0 sc_out sc_lv 32 signal 12 } 
	{ real_arr128_2_q0 sc_in sc_lv 32 signal 12 } 
	{ real_arr128_2_we0 sc_out sc_logic 1 signal 12 } 
	{ real_arr128_2_address1 sc_out sc_lv 5 signal 12 } 
	{ real_arr128_2_ce1 sc_out sc_logic 1 signal 12 } 
	{ real_arr128_2_d1 sc_out sc_lv 32 signal 12 } 
	{ real_arr128_2_q1 sc_in sc_lv 32 signal 12 } 
	{ real_arr128_2_we1 sc_out sc_logic 1 signal 12 } 
	{ imag_arr128_2_address0 sc_out sc_lv 5 signal 13 } 
	{ imag_arr128_2_ce0 sc_out sc_logic 1 signal 13 } 
	{ imag_arr128_2_d0 sc_out sc_lv 32 signal 13 } 
	{ imag_arr128_2_q0 sc_in sc_lv 32 signal 13 } 
	{ imag_arr128_2_we0 sc_out sc_logic 1 signal 13 } 
	{ imag_arr128_2_address1 sc_out sc_lv 5 signal 13 } 
	{ imag_arr128_2_ce1 sc_out sc_logic 1 signal 13 } 
	{ imag_arr128_2_d1 sc_out sc_lv 32 signal 13 } 
	{ imag_arr128_2_q1 sc_in sc_lv 32 signal 13 } 
	{ imag_arr128_2_we1 sc_out sc_logic 1 signal 13 } 
	{ real_arr128_3_address0 sc_out sc_lv 5 signal 14 } 
	{ real_arr128_3_ce0 sc_out sc_logic 1 signal 14 } 
	{ real_arr128_3_d0 sc_out sc_lv 32 signal 14 } 
	{ real_arr128_3_q0 sc_in sc_lv 32 signal 14 } 
	{ real_arr128_3_we0 sc_out sc_logic 1 signal 14 } 
	{ real_arr128_3_address1 sc_out sc_lv 5 signal 14 } 
	{ real_arr128_3_ce1 sc_out sc_logic 1 signal 14 } 
	{ real_arr128_3_d1 sc_out sc_lv 32 signal 14 } 
	{ real_arr128_3_q1 sc_in sc_lv 32 signal 14 } 
	{ real_arr128_3_we1 sc_out sc_logic 1 signal 14 } 
	{ imag_arr128_3_address0 sc_out sc_lv 5 signal 15 } 
	{ imag_arr128_3_ce0 sc_out sc_logic 1 signal 15 } 
	{ imag_arr128_3_d0 sc_out sc_lv 32 signal 15 } 
	{ imag_arr128_3_q0 sc_in sc_lv 32 signal 15 } 
	{ imag_arr128_3_we0 sc_out sc_logic 1 signal 15 } 
	{ imag_arr128_3_address1 sc_out sc_lv 5 signal 15 } 
	{ imag_arr128_3_ce1 sc_out sc_logic 1 signal 15 } 
	{ imag_arr128_3_d1 sc_out sc_lv 32 signal 15 } 
	{ imag_arr128_3_q1 sc_in sc_lv 32 signal 15 } 
	{ imag_arr128_3_we1 sc_out sc_logic 1 signal 15 } 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
}
set NewPortList {[ 
	{ "name": "real_arr256_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "address0" }} , 
 	{ "name": "real_arr256_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "ce0" }} , 
 	{ "name": "real_arr256_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "d0" }} , 
 	{ "name": "real_arr256_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "q0" }} , 
 	{ "name": "real_arr256_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "we0" }} , 
 	{ "name": "real_arr256_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "address1" }} , 
 	{ "name": "real_arr256_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "ce1" }} , 
 	{ "name": "real_arr256_0_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "d1" }} , 
 	{ "name": "real_arr256_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "q1" }} , 
 	{ "name": "real_arr256_0_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_0", "role": "we1" }} , 
 	{ "name": "real_arr256_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "address0" }} , 
 	{ "name": "real_arr256_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "ce0" }} , 
 	{ "name": "real_arr256_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "d0" }} , 
 	{ "name": "real_arr256_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "q0" }} , 
 	{ "name": "real_arr256_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "we0" }} , 
 	{ "name": "real_arr256_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "address1" }} , 
 	{ "name": "real_arr256_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "ce1" }} , 
 	{ "name": "real_arr256_1_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "d1" }} , 
 	{ "name": "real_arr256_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "q1" }} , 
 	{ "name": "real_arr256_1_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_1", "role": "we1" }} , 
 	{ "name": "real_arr128_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "address0" }} , 
 	{ "name": "real_arr128_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "ce0" }} , 
 	{ "name": "real_arr128_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "d0" }} , 
 	{ "name": "real_arr128_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "q0" }} , 
 	{ "name": "real_arr128_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "we0" }} , 
 	{ "name": "real_arr128_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "address1" }} , 
 	{ "name": "real_arr128_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "ce1" }} , 
 	{ "name": "real_arr128_0_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "d1" }} , 
 	{ "name": "real_arr128_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "q1" }} , 
 	{ "name": "real_arr128_0_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_0", "role": "we1" }} , 
 	{ "name": "imag_arr256_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "address0" }} , 
 	{ "name": "imag_arr256_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "ce0" }} , 
 	{ "name": "imag_arr256_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "d0" }} , 
 	{ "name": "imag_arr256_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "q0" }} , 
 	{ "name": "imag_arr256_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "we0" }} , 
 	{ "name": "imag_arr256_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "address1" }} , 
 	{ "name": "imag_arr256_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "ce1" }} , 
 	{ "name": "imag_arr256_0_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "d1" }} , 
 	{ "name": "imag_arr256_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "q1" }} , 
 	{ "name": "imag_arr256_0_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_0", "role": "we1" }} , 
 	{ "name": "imag_arr256_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "address0" }} , 
 	{ "name": "imag_arr256_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "ce0" }} , 
 	{ "name": "imag_arr256_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "d0" }} , 
 	{ "name": "imag_arr256_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "q0" }} , 
 	{ "name": "imag_arr256_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "we0" }} , 
 	{ "name": "imag_arr256_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "address1" }} , 
 	{ "name": "imag_arr256_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "ce1" }} , 
 	{ "name": "imag_arr256_1_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "d1" }} , 
 	{ "name": "imag_arr256_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "q1" }} , 
 	{ "name": "imag_arr256_1_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_1", "role": "we1" }} , 
 	{ "name": "imag_arr128_0_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "address0" }} , 
 	{ "name": "imag_arr128_0_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "ce0" }} , 
 	{ "name": "imag_arr128_0_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "d0" }} , 
 	{ "name": "imag_arr128_0_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "q0" }} , 
 	{ "name": "imag_arr128_0_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "we0" }} , 
 	{ "name": "imag_arr128_0_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "address1" }} , 
 	{ "name": "imag_arr128_0_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "ce1" }} , 
 	{ "name": "imag_arr128_0_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "d1" }} , 
 	{ "name": "imag_arr128_0_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "q1" }} , 
 	{ "name": "imag_arr128_0_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_0", "role": "we1" }} , 
 	{ "name": "real_arr256_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "address0" }} , 
 	{ "name": "real_arr256_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "ce0" }} , 
 	{ "name": "real_arr256_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "d0" }} , 
 	{ "name": "real_arr256_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "q0" }} , 
 	{ "name": "real_arr256_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "we0" }} , 
 	{ "name": "real_arr256_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "address1" }} , 
 	{ "name": "real_arr256_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "ce1" }} , 
 	{ "name": "real_arr256_2_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "d1" }} , 
 	{ "name": "real_arr256_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "q1" }} , 
 	{ "name": "real_arr256_2_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_2", "role": "we1" }} , 
 	{ "name": "real_arr256_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "address0" }} , 
 	{ "name": "real_arr256_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "ce0" }} , 
 	{ "name": "real_arr256_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "d0" }} , 
 	{ "name": "real_arr256_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "q0" }} , 
 	{ "name": "real_arr256_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "we0" }} , 
 	{ "name": "real_arr256_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "address1" }} , 
 	{ "name": "real_arr256_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "ce1" }} , 
 	{ "name": "real_arr256_3_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "d1" }} , 
 	{ "name": "real_arr256_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "q1" }} , 
 	{ "name": "real_arr256_3_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr256_3", "role": "we1" }} , 
 	{ "name": "real_arr128_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "address0" }} , 
 	{ "name": "real_arr128_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "ce0" }} , 
 	{ "name": "real_arr128_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "d0" }} , 
 	{ "name": "real_arr128_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "q0" }} , 
 	{ "name": "real_arr128_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "we0" }} , 
 	{ "name": "real_arr128_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "address1" }} , 
 	{ "name": "real_arr128_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "ce1" }} , 
 	{ "name": "real_arr128_1_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "d1" }} , 
 	{ "name": "real_arr128_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "q1" }} , 
 	{ "name": "real_arr128_1_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_1", "role": "we1" }} , 
 	{ "name": "imag_arr256_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "address0" }} , 
 	{ "name": "imag_arr256_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "ce0" }} , 
 	{ "name": "imag_arr256_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "d0" }} , 
 	{ "name": "imag_arr256_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "q0" }} , 
 	{ "name": "imag_arr256_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "we0" }} , 
 	{ "name": "imag_arr256_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "address1" }} , 
 	{ "name": "imag_arr256_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "ce1" }} , 
 	{ "name": "imag_arr256_2_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "d1" }} , 
 	{ "name": "imag_arr256_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "q1" }} , 
 	{ "name": "imag_arr256_2_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_2", "role": "we1" }} , 
 	{ "name": "imag_arr256_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "address0" }} , 
 	{ "name": "imag_arr256_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "ce0" }} , 
 	{ "name": "imag_arr256_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "d0" }} , 
 	{ "name": "imag_arr256_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "q0" }} , 
 	{ "name": "imag_arr256_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "we0" }} , 
 	{ "name": "imag_arr256_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":6, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "address1" }} , 
 	{ "name": "imag_arr256_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "ce1" }} , 
 	{ "name": "imag_arr256_3_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "d1" }} , 
 	{ "name": "imag_arr256_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "q1" }} , 
 	{ "name": "imag_arr256_3_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr256_3", "role": "we1" }} , 
 	{ "name": "imag_arr128_1_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "address0" }} , 
 	{ "name": "imag_arr128_1_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "ce0" }} , 
 	{ "name": "imag_arr128_1_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "d0" }} , 
 	{ "name": "imag_arr128_1_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "q0" }} , 
 	{ "name": "imag_arr128_1_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "we0" }} , 
 	{ "name": "imag_arr128_1_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "address1" }} , 
 	{ "name": "imag_arr128_1_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "ce1" }} , 
 	{ "name": "imag_arr128_1_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "d1" }} , 
 	{ "name": "imag_arr128_1_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "q1" }} , 
 	{ "name": "imag_arr128_1_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_1", "role": "we1" }} , 
 	{ "name": "real_arr128_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "address0" }} , 
 	{ "name": "real_arr128_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "ce0" }} , 
 	{ "name": "real_arr128_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "d0" }} , 
 	{ "name": "real_arr128_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "q0" }} , 
 	{ "name": "real_arr128_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "we0" }} , 
 	{ "name": "real_arr128_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "address1" }} , 
 	{ "name": "real_arr128_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "ce1" }} , 
 	{ "name": "real_arr128_2_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "d1" }} , 
 	{ "name": "real_arr128_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "q1" }} , 
 	{ "name": "real_arr128_2_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_2", "role": "we1" }} , 
 	{ "name": "imag_arr128_2_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "address0" }} , 
 	{ "name": "imag_arr128_2_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "ce0" }} , 
 	{ "name": "imag_arr128_2_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "d0" }} , 
 	{ "name": "imag_arr128_2_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "q0" }} , 
 	{ "name": "imag_arr128_2_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "we0" }} , 
 	{ "name": "imag_arr128_2_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "address1" }} , 
 	{ "name": "imag_arr128_2_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "ce1" }} , 
 	{ "name": "imag_arr128_2_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "d1" }} , 
 	{ "name": "imag_arr128_2_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "q1" }} , 
 	{ "name": "imag_arr128_2_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_2", "role": "we1" }} , 
 	{ "name": "real_arr128_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "address0" }} , 
 	{ "name": "real_arr128_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "ce0" }} , 
 	{ "name": "real_arr128_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "d0" }} , 
 	{ "name": "real_arr128_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "q0" }} , 
 	{ "name": "real_arr128_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "we0" }} , 
 	{ "name": "real_arr128_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "address1" }} , 
 	{ "name": "real_arr128_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "ce1" }} , 
 	{ "name": "real_arr128_3_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "d1" }} , 
 	{ "name": "real_arr128_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "q1" }} , 
 	{ "name": "real_arr128_3_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "real_arr128_3", "role": "we1" }} , 
 	{ "name": "imag_arr128_3_address0", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "address0" }} , 
 	{ "name": "imag_arr128_3_ce0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "ce0" }} , 
 	{ "name": "imag_arr128_3_d0", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "d0" }} , 
 	{ "name": "imag_arr128_3_q0", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "q0" }} , 
 	{ "name": "imag_arr128_3_we0", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "we0" }} , 
 	{ "name": "imag_arr128_3_address1", "direction": "out", "datatype": "sc_lv", "bitwidth":5, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "address1" }} , 
 	{ "name": "imag_arr128_3_ce1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "ce1" }} , 
 	{ "name": "imag_arr128_3_d1", "direction": "out", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "d1" }} , 
 	{ "name": "imag_arr128_3_q1", "direction": "in", "datatype": "sc_lv", "bitwidth":32, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "q1" }} , 
 	{ "name": "imag_arr128_3_we1", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "signal", "bundle":{"name": "imag_arr128_3", "role": "we1" }} , 
 	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }}  ]}
set Spec2ImplPortList { 
	real_arr256_0 { ap_memory {  { real_arr256_0_address0 mem_address 1 6 }  { real_arr256_0_ce0 mem_ce 1 1 }  { real_arr256_0_d0 mem_din 1 32 }  { real_arr256_0_q0 mem_dout 0 32 }  { real_arr256_0_we0 mem_we 1 1 }  { real_arr256_0_address1 mem_address 1 6 }  { real_arr256_0_ce1 mem_ce 1 1 }  { real_arr256_0_d1 mem_din 1 32 }  { real_arr256_0_q1 mem_dout 0 32 }  { real_arr256_0_we1 mem_we 1 1 } } }
	real_arr256_1 { ap_memory {  { real_arr256_1_address0 mem_address 1 6 }  { real_arr256_1_ce0 mem_ce 1 1 }  { real_arr256_1_d0 mem_din 1 32 }  { real_arr256_1_q0 mem_dout 0 32 }  { real_arr256_1_we0 mem_we 1 1 }  { real_arr256_1_address1 mem_address 1 6 }  { real_arr256_1_ce1 mem_ce 1 1 }  { real_arr256_1_d1 mem_din 1 32 }  { real_arr256_1_q1 mem_dout 0 32 }  { real_arr256_1_we1 mem_we 1 1 } } }
	real_arr128_0 { ap_memory {  { real_arr128_0_address0 mem_address 1 5 }  { real_arr128_0_ce0 mem_ce 1 1 }  { real_arr128_0_d0 mem_din 1 32 }  { real_arr128_0_q0 mem_dout 0 32 }  { real_arr128_0_we0 mem_we 1 1 }  { real_arr128_0_address1 mem_address 1 5 }  { real_arr128_0_ce1 mem_ce 1 1 }  { real_arr128_0_d1 mem_din 1 32 }  { real_arr128_0_q1 mem_dout 0 32 }  { real_arr128_0_we1 mem_we 1 1 } } }
	imag_arr256_0 { ap_memory {  { imag_arr256_0_address0 mem_address 1 6 }  { imag_arr256_0_ce0 mem_ce 1 1 }  { imag_arr256_0_d0 mem_din 1 32 }  { imag_arr256_0_q0 mem_dout 0 32 }  { imag_arr256_0_we0 mem_we 1 1 }  { imag_arr256_0_address1 mem_address 1 6 }  { imag_arr256_0_ce1 mem_ce 1 1 }  { imag_arr256_0_d1 mem_din 1 32 }  { imag_arr256_0_q1 mem_dout 0 32 }  { imag_arr256_0_we1 mem_we 1 1 } } }
	imag_arr256_1 { ap_memory {  { imag_arr256_1_address0 mem_address 1 6 }  { imag_arr256_1_ce0 mem_ce 1 1 }  { imag_arr256_1_d0 mem_din 1 32 }  { imag_arr256_1_q0 mem_dout 0 32 }  { imag_arr256_1_we0 mem_we 1 1 }  { imag_arr256_1_address1 mem_address 1 6 }  { imag_arr256_1_ce1 mem_ce 1 1 }  { imag_arr256_1_d1 mem_din 1 32 }  { imag_arr256_1_q1 mem_dout 0 32 }  { imag_arr256_1_we1 mem_we 1 1 } } }
	imag_arr128_0 { ap_memory {  { imag_arr128_0_address0 mem_address 1 5 }  { imag_arr128_0_ce0 mem_ce 1 1 }  { imag_arr128_0_d0 mem_din 1 32 }  { imag_arr128_0_q0 mem_dout 0 32 }  { imag_arr128_0_we0 mem_we 1 1 }  { imag_arr128_0_address1 mem_address 1 5 }  { imag_arr128_0_ce1 mem_ce 1 1 }  { imag_arr128_0_d1 mem_din 1 32 }  { imag_arr128_0_q1 mem_dout 0 32 }  { imag_arr128_0_we1 mem_we 1 1 } } }
	real_arr256_2 { ap_memory {  { real_arr256_2_address0 mem_address 1 6 }  { real_arr256_2_ce0 mem_ce 1 1 }  { real_arr256_2_d0 mem_din 1 32 }  { real_arr256_2_q0 mem_dout 0 32 }  { real_arr256_2_we0 mem_we 1 1 }  { real_arr256_2_address1 mem_address 1 6 }  { real_arr256_2_ce1 mem_ce 1 1 }  { real_arr256_2_d1 mem_din 1 32 }  { real_arr256_2_q1 mem_dout 0 32 }  { real_arr256_2_we1 mem_we 1 1 } } }
	real_arr256_3 { ap_memory {  { real_arr256_3_address0 mem_address 1 6 }  { real_arr256_3_ce0 mem_ce 1 1 }  { real_arr256_3_d0 mem_din 1 32 }  { real_arr256_3_q0 mem_dout 0 32 }  { real_arr256_3_we0 mem_we 1 1 }  { real_arr256_3_address1 mem_address 1 6 }  { real_arr256_3_ce1 mem_ce 1 1 }  { real_arr256_3_d1 mem_din 1 32 }  { real_arr256_3_q1 mem_dout 0 32 }  { real_arr256_3_we1 mem_we 1 1 } } }
	real_arr128_1 { ap_memory {  { real_arr128_1_address0 mem_address 1 5 }  { real_arr128_1_ce0 mem_ce 1 1 }  { real_arr128_1_d0 mem_din 1 32 }  { real_arr128_1_q0 mem_dout 0 32 }  { real_arr128_1_we0 mem_we 1 1 }  { real_arr128_1_address1 mem_address 1 5 }  { real_arr128_1_ce1 mem_ce 1 1 }  { real_arr128_1_d1 mem_din 1 32 }  { real_arr128_1_q1 mem_dout 0 32 }  { real_arr128_1_we1 mem_we 1 1 } } }
	imag_arr256_2 { ap_memory {  { imag_arr256_2_address0 mem_address 1 6 }  { imag_arr256_2_ce0 mem_ce 1 1 }  { imag_arr256_2_d0 mem_din 1 32 }  { imag_arr256_2_q0 mem_dout 0 32 }  { imag_arr256_2_we0 mem_we 1 1 }  { imag_arr256_2_address1 mem_address 1 6 }  { imag_arr256_2_ce1 mem_ce 1 1 }  { imag_arr256_2_d1 mem_din 1 32 }  { imag_arr256_2_q1 mem_dout 0 32 }  { imag_arr256_2_we1 mem_we 1 1 } } }
	imag_arr256_3 { ap_memory {  { imag_arr256_3_address0 mem_address 1 6 }  { imag_arr256_3_ce0 mem_ce 1 1 }  { imag_arr256_3_d0 mem_din 1 32 }  { imag_arr256_3_q0 mem_dout 0 32 }  { imag_arr256_3_we0 mem_we 1 1 }  { imag_arr256_3_address1 mem_address 1 6 }  { imag_arr256_3_ce1 mem_ce 1 1 }  { imag_arr256_3_d1 mem_din 1 32 }  { imag_arr256_3_q1 mem_dout 0 32 }  { imag_arr256_3_we1 mem_we 1 1 } } }
	imag_arr128_1 { ap_memory {  { imag_arr128_1_address0 mem_address 1 5 }  { imag_arr128_1_ce0 mem_ce 1 1 }  { imag_arr128_1_d0 mem_din 1 32 }  { imag_arr128_1_q0 mem_dout 0 32 }  { imag_arr128_1_we0 mem_we 1 1 }  { imag_arr128_1_address1 mem_address 1 5 }  { imag_arr128_1_ce1 mem_ce 1 1 }  { imag_arr128_1_d1 mem_din 1 32 }  { imag_arr128_1_q1 mem_dout 0 32 }  { imag_arr128_1_we1 mem_we 1 1 } } }
	real_arr128_2 { ap_memory {  { real_arr128_2_address0 mem_address 1 5 }  { real_arr128_2_ce0 mem_ce 1 1 }  { real_arr128_2_d0 mem_din 1 32 }  { real_arr128_2_q0 mem_dout 0 32 }  { real_arr128_2_we0 mem_we 1 1 }  { real_arr128_2_address1 mem_address 1 5 }  { real_arr128_2_ce1 mem_ce 1 1 }  { real_arr128_2_d1 mem_din 1 32 }  { real_arr128_2_q1 mem_dout 0 32 }  { real_arr128_2_we1 mem_we 1 1 } } }
	imag_arr128_2 { ap_memory {  { imag_arr128_2_address0 mem_address 1 5 }  { imag_arr128_2_ce0 mem_ce 1 1 }  { imag_arr128_2_d0 mem_din 1 32 }  { imag_arr128_2_q0 mem_dout 0 32 }  { imag_arr128_2_we0 mem_we 1 1 }  { imag_arr128_2_address1 mem_address 1 5 }  { imag_arr128_2_ce1 mem_ce 1 1 }  { imag_arr128_2_d1 mem_din 1 32 }  { imag_arr128_2_q1 mem_dout 0 32 }  { imag_arr128_2_we1 mem_we 1 1 } } }
	real_arr128_3 { ap_memory {  { real_arr128_3_address0 mem_address 1 5 }  { real_arr128_3_ce0 mem_ce 1 1 }  { real_arr128_3_d0 mem_din 1 32 }  { real_arr128_3_q0 mem_dout 0 32 }  { real_arr128_3_we0 mem_we 1 1 }  { real_arr128_3_address1 mem_address 1 5 }  { real_arr128_3_ce1 mem_ce 1 1 }  { real_arr128_3_d1 mem_din 1 32 }  { real_arr128_3_q1 mem_dout 0 32 }  { real_arr128_3_we1 mem_we 1 1 } } }
	imag_arr128_3 { ap_memory {  { imag_arr128_3_address0 mem_address 1 5 }  { imag_arr128_3_ce0 mem_ce 1 1 }  { imag_arr128_3_d0 mem_din 1 32 }  { imag_arr128_3_q0 mem_dout 0 32 }  { imag_arr128_3_we0 mem_we 1 1 }  { imag_arr128_3_address1 mem_address 1 5 }  { imag_arr128_3_ce1 mem_ce 1 1 }  { imag_arr128_3_d1 mem_din 1 32 }  { imag_arr128_3_q1 mem_dout 0 32 }  { imag_arr128_3_we1 mem_we 1 1 } } }
}
