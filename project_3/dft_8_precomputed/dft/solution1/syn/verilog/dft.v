// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

(* CORE_GENERATION_INFO="dft,hls_ip_2015_4,{HLS_INPUT_TYPE=cxx,HLS_INPUT_FLOAT=1,HLS_INPUT_FIXED=0,HLS_INPUT_PART=xc7z020clg484-1,HLS_INPUT_CLOCK=10.000000,HLS_INPUT_ARCH=others,HLS_SYN_CLOCK=8.092000,HLS_SYN_LAT=1058,HLS_SYN_TPT=none,HLS_SYN_MEM=0,HLS_SYN_DSP=17,HLS_SYN_FF=1487,HLS_SYN_LUT=2371}" *)

module dft (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_idle,
        ap_ready,
        real_i_address0,
        real_i_ce0,
        real_i_we0,
        real_i_d0,
        real_i_q0,
        imag_i_address0,
        imag_i_ce0,
        imag_i_we0,
        imag_i_d0,
        imag_i_q0
);

parameter    ap_const_logic_1 = 1'b1;
parameter    ap_const_logic_0 = 1'b0;
parameter    ap_ST_st1_fsm_0 = 20'b1;
parameter    ap_ST_st2_fsm_1 = 20'b10;
parameter    ap_ST_st3_fsm_2 = 20'b100;
parameter    ap_ST_st4_fsm_3 = 20'b1000;
parameter    ap_ST_st5_fsm_4 = 20'b10000;
parameter    ap_ST_st6_fsm_5 = 20'b100000;
parameter    ap_ST_st7_fsm_6 = 20'b1000000;
parameter    ap_ST_st8_fsm_7 = 20'b10000000;
parameter    ap_ST_st9_fsm_8 = 20'b100000000;
parameter    ap_ST_st10_fsm_9 = 20'b1000000000;
parameter    ap_ST_st11_fsm_10 = 20'b10000000000;
parameter    ap_ST_st12_fsm_11 = 20'b100000000000;
parameter    ap_ST_st13_fsm_12 = 20'b1000000000000;
parameter    ap_ST_st14_fsm_13 = 20'b10000000000000;
parameter    ap_ST_st15_fsm_14 = 20'b100000000000000;
parameter    ap_ST_st16_fsm_15 = 20'b1000000000000000;
parameter    ap_ST_st17_fsm_16 = 20'b10000000000000000;
parameter    ap_ST_st18_fsm_17 = 20'b100000000000000000;
parameter    ap_ST_st19_fsm_18 = 20'b1000000000000000000;
parameter    ap_ST_st20_fsm_19 = 20'b10000000000000000000;
parameter    ap_const_lv32_0 = 32'b00000000000000000000000000000000;
parameter    ap_const_lv1_1 = 1'b1;
parameter    ap_const_lv32_B = 32'b1011;
parameter    ap_const_lv32_10 = 32'b10000;
parameter    ap_const_lv32_1 = 32'b1;
parameter    ap_const_lv1_0 = 1'b0;
parameter    ap_const_lv32_2 = 32'b10;
parameter    ap_const_lv32_6 = 32'b110;
parameter    ap_const_lv32_12 = 32'b10010;
parameter    ap_const_lv4_0 = 4'b0000;
parameter    ap_const_lv32_11 = 32'b10001;
parameter    ap_const_lv32_13 = 32'b10011;
parameter    ap_const_lv32_7 = 32'b111;
parameter    ap_const_lv32_C = 32'b1100;
parameter    ap_const_lv4_8 = 4'b1000;
parameter    ap_const_lv4_1 = 4'b1;
parameter    ap_const_lv2_0 = 2'b00;
parameter    ap_const_lv2_1 = 2'b1;
parameter    ap_true = 1'b1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
output   ap_idle;
output   ap_ready;
output  [2:0] real_i_address0;
output   real_i_ce0;
output   real_i_we0;
output  [31:0] real_i_d0;
input  [31:0] real_i_q0;
output  [2:0] imag_i_address0;
output   imag_i_ce0;
output   imag_i_we0;
output  [31:0] imag_i_d0;
input  [31:0] imag_i_q0;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg[2:0] real_i_address0;
reg real_i_ce0;
reg real_i_we0;
reg[2:0] imag_i_address0;
reg imag_i_ce0;
reg imag_i_we0;
(* fsm_encoding = "none" *) reg   [19:0] ap_CS_fsm = 20'b1;
reg    ap_sig_cseq_ST_st1_fsm_0;
reg    ap_sig_bdd_36;
wire   [2:0] cct_address0;
reg    cct_ce0;
wire   [31:0] cct_q0;
wire   [2:0] sct_address0;
reg    sct_ce0;
wire   [31:0] sct_q0;
wire   [31:0] grp_fu_198_p2;
reg   [31:0] reg_232;
reg    ap_sig_cseq_ST_st12_fsm_11;
reg    ap_sig_bdd_80;
reg    ap_sig_cseq_ST_st17_fsm_16;
reg    ap_sig_bdd_87;
wire   [31:0] grp_fu_202_p2;
reg   [31:0] reg_239;
wire  signed [2:0] tmp_1_fu_245_p1;
reg  signed [2:0] tmp_1_reg_318;
reg    ap_sig_cseq_ST_st2_fsm_1;
reg    ap_sig_bdd_99;
wire   [3:0] i_2_fu_255_p2;
reg   [3:0] i_2_reg_326;
reg   [2:0] tmp_real_addr_reg_331;
wire   [0:0] exitcond2_fu_249_p2;
reg   [2:0] tmp_imag_addr_reg_336;
wire   [3:0] j_1_fu_277_p2;
reg   [3:0] j_1_reg_344;
reg    ap_sig_cseq_ST_st3_fsm_2;
reg    ap_sig_bdd_118;
wire   [0:0] exitcond1_fu_271_p2;
wire   [31:0] grp_fu_208_p2;
reg   [31:0] tmp_9_reg_393;
reg    ap_sig_cseq_ST_st7_fsm_6;
reg    ap_sig_bdd_149;
wire   [31:0] grp_fu_214_p2;
reg   [31:0] tmp_s_reg_398;
wire   [31:0] grp_fu_220_p2;
reg   [31:0] tmp_6_reg_403;
wire   [31:0] grp_fu_226_p2;
reg   [31:0] tmp_10_reg_408;
wire   [3:0] i_1_fu_306_p2;
reg   [3:0] i_1_reg_416;
reg    ap_sig_cseq_ST_st19_fsm_18;
reg    ap_sig_bdd_164;
wire   [63:0] tmp_3_fu_312_p1;
reg   [63:0] tmp_3_reg_421;
wire   [0:0] exitcond_fu_300_p2;
reg   [2:0] tmp_real_address0;
reg    tmp_real_ce0;
reg    tmp_real_we0;
reg   [31:0] tmp_real_d0;
wire   [31:0] tmp_real_q0;
reg   [2:0] tmp_imag_address0;
reg    tmp_imag_ce0;
reg    tmp_imag_we0;
wire   [31:0] tmp_imag_d0;
wire   [31:0] tmp_imag_q0;
reg   [3:0] i_reg_140;
reg   [31:0] storemerge_reg_151;
reg    ap_sig_cseq_ST_st18_fsm_17;
reg    ap_sig_bdd_209;
reg   [31:0] tmp_2_reg_164;
reg   [3:0] j_reg_176;
reg   [3:0] i1_reg_187;
reg    ap_sig_cseq_ST_st20_fsm_19;
reg    ap_sig_bdd_222;
wire   [63:0] tmp_fu_261_p1;
wire   [63:0] tmp_7_fu_288_p1;
wire   [63:0] tmp_8_fu_294_p1;
reg   [31:0] grp_fu_198_p0;
reg   [31:0] grp_fu_198_p1;
reg    ap_sig_cseq_ST_st8_fsm_7;
reg    ap_sig_bdd_245;
reg    ap_sig_cseq_ST_st13_fsm_12;
reg    ap_sig_bdd_252;
reg   [31:0] grp_fu_202_p0;
reg   [31:0] grp_fu_202_p1;
wire  signed [2:0] tmp_13_fu_267_p1;
wire   [2:0] index_fu_283_p2;
reg   [1:0] grp_fu_198_opcode;
wire    grp_fu_198_ce;
wire    grp_fu_202_ce;
wire    grp_fu_208_ce;
wire    grp_fu_214_ce;
wire    grp_fu_220_ce;
wire    grp_fu_226_ce;
reg   [19:0] ap_NS_fsm;


dft_cct #(
    .DataWidth( 32 ),
    .AddressRange( 8 ),
    .AddressWidth( 3 ))
cct_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( cct_address0 ),
    .ce0( cct_ce0 ),
    .q0( cct_q0 )
);

dft_sct #(
    .DataWidth( 32 ),
    .AddressRange( 8 ),
    .AddressWidth( 3 ))
sct_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( sct_address0 ),
    .ce0( sct_ce0 ),
    .q0( sct_q0 )
);

dft_tmp_real #(
    .DataWidth( 32 ),
    .AddressRange( 8 ),
    .AddressWidth( 3 ))
tmp_real_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( tmp_real_address0 ),
    .ce0( tmp_real_ce0 ),
    .we0( tmp_real_we0 ),
    .d0( tmp_real_d0 ),
    .q0( tmp_real_q0 )
);

dft_tmp_real #(
    .DataWidth( 32 ),
    .AddressRange( 8 ),
    .AddressWidth( 3 ))
tmp_imag_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( tmp_imag_address0 ),
    .ce0( tmp_imag_ce0 ),
    .we0( tmp_imag_we0 ),
    .d0( tmp_imag_d0 ),
    .q0( tmp_imag_q0 )
);

dft_faddfsub_32ns_32ns_32_5_full_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 5 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
dft_faddfsub_32ns_32ns_32_5_full_dsp_U0(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( grp_fu_198_p0 ),
    .din1( grp_fu_198_p1 ),
    .opcode( grp_fu_198_opcode ),
    .ce( grp_fu_198_ce ),
    .dout( grp_fu_198_p2 )
);

dft_fadd_32ns_32ns_32_5_full_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 5 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
dft_fadd_32ns_32ns_32_5_full_dsp_U1(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( grp_fu_202_p0 ),
    .din1( grp_fu_202_p1 ),
    .ce( grp_fu_202_ce ),
    .dout( grp_fu_202_p2 )
);

dft_fmul_32ns_32ns_32_4_max_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 4 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
dft_fmul_32ns_32ns_32_4_max_dsp_U2(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( real_i_q0 ),
    .din1( cct_q0 ),
    .ce( grp_fu_208_ce ),
    .dout( grp_fu_208_p2 )
);

dft_fmul_32ns_32ns_32_4_max_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 4 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
dft_fmul_32ns_32ns_32_4_max_dsp_U3(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( imag_i_q0 ),
    .din1( sct_q0 ),
    .ce( grp_fu_214_ce ),
    .dout( grp_fu_214_p2 )
);

dft_fmul_32ns_32ns_32_4_max_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 4 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
dft_fmul_32ns_32ns_32_4_max_dsp_U4(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( real_i_q0 ),
    .din1( sct_q0 ),
    .ce( grp_fu_220_ce ),
    .dout( grp_fu_220_p2 )
);

dft_fmul_32ns_32ns_32_4_max_dsp #(
    .ID( 1 ),
    .NUM_STAGE( 4 ),
    .din0_WIDTH( 32 ),
    .din1_WIDTH( 32 ),
    .dout_WIDTH( 32 ))
dft_fmul_32ns_32ns_32_4_max_dsp_U5(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .din0( imag_i_q0 ),
    .din1( cct_q0 ),
    .ce( grp_fu_226_ce ),
    .dout( grp_fu_226_p2 )
);



always @ (posedge ap_clk) begin : ap_ret_ap_CS_fsm
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_st1_fsm_0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~(exitcond2_fu_249_p2 == ap_const_lv1_0))) begin
        i1_reg_187 <= ap_const_lv4_0;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st20_fsm_19)) begin
        i1_reg_187 <= i_1_reg_416;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2) & ~(ap_const_lv1_0 == exitcond1_fu_271_p2))) begin
        i_reg_140 <= i_2_reg_326;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        i_reg_140 <= ap_const_lv4_0;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17)) begin
        j_reg_176 <= j_1_reg_344;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & (exitcond2_fu_249_p2 == ap_const_lv1_0))) begin
        j_reg_176 <= ap_const_lv4_0;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17)) begin
        storemerge_reg_151 <= reg_239;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & (exitcond2_fu_249_p2 == ap_const_lv1_0))) begin
        storemerge_reg_151 <= ap_const_lv32_0;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17)) begin
        tmp_2_reg_164 <= reg_232;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & (exitcond2_fu_249_p2 == ap_const_lv1_0))) begin
        tmp_2_reg_164 <= ap_const_lv32_0;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18)) begin
        i_1_reg_416 <= i_1_fu_306_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        i_2_reg_326 <= i_2_fu_255_p2;
        tmp_1_reg_318 <= tmp_1_fu_245_p1;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        j_1_reg_344 <= j_1_fu_277_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st12_fsm_11) | (ap_const_logic_1 == ap_sig_cseq_ST_st17_fsm_16))) begin
        reg_232 <= grp_fu_198_p2;
        reg_239 <= grp_fu_202_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st7_fsm_6)) begin
        tmp_10_reg_408 <= grp_fu_226_p2;
        tmp_6_reg_403 <= grp_fu_220_p2;
        tmp_9_reg_393 <= grp_fu_208_p2;
        tmp_s_reg_398 <= grp_fu_214_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18) & (ap_const_lv1_0 == exitcond_fu_300_p2))) begin
        tmp_3_reg_421[3 : 0] <= tmp_3_fu_312_p1[3 : 0];
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & (exitcond2_fu_249_p2 == ap_const_lv1_0))) begin
        tmp_imag_addr_reg_336 <= tmp_fu_261_p1;
        tmp_real_addr_reg_331 <= tmp_fu_261_p1;
    end
end

always @ (ap_sig_cseq_ST_st19_fsm_18 or exitcond_fu_300_p2) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18) & ~(ap_const_lv1_0 == exitcond_fu_300_p2))) begin
        ap_done = ap_const_logic_1;
    end else begin
        ap_done = ap_const_logic_0;
    end
end

always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if ((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0))) begin
        ap_idle = ap_const_logic_1;
    end else begin
        ap_idle = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st19_fsm_18 or exitcond_fu_300_p2) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18) & ~(ap_const_lv1_0 == exitcond_fu_300_p2))) begin
        ap_ready = ap_const_logic_1;
    end else begin
        ap_ready = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_80) begin
    if (ap_sig_bdd_80) begin
        ap_sig_cseq_ST_st12_fsm_11 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st12_fsm_11 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_252) begin
    if (ap_sig_bdd_252) begin
        ap_sig_cseq_ST_st13_fsm_12 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st13_fsm_12 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_87) begin
    if (ap_sig_bdd_87) begin
        ap_sig_cseq_ST_st17_fsm_16 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st17_fsm_16 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_209) begin
    if (ap_sig_bdd_209) begin
        ap_sig_cseq_ST_st18_fsm_17 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st18_fsm_17 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_164) begin
    if (ap_sig_bdd_164) begin
        ap_sig_cseq_ST_st19_fsm_18 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st19_fsm_18 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_36) begin
    if (ap_sig_bdd_36) begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_222) begin
    if (ap_sig_bdd_222) begin
        ap_sig_cseq_ST_st20_fsm_19 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st20_fsm_19 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_99) begin
    if (ap_sig_bdd_99) begin
        ap_sig_cseq_ST_st2_fsm_1 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st2_fsm_1 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_118) begin
    if (ap_sig_bdd_118) begin
        ap_sig_cseq_ST_st3_fsm_2 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st3_fsm_2 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_149) begin
    if (ap_sig_bdd_149) begin
        ap_sig_cseq_ST_st7_fsm_6 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st7_fsm_6 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_245) begin
    if (ap_sig_bdd_245) begin
        ap_sig_cseq_ST_st8_fsm_7 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st8_fsm_7 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st3_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        cct_ce0 = ap_const_logic_1;
    end else begin
        cct_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st8_fsm_7 or ap_sig_cseq_ST_st13_fsm_12) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7)) begin
        grp_fu_198_opcode = ap_const_lv2_1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st13_fsm_12)) begin
        grp_fu_198_opcode = ap_const_lv2_0;
    end else begin
        grp_fu_198_opcode = 'bx;
    end
end

always @ (tmp_9_reg_393 or tmp_2_reg_164 or ap_sig_cseq_ST_st8_fsm_7 or ap_sig_cseq_ST_st13_fsm_12) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st13_fsm_12)) begin
        grp_fu_198_p0 = tmp_2_reg_164;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7)) begin
        grp_fu_198_p0 = tmp_9_reg_393;
    end else begin
        grp_fu_198_p0 = 'bx;
    end
end

always @ (reg_232 or tmp_s_reg_398 or ap_sig_cseq_ST_st8_fsm_7 or ap_sig_cseq_ST_st13_fsm_12) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st13_fsm_12)) begin
        grp_fu_198_p1 = reg_232;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7)) begin
        grp_fu_198_p1 = tmp_s_reg_398;
    end else begin
        grp_fu_198_p1 = 'bx;
    end
end

always @ (tmp_6_reg_403 or storemerge_reg_151 or ap_sig_cseq_ST_st8_fsm_7 or ap_sig_cseq_ST_st13_fsm_12) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st13_fsm_12)) begin
        grp_fu_202_p0 = storemerge_reg_151;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7)) begin
        grp_fu_202_p0 = tmp_6_reg_403;
    end else begin
        grp_fu_202_p0 = 'bx;
    end
end

always @ (reg_239 or tmp_10_reg_408 or ap_sig_cseq_ST_st8_fsm_7 or ap_sig_cseq_ST_st13_fsm_12) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st13_fsm_12)) begin
        grp_fu_202_p1 = reg_239;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st8_fsm_7)) begin
        grp_fu_202_p1 = tmp_10_reg_408;
    end else begin
        grp_fu_202_p1 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_st3_fsm_2 or tmp_3_reg_421 or ap_sig_cseq_ST_st20_fsm_19 or tmp_7_fu_288_p1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st20_fsm_19)) begin
        imag_i_address0 = tmp_3_reg_421;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        imag_i_address0 = tmp_7_fu_288_p1;
    end else begin
        imag_i_address0 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_st3_fsm_2 or ap_sig_cseq_ST_st20_fsm_19) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2) | (ap_const_logic_1 == ap_sig_cseq_ST_st20_fsm_19))) begin
        imag_i_ce0 = ap_const_logic_1;
    end else begin
        imag_i_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st20_fsm_19) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st20_fsm_19)) begin
        imag_i_we0 = ap_const_logic_1;
    end else begin
        imag_i_we0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st3_fsm_2 or tmp_3_reg_421 or ap_sig_cseq_ST_st20_fsm_19 or tmp_7_fu_288_p1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st20_fsm_19)) begin
        real_i_address0 = tmp_3_reg_421;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        real_i_address0 = tmp_7_fu_288_p1;
    end else begin
        real_i_address0 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_st3_fsm_2 or ap_sig_cseq_ST_st20_fsm_19) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2) | (ap_const_logic_1 == ap_sig_cseq_ST_st20_fsm_19))) begin
        real_i_ce0 = ap_const_logic_1;
    end else begin
        real_i_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st20_fsm_19) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st20_fsm_19)) begin
        real_i_we0 = ap_const_logic_1;
    end else begin
        real_i_we0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st3_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        sct_ce0 = ap_const_logic_1;
    end else begin
        sct_ce0 = ap_const_logic_0;
    end
end

always @ (tmp_imag_addr_reg_336 or ap_sig_cseq_ST_st3_fsm_2 or ap_sig_cseq_ST_st19_fsm_18 or tmp_3_fu_312_p1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        tmp_imag_address0 = tmp_imag_addr_reg_336;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18)) begin
        tmp_imag_address0 = tmp_3_fu_312_p1;
    end else begin
        tmp_imag_address0 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_st3_fsm_2 or ap_sig_cseq_ST_st19_fsm_18) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2) | (ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18))) begin
        tmp_imag_ce0 = ap_const_logic_1;
    end else begin
        tmp_imag_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st3_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st3_fsm_2)) begin
        tmp_imag_we0 = ap_const_logic_1;
    end else begin
        tmp_imag_we0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st2_fsm_1 or tmp_real_addr_reg_331 or ap_sig_cseq_ST_st19_fsm_18 or tmp_3_fu_312_p1 or ap_sig_cseq_ST_st18_fsm_17 or tmp_fu_261_p1) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17)) begin
        tmp_real_address0 = tmp_real_addr_reg_331;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        tmp_real_address0 = tmp_fu_261_p1;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18)) begin
        tmp_real_address0 = tmp_3_fu_312_p1;
    end else begin
        tmp_real_address0 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_st2_fsm_1 or ap_sig_cseq_ST_st19_fsm_18 or ap_sig_cseq_ST_st18_fsm_17) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) | (ap_const_logic_1 == ap_sig_cseq_ST_st19_fsm_18) | (ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17))) begin
        tmp_real_ce0 = ap_const_logic_1;
    end else begin
        tmp_real_ce0 = ap_const_logic_0;
    end
end

always @ (reg_232 or ap_sig_cseq_ST_st2_fsm_1 or ap_sig_cseq_ST_st18_fsm_17) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17)) begin
        tmp_real_d0 = reg_232;
    end else if ((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1)) begin
        tmp_real_d0 = ap_const_lv32_0;
    end else begin
        tmp_real_d0 = 'bx;
    end
end

always @ (ap_sig_cseq_ST_st2_fsm_1 or exitcond2_fu_249_p2 or ap_sig_cseq_ST_st18_fsm_17) begin
    if ((((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & (exitcond2_fu_249_p2 == ap_const_lv1_0)) | (ap_const_logic_1 == ap_sig_cseq_ST_st18_fsm_17))) begin
        tmp_real_we0 = ap_const_logic_1;
    end else begin
        tmp_real_we0 = ap_const_logic_0;
    end
end
always @ (ap_start or ap_CS_fsm or exitcond2_fu_249_p2 or exitcond1_fu_271_p2 or exitcond_fu_300_p2) begin
    case (ap_CS_fsm)
        ap_ST_st1_fsm_0 : 
        begin
            if (~(ap_start == ap_const_logic_0)) begin
                ap_NS_fsm = ap_ST_st2_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end
        end
        ap_ST_st2_fsm_1 : 
        begin
            if ((exitcond2_fu_249_p2 == ap_const_lv1_0)) begin
                ap_NS_fsm = ap_ST_st3_fsm_2;
            end else begin
                ap_NS_fsm = ap_ST_st19_fsm_18;
            end
        end
        ap_ST_st3_fsm_2 : 
        begin
            if ((ap_const_lv1_0 == exitcond1_fu_271_p2)) begin
                ap_NS_fsm = ap_ST_st4_fsm_3;
            end else begin
                ap_NS_fsm = ap_ST_st2_fsm_1;
            end
        end
        ap_ST_st4_fsm_3 : 
        begin
            ap_NS_fsm = ap_ST_st5_fsm_4;
        end
        ap_ST_st5_fsm_4 : 
        begin
            ap_NS_fsm = ap_ST_st6_fsm_5;
        end
        ap_ST_st6_fsm_5 : 
        begin
            ap_NS_fsm = ap_ST_st7_fsm_6;
        end
        ap_ST_st7_fsm_6 : 
        begin
            ap_NS_fsm = ap_ST_st8_fsm_7;
        end
        ap_ST_st8_fsm_7 : 
        begin
            ap_NS_fsm = ap_ST_st9_fsm_8;
        end
        ap_ST_st9_fsm_8 : 
        begin
            ap_NS_fsm = ap_ST_st10_fsm_9;
        end
        ap_ST_st10_fsm_9 : 
        begin
            ap_NS_fsm = ap_ST_st11_fsm_10;
        end
        ap_ST_st11_fsm_10 : 
        begin
            ap_NS_fsm = ap_ST_st12_fsm_11;
        end
        ap_ST_st12_fsm_11 : 
        begin
            ap_NS_fsm = ap_ST_st13_fsm_12;
        end
        ap_ST_st13_fsm_12 : 
        begin
            ap_NS_fsm = ap_ST_st14_fsm_13;
        end
        ap_ST_st14_fsm_13 : 
        begin
            ap_NS_fsm = ap_ST_st15_fsm_14;
        end
        ap_ST_st15_fsm_14 : 
        begin
            ap_NS_fsm = ap_ST_st16_fsm_15;
        end
        ap_ST_st16_fsm_15 : 
        begin
            ap_NS_fsm = ap_ST_st17_fsm_16;
        end
        ap_ST_st17_fsm_16 : 
        begin
            ap_NS_fsm = ap_ST_st18_fsm_17;
        end
        ap_ST_st18_fsm_17 : 
        begin
            ap_NS_fsm = ap_ST_st3_fsm_2;
        end
        ap_ST_st19_fsm_18 : 
        begin
            if (~(ap_const_lv1_0 == exitcond_fu_300_p2)) begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end else begin
                ap_NS_fsm = ap_ST_st20_fsm_19;
            end
        end
        ap_ST_st20_fsm_19 : 
        begin
            ap_NS_fsm = ap_ST_st19_fsm_18;
        end
        default : 
        begin
            ap_NS_fsm = 'bx;
        end
    endcase
end



always @ (ap_CS_fsm) begin
    ap_sig_bdd_118 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_2]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_149 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_6]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_164 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_12]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_209 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_11]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_222 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_13]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_245 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_7]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_252 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_C]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_36 = (ap_CS_fsm[ap_const_lv32_0] == ap_const_lv1_1);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_80 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_B]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_87 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_10]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_99 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_1]);
end

assign cct_address0 = tmp_8_fu_294_p1;

assign exitcond1_fu_271_p2 = (j_reg_176 == ap_const_lv4_8? 1'b1: 1'b0);

assign exitcond2_fu_249_p2 = (i_reg_140 == ap_const_lv4_8? 1'b1: 1'b0);

assign exitcond_fu_300_p2 = (i1_reg_187 == ap_const_lv4_8? 1'b1: 1'b0);

assign grp_fu_198_ce = ap_const_logic_1;

assign grp_fu_202_ce = ap_const_logic_1;

assign grp_fu_208_ce = ap_const_logic_1;

assign grp_fu_214_ce = ap_const_logic_1;

assign grp_fu_220_ce = ap_const_logic_1;

assign grp_fu_226_ce = ap_const_logic_1;

assign i_1_fu_306_p2 = (i1_reg_187 + ap_const_lv4_1);

assign i_2_fu_255_p2 = (ap_const_lv4_1 + i_reg_140);

assign imag_i_d0 = tmp_imag_q0;

assign index_fu_283_p2 = ($signed(tmp_1_reg_318) * $signed(tmp_13_fu_267_p1));

assign j_1_fu_277_p2 = (ap_const_lv4_1 + j_reg_176);

assign real_i_d0 = tmp_real_q0;

assign sct_address0 = tmp_8_fu_294_p1;

assign tmp_13_fu_267_p1 = j_reg_176[2:0];

assign tmp_1_fu_245_p1 = i_reg_140[2:0];

assign tmp_3_fu_312_p1 = i1_reg_187;

assign tmp_7_fu_288_p1 = j_reg_176;

assign tmp_8_fu_294_p1 = index_fu_283_p2;

assign tmp_fu_261_p1 = i_reg_140;

assign tmp_imag_d0 = storemerge_reg_151;
always @ (posedge ap_clk) begin
    tmp_3_reg_421[63:4] <= 60'b000000000000000000000000000000000000000000000000000000000000;
end



endmodule //dft

