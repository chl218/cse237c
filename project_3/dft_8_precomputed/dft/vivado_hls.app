<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="com.autoesl.autopilot.project" name="dft" top="dft">
  <files>
    <file name="../../out.gold.dat" sc="0" tb="1" cflags=" "/>
    <file name="../../dft_test.cpp" sc="0" tb="1" cflags=" "/>
    <file name="dft.h" sc="0" tb="false" cflags=""/>
    <file name="dft.cpp" sc="0" tb="false" cflags=""/>
    <file name="coefficients8.h" sc="0" tb="false" cflags=""/>
  </files>
  <solutions>
    <solution name="solution1" status="inactive"/>
    <solution name="solution2" status="active"/>
  </solutions>
  <includePaths/>
  <libraryPaths/>
  <Simulation>
    <SimFlow askAgain="false" name="csim" csimMode="0" lastCsimMode="0"/>
  </Simulation>
</project>
