#include "dft.h"
#include "coefficients8.h"

void dft(DTYPE real_i[SIZE], DTYPE imag_i[SIZE])
{

	DTYPE tmp_real[SIZE];
	DTYPE tmp_imag[SIZE];

	for(int i = 0; i < SIZE; i++) {
		tmp_real[i] = 0;
		tmp_imag[i] = 0;

		for(int j = 0; j < SIZE; j++) {
			int index = (i*j)%SIZE;
			tmp_real[i] += (real_i[j]*cct[index] - imag_i[j]*sct[index]);
			tmp_imag[i] += (real_i[j]*sct[index] + imag_i[j]*cct[index]);
		}
	}

	for(int i = 0; i < SIZE; i++) {
		real_i[i] = tmp_real[i];
		imag_i[i] = tmp_imag[i];

	}
}
