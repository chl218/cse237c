#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>

#define NO_SAMPLE 512
#define NO_SAMPLE_X2 1024

int main(int argc, char *argv[]) {

  int fdr, fdw;

  int tologic[NO_SAMPLE_X2];
  int fromlogic[NO_SAMPLE_X2];
  FILE * fout;
  FILE * fin;

  int i;

  float * I;
  float * Q;
  float * R;
  float * Theta;

  float db_var;

  I=(float*)malloc(sizeof(float)*NO_SAMPLE);
  Q=(float*)malloc(sizeof(float)*NO_SAMPLE);
  R=(float*)malloc(sizeof(float)*NO_SAMPLE);
  Theta=(float*)malloc(sizeof(float)*NO_SAMPLE);

  fin=fopen("input_i.dat","r");
  for (i=0;i<NO_SAMPLE;i++)
  {
        fscanf(fin,"%f",&I[i]);
	
  }
  fclose(fin);

  fin=fopen("input_q.dat","r");
  for (i=0;i<NO_SAMPLE;i++)
  {
        fscanf(fin,"%f",&Q[i]);
	
  }
  fclose(fin);


  //Write Code Here: put I[] and Q[] into tologic[]. Hint: make sure this is compatible with how you receive input in the xillybus_wrapper function in HLS
  
  //Write Code Here: Xillybus APIs open(), write(), read() and close() just like lab 1.

  //Write Code Here: decode the data in fromlogic[] into R[] and Theta[]. Hint: make sure this is compatible with how you send output in the xillybus_wrapper function in HLS


  fout=fopen("output_R.txt","w");
  for (i=0;i<NO_SAMPLE;i++)
  {
	fprintf(fout,"%d ",i);
	fprintf(fout,"%f ",R[i]);
	fprintf(fout,"\n");
  }
  fclose(fout);

  fout=fopen("output_Theta.txt","w");
  for (i=0;i<NO_SAMPLE;i++)
  {
	fprintf(fout,"%d ",i);
	fprintf(fout,"%f ",Theta[i]);
	fprintf(fout,"\n");
  }
  fclose(fout);

  printf("Thetas at the R peaks are:\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n %f\n",Theta[31],Theta[63],Theta[95],Theta[127],Theta[159],Theta[191],Theta[223],Theta[255],Theta[287],Theta[319],Theta[351],Theta[383],Theta[415],Theta[447],Theta[479],Theta[511]);



  free(I);
  free(Q);
  free(R);
  free(Theta);


  return 0;
}
