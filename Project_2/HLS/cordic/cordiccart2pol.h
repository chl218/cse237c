#ifndef CORDICCART2POL_H
#define CORDICCART2POL_H

#define NO_ITER 11
#define NO_COEF 16

#include <ap_int.h>
#include <ap_fixed.h>

typedef ap_fixed<22,8> data_t;

void cordiccart2pol(
	data_t x,
	data_t y,

	data_t * r,
	data_t * theta
	);

#endif
