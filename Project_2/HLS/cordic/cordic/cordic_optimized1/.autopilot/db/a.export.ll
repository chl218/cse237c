; ModuleID = 'D:/Projects/vivado/Project_2/HLS/cordic/cordic/cordic_optimized1/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535]
@cordiccart2pol_str = internal unnamed_addr constant [15 x i8] c"cordiccart2pol\00"
@angles_V_old = constant [16 x i20] [i20 6433, i20 3798, i20 2006, i20 1018, i20 511, i20 255, i20 127, i20 63, i20 31, i20 15, i20 7, i20 3, i20 1, i20 0, i20 0, i20 0]
@angles_V = constant [16 x i13] [i13 -1759, i13 3798, i13 2006, i13 1018, i13 511, i13 255, i13 127, i13 63, i13 31, i13 15, i13 7, i13 3, i13 1, i13 0, i13 0, i13 0]
@Kvalues_V_old = constant [16 x i20] [i20 8192, i20 4096, i20 2048, i20 1024, i20 512, i20 256, i20 128, i20 64, i20 32, i20 16, i20 8, i20 4, i20 2, i20 1, i20 0, i20 0]
@Kvalues_V = constant [16 x i14] [i14 -8192, i14 4096, i14 2048, i14 1024, i14 512, i14 256, i14 128, i14 64, i14 32, i14 16, i14 8, i14 4, i14 2, i14 1, i14 0, i14 0]

declare i35 @llvm.part.select.i35(i35, i32, i32) nounwind readnone

declare i34 @llvm.part.select.i34(i34, i32, i32) nounwind readnone

declare i33 @llvm.part.select.i33(i33, i32, i32) nounwind readnone

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

define void @cordiccart2pol(i20 %x_V, i20 %y_V, i20* %r_V_3, i20* %theta_V) {
_ifconv:
  call void (...)* @_ssdm_op_SpecBitsMap(i20 %x_V), !map !7
  call void (...)* @_ssdm_op_SpecBitsMap(i20 %y_V), !map !13
  call void (...)* @_ssdm_op_SpecBitsMap(i20* %r_V_3), !map !17
  call void (...)* @_ssdm_op_SpecBitsMap(i20* %theta_V), !map !21
  call void (...)* @_ssdm_op_SpecTopModule([15 x i8]* @cordiccart2pol_str) nounwind
  %y_V_read = call i20 @_ssdm_op_Read.ap_auto.i20(i20 %y_V)
  %x_V_read = call i20 @_ssdm_op_Read.ap_auto.i20(i20 %x_V)
  %tmp_1 = call i1 @_ssdm_op_BitSelect.i1.i20.i32(i20 %x_V_read, i32 19)
  %tmp_1_i = icmp sgt i20 %y_V_read, 0
  %or_cond_i = and i1 %tmp_1, %tmp_1_i
  %p_Val2_1_i = sub i20 0, %x_V_read
  %p_Val2_2_cast_cast = select i1 %or_cond_i, i15 12867, i15 0
  %p_read2_read1_i = select i1 %or_cond_i, i20 %y_V_read, i20 %x_V_read
  %p_Val2_1 = select i1 %or_cond_i, i20 %p_Val2_1_i, i20 %y_V_read
  %tmp_2_i = and i20 %p_read2_read1_i, %p_Val2_1
  %tmp_5 = call i1 @_ssdm_op_BitSelect.i1.i20.i32(i20 %tmp_2_i, i32 19)
  %p_Val2_5_i = sub i20 0, %p_Val2_1
  %p_Val2_7_i = select i1 %or_cond_i, i15 0, i15 -12867
  %p_Val2_9_ph = select i1 %tmp_5, i15 %p_Val2_7_i, i15 %p_Val2_2_cast_cast
  %p_Val2_9_ph_cast = sext i15 %p_Val2_9_ph to i20
  %p_Val2_7_ph = select i1 %tmp_5, i20 %x_V_read, i20 %p_Val2_1
  %temp_I_V_ph = select i1 %tmp_5, i20 %p_Val2_5_i, i20 %p_read2_read1_i
  br label %correctQuadrant.exit

correctQuadrant.exit:                             ; preds = %0, %_ifconv
  %p_Val2_6 = phi i20 [ %curr_T_V, %0 ], [ %p_Val2_9_ph_cast, %_ifconv ]
  %p_Val2_4 = phi i20 [ %curr_Q_V, %0 ], [ %p_Val2_7_ph, %_ifconv ]
  %temp_I_V = phi i20 [ %curr_I_V, %0 ], [ %temp_I_V_ph, %_ifconv ]
  %p_s = phi i5 [ %j_V, %0 ], [ 0, %_ifconv ]
  %exitcond = icmp eq i5 %p_s, -16
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 16, i64 16, i64 16)
  %j_V = add i5 %p_s, 1
  br i1 %exitcond, label %ap_fixed_base.exit, label %0

; <label>:0                                       ; preds = %correctQuadrant.exit
  %tmp_6 = call i1 @_ssdm_op_BitSelect.i1.i20.i32(i20 %p_Val2_4, i32 19)
  %tmp_2 = zext i5 %p_s to i64
  %Kvalues_V_addr = getelementptr [16 x i14]* @Kvalues_V, i64 0, i64 %tmp_2
  %Kvalues_V_load = load i14* %Kvalues_V_addr, align 2
  %OP2_V_cast = zext i14 %Kvalues_V_load to i34
  %tmp = zext i14 %Kvalues_V_load to i33
  %tmp_3 = sext i20 %p_Val2_4 to i33
  %OP2_V_1_cast1 = mul i33 %tmp_3, %tmp
  %mf9_cast = sub i33 0, %OP2_V_1_cast1
  %tmp_4 = call i33 @_ssdm_op_BitConcatenate.i33.i20.i13(i20 %temp_I_V, i13 0)
  %tmp_4_cast4_cast = zext i33 %tmp_4 to i34
  %tmp_8 = select i1 %tmp_6, i33 %OP2_V_1_cast1, i33 %mf9_cast
  %tmp_5_cast = zext i33 %tmp_8 to i34
  %p_Val2_3 = sub i34 %tmp_4_cast4_cast, %tmp_5_cast
  %curr_I_V = call i20 @_ssdm_op_PartSelect.i20.i34.i32.i32(i34 %p_Val2_3, i32 13, i32 32)
  %OP1_V_2_cast = sext i20 %temp_I_V to i34
  %r_V = mul i34 %OP1_V_2_cast, %OP2_V_cast
  %mf1 = sub i34 0, %r_V
  %tmp_9 = select i1 %tmp_6, i34 %r_V, i34 %mf1
  %tmp_8_cast = zext i34 %tmp_9 to i35
  %tmp_7 = call i33 @_ssdm_op_BitConcatenate.i33.i20.i13(i20 %p_Val2_4, i13 0)
  %tmp_7_cast7_cast = zext i33 %tmp_7 to i35
  %p_Val2_5 = add i35 %tmp_7_cast7_cast, %tmp_8_cast
  %curr_Q_V = call i20 @_ssdm_op_PartSelect.i20.i35.i32.i32(i35 %p_Val2_5, i32 13, i32 32)
  %angles_V_addr = getelementptr [16 x i13]* @angles_V, i64 0, i64 %tmp_2
  %angles_V_load = load i13* %angles_V_addr, align 2
  %angles_V_load_cast = zext i13 %angles_V_load to i14
  %mf = sub i14 0, %angles_V_load_cast
  %p_Val2_s = select i1 %tmp_6, i14 %angles_V_load_cast, i14 %mf
  %p_Val2_cast = sext i14 %p_Val2_s to i20
  %curr_T_V = sub i20 %p_Val2_6, %p_Val2_cast
  br label %correctQuadrant.exit

ap_fixed_base.exit:                               ; preds = %correctQuadrant.exit
  call void @_ssdm_op_Write.ap_auto.i20P(i20* %theta_V, i20 %p_Val2_6)
  %OP1_V_cast = sext i20 %temp_I_V to i33
  %p_Val2_s_8 = mul i33 %OP1_V_cast, 4973
  %tmp_s = call i20 @_ssdm_op_PartSelect.i20.i33.i32.i32(i33 %p_Val2_s_8, i32 13, i32 32)
  call void @_ssdm_op_Write.ap_auto.i20P(i20* %r_V_3, i20 %tmp_s)
  ret void
}

define weak void @_ssdm_op_Write.ap_auto.i20P(i20*, i20) {
entry:
  store i20 %1, i20* %0
  ret void
}

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak i20 @_ssdm_op_Read.ap_auto.i20(i20) {
entry:
  ret i20 %0
}

define weak i20 @_ssdm_op_PartSelect.i20.i35.i32.i32(i35, i32, i32) nounwind readnone {
entry:
  %empty = call i35 @llvm.part.select.i35(i35 %0, i32 %1, i32 %2)
  %empty_9 = trunc i35 %empty to i20
  ret i20 %empty_9
}

define weak i20 @_ssdm_op_PartSelect.i20.i34.i32.i32(i34, i32, i32) nounwind readnone {
entry:
  %empty = call i34 @llvm.part.select.i34(i34 %0, i32 %1, i32 %2)
  %empty_10 = trunc i34 %empty to i20
  ret i20 %empty_10
}

define weak i20 @_ssdm_op_PartSelect.i20.i33.i32.i32(i33, i32, i32) nounwind readnone {
entry:
  %empty = call i33 @llvm.part.select.i33(i33 %0, i32 %1, i32 %2)
  %empty_11 = trunc i33 %empty to i20
  ret i20 %empty_11
}

declare i16 @_ssdm_op_HSub(...)

declare i16 @_ssdm_op_HMul(...)

declare i16 @_ssdm_op_HDiv(...)

declare i16 @_ssdm_op_HAdd(...)

define weak i1 @_ssdm_op_BitSelect.i1.i20.i32(i20, i32) nounwind readnone {
entry:
  %empty = trunc i32 %1 to i20
  %empty_12 = shl i20 1, %empty
  %empty_13 = and i20 %0, %empty_12
  %empty_14 = icmp ne i20 %empty_13, 0
  ret i1 %empty_14
}

define weak i33 @_ssdm_op_BitConcatenate.i33.i20.i13(i20, i13) nounwind readnone {
entry:
  %empty = zext i20 %0 to i33
  %empty_15 = zext i13 %1 to i33
  %empty_16 = shl i33 %empty, 13
  %empty_17 = or i33 %empty_16, %empty_15
  ret i33 %empty_17
}

declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [1 x i32]* @llvm_global_ctors_0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 0, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 19, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"x.V", metadata !11, metadata !"int20", i32 0, i32 19}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 0, i32 0}
!13 = metadata !{metadata !14}
!14 = metadata !{i32 0, i32 19, metadata !15}
!15 = metadata !{metadata !16}
!16 = metadata !{metadata !"y.V", metadata !11, metadata !"int20", i32 0, i32 19}
!17 = metadata !{metadata !18}
!18 = metadata !{i32 0, i32 19, metadata !19}
!19 = metadata !{metadata !20}
!20 = metadata !{metadata !"r.V.3", metadata !5, metadata !"int20", i32 0, i32 19}
!21 = metadata !{metadata !22}
!22 = metadata !{i32 0, i32 19, metadata !23}
!23 = metadata !{metadata !24}
!24 = metadata !{metadata !"theta.V", metadata !5, metadata !"int20", i32 0, i32 19}
