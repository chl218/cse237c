; ModuleID = 'D:/Projects/vivado/Project_2/HLS/cordic/cordic/cordic_baseline/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535]
@cordiccart2pol_str = internal unnamed_addr constant [15 x i8] c"cordiccart2pol\00"
@angles = constant [16 x float] [float 0x3FE921FB60000000, float 0x3FDDAC6700000000, float 0x3FCF5B7600000000, float 0x3FBFD5BAA0000000, float 0x3FAFF55BC0000000, float 0x3F9FFD55C0000000, float 0x3F8FFF5560000000, float 0x3F7FFFD560000000, float 0x3F6FFFF560000000, float 0x3F5FFFFD60000000, float 0x3F4FFFFF60000000, float 0x3F3FFFFFE0000000, float 0x3F30000000000000, float 0x3F20000000000000, float 0x3F10000000000000, float 0x3F00000000000000], align 16
@Kvalues = constant [16 x float] [float 1.000000e+00, float 5.000000e-01, float 2.500000e-01, float 1.250000e-01, float 6.250000e-02, float 3.125000e-02, float 1.562500e-02, float 7.812500e-03, float 3.906250e-03, float 1.953125e-03, float 9.765625e-04, float 0x3F40000000000000, float 0x3F30000000000000, float 0x3F20000000000000, float 0x3F10000000000000, float 0x3F00000000000000], align 16

declare i32 @llvm.part.select.i32(i32, i32, i32) nounwind readnone

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

define void @cordiccart2pol(float %x, float %y, float* %r, float* %theta) nounwind uwtable {
_ifconv:
  call void (...)* @_ssdm_op_SpecBitsMap(float %x) nounwind, !map !7
  call void (...)* @_ssdm_op_SpecBitsMap(float %y) nounwind, !map !13
  call void (...)* @_ssdm_op_SpecBitsMap(float* %r) nounwind, !map !17
  call void (...)* @_ssdm_op_SpecBitsMap(float* %theta) nounwind, !map !21
  call void (...)* @_ssdm_op_SpecTopModule([15 x i8]* @cordiccart2pol_str) nounwind
  %y_read = call float @_ssdm_op_Read.ap_auto.float(float %y) nounwind
  %x_read = call float @_ssdm_op_Read.ap_auto.float(float %x) nounwind
  %x_to_int = bitcast float %x_read to i32
  %tmp = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %x_to_int, i32 23, i32 30)
  %tmp_5 = trunc i32 %x_to_int to i23
  %notlhs = icmp ne i8 %tmp, -1
  %notrhs = icmp eq i23 %tmp_5, 0
  %tmp_6 = or i1 %notrhs, %notlhs
  %tmp_7 = fcmp olt float %x_read, 0.000000e+00
  %tmp_8 = and i1 %tmp_6, %tmp_7
  %y_to_int = bitcast float %y_read to i32
  %tmp_10 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %y_to_int, i32 23, i32 30)
  %tmp_11 = trunc i32 %y_to_int to i23
  %notlhs6 = icmp ne i8 %tmp_10, -1
  %notrhs7 = icmp eq i23 %tmp_11, 0
  %tmp_12 = or i1 %notrhs7, %notlhs6
  %tmp_13 = fcmp ogt float %y_read, 0.000000e+00
  %tmp_14 = and i1 %tmp_12, %tmp_13
  %or_cond_i = and i1 %tmp_8, %tmp_14
  %tmp_3_neg_i = xor i32 %x_to_int, -2147483648
  %tmp_3_i = bitcast i32 %tmp_3_neg_i to float
  %theta_i = select i1 %or_cond_i, float 0x3FF921FB60000000, float 0.000000e+00
  %x_i = select i1 %or_cond_i, float %y_read, float %x_read
  %y_i = select i1 %or_cond_i, float %tmp_3_i, float %y_read
  %x_i_to_int = bitcast float %x_i to i32
  %tmp_15 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %x_i_to_int, i32 23, i32 30)
  %tmp_16 = trunc i32 %x_i_to_int to i23
  %notlhs8 = icmp ne i8 %tmp_15, -1
  %notrhs9 = icmp eq i23 %tmp_16, 0
  %tmp_17 = or i1 %notrhs9, %notlhs8
  %tmp_18 = fcmp olt float %x_i, 0.000000e+00
  %tmp_19 = and i1 %tmp_17, %tmp_18
  %y_i_to_int = bitcast float %y_i to i32
  %tmp_20 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %y_i_to_int, i32 23, i32 30)
  %tmp_21 = trunc i32 %y_i_to_int to i23
  %notlhs1 = icmp ne i8 %tmp_20, -1
  %notrhs1 = icmp eq i23 %tmp_21, 0
  %tmp_22 = or i1 %notrhs1, %notlhs1
  %tmp_23 = fcmp olt float %y_i, 0.000000e+00
  %tmp_24 = and i1 %tmp_22, %tmp_23
  %or_cond3_i = and i1 %tmp_19, %tmp_24
  %tmp_7_neg_i = xor i32 %y_i_to_int, -2147483648
  %tmp_7_i = bitcast i32 %tmp_7_neg_i to float
  %tmp_8_i = fadd float %theta_i, 0xBFF921FB60000000
  %curr_T1_ph = select i1 %or_cond3_i, float %tmp_8_i, float %theta_i
  %curr_Q1_ph = select i1 %or_cond3_i, float %x_read, float %y_i
  %temp_I_ph = select i1 %or_cond3_i, float %tmp_7_i, float %x_i
  br label %correctQuadrant.exit

correctQuadrant.exit:                             ; preds = %0, %_ifconv
  %curr_T1 = phi float [ %curr_T, %0 ], [ %curr_T1_ph, %_ifconv ]
  %curr_Q1 = phi float [ %curr_Q, %0 ], [ %curr_Q1_ph, %_ifconv ]
  %temp_I = phi float [ %curr_I, %0 ], [ %temp_I_ph, %_ifconv ]
  %j = phi i5 [ %j_1, %0 ], [ 0, %_ifconv ]
  %exitcond = icmp eq i5 %j, -16
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 16, i64 16, i64 16) nounwind
  %j_1 = add i5 %j, 1
  br i1 %exitcond, label %1, label %0

; <label>:0                                       ; preds = %correctQuadrant.exit
  %curr_Q1_to_int = bitcast float %curr_Q1 to i32
  %tmp_25 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %curr_Q1_to_int, i32 23, i32 30)
  %tmp_26 = trunc i32 %curr_Q1_to_int to i23
  %notlhs2 = icmp ne i8 %tmp_25, -1
  %notrhs2 = icmp eq i23 %tmp_26, 0
  %tmp_27 = or i1 %notrhs2, %notlhs2
  %tmp_28 = fcmp oge float %curr_Q1, 0.000000e+00
  %tmp_29 = and i1 %tmp_27, %tmp_28
  %sign = select i1 %tmp_29, float -1.000000e+00, float 1.000000e+00
  %tmp_9 = zext i5 %j to i64
  %Kvalues_addr = getelementptr inbounds [16 x float]* @Kvalues, i64 0, i64 %tmp_9
  %Kvalues_load = load float* %Kvalues_addr, align 4
  %tmp_s = fmul float %curr_Q1, %Kvalues_load
  %tmp_1 = fmul float %sign, %tmp_s
  %curr_I = fsub float %temp_I, %tmp_1
  %tmp_2 = fmul float %temp_I, %Kvalues_load
  %tmp_3 = fmul float %sign, %tmp_2
  %curr_Q = fadd float %tmp_3, %curr_Q1
  %angles_addr = getelementptr inbounds [16 x float]* @angles, i64 0, i64 %tmp_9
  %angles_load = load float* %angles_addr, align 4
  %tmp_4 = fmul float %sign, %angles_load
  %curr_T = fsub float %curr_T1, %tmp_4
  br label %correctQuadrant.exit

; <label>:1                                       ; preds = %correctQuadrant.exit
  call void @_ssdm_op_Write.ap_auto.floatP(float* %theta, float %curr_T1) nounwind
  %r_assign = fmul float %temp_I, 0x3FE36DE460000000
  call void @_ssdm_op_Write.ap_auto.floatP(float* %r, float %r_assign) nounwind
  ret void
}

define weak void @_ssdm_op_Write.ap_auto.floatP(float*, float) {
entry:
  store float %1, float* %0
  ret void
}

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak float @_ssdm_op_Read.ap_auto.float(float) {
entry:
  ret float %0
}

define weak i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32, i32, i32) nounwind readnone {
entry:
  %empty = call i32 @llvm.part.select.i32(i32 %0, i32 %1, i32 %2)
  %empty_4 = trunc i32 %empty to i8
  ret i8 %empty_4
}

declare i23 @_ssdm_op_PartSelect.i23.i32.i32.i32(i32, i32, i32) nounwind readnone

declare i16 @_ssdm_op_HSub(...)

declare i16 @_ssdm_op_HMul(...)

declare i16 @_ssdm_op_HDiv(...)

declare i16 @_ssdm_op_HAdd(...)

declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [1 x i32]* @llvm_global_ctors_0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 0, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 31, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"x", metadata !11, metadata !"float", i32 0, i32 31}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 0, i32 0}
!13 = metadata !{metadata !14}
!14 = metadata !{i32 0, i32 31, metadata !15}
!15 = metadata !{metadata !16}
!16 = metadata !{metadata !"y", metadata !11, metadata !"float", i32 0, i32 31}
!17 = metadata !{metadata !18}
!18 = metadata !{i32 0, i32 31, metadata !19}
!19 = metadata !{metadata !20}
!20 = metadata !{metadata !"r", metadata !5, metadata !"float", i32 0, i32 31}
!21 = metadata !{metadata !22}
!22 = metadata !{i32 0, i32 31, metadata !23}
!23 = metadata !{metadata !24}
!24 = metadata !{metadata !"theta", metadata !5, metadata !"float", i32 0, i32 31}
