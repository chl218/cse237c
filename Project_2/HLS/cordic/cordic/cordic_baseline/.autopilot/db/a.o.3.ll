; ModuleID = 'D:/Projects/vivado/Project_2/HLS/cordic/cordic/cordic_baseline/.autopilot/db/a.o.3.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a] ; [#uses=0 type=[1 x void ()*]*]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535] ; [#uses=0 type=[1 x i32]*]
@cordiccart2pol_str = internal unnamed_addr constant [15 x i8] c"cordiccart2pol\00" ; [#uses=1 type=[15 x i8]*]
@angles = constant [16 x float] [float 0x3FE921FB60000000, float 0x3FDDAC6700000000, float 0x3FCF5B7600000000, float 0x3FBFD5BAA0000000, float 0x3FAFF55BC0000000, float 0x3F9FFD55C0000000, float 0x3F8FFF5560000000, float 0x3F7FFFD560000000, float 0x3F6FFFF560000000, float 0x3F5FFFFD60000000, float 0x3F4FFFFF60000000, float 0x3F3FFFFFE0000000, float 0x3F30000000000000, float 0x3F20000000000000, float 0x3F10000000000000, float 0x3F00000000000000], align 16 ; [#uses=1 type=[16 x float]*]
@Kvalues = constant [16 x float] [float 1.000000e+00, float 5.000000e-01, float 2.500000e-01, float 1.250000e-01, float 6.250000e-02, float 3.125000e-02, float 1.562500e-02, float 7.812500e-03, float 3.906250e-03, float 1.953125e-03, float 9.765625e-04, float 0x3F40000000000000, float 0x3F30000000000000, float 0x3F20000000000000, float 0x3F10000000000000, float 0x3F00000000000000], align 16 ; [#uses=1 type=[16 x float]*]

; [#uses=1]
declare i32 @llvm.part.select.i32(i32, i32, i32) nounwind readnone

; [#uses=18]
declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

; [#uses=0]
define void @cordiccart2pol(float %x, float %y, float* %r, float* %theta) nounwind uwtable {
_ifconv:
  call void (...)* @_ssdm_op_SpecBitsMap(float %x) nounwind, !map !7
  call void (...)* @_ssdm_op_SpecBitsMap(float %y) nounwind, !map !13
  call void (...)* @_ssdm_op_SpecBitsMap(float* %r) nounwind, !map !17
  call void (...)* @_ssdm_op_SpecBitsMap(float* %theta) nounwind, !map !21
  call void (...)* @_ssdm_op_SpecTopModule([15 x i8]* @cordiccart2pol_str) nounwind
  %y_read = call float @_ssdm_op_Read.ap_auto.float(float %y) nounwind ; [#uses=4 type=float]
  call void @llvm.dbg.value(metadata !{float %y_read}, i64 0, metadata !25), !dbg !35 ; [debug line = 25:38] [debug variable = y]
  call void @llvm.dbg.value(metadata !{float %y_read}, i64 0, metadata !36), !dbg !42 ; [debug line = 7:56@33:2] [debug variable = y]
  %x_read = call float @_ssdm_op_Read.ap_auto.float(float %x) nounwind ; [#uses=4 type=float]
  call void @llvm.dbg.value(metadata !{float %x_read}, i64 0, metadata !43), !dbg !44 ; [debug line = 25:28] [debug variable = x]
  call void @llvm.dbg.value(metadata !{float %x_read}, i64 0, metadata !45), !dbg !46 ; [debug line = 7:45@33:2] [debug variable = x]
  call void @llvm.dbg.value(metadata !{float %x}, i64 0, metadata !43), !dbg !44 ; [debug line = 25:28] [debug variable = x]
  call void @llvm.dbg.value(metadata !{float %y}, i64 0, metadata !25), !dbg !35 ; [debug line = 25:38] [debug variable = y]
  call void @llvm.dbg.value(metadata !{float* %r}, i64 0, metadata !47), !dbg !48 ; [debug line = 25:50] [debug variable = r]
  call void @llvm.dbg.value(metadata !{float* %theta}, i64 0, metadata !49), !dbg !50 ; [debug line = 25:62] [debug variable = theta]
  call void @llvm.dbg.value(metadata !{float %x}, i64 0, metadata !51), !dbg !52 ; [debug line = 27:19] [debug variable = curr_I]
  call void @llvm.dbg.value(metadata !{float %y}, i64 0, metadata !53), !dbg !54 ; [debug line = 28:19] [debug variable = curr_Q]
  call void @llvm.dbg.value(metadata !{float %x}, i64 0, metadata !45), !dbg !46 ; [debug line = 7:45@33:2] [debug variable = x]
  call void @llvm.dbg.value(metadata !{float %y}, i64 0, metadata !36), !dbg !42 ; [debug line = 7:56@33:2] [debug variable = y]
  call void @llvm.dbg.value(metadata !{float %x}, i64 0, metadata !55), !dbg !57 ; [debug line = 8:17@33:2] [debug variable = tmp]
  %x_to_int = bitcast float %x_read to i32        ; [#uses=3 type=i32]
  %tmp = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %x_to_int, i32 23, i32 30) ; [#uses=1 type=i8]
  %tmp_5 = trunc i32 %x_to_int to i23             ; [#uses=1 type=i23]
  %notlhs = icmp ne i8 %tmp, -1                   ; [#uses=1 type=i1]
  %notrhs = icmp eq i23 %tmp_5, 0                 ; [#uses=1 type=i1]
  %tmp_6 = or i1 %notrhs, %notlhs                 ; [#uses=1 type=i1]
  %tmp_7 = fcmp olt float %x_read, 0.000000e+00, !dbg !58 ; [#uses=1 type=i1] [debug line = 11:2@33:2]
  %tmp_8 = and i1 %tmp_6, %tmp_7, !dbg !58        ; [#uses=1 type=i1] [debug line = 11:2@33:2]
  %y_to_int = bitcast float %y_read to i32        ; [#uses=2 type=i32]
  %tmp_10 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %y_to_int, i32 23, i32 30) ; [#uses=1 type=i8]
  %tmp_11 = trunc i32 %y_to_int to i23            ; [#uses=1 type=i23]
  %notlhs6 = icmp ne i8 %tmp_10, -1               ; [#uses=1 type=i1]
  %notrhs7 = icmp eq i23 %tmp_11, 0               ; [#uses=1 type=i1]
  %tmp_12 = or i1 %notrhs7, %notlhs6              ; [#uses=1 type=i1]
  %tmp_13 = fcmp ogt float %y_read, 0.000000e+00, !dbg !58 ; [#uses=1 type=i1] [debug line = 11:2@33:2]
  %tmp_14 = and i1 %tmp_12, %tmp_13, !dbg !58     ; [#uses=1 type=i1] [debug line = 11:2@33:2]
  %or_cond_i = and i1 %tmp_8, %tmp_14, !dbg !58   ; [#uses=3 type=i1] [debug line = 11:2@33:2]
  %tmp_3_neg_i = xor i32 %x_to_int, -2147483648, !dbg !59 ; [#uses=1 type=i32] [debug line = 13:3@33:2]
  %tmp_3_i = bitcast i32 %tmp_3_neg_i to float, !dbg !59 ; [#uses=1 type=float] [debug line = 13:3@33:2]
  %theta_i = select i1 %or_cond_i, float 0x3FF921FB60000000, float 0.000000e+00 ; [#uses=2 type=float]
  %x_i = select i1 %or_cond_i, float %y_read, float %x_read ; [#uses=3 type=float]
  %y_i = select i1 %or_cond_i, float %tmp_3_i, float %y_read ; [#uses=3 type=float]
  %x_i_to_int = bitcast float %x_i to i32         ; [#uses=2 type=i32]
  %tmp_15 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %x_i_to_int, i32 23, i32 30) ; [#uses=1 type=i8]
  %tmp_16 = trunc i32 %x_i_to_int to i23          ; [#uses=1 type=i23]
  %notlhs8 = icmp ne i8 %tmp_15, -1               ; [#uses=1 type=i1]
  %notrhs9 = icmp eq i23 %tmp_16, 0               ; [#uses=1 type=i1]
  %tmp_17 = or i1 %notrhs9, %notlhs8              ; [#uses=1 type=i1]
  %tmp_18 = fcmp olt float %x_i, 0.000000e+00, !dbg !61 ; [#uses=1 type=i1] [debug line = 17:2@33:2]
  %tmp_19 = and i1 %tmp_17, %tmp_18, !dbg !61     ; [#uses=1 type=i1] [debug line = 17:2@33:2]
  %y_i_to_int = bitcast float %y_i to i32         ; [#uses=3 type=i32]
  %tmp_20 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %y_i_to_int, i32 23, i32 30) ; [#uses=1 type=i8]
  %tmp_21 = trunc i32 %y_i_to_int to i23          ; [#uses=1 type=i23]
  %notlhs1 = icmp ne i8 %tmp_20, -1               ; [#uses=1 type=i1]
  %notrhs1 = icmp eq i23 %tmp_21, 0               ; [#uses=1 type=i1]
  %tmp_22 = or i1 %notrhs1, %notlhs1              ; [#uses=1 type=i1]
  %tmp_23 = fcmp olt float %y_i, 0.000000e+00, !dbg !61 ; [#uses=1 type=i1] [debug line = 17:2@33:2]
  %tmp_24 = and i1 %tmp_22, %tmp_23, !dbg !61     ; [#uses=1 type=i1] [debug line = 17:2@33:2]
  %or_cond3_i = and i1 %tmp_19, %tmp_24, !dbg !61 ; [#uses=3 type=i1] [debug line = 17:2@33:2]
  %tmp_7_neg_i = xor i32 %y_i_to_int, -2147483648, !dbg !62 ; [#uses=1 type=i32] [debug line = 18:3@33:2]
  %tmp_7_i = bitcast i32 %tmp_7_neg_i to float, !dbg !62 ; [#uses=1 type=float] [debug line = 18:3@33:2]
  %tmp_8_i = fadd float %theta_i, 0xBFF921FB60000000, !dbg !64 ; [#uses=1 type=float] [debug line = 20:3@33:2]
  %curr_T1_ph = select i1 %or_cond3_i, float %tmp_8_i, float %theta_i ; [#uses=1 type=float]
  %curr_Q1_ph = select i1 %or_cond3_i, float %x_read, float %y_i ; [#uses=1 type=float]
  %temp_I_ph = select i1 %or_cond3_i, float %tmp_7_i, float %x_i ; [#uses=1 type=float]
  br label %correctQuadrant.exit, !dbg !65        ; [debug line = 35:15]

correctQuadrant.exit:                             ; preds = %0, %_ifconv
  %curr_T1 = phi float [ %curr_T, %0 ], [ %curr_T1_ph, %_ifconv ] ; [#uses=2 type=float]
  %curr_Q1 = phi float [ %curr_Q, %0 ], [ %curr_Q1_ph, %_ifconv ] ; [#uses=4 type=float]
  %temp_I = phi float [ %curr_I, %0 ], [ %temp_I_ph, %_ifconv ] ; [#uses=3 type=float]
  %j = phi i5 [ %j_1, %0 ], [ 0, %_ifconv ]       ; [#uses=3 type=i5]
  %exitcond = icmp eq i5 %j, -16, !dbg !65        ; [#uses=1 type=i1] [debug line = 35:15]
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 16, i64 16, i64 16) nounwind ; [#uses=0 type=i32]
  %j_1 = add i5 %j, 1, !dbg !67                   ; [#uses=1 type=i5] [debug line = 35:25]
  br i1 %exitcond, label %1, label %0, !dbg !65   ; [debug line = 35:15]

; <label>:0                                       ; preds = %correctQuadrant.exit
  call void @llvm.dbg.value(metadata !{float %temp_I}, i64 0, metadata !68), !dbg !69 ; [debug line = 36:3] [debug variable = temp_I]
  %curr_Q1_to_int = bitcast float %curr_Q1 to i32 ; [#uses=2 type=i32]
  %tmp_25 = call i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32 %curr_Q1_to_int, i32 23, i32 30) ; [#uses=1 type=i8]
  %tmp_26 = trunc i32 %curr_Q1_to_int to i23      ; [#uses=1 type=i23]
  %notlhs2 = icmp ne i8 %tmp_25, -1               ; [#uses=1 type=i1]
  %notrhs2 = icmp eq i23 %tmp_26, 0               ; [#uses=1 type=i1]
  %tmp_27 = or i1 %notrhs2, %notlhs2              ; [#uses=1 type=i1]
  %tmp_28 = fcmp oge float %curr_Q1, 0.000000e+00, !dbg !71 ; [#uses=1 type=i1] [debug line = 37:3]
  %tmp_29 = and i1 %tmp_27, %tmp_28, !dbg !71     ; [#uses=1 type=i1] [debug line = 37:3]
  %sign = select i1 %tmp_29, float -1.000000e+00, float 1.000000e+00, !dbg !71 ; [#uses=3 type=float] [debug line = 37:3]
  %tmp_9 = zext i5 %j to i64, !dbg !72            ; [#uses=2 type=i64] [debug line = 42:3]
  %Kvalues_addr = getelementptr inbounds [16 x float]* @Kvalues, i64 0, i64 %tmp_9, !dbg !72 ; [#uses=1 type=float*] [debug line = 42:3]
  %Kvalues_load = load float* %Kvalues_addr, align 4, !dbg !72 ; [#uses=2 type=float] [debug line = 42:3]
  %tmp_s = fmul float %curr_Q1, %Kvalues_load, !dbg !72 ; [#uses=1 type=float] [debug line = 42:3]
  %tmp_1 = fmul float %sign, %tmp_s, !dbg !72     ; [#uses=1 type=float] [debug line = 42:3]
  %curr_I = fsub float %temp_I, %tmp_1, !dbg !72  ; [#uses=1 type=float] [debug line = 42:3]
  call void @llvm.dbg.value(metadata !{float %curr_I}, i64 0, metadata !51), !dbg !72 ; [debug line = 42:3] [debug variable = curr_I]
  %tmp_2 = fmul float %temp_I, %Kvalues_load, !dbg !73 ; [#uses=1 type=float] [debug line = 43:3]
  %tmp_3 = fmul float %sign, %tmp_2, !dbg !73     ; [#uses=1 type=float] [debug line = 43:3]
  %curr_Q = fadd float %tmp_3, %curr_Q1, !dbg !73 ; [#uses=1 type=float] [debug line = 43:3]
  call void @llvm.dbg.value(metadata !{float %curr_Q}, i64 0, metadata !53), !dbg !73 ; [debug line = 43:3] [debug variable = curr_Q]
  %angles_addr = getelementptr inbounds [16 x float]* @angles, i64 0, i64 %tmp_9, !dbg !74 ; [#uses=1 type=float*] [debug line = 44:3]
  %angles_load = load float* %angles_addr, align 4, !dbg !74 ; [#uses=1 type=float] [debug line = 44:3]
  %tmp_4 = fmul float %sign, %angles_load, !dbg !74 ; [#uses=1 type=float] [debug line = 44:3]
  %curr_T = fsub float %curr_T1, %tmp_4, !dbg !74 ; [#uses=1 type=float] [debug line = 44:3]
  call void @llvm.dbg.value(metadata !{float %curr_T}, i64 0, metadata !75), !dbg !74 ; [debug line = 44:3] [debug variable = curr_T]
  call void @llvm.dbg.value(metadata !{i5 %j_1}, i64 0, metadata !76), !dbg !67 ; [debug line = 35:25] [debug variable = j]
  br label %correctQuadrant.exit, !dbg !67        ; [debug line = 35:25]

; <label>:1                                       ; preds = %correctQuadrant.exit
  call void @_ssdm_op_Write.ap_auto.floatP(float* %theta, float %curr_T1) nounwind, !dbg !78 ; [debug line = 47:2]
  %r_assign = fmul float %temp_I, 0x3FE36DE460000000, !dbg !79 ; [#uses=1 type=float] [debug line = 48:2]
  call void @_ssdm_op_Write.ap_auto.floatP(float* %r, float %r_assign) nounwind, !dbg !79 ; [debug line = 48:2]
  ret void, !dbg !80                              ; [debug line = 49:1]
}

; [#uses=2]
define weak void @_ssdm_op_Write.ap_auto.floatP(float*, float) {
entry:
  store float %1, float* %0
  ret void
}

; [#uses=1]
define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

; [#uses=1]
define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

; [#uses=4]
define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

; [#uses=2]
define weak float @_ssdm_op_Read.ap_auto.float(float) {
entry:
  ret float %0
}

; [#uses=5]
define weak i8 @_ssdm_op_PartSelect.i8.i32.i32.i32(i32, i32, i32) nounwind readnone {
entry:
  %empty = call i32 @llvm.part.select.i32(i32 %0, i32 %1, i32 %2) ; [#uses=1 type=i32]
  %empty_4 = trunc i32 %empty to i8               ; [#uses=1 type=i8]
  ret i8 %empty_4
}

; [#uses=0]
declare i23 @_ssdm_op_PartSelect.i23.i32.i32.i32(i32, i32, i32) nounwind readnone

; [#uses=0]
declare i16 @_ssdm_op_HSub(...)

; [#uses=0]
declare i16 @_ssdm_op_HMul(...)

; [#uses=0]
declare i16 @_ssdm_op_HDiv(...)

; [#uses=0]
declare i16 @_ssdm_op_HAdd(...)

; [#uses=1]
declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [1 x i32]* @llvm_global_ctors_0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 0, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 31, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"x", metadata !11, metadata !"float", i32 0, i32 31}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 0, i32 0}
!13 = metadata !{metadata !14}
!14 = metadata !{i32 0, i32 31, metadata !15}
!15 = metadata !{metadata !16}
!16 = metadata !{metadata !"y", metadata !11, metadata !"float", i32 0, i32 31}
!17 = metadata !{metadata !18}
!18 = metadata !{i32 0, i32 31, metadata !19}
!19 = metadata !{metadata !20}
!20 = metadata !{metadata !"r", metadata !5, metadata !"float", i32 0, i32 31}
!21 = metadata !{metadata !22}
!22 = metadata !{i32 0, i32 31, metadata !23}
!23 = metadata !{metadata !24}
!24 = metadata !{metadata !"theta", metadata !5, metadata !"float", i32 0, i32 31}
!25 = metadata !{i32 786689, metadata !26, metadata !"y", metadata !27, i32 33554457, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!26 = metadata !{i32 786478, i32 0, metadata !27, metadata !"cordiccart2pol", metadata !"cordiccart2pol", metadata !"_Z14cordiccart2polffPfS_", metadata !27, i32 25, metadata !28, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, void (float, float, float*, float*)* @cordiccart2pol, null, null, metadata !33, i32 26} ; [ DW_TAG_subprogram ]
!27 = metadata !{i32 786473, metadata !"cordiccart2pol.cpp", metadata !"d:/Projects/vivado/Project_2/HLS/cordic", null} ; [ DW_TAG_file_type ]
!28 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !29, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!29 = metadata !{null, metadata !30, metadata !30, metadata !32, metadata !32}
!30 = metadata !{i32 786454, null, metadata !"data_t", metadata !27, i32 10, i64 0, i64 0, i64 0, i32 0, metadata !31} ; [ DW_TAG_typedef ]
!31 = metadata !{i32 786468, null, metadata !"float", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 4} ; [ DW_TAG_base_type ]
!32 = metadata !{i32 786447, null, metadata !"", null, i32 0, i64 64, i64 64, i64 0, i32 0, metadata !30} ; [ DW_TAG_pointer_type ]
!33 = metadata !{metadata !34}
!34 = metadata !{i32 786468}                      ; [ DW_TAG_base_type ]
!35 = metadata !{i32 25, i32 38, metadata !26, null}
!36 = metadata !{i32 790532, metadata !37, metadata !"y", null, i32 7, metadata !32, i32 0, metadata !40} ; [ DW_TAG_arg_variable_ro ]
!37 = metadata !{i32 786478, i32 0, metadata !27, metadata !"correctQuadrant", metadata !"correctQuadrant", metadata !"_Z15correctQuadrantPfS_S_", metadata !27, i32 7, metadata !38, i1 false, i1 true, i32 0, i32 0, null, i32 256, i1 false, null, null, null, metadata !33, i32 7} ; [ DW_TAG_subprogram ]
!38 = metadata !{i32 786453, i32 0, metadata !"", i32 0, i32 0, i64 0, i64 0, i64 0, i32 0, null, metadata !39, i32 0, i32 0} ; [ DW_TAG_subroutine_type ]
!39 = metadata !{null, metadata !32, metadata !32, metadata !32}
!40 = metadata !{i32 33, i32 2, metadata !41, null}
!41 = metadata !{i32 786443, metadata !26, i32 26, i32 1, metadata !27, i32 3} ; [ DW_TAG_lexical_block ]
!42 = metadata !{i32 7, i32 56, metadata !37, metadata !40}
!43 = metadata !{i32 786689, metadata !26, metadata !"x", metadata !27, i32 16777241, metadata !30, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!44 = metadata !{i32 25, i32 28, metadata !26, null}
!45 = metadata !{i32 790532, metadata !37, metadata !"x", null, i32 7, metadata !32, i32 0, metadata !40} ; [ DW_TAG_arg_variable_ro ]
!46 = metadata !{i32 7, i32 45, metadata !37, metadata !40}
!47 = metadata !{i32 786689, metadata !26, metadata !"r", metadata !27, i32 50331673, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!48 = metadata !{i32 25, i32 50, metadata !26, null}
!49 = metadata !{i32 786689, metadata !26, metadata !"theta", metadata !27, i32 67108889, metadata !32, i32 0, i32 0} ; [ DW_TAG_arg_variable ]
!50 = metadata !{i32 25, i32 62, metadata !26, null}
!51 = metadata !{i32 786688, metadata !41, metadata !"curr_I", metadata !27, i32 27, metadata !30, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!52 = metadata !{i32 27, i32 19, metadata !41, null}
!53 = metadata !{i32 786688, metadata !41, metadata !"curr_Q", metadata !27, i32 28, metadata !30, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!54 = metadata !{i32 28, i32 19, metadata !41, null}
!55 = metadata !{i32 786688, metadata !56, metadata !"tmp", metadata !27, i32 8, metadata !30, i32 0, metadata !40} ; [ DW_TAG_auto_variable ]
!56 = metadata !{i32 786443, metadata !37, i32 7, i32 59, metadata !27, i32 0} ; [ DW_TAG_lexical_block ]
!57 = metadata !{i32 8, i32 17, metadata !56, metadata !40}
!58 = metadata !{i32 11, i32 2, metadata !56, metadata !40}
!59 = metadata !{i32 13, i32 3, metadata !60, metadata !40}
!60 = metadata !{i32 786443, metadata !56, i32 11, i32 23, metadata !27, i32 1} ; [ DW_TAG_lexical_block ]
!61 = metadata !{i32 17, i32 2, metadata !56, metadata !40}
!62 = metadata !{i32 18, i32 3, metadata !63, metadata !40}
!63 = metadata !{i32 786443, metadata !56, i32 17, i32 23, metadata !27, i32 2} ; [ DW_TAG_lexical_block ]
!64 = metadata !{i32 20, i32 3, metadata !63, metadata !40}
!65 = metadata !{i32 35, i32 15, metadata !66, null}
!66 = metadata !{i32 786443, metadata !41, i32 35, i32 2, metadata !27, i32 4} ; [ DW_TAG_lexical_block ]
!67 = metadata !{i32 35, i32 25, metadata !66, null}
!68 = metadata !{i32 786688, metadata !41, metadata !"temp_I", metadata !27, i32 30, metadata !30, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!69 = metadata !{i32 36, i32 3, metadata !70, null}
!70 = metadata !{i32 786443, metadata !66, i32 35, i32 30, metadata !27, i32 5} ; [ DW_TAG_lexical_block ]
!71 = metadata !{i32 37, i32 3, metadata !70, null}
!72 = metadata !{i32 42, i32 3, metadata !70, null}
!73 = metadata !{i32 43, i32 3, metadata !70, null}
!74 = metadata !{i32 44, i32 3, metadata !70, null}
!75 = metadata !{i32 786688, metadata !41, metadata !"curr_T", metadata !27, i32 29, metadata !30, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!76 = metadata !{i32 786688, metadata !66, metadata !"j", metadata !27, i32 35, metadata !77, i32 0, i32 0} ; [ DW_TAG_auto_variable ]
!77 = metadata !{i32 786468, null, metadata !"int", null, i32 0, i64 32, i64 32, i64 0, i32 0, i32 5} ; [ DW_TAG_base_type ]
!78 = metadata !{i32 47, i32 2, metadata !41, null}
!79 = metadata !{i32 48, i32 2, metadata !41, null}
!80 = metadata !{i32 49, i32 1, metadata !41, null}
