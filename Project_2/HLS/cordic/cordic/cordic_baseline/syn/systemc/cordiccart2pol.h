// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

#ifndef _cordiccart2pol_HH_
#define _cordiccart2pol_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "cordiccart2pol_faddfsub_32ns_32ns_32_5_full_dsp.h"
#include "cordiccart2pol_fadd_32ns_32ns_32_5_full_dsp.h"
#include "cordiccart2pol_fmul_32ns_32ns_32_4_max_dsp.h"
#include "cordiccart2pol_fcmp_32ns_32ns_1_1.h"
#include "cordiccart2pol_Kvalues.h"
#include "cordiccart2pol_angles.h"

namespace ap_rtl {

struct cordiccart2pol : public sc_module {
    // Port declarations 12
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<32> > x;
    sc_in< sc_lv<32> > y;
    sc_out< sc_lv<32> > r;
    sc_out< sc_logic > r_ap_vld;
    sc_out< sc_lv<32> > theta;
    sc_out< sc_logic > theta_ap_vld;


    // Module declarations
    cordiccart2pol(sc_module_name name);
    SC_HAS_PROCESS(cordiccart2pol);

    ~cordiccart2pol();

    sc_trace_file* mVcdFile;

    ofstream mHdltvinHandle;
    ofstream mHdltvoutHandle;
    cordiccart2pol_Kvalues* Kvalues_U;
    cordiccart2pol_angles* angles_U;
    cordiccart2pol_faddfsub_32ns_32ns_32_5_full_dsp<1,5,32,32,32>* cordiccart2pol_faddfsub_32ns_32ns_32_5_full_dsp_U0;
    cordiccart2pol_fadd_32ns_32ns_32_5_full_dsp<1,5,32,32,32>* cordiccart2pol_fadd_32ns_32ns_32_5_full_dsp_U1;
    cordiccart2pol_fmul_32ns_32ns_32_4_max_dsp<1,4,32,32,32>* cordiccart2pol_fmul_32ns_32ns_32_4_max_dsp_U2;
    cordiccart2pol_fmul_32ns_32ns_32_4_max_dsp<1,4,32,32,32>* cordiccart2pol_fmul_32ns_32ns_32_4_max_dsp_U3;
    cordiccart2pol_fmul_32ns_32ns_32_4_max_dsp<1,4,32,32,32>* cordiccart2pol_fmul_32ns_32ns_32_4_max_dsp_U4;
    cordiccart2pol_fcmp_32ns_32ns_1_1<1,1,32,32,1>* cordiccart2pol_fcmp_32ns_32ns_1_1_U5;
    cordiccart2pol_fcmp_32ns_32ns_1_1<1,1,32,32,1>* cordiccart2pol_fcmp_32ns_32ns_1_1_U6;
    sc_signal< sc_lv<27> > ap_CS_fsm;
    sc_signal< sc_logic > ap_sig_cseq_ST_st1_fsm_0;
    sc_signal< bool > ap_sig_bdd_43;
    sc_signal< sc_lv<4> > Kvalues_address0;
    sc_signal< sc_logic > Kvalues_ce0;
    sc_signal< sc_lv<32> > Kvalues_q0;
    sc_signal< sc_lv<4> > angles_address0;
    sc_signal< sc_logic > angles_ce0;
    sc_signal< sc_lv<32> > angles_q0;
    sc_signal< sc_lv<32> > grp_fu_162_p2;
    sc_signal< sc_lv<32> > reg_195;
    sc_signal< sc_logic > ap_sig_cseq_ST_st15_fsm_14;
    sc_signal< bool > ap_sig_bdd_77;
    sc_signal< sc_logic > ap_sig_cseq_ST_st19_fsm_18;
    sc_signal< bool > ap_sig_bdd_84;
    sc_signal< sc_lv<32> > grp_fu_171_p2;
    sc_signal< sc_lv<32> > reg_201;
    sc_signal< sc_lv<1> > grp_fu_182_p2;
    sc_signal< sc_lv<1> > tmp_7_reg_520;
    sc_signal< sc_lv<1> > grp_fu_188_p2;
    sc_signal< sc_lv<1> > tmp_13_reg_525;
    sc_signal< sc_lv<1> > or_cond_i_fu_287_p2;
    sc_signal< sc_lv<1> > or_cond_i_reg_530;
    sc_signal< sc_logic > ap_sig_cseq_ST_st2_fsm_1;
    sc_signal< bool > ap_sig_bdd_107;
    sc_signal< sc_lv<32> > tmp_3_neg_i_fu_293_p2;
    sc_signal< sc_lv<32> > tmp_3_neg_i_reg_537;
    sc_signal< sc_lv<32> > x_i_fu_302_p3;
    sc_signal< sc_lv<32> > x_i_reg_542;
    sc_signal< sc_logic > ap_sig_cseq_ST_st3_fsm_2;
    sc_signal< bool > ap_sig_bdd_118;
    sc_signal< sc_lv<32> > y_i_fu_308_p3;
    sc_signal< sc_lv<32> > y_i_reg_548;
    sc_signal< sc_lv<1> > tmp_18_reg_554;
    sc_signal< sc_lv<1> > tmp_23_reg_559;
    sc_signal< sc_lv<1> > or_cond3_i_fu_395_p2;
    sc_signal< sc_lv<1> > or_cond3_i_reg_564;
    sc_signal< sc_logic > ap_sig_cseq_ST_st4_fsm_3;
    sc_signal< bool > ap_sig_bdd_131;
    sc_signal< sc_lv<32> > curr_Q1_ph_fu_411_p3;
    sc_signal< sc_lv<32> > curr_Q1_ph_reg_569;
    sc_signal< sc_lv<32> > temp_I_ph_fu_417_p3;
    sc_signal< sc_lv<32> > temp_I_ph_reg_574;
    sc_signal< sc_lv<32> > theta_i_fu_424_p3;
    sc_signal< sc_lv<32> > theta_i_reg_579;
    sc_signal< sc_logic > ap_sig_cseq_ST_st5_fsm_4;
    sc_signal< bool > ap_sig_bdd_144;
    sc_signal< sc_lv<32> > curr_T1_ph_fu_432_p3;
    sc_signal< sc_lv<32> > curr_T1_ph_reg_585;
    sc_signal< sc_logic > ap_sig_cseq_ST_st9_fsm_8;
    sc_signal< bool > ap_sig_bdd_153;
    sc_signal< sc_lv<5> > j_1_fu_444_p2;
    sc_signal< sc_lv<5> > j_1_reg_593;
    sc_signal< sc_logic > ap_sig_cseq_ST_st11_fsm_10;
    sc_signal< bool > ap_sig_bdd_162;
    sc_signal< sc_lv<1> > tmp_29_fu_486_p2;
    sc_signal< sc_lv<1> > tmp_29_reg_598;
    sc_signal< sc_lv<1> > exitcond_fu_438_p2;
    sc_signal< sc_lv<32> > sign_fu_498_p3;
    sc_signal< sc_lv<32> > sign_reg_613;
    sc_signal< sc_logic > ap_sig_cseq_ST_st12_fsm_11;
    sc_signal< bool > ap_sig_bdd_181;
    sc_signal< sc_lv<32> > grp_fu_177_p2;
    sc_signal< sc_lv<32> > tmp_4_reg_631;
    sc_signal< sc_lv<32> > grp_fu_150_p2;
    sc_signal< sc_lv<32> > curr_T_reg_636;
    sc_signal< sc_logic > ap_sig_cseq_ST_st20_fsm_19;
    sc_signal< bool > ap_sig_bdd_194;
    sc_signal< sc_logic > ap_sig_cseq_ST_st24_fsm_23;
    sc_signal< bool > ap_sig_bdd_202;
    sc_signal< sc_lv<32> > grp_fu_157_p2;
    sc_signal< sc_lv<32> > curr_T1_reg_108;
    sc_signal< sc_logic > ap_sig_cseq_ST_st10_fsm_9;
    sc_signal< bool > ap_sig_bdd_213;
    sc_signal< sc_lv<32> > curr_Q1_reg_119;
    sc_signal< sc_lv<32> > temp_I_reg_129;
    sc_signal< sc_lv<5> > j_reg_139;
    sc_signal< sc_lv<64> > tmp_9_fu_492_p1;
    sc_signal< sc_logic > ap_sig_cseq_ST_st27_fsm_26;
    sc_signal< bool > ap_sig_bdd_228;
    sc_signal< sc_lv<32> > grp_fu_150_p0;
    sc_signal< sc_lv<32> > grp_fu_150_p1;
    sc_signal< sc_logic > ap_sig_cseq_ST_st16_fsm_15;
    sc_signal< bool > ap_sig_bdd_238;
    sc_signal< sc_lv<32> > grp_fu_162_p0;
    sc_signal< sc_lv<32> > grp_fu_162_p1;
    sc_signal< sc_lv<32> > grp_fu_171_p0;
    sc_signal< sc_lv<32> > grp_fu_171_p1;
    sc_signal< sc_lv<32> > grp_fu_177_p0;
    sc_signal< sc_lv<32> > grp_fu_182_p0;
    sc_signal< sc_lv<32> > grp_fu_188_p0;
    sc_signal< sc_lv<32> > x_to_int_fu_207_p1;
    sc_signal< sc_lv<8> > tmp_fu_210_p4;
    sc_signal< sc_lv<23> > tmp_5_fu_220_p1;
    sc_signal< sc_lv<1> > notrhs_fu_230_p2;
    sc_signal< sc_lv<1> > notlhs_fu_224_p2;
    sc_signal< sc_lv<1> > tmp_6_fu_236_p2;
    sc_signal< sc_lv<32> > y_to_int_fu_247_p1;
    sc_signal< sc_lv<8> > tmp_10_fu_250_p4;
    sc_signal< sc_lv<23> > tmp_11_fu_260_p1;
    sc_signal< sc_lv<1> > notrhs7_fu_270_p2;
    sc_signal< sc_lv<1> > notlhs6_fu_264_p2;
    sc_signal< sc_lv<1> > tmp_12_fu_276_p2;
    sc_signal< sc_lv<1> > tmp_8_fu_242_p2;
    sc_signal< sc_lv<1> > tmp_14_fu_282_p2;
    sc_signal< sc_lv<32> > tmp_3_i_fu_299_p1;
    sc_signal< sc_lv<32> > x_i_to_int_fu_315_p1;
    sc_signal< sc_lv<8> > tmp_15_fu_318_p4;
    sc_signal< sc_lv<23> > tmp_16_fu_328_p1;
    sc_signal< sc_lv<1> > notrhs9_fu_338_p2;
    sc_signal< sc_lv<1> > notlhs8_fu_332_p2;
    sc_signal< sc_lv<1> > tmp_17_fu_344_p2;
    sc_signal< sc_lv<32> > y_i_to_int_fu_355_p1;
    sc_signal< sc_lv<8> > tmp_20_fu_358_p4;
    sc_signal< sc_lv<23> > tmp_21_fu_368_p1;
    sc_signal< sc_lv<1> > notrhs1_fu_378_p2;
    sc_signal< sc_lv<1> > notlhs1_fu_372_p2;
    sc_signal< sc_lv<1> > tmp_22_fu_384_p2;
    sc_signal< sc_lv<1> > tmp_19_fu_350_p2;
    sc_signal< sc_lv<1> > tmp_24_fu_390_p2;
    sc_signal< sc_lv<32> > tmp_7_neg_i_fu_401_p2;
    sc_signal< sc_lv<32> > tmp_7_i_fu_407_p1;
    sc_signal< sc_lv<32> > curr_Q1_to_int_fu_450_p1;
    sc_signal< sc_lv<8> > tmp_25_fu_454_p4;
    sc_signal< sc_lv<23> > tmp_26_fu_464_p1;
    sc_signal< sc_lv<1> > notrhs2_fu_474_p2;
    sc_signal< sc_lv<1> > notlhs2_fu_468_p2;
    sc_signal< sc_lv<1> > tmp_27_fu_480_p2;
    sc_signal< sc_lv<2> > grp_fu_150_opcode;
    sc_signal< sc_logic > grp_fu_150_ce;
    sc_signal< sc_logic > grp_fu_157_ce;
    sc_signal< sc_logic > grp_fu_162_ce;
    sc_signal< sc_logic > grp_fu_171_ce;
    sc_signal< sc_logic > grp_fu_177_ce;
    sc_signal< sc_lv<5> > grp_fu_182_opcode;
    sc_signal< sc_lv<5> > grp_fu_188_opcode;
    sc_signal< sc_lv<27> > ap_NS_fsm;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<27> ap_ST_st1_fsm_0;
    static const sc_lv<27> ap_ST_st2_fsm_1;
    static const sc_lv<27> ap_ST_st3_fsm_2;
    static const sc_lv<27> ap_ST_st4_fsm_3;
    static const sc_lv<27> ap_ST_st5_fsm_4;
    static const sc_lv<27> ap_ST_st6_fsm_5;
    static const sc_lv<27> ap_ST_st7_fsm_6;
    static const sc_lv<27> ap_ST_st8_fsm_7;
    static const sc_lv<27> ap_ST_st9_fsm_8;
    static const sc_lv<27> ap_ST_st10_fsm_9;
    static const sc_lv<27> ap_ST_st11_fsm_10;
    static const sc_lv<27> ap_ST_st12_fsm_11;
    static const sc_lv<27> ap_ST_st13_fsm_12;
    static const sc_lv<27> ap_ST_st14_fsm_13;
    static const sc_lv<27> ap_ST_st15_fsm_14;
    static const sc_lv<27> ap_ST_st16_fsm_15;
    static const sc_lv<27> ap_ST_st17_fsm_16;
    static const sc_lv<27> ap_ST_st18_fsm_17;
    static const sc_lv<27> ap_ST_st19_fsm_18;
    static const sc_lv<27> ap_ST_st20_fsm_19;
    static const sc_lv<27> ap_ST_st21_fsm_20;
    static const sc_lv<27> ap_ST_st22_fsm_21;
    static const sc_lv<27> ap_ST_st23_fsm_22;
    static const sc_lv<27> ap_ST_st24_fsm_23;
    static const sc_lv<27> ap_ST_st25_fsm_24;
    static const sc_lv<27> ap_ST_st26_fsm_25;
    static const sc_lv<27> ap_ST_st27_fsm_26;
    static const sc_lv<32> ap_const_lv32_0;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<32> ap_const_lv32_E;
    static const sc_lv<32> ap_const_lv32_12;
    static const sc_lv<32> ap_const_lv32_1;
    static const sc_lv<32> ap_const_lv32_2;
    static const sc_lv<32> ap_const_lv32_3;
    static const sc_lv<32> ap_const_lv32_4;
    static const sc_lv<32> ap_const_lv32_8;
    static const sc_lv<32> ap_const_lv32_A;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<32> ap_const_lv32_B;
    static const sc_lv<32> ap_const_lv32_13;
    static const sc_lv<32> ap_const_lv32_17;
    static const sc_lv<32> ap_const_lv32_9;
    static const sc_lv<5> ap_const_lv5_0;
    static const sc_lv<32> ap_const_lv32_1A;
    static const sc_lv<32> ap_const_lv32_BFC90FDB;
    static const sc_lv<32> ap_const_lv32_F;
    static const sc_lv<32> ap_const_lv32_3F1B6F23;
    static const sc_lv<32> ap_const_lv32_1E;
    static const sc_lv<8> ap_const_lv8_FF;
    static const sc_lv<23> ap_const_lv23_0;
    static const sc_lv<32> ap_const_lv32_80000000;
    static const sc_lv<32> ap_const_lv32_3FC90FDB;
    static const sc_lv<5> ap_const_lv5_10;
    static const sc_lv<5> ap_const_lv5_1;
    static const sc_lv<32> ap_const_lv32_BF800000;
    static const sc_lv<32> ap_const_lv32_3F800000;
    static const sc_lv<2> ap_const_lv2_0;
    static const sc_lv<2> ap_const_lv2_1;
    static const sc_lv<5> ap_const_lv5_4;
    static const sc_lv<5> ap_const_lv5_3;
    static const sc_lv<5> ap_const_lv5_2;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_Kvalues_address0();
    void thread_Kvalues_ce0();
    void thread_angles_address0();
    void thread_angles_ce0();
    void thread_ap_done();
    void thread_ap_idle();
    void thread_ap_ready();
    void thread_ap_sig_bdd_107();
    void thread_ap_sig_bdd_118();
    void thread_ap_sig_bdd_131();
    void thread_ap_sig_bdd_144();
    void thread_ap_sig_bdd_153();
    void thread_ap_sig_bdd_162();
    void thread_ap_sig_bdd_181();
    void thread_ap_sig_bdd_194();
    void thread_ap_sig_bdd_202();
    void thread_ap_sig_bdd_213();
    void thread_ap_sig_bdd_228();
    void thread_ap_sig_bdd_238();
    void thread_ap_sig_bdd_43();
    void thread_ap_sig_bdd_77();
    void thread_ap_sig_bdd_84();
    void thread_ap_sig_cseq_ST_st10_fsm_9();
    void thread_ap_sig_cseq_ST_st11_fsm_10();
    void thread_ap_sig_cseq_ST_st12_fsm_11();
    void thread_ap_sig_cseq_ST_st15_fsm_14();
    void thread_ap_sig_cseq_ST_st16_fsm_15();
    void thread_ap_sig_cseq_ST_st19_fsm_18();
    void thread_ap_sig_cseq_ST_st1_fsm_0();
    void thread_ap_sig_cseq_ST_st20_fsm_19();
    void thread_ap_sig_cseq_ST_st24_fsm_23();
    void thread_ap_sig_cseq_ST_st27_fsm_26();
    void thread_ap_sig_cseq_ST_st2_fsm_1();
    void thread_ap_sig_cseq_ST_st3_fsm_2();
    void thread_ap_sig_cseq_ST_st4_fsm_3();
    void thread_ap_sig_cseq_ST_st5_fsm_4();
    void thread_ap_sig_cseq_ST_st9_fsm_8();
    void thread_curr_Q1_ph_fu_411_p3();
    void thread_curr_Q1_to_int_fu_450_p1();
    void thread_curr_T1_ph_fu_432_p3();
    void thread_exitcond_fu_438_p2();
    void thread_grp_fu_150_ce();
    void thread_grp_fu_150_opcode();
    void thread_grp_fu_150_p0();
    void thread_grp_fu_150_p1();
    void thread_grp_fu_157_ce();
    void thread_grp_fu_162_ce();
    void thread_grp_fu_162_p0();
    void thread_grp_fu_162_p1();
    void thread_grp_fu_171_ce();
    void thread_grp_fu_171_p0();
    void thread_grp_fu_171_p1();
    void thread_grp_fu_177_ce();
    void thread_grp_fu_177_p0();
    void thread_grp_fu_182_opcode();
    void thread_grp_fu_182_p0();
    void thread_grp_fu_188_opcode();
    void thread_grp_fu_188_p0();
    void thread_j_1_fu_444_p2();
    void thread_notlhs1_fu_372_p2();
    void thread_notlhs2_fu_468_p2();
    void thread_notlhs6_fu_264_p2();
    void thread_notlhs8_fu_332_p2();
    void thread_notlhs_fu_224_p2();
    void thread_notrhs1_fu_378_p2();
    void thread_notrhs2_fu_474_p2();
    void thread_notrhs7_fu_270_p2();
    void thread_notrhs9_fu_338_p2();
    void thread_notrhs_fu_230_p2();
    void thread_or_cond3_i_fu_395_p2();
    void thread_or_cond_i_fu_287_p2();
    void thread_r();
    void thread_r_ap_vld();
    void thread_sign_fu_498_p3();
    void thread_temp_I_ph_fu_417_p3();
    void thread_theta();
    void thread_theta_ap_vld();
    void thread_theta_i_fu_424_p3();
    void thread_tmp_10_fu_250_p4();
    void thread_tmp_11_fu_260_p1();
    void thread_tmp_12_fu_276_p2();
    void thread_tmp_14_fu_282_p2();
    void thread_tmp_15_fu_318_p4();
    void thread_tmp_16_fu_328_p1();
    void thread_tmp_17_fu_344_p2();
    void thread_tmp_19_fu_350_p2();
    void thread_tmp_20_fu_358_p4();
    void thread_tmp_21_fu_368_p1();
    void thread_tmp_22_fu_384_p2();
    void thread_tmp_24_fu_390_p2();
    void thread_tmp_25_fu_454_p4();
    void thread_tmp_26_fu_464_p1();
    void thread_tmp_27_fu_480_p2();
    void thread_tmp_29_fu_486_p2();
    void thread_tmp_3_i_fu_299_p1();
    void thread_tmp_3_neg_i_fu_293_p2();
    void thread_tmp_5_fu_220_p1();
    void thread_tmp_6_fu_236_p2();
    void thread_tmp_7_i_fu_407_p1();
    void thread_tmp_7_neg_i_fu_401_p2();
    void thread_tmp_8_fu_242_p2();
    void thread_tmp_9_fu_492_p1();
    void thread_tmp_fu_210_p4();
    void thread_x_i_fu_302_p3();
    void thread_x_i_to_int_fu_315_p1();
    void thread_x_to_int_fu_207_p1();
    void thread_y_i_fu_308_p3();
    void thread_y_i_to_int_fu_355_p1();
    void thread_y_to_int_fu_247_p1();
    void thread_ap_NS_fsm();
    void thread_hdltv_gen();
};

}

using namespace ap_rtl;

#endif
