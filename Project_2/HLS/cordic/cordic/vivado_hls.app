<project xmlns="com.autoesl.autopilot.project" name="cordic" top="cordiccart2pol">
    <includePaths/>
    <libraryPaths/>
    <Simulation>
        <SimFlow askAgain="false" name="csim" csimMode="0" lastCsimMode="0"/>
    </Simulation>
    <files xmlns="">
        <file name="../../cordiccart2pol_test.cpp" sc="0" tb="1" cflags=" "/>
        <file name="cordiccart2pol.cpp" sc="0" tb="false" cflags=""/>
        <file name="cordiccart2pol.h" sc="0" tb="false" cflags=""/>
    </files>
    <solutions xmlns="">
        <solution name="cordic_baseline" status="inactive"/>
        <solution name="cordic_baseline" status="inactive"/>
        <solution name="cordic_baseline" status="inactive"/>
        <solution name="cordic_optimized1" status="inactive"/>
        <solution name="cordic_baseline" status="inactive"/>
        <solution name="cordic_optimized2" status="inactive"/>
        <solution name="cordic_optimized3" status="active"/>
    </solutions>
</project>

