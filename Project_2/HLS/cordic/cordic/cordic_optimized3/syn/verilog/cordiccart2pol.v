// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

(* CORE_GENERATION_INFO="cordiccart2pol,hls_ip_2015_4,{HLS_INPUT_TYPE=cxx,HLS_INPUT_FLOAT=0,HLS_INPUT_FIXED=1,HLS_INPUT_PART=xc7z020clg484-1,HLS_INPUT_CLOCK=10.000000,HLS_INPUT_ARCH=others,HLS_SYN_CLOCK=7.881000,HLS_SYN_LAT=13,HLS_SYN_TPT=none,HLS_SYN_MEM=0,HLS_SYN_DSP=1,HLS_SYN_FF=113,HLS_SYN_LUT=655}" *)

module cordiccart2pol (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_idle,
        ap_ready,
        x_V,
        y_V,
        r_V,
        r_V_ap_vld,
        theta_V,
        theta_V_ap_vld
);

parameter    ap_const_logic_1 = 1'b1;
parameter    ap_const_logic_0 = 1'b0;
parameter    ap_ST_st1_fsm_0 = 3'b1;
parameter    ap_ST_pp0_stg0_fsm_1 = 3'b10;
parameter    ap_ST_st4_fsm_2 = 3'b100;
parameter    ap_const_lv32_0 = 32'b00000000000000000000000000000000;
parameter    ap_const_lv1_1 = 1'b1;
parameter    ap_const_lv32_1 = 32'b1;
parameter    ap_const_lv1_0 = 1'b0;
parameter    ap_const_lv4_0 = 4'b0000;
parameter    ap_const_lv32_2 = 32'b10;
parameter    ap_const_lv32_15 = 32'b10101;
parameter    ap_const_lv22_0 = 22'b0000000000000000000000;
parameter    ap_const_lv16_9B78 = 16'b1001101101111000;
parameter    ap_const_lv16_6487 = 16'b110010010000111;
parameter    ap_const_lv16_0 = 16'b0000000000000000;
parameter    ap_const_lv4_B = 4'b1011;
parameter    ap_const_lv4_1 = 4'b1;
parameter    ap_const_lv32_E = 32'b1110;
parameter    ap_const_lv32_23 = 32'b100011;
parameter    ap_const_lv36_26DD = 36'b10011011011101;
parameter    ap_true = 1'b1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
output   ap_idle;
output   ap_ready;
input  [21:0] x_V;
input  [21:0] y_V;
output  [21:0] r_V;
output   r_V_ap_vld;
output  [21:0] theta_V;
output   theta_V_ap_vld;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg r_V_ap_vld;
reg[21:0] theta_V;
reg theta_V_ap_vld;
(* fsm_encoding = "none" *) reg   [2:0] ap_CS_fsm = 3'b1;
reg    ap_sig_cseq_ST_st1_fsm_0;
reg    ap_sig_bdd_19;
wire   [3:0] angles_V_address0;
reg    angles_V_ce0;
wire   [13:0] angles_V_q0;
reg   [21:0] p_Val2_2_reg_106;
reg  signed [21:0] p_Val2_5_reg_116;
reg   [21:0] p_Val2_1_reg_126;
reg   [3:0] p_s_reg_135;
wire  signed [21:0] theta_V_loc_cast_fu_214_p1;
wire   [21:0] ssdm_int_V_write_assign_fu_219_p3;
wire   [21:0] ssdm_int_V_write_assign_1_fu_227_p3;
wire   [0:0] exitcond_fu_235_p2;
reg   [0:0] exitcond_reg_382;
reg    ap_sig_cseq_ST_pp0_stg0_fsm_1;
reg    ap_sig_bdd_63;
reg    ap_reg_ppiten_pp0_it0 = 1'b0;
reg    ap_reg_ppiten_pp0_it1 = 1'b0;
wire   [3:0] i_V_fu_241_p2;
wire   [0:0] tmp_11_fu_247_p3;
reg   [0:0] tmp_11_reg_391;
wire   [21:0] p_Val2_s_fu_288_p3;
reg   [21:0] p_Val2_s_reg_401;
wire   [21:0] p_Val2_4_fu_314_p3;
wire   [21:0] p_Val2_8_fu_338_p3;
reg   [21:0] p_Val2_5_phi_fu_119_p4;
wire   [63:0] tmp_1_fu_255_p1;
reg    ap_sig_cseq_ST_st4_fsm_2;
reg    ap_sig_bdd_107;
wire   [0:0] tmp_9_fu_154_p3;
wire   [21:0] p_Val2_1_i_fu_162_p2;
wire   [21:0] p_Val2_3_i_fu_176_p2;
wire   [21:0] ssdm_int_V_read_assign_1_fu_182_p3;
wire   [0:0] tmp_10_fu_190_p3;
wire   [0:0] tmp_fu_146_p3;
wire   [15:0] tmp_4_i_fu_198_p3;
wire   [15:0] theta_V_loc_fu_206_p3;
wire   [21:0] ssdm_int_V_read_assign_fu_168_p3;
wire   [21:0] sh_cast_fu_260_p1;
wire   [21:0] p_Val2_6_fu_264_p2;
wire   [21:0] p_Val2_7_fu_276_p2;
wire   [21:0] tmp_8_fu_270_p2;
wire   [21:0] tmp_6_fu_282_p2;
wire   [21:0] p_Val2_9_fu_296_p2;
wire   [21:0] tmp_3_fu_302_p2;
wire   [21:0] tmp_7_fu_308_p2;
wire   [21:0] p_Val2_10_cast_fu_322_p1;
wire   [21:0] tmp_2_fu_326_p2;
wire   [21:0] tmp_4_fu_332_p2;
wire  signed [35:0] p_Val2_s_5_fu_360_p2;
wire   [14:0] p_Val2_s_5_fu_360_p1;
reg   [2:0] ap_NS_fsm;


cordiccart2pol_angles_V #(
    .DataWidth( 14 ),
    .AddressRange( 16 ),
    .AddressWidth( 4 ))
angles_V_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( angles_V_address0 ),
    .ce0( angles_V_ce0 ),
    .q0( angles_V_q0 )
);

cordiccart2pol_mul_mul_22s_15ns_36_1 #(
    .ID( 1 ),
    .NUM_STAGE( 1 ),
    .din0_WIDTH( 22 ),
    .din1_WIDTH( 15 ),
    .dout_WIDTH( 36 ))
cordiccart2pol_mul_mul_22s_15ns_36_1_U0(
    .din0( p_Val2_5_reg_116 ),
    .din1( p_Val2_s_5_fu_360_p1 ),
    .dout( p_Val2_s_5_fu_360_p2 )
);



always @ (posedge ap_clk) begin : ap_ret_ap_CS_fsm
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_st1_fsm_0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_reg_ppiten_pp0_it0
    if (ap_rst == 1'b1) begin
        ap_reg_ppiten_pp0_it0 <= ap_const_logic_0;
    end else begin
        if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & ~(exitcond_fu_235_p2 == ap_const_lv1_0))) begin
            ap_reg_ppiten_pp0_it0 <= ap_const_logic_0;
        end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
            ap_reg_ppiten_pp0_it0 <= ap_const_logic_1;
        end
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_reg_ppiten_pp0_it1
    if (ap_rst == 1'b1) begin
        ap_reg_ppiten_pp0_it1 <= ap_const_logic_0;
    end else begin
        if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (exitcond_fu_235_p2 == ap_const_lv1_0))) begin
            ap_reg_ppiten_pp0_it1 <= ap_const_logic_1;
        end else if ((((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0)) | ((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & ~(exitcond_fu_235_p2 == ap_const_lv1_0)))) begin
            ap_reg_ppiten_pp0_it1 <= ap_const_logic_0;
        end
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & (exitcond_fu_235_p2 == ap_const_lv1_0))) begin
        p_Val2_1_reg_126 <= p_Val2_4_fu_314_p3;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        p_Val2_1_reg_126 <= ssdm_int_V_write_assign_fu_219_p3;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (exitcond_reg_382 == ap_const_lv1_0))) begin
        p_Val2_2_reg_106 <= p_Val2_8_fu_338_p3;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        p_Val2_2_reg_106 <= theta_V_loc_cast_fu_214_p1;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (exitcond_reg_382 == ap_const_lv1_0))) begin
        p_Val2_5_reg_116 <= p_Val2_s_reg_401;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        p_Val2_5_reg_116 <= ssdm_int_V_write_assign_1_fu_227_p3;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & (exitcond_fu_235_p2 == ap_const_lv1_0))) begin
        p_s_reg_135 <= i_V_fu_241_p2;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        p_s_reg_135 <= ap_const_lv4_0;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1)) begin
        exitcond_reg_382 <= exitcond_fu_235_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & (exitcond_fu_235_p2 == ap_const_lv1_0))) begin
        p_Val2_s_reg_401 <= p_Val2_s_fu_288_p3;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (exitcond_fu_235_p2 == ap_const_lv1_0))) begin
        tmp_11_reg_391 <= p_Val2_1_reg_126[ap_const_lv32_15];
    end
end

always @ (ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it0) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it0))) begin
        angles_V_ce0 = ap_const_logic_1;
    end else begin
        angles_V_ce0 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st4_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_2)) begin
        ap_done = ap_const_logic_1;
    end else begin
        ap_done = ap_const_logic_0;
    end
end

always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if ((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0))) begin
        ap_idle = ap_const_logic_1;
    end else begin
        ap_idle = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st4_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_2)) begin
        ap_ready = ap_const_logic_1;
    end else begin
        ap_ready = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_63) begin
    if (ap_sig_bdd_63) begin
        ap_sig_cseq_ST_pp0_stg0_fsm_1 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_pp0_stg0_fsm_1 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_19) begin
    if (ap_sig_bdd_19) begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_107) begin
    if (ap_sig_bdd_107) begin
        ap_sig_cseq_ST_st4_fsm_2 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st4_fsm_2 = ap_const_logic_0;
    end
end

always @ (p_Val2_5_reg_116 or exitcond_reg_382 or ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it1 or p_Val2_s_reg_401) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (exitcond_reg_382 == ap_const_lv1_0))) begin
        p_Val2_5_phi_fu_119_p4 = p_Val2_s_reg_401;
    end else begin
        p_Val2_5_phi_fu_119_p4 = p_Val2_5_reg_116;
    end
end

always @ (ap_sig_cseq_ST_st4_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st4_fsm_2)) begin
        r_V_ap_vld = ap_const_logic_1;
    end else begin
        r_V_ap_vld = ap_const_logic_0;
    end
end

always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0 or theta_V_loc_cast_fu_214_p1 or exitcond_reg_382 or ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it1 or p_Val2_8_fu_338_p3) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (exitcond_reg_382 == ap_const_lv1_0))) begin
        theta_V = p_Val2_8_fu_338_p3;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        theta_V = theta_V_loc_cast_fu_214_p1;
    end else begin
        theta_V = 'bx;
    end
end

always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0 or exitcond_reg_382 or ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it1) begin
    if ((((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0)) | ((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (exitcond_reg_382 == ap_const_lv1_0)))) begin
        theta_V_ap_vld = ap_const_logic_1;
    end else begin
        theta_V_ap_vld = ap_const_logic_0;
    end
end
always @ (ap_start or ap_CS_fsm or exitcond_fu_235_p2 or ap_reg_ppiten_pp0_it0) begin
    case (ap_CS_fsm)
        ap_ST_st1_fsm_0 : 
        begin
            if (~(ap_start == ap_const_logic_0)) begin
                ap_NS_fsm = ap_ST_pp0_stg0_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end
        end
        ap_ST_pp0_stg0_fsm_1 : 
        begin
            if (~((ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & ~(exitcond_fu_235_p2 == ap_const_lv1_0))) begin
                ap_NS_fsm = ap_ST_pp0_stg0_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st4_fsm_2;
            end
        end
        ap_ST_st4_fsm_2 : 
        begin
            ap_NS_fsm = ap_ST_st1_fsm_0;
        end
        default : 
        begin
            ap_NS_fsm = 'bx;
        end
    endcase
end


assign angles_V_address0 = tmp_1_fu_255_p1;


always @ (ap_CS_fsm) begin
    ap_sig_bdd_107 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_2]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_19 = (ap_CS_fsm[ap_const_lv32_0] == ap_const_lv1_1);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_63 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_1]);
end

assign exitcond_fu_235_p2 = (p_s_reg_135 == ap_const_lv4_B? 1'b1: 1'b0);

assign i_V_fu_241_p2 = (p_s_reg_135 + ap_const_lv4_1);

assign p_Val2_10_cast_fu_322_p1 = angles_V_q0;

assign p_Val2_1_i_fu_162_p2 = (ap_const_lv22_0 - y_V);

assign p_Val2_3_i_fu_176_p2 = (ap_const_lv22_0 - x_V);

assign p_Val2_4_fu_314_p3 = ((tmp_11_fu_247_p3[0:0] === 1'b1) ? tmp_3_fu_302_p2 : tmp_7_fu_308_p2);

assign p_Val2_6_fu_264_p2 = $signed(p_Val2_1_reg_126) >>> sh_cast_fu_260_p1;

assign p_Val2_7_fu_276_p2 = p_Val2_1_reg_126 >> sh_cast_fu_260_p1;

assign p_Val2_8_fu_338_p3 = ((tmp_11_reg_391[0:0] === 1'b1) ? tmp_2_fu_326_p2 : tmp_4_fu_332_p2);

assign p_Val2_9_fu_296_p2 = $signed(p_Val2_5_phi_fu_119_p4) >>> sh_cast_fu_260_p1;

assign p_Val2_s_5_fu_360_p1 = ap_const_lv36_26DD;

assign p_Val2_s_fu_288_p3 = ((tmp_11_fu_247_p3[0:0] === 1'b1) ? tmp_8_fu_270_p2 : tmp_6_fu_282_p2);

assign r_V = {{p_Val2_s_5_fu_360_p2[ap_const_lv32_23 : ap_const_lv32_E]}};

assign sh_cast_fu_260_p1 = p_s_reg_135;

assign ssdm_int_V_read_assign_1_fu_182_p3 = ((tmp_9_fu_154_p3[0:0] === 1'b1) ? x_V : p_Val2_3_i_fu_176_p2);

assign ssdm_int_V_read_assign_fu_168_p3 = ((tmp_9_fu_154_p3[0:0] === 1'b1) ? p_Val2_1_i_fu_162_p2 : y_V);

assign ssdm_int_V_write_assign_1_fu_227_p3 = ((tmp_fu_146_p3[0:0] === 1'b1) ? ssdm_int_V_read_assign_fu_168_p3 : x_V);

assign ssdm_int_V_write_assign_fu_219_p3 = ((tmp_fu_146_p3[0:0] === 1'b1) ? ssdm_int_V_read_assign_1_fu_182_p3 : y_V);

assign theta_V_loc_cast_fu_214_p1 = $signed(theta_V_loc_fu_206_p3);

assign theta_V_loc_fu_206_p3 = ((tmp_fu_146_p3[0:0] === 1'b1) ? tmp_4_i_fu_198_p3 : ap_const_lv16_0);

assign tmp_10_fu_190_p3 = ssdm_int_V_read_assign_1_fu_182_p3[ap_const_lv32_15];

assign tmp_11_fu_247_p3 = p_Val2_1_reg_126[ap_const_lv32_15];

assign tmp_1_fu_255_p1 = p_s_reg_135;

assign tmp_2_fu_326_p2 = (p_Val2_2_reg_106 - p_Val2_10_cast_fu_322_p1);

assign tmp_3_fu_302_p2 = (p_Val2_1_reg_126 + p_Val2_9_fu_296_p2);

assign tmp_4_fu_332_p2 = (p_Val2_10_cast_fu_322_p1 + p_Val2_2_reg_106);

assign tmp_4_i_fu_198_p3 = ((tmp_10_fu_190_p3[0:0] === 1'b1) ? ap_const_lv16_9B78 : ap_const_lv16_6487);

assign tmp_6_fu_282_p2 = (p_Val2_7_fu_276_p2 + p_Val2_5_phi_fu_119_p4);

assign tmp_7_fu_308_p2 = (p_Val2_1_reg_126 - p_Val2_9_fu_296_p2);

assign tmp_8_fu_270_p2 = (p_Val2_5_phi_fu_119_p4 - p_Val2_6_fu_264_p2);

assign tmp_9_fu_154_p3 = y_V[ap_const_lv32_15];

assign tmp_fu_146_p3 = x_V[ap_const_lv32_15];


endmodule //cordiccart2pol

