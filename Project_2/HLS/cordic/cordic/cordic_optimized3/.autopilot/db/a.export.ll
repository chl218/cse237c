; ModuleID = 'D:/Projects/vivado/Project_2/HLS/cordic/cordic/cordic_optimized3/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535]
@cordiccart2pol_str = internal unnamed_addr constant [15 x i8] c"cordiccart2pol\00"
@angles_V_old = constant [16 x i22] [i22 12867, i22 7596, i22 4013, i22 2037, i22 1022, i22 511, i22 255, i22 127, i22 63, i22 31, i22 15, i22 7, i22 3, i22 1, i22 0, i22 0]
@angles_V = constant [16 x i14] [i14 -3517, i14 7596, i14 4013, i14 2037, i14 1022, i14 511, i14 255, i14 127, i14 63, i14 31, i14 15, i14 7, i14 3, i14 1, i14 0, i14 0]
@p_str2 = private unnamed_addr constant [1 x i8] zeroinitializer, align 1
@p_str = private unnamed_addr constant [15 x i8] c"Shift_Add_Loop\00", align 1

declare i36 @llvm.part.select.i36(i36, i32, i32) nounwind readnone

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

define void @cordiccart2pol(i22 %x_V, i22 %y_V, i22* %r_V, i22* %theta_V) {
_ifconv1:
  call void (...)* @_ssdm_op_SpecBitsMap(i22 %x_V), !map !7
  call void (...)* @_ssdm_op_SpecBitsMap(i22 %y_V), !map !13
  call void (...)* @_ssdm_op_SpecBitsMap(i22* %r_V), !map !17
  call void (...)* @_ssdm_op_SpecBitsMap(i22* %theta_V), !map !21
  call void (...)* @_ssdm_op_SpecTopModule([15 x i8]* @cordiccart2pol_str) nounwind
  %y_V_read = call i22 @_ssdm_op_Read.ap_auto.i22(i22 %y_V)
  %x_V_read = call i22 @_ssdm_op_Read.ap_auto.i22(i22 %x_V)
  %tmp = call i1 @_ssdm_op_BitSelect.i1.i22.i32(i22 %x_V_read, i32 21)
  %tmp_9 = call i1 @_ssdm_op_BitSelect.i1.i22.i32(i22 %y_V_read, i32 21)
  %p_Val2_1_i = sub i22 0, %y_V_read
  %ssdm_int_V_read_assign = select i1 %tmp_9, i22 %p_Val2_1_i, i22 %y_V_read
  %p_Val2_3_i = sub i22 0, %x_V_read
  %ssdm_int_V_read_assign_1 = select i1 %tmp_9, i22 %x_V_read, i22 %p_Val2_3_i
  %tmp_10 = call i1 @_ssdm_op_BitSelect.i1.i22.i32(i22 %ssdm_int_V_read_assign_1, i32 21)
  %tmp_4_i = select i1 %tmp_10, i16 -25736, i16 25735
  %theta_V_loc = select i1 %tmp, i16 %tmp_4_i, i16 0
  %theta_V_loc_cast = sext i16 %theta_V_loc to i22
  %ssdm_int_V_write_assign = select i1 %tmp, i22 %ssdm_int_V_read_assign_1, i22 %y_V_read
  %ssdm_int_V_write_assign_1 = select i1 %tmp, i22 %ssdm_int_V_read_assign, i22 %x_V_read
  call void @_ssdm_op_Write.ap_auto.i22P(i22* %theta_V, i22 %theta_V_loc_cast)
  br label %0

; <label>:0                                       ; preds = %_ifconv, %_ifconv1
  %p_Val2_2 = phi i22 [ %theta_V_loc_cast, %_ifconv1 ], [ %p_Val2_8, %_ifconv ]
  %p_Val2_5 = phi i22 [ %ssdm_int_V_write_assign_1, %_ifconv1 ], [ %p_Val2_s, %_ifconv ]
  %p_Val2_1 = phi i22 [ %ssdm_int_V_write_assign, %_ifconv1 ], [ %p_Val2_4, %_ifconv ]
  %p_s = phi i4 [ 0, %_ifconv1 ], [ %i_V, %_ifconv ]
  %exitcond = icmp eq i4 %p_s, -5
  %i_V = add i4 %p_s, 1
  br i1 %exitcond, label %ap_fixed_base.exit, label %_ifconv

_ifconv:                                          ; preds = %0
  %empty = call i32 (...)* @_ssdm_op_SpecLoopTripCount(i64 11, i64 11, i64 11)
  call void (...)* @_ssdm_op_SpecLoopName([15 x i8]* @p_str) nounwind
  %tmp_5 = call i32 (...)* @_ssdm_op_SpecRegionBegin([15 x i8]* @p_str)
  call void (...)* @_ssdm_op_SpecPipeline(i32 1, i32 1, i32 1, i32 0, [1 x i8]* @p_str2) nounwind
  %tmp_11 = call i1 @_ssdm_op_BitSelect.i1.i22.i32(i22 %p_Val2_1, i32 21)
  %tmp_1 = zext i4 %p_s to i64
  %angles_V_addr = getelementptr [16 x i14]* @angles_V, i64 0, i64 %tmp_1
  %p_Val2_3 = load i14* %angles_V_addr, align 2
  %p_Val2_10_cast = zext i14 %p_Val2_3 to i22
  %tmp_2 = sub i22 %p_Val2_2, %p_Val2_10_cast
  %tmp_4 = add i22 %p_Val2_10_cast, %p_Val2_2
  %p_Val2_8 = select i1 %tmp_11, i22 %tmp_2, i22 %tmp_4
  call void @_ssdm_op_Write.ap_auto.i22P(i22* %theta_V, i22 %p_Val2_8)
  %sh_cast = zext i4 %p_s to i22
  %p_Val2_6 = ashr i22 %p_Val2_1, %sh_cast
  %tmp_8 = sub i22 %p_Val2_5, %p_Val2_6
  %p_Val2_7 = lshr i22 %p_Val2_1, %sh_cast
  %tmp_6 = add i22 %p_Val2_7, %p_Val2_5
  %p_Val2_s = select i1 %tmp_11, i22 %tmp_8, i22 %tmp_6
  %p_Val2_9 = ashr i22 %p_Val2_5, %sh_cast
  %tmp_3 = add i22 %p_Val2_1, %p_Val2_9
  %tmp_7 = sub i22 %p_Val2_1, %p_Val2_9
  %p_Val2_4 = select i1 %tmp_11, i22 %tmp_3, i22 %tmp_7
  %empty_4 = call i32 (...)* @_ssdm_op_SpecRegionEnd([15 x i8]* @p_str, i32 %tmp_5)
  br label %0

ap_fixed_base.exit:                               ; preds = %0
  %OP1_V_cast = sext i22 %p_Val2_5 to i36
  %p_Val2_s_5 = mul i36 %OP1_V_cast, 9949
  %tmp_s = call i22 @_ssdm_op_PartSelect.i22.i36.i32.i32(i36 %p_Val2_s_5, i32 14, i32 35)
  call void @_ssdm_op_Write.ap_auto.i22P(i22* %r_V, i22 %tmp_s)
  ret void
}

define weak void @_ssdm_op_Write.ap_auto.i22P(i22*, i22) {
entry:
  store i22 %1, i22* %0
  ret void
}

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecRegionEnd(...) {
entry:
  ret i32 0
}

define weak i32 @_ssdm_op_SpecRegionBegin(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecPipeline(...) nounwind {
entry:
  ret void
}

define weak i32 @_ssdm_op_SpecLoopTripCount(...) {
entry:
  ret i32 0
}

define weak void @_ssdm_op_SpecLoopName(...) nounwind {
entry:
  ret void
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak i22 @_ssdm_op_Read.ap_auto.i22(i22) {
entry:
  ret i22 %0
}

define weak i22 @_ssdm_op_PartSelect.i22.i36.i32.i32(i36, i32, i32) nounwind readnone {
entry:
  %empty = call i36 @llvm.part.select.i36(i36 %0, i32 %1, i32 %2)
  %empty_6 = trunc i36 %empty to i22
  ret i22 %empty_6
}

declare i16 @_ssdm_op_HSub(...)

declare i16 @_ssdm_op_HMul(...)

declare i16 @_ssdm_op_HDiv(...)

declare i16 @_ssdm_op_HAdd(...)

define weak i1 @_ssdm_op_BitSelect.i1.i22.i32(i22, i32) nounwind readnone {
entry:
  %empty = trunc i32 %1 to i22
  %empty_7 = shl i22 1, %empty
  %empty_8 = and i22 %0, %empty_7
  %empty_9 = icmp ne i22 %empty_8, 0
  ret i1 %empty_9
}

declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0}

!0 = metadata !{metadata !1, [1 x i32]* @llvm_global_ctors_0}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"llvm.global_ctors.0", metadata !5, metadata !"", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 0, i32 1}
!7 = metadata !{metadata !8}
!8 = metadata !{i32 0, i32 21, metadata !9}
!9 = metadata !{metadata !10}
!10 = metadata !{metadata !"x.V", metadata !11, metadata !"int22", i32 0, i32 21}
!11 = metadata !{metadata !12}
!12 = metadata !{i32 0, i32 0, i32 0}
!13 = metadata !{metadata !14}
!14 = metadata !{i32 0, i32 21, metadata !15}
!15 = metadata !{metadata !16}
!16 = metadata !{metadata !"y.V", metadata !11, metadata !"int22", i32 0, i32 21}
!17 = metadata !{metadata !18}
!18 = metadata !{i32 0, i32 21, metadata !19}
!19 = metadata !{metadata !20}
!20 = metadata !{metadata !"r.V", metadata !5, metadata !"int22", i32 0, i32 21}
!21 = metadata !{metadata !22}
!22 = metadata !{i32 0, i32 21, metadata !23}
!23 = metadata !{metadata !24}
!24 = metadata !{metadata !"theta.V", metadata !5, metadata !"int22", i32 0, i32 21}
