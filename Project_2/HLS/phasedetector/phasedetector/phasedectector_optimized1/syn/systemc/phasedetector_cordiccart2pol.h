// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

#ifndef _phasedetector_cordiccart2pol_HH_
#define _phasedetector_cordiccart2pol_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct phasedetector_cordiccart2pol : public sc_module {
    // Port declarations 6
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_lv<24> > x_V;
    sc_in< sc_lv<24> > y_V;
    sc_out< sc_lv<25> > ap_return_0;
    sc_out< sc_lv<25> > ap_return_1;


    // Module declarations
    phasedetector_cordiccart2pol(sc_module_name name);
    SC_HAS_PROCESS(phasedetector_cordiccart2pol);

    ~phasedetector_cordiccart2pol();

    sc_trace_file* mVcdFile;

    sc_signal< sc_lv<1> > tmp_39_fu_146_p3;
    sc_signal< sc_lv<1> > tmp_39_reg_1390;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_39_reg_1390_pp0_it1;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_39_reg_1390_pp0_it2;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_39_reg_1390_pp0_it3;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_39_reg_1390_pp0_it4;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_39_reg_1390_pp0_it5;
    sc_signal< sc_lv<1> > tmp_43_reg_1395;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_43_reg_1395_pp0_it1;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_43_reg_1395_pp0_it2;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_43_reg_1395_pp0_it3;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_43_reg_1395_pp0_it4;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_43_reg_1395_pp0_it5;
    sc_signal< sc_lv<1> > tmp_45_reg_1400;
    sc_signal< sc_lv<1> > tmp_47_reg_1406;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_47_reg_1406_pp0_it1;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_47_reg_1406_pp0_it2;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_47_reg_1406_pp0_it3;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_47_reg_1406_pp0_it4;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_47_reg_1406_pp0_it5;
    sc_signal< sc_lv<25> > tmp_s_fu_234_p2;
    sc_signal< sc_lv<25> > tmp_s_reg_1411;
    sc_signal< sc_lv<25> > tmp_33_fu_240_p2;
    sc_signal< sc_lv<25> > tmp_33_reg_1417;
    sc_signal< sc_lv<25> > tmp_34_fu_246_p2;
    sc_signal< sc_lv<25> > tmp_34_reg_1422;
    sc_signal< sc_lv<1> > tmp_51_reg_1427;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_51_reg_1427_pp0_it2;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_51_reg_1427_pp0_it3;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_51_reg_1427_pp0_it4;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_51_reg_1427_pp0_it5;
    sc_signal< sc_lv<1> > tmp_55_reg_1432;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_55_reg_1432_pp0_it2;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_55_reg_1432_pp0_it3;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_55_reg_1432_pp0_it4;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_55_reg_1432_pp0_it5;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_55_reg_1432_pp0_it6;
    sc_signal< sc_lv<25> > p_Val2_45_2_fu_396_p3;
    sc_signal< sc_lv<25> > p_Val2_45_2_reg_1437;
    sc_signal< sc_lv<25> > p_Val2_49_2_fu_430_p3;
    sc_signal< sc_lv<25> > p_Val2_49_2_reg_1443;
    sc_signal< sc_lv<1> > tmp_57_reg_1449;
    sc_signal< sc_lv<1> > tmp_59_reg_1455;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_59_reg_1455_pp0_it2;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_59_reg_1455_pp0_it3;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_59_reg_1455_pp0_it4;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_59_reg_1455_pp0_it5;
    sc_signal< sc_lv<22> > tmp_40_reg_1460;
    sc_signal< sc_lv<22> > tmp_42_reg_1466;
    sc_signal< sc_lv<1> > tmp_63_reg_1471;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_63_reg_1471_pp0_it3;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_63_reg_1471_pp0_it4;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_63_reg_1471_pp0_it5;
    sc_signal< sc_lv<25> > p_Val2_45_4_fu_563_p3;
    sc_signal< sc_lv<25> > p_Val2_45_4_reg_1476;
    sc_signal< sc_lv<25> > p_Val2_49_4_fu_597_p3;
    sc_signal< sc_lv<25> > p_Val2_49_4_reg_1482;
    sc_signal< sc_lv<1> > tmp_67_reg_1488;
    sc_signal< sc_lv<1> > tmp_70_reg_1494;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_70_reg_1494_pp0_it3;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_70_reg_1494_pp0_it4;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_70_reg_1494_pp0_it5;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_70_reg_1494_pp0_it6;
    sc_signal< sc_lv<20> > tmp_48_reg_1499;
    sc_signal< sc_lv<20> > tmp_50_reg_1505;
    sc_signal< sc_lv<1> > tmp_75_reg_1510;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_75_reg_1510_pp0_it4;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_75_reg_1510_pp0_it5;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_75_reg_1510_pp0_it6;
    sc_signal< sc_lv<25> > p_Val2_45_6_fu_730_p3;
    sc_signal< sc_lv<25> > p_Val2_45_6_reg_1515;
    sc_signal< sc_lv<25> > p_Val2_49_6_fu_764_p3;
    sc_signal< sc_lv<25> > p_Val2_49_6_reg_1521;
    sc_signal< sc_lv<1> > tmp_77_reg_1527;
    sc_signal< sc_lv<1> > tmp_80_reg_1533;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_80_reg_1533_pp0_it4;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_80_reg_1533_pp0_it5;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_80_reg_1533_pp0_it6;
    sc_signal< sc_lv<18> > tmp_56_reg_1538;
    sc_signal< sc_lv<18> > tmp_60_reg_1544;
    sc_signal< sc_lv<1> > tmp_85_reg_1549;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_85_reg_1549_pp0_it5;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_85_reg_1549_pp0_it6;
    sc_signal< sc_lv<25> > p_Val2_45_8_fu_897_p3;
    sc_signal< sc_lv<25> > p_Val2_45_8_reg_1554;
    sc_signal< sc_lv<25> > p_Val2_49_8_fu_931_p3;
    sc_signal< sc_lv<25> > p_Val2_49_8_reg_1560;
    sc_signal< sc_lv<1> > tmp_86_reg_1566;
    sc_signal< sc_lv<1> > tmp_87_reg_1572;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_87_reg_1572_pp0_it5;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_87_reg_1572_pp0_it6;
    sc_signal< sc_lv<16> > tmp_68_reg_1577;
    sc_signal< sc_lv<16> > tmp_71_reg_1583;
    sc_signal< sc_lv<1> > tmp_89_reg_1588;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_89_reg_1588_pp0_it6;
    sc_signal< sc_lv<25> > p_Val2_45_s_fu_1064_p3;
    sc_signal< sc_lv<25> > p_Val2_45_s_reg_1593;
    sc_signal< sc_lv<1> > tmp_90_reg_1599;
    sc_signal< sc_lv<1> > tmp_91_reg_1604;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_91_reg_1604_pp0_it6;
    sc_signal< sc_lv<14> > tmp_78_reg_1609;
    sc_signal< sc_lv<20> > tmp2_fu_1188_p2;
    sc_signal< sc_lv<20> > tmp2_reg_1615;
    sc_signal< sc_lv<16> > tmp5_fu_1194_p2;
    sc_signal< sc_lv<16> > tmp5_reg_1620;
    sc_signal< sc_lv<25> > p_Val2_45_10_fu_1216_p3;
    sc_signal< sc_lv<25> > p_Val2_45_10_reg_1625;
    sc_signal< sc_lv<1> > tmp_41_fu_154_p3;
    sc_signal< sc_lv<24> > p_Val2_i_fu_162_p2;
    sc_signal< sc_lv<25> > x_V_cast1_fu_142_p1;
    sc_signal< sc_lv<25> > p_Val2_2_i_fu_176_p2;
    sc_signal< sc_lv<25> > ssdm_int_V_read_assign_1_fu_182_p3;
    sc_signal< sc_lv<24> > ssdm_int_V_read_assign_fu_168_p3;
    sc_signal< sc_lv<24> > ssdm_int_V_write_assign_fu_198_p3;
    sc_signal< sc_lv<25> > y_V_cast2_fu_138_p1;
    sc_signal< sc_lv<25> > ssdm_int_V_write_assign_2_fu_210_p3;
    sc_signal< sc_lv<25> > ssdm_int_V_write_assign_2_cast_fu_206_p1;
    sc_signal< sc_lv<25> > p_Val2_2_fu_257_p3;
    sc_signal< sc_lv<24> > tmp_35_fu_278_p4;
    sc_signal< sc_lv<25> > p_Val2_1_fu_252_p3;
    sc_signal< sc_lv<25> > p_Val2_42_1_fu_292_p1;
    sc_signal< sc_lv<25> > tmp_fu_288_p1;
    sc_signal< sc_lv<1> > tmp_49_fu_262_p3;
    sc_signal< sc_lv<25> > tmp_99_1_fu_296_p2;
    sc_signal< sc_lv<25> > tmp_104_1_fu_302_p2;
    sc_signal< sc_lv<24> > tmp_36_fu_316_p4;
    sc_signal< sc_lv<25> > p_Val2_47_1_fu_326_p1;
    sc_signal< sc_lv<25> > tmp_110_1_fu_330_p2;
    sc_signal< sc_lv<25> > tmp_115_1_fu_336_p2;
    sc_signal< sc_lv<25> > p_Val2_49_1_fu_342_p3;
    sc_signal< sc_lv<23> > tmp_37_fu_366_p4;
    sc_signal< sc_lv<25> > p_Val2_45_1_fu_308_p3;
    sc_signal< sc_lv<25> > p_Val2_42_2_fu_380_p1;
    sc_signal< sc_lv<25> > tmp_27_fu_376_p1;
    sc_signal< sc_lv<1> > tmp_53_fu_350_p3;
    sc_signal< sc_lv<25> > tmp_99_2_fu_384_p2;
    sc_signal< sc_lv<25> > tmp_104_2_fu_390_p2;
    sc_signal< sc_lv<23> > tmp_38_fu_404_p4;
    sc_signal< sc_lv<25> > p_Val2_47_2_fu_414_p1;
    sc_signal< sc_lv<25> > tmp_110_2_fu_418_p2;
    sc_signal< sc_lv<25> > tmp_115_2_fu_424_p2;
    sc_signal< sc_lv<25> > p_Val2_42_3_fu_477_p1;
    sc_signal< sc_lv<25> > tmp_28_fu_474_p1;
    sc_signal< sc_lv<25> > tmp_99_3_fu_480_p2;
    sc_signal< sc_lv<25> > tmp_104_3_fu_485_p2;
    sc_signal< sc_lv<25> > p_Val2_47_3_fu_497_p1;
    sc_signal< sc_lv<25> > tmp_110_3_fu_500_p2;
    sc_signal< sc_lv<25> > tmp_115_3_fu_505_p2;
    sc_signal< sc_lv<25> > p_Val2_49_3_fu_510_p3;
    sc_signal< sc_lv<21> > tmp_44_fu_533_p4;
    sc_signal< sc_lv<25> > p_Val2_45_3_fu_490_p3;
    sc_signal< sc_lv<25> > p_Val2_42_4_fu_547_p1;
    sc_signal< sc_lv<25> > tmp_29_fu_543_p1;
    sc_signal< sc_lv<1> > tmp_61_fu_517_p3;
    sc_signal< sc_lv<25> > tmp_99_4_fu_551_p2;
    sc_signal< sc_lv<25> > tmp_104_4_fu_557_p2;
    sc_signal< sc_lv<21> > tmp_46_fu_571_p4;
    sc_signal< sc_lv<25> > p_Val2_47_4_fu_581_p1;
    sc_signal< sc_lv<25> > tmp_110_4_fu_585_p2;
    sc_signal< sc_lv<25> > tmp_115_4_fu_591_p2;
    sc_signal< sc_lv<25> > p_Val2_42_5_fu_644_p1;
    sc_signal< sc_lv<25> > tmp_30_fu_641_p1;
    sc_signal< sc_lv<25> > tmp_99_5_fu_647_p2;
    sc_signal< sc_lv<25> > tmp_104_5_fu_652_p2;
    sc_signal< sc_lv<25> > p_Val2_47_5_fu_664_p1;
    sc_signal< sc_lv<25> > tmp_110_5_fu_667_p2;
    sc_signal< sc_lv<25> > tmp_115_5_fu_672_p2;
    sc_signal< sc_lv<25> > p_Val2_49_5_fu_677_p3;
    sc_signal< sc_lv<19> > tmp_52_fu_700_p4;
    sc_signal< sc_lv<25> > p_Val2_45_5_fu_657_p3;
    sc_signal< sc_lv<25> > p_Val2_42_6_fu_714_p1;
    sc_signal< sc_lv<25> > tmp_31_fu_710_p1;
    sc_signal< sc_lv<1> > tmp_72_fu_684_p3;
    sc_signal< sc_lv<25> > tmp_99_6_fu_718_p2;
    sc_signal< sc_lv<25> > tmp_104_6_fu_724_p2;
    sc_signal< sc_lv<19> > tmp_54_fu_738_p4;
    sc_signal< sc_lv<25> > p_Val2_47_6_fu_748_p1;
    sc_signal< sc_lv<25> > tmp_110_6_fu_752_p2;
    sc_signal< sc_lv<25> > tmp_115_6_fu_758_p2;
    sc_signal< sc_lv<25> > tmp_58_fu_811_p1;
    sc_signal< sc_lv<25> > tmp_32_fu_808_p1;
    sc_signal< sc_lv<25> > tmp_99_7_fu_814_p2;
    sc_signal< sc_lv<25> > tmp_104_7_fu_819_p2;
    sc_signal< sc_lv<25> > tmp_62_fu_831_p1;
    sc_signal< sc_lv<25> > tmp_110_7_fu_834_p2;
    sc_signal< sc_lv<25> > tmp_115_7_fu_839_p2;
    sc_signal< sc_lv<25> > p_Val2_49_7_fu_844_p3;
    sc_signal< sc_lv<17> > tmp_64_fu_867_p4;
    sc_signal< sc_lv<25> > p_Val2_45_7_fu_824_p3;
    sc_signal< sc_lv<25> > p_Val2_42_8_fu_881_p1;
    sc_signal< sc_lv<25> > tmp_65_fu_877_p1;
    sc_signal< sc_lv<1> > tmp_82_fu_851_p3;
    sc_signal< sc_lv<25> > tmp_99_8_fu_885_p2;
    sc_signal< sc_lv<25> > tmp_104_8_fu_891_p2;
    sc_signal< sc_lv<17> > tmp_66_fu_905_p4;
    sc_signal< sc_lv<25> > p_Val2_47_8_fu_915_p1;
    sc_signal< sc_lv<25> > tmp_110_8_fu_919_p2;
    sc_signal< sc_lv<25> > tmp_115_8_fu_925_p2;
    sc_signal< sc_lv<25> > p_Val2_42_9_fu_978_p1;
    sc_signal< sc_lv<25> > tmp_69_fu_975_p1;
    sc_signal< sc_lv<25> > tmp_99_9_fu_981_p2;
    sc_signal< sc_lv<25> > tmp_104_9_fu_986_p2;
    sc_signal< sc_lv<25> > p_Val2_47_9_fu_998_p1;
    sc_signal< sc_lv<25> > tmp_110_9_fu_1001_p2;
    sc_signal< sc_lv<25> > tmp_115_9_fu_1006_p2;
    sc_signal< sc_lv<25> > p_Val2_49_9_fu_1011_p3;
    sc_signal< sc_lv<15> > tmp_73_fu_1034_p4;
    sc_signal< sc_lv<25> > p_Val2_45_9_fu_991_p3;
    sc_signal< sc_lv<25> > p_Val2_42_s_fu_1048_p1;
    sc_signal< sc_lv<25> > tmp_74_fu_1044_p1;
    sc_signal< sc_lv<1> > tmp_88_fu_1018_p3;
    sc_signal< sc_lv<25> > tmp_99_s_fu_1052_p2;
    sc_signal< sc_lv<25> > tmp_104_s_fu_1058_p2;
    sc_signal< sc_lv<15> > tmp_76_fu_1072_p4;
    sc_signal< sc_lv<25> > p_Val2_47_s_fu_1082_p1;
    sc_signal< sc_lv<25> > tmp_110_s_fu_1086_p2;
    sc_signal< sc_lv<25> > tmp_115_s_fu_1092_p2;
    sc_signal< sc_lv<25> > p_Val2_49_s_fu_1098_p3;
    sc_signal< sc_lv<19> > theta_V_i_fu_1132_p3;
    sc_signal< sc_lv<19> > ssdm_int_V_write_assign_3_fu_1139_p3;
    sc_signal< sc_lv<19> > tmp_49_cast_cast_fu_1157_p3;
    sc_signal< sc_lv<19> > tmp_43_cast_cast_fu_1150_p3;
    sc_signal< sc_lv<19> > tmp3_fu_1178_p2;
    sc_signal< sc_lv<20> > ssdm_int_V_write_assign_1_cast_fu_1146_p1;
    sc_signal< sc_lv<20> > tmp3_cast_fu_1184_p1;
    sc_signal< sc_lv<16> > tmp_76_cast_cast_fu_1171_p3;
    sc_signal< sc_lv<16> > tmp_67_cast_cast_fu_1164_p3;
    sc_signal< sc_lv<25> > p_Val2_42_7_fu_1203_p1;
    sc_signal< sc_lv<25> > tmp_79_fu_1200_p1;
    sc_signal< sc_lv<25> > tmp_99_10_fu_1206_p2;
    sc_signal< sc_lv<25> > tmp_104_10_fu_1211_p2;
    sc_signal< sc_lv<17> > tmp_58_cast_cast_fu_1223_p3;
    sc_signal< sc_lv<17> > tmp5_cast_fu_1279_p1;
    sc_signal< sc_lv<17> > tmp4_fu_1282_p2;
    sc_signal< sc_lv<20> > tmp4_cast_fu_1288_p1;
    sc_signal< sc_lv<13> > tmp_103_cast_cast_fu_1244_p3;
    sc_signal< sc_lv<13> > tmp_94_cast_cast_fu_1237_p3;
    sc_signal< sc_lv<13> > tmp8_fu_1297_p2;
    sc_signal< sc_lv<14> > tmp_85_cast_cast_fu_1230_p3;
    sc_signal< sc_lv<14> > tmp8_cast_fu_1303_p1;
    sc_signal< sc_lv<11> > tmp_124_cast_cast_fu_1258_p3;
    sc_signal< sc_lv<11> > tmp_115_cast_cast_fu_1251_p3;
    sc_signal< sc_lv<9> > tmp_142_cast_cast_fu_1272_p3;
    sc_signal< sc_lv<9> > tmp_133_cast_cast_fu_1265_p3;
    sc_signal< sc_lv<9> > tmp11_fu_1319_p2;
    sc_signal< sc_lv<11> > tmp10_fu_1313_p2;
    sc_signal< sc_lv<11> > tmp11_cast_fu_1325_p1;
    sc_signal< sc_lv<11> > tmp9_fu_1329_p2;
    sc_signal< sc_lv<14> > tmp7_fu_1307_p2;
    sc_signal< sc_lv<14> > tmp9_cast_fu_1335_p1;
    sc_signal< sc_lv<14> > tmp6_fu_1339_p2;
    sc_signal< sc_lv<20> > tmp1_fu_1292_p2;
    sc_signal< sc_lv<20> > tmp6_cast_fu_1345_p1;
    sc_signal< sc_lv<20> > p_Val2_40_s_fu_1349_p2;
    sc_signal< sc_lv<25> > p_Val2_s_fu_1362_p0;
    sc_signal< sc_lv<42> > p_Val2_s_fu_1362_p2;
    sc_signal< sc_lv<25> > ssdm_int_V_write_assign_4_fu_1368_p4;
    sc_signal< sc_lv<25> > p_Val2_40_10_cast_fu_1355_p1;
    static const bool ap_true;
    static const sc_lv<32> ap_const_lv32_17;
    static const sc_lv<24> ap_const_lv24_0;
    static const sc_lv<25> ap_const_lv25_0;
    static const sc_lv<32> ap_const_lv32_18;
    static const sc_lv<32> ap_const_lv32_1;
    static const sc_lv<32> ap_const_lv32_2;
    static const sc_lv<32> ap_const_lv32_3;
    static const sc_lv<32> ap_const_lv32_4;
    static const sc_lv<32> ap_const_lv32_5;
    static const sc_lv<32> ap_const_lv32_6;
    static const sc_lv<32> ap_const_lv32_7;
    static const sc_lv<32> ap_const_lv32_8;
    static const sc_lv<32> ap_const_lv32_9;
    static const sc_lv<32> ap_const_lv32_A;
    static const sc_lv<32> ap_const_lv32_B;
    static const sc_lv<19> ap_const_lv19_4DBC0;
    static const sc_lv<19> ap_const_lv19_3243F;
    static const sc_lv<19> ap_const_lv19_0;
    static const sc_lv<19> ap_const_lv19_66DE1;
    static const sc_lv<19> ap_const_lv19_1921F;
    static const sc_lv<19> ap_const_lv19_7129D;
    static const sc_lv<19> ap_const_lv19_ED63;
    static const sc_lv<16> ap_const_lv16_C055;
    static const sc_lv<16> ap_const_lv16_3FAB;
    static const sc_lv<16> ap_const_lv16_E00B;
    static const sc_lv<16> ap_const_lv16_1FF5;
    static const sc_lv<17> ap_const_lv17_18293;
    static const sc_lv<17> ap_const_lv17_7D6D;
    static const sc_lv<14> ap_const_lv14_3002;
    static const sc_lv<14> ap_const_lv14_FFE;
    static const sc_lv<13> ap_const_lv13_1801;
    static const sc_lv<13> ap_const_lv13_7FF;
    static const sc_lv<13> ap_const_lv13_1C01;
    static const sc_lv<13> ap_const_lv13_3FF;
    static const sc_lv<11> ap_const_lv11_601;
    static const sc_lv<11> ap_const_lv11_1FF;
    static const sc_lv<11> ap_const_lv11_701;
    static const sc_lv<11> ap_const_lv11_FF;
    static const sc_lv<9> ap_const_lv9_181;
    static const sc_lv<9> ap_const_lv9_7F;
    static const sc_lv<9> ap_const_lv9_1C1;
    static const sc_lv<9> ap_const_lv9_3F;
    static const sc_lv<42> ap_const_lv42_136ED;
    static const sc_lv<32> ap_const_lv32_11;
    static const sc_lv<32> ap_const_lv32_29;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_ap_return_0();
    void thread_ap_return_1();
    void thread_p_Val2_1_fu_252_p3();
    void thread_p_Val2_2_fu_257_p3();
    void thread_p_Val2_2_i_fu_176_p2();
    void thread_p_Val2_40_10_cast_fu_1355_p1();
    void thread_p_Val2_40_s_fu_1349_p2();
    void thread_p_Val2_42_1_fu_292_p1();
    void thread_p_Val2_42_2_fu_380_p1();
    void thread_p_Val2_42_3_fu_477_p1();
    void thread_p_Val2_42_4_fu_547_p1();
    void thread_p_Val2_42_5_fu_644_p1();
    void thread_p_Val2_42_6_fu_714_p1();
    void thread_p_Val2_42_7_fu_1203_p1();
    void thread_p_Val2_42_8_fu_881_p1();
    void thread_p_Val2_42_9_fu_978_p1();
    void thread_p_Val2_42_s_fu_1048_p1();
    void thread_p_Val2_45_10_fu_1216_p3();
    void thread_p_Val2_45_1_fu_308_p3();
    void thread_p_Val2_45_2_fu_396_p3();
    void thread_p_Val2_45_3_fu_490_p3();
    void thread_p_Val2_45_4_fu_563_p3();
    void thread_p_Val2_45_5_fu_657_p3();
    void thread_p_Val2_45_6_fu_730_p3();
    void thread_p_Val2_45_7_fu_824_p3();
    void thread_p_Val2_45_8_fu_897_p3();
    void thread_p_Val2_45_9_fu_991_p3();
    void thread_p_Val2_45_s_fu_1064_p3();
    void thread_p_Val2_47_1_fu_326_p1();
    void thread_p_Val2_47_2_fu_414_p1();
    void thread_p_Val2_47_3_fu_497_p1();
    void thread_p_Val2_47_4_fu_581_p1();
    void thread_p_Val2_47_5_fu_664_p1();
    void thread_p_Val2_47_6_fu_748_p1();
    void thread_p_Val2_47_8_fu_915_p1();
    void thread_p_Val2_47_9_fu_998_p1();
    void thread_p_Val2_47_s_fu_1082_p1();
    void thread_p_Val2_49_1_fu_342_p3();
    void thread_p_Val2_49_2_fu_430_p3();
    void thread_p_Val2_49_3_fu_510_p3();
    void thread_p_Val2_49_4_fu_597_p3();
    void thread_p_Val2_49_5_fu_677_p3();
    void thread_p_Val2_49_6_fu_764_p3();
    void thread_p_Val2_49_7_fu_844_p3();
    void thread_p_Val2_49_8_fu_931_p3();
    void thread_p_Val2_49_9_fu_1011_p3();
    void thread_p_Val2_49_s_fu_1098_p3();
    void thread_p_Val2_i_fu_162_p2();
    void thread_p_Val2_s_fu_1362_p0();
    void thread_p_Val2_s_fu_1362_p2();
    void thread_ssdm_int_V_read_assign_1_fu_182_p3();
    void thread_ssdm_int_V_read_assign_fu_168_p3();
    void thread_ssdm_int_V_write_assign_1_cast_fu_1146_p1();
    void thread_ssdm_int_V_write_assign_2_cast_fu_206_p1();
    void thread_ssdm_int_V_write_assign_2_fu_210_p3();
    void thread_ssdm_int_V_write_assign_3_fu_1139_p3();
    void thread_ssdm_int_V_write_assign_4_fu_1368_p4();
    void thread_ssdm_int_V_write_assign_fu_198_p3();
    void thread_theta_V_i_fu_1132_p3();
    void thread_tmp10_fu_1313_p2();
    void thread_tmp11_cast_fu_1325_p1();
    void thread_tmp11_fu_1319_p2();
    void thread_tmp1_fu_1292_p2();
    void thread_tmp2_fu_1188_p2();
    void thread_tmp3_cast_fu_1184_p1();
    void thread_tmp3_fu_1178_p2();
    void thread_tmp4_cast_fu_1288_p1();
    void thread_tmp4_fu_1282_p2();
    void thread_tmp5_cast_fu_1279_p1();
    void thread_tmp5_fu_1194_p2();
    void thread_tmp6_cast_fu_1345_p1();
    void thread_tmp6_fu_1339_p2();
    void thread_tmp7_fu_1307_p2();
    void thread_tmp8_cast_fu_1303_p1();
    void thread_tmp8_fu_1297_p2();
    void thread_tmp9_cast_fu_1335_p1();
    void thread_tmp9_fu_1329_p2();
    void thread_tmp_103_cast_cast_fu_1244_p3();
    void thread_tmp_104_10_fu_1211_p2();
    void thread_tmp_104_1_fu_302_p2();
    void thread_tmp_104_2_fu_390_p2();
    void thread_tmp_104_3_fu_485_p2();
    void thread_tmp_104_4_fu_557_p2();
    void thread_tmp_104_5_fu_652_p2();
    void thread_tmp_104_6_fu_724_p2();
    void thread_tmp_104_7_fu_819_p2();
    void thread_tmp_104_8_fu_891_p2();
    void thread_tmp_104_9_fu_986_p2();
    void thread_tmp_104_s_fu_1058_p2();
    void thread_tmp_110_1_fu_330_p2();
    void thread_tmp_110_2_fu_418_p2();
    void thread_tmp_110_3_fu_500_p2();
    void thread_tmp_110_4_fu_585_p2();
    void thread_tmp_110_5_fu_667_p2();
    void thread_tmp_110_6_fu_752_p2();
    void thread_tmp_110_7_fu_834_p2();
    void thread_tmp_110_8_fu_919_p2();
    void thread_tmp_110_9_fu_1001_p2();
    void thread_tmp_110_s_fu_1086_p2();
    void thread_tmp_115_1_fu_336_p2();
    void thread_tmp_115_2_fu_424_p2();
    void thread_tmp_115_3_fu_505_p2();
    void thread_tmp_115_4_fu_591_p2();
    void thread_tmp_115_5_fu_672_p2();
    void thread_tmp_115_6_fu_758_p2();
    void thread_tmp_115_7_fu_839_p2();
    void thread_tmp_115_8_fu_925_p2();
    void thread_tmp_115_9_fu_1006_p2();
    void thread_tmp_115_cast_cast_fu_1251_p3();
    void thread_tmp_115_s_fu_1092_p2();
    void thread_tmp_124_cast_cast_fu_1258_p3();
    void thread_tmp_133_cast_cast_fu_1265_p3();
    void thread_tmp_142_cast_cast_fu_1272_p3();
    void thread_tmp_27_fu_376_p1();
    void thread_tmp_28_fu_474_p1();
    void thread_tmp_29_fu_543_p1();
    void thread_tmp_30_fu_641_p1();
    void thread_tmp_31_fu_710_p1();
    void thread_tmp_32_fu_808_p1();
    void thread_tmp_33_fu_240_p2();
    void thread_tmp_34_fu_246_p2();
    void thread_tmp_35_fu_278_p4();
    void thread_tmp_36_fu_316_p4();
    void thread_tmp_37_fu_366_p4();
    void thread_tmp_38_fu_404_p4();
    void thread_tmp_39_fu_146_p3();
    void thread_tmp_41_fu_154_p3();
    void thread_tmp_43_cast_cast_fu_1150_p3();
    void thread_tmp_44_fu_533_p4();
    void thread_tmp_46_fu_571_p4();
    void thread_tmp_49_cast_cast_fu_1157_p3();
    void thread_tmp_49_fu_262_p3();
    void thread_tmp_52_fu_700_p4();
    void thread_tmp_53_fu_350_p3();
    void thread_tmp_54_fu_738_p4();
    void thread_tmp_58_cast_cast_fu_1223_p3();
    void thread_tmp_58_fu_811_p1();
    void thread_tmp_61_fu_517_p3();
    void thread_tmp_62_fu_831_p1();
    void thread_tmp_64_fu_867_p4();
    void thread_tmp_65_fu_877_p1();
    void thread_tmp_66_fu_905_p4();
    void thread_tmp_67_cast_cast_fu_1164_p3();
    void thread_tmp_69_fu_975_p1();
    void thread_tmp_72_fu_684_p3();
    void thread_tmp_73_fu_1034_p4();
    void thread_tmp_74_fu_1044_p1();
    void thread_tmp_76_cast_cast_fu_1171_p3();
    void thread_tmp_76_fu_1072_p4();
    void thread_tmp_79_fu_1200_p1();
    void thread_tmp_82_fu_851_p3();
    void thread_tmp_85_cast_cast_fu_1230_p3();
    void thread_tmp_88_fu_1018_p3();
    void thread_tmp_94_cast_cast_fu_1237_p3();
    void thread_tmp_99_10_fu_1206_p2();
    void thread_tmp_99_1_fu_296_p2();
    void thread_tmp_99_2_fu_384_p2();
    void thread_tmp_99_3_fu_480_p2();
    void thread_tmp_99_4_fu_551_p2();
    void thread_tmp_99_5_fu_647_p2();
    void thread_tmp_99_6_fu_718_p2();
    void thread_tmp_99_7_fu_814_p2();
    void thread_tmp_99_8_fu_885_p2();
    void thread_tmp_99_9_fu_981_p2();
    void thread_tmp_99_s_fu_1052_p2();
    void thread_tmp_fu_288_p1();
    void thread_tmp_s_fu_234_p2();
    void thread_x_V_cast1_fu_142_p1();
    void thread_y_V_cast2_fu_138_p1();
};

}

using namespace ap_rtl;

#endif
