-- ==============================================================
-- RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
-- Version: 2015.4
-- Copyright (C) 2015 Xilinx Inc. All rights reserved.
-- 
-- ===========================================================

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity phasedetector_correct_quad is
port (
    x_V_read : IN STD_LOGIC_VECTOR (20 downto 0);
    y_V_read : IN STD_LOGIC_VECTOR (20 downto 0);
    ap_return_0 : OUT STD_LOGIC_VECTOR (21 downto 0);
    ap_return_1 : OUT STD_LOGIC_VECTOR (21 downto 0);
    ap_return_2 : OUT STD_LOGIC_VECTOR (21 downto 0) );
end;


architecture behav of phasedetector_correct_quad is 
    constant ap_true : BOOLEAN := true;
    constant ap_const_lv32_14 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000010100";
    constant ap_const_lv21_0 : STD_LOGIC_VECTOR (20 downto 0) := "000000000000000000000";
    constant ap_const_lv22_0 : STD_LOGIC_VECTOR (21 downto 0) := "0000000000000000000000";
    constant ap_const_lv32_15 : STD_LOGIC_VECTOR (31 downto 0) := "00000000000000000000000000010101";
    constant ap_const_lv16_9B78 : STD_LOGIC_VECTOR (15 downto 0) := "1001101101111000";
    constant ap_const_lv16_6487 : STD_LOGIC_VECTOR (15 downto 0) := "0110010010000111";
    constant ap_const_lv16_0 : STD_LOGIC_VECTOR (15 downto 0) := "0000000000000000";
    constant ap_const_logic_1 : STD_LOGIC := '1';
    constant ap_const_logic_0 : STD_LOGIC := '0';

    signal tmp_39_fu_62_p3 : STD_LOGIC_VECTOR (0 downto 0);
    signal p_Val2_s_fu_70_p2 : STD_LOGIC_VECTOR (20 downto 0);
    signal x_V_read_cast1_fu_50_p1 : STD_LOGIC_VECTOR (21 downto 0);
    signal p_Val2_2_fu_84_p2 : STD_LOGIC_VECTOR (21 downto 0);
    signal ssdm_int_V_read_assign_1_fu_90_p3 : STD_LOGIC_VECTOR (21 downto 0);
    signal tmp_40_fu_98_p3 : STD_LOGIC_VECTOR (0 downto 0);
    signal tmp_fu_54_p3 : STD_LOGIC_VECTOR (0 downto 0);
    signal ssdm_int_V_read_assign_fu_76_p3 : STD_LOGIC_VECTOR (20 downto 0);
    signal ssdm_int_V_read_assign_2_fu_114_p3 : STD_LOGIC_VECTOR (20 downto 0);
    signal y_V_read_cast2_fu_46_p1 : STD_LOGIC_VECTOR (21 downto 0);
    signal theta_V_fu_106_p3 : STD_LOGIC_VECTOR (15 downto 0);
    signal ssdm_int_V_write_assign_fu_134_p3 : STD_LOGIC_VECTOR (15 downto 0);
    signal ssdm_int_V_write_assign_cast_fu_142_p1 : STD_LOGIC_VECTOR (21 downto 0);
    signal ssdm_int_V_write_assign_1_cast_fu_122_p1 : STD_LOGIC_VECTOR (21 downto 0);
    signal ssdm_int_V_read_assign_3_fu_126_p3 : STD_LOGIC_VECTOR (21 downto 0);


begin



    ap_return_0 <= ssdm_int_V_write_assign_cast_fu_142_p1;
    ap_return_1 <= ssdm_int_V_write_assign_1_cast_fu_122_p1;
    ap_return_2 <= ssdm_int_V_read_assign_3_fu_126_p3;
    p_Val2_2_fu_84_p2 <= std_logic_vector(unsigned(ap_const_lv22_0) - unsigned(x_V_read_cast1_fu_50_p1));
    p_Val2_s_fu_70_p2 <= std_logic_vector(unsigned(ap_const_lv21_0) - unsigned(y_V_read));
    ssdm_int_V_read_assign_1_fu_90_p3 <= 
        x_V_read_cast1_fu_50_p1 when (tmp_39_fu_62_p3(0) = '1') else 
        p_Val2_2_fu_84_p2;
    ssdm_int_V_read_assign_2_fu_114_p3 <= 
        ssdm_int_V_read_assign_fu_76_p3 when (tmp_fu_54_p3(0) = '1') else 
        x_V_read;
    ssdm_int_V_read_assign_3_fu_126_p3 <= 
        ssdm_int_V_read_assign_1_fu_90_p3 when (tmp_fu_54_p3(0) = '1') else 
        y_V_read_cast2_fu_46_p1;
    ssdm_int_V_read_assign_fu_76_p3 <= 
        p_Val2_s_fu_70_p2 when (tmp_39_fu_62_p3(0) = '1') else 
        y_V_read;
    ssdm_int_V_write_assign_1_cast_fu_122_p1 <= std_logic_vector(resize(unsigned(ssdm_int_V_read_assign_2_fu_114_p3),22));
        ssdm_int_V_write_assign_cast_fu_142_p1 <= std_logic_vector(resize(signed(ssdm_int_V_write_assign_fu_134_p3),22));

    ssdm_int_V_write_assign_fu_134_p3 <= 
        theta_V_fu_106_p3 when (tmp_fu_54_p3(0) = '1') else 
        ap_const_lv16_0;
    theta_V_fu_106_p3 <= 
        ap_const_lv16_9B78 when (tmp_40_fu_98_p3(0) = '1') else 
        ap_const_lv16_6487;
    tmp_39_fu_62_p3 <= y_V_read(20 downto 20);
    tmp_40_fu_98_p3 <= ssdm_int_V_read_assign_1_fu_90_p3(21 downto 21);
    tmp_fu_54_p3 <= x_V_read(20 downto 20);
        x_V_read_cast1_fu_50_p1 <= std_logic_vector(resize(signed(x_V_read),22));

        y_V_read_cast2_fu_46_p1 <= std_logic_vector(resize(signed(y_V_read),22));

end behav;
