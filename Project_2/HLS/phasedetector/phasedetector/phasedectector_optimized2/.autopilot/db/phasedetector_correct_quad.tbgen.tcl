set moduleName phasedetector_correct_quad
set isCombinational 1
set isDatapathOnly 0
set isPipelined 0
set pipeline_type function
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {phasedetector_correct_quad}
set C_modelType { int 66 }
set C_modelArgList { 
	{ x_V_read int 21 regular  }
	{ y_V_read int 21 regular  }
}
set C_modelArgMapList {[ 
	{ "Name" : "x_V_read", "interface" : "wire", "bitwidth" : 21, "direction" : "READONLY"} , 
 	{ "Name" : "y_V_read", "interface" : "wire", "bitwidth" : 21, "direction" : "READONLY"} , 
 	{ "Name" : "ap_return", "interface" : "wire", "bitwidth" : 66} ]}
# RTL Port declarations: 
set portNum 5
set portList { 
	{ x_V_read sc_in sc_lv 21 signal 0 } 
	{ y_V_read sc_in sc_lv 21 signal 1 } 
	{ ap_return_0 sc_out sc_lv 22 signal -1 } 
	{ ap_return_1 sc_out sc_lv 22 signal -1 } 
	{ ap_return_2 sc_out sc_lv 22 signal -1 } 
}
set NewPortList {[ 
	{ "name": "x_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":21, "type": "signal", "bundle":{"name": "x_V_read", "role": "default" }} , 
 	{ "name": "y_V_read", "direction": "in", "datatype": "sc_lv", "bitwidth":21, "type": "signal", "bundle":{"name": "y_V_read", "role": "default" }} , 
 	{ "name": "ap_return_0", "direction": "out", "datatype": "sc_lv", "bitwidth":22, "type": "signal", "bundle":{"name": "ap_return_0", "role": "default" }} , 
 	{ "name": "ap_return_1", "direction": "out", "datatype": "sc_lv", "bitwidth":22, "type": "signal", "bundle":{"name": "ap_return_1", "role": "default" }} , 
 	{ "name": "ap_return_2", "direction": "out", "datatype": "sc_lv", "bitwidth":22, "type": "signal", "bundle":{"name": "ap_return_2", "role": "default" }}  ]}
set Spec2ImplPortList { 
	x_V_read { ap_none {  { x_V_read in_data 0 21 } } }
	y_V_read { ap_none {  { y_V_read in_data 0 21 } } }
}
