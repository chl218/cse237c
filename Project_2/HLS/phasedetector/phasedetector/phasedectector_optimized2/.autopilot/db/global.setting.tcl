
set TopModule "phasedetector"
set ClockPeriod "10.000000"
set ClockList {ap_clk}
set multiClockList {}
set PortClockMap {}
set CombLogicFlag 0
set PipelineFlag 1
set DataflowTaskPipelineFlag  1
set TrivialPipelineFlag 0
set noPortSwitchingFlag 0
set FloatingPointFlag 1
set FftOrFirFlag 0
set NbRWValue 0
set intNbAccess 0
set NewDSPMapping 1
set HasDSPModule 1
set ResetLevelFlag 1
set ResetStyle "control"
set ResetSyncFlag 1
set ResetRegisterFlag 0
set ResetVariableFlag 0
set fsmEncStyle "onehot"
set maxFanout "0"
set RtlPrefix ""
set ExtraCCFlags ""
set ExtraCLdFlags ""
set SynCheckOptions ""
set PresynOptions ""
set PreprocOptions ""
set SchedOptions ""
set BindOptions ""
set RtlGenOptions ""
set RtlWriterOptions ""
set CbcGenFlag ""
set CasGenFlag ""
set CasMonitorFlag ""
set AutoSimOptions {}
set ExportMCPathFlag "0"
set SCTraceFileName "mytrace"
set SCTraceFileFormat "vcd"
set SCTraceOption "all"
set TargetInfo "xc7z020:clg484:-1"
set SourceFiles {sc {} c {../../phasedetector.cpp ../../fir.cpp ../../cordiccart2pol.cpp}}
set SourceFlags {sc {} c {{} {} {}}}
set DirectiveFile {D:/Projects/vivado/Project_2/HLS/phasedetector/phasedetector/phasedectector_optimized2/phasedectector_optimized2.directive}
set TBFiles {verilog {../../phasedetector_test.cpp ../../out.gold.dat ../../input_q.dat ../../input_i.dat} bc {../../phasedetector_test.cpp ../../out.gold.dat ../../input_q.dat ../../input_i.dat} sc {../../phasedetector_test.cpp ../../out.gold.dat ../../input_q.dat ../../input_i.dat} vhdl {../../phasedetector_test.cpp ../../out.gold.dat ../../input_q.dat ../../input_i.dat} c {} cas {../../phasedetector_test.cpp ../../out.gold.dat ../../input_q.dat ../../input_i.dat}}
set SpecLanguage "C"
set TVInFiles {bc {} c {} sc {} cas {} vhdl {} verilog {}}
set TVOutFiles {bc {} c {} sc {} cas {} vhdl {} verilog {}}
set TBTops {verilog {} bc {} sc {} vhdl {} c {} cas {}}
set TBInstNames {verilog {} bc {} sc {} vhdl {} c {} cas {}}
set ExtraGlobalOptions {"area_timing" 1 "clock_gate" 1 "impl_flow" map "power_gate" 0}
set PlatformFiles {{DefaultPlatform {xilinx/zynq/zynq xilinx/zynq/zynq_fpv6}}}
set DefaultPlatform "DefaultPlatform"
set TBTVFileNotFound ""
set AppFile "../vivado_hls.app"
set ApsFile "phasedectector_optimized2.aps"
set AvePath "../.."
set HPFPO "0"
