/*
	Filename: phasedetector.cpp
		Phase detector

	INPUT:
		I: signal for I sample
		Q: signal for Q sample

	OUTPUT:
		R: Radius
		Theta: Angle

*/

#include "phasedetector.h"

void phasedetector (
  data_t I,
  data_t Q,

  data_t *R,
  data_t *theta
  ){
#pragma HLS PIPELINE II=1

	fir_data_t x, y;
	cordic_data_t r, t;

	fir((fir_data_t)I, (fir_data_t)Q, &x, &y);
	cordiccart2pol((cordic_data_t)x, (cordic_data_t)y, &r, &t);

	*R = (data_t)r;
	*theta = (data_t)t;
}




