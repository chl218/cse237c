/*
	Filename: fir.h
		Header file
		FIR lab written for 237C class at UCSD.

*/
#ifndef PHASE_DETECTOR_H_
#define PHASE_DETECTOR_H_

#include <ap_int.h>
#include <ap_fixed.h>

const int N = 32;

#define NO_ITER 12
#define NO_COEF 16

typedef float data_t;
typedef ap_int<2> fir_coef_t;

typedef ap_fixed<19,7> fir_data_t;
typedef ap_fixed<22,8> cordic_data_t;


void phasedetector (
  data_t I,
  data_t Q,

  data_t *R,
  data_t *theta
  );

void cordiccart2pol(
	cordic_data_t x,
	cordic_data_t y,

	cordic_data_t *r,
	cordic_data_t *theta);

void fir (
	fir_data_t I,
	fir_data_t Q,

	fir_data_t *X,
	fir_data_t *Y
  );

#endif
