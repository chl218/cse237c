/*
	Filename: fir_test.h
		FIR lab wirtten for 237C class at UCSD.
		Testbench file
*/


#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "phasedetector.h"

struct Rmse
{
	int num_sq;
	float sum_sq;
	float error;

	Rmse(){ num_sq = 0; sum_sq = 0; error = 0; }

	float add_value(float d_n)
	{
		num_sq++;
		sum_sq += (d_n*d_n);
		error = sqrtf(sum_sq / num_sq);
		return error;
	}

};


Rmse rmse_theta;
Rmse rmse_r;


int main ()
{
  float rms_error_r, rms_error_th;

  const int    SAMPLES = 1024;

  data_t output_R, output_Theta;
  int i;

  float signal_I = 0;
  float signal_Q = 0;

  float gold_R[SAMPLES];
  float gold_Theta[SAMPLES];


  FILE * finGold = fopen("out.gold.dat","r");
  for (i = 0; i < SAMPLES; i++)
  {
	  fscanf(finGold, "%f %f", &gold_R[i], &gold_Theta[i]);
	  //printf("%f, %f\n",gold_R[i], gold_Theta[i]);
  }
  fclose(finGold);

  FILE * finI = fopen("input_i.dat","r");
  FILE * finQ = fopen("input_q.dat","r");
  FILE * fp   = fopen("out.dat","w");

  for (i = 0; i < SAMPLES;i++)
  {
	  fscanf(finI,"%f",&signal_I);
	  fscanf(finQ,"%f",&signal_Q);

	  //Call the HLS block
	  phasedetector(signal_I, signal_Q, &output_R, &output_Theta);

	  // Accumulating error
	  rms_error_r  = rmse_r.add_value((float)output_R - gold_R[i]);
	  rms_error_th = rmse_theta.add_value((float)output_Theta - gold_Theta[i]);

	  fprintf(fp, "%f %f\n", (float)output_R, (float)output_Theta);
	  printf("%i\tI:%f\tQ:%f\tR:%f\ttheta:%f\n",
			  i, signal_I, signal_Q, output_R, output_Theta, 20);
  }

  fclose(fp);
  fclose(finQ);
  fclose(finI);

  printf("----------------------------------------------\n");
  printf("   RMSE(R)           RMSE(Theta)\n");
  printf("%0.15f %0.15f\n", rmse_r.error, rmse_theta.error);
  printf("----------------------------------------------\n");

  float error_threshold = 0.001;

  int success = (rmse_r.error < error_threshold) && (rmse_theta.error < error_threshold);

  if (success) return 0;
  else return 1;
}
