#include "phasedetector.h"

//cordic_data_t Kvalues[NO_ITER] = {1,	0.500000000000000,	0.250000000000000,	0.125000000000000,	0.0625000000000000,	0.0312500000000000,	0.0156250000000000,	0.00781250000000000,	0.00390625000000000,	0.00195312500000000,	0.000976562500000000,	0.000488281250000000,	0.000244140625000000,	0.000122070312500000,	6.10351562500000e-05,	3.05175781250000e-05};

cordic_data_t angles[NO_COEF] = {0.785398163397448,	0.463647609000806,	0.244978663126864,	0.124354994546761,	0.0624188099959574,	0.0312398334302683,	0.0156237286204768,	0.00781234106010111,	0.00390623013196697,	0.00195312251647882,	0.000976562189559320,	0.000488281211194898,	0.000244140620149362,	0.000122070311893670,	6.10351561742088e-05,	3.05175781155261e-05};

void correctQuadrant(cordic_data_t * x, cordic_data_t * y, cordic_data_t * theta)
{
	#pragma HLS PIPELINE II=1
	cordic_data_t temp_x = *x;
	if (*x < 0) {
		*x = *y < 0 ? (cordic_data_t)-*y  : *y;
		*y = *y < 0 ? temp_x : (cordic_data_t)-temp_x;
		*theta = *y < 0 ? (cordic_data_t)-1.57079632679 : (cordic_data_t)1.57079632679;
	}
}


void cordiccart2pol(cordic_data_t x, cordic_data_t y, cordic_data_t * r, cordic_data_t * theta)
{
	*theta = 0;

	correctQuadrant(&x, &y, theta);

	cordic_data_t temp_x;

	Shift_Add_Loop: for (ap_uint<5> j = 0; j < NO_ITER; j++){
	#pragma HLS PIPELINE II=1
		temp_x = x;

		*theta = y < 0 ? *theta - angles[j] : *theta + angles[j];
		x = y < 0 ? x - (y >> j): x + (y >> j);
		y = y < 0 ? y + (temp_x >> j) : y - (temp_x >> j);

	}

	*r = x * (cordic_data_t)0.60727884422;


}
