/*
	Filename: fir.cpp
		Complex FIR or Match filter
		firI1 and firI2 share fir_coef_t c[N]
		firQ1 and firQ2 share fir_coef_t c[N]

	INPUT:
		I: signal for I sample
		I: signal for Q sample

	OUTPUT:
		X: filtered output
		Y: filtered output

*/

#include "phasedetector.h"

void firI1 (
		fir_data_t *y,
		fir_data_t x
  ) {

	fir_coef_t c[N] = {1,    -1,    1,    -1,    -1,    -1,    1,    1,    -1,    -1,    -1,    1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    1,    1,    1,    1,    -1,    -1,    1,    1,    1,    -1,    -1,    -1};

	static fir_data_t shift_reg[N];
	#pragma HLS ARRAY_PARTITION variable=shift_reg cyclic factor=8 dim=1

	fir_data_t acc = 0;
	Shift_Accum_Loop: for(ap_uint<5> i = N-1; i >= 1; i--) {
	#pragma HLS PIPELINE II=1
		shift_reg[i] = shift_reg[i-1];
		acc = c[i] < (fir_coef_t)0 ? acc - shift_reg[i]
				    			   : acc + shift_reg[i];
	}

	shift_reg[0] = x;
	*y = c[0] < (fir_coef_t)0 ? acc - x : acc + x;

}


void firI2 (
	fir_data_t *y,
	fir_data_t x
  ) {

	fir_coef_t c[N] = {1,    -1,    1,    -1,    -1,    -1,    1,    1,    -1,    -1,    -1,    1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    1,    1,    1,    1,    -1,    -1,    1,    1,    1,    -1,    -1,    -1};

	static fir_data_t shift_reg[N];
	#pragma HLS ARRAY_PARTITION variable=shift_reg cyclic factor=8 dim=1

	fir_data_t acc = 0;
	Shift_Accum_Loop: for(ap_uint<5> i = N-1; i >= 1; i--) {
	#pragma HLS PIPELINE II=1
		shift_reg[i] = shift_reg[i-1];
		acc = c[i] < (fir_coef_t)0 ? acc - shift_reg[i]
				    			   : acc + shift_reg[i];
	}

	shift_reg[0] = x;
	*y = c[0] < (fir_coef_t)0 ? acc - x : acc + x;

}

void firQ1 (
	fir_data_t *y,
	fir_data_t x
  ) {

	fir_coef_t c[N] = {-1,    -1,    1,    -1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    -1,    1,    -1,    1,    1,    -1,    1,    -1,    -1,    1,    -1,    1,    1,    1,    1,    -1,    1,    -1,    1,    1};

	static fir_data_t shift_reg[N];
	#pragma HLS ARRAY_PARTITION variable=shift_reg cyclic factor=8 dim=1

	fir_data_t acc = 0;
	Shift_Accum_Loop: for(ap_uint<5> i = N-1; i >= 1; i--) {
	#pragma HLS PIPELINE II=1
		shift_reg[i] = shift_reg[i-1];
		acc = c[i] < (fir_coef_t)0 ? acc - shift_reg[i]
				    			   : acc + shift_reg[i];
	}

	shift_reg[0] = x;
	*y = c[0] < (fir_coef_t)0 ? acc - x : acc + x;

}

void firQ2 (
	fir_data_t *y,
	fir_data_t x
  ) {

	fir_coef_t c[N] = {-1,    -1,    1,    -1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    -1,    1,    -1,    1,    1,    -1,    1,    -1,    -1,    1,    -1,    1,    1,    1,    1,    -1,    1,    -1,    1,    1};

	static fir_data_t shift_reg[N];
	#pragma HLS ARRAY_PARTITION variable=shift_reg cyclic factor=8 dim=1

	fir_data_t acc = 0;
	Shift_Accum_Loop: for(ap_uint<5> i = N-1; i >= 1; i--) {
	#pragma HLS PIPELINE II=1
		shift_reg[i] = shift_reg[i-1];
		acc = c[i] < (fir_coef_t)0 ? acc - shift_reg[i]
				    			   : acc + shift_reg[i];
	}

	shift_reg[0] = x;
	*y = c[0] < (fir_coef_t)0 ? acc - x : acc + x;

}

void fir (
	fir_data_t I,
	fir_data_t Q,

	fir_data_t *X,
	fir_data_t *Y
  ) {

	fir_data_t IinIfir=0, QinQfir=0, QinIfir=0, IinQfir=0;

	firI1(&IinIfir, I);
	firQ1(&QinQfir, Q);

	firI2(&QinIfir, Q);
	firQ2(&IinQfir, I);

	//Calculate X
	*X = IinIfir + QinQfir;
	//Calculate Y
	*Y = QinIfir - IinQfir;

}
