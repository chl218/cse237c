// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

#ifndef _fir_firQ2_HH_
#define _fir_firQ2_HH_

#include "systemc.h"
#include "AESL_pkg.h"

#include "fir_mux_32to1_sel6_19_1.h"
#include "fir_firQ1_c2.h"

namespace ap_rtl {

struct fir_firQ2 : public sc_module {
    // Port declarations 8
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_logic > ap_start;
    sc_out< sc_logic > ap_done;
    sc_out< sc_logic > ap_idle;
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<19> > x_V;
    sc_out< sc_lv<19> > ap_return;


    // Module declarations
    fir_firQ2(sc_module_name name);
    SC_HAS_PROCESS(fir_firQ2);

    ~fir_firQ2();

    sc_trace_file* mVcdFile;

    fir_firQ1_c2* c3_U;
    fir_mux_32to1_sel6_19_1<1,1,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,19,6,19>* fir_mux_32to1_sel6_19_1_U8;
    sc_signal< sc_lv<3> > ap_CS_fsm;
    sc_signal< sc_logic > ap_sig_cseq_ST_st1_fsm_0;
    sc_signal< bool > ap_sig_bdd_21;
    sc_signal< sc_lv<19> > shift_reg_V_0_0;
    sc_signal< sc_lv<19> > shift_reg_V_0_1;
    sc_signal< sc_lv<19> > shift_reg_V_0_2;
    sc_signal< sc_lv<19> > shift_reg_V_0_3;
    sc_signal< sc_lv<19> > shift_reg_V_153_0;
    sc_signal< sc_lv<19> > shift_reg_V_153_1;
    sc_signal< sc_lv<19> > shift_reg_V_153_2;
    sc_signal< sc_lv<19> > shift_reg_V_153_3;
    sc_signal< sc_lv<19> > shift_reg_V_254_0;
    sc_signal< sc_lv<19> > shift_reg_V_254_1;
    sc_signal< sc_lv<19> > shift_reg_V_254_2;
    sc_signal< sc_lv<19> > shift_reg_V_254_3;
    sc_signal< sc_lv<19> > shift_reg_V_355_0;
    sc_signal< sc_lv<19> > shift_reg_V_355_1;
    sc_signal< sc_lv<19> > shift_reg_V_355_2;
    sc_signal< sc_lv<19> > shift_reg_V_355_3;
    sc_signal< sc_lv<19> > shift_reg_V_4_0;
    sc_signal< sc_lv<19> > shift_reg_V_4_1;
    sc_signal< sc_lv<19> > shift_reg_V_4_2;
    sc_signal< sc_lv<19> > shift_reg_V_4_3;
    sc_signal< sc_lv<19> > shift_reg_V_5_0;
    sc_signal< sc_lv<19> > shift_reg_V_5_1;
    sc_signal< sc_lv<19> > shift_reg_V_5_2;
    sc_signal< sc_lv<19> > shift_reg_V_5_3;
    sc_signal< sc_lv<19> > shift_reg_V_6_0;
    sc_signal< sc_lv<19> > shift_reg_V_6_1;
    sc_signal< sc_lv<19> > shift_reg_V_6_2;
    sc_signal< sc_lv<19> > shift_reg_V_6_3;
    sc_signal< sc_lv<19> > shift_reg_V_7_0;
    sc_signal< sc_lv<19> > shift_reg_V_7_1;
    sc_signal< sc_lv<19> > shift_reg_V_7_2;
    sc_signal< sc_lv<19> > shift_reg_V_7_3;
    sc_signal< sc_lv<5> > c3_address0;
    sc_signal< sc_logic > c3_ce0;
    sc_signal< sc_lv<2> > c3_q0;
    sc_signal< sc_lv<1> > shift_reg_V_0_1_flag_reg_282;
    sc_signal< sc_lv<19> > p_Val2_s_reg_296;
    sc_signal< sc_lv<5> > p_s_reg_308;
    sc_signal< sc_lv<1> > tmp_fu_700_p2;
    sc_signal< sc_lv<1> > tmp_reg_1438;
    sc_signal< sc_logic > ap_sig_cseq_ST_pp0_stg0_fsm_1;
    sc_signal< bool > ap_sig_bdd_86;
    sc_signal< sc_logic > ap_reg_ppiten_pp0_it0;
    sc_signal< sc_logic > ap_reg_ppiten_pp0_it1;
    sc_signal< sc_logic > ap_reg_ppiten_pp0_it2;
    sc_signal< sc_lv<1> > ap_reg_ppstg_tmp_reg_1438_pp0_it1;
    sc_signal< sc_lv<3> > tmp_3_fu_721_p1;
    sc_signal< sc_lv<3> > tmp_3_reg_1442;
    sc_signal< sc_lv<2> > newIndex_t_reg_1447;
    sc_signal< sc_lv<3> > tmp_4_fu_735_p1;
    sc_signal< sc_lv<3> > tmp_4_reg_1452;
    sc_signal< sc_lv<2> > newIndex15_t_reg_1456;
    sc_signal< sc_lv<5> > i_V_fu_749_p2;
    sc_signal< sc_lv<19> > tmp_2_fu_768_p34;
    sc_signal< sc_lv<19> > tmp_2_reg_1470;
    sc_signal< sc_lv<2> > f_op_V_reg_1475;
    sc_signal< sc_lv<19> > acc_V_fu_1006_p2;
    sc_signal< sc_lv<64> > tmp_7_fu_706_p1;
    sc_signal< sc_logic > ap_sig_cseq_ST_st5_fsm_2;
    sc_signal< bool > ap_sig_bdd_135;
    sc_signal< sc_lv<19> > shift_reg_V_7_3_loc_fu_136;
    sc_signal< sc_lv<19> > shift_reg_V_7_2_loc_fu_140;
    sc_signal< sc_lv<19> > shift_reg_V_7_1_loc_fu_144;
    sc_signal< sc_lv<19> > shift_reg_V_7_0_loc_fu_148;
    sc_signal< sc_lv<19> > shift_reg_V_6_3_loc_fu_152;
    sc_signal< sc_lv<19> > shift_reg_V_6_2_loc_fu_156;
    sc_signal< sc_lv<19> > shift_reg_V_6_1_loc_fu_160;
    sc_signal< sc_lv<19> > shift_reg_V_6_0_loc_fu_164;
    sc_signal< sc_lv<19> > shift_reg_V_5_3_loc_fu_168;
    sc_signal< sc_lv<19> > shift_reg_V_5_2_loc_fu_172;
    sc_signal< sc_lv<19> > shift_reg_V_5_1_loc_fu_176;
    sc_signal< sc_lv<19> > shift_reg_V_5_0_loc_fu_180;
    sc_signal< sc_lv<19> > shift_reg_V_4_3_loc_fu_184;
    sc_signal< sc_lv<19> > shift_reg_V_4_2_loc_fu_188;
    sc_signal< sc_lv<19> > shift_reg_V_4_1_loc_fu_192;
    sc_signal< sc_lv<19> > shift_reg_V_4_0_loc_fu_196;
    sc_signal< sc_lv<19> > shift_reg_V_355_3_loc_fu_200;
    sc_signal< sc_lv<19> > shift_reg_V_355_2_loc_fu_204;
    sc_signal< sc_lv<19> > shift_reg_V_355_1_loc_fu_208;
    sc_signal< sc_lv<19> > shift_reg_V_355_0_loc_fu_212;
    sc_signal< sc_lv<19> > shift_reg_V_254_3_loc_fu_216;
    sc_signal< sc_lv<19> > shift_reg_V_254_2_loc_fu_220;
    sc_signal< sc_lv<19> > shift_reg_V_254_1_loc_fu_224;
    sc_signal< sc_lv<19> > shift_reg_V_254_0_loc_fu_228;
    sc_signal< sc_lv<19> > shift_reg_V_153_3_loc_fu_232;
    sc_signal< sc_lv<19> > shift_reg_V_153_2_loc_fu_236;
    sc_signal< sc_lv<19> > shift_reg_V_153_1_loc_fu_240;
    sc_signal< sc_lv<19> > shift_reg_V_153_0_loc_fu_244;
    sc_signal< sc_lv<19> > shift_reg_V_0_3_loc_fu_248;
    sc_signal< sc_lv<19> > shift_reg_V_0_2_loc_fu_252;
    sc_signal< sc_lv<19> > shift_reg_V_0_1_loc_fu_256;
    sc_signal< sc_lv<19> > shift_reg_V_0_0_loc_fu_260;
    sc_signal< sc_lv<6> > lhs_V_cast_fu_711_p1;
    sc_signal< sc_lv<6> > r_V_fu_715_p2;
    sc_signal< sc_lv<5> > tmp_6_fu_758_p3;
    sc_signal< sc_lv<6> > tmp_2_fu_768_p33;
    sc_signal< sc_lv<2> > p_Val2_4_fu_1001_p1;
    sc_signal< sc_lv<19> > p_Val2_4_fu_1001_p2;
    sc_signal< sc_lv<3> > ap_NS_fsm;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const sc_lv<3> ap_ST_st1_fsm_0;
    static const sc_lv<3> ap_ST_pp0_stg0_fsm_1;
    static const sc_lv<3> ap_ST_st5_fsm_2;
    static const bool ap_true;
    static const sc_lv<32> ap_const_lv32_0;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<19> ap_const_lv19_0;
    static const sc_lv<32> ap_const_lv32_1;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_lv<5> ap_const_lv5_1F;
    static const sc_lv<32> ap_const_lv32_2;
    static const sc_lv<3> ap_const_lv3_6;
    static const sc_lv<3> ap_const_lv3_5;
    static const sc_lv<3> ap_const_lv3_4;
    static const sc_lv<3> ap_const_lv3_3;
    static const sc_lv<3> ap_const_lv3_2;
    static const sc_lv<3> ap_const_lv3_1;
    static const sc_lv<3> ap_const_lv3_0;
    static const sc_lv<2> ap_const_lv2_2;
    static const sc_lv<2> ap_const_lv2_1;
    static const sc_lv<2> ap_const_lv2_0;
    static const sc_lv<5> ap_const_lv5_0;
    static const sc_lv<6> ap_const_lv6_3F;
    static const sc_lv<32> ap_const_lv32_3;
    static const sc_lv<32> ap_const_lv32_4;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_acc_V_fu_1006_p2();
    void thread_ap_done();
    void thread_ap_idle();
    void thread_ap_ready();
    void thread_ap_return();
    void thread_ap_sig_bdd_135();
    void thread_ap_sig_bdd_21();
    void thread_ap_sig_bdd_86();
    void thread_ap_sig_cseq_ST_pp0_stg0_fsm_1();
    void thread_ap_sig_cseq_ST_st1_fsm_0();
    void thread_ap_sig_cseq_ST_st5_fsm_2();
    void thread_c3_address0();
    void thread_c3_ce0();
    void thread_i_V_fu_749_p2();
    void thread_lhs_V_cast_fu_711_p1();
    void thread_p_Val2_4_fu_1001_p1();
    void thread_p_Val2_4_fu_1001_p2();
    void thread_r_V_fu_715_p2();
    void thread_tmp_2_fu_768_p33();
    void thread_tmp_3_fu_721_p1();
    void thread_tmp_4_fu_735_p1();
    void thread_tmp_6_fu_758_p3();
    void thread_tmp_7_fu_706_p1();
    void thread_tmp_fu_700_p2();
    void thread_ap_NS_fsm();
};

}

using namespace ap_rtl;

#endif
