// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module fir_firQ1 (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_idle,
        ap_ready,
        x_V,
        ap_return
);

parameter    ap_const_logic_1 = 1'b1;
parameter    ap_const_logic_0 = 1'b0;
parameter    ap_ST_st1_fsm_0 = 3'b1;
parameter    ap_ST_pp0_stg0_fsm_1 = 3'b10;
parameter    ap_ST_st5_fsm_2 = 3'b100;
parameter    ap_true = 1'b1;
parameter    ap_const_lv32_0 = 32'b00000000000000000000000000000000;
parameter    ap_const_lv1_1 = 1'b1;
parameter    ap_const_lv19_0 = 19'b0000000000000000000;
parameter    ap_const_lv32_1 = 32'b1;
parameter    ap_const_lv1_0 = 1'b0;
parameter    ap_const_lv5_1F = 5'b11111;
parameter    ap_const_lv32_2 = 32'b10;
parameter    ap_const_lv3_6 = 3'b110;
parameter    ap_const_lv3_5 = 3'b101;
parameter    ap_const_lv3_4 = 3'b100;
parameter    ap_const_lv3_3 = 3'b11;
parameter    ap_const_lv3_2 = 3'b10;
parameter    ap_const_lv3_1 = 3'b1;
parameter    ap_const_lv3_0 = 3'b000;
parameter    ap_const_lv2_2 = 2'b10;
parameter    ap_const_lv2_1 = 2'b1;
parameter    ap_const_lv2_0 = 2'b00;
parameter    ap_const_lv5_0 = 5'b00000;
parameter    ap_const_lv6_3F = 6'b111111;
parameter    ap_const_lv32_3 = 32'b11;
parameter    ap_const_lv32_4 = 32'b100;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
output   ap_idle;
output   ap_ready;
input  [18:0] x_V;
output  [18:0] ap_return;

reg ap_done;
reg ap_idle;
reg ap_ready;
(* fsm_encoding = "none" *) reg   [2:0] ap_CS_fsm = 3'b1;
reg    ap_sig_cseq_ST_st1_fsm_0;
reg    ap_sig_bdd_21;
reg   [18:0] shift_reg_V_1_0_0 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_0_1 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_0_2 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_0_3 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_1_0 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_1_1 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_1_2 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_1_3 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_2_0 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_2_1 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_2_2 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_2_3 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_3_0 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_3_1 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_3_2 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_3_3 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_4_0 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_4_1 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_4_2 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_4_3 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_5_0 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_5_1 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_5_2 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_5_3 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_6_0 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_6_1 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_6_2 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_6_3 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_7_0 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_7_1 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_7_2 = 19'b0000000000000000000;
reg   [18:0] shift_reg_V_1_7_3 = 19'b0000000000000000000;
wire   [4:0] c2_address0;
reg    c2_ce0;
wire   [1:0] c2_q0;
reg   [0:0] shift_reg_V_1_0_1_flag_reg_282;
reg   [18:0] p_Val2_s_reg_296;
reg   [4:0] p_s_reg_308;
wire   [0:0] tmp_fu_700_p2;
reg   [0:0] tmp_reg_1438;
reg    ap_sig_cseq_ST_pp0_stg0_fsm_1;
reg    ap_sig_bdd_86;
reg    ap_reg_ppiten_pp0_it0 = 1'b0;
reg    ap_reg_ppiten_pp0_it1 = 1'b0;
reg    ap_reg_ppiten_pp0_it2 = 1'b0;
reg   [0:0] ap_reg_ppstg_tmp_reg_1438_pp0_it1;
wire   [2:0] tmp_5_fu_721_p1;
reg   [2:0] tmp_5_reg_1442;
reg   [1:0] newIndex_t_reg_1447;
wire   [2:0] tmp_6_fu_735_p1;
reg   [2:0] tmp_6_reg_1452;
reg   [1:0] newIndex15_t_reg_1456;
wire   [4:0] i_V_fu_749_p2;
wire  signed [18:0] tmp_3_fu_768_p34;
reg  signed [18:0] tmp_3_reg_1470;
reg   [1:0] f_op_V_reg_1475;
wire   [18:0] acc_V_fu_1006_p2;
wire   [63:0] tmp_8_fu_706_p1;
reg    ap_sig_cseq_ST_st5_fsm_2;
reg    ap_sig_bdd_135;
reg   [18:0] shift_reg_V_1_7_3_loc_fu_136;
reg   [18:0] shift_reg_V_1_7_2_loc_fu_140;
reg   [18:0] shift_reg_V_1_7_1_loc_fu_144;
reg   [18:0] shift_reg_V_1_7_0_loc_fu_148;
reg   [18:0] shift_reg_V_1_6_3_loc_fu_152;
reg   [18:0] shift_reg_V_1_6_2_loc_fu_156;
reg   [18:0] shift_reg_V_1_6_1_loc_fu_160;
reg   [18:0] shift_reg_V_1_6_0_loc_fu_164;
reg   [18:0] shift_reg_V_1_5_3_loc_fu_168;
reg   [18:0] shift_reg_V_1_5_2_loc_fu_172;
reg   [18:0] shift_reg_V_1_5_1_loc_fu_176;
reg   [18:0] shift_reg_V_1_5_0_loc_fu_180;
reg   [18:0] shift_reg_V_1_4_3_loc_fu_184;
reg   [18:0] shift_reg_V_1_4_2_loc_fu_188;
reg   [18:0] shift_reg_V_1_4_1_loc_fu_192;
reg   [18:0] shift_reg_V_1_4_0_loc_fu_196;
reg   [18:0] shift_reg_V_1_3_3_loc_fu_200;
reg   [18:0] shift_reg_V_1_3_2_loc_fu_204;
reg   [18:0] shift_reg_V_1_3_1_loc_fu_208;
reg   [18:0] shift_reg_V_1_3_0_loc_fu_212;
reg   [18:0] shift_reg_V_1_2_3_loc_fu_216;
reg   [18:0] shift_reg_V_1_2_2_loc_fu_220;
reg   [18:0] shift_reg_V_1_2_1_loc_fu_224;
reg   [18:0] shift_reg_V_1_2_0_loc_fu_228;
reg   [18:0] shift_reg_V_1_1_3_loc_fu_232;
reg   [18:0] shift_reg_V_1_1_2_loc_fu_236;
reg   [18:0] shift_reg_V_1_1_1_loc_fu_240;
reg   [18:0] shift_reg_V_1_1_0_loc_fu_244;
reg   [18:0] shift_reg_V_1_0_3_loc_fu_248;
reg   [18:0] shift_reg_V_1_0_2_loc_fu_252;
reg   [18:0] shift_reg_V_1_0_1_loc_fu_256;
reg   [18:0] shift_reg_V_1_0_0_loc_fu_260;
wire   [5:0] lhs_V_cast_fu_711_p1;
wire   [5:0] r_V_fu_715_p2;
wire   [4:0] tmp_1_fu_758_p3;
wire   [5:0] tmp_3_fu_768_p33;
wire  signed [1:0] p_Val2_9_fu_1001_p1;
wire   [18:0] p_Val2_9_fu_1001_p2;
reg   [2:0] ap_NS_fsm;


fir_firQ1_c2 #(
    .DataWidth( 2 ),
    .AddressRange( 32 ),
    .AddressWidth( 5 ))
c2_U(
    .clk( ap_clk ),
    .reset( ap_rst ),
    .address0( c2_address0 ),
    .ce0( c2_ce0 ),
    .q0( c2_q0 )
);

fir_mux_32to1_sel6_19_1 #(
    .ID( 1 ),
    .NUM_STAGE( 1 ),
    .din1_WIDTH( 19 ),
    .din2_WIDTH( 19 ),
    .din3_WIDTH( 19 ),
    .din4_WIDTH( 19 ),
    .din5_WIDTH( 19 ),
    .din6_WIDTH( 19 ),
    .din7_WIDTH( 19 ),
    .din8_WIDTH( 19 ),
    .din9_WIDTH( 19 ),
    .din10_WIDTH( 19 ),
    .din11_WIDTH( 19 ),
    .din12_WIDTH( 19 ),
    .din13_WIDTH( 19 ),
    .din14_WIDTH( 19 ),
    .din15_WIDTH( 19 ),
    .din16_WIDTH( 19 ),
    .din17_WIDTH( 19 ),
    .din18_WIDTH( 19 ),
    .din19_WIDTH( 19 ),
    .din20_WIDTH( 19 ),
    .din21_WIDTH( 19 ),
    .din22_WIDTH( 19 ),
    .din23_WIDTH( 19 ),
    .din24_WIDTH( 19 ),
    .din25_WIDTH( 19 ),
    .din26_WIDTH( 19 ),
    .din27_WIDTH( 19 ),
    .din28_WIDTH( 19 ),
    .din29_WIDTH( 19 ),
    .din30_WIDTH( 19 ),
    .din31_WIDTH( 19 ),
    .din32_WIDTH( 19 ),
    .din33_WIDTH( 6 ),
    .dout_WIDTH( 19 ))
fir_mux_32to1_sel6_19_1_U3(
    .din1( shift_reg_V_1_0_0_loc_fu_260 ),
    .din2( shift_reg_V_1_0_1_loc_fu_256 ),
    .din3( shift_reg_V_1_0_2_loc_fu_252 ),
    .din4( shift_reg_V_1_0_3_loc_fu_248 ),
    .din5( shift_reg_V_1_1_0_loc_fu_244 ),
    .din6( shift_reg_V_1_1_1_loc_fu_240 ),
    .din7( shift_reg_V_1_1_2_loc_fu_236 ),
    .din8( shift_reg_V_1_1_3_loc_fu_232 ),
    .din9( shift_reg_V_1_2_0_loc_fu_228 ),
    .din10( shift_reg_V_1_2_1_loc_fu_224 ),
    .din11( shift_reg_V_1_2_2_loc_fu_220 ),
    .din12( shift_reg_V_1_2_3_loc_fu_216 ),
    .din13( shift_reg_V_1_3_0_loc_fu_212 ),
    .din14( shift_reg_V_1_3_1_loc_fu_208 ),
    .din15( shift_reg_V_1_3_2_loc_fu_204 ),
    .din16( shift_reg_V_1_3_3_loc_fu_200 ),
    .din17( shift_reg_V_1_4_0_loc_fu_196 ),
    .din18( shift_reg_V_1_4_1_loc_fu_192 ),
    .din19( shift_reg_V_1_4_2_loc_fu_188 ),
    .din20( shift_reg_V_1_4_3_loc_fu_184 ),
    .din21( shift_reg_V_1_5_0_loc_fu_180 ),
    .din22( shift_reg_V_1_5_1_loc_fu_176 ),
    .din23( shift_reg_V_1_5_2_loc_fu_172 ),
    .din24( shift_reg_V_1_5_3_loc_fu_168 ),
    .din25( shift_reg_V_1_6_0_loc_fu_164 ),
    .din26( shift_reg_V_1_6_1_loc_fu_160 ),
    .din27( shift_reg_V_1_6_2_loc_fu_156 ),
    .din28( shift_reg_V_1_6_3_loc_fu_152 ),
    .din29( shift_reg_V_1_7_0_loc_fu_148 ),
    .din30( shift_reg_V_1_7_1_loc_fu_144 ),
    .din31( shift_reg_V_1_7_2_loc_fu_140 ),
    .din32( shift_reg_V_1_7_3_loc_fu_136 ),
    .din33( tmp_3_fu_768_p33 ),
    .dout( tmp_3_fu_768_p34 )
);



always @ (posedge ap_clk) begin : ap_ret_ap_CS_fsm
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_st1_fsm_0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_reg_ppiten_pp0_it0
    if (ap_rst == 1'b1) begin
        ap_reg_ppiten_pp0_it0 <= ap_const_logic_0;
    end else begin
        if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & ~(tmp_fu_700_p2 == ap_const_lv1_0))) begin
            ap_reg_ppiten_pp0_it0 <= ap_const_logic_0;
        end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
            ap_reg_ppiten_pp0_it0 <= ap_const_logic_1;
        end
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_reg_ppiten_pp0_it1
    if (ap_rst == 1'b1) begin
        ap_reg_ppiten_pp0_it1 <= ap_const_logic_0;
    end else begin
        if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (tmp_fu_700_p2 == ap_const_lv1_0))) begin
            ap_reg_ppiten_pp0_it1 <= ap_const_logic_1;
        end else if ((((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0)) | ((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & ~(tmp_fu_700_p2 == ap_const_lv1_0)))) begin
            ap_reg_ppiten_pp0_it1 <= ap_const_logic_0;
        end
    end
end

always @ (posedge ap_clk) begin : ap_ret_ap_reg_ppiten_pp0_it2
    if (ap_rst == 1'b1) begin
        ap_reg_ppiten_pp0_it2 <= ap_const_logic_0;
    end else begin
        ap_reg_ppiten_pp0_it2 <= ap_reg_ppiten_pp0_it1;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it2) & (ap_reg_ppstg_tmp_reg_1438_pp0_it1 == ap_const_lv1_0))) begin
        p_Val2_s_reg_296 <= acc_V_fu_1006_p2;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        p_Val2_s_reg_296 <= ap_const_lv19_0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & (tmp_fu_700_p2 == ap_const_lv1_0))) begin
        p_s_reg_308 <= i_V_fu_749_p2;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        p_s_reg_308 <= ap_const_lv5_1F;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_0) & (newIndex15_t_reg_1456 == ap_const_lv2_0))) begin
        shift_reg_V_1_0_0_loc_fu_260 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_0_0_loc_fu_260 <= shift_reg_V_1_0_0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it2) & (ap_reg_ppstg_tmp_reg_1438_pp0_it1 == ap_const_lv1_0))) begin
        shift_reg_V_1_0_1_flag_reg_282 <= ap_const_lv1_1;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_0_1_flag_reg_282 <= ap_const_lv1_0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_0) & (newIndex15_t_reg_1456 == ap_const_lv2_1))) begin
        shift_reg_V_1_0_1_loc_fu_256 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_0_1_loc_fu_256 <= shift_reg_V_1_0_1;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_0) & (newIndex15_t_reg_1456 == ap_const_lv2_2))) begin
        shift_reg_V_1_0_2_loc_fu_252 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_0_2_loc_fu_252 <= shift_reg_V_1_0_2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_0) & ~(newIndex15_t_reg_1456 == ap_const_lv2_2) & ~(newIndex15_t_reg_1456 == ap_const_lv2_1) & ~(newIndex15_t_reg_1456 == ap_const_lv2_0))) begin
        shift_reg_V_1_0_3_loc_fu_248 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_0_3_loc_fu_248 <= shift_reg_V_1_0_3;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_1) & (newIndex15_t_reg_1456 == ap_const_lv2_0))) begin
        shift_reg_V_1_1_0_loc_fu_244 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_1_0_loc_fu_244 <= shift_reg_V_1_1_0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_1) & (newIndex15_t_reg_1456 == ap_const_lv2_1))) begin
        shift_reg_V_1_1_1_loc_fu_240 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_1_1_loc_fu_240 <= shift_reg_V_1_1_1;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_1) & (newIndex15_t_reg_1456 == ap_const_lv2_2))) begin
        shift_reg_V_1_1_2_loc_fu_236 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_1_2_loc_fu_236 <= shift_reg_V_1_1_2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_1) & ~(newIndex15_t_reg_1456 == ap_const_lv2_2) & ~(newIndex15_t_reg_1456 == ap_const_lv2_1) & ~(newIndex15_t_reg_1456 == ap_const_lv2_0))) begin
        shift_reg_V_1_1_3_loc_fu_232 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_1_3_loc_fu_232 <= shift_reg_V_1_1_3;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_2) & (newIndex15_t_reg_1456 == ap_const_lv2_0))) begin
        shift_reg_V_1_2_0_loc_fu_228 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_2_0_loc_fu_228 <= shift_reg_V_1_2_0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_2) & (newIndex15_t_reg_1456 == ap_const_lv2_1))) begin
        shift_reg_V_1_2_1_loc_fu_224 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_2_1_loc_fu_224 <= shift_reg_V_1_2_1;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_2) & (newIndex15_t_reg_1456 == ap_const_lv2_2))) begin
        shift_reg_V_1_2_2_loc_fu_220 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_2_2_loc_fu_220 <= shift_reg_V_1_2_2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_2) & ~(newIndex15_t_reg_1456 == ap_const_lv2_2) & ~(newIndex15_t_reg_1456 == ap_const_lv2_1) & ~(newIndex15_t_reg_1456 == ap_const_lv2_0))) begin
        shift_reg_V_1_2_3_loc_fu_216 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_2_3_loc_fu_216 <= shift_reg_V_1_2_3;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_3) & (newIndex15_t_reg_1456 == ap_const_lv2_0))) begin
        shift_reg_V_1_3_0_loc_fu_212 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_3_0_loc_fu_212 <= shift_reg_V_1_3_0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_3) & (newIndex15_t_reg_1456 == ap_const_lv2_1))) begin
        shift_reg_V_1_3_1_loc_fu_208 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_3_1_loc_fu_208 <= shift_reg_V_1_3_1;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_3) & (newIndex15_t_reg_1456 == ap_const_lv2_2))) begin
        shift_reg_V_1_3_2_loc_fu_204 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_3_2_loc_fu_204 <= shift_reg_V_1_3_2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_3) & ~(newIndex15_t_reg_1456 == ap_const_lv2_2) & ~(newIndex15_t_reg_1456 == ap_const_lv2_1) & ~(newIndex15_t_reg_1456 == ap_const_lv2_0))) begin
        shift_reg_V_1_3_3_loc_fu_200 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_3_3_loc_fu_200 <= shift_reg_V_1_3_3;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_4) & (newIndex15_t_reg_1456 == ap_const_lv2_0))) begin
        shift_reg_V_1_4_0_loc_fu_196 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_4_0_loc_fu_196 <= shift_reg_V_1_4_0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_4) & (newIndex15_t_reg_1456 == ap_const_lv2_1))) begin
        shift_reg_V_1_4_1_loc_fu_192 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_4_1_loc_fu_192 <= shift_reg_V_1_4_1;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_4) & (newIndex15_t_reg_1456 == ap_const_lv2_2))) begin
        shift_reg_V_1_4_2_loc_fu_188 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_4_2_loc_fu_188 <= shift_reg_V_1_4_2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_4) & ~(newIndex15_t_reg_1456 == ap_const_lv2_2) & ~(newIndex15_t_reg_1456 == ap_const_lv2_1) & ~(newIndex15_t_reg_1456 == ap_const_lv2_0))) begin
        shift_reg_V_1_4_3_loc_fu_184 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_4_3_loc_fu_184 <= shift_reg_V_1_4_3;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_5) & (newIndex15_t_reg_1456 == ap_const_lv2_0))) begin
        shift_reg_V_1_5_0_loc_fu_180 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_5_0_loc_fu_180 <= shift_reg_V_1_5_0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_5) & (newIndex15_t_reg_1456 == ap_const_lv2_1))) begin
        shift_reg_V_1_5_1_loc_fu_176 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_5_1_loc_fu_176 <= shift_reg_V_1_5_1;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_5) & (newIndex15_t_reg_1456 == ap_const_lv2_2))) begin
        shift_reg_V_1_5_2_loc_fu_172 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_5_2_loc_fu_172 <= shift_reg_V_1_5_2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_5) & ~(newIndex15_t_reg_1456 == ap_const_lv2_2) & ~(newIndex15_t_reg_1456 == ap_const_lv2_1) & ~(newIndex15_t_reg_1456 == ap_const_lv2_0))) begin
        shift_reg_V_1_5_3_loc_fu_168 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_5_3_loc_fu_168 <= shift_reg_V_1_5_3;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_6) & (newIndex15_t_reg_1456 == ap_const_lv2_0))) begin
        shift_reg_V_1_6_0_loc_fu_164 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_6_0_loc_fu_164 <= shift_reg_V_1_6_0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_6) & (newIndex15_t_reg_1456 == ap_const_lv2_1))) begin
        shift_reg_V_1_6_1_loc_fu_160 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_6_1_loc_fu_160 <= shift_reg_V_1_6_1;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_6) & (newIndex15_t_reg_1456 == ap_const_lv2_2))) begin
        shift_reg_V_1_6_2_loc_fu_156 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_6_2_loc_fu_156 <= shift_reg_V_1_6_2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & (tmp_6_reg_1452 == ap_const_lv3_6) & ~(newIndex15_t_reg_1456 == ap_const_lv2_2) & ~(newIndex15_t_reg_1456 == ap_const_lv2_1) & ~(newIndex15_t_reg_1456 == ap_const_lv2_0))) begin
        shift_reg_V_1_6_3_loc_fu_152 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_6_3_loc_fu_152 <= shift_reg_V_1_6_3;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & ~(tmp_6_reg_1452 == ap_const_lv3_6) & ~(tmp_6_reg_1452 == ap_const_lv3_5) & ~(tmp_6_reg_1452 == ap_const_lv3_4) & ~(tmp_6_reg_1452 == ap_const_lv3_3) & ~(tmp_6_reg_1452 == ap_const_lv3_2) & ~(tmp_6_reg_1452 == ap_const_lv3_1) & ~(tmp_6_reg_1452 == ap_const_lv3_0) & (newIndex15_t_reg_1456 == ap_const_lv2_0))) begin
        shift_reg_V_1_7_0_loc_fu_148 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_7_0_loc_fu_148 <= shift_reg_V_1_7_0;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & ~(tmp_6_reg_1452 == ap_const_lv3_6) & ~(tmp_6_reg_1452 == ap_const_lv3_5) & ~(tmp_6_reg_1452 == ap_const_lv3_4) & ~(tmp_6_reg_1452 == ap_const_lv3_3) & ~(tmp_6_reg_1452 == ap_const_lv3_2) & ~(tmp_6_reg_1452 == ap_const_lv3_1) & ~(tmp_6_reg_1452 == ap_const_lv3_0) & (newIndex15_t_reg_1456 == ap_const_lv2_1))) begin
        shift_reg_V_1_7_1_loc_fu_144 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_7_1_loc_fu_144 <= shift_reg_V_1_7_1;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & ~(tmp_6_reg_1452 == ap_const_lv3_6) & ~(tmp_6_reg_1452 == ap_const_lv3_5) & ~(tmp_6_reg_1452 == ap_const_lv3_4) & ~(tmp_6_reg_1452 == ap_const_lv3_3) & ~(tmp_6_reg_1452 == ap_const_lv3_2) & ~(tmp_6_reg_1452 == ap_const_lv3_1) & ~(tmp_6_reg_1452 == ap_const_lv3_0) & (newIndex15_t_reg_1456 == ap_const_lv2_2))) begin
        shift_reg_V_1_7_2_loc_fu_140 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_7_2_loc_fu_140 <= shift_reg_V_1_7_2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it1) & ~(tmp_6_reg_1452 == ap_const_lv3_6) & ~(tmp_6_reg_1452 == ap_const_lv3_5) & ~(tmp_6_reg_1452 == ap_const_lv3_4) & ~(tmp_6_reg_1452 == ap_const_lv3_3) & ~(tmp_6_reg_1452 == ap_const_lv3_2) & ~(tmp_6_reg_1452 == ap_const_lv3_1) & ~(tmp_6_reg_1452 == ap_const_lv3_0) & ~(newIndex15_t_reg_1456 == ap_const_lv2_2) & ~(newIndex15_t_reg_1456 == ap_const_lv2_1) & ~(newIndex15_t_reg_1456 == ap_const_lv2_0))) begin
        shift_reg_V_1_7_3_loc_fu_136 <= tmp_3_fu_768_p34;
    end else if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
        shift_reg_V_1_7_3_loc_fu_136 <= shift_reg_V_1_7_3;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1)) begin
        ap_reg_ppstg_tmp_reg_1438_pp0_it1 <= tmp_reg_1438;
        tmp_reg_1438 <= tmp_fu_700_p2;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (tmp_reg_1438 == ap_const_lv1_0))) begin
        f_op_V_reg_1475 <= c2_q0;
        tmp_3_reg_1470 <= tmp_3_fu_768_p34;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (tmp_fu_700_p2 == ap_const_lv1_0))) begin
        newIndex15_t_reg_1456 <= {{p_s_reg_308[ap_const_lv32_4 : ap_const_lv32_3]}};
        newIndex_t_reg_1447 <= {{r_V_fu_715_p2[ap_const_lv32_4 : ap_const_lv32_3]}};
        tmp_5_reg_1442 <= tmp_5_fu_721_p1;
        tmp_6_reg_1452 <= tmp_6_fu_735_p1;
    end
end

always @ (posedge ap_clk) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_2)) begin
        shift_reg_V_1_0_0 <= x_V;
    end
end

always @ (posedge ap_clk) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_2) & ~(shift_reg_V_1_0_1_flag_reg_282 == ap_const_lv1_0))) begin
        shift_reg_V_1_0_1 <= shift_reg_V_1_0_1_loc_fu_256;
        shift_reg_V_1_0_2 <= shift_reg_V_1_0_2_loc_fu_252;
        shift_reg_V_1_0_3 <= shift_reg_V_1_0_3_loc_fu_248;
        shift_reg_V_1_1_0 <= shift_reg_V_1_1_0_loc_fu_244;
        shift_reg_V_1_1_1 <= shift_reg_V_1_1_1_loc_fu_240;
        shift_reg_V_1_1_2 <= shift_reg_V_1_1_2_loc_fu_236;
        shift_reg_V_1_1_3 <= shift_reg_V_1_1_3_loc_fu_232;
        shift_reg_V_1_2_0 <= shift_reg_V_1_2_0_loc_fu_228;
        shift_reg_V_1_2_1 <= shift_reg_V_1_2_1_loc_fu_224;
        shift_reg_V_1_2_2 <= shift_reg_V_1_2_2_loc_fu_220;
        shift_reg_V_1_2_3 <= shift_reg_V_1_2_3_loc_fu_216;
        shift_reg_V_1_3_0 <= shift_reg_V_1_3_0_loc_fu_212;
        shift_reg_V_1_3_1 <= shift_reg_V_1_3_1_loc_fu_208;
        shift_reg_V_1_3_2 <= shift_reg_V_1_3_2_loc_fu_204;
        shift_reg_V_1_3_3 <= shift_reg_V_1_3_3_loc_fu_200;
        shift_reg_V_1_4_0 <= shift_reg_V_1_4_0_loc_fu_196;
        shift_reg_V_1_4_1 <= shift_reg_V_1_4_1_loc_fu_192;
        shift_reg_V_1_4_2 <= shift_reg_V_1_4_2_loc_fu_188;
        shift_reg_V_1_4_3 <= shift_reg_V_1_4_3_loc_fu_184;
        shift_reg_V_1_5_0 <= shift_reg_V_1_5_0_loc_fu_180;
        shift_reg_V_1_5_1 <= shift_reg_V_1_5_1_loc_fu_176;
        shift_reg_V_1_5_2 <= shift_reg_V_1_5_2_loc_fu_172;
        shift_reg_V_1_5_3 <= shift_reg_V_1_5_3_loc_fu_168;
        shift_reg_V_1_6_0 <= shift_reg_V_1_6_0_loc_fu_164;
        shift_reg_V_1_6_1 <= shift_reg_V_1_6_1_loc_fu_160;
        shift_reg_V_1_6_2 <= shift_reg_V_1_6_2_loc_fu_156;
        shift_reg_V_1_6_3 <= shift_reg_V_1_6_3_loc_fu_152;
        shift_reg_V_1_7_0 <= shift_reg_V_1_7_0_loc_fu_148;
        shift_reg_V_1_7_1 <= shift_reg_V_1_7_1_loc_fu_144;
        shift_reg_V_1_7_2 <= shift_reg_V_1_7_2_loc_fu_140;
        shift_reg_V_1_7_3 <= shift_reg_V_1_7_3_loc_fu_136;
    end
end

always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0 or ap_sig_cseq_ST_st5_fsm_2) begin
    if (((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0)) | (ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_2))) begin
        ap_done = ap_const_logic_1;
    end else begin
        ap_done = ap_const_logic_0;
    end
end

always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if ((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0))) begin
        ap_idle = ap_const_logic_1;
    end else begin
        ap_idle = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_st5_fsm_2) begin
    if ((ap_const_logic_1 == ap_sig_cseq_ST_st5_fsm_2)) begin
        ap_ready = ap_const_logic_1;
    end else begin
        ap_ready = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_86) begin
    if (ap_sig_bdd_86) begin
        ap_sig_cseq_ST_pp0_stg0_fsm_1 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_pp0_stg0_fsm_1 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_21) begin
    if (ap_sig_bdd_21) begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_135) begin
    if (ap_sig_bdd_135) begin
        ap_sig_cseq_ST_st5_fsm_2 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st5_fsm_2 = ap_const_logic_0;
    end
end

always @ (ap_sig_cseq_ST_pp0_stg0_fsm_1 or ap_reg_ppiten_pp0_it0) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_pp0_stg0_fsm_1) & (ap_const_logic_1 == ap_reg_ppiten_pp0_it0))) begin
        c2_ce0 = ap_const_logic_1;
    end else begin
        c2_ce0 = ap_const_logic_0;
    end
end
always @ (ap_start or ap_CS_fsm or tmp_fu_700_p2 or ap_reg_ppiten_pp0_it0 or ap_reg_ppiten_pp0_it1 or ap_reg_ppiten_pp0_it2) begin
    case (ap_CS_fsm)
        ap_ST_st1_fsm_0 : 
        begin
            if (~(ap_start == ap_const_logic_0)) begin
                ap_NS_fsm = ap_ST_pp0_stg0_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end
        end
        ap_ST_pp0_stg0_fsm_1 : 
        begin
            if ((~((ap_const_logic_1 == ap_reg_ppiten_pp0_it2) & ~(ap_const_logic_1 == ap_reg_ppiten_pp0_it1)) & ~((ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & ~(tmp_fu_700_p2 == ap_const_lv1_0) & ~(ap_const_logic_1 == ap_reg_ppiten_pp0_it1)))) begin
                ap_NS_fsm = ap_ST_pp0_stg0_fsm_1;
            end else if (((ap_const_logic_1 == ap_reg_ppiten_pp0_it0) & ~(tmp_fu_700_p2 == ap_const_lv1_0) & ~(ap_const_logic_1 == ap_reg_ppiten_pp0_it1))) begin
                ap_NS_fsm = ap_ST_st5_fsm_2;
            end else begin
                ap_NS_fsm = ap_ST_st5_fsm_2;
            end
        end
        ap_ST_st5_fsm_2 : 
        begin
            ap_NS_fsm = ap_ST_st1_fsm_0;
        end
        default : 
        begin
            ap_NS_fsm = 'bx;
        end
    endcase
end


assign acc_V_fu_1006_p2 = (p_Val2_9_fu_1001_p2 + p_Val2_s_reg_296);

assign ap_return = (p_Val2_s_reg_296 - x_V);


always @ (ap_CS_fsm) begin
    ap_sig_bdd_135 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_2]);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_21 = (ap_CS_fsm[ap_const_lv32_0] == ap_const_lv1_1);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_86 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_1]);
end

assign c2_address0 = tmp_8_fu_706_p1;

assign i_V_fu_749_p2 = ($signed(p_s_reg_308) + $signed(ap_const_lv5_1F));

assign lhs_V_cast_fu_711_p1 = p_s_reg_308;

assign p_Val2_9_fu_1001_p1 = f_op_V_reg_1475;

assign p_Val2_9_fu_1001_p2 = ($signed(tmp_3_reg_1470) * $signed(p_Val2_9_fu_1001_p1));

assign r_V_fu_715_p2 = ($signed(ap_const_lv6_3F) + $signed(lhs_V_cast_fu_711_p1));

assign tmp_1_fu_758_p3 = {{tmp_5_reg_1442}, {newIndex_t_reg_1447}};

assign tmp_3_fu_768_p33 = tmp_1_fu_758_p3;

assign tmp_5_fu_721_p1 = r_V_fu_715_p2[2:0];

assign tmp_6_fu_735_p1 = p_s_reg_308[2:0];

assign tmp_8_fu_706_p1 = p_s_reg_308;

assign tmp_fu_700_p2 = (p_s_reg_308 == ap_const_lv5_0? 1'b1: 1'b0);


endmodule //fir_firQ1

