// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC
// Version: 2015.4
// Copyright (C) 2015 Xilinx Inc. All rights reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

(* CORE_GENERATION_INFO="fir,hls_ip_2015_4,{HLS_INPUT_TYPE=cxx,HLS_INPUT_FLOAT=0,HLS_INPUT_FIXED=1,HLS_INPUT_PART=xc7z020clg484-1,HLS_INPUT_CLOCK=10.000000,HLS_INPUT_ARCH=others,HLS_SYN_CLOCK=8.460000,HLS_SYN_LAT=35,HLS_SYN_TPT=none,HLS_SYN_MEM=0,HLS_SYN_DSP=4,HLS_SYN_FF=5134,HLS_SYN_LUT=3467}" *)

module fir (
        ap_clk,
        ap_rst,
        ap_start,
        ap_done,
        ap_idle,
        ap_ready,
        I_V,
        Q_V,
        X_V,
        X_V_ap_vld,
        Y_V,
        Y_V_ap_vld
);

parameter    ap_const_logic_1 = 1'b1;
parameter    ap_const_logic_0 = 1'b0;
parameter    ap_ST_st1_fsm_0 = 2'b1;
parameter    ap_ST_st2_fsm_1 = 2'b10;
parameter    ap_const_lv32_0 = 32'b00000000000000000000000000000000;
parameter    ap_const_lv1_1 = 1'b1;
parameter    ap_const_lv32_1 = 32'b1;
parameter    ap_true = 1'b1;

input   ap_clk;
input   ap_rst;
input   ap_start;
output   ap_done;
output   ap_idle;
output   ap_ready;
input  [18:0] I_V;
input  [18:0] Q_V;
output  [18:0] X_V;
output   X_V_ap_vld;
output  [18:0] Y_V;
output   Y_V_ap_vld;

reg ap_done;
reg ap_idle;
reg ap_ready;
reg X_V_ap_vld;
reg Y_V_ap_vld;
(* fsm_encoding = "none" *) reg   [1:0] ap_CS_fsm = 2'b1;
reg    ap_sig_cseq_ST_st1_fsm_0;
reg    ap_sig_bdd_18;
wire    grp_fir_firI1_fu_316_ap_start;
wire    grp_fir_firI1_fu_316_ap_done;
wire    grp_fir_firI1_fu_316_ap_idle;
wire    grp_fir_firI1_fu_316_ap_ready;
wire   [18:0] grp_fir_firI1_fu_316_x_V;
wire   [18:0] grp_fir_firI1_fu_316_ap_return;
wire    grp_fir_firQ1_fu_388_ap_start;
wire    grp_fir_firQ1_fu_388_ap_done;
wire    grp_fir_firQ1_fu_388_ap_idle;
wire    grp_fir_firQ1_fu_388_ap_ready;
wire   [18:0] grp_fir_firQ1_fu_388_x_V;
wire   [18:0] grp_fir_firQ1_fu_388_ap_return;
wire    grp_fir_firI2_fu_460_ap_start;
wire    grp_fir_firI2_fu_460_ap_done;
wire    grp_fir_firI2_fu_460_ap_idle;
wire    grp_fir_firI2_fu_460_ap_ready;
wire   [18:0] grp_fir_firI2_fu_460_x_V;
wire   [18:0] grp_fir_firI2_fu_460_ap_return;
wire    grp_fir_firQ2_fu_532_ap_start;
wire    grp_fir_firQ2_fu_532_ap_done;
wire    grp_fir_firQ2_fu_532_ap_idle;
wire    grp_fir_firQ2_fu_532_ap_ready;
wire   [18:0] grp_fir_firQ2_fu_532_x_V;
wire   [18:0] grp_fir_firQ2_fu_532_ap_return;
reg    grp_fir_firI1_fu_316_ap_start_ap_start_reg = 1'b0;
reg    ap_sig_cseq_ST_st2_fsm_1;
reg    ap_sig_bdd_77;
reg    grp_fir_firQ1_fu_388_ap_start_ap_start_reg = 1'b0;
reg    grp_fir_firI2_fu_460_ap_start_ap_start_reg = 1'b0;
reg    grp_fir_firQ2_fu_532_ap_start_ap_start_reg = 1'b0;
reg   [1:0] ap_NS_fsm;


fir_firI1 grp_fir_firI1_fu_316(
    .ap_clk( ap_clk ),
    .ap_rst( ap_rst ),
    .ap_start( grp_fir_firI1_fu_316_ap_start ),
    .ap_done( grp_fir_firI1_fu_316_ap_done ),
    .ap_idle( grp_fir_firI1_fu_316_ap_idle ),
    .ap_ready( grp_fir_firI1_fu_316_ap_ready ),
    .x_V( grp_fir_firI1_fu_316_x_V ),
    .ap_return( grp_fir_firI1_fu_316_ap_return )
);

fir_firQ1 grp_fir_firQ1_fu_388(
    .ap_clk( ap_clk ),
    .ap_rst( ap_rst ),
    .ap_start( grp_fir_firQ1_fu_388_ap_start ),
    .ap_done( grp_fir_firQ1_fu_388_ap_done ),
    .ap_idle( grp_fir_firQ1_fu_388_ap_idle ),
    .ap_ready( grp_fir_firQ1_fu_388_ap_ready ),
    .x_V( grp_fir_firQ1_fu_388_x_V ),
    .ap_return( grp_fir_firQ1_fu_388_ap_return )
);

fir_firI2 grp_fir_firI2_fu_460(
    .ap_clk( ap_clk ),
    .ap_rst( ap_rst ),
    .ap_start( grp_fir_firI2_fu_460_ap_start ),
    .ap_done( grp_fir_firI2_fu_460_ap_done ),
    .ap_idle( grp_fir_firI2_fu_460_ap_idle ),
    .ap_ready( grp_fir_firI2_fu_460_ap_ready ),
    .x_V( grp_fir_firI2_fu_460_x_V ),
    .ap_return( grp_fir_firI2_fu_460_ap_return )
);

fir_firQ2 grp_fir_firQ2_fu_532(
    .ap_clk( ap_clk ),
    .ap_rst( ap_rst ),
    .ap_start( grp_fir_firQ2_fu_532_ap_start ),
    .ap_done( grp_fir_firQ2_fu_532_ap_done ),
    .ap_idle( grp_fir_firQ2_fu_532_ap_idle ),
    .ap_ready( grp_fir_firQ2_fu_532_ap_ready ),
    .x_V( grp_fir_firQ2_fu_532_x_V ),
    .ap_return( grp_fir_firQ2_fu_532_ap_return )
);



always @ (posedge ap_clk) begin : ap_ret_ap_CS_fsm
    if (ap_rst == 1'b1) begin
        ap_CS_fsm <= ap_ST_st1_fsm_0;
    end else begin
        ap_CS_fsm <= ap_NS_fsm;
    end
end

always @ (posedge ap_clk) begin : ap_ret_grp_fir_firI1_fu_316_ap_start_ap_start_reg
    if (ap_rst == 1'b1) begin
        grp_fir_firI1_fu_316_ap_start_ap_start_reg <= ap_const_logic_0;
    end else begin
        if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
            grp_fir_firI1_fu_316_ap_start_ap_start_reg <= ap_const_logic_1;
        end else if ((ap_const_logic_1 == grp_fir_firI1_fu_316_ap_ready)) begin
            grp_fir_firI1_fu_316_ap_start_ap_start_reg <= ap_const_logic_0;
        end
    end
end

always @ (posedge ap_clk) begin : ap_ret_grp_fir_firI2_fu_460_ap_start_ap_start_reg
    if (ap_rst == 1'b1) begin
        grp_fir_firI2_fu_460_ap_start_ap_start_reg <= ap_const_logic_0;
    end else begin
        if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
            grp_fir_firI2_fu_460_ap_start_ap_start_reg <= ap_const_logic_1;
        end else if ((ap_const_logic_1 == grp_fir_firI2_fu_460_ap_ready)) begin
            grp_fir_firI2_fu_460_ap_start_ap_start_reg <= ap_const_logic_0;
        end
    end
end

always @ (posedge ap_clk) begin : ap_ret_grp_fir_firQ1_fu_388_ap_start_ap_start_reg
    if (ap_rst == 1'b1) begin
        grp_fir_firQ1_fu_388_ap_start_ap_start_reg <= ap_const_logic_0;
    end else begin
        if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
            grp_fir_firQ1_fu_388_ap_start_ap_start_reg <= ap_const_logic_1;
        end else if ((ap_const_logic_1 == grp_fir_firQ1_fu_388_ap_ready)) begin
            grp_fir_firQ1_fu_388_ap_start_ap_start_reg <= ap_const_logic_0;
        end
    end
end

always @ (posedge ap_clk) begin : ap_ret_grp_fir_firQ2_fu_532_ap_start_ap_start_reg
    if (ap_rst == 1'b1) begin
        grp_fir_firQ2_fu_532_ap_start_ap_start_reg <= ap_const_logic_0;
    end else begin
        if (((ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0) & ~(ap_start == ap_const_logic_0))) begin
            grp_fir_firQ2_fu_532_ap_start_ap_start_reg <= ap_const_logic_1;
        end else if ((ap_const_logic_1 == grp_fir_firQ2_fu_532_ap_ready)) begin
            grp_fir_firQ2_fu_532_ap_start_ap_start_reg <= ap_const_logic_0;
        end
    end
end

always @ (grp_fir_firI1_fu_316_ap_done or grp_fir_firQ1_fu_388_ap_done or grp_fir_firI2_fu_460_ap_done or grp_fir_firQ2_fu_532_ap_done or ap_sig_cseq_ST_st2_fsm_1) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~((ap_const_logic_0 == grp_fir_firI1_fu_316_ap_done) | (ap_const_logic_0 == grp_fir_firQ1_fu_388_ap_done) | (ap_const_logic_0 == grp_fir_firI2_fu_460_ap_done) | (ap_const_logic_0 == grp_fir_firQ2_fu_532_ap_done)))) begin
        X_V_ap_vld = ap_const_logic_1;
    end else begin
        X_V_ap_vld = ap_const_logic_0;
    end
end

always @ (grp_fir_firI1_fu_316_ap_done or grp_fir_firQ1_fu_388_ap_done or grp_fir_firI2_fu_460_ap_done or grp_fir_firQ2_fu_532_ap_done or ap_sig_cseq_ST_st2_fsm_1) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~((ap_const_logic_0 == grp_fir_firI1_fu_316_ap_done) | (ap_const_logic_0 == grp_fir_firQ1_fu_388_ap_done) | (ap_const_logic_0 == grp_fir_firI2_fu_460_ap_done) | (ap_const_logic_0 == grp_fir_firQ2_fu_532_ap_done)))) begin
        Y_V_ap_vld = ap_const_logic_1;
    end else begin
        Y_V_ap_vld = ap_const_logic_0;
    end
end

always @ (grp_fir_firI1_fu_316_ap_done or grp_fir_firQ1_fu_388_ap_done or grp_fir_firI2_fu_460_ap_done or grp_fir_firQ2_fu_532_ap_done or ap_sig_cseq_ST_st2_fsm_1) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~((ap_const_logic_0 == grp_fir_firI1_fu_316_ap_done) | (ap_const_logic_0 == grp_fir_firQ1_fu_388_ap_done) | (ap_const_logic_0 == grp_fir_firI2_fu_460_ap_done) | (ap_const_logic_0 == grp_fir_firQ2_fu_532_ap_done)))) begin
        ap_done = ap_const_logic_1;
    end else begin
        ap_done = ap_const_logic_0;
    end
end

always @ (ap_start or ap_sig_cseq_ST_st1_fsm_0) begin
    if ((~(ap_const_logic_1 == ap_start) & (ap_const_logic_1 == ap_sig_cseq_ST_st1_fsm_0))) begin
        ap_idle = ap_const_logic_1;
    end else begin
        ap_idle = ap_const_logic_0;
    end
end

always @ (grp_fir_firI1_fu_316_ap_done or grp_fir_firQ1_fu_388_ap_done or grp_fir_firI2_fu_460_ap_done or grp_fir_firQ2_fu_532_ap_done or ap_sig_cseq_ST_st2_fsm_1) begin
    if (((ap_const_logic_1 == ap_sig_cseq_ST_st2_fsm_1) & ~((ap_const_logic_0 == grp_fir_firI1_fu_316_ap_done) | (ap_const_logic_0 == grp_fir_firQ1_fu_388_ap_done) | (ap_const_logic_0 == grp_fir_firI2_fu_460_ap_done) | (ap_const_logic_0 == grp_fir_firQ2_fu_532_ap_done)))) begin
        ap_ready = ap_const_logic_1;
    end else begin
        ap_ready = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_18) begin
    if (ap_sig_bdd_18) begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st1_fsm_0 = ap_const_logic_0;
    end
end

always @ (ap_sig_bdd_77) begin
    if (ap_sig_bdd_77) begin
        ap_sig_cseq_ST_st2_fsm_1 = ap_const_logic_1;
    end else begin
        ap_sig_cseq_ST_st2_fsm_1 = ap_const_logic_0;
    end
end
always @ (ap_start or ap_CS_fsm or grp_fir_firI1_fu_316_ap_done or grp_fir_firQ1_fu_388_ap_done or grp_fir_firI2_fu_460_ap_done or grp_fir_firQ2_fu_532_ap_done) begin
    case (ap_CS_fsm)
        ap_ST_st1_fsm_0 : 
        begin
            if (~(ap_start == ap_const_logic_0)) begin
                ap_NS_fsm = ap_ST_st2_fsm_1;
            end else begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end
        end
        ap_ST_st2_fsm_1 : 
        begin
            if (~((ap_const_logic_0 == grp_fir_firI1_fu_316_ap_done) | (ap_const_logic_0 == grp_fir_firQ1_fu_388_ap_done) | (ap_const_logic_0 == grp_fir_firI2_fu_460_ap_done) | (ap_const_logic_0 == grp_fir_firQ2_fu_532_ap_done))) begin
                ap_NS_fsm = ap_ST_st1_fsm_0;
            end else begin
                ap_NS_fsm = ap_ST_st2_fsm_1;
            end
        end
        default : 
        begin
            ap_NS_fsm = 'bx;
        end
    endcase
end


assign X_V = (grp_fir_firQ1_fu_388_ap_return + grp_fir_firI1_fu_316_ap_return);

assign Y_V = (grp_fir_firI2_fu_460_ap_return - grp_fir_firQ2_fu_532_ap_return);


always @ (ap_CS_fsm) begin
    ap_sig_bdd_18 = (ap_CS_fsm[ap_const_lv32_0] == ap_const_lv1_1);
end


always @ (ap_CS_fsm) begin
    ap_sig_bdd_77 = (ap_const_lv1_1 == ap_CS_fsm[ap_const_lv32_1]);
end

assign grp_fir_firI1_fu_316_ap_start = grp_fir_firI1_fu_316_ap_start_ap_start_reg;

assign grp_fir_firI1_fu_316_x_V = I_V;

assign grp_fir_firI2_fu_460_ap_start = grp_fir_firI2_fu_460_ap_start_ap_start_reg;

assign grp_fir_firI2_fu_460_x_V = Q_V;

assign grp_fir_firQ1_fu_388_ap_start = grp_fir_firQ1_fu_388_ap_start_ap_start_reg;

assign grp_fir_firQ1_fu_388_x_V = Q_V;

assign grp_fir_firQ2_fu_532_ap_start = grp_fir_firQ2_fu_532_ap_start_ap_start_reg;

assign grp_fir_firQ2_fu_532_x_V = I_V;


endmodule //fir

