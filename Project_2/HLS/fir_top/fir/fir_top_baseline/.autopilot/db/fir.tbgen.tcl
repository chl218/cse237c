set C_TypeInfoList {{ 
"fir" : [[], { "return": [[], "void"]} , [{"ExternC" : 0}], [ {"I": [[],"0"] }, {"Q": [[],"0"] }, {"X": [[],{ "pointer": "0"}] }, {"Y": [[],{ "pointer": "0"}] }],[],""], 
"0": [ "data_t", {"typedef": [[[],"1"],""]}], 
"1": [ "ap_fixed<19, 7, 5, 3, 0>", {"hls_type": {"ap_fixed": [[[[], {"scalar": { "int": 19}}],[[], {"scalar": { "int": 7}}],[[], {"scalar": { "2": 5}}],[[], {"scalar": { "3": 3}}],[[], {"scalar": { "int": 0}}]],""]}}], 
"3": [ "ap_o_mode", {"enum": [[],[],[{"SC_SAT":  {"scalar": "__integer__"}},{"SC_SAT_ZERO":  {"scalar": "__integer__"}},{"SC_SAT_SYM":  {"scalar": "__integer__"}},{"SC_WRAP":  {"scalar": "__integer__"}},{"SC_WRAP_SM":  {"scalar": "__integer__"}}],""]}], 
"2": [ "ap_q_mode", {"enum": [[],[],[{"SC_RND":  {"scalar": "__integer__"}},{"SC_RND_ZERO":  {"scalar": "__integer__"}},{"SC_RND_MIN_INF":  {"scalar": "__integer__"}},{"SC_RND_INF":  {"scalar": "__integer__"}},{"SC_RND_CONV":  {"scalar": "__integer__"}},{"SC_TRN":  {"scalar": "__integer__"}},{"SC_TRN_ZERO":  {"scalar": "__integer__"}}],""]}]
}}
set moduleName fir
set isCombinational 0
set isDatapathOnly 0
set isPipelined 0
set pipeline_type none
set FunctionProtocol ap_ctrl_hs
set isOneStateSeq 0
set C_modelName {fir}
set C_modelType { void 0 }
set C_modelArgList { 
	{ I_V int 19 regular  }
	{ Q_V int 19 regular  }
	{ X_V int 19 regular {pointer 1}  }
	{ Y_V int 19 regular {pointer 1}  }
}
set C_modelArgMapList {[ 
	{ "Name" : "I_V", "interface" : "wire", "bitwidth" : 19, "direction" : "READONLY", "bitSlice":[{"low":0,"up":18,"cElement": [{"cName": "I.V","cData": "int19","bit_use": { "low": 0,"up": 18},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "Q_V", "interface" : "wire", "bitwidth" : 19, "direction" : "READONLY", "bitSlice":[{"low":0,"up":18,"cElement": [{"cName": "Q.V","cData": "int19","bit_use": { "low": 0,"up": 18},"cArray": [{"low" : 0,"up" : 0,"step" : 0}]}]}]} , 
 	{ "Name" : "X_V", "interface" : "wire", "bitwidth" : 19, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":18,"cElement": [{"cName": "X.V","cData": "int19","bit_use": { "low": 0,"up": 18},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} , 
 	{ "Name" : "Y_V", "interface" : "wire", "bitwidth" : 19, "direction" : "WRITEONLY", "bitSlice":[{"low":0,"up":18,"cElement": [{"cName": "Y.V","cData": "int19","bit_use": { "low": 0,"up": 18},"cArray": [{"low" : 0,"up" : 0,"step" : 1}]}]}]} ]}
# RTL Port declarations: 
set portNum 12
set portList { 
	{ ap_clk sc_in sc_logic 1 clock -1 } 
	{ ap_rst sc_in sc_logic 1 reset -1 active_high_sync } 
	{ ap_start sc_in sc_logic 1 start -1 } 
	{ ap_done sc_out sc_logic 1 predone -1 } 
	{ ap_idle sc_out sc_logic 1 done -1 } 
	{ ap_ready sc_out sc_logic 1 ready -1 } 
	{ I_V sc_in sc_lv 19 signal 0 } 
	{ Q_V sc_in sc_lv 19 signal 1 } 
	{ X_V sc_out sc_lv 19 signal 2 } 
	{ X_V_ap_vld sc_out sc_logic 1 outvld 2 } 
	{ Y_V sc_out sc_lv 19 signal 3 } 
	{ Y_V_ap_vld sc_out sc_logic 1 outvld 3 } 
}
set NewPortList {[ 
	{ "name": "ap_clk", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "clock", "bundle":{"name": "ap_clk", "role": "default" }} , 
 	{ "name": "ap_rst", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "reset", "bundle":{"name": "ap_rst", "role": "default" }} , 
 	{ "name": "ap_start", "direction": "in", "datatype": "sc_logic", "bitwidth":1, "type": "start", "bundle":{"name": "ap_start", "role": "default" }} , 
 	{ "name": "ap_done", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "predone", "bundle":{"name": "ap_done", "role": "default" }} , 
 	{ "name": "ap_idle", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "done", "bundle":{"name": "ap_idle", "role": "default" }} , 
 	{ "name": "ap_ready", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "ready", "bundle":{"name": "ap_ready", "role": "default" }} , 
 	{ "name": "I_V", "direction": "in", "datatype": "sc_lv", "bitwidth":19, "type": "signal", "bundle":{"name": "I_V", "role": "default" }} , 
 	{ "name": "Q_V", "direction": "in", "datatype": "sc_lv", "bitwidth":19, "type": "signal", "bundle":{"name": "Q_V", "role": "default" }} , 
 	{ "name": "X_V", "direction": "out", "datatype": "sc_lv", "bitwidth":19, "type": "signal", "bundle":{"name": "X_V", "role": "default" }} , 
 	{ "name": "X_V_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "X_V", "role": "ap_vld" }} , 
 	{ "name": "Y_V", "direction": "out", "datatype": "sc_lv", "bitwidth":19, "type": "signal", "bundle":{"name": "Y_V", "role": "default" }} , 
 	{ "name": "Y_V_ap_vld", "direction": "out", "datatype": "sc_logic", "bitwidth":1, "type": "outvld", "bundle":{"name": "Y_V", "role": "ap_vld" }}  ]}
set Spec2ImplPortList { 
	I_V { ap_none {  { I_V in_data 0 19 } } }
	Q_V { ap_none {  { Q_V in_data 0 19 } } }
	X_V { ap_vld {  { X_V out_data 1 19 }  { X_V_ap_vld out_vld 1 1 } } }
	Y_V { ap_vld {  { Y_V out_data 1 19 }  { Y_V_ap_vld out_vld 1 1 } } }
}

# RTL port scheduling information:
set fifoSchedulingInfoList { 
}

# RTL bus port read request latency information:
set busReadReqLatencyList { 
}

# RTL bus port write response latency information:
set busWriteResLatencyList { 
}

# RTL array port load latency information:
set memoryLoadLatencyList { 
}
