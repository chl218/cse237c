/*
	Filename: fir.cpp
		Complex FIR or Match filter
		firI1 and firI2 share coef_t c[N]
		firQ1 and firQ2 share coef_t c[N]
		
	INPUT:
		I: signal for I sample
		I: signal for Q sample

	OUTPUT:
		X: filtered output
		Y: filtered output

*/

#include "fir.h"

void firI1 (
	data_t *y,
	data_t x
	) {

	coef_t c[N] = {1,    -1,    1,    -1,    -1,    -1,    1,    1,    -1,    -1,    -1,    1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    1,    1,    1,    1,    -1,    -1,    1,    1,    1,    -1,    -1,    -1};

	static data_t shift_reg[N];
	#pragma HLS ARRAY_PARTITION variable=shift_reg cyclic factor=8 dim=1

	data_t acc = 0;
	data_t data;
	ap_uint<5> i = 0;
	Shift_Accum_Loop: for(i = N-1; i >= 1; i--) {
	#pragma HLS PIPELINE II=1
		shift_reg[i] = shift_reg[i-1];
		acc += shift_reg[i] * c[i];
	}

	shift_reg[0] = x;
	*y = acc + x * c[0];

}

void firI2 (
  data_t *y,
  data_t x
  ) {

	coef_t c[N] = {1,    -1,    1,    -1,    -1,    -1,    1,    1,    -1,    -1,    -1,    1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    1,    1,    1,    1,    -1,    -1,    1,    1,    1,    -1,    -1,    -1};

	static data_t shift_reg[N];
	#pragma HLS ARRAY_PARTITION variable=shift_reg cyclic factor=8 dim=1

	data_t acc = 0;
	data_t data;
	ap_uint<5> i = 0;
	Shift_Accum_Loop: for(i = N-1; i >= 1; i--) {
	#pragma HLS PIPELINE II=1
		shift_reg[i] = shift_reg[i-1];
		acc += shift_reg[i] * c[i];
	}

	shift_reg[0] = x;
	*y = acc + x * c[0];

}

void firQ1 (
  data_t *y,
  data_t x
  ) {

	coef_t c[N] = {-1,    -1,    1,    -1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    -1,    1,    -1,    1,    1,    -1,    1,    -1,    -1,    1,    -1,    1,    1,    1,    1,    -1,    1,    -1,    1,    1};


	static data_t shift_reg[N];
	#pragma HLS ARRAY_PARTITION variable=shift_reg cyclic factor=8 dim=1

	data_t acc = 0;
	data_t data;
	ap_uint<5> i = 0;
	Shift_Accum_Loop: for(i = N-1; i >= 1; i--) {
	#pragma HLS PIPELINE II=1
		shift_reg[i] = shift_reg[i-1];
		acc += shift_reg[i] * c[i];
	}

	shift_reg[0] = x;
	*y = acc + x * c[0];

}

void firQ2 (
  data_t *y,
  data_t x
  ) {

	coef_t c[N] = {-1,    -1,    1,    -1,    1,    -1,    1,    -1,    -1,    -1,    -1,    1,    -1,    1,    -1,    1,    1,    -1,    1,    -1,    -1,    1,    -1,    1,    1,    1,    1,    -1,    1,    -1,    1,    1};

	static data_t shift_reg[N];
	#pragma HLS ARRAY_PARTITION variable=shift_reg cyclic factor=8 dim=1

	data_t acc = 0;
	data_t data;
	ap_uint<5> i = 0;
	Shift_Accum_Loop: for(i = N-1; i >= 1; i--) {
	#pragma HLS PIPELINE II=1
		shift_reg[i] = shift_reg[i-1];
		acc += shift_reg[i] * c[i];
	}

	shift_reg[0] = x;
	*y = acc + x * c[0];
}

void fir (
  data_t I,
  data_t Q,

  data_t *X,
  data_t *Y
  ) {

	data_t IinIfir, QinQfir, QinIfir, IinQfir;

	firI1(&IinIfir, I);
	firQ1(&QinQfir, Q);

	firI2(&QinIfir, Q);
	firQ2(&IinQfir, I);
	
	//Calculate X
	*X = IinIfir + QinQfir;
	//Calculate Y
	*Y = QinIfir - IinQfir;



}


