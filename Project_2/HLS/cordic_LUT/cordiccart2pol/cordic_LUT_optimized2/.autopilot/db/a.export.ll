; ModuleID = 'D:/Projects/vivado/Project_2/HLS/cordic_LUT/cordiccart2pol/cordic_LUT_optimized2/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@my_LUT_th = global [256 x float] zeroinitializer, align 16
@my_LUT_r = global [256 x float] zeroinitializer, align 16
@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535]
@cordiccart2pol_str = internal unnamed_addr constant [15 x i8] c"cordiccart2pol\00"
@p_str1 = private unnamed_addr constant [14 x i8] c"RAM_1P_LUTRAM\00", align 1
@p_str = private unnamed_addr constant [1 x i8] zeroinitializer, align 1

declare i64 @llvm.part.select.i64(i64, i32, i32) nounwind readnone

declare i13 @llvm.part.select.i13(i13, i32, i32) nounwind readnone

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

define void @cordiccart2pol(float %x, float %y, float* %r, float* %theta) nounwind uwtable {
_ifconv:
  call void (...)* @_ssdm_op_SpecBitsMap(float %x) nounwind, !map !19
  call void (...)* @_ssdm_op_SpecBitsMap(float %y) nounwind, !map !25
  call void (...)* @_ssdm_op_SpecBitsMap(float* %r) nounwind, !map !29
  call void (...)* @_ssdm_op_SpecBitsMap(float* %theta) nounwind, !map !33
  call void (...)* @_ssdm_op_SpecTopModule([15 x i8]* @cordiccart2pol_str) nounwind
  %y_read = call float @_ssdm_op_Read.ap_auto.float(float %y) nounwind
  %x_read = call float @_ssdm_op_Read.ap_auto.float(float %x) nounwind
  call void (...)* @_ssdm_op_SpecMemCore([256 x float]* @my_LUT_th, [1 x i8]* @p_str, [14 x i8]* @p_str1, [1 x i8]* @p_str, i32 -1, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str) nounwind
  call void (...)* @_ssdm_op_SpecMemCore([256 x float]* @my_LUT_r, [1 x i8]* @p_str, [14 x i8]* @p_str1, [1 x i8]* @p_str, i32 -1, [1 x i8]* @p_str, [1 x i8]* @p_str, [1 x i8]* @p_str) nounwind
  %d_assign = fpext float %x_read to double
  %ireg_V = bitcast double %d_assign to i64
  %tmp_9 = trunc i64 %ireg_V to i63
  %isneg = call i1 @_ssdm_op_BitSelect.i1.i64.i32(i64 %ireg_V, i32 63)
  %exp_tmp_V = call i11 @_ssdm_op_PartSelect.i11.i64.i32.i32(i64 %ireg_V, i32 52, i32 62)
  %tmp_2 = zext i11 %exp_tmp_V to i12
  %exp_V = add i12 -1023, %tmp_2
  %tmp_18 = trunc i64 %ireg_V to i52
  %tmp = call i53 @_ssdm_op_BitConcatenate.i53.i1.i52(i1 true, i52 %tmp_18)
  %p_Result_5 = zext i53 %tmp to i54
  %man_V_1 = sub i54 0, %p_Result_5
  %man_V = select i1 %isneg, i54 %man_V_1, i54 %p_Result_5
  %tmp_7 = icmp eq i63 %tmp_9, 0
  %F2 = sub i12 1075, %tmp_2
  %QUAN_INC = icmp sgt i12 %F2, 10
  %tmp_s = add i12 -10, %F2
  %tmp_1 = sub i12 10, %F2
  %sh_amt = select i1 %QUAN_INC, i12 %tmp_s, i12 %tmp_1
  %sh_amt_cast = sext i12 %sh_amt to i32
  %tmp_3 = icmp eq i12 %F2, 10
  %fixed_x_V = trunc i54 %man_V to i13
  %tmp_5 = icmp ult i12 %sh_amt, 54
  %tmp_4 = icmp ult i12 %sh_amt, 13
  %tmp_6 = zext i32 %sh_amt_cast to i54
  %tmp_8 = ashr i54 %man_V, %tmp_6
  %tmp_27 = trunc i54 %tmp_8 to i13
  %p_s = select i1 %isneg, i13 -1, i13 0
  %tmp_10 = sext i13 %fixed_x_V to i32
  %tmp_11 = shl i32 %tmp_10, %sh_amt_cast
  %tmp_33 = trunc i32 %tmp_11 to i13
  %p_Val2_2 = select i1 %tmp_5, i13 %tmp_27, i13 %p_s
  %tmp_12 = icmp sgt i12 %tmp_s, 54
  %tmp_13 = add i12 -11, %F2
  %tmp_17_cast = sext i12 %tmp_13 to i32
  %tmp_46 = call i1 @_ssdm_op_BitSelect.i1.i54.i32(i54 %man_V, i32 %tmp_17_cast)
  %qb = select i1 %tmp_12, i1 %isneg, i1 %tmp_46
  %tmp_14 = icmp sgt i12 %F2, 11
  %tmp_15 = add i12 -12, %F2
  %tmp_16 = icmp sgt i12 %tmp_15, 53
  %tmp_49 = trunc i12 %tmp_15 to i6
  %p_op = sub i6 -11, %tmp_49
  %tmp_55 = select i1 %tmp_16, i6 0, i6 %p_op
  %tmp_61 = zext i6 %tmp_55 to i54
  %tmp_64 = lshr i54 -1, %tmp_61
  %p_Result_s = and i54 %man_V, %tmp_64
  %tmp_17 = icmp ne i54 %p_Result_s, 0
  %r_1 = and i1 %tmp_14, %tmp_17
  %tmp_72 = call i1 @_ssdm_op_BitSelect.i1.i13.i32(i13 %p_Val2_2, i32 12)
  %p_r_i_i1 = or i1 %isneg, %r_1
  %qb_assign_1 = and i1 %p_r_i_i1, %qb
  %tmp_19 = zext i1 %qb_assign_1 to i13
  %fixed_x_V_7 = add i13 %p_Val2_2, %tmp_19
  %tmp_75 = call i1 @_ssdm_op_BitSelect.i1.i13.i32(i13 %fixed_x_V_7, i32 12)
  %fixed_x_V_2 = select i1 %tmp_3, i13 %fixed_x_V, i13 0
  %sel_tmp1 = xor i1 %tmp_3, true
  %sel_tmp2 = and i1 %QUAN_INC, %sel_tmp1
  %sel_tmp3 = xor i1 %tmp_72, true
  %sel_tmp4 = and i1 %sel_tmp2, %sel_tmp3
  %fixed_x_V_3 = select i1 %sel_tmp4, i13 %fixed_x_V_7, i13 %fixed_x_V_2
  %sel_tmp8 = and i1 %sel_tmp2, %tmp_72
  %fixed_x_V_4 = select i1 %sel_tmp8, i13 %fixed_x_V_7, i13 %fixed_x_V_3
  %sel_tmp = icmp slt i12 %F2, 10
  %sel_tmp5 = and i1 %sel_tmp, %tmp_4
  %fixed_x_V_8 = select i1 %sel_tmp5, i13 %tmp_33, i13 %fixed_x_V_4
  %tmp129_demorgan = or i1 %tmp_75, %sel_tmp5
  %tmp1 = xor i1 %tmp129_demorgan, true
  %carry_1_i1 = and i1 %sel_tmp8, %tmp1
  %tmp130_cast_cast = select i1 %QUAN_INC, i12 2, i12 1
  %tmp_20 = add i12 %tmp130_cast_cast, %exp_V
  %tmp_21 = icmp sgt i12 %tmp_20, 2
  %pos1 = add i12 3, %F2
  %pos1_cast = sext i12 %pos1 to i32
  %pos2 = add i12 4, %F2
  %pos2_cast = sext i12 %pos2 to i32
  %newsignbit = call i1 @_ssdm_op_BitSelect.i1.i13.i32(i13 %fixed_x_V_8, i32 12)
  %tmp_22 = icmp slt i12 %pos1, 54
  %tmp_80 = call i1 @_ssdm_op_BitSelect.i1.i12.i32(i12 %pos1, i32 11)
  %rev = xor i1 %tmp_80, true
  %tmp_23 = zext i32 %pos1_cast to i54
  %tmp_24 = ashr i54 %man_V, %tmp_23
  %lD = trunc i54 %tmp_24 to i1
  %tmp2 = and i1 %lD, %rev
  %Range1_all_ones_1 = and i1 %tmp2, %tmp_22
  %tmp_83 = call i1 @_ssdm_op_BitSelect.i1.i12.i32(i12 %pos2, i32 11)
  %rev6 = xor i1 %tmp_83, true
  %tmp_25 = icmp slt i12 %pos2, 54
  %or_cond115_i1 = and i1 %tmp_25, %rev6
  %tmp_26 = zext i32 %pos2_cast to i54
  %Range2_V_1 = lshr i54 %man_V, %tmp_26
  %r_V = lshr i54 -1, %tmp_26
  %Range2_all_ones = icmp eq i54 %Range2_V_1, %r_V
  %Range2_all_ones_1_i1 = select i1 %or_cond115_i1, i1 %Range2_all_ones, i1 %rev6
  %or_cond117_i1 = and i1 %tmp_25, %rev
  %Range1_all_ones = and i1 %Range2_all_ones_1_i1, %Range1_all_ones_1
  %tmp_28 = icmp eq i54 %Range2_V_1, 0
  %Range1_all_zeros = xor i1 %Range1_all_ones_1, true
  %p_122_i1 = and i1 %tmp_28, %Range1_all_zeros
  %tmp_29 = icmp eq i12 %pos2, 54
  %Range1_all_zeros_1 = icmp eq i54 %man_V, 0
  %p_119_i1 = or i1 %Range1_all_zeros_1, %rev
  %tmp_28_not = xor i1 %tmp_25, true
  %sel_tmp6 = or i1 %tmp_80, %tmp_28_not
  %sel_tmp7 = and i1 %tmp_29, %sel_tmp6
  %sel_tmp9 = select i1 %sel_tmp7, i1 %Range1_all_ones_1, i1 %rev
  %Range1_all_ones_2_i1 = select i1 %or_cond117_i1, i1 %Range1_all_ones, i1 %sel_tmp9
  %sel_tmp10 = select i1 %sel_tmp7, i1 %Range1_all_zeros, i1 %p_119_i1
  %Range1_all_zeros_2_i1 = select i1 %or_cond117_i1, i1 %p_122_i1, i1 %sel_tmp10
  %deleted_zeros = select i1 %carry_1_i1, i1 %Range1_all_ones_2_i1, i1 %Range1_all_zeros_2_i1
  %sel_tmp23_not = xor i1 %sel_tmp8, true
  %tmp3 = or i1 %sel_tmp5, %sel_tmp23_not
  %carry_1_not_i1 = or i1 %tmp3, %tmp_75
  %Range2_all_ones_1_not_i1 = xor i1 %Range2_all_ones_1_i1, true
  %brmerge123_i1 = or i1 %carry_1_not_i1, %Range2_all_ones_1_not_i1
  %Range1_all_ones_2_mux_i1 = and i1 %Range1_all_ones_2_i1, %carry_1_not_i1
  %tmp_84 = call i1 @_ssdm_op_BitSelect.i1.i12.i32(i12 %pos1, i32 11)
  %p_120_i1 = or i1 %tmp_84, %Range1_all_zeros
  %deleted_ones = select i1 %brmerge123_i1, i1 %Range1_all_ones_2_mux_i1, i1 %p_120_i1
  %Range1_all_ones_2_i1_6 = and i1 %carry_1_i1, %Range1_all_ones_2_i1
  %tmp_30 = xor i1 %Range1_all_ones_2_i1_6, true
  %sel_tmp11 = xor i1 %tmp_22, true
  %deleted_ones_0_i1 = or i1 %deleted_ones, %sel_tmp11
  %tmp4 = and i1 %newsignbit, %sel_tmp11
  %sel_tmp12 = and i1 %tmp4, %isneg
  %sel_tmp13 = and i1 %tmp_22, %isneg
  %p_Repl2_s = select i1 %sel_tmp13, i1 %tmp_30, i1 %sel_tmp12
  %deleted_zeros_not = xor i1 %deleted_zeros, true
  %deleted_zeros_0_not_i1 = and i1 %tmp_22, %deleted_zeros_not
  %brmerge_i1 = or i1 %newsignbit, %deleted_zeros_0_not_i1
  %tmp_32 = xor i1 %isneg, true
  %overflow = and i1 %brmerge_i1, %tmp_32
  %brmerge121_demorgan_i1 = and i1 %newsignbit, %deleted_ones_0_i1
  %brmerge121_i1 = xor i1 %brmerge121_demorgan_i1, true
  %underflow = and i1 %p_Repl2_s, %brmerge121_i1
  %brmerge_i_i1 = or i1 %underflow, %overflow
  %p_Result_6 = call i13 @_ssdm_op_BitSet.i13.i13.i32.i1(i13 %fixed_x_V_8, i32 12, i1 %p_Repl2_s)
  %sel_tmp57_demorgan = or i1 %tmp_7, %tmp_21
  %sel_tmp14 = xor i1 %tmp_7, true
  %tmp5 = and i1 %brmerge_i_i1, %sel_tmp14
  %sel_tmp15 = and i1 %tmp5, %tmp_21
  %d_assign_1 = fpext float %y_read to double
  %ireg_V_1 = bitcast double %d_assign_1 to i64
  %tmp_86 = trunc i64 %ireg_V_1 to i63
  %isneg_1 = call i1 @_ssdm_op_BitSelect.i1.i64.i32(i64 %ireg_V_1, i32 63)
  %exp_tmp_V_1 = call i11 @_ssdm_op_PartSelect.i11.i64.i32.i32(i64 %ireg_V_1, i32 52, i32 62)
  %tmp_34 = zext i11 %exp_tmp_V_1 to i12
  %exp_V_1 = add i12 -1023, %tmp_34
  %tmp_88 = trunc i64 %ireg_V_1 to i52
  %tmp_35 = call i53 @_ssdm_op_BitConcatenate.i53.i1.i52(i1 true, i52 %tmp_88)
  %p_Result_7 = zext i53 %tmp_35 to i54
  %man_V_3 = sub i54 0, %p_Result_7
  %man_V_6 = select i1 %isneg_1, i54 %man_V_3, i54 %p_Result_7
  %tmp_36 = icmp eq i63 %tmp_86, 0
  %F2_1 = sub i12 1075, %tmp_34
  %QUAN_INC_1 = icmp sgt i12 %F2_1, 10
  %tmp_37 = add i12 -10, %F2_1
  %tmp_38 = sub i12 10, %F2_1
  %sh_amt_1 = select i1 %QUAN_INC_1, i12 %tmp_37, i12 %tmp_38
  %sh_amt_1_cast = sext i12 %sh_amt_1 to i32
  %tmp_39 = icmp eq i12 %F2_1, 10
  %fixed_y_V = trunc i54 %man_V_6 to i13
  %tmp_40 = icmp ult i12 %sh_amt_1, 54
  %tmp_41 = icmp ult i12 %sh_amt_1, 13
  %tmp_42 = zext i32 %sh_amt_1_cast to i54
  %tmp_43 = ashr i54 %man_V_6, %tmp_42
  %tmp_90 = trunc i54 %tmp_43 to i13
  %p_7 = select i1 %isneg_1, i13 -1, i13 0
  %tmp_44 = sext i13 %fixed_y_V to i32
  %tmp_45 = shl i32 %tmp_44, %sh_amt_1_cast
  %tmp_91 = trunc i32 %tmp_45 to i13
  %p_Val2_7 = select i1 %tmp_40, i13 %tmp_90, i13 %p_7
  %tmp_47 = icmp sgt i12 %tmp_37, 54
  %tmp_48 = add i12 -11, %F2_1
  %tmp_64_cast = sext i12 %tmp_48 to i32
  %tmp_92 = call i1 @_ssdm_op_BitSelect.i1.i54.i32(i54 %man_V_6, i32 %tmp_64_cast)
  %qb_1 = select i1 %tmp_47, i1 %isneg_1, i1 %tmp_92
  %tmp_50 = icmp sgt i12 %F2_1, 11
  %tmp_51 = add i12 -12, %F2_1
  %tmp_52 = icmp sgt i12 %tmp_51, 53
  %tmp_93 = trunc i12 %tmp_51 to i6
  %p_op1 = sub i6 -11, %tmp_93
  %tmp_94 = select i1 %tmp_52, i6 0, i6 %p_op1
  %tmp_95 = zext i6 %tmp_94 to i54
  %tmp_96 = lshr i54 -1, %tmp_95
  %p_Result_3 = and i54 %man_V_6, %tmp_96
  %tmp_53 = icmp ne i54 %p_Result_3, 0
  %r_2 = and i1 %tmp_50, %tmp_53
  %tmp_98 = call i1 @_ssdm_op_BitSelect.i1.i13.i32(i13 %p_Val2_7, i32 12)
  %p_r_i_i = or i1 %isneg_1, %r_2
  %qb_assign_3 = and i1 %p_r_i_i, %qb_1
  %tmp_54 = zext i1 %qb_assign_3 to i13
  %fixed_y_V_7 = add i13 %p_Val2_7, %tmp_54
  %tmp_99 = call i1 @_ssdm_op_BitSelect.i1.i13.i32(i13 %fixed_y_V_7, i32 12)
  %fixed_y_V_2 = select i1 %tmp_39, i13 %fixed_y_V, i13 0
  %sel_tmp16 = xor i1 %tmp_39, true
  %sel_tmp17 = and i1 %QUAN_INC_1, %sel_tmp16
  %sel_tmp18 = xor i1 %tmp_98, true
  %sel_tmp19 = and i1 %sel_tmp17, %sel_tmp18
  %fixed_y_V_3 = select i1 %sel_tmp19, i13 %fixed_y_V_7, i13 %fixed_y_V_2
  %sel_tmp20 = and i1 %sel_tmp17, %tmp_98
  %fixed_y_V_4 = select i1 %sel_tmp20, i13 %fixed_y_V_7, i13 %fixed_y_V_3
  %sel_tmp21 = icmp slt i12 %F2_1, 10
  %sel_tmp22 = and i1 %sel_tmp21, %tmp_41
  %fixed_y_V_8 = select i1 %sel_tmp22, i13 %tmp_91, i13 %fixed_y_V_4
  %tmp135_demorgan = or i1 %tmp_99, %sel_tmp22
  %tmp6 = xor i1 %tmp135_demorgan, true
  %carry_1_i = and i1 %sel_tmp20, %tmp6
  %tmp136_cast_cast = select i1 %QUAN_INC_1, i12 2, i12 1
  %tmp_56 = add i12 %tmp136_cast_cast, %exp_V_1
  %tmp_57 = icmp sgt i12 %tmp_56, 2
  %pos1_1 = add i12 3, %F2_1
  %pos1_1_cast = sext i12 %pos1_1 to i32
  %pos2_1 = add i12 4, %F2_1
  %pos2_1_cast = sext i12 %pos2_1 to i32
  %newsignbit_1 = call i1 @_ssdm_op_BitSelect.i1.i13.i32(i13 %fixed_y_V_8, i32 12)
  %tmp_58 = icmp slt i12 %pos1_1, 54
  %tmp_101 = call i1 @_ssdm_op_BitSelect.i1.i12.i32(i12 %pos1_1, i32 11)
  %rev1 = xor i1 %tmp_101, true
  %tmp_59 = zext i32 %pos1_1_cast to i54
  %tmp_60 = ashr i54 %man_V_6, %tmp_59
  %lD_1 = trunc i54 %tmp_60 to i1
  %tmp7 = and i1 %lD_1, %rev1
  %Range1_all_ones_3 = and i1 %tmp7, %tmp_58
  %tmp_103 = call i1 @_ssdm_op_BitSelect.i1.i12.i32(i12 %pos2_1, i32 11)
  %rev2 = xor i1 %tmp_103, true
  %tmp_62 = icmp slt i12 %pos2_1, 54
  %or_cond115_i = and i1 %tmp_62, %rev2
  %tmp_63 = zext i32 %pos2_1_cast to i54
  %Range2_V_3 = lshr i54 %man_V_6, %tmp_63
  %r_V_1 = lshr i54 -1, %tmp_63
  %Range2_all_ones_1 = icmp eq i54 %Range2_V_3, %r_V_1
  %Range2_all_ones_1_i = select i1 %or_cond115_i, i1 %Range2_all_ones_1, i1 %rev2
  %or_cond117_i = and i1 %tmp_62, %rev1
  %Range1_all_ones_2 = and i1 %Range2_all_ones_1_i, %Range1_all_ones_3
  %tmp_65 = icmp eq i54 %Range2_V_3, 0
  %Range1_all_zeros_2 = xor i1 %Range1_all_ones_3, true
  %p_122_i = and i1 %tmp_65, %Range1_all_zeros_2
  %tmp_66 = icmp eq i12 %pos2_1, 54
  %Range1_all_zeros_3 = icmp eq i54 %man_V_6, 0
  %p_119_i = or i1 %Range1_all_zeros_3, %rev1
  %tmp_65_not = xor i1 %tmp_62, true
  %sel_tmp23 = or i1 %tmp_101, %tmp_65_not
  %sel_tmp24 = and i1 %tmp_66, %sel_tmp23
  %sel_tmp25 = select i1 %sel_tmp24, i1 %Range1_all_ones_3, i1 %rev1
  %Range1_all_ones_2_i = select i1 %or_cond117_i, i1 %Range1_all_ones_2, i1 %sel_tmp25
  %sel_tmp26 = select i1 %sel_tmp24, i1 %Range1_all_zeros_2, i1 %p_119_i
  %Range1_all_zeros_2_i = select i1 %or_cond117_i, i1 %p_122_i, i1 %sel_tmp26
  %deleted_zeros_1 = select i1 %carry_1_i, i1 %Range1_all_ones_2_i, i1 %Range1_all_zeros_2_i
  %sel_tmp89_not = xor i1 %sel_tmp20, true
  %tmp8 = or i1 %sel_tmp22, %sel_tmp89_not
  %carry_1_not_i = or i1 %tmp8, %tmp_99
  %Range2_all_ones_1_not_i = xor i1 %Range2_all_ones_1_i, true
  %brmerge123_i = or i1 %carry_1_not_i, %Range2_all_ones_1_not_i
  %Range1_all_ones_2_mux_i = and i1 %Range1_all_ones_2_i, %carry_1_not_i
  %tmp_104 = call i1 @_ssdm_op_BitSelect.i1.i12.i32(i12 %pos1_1, i32 11)
  %p_120_i = or i1 %tmp_104, %Range1_all_zeros_2
  %deleted_ones_1 = select i1 %brmerge123_i, i1 %Range1_all_ones_2_mux_i, i1 %p_120_i
  %Range1_all_ones_2_i_7 = and i1 %carry_1_i, %Range1_all_ones_2_i
  %tmp_67 = xor i1 %Range1_all_ones_2_i_7, true
  %sel_tmp27 = xor i1 %tmp_58, true
  %deleted_ones_0_i = or i1 %deleted_ones_1, %sel_tmp27
  %tmp9 = and i1 %newsignbit_1, %sel_tmp27
  %sel_tmp28 = and i1 %tmp9, %isneg_1
  %sel_tmp29 = and i1 %tmp_58, %isneg_1
  %p_Repl2_1 = select i1 %sel_tmp29, i1 %tmp_67, i1 %sel_tmp28
  %deleted_zeros_1_not = xor i1 %deleted_zeros_1, true
  %deleted_zeros_0_not_i = and i1 %tmp_58, %deleted_zeros_1_not
  %brmerge_i = or i1 %newsignbit_1, %deleted_zeros_0_not_i
  %tmp_68 = xor i1 %isneg_1, true
  %overflow_1 = and i1 %brmerge_i, %tmp_68
  %brmerge121_demorgan_i = and i1 %newsignbit_1, %deleted_ones_0_i
  %brmerge121_i = xor i1 %brmerge121_demorgan_i, true
  %underflow_1 = and i1 %p_Repl2_1, %brmerge121_i
  %brmerge_i_i = or i1 %underflow_1, %overflow_1
  %p_Result_8 = call i13 @_ssdm_op_BitSet.i13.i13.i32.i1(i13 %fixed_y_V_8, i32 12, i1 %p_Repl2_1)
  %sel_tmp123_demorgan = or i1 %tmp_36, %tmp_57
  %sel_tmp30 = xor i1 %tmp_36, true
  %tmp10 = and i1 %brmerge_i_i, %sel_tmp30
  %sel_tmp31 = and i1 %tmp10, %tmp_57
  %tmp_31 = call i4 @_ssdm_op_PartSelect.i4.i13.i32.i32(i13 %p_Result_6, i32 9, i32 12)
  %tmp_106 = call i4 @_ssdm_op_PartSelect.i4.i13.i32.i32(i13 %fixed_x_V_8, i32 9, i32 12)
  %tmp_107 = select i1 %tmp_7, i4 0, i4 %tmp_106
  %tmp_69 = select i1 %sel_tmp57_demorgan, i4 %tmp_107, i4 %tmp_106
  %tmp_70 = select i1 %sel_tmp15, i4 %tmp_31, i4 %tmp_69
  %tmp_71 = call i4 @_ssdm_op_PartSelect.i4.i13.i32.i32(i13 %p_Result_8, i32 9, i32 12)
  %tmp_108 = call i4 @_ssdm_op_PartSelect.i4.i13.i32.i32(i13 %fixed_y_V_8, i32 9, i32 12)
  %tmp_109 = select i1 %tmp_36, i4 0, i4 %tmp_108
  %tmp_73 = select i1 %sel_tmp123_demorgan, i4 %tmp_109, i4 %tmp_108
  %tmp_74 = select i1 %sel_tmp31, i4 %tmp_71, i4 %tmp_73
  %p_Result_29_3 = call i8 @_ssdm_op_BitConcatenate.i8.i4.i4(i4 %tmp_70, i4 %tmp_74)
  %tmp_76 = zext i8 %p_Result_29_3 to i64
  %my_LUT_r_addr = getelementptr inbounds [256 x float]* @my_LUT_r, i64 0, i64 %tmp_76
  %my_LUT_r_load = load volatile float* %my_LUT_r_addr, align 4
  call void @_ssdm_op_Write.ap_auto.floatP(float* %r, float %my_LUT_r_load) nounwind
  %my_LUT_th_addr = getelementptr inbounds [256 x float]* @my_LUT_th, i64 0, i64 %tmp_76
  %my_LUT_th_load = load volatile float* %my_LUT_th_addr, align 4
  call void @_ssdm_op_Write.ap_auto.floatP(float* %theta, float %my_LUT_th_load) nounwind
  ret void
}

define weak void @_ssdm_op_Write.ap_auto.floatP(float*, float) {
entry:
  store float %1, float* %0
  ret void
}

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak void @_ssdm_op_SpecMemCore(...) {
entry:
  ret void
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak float @_ssdm_op_Read.ap_auto.float(float) {
entry:
  ret float %0
}

declare i63 @_ssdm_op_PartSelect.i63.i64.i32.i32(i64, i32, i32) nounwind readnone

declare i6 @_ssdm_op_PartSelect.i6.i12.i32.i32(i12, i32, i32) nounwind readnone

declare i52 @_ssdm_op_PartSelect.i52.i64.i32.i32(i64, i32, i32) nounwind readnone

define weak i4 @_ssdm_op_PartSelect.i4.i13.i32.i32(i13, i32, i32) nounwind readnone {
entry:
  %empty = call i13 @llvm.part.select.i13(i13 %0, i32 %1, i32 %2)
  %empty_8 = trunc i13 %empty to i4
  ret i4 %empty_8
}

declare i13 @_ssdm_op_PartSelect.i13.i54.i32.i32(i54, i32, i32) nounwind readnone

declare i13 @_ssdm_op_PartSelect.i13.i32.i32.i32(i32, i32, i32) nounwind readnone

define weak i11 @_ssdm_op_PartSelect.i11.i64.i32.i32(i64, i32, i32) nounwind readnone {
entry:
  %empty = call i64 @llvm.part.select.i64(i64 %0, i32 %1, i32 %2)
  %empty_9 = trunc i64 %empty to i11
  ret i11 %empty_9
}

declare i1 @_ssdm_op_PartSelect.i1.i54.i32.i32(i54, i32, i32) nounwind readnone

declare i16 @_ssdm_op_HSub(...)

declare i16 @_ssdm_op_HMul(...)

declare i16 @_ssdm_op_HDiv(...)

declare i16 @_ssdm_op_HAdd(...)

define weak i13 @_ssdm_op_BitSet.i13.i13.i32.i1(i13, i32, i1) nounwind readnone {
entry:
  %empty = icmp ne i1 %2, false
  %empty_10 = zext i1 %empty to i13
  %empty_11 = trunc i32 %1 to i13
  %empty_12 = shl i13 %empty_10, %empty_11
  %empty_13 = shl i13 1, %empty_11
  %empty_14 = xor i13 %empty_13, -1
  %empty_15 = and i13 %empty_14, %0
  %empty_16 = or i13 %empty_12, %empty_15
  ret i13 %empty_16
}

define weak i1 @_ssdm_op_BitSelect.i1.i64.i32(i64, i32) nounwind readnone {
entry:
  %empty = zext i32 %1 to i64
  %empty_17 = shl i64 1, %empty
  %empty_18 = and i64 %0, %empty_17
  %empty_19 = icmp ne i64 %empty_18, 0
  ret i1 %empty_19
}

define weak i1 @_ssdm_op_BitSelect.i1.i54.i32(i54, i32) nounwind readnone {
entry:
  %empty = zext i32 %1 to i54
  %empty_20 = shl i54 1, %empty
  %empty_21 = and i54 %0, %empty_20
  %empty_22 = icmp ne i54 %empty_21, 0
  ret i1 %empty_22
}

define weak i1 @_ssdm_op_BitSelect.i1.i13.i32(i13, i32) nounwind readnone {
entry:
  %empty = trunc i32 %1 to i13
  %empty_23 = shl i13 1, %empty
  %empty_24 = and i13 %0, %empty_23
  %empty_25 = icmp ne i13 %empty_24, 0
  ret i1 %empty_25
}

define weak i1 @_ssdm_op_BitSelect.i1.i12.i32(i12, i32) nounwind readnone {
entry:
  %empty = trunc i32 %1 to i12
  %empty_26 = shl i12 1, %empty
  %empty_27 = and i12 %0, %empty_26
  %empty_28 = icmp ne i12 %empty_27, 0
  ret i1 %empty_28
}

define weak i8 @_ssdm_op_BitConcatenate.i8.i4.i4(i4, i4) nounwind readnone {
entry:
  %empty = zext i4 %0 to i8
  %empty_29 = zext i4 %1 to i8
  %empty_30 = shl i8 %empty, 4
  %empty_31 = or i8 %empty_30, %empty_29
  ret i8 %empty_31
}

define weak i53 @_ssdm_op_BitConcatenate.i53.i1.i52(i1, i52) nounwind readnone {
entry:
  %empty = zext i1 %0 to i53
  %empty_32 = zext i52 %1 to i53
  %empty_33 = shl i53 %empty, 52
  %empty_34 = or i53 %empty_33, %empty_32
  ret i53 %empty_34
}

declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0, !7, !12}

!0 = metadata !{metadata !1, [256 x float]* @my_LUT_th}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"my_LUT_th", metadata !5, metadata !"float", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 255, i32 1}
!7 = metadata !{metadata !8, [256 x float]* @my_LUT_r}
!8 = metadata !{metadata !9}
!9 = metadata !{i32 0, i32 31, metadata !10}
!10 = metadata !{metadata !11}
!11 = metadata !{metadata !"my_LUT_r", metadata !5, metadata !"float", i32 0, i32 31}
!12 = metadata !{metadata !13, [1 x i32]* @llvm_global_ctors_0}
!13 = metadata !{metadata !14}
!14 = metadata !{i32 0, i32 31, metadata !15}
!15 = metadata !{metadata !16}
!16 = metadata !{metadata !"llvm.global_ctors.0", metadata !17, metadata !"", i32 0, i32 31}
!17 = metadata !{metadata !18}
!18 = metadata !{i32 0, i32 0, i32 1}
!19 = metadata !{metadata !20}
!20 = metadata !{i32 0, i32 31, metadata !21}
!21 = metadata !{metadata !22}
!22 = metadata !{metadata !"x", metadata !23, metadata !"float", i32 0, i32 31}
!23 = metadata !{metadata !24}
!24 = metadata !{i32 0, i32 0, i32 0}
!25 = metadata !{metadata !26}
!26 = metadata !{i32 0, i32 31, metadata !27}
!27 = metadata !{metadata !28}
!28 = metadata !{metadata !"y", metadata !23, metadata !"float", i32 0, i32 31}
!29 = metadata !{metadata !30}
!30 = metadata !{i32 0, i32 31, metadata !31}
!31 = metadata !{metadata !32}
!32 = metadata !{metadata !"r", metadata !17, metadata !"float", i32 0, i32 31}
!33 = metadata !{metadata !34}
!34 = metadata !{i32 0, i32 31, metadata !35}
!35 = metadata !{metadata !36}
!36 = metadata !{metadata !"theta", metadata !17, metadata !"float", i32 0, i32 31}
