; ModuleID = 'D:/Projects/vivado/Project_2/HLS/cordic_LUT/cordiccart2pol/cordic_LUT/.autopilot/db/a.o.2.bc'
target datalayout = "e-p:64:64:64-i1:8:8-i8:8:8-i16:16:16-i32:32:32-i64:64:64-f32:32:32-f64:64:64-v64:64:64-v128:128:128-a0:0:64-s0:64:64-f80:128:128-n8:16:32:64-S128"
target triple = "x86_64-w64-mingw32"

@my_LUT_th = global [1024 x float] zeroinitializer, align 16
@my_LUT_r = global [1024 x float] zeroinitializer, align 16
@llvm_global_ctors_1 = appending global [1 x void ()*] [void ()* @_GLOBAL__I_a]
@llvm_global_ctors_0 = appending global [1 x i32] [i32 65535]
@cordiccart2pol_str = internal unnamed_addr constant [15 x i8] c"cordiccart2pol\00"

declare i64 @llvm.part.select.i64(i64, i32, i32) nounwind readnone

declare i32 @llvm.part.select.i32(i32, i32, i32) nounwind readnone

declare i12 @llvm.part.select.i12(i12, i32, i32) nounwind readnone

declare void @llvm.dbg.value(metadata, i64, metadata) nounwind readnone

define void @cordiccart2pol(float %x, float %y, float* %r, float* %theta) nounwind uwtable {
_ifconv:
  call void (...)* @_ssdm_op_SpecBitsMap(float %x) nounwind, !map !19
  call void (...)* @_ssdm_op_SpecBitsMap(float %y) nounwind, !map !25
  call void (...)* @_ssdm_op_SpecBitsMap(float* %r) nounwind, !map !29
  call void (...)* @_ssdm_op_SpecBitsMap(float* %theta) nounwind, !map !33
  call void (...)* @_ssdm_op_SpecTopModule([15 x i8]* @cordiccart2pol_str) nounwind
  %y_read = call float @_ssdm_op_Read.ap_auto.float(float %y) nounwind
  %x_read = call float @_ssdm_op_Read.ap_auto.float(float %x) nounwind
  %d_assign = fpext float %x_read to double
  %ireg_V = bitcast double %d_assign to i64
  %tmp_4 = trunc i64 %ireg_V to i63
  %isneg = call i1 @_ssdm_op_BitSelect.i1.i64.i32(i64 %ireg_V, i32 63)
  %exp_tmp_V = call i11 @_ssdm_op_PartSelect.i11.i64.i32.i32(i64 %ireg_V, i32 52, i32 62)
  %tmp_2 = zext i11 %exp_tmp_V to i12
  %exp_V = add i12 -1023, %tmp_2
  %tmp_13 = trunc i64 %ireg_V to i52
  %tmp = call i53 @_ssdm_op_BitConcatenate.i53.i1.i52(i1 true, i52 %tmp_13)
  %p_Result_5 = zext i53 %tmp to i54
  %man_V_1 = sub i54 0, %p_Result_5
  %man_V = select i1 %isneg, i54 %man_V_1, i54 %p_Result_5
  %tmp_7 = icmp eq i63 %tmp_4, 0
  %F2 = sub i12 1075, %tmp_2
  %QUAN_INC = icmp sgt i12 %F2, 30
  %tmp_s = add i12 -30, %F2
  %tmp_1 = sub i12 30, %F2
  %sh_amt = select i1 %QUAN_INC, i12 %tmp_s, i12 %tmp_1
  %sh_amt_cast = sext i12 %sh_amt to i32
  %tmp_3 = icmp eq i12 %F2, 30
  %fixed_x_V = trunc i54 %man_V to i32
  %tmp_5 = icmp ult i12 %sh_amt, 54
  %tmp_21 = call i7 @_ssdm_op_PartSelect.i7.i12.i32.i32(i12 %sh_amt, i32 5, i32 11)
  %icmp = icmp eq i7 %tmp_21, 0
  %tmp_6 = shl i32 %fixed_x_V, %sh_amt_cast
  %fixed_x_V_1 = select i1 %icmp, i32 %tmp_6, i32 0
  %tmp_8 = zext i32 %sh_amt_cast to i54
  %tmp_9 = ashr i54 %man_V, %tmp_8
  %tmp_23 = trunc i54 %tmp_9 to i32
  %p_Val2_0_i_i1 = select i1 %isneg, i32 -1, i32 0
  %p_Val2_2 = select i1 %tmp_5, i32 %tmp_23, i32 %p_Val2_0_i_i1
  %tmp_10 = icmp sgt i12 %tmp_s, 54
  %tmp_11 = add i12 -31, %F2
  %tmp_15_cast = sext i12 %tmp_11 to i32
  %tmp_26 = call i1 @_ssdm_op_BitSelect.i1.i54.i32(i54 %man_V, i32 %tmp_15_cast)
  %qb = select i1 %tmp_10, i1 %isneg, i1 %tmp_26
  %tmp_32 = call i7 @_ssdm_op_PartSelect.i7.i12.i32.i32(i12 %F2, i32 5, i32 11)
  %icmp5 = icmp sgt i7 %tmp_32, 0
  %tmp_12 = add i12 -32, %F2
  %tmp_14 = icmp sgt i12 %tmp_12, 53
  %tmp_42 = trunc i12 %tmp_12 to i6
  %p_op = sub i6 -11, %tmp_42
  %tmp_46 = select i1 %tmp_14, i6 0, i6 %p_op
  %tmp_49 = zext i6 %tmp_46 to i54
  %tmp_52 = lshr i54 -1, %tmp_49
  %p_Result_s = and i54 %man_V, %tmp_52
  %tmp_15 = icmp ne i54 %p_Result_s, 0
  %r_1 = and i1 %icmp5, %tmp_15
  %tmp_59 = call i1 @_ssdm_op_BitSelect.i1.i32.i32(i32 %p_Val2_2, i32 31)
  %p_r_i_i1 = or i1 %isneg, %r_1
  %qb_assign_1 = and i1 %p_r_i_i1, %qb
  %tmp_16 = zext i1 %qb_assign_1 to i32
  %fixed_x_V_7 = add nsw i32 %p_Val2_2, %tmp_16
  %tmp_62 = call i1 @_ssdm_op_BitSelect.i1.i32.i32(i32 %fixed_x_V_7, i32 31)
  %tmp_17 = xor i1 %tmp_62, true
  %fixed_x_V_3 = select i1 %tmp_3, i32 %fixed_x_V, i32 %fixed_x_V_7
  %sel_tmp1 = xor i1 %tmp_3, true
  %tmp1 = and i1 %tmp_59, %sel_tmp1
  %sel_tmp3 = and i1 %tmp1, %QUAN_INC
  %fixed_x_V_4 = select i1 %sel_tmp3, i32 %fixed_x_V_7, i32 %fixed_x_V_3
  %sel_tmp7 = icmp slt i12 %F2, 30
  %fixed_x_V_8 = select i1 %sel_tmp7, i32 %fixed_x_V_1, i32 %fixed_x_V_4
  %not_sel_tmp = icmp sgt i12 %F2, 29
  %tmp2 = and i1 %not_sel_tmp, %tmp_17
  %carry_1_i1 = and i1 %tmp2, %sel_tmp3
  %tmp107_cast_cast = select i1 %QUAN_INC, i12 2, i12 1
  %tmp_18 = add i12 %tmp107_cast_cast, %exp_V
  %tmp_67 = call i11 @_ssdm_op_PartSelect.i11.i12.i32.i32(i12 %tmp_18, i32 1, i32 11)
  %icmp8 = icmp sgt i11 %tmp_67, 0
  %pos1 = add i12 2, %F2
  %pos1_cast = sext i12 %pos1 to i32
  %pos2 = add i12 3, %F2
  %pos2_cast = sext i12 %pos2 to i32
  %newsignbit = call i1 @_ssdm_op_BitSelect.i1.i32.i32(i32 %fixed_x_V_8, i32 31)
  %tmp_19 = icmp slt i12 %pos1, 54
  %tmp_73 = call i1 @_ssdm_op_BitSelect.i1.i12.i32(i12 %pos1, i32 11)
  %rev = xor i1 %tmp_73, true
  %tmp_20 = zext i32 %pos1_cast to i54
  %tmp_22 = ashr i54 %man_V, %tmp_20
  %lD = trunc i54 %tmp_22 to i1
  %tmp3 = and i1 %lD, %rev
  %Range1_all_ones_1 = and i1 %tmp3, %tmp_19
  %tmp_78 = call i1 @_ssdm_op_BitSelect.i1.i12.i32(i12 %pos2, i32 11)
  %rev1 = xor i1 %tmp_78, true
  %tmp_24 = icmp slt i12 %pos2, 54
  %or_cond115_i1 = and i1 %tmp_24, %rev1
  %tmp_25 = zext i32 %pos2_cast to i54
  %Range2_V_1 = lshr i54 %man_V, %tmp_25
  %r_V = lshr i54 -1, %tmp_25
  %Range2_all_ones = icmp eq i54 %Range2_V_1, %r_V
  %Range2_all_ones_1_i1 = select i1 %or_cond115_i1, i1 %Range2_all_ones, i1 %rev1
  %or_cond117_i1 = and i1 %tmp_24, %rev
  %Range1_all_ones = and i1 %Range2_all_ones_1_i1, %Range1_all_ones_1
  %tmp_27 = icmp eq i54 %Range2_V_1, 0
  %Range1_all_zeros = xor i1 %Range1_all_ones_1, true
  %p_122_i1 = and i1 %tmp_27, %Range1_all_zeros
  %tmp_28 = icmp eq i12 %pos2, 54
  %Range1_all_zeros_1 = icmp eq i54 %man_V, 0
  %p_119_i1 = or i1 %Range1_all_zeros_1, %rev
  %tmp_27_not = xor i1 %tmp_24, true
  %sel_tmp = or i1 %tmp_73, %tmp_27_not
  %sel_tmp2 = and i1 %tmp_28, %sel_tmp
  %sel_tmp4 = select i1 %sel_tmp2, i1 %Range1_all_ones_1, i1 %rev
  %Range1_all_ones_2_i1 = select i1 %or_cond117_i1, i1 %Range1_all_ones, i1 %sel_tmp4
  %sel_tmp5 = select i1 %sel_tmp2, i1 %Range1_all_zeros, i1 %p_119_i1
  %Range1_all_zeros_2_i1 = select i1 %or_cond117_i1, i1 %p_122_i1, i1 %sel_tmp5
  %deleted_zeros = select i1 %carry_1_i1, i1 %Range1_all_ones_2_i1, i1 %Range1_all_zeros_2_i1
  %carry_1_not_i1 = xor i1 %carry_1_i1, true
  %Range2_all_ones_1_not_i1 = xor i1 %Range2_all_ones_1_i1, true
  %brmerge123_i1 = or i1 %carry_1_not_i1, %Range2_all_ones_1_not_i1
  %Range1_all_ones_2_mux_i1 = and i1 %Range1_all_ones_2_i1, %carry_1_not_i1
  %tmp_80 = call i1 @_ssdm_op_BitSelect.i1.i12.i32(i12 %pos1, i32 11)
  %p_120_i1 = or i1 %tmp_80, %Range1_all_zeros
  %deleted_ones = select i1 %brmerge123_i1, i1 %Range1_all_ones_2_mux_i1, i1 %p_120_i1
  %Range1_all_ones_2_i1_8 = and i1 %carry_1_i1, %Range1_all_ones_2_i1
  %tmp_29 = xor i1 %Range1_all_ones_2_i1_8, true
  %sel_tmp6 = xor i1 %tmp_19, true
  %deleted_ones_0_i1 = or i1 %deleted_ones, %sel_tmp6
  %tmp4 = and i1 %newsignbit, %sel_tmp6
  %sel_tmp8 = and i1 %tmp4, %isneg
  %sel_tmp9 = and i1 %tmp_19, %isneg
  %p_Repl2_s = select i1 %sel_tmp9, i1 %tmp_29, i1 %sel_tmp8
  %deleted_zeros_not = xor i1 %deleted_zeros, true
  %deleted_zeros_0_not_i1 = and i1 %tmp_19, %deleted_zeros_not
  %brmerge_i1 = or i1 %newsignbit, %deleted_zeros_0_not_i1
  %tmp_31 = xor i1 %isneg, true
  %overflow = and i1 %brmerge_i1, %tmp_31
  %brmerge121_demorgan_i1 = and i1 %newsignbit, %deleted_ones_0_i1
  %brmerge121_i1 = xor i1 %brmerge121_demorgan_i1, true
  %underflow = and i1 %p_Repl2_s, %brmerge121_i1
  %brmerge_i_i1 = or i1 %underflow, %overflow
  %p_Result_6 = call i32 @_ssdm_op_BitSet.i32.i32.i32.i1(i32 %fixed_x_V_8, i32 31, i1 %p_Repl2_s)
  %sel_tmp45_demorgan = or i1 %tmp_7, %icmp8
  %sel_tmp10 = xor i1 %tmp_7, true
  %tmp5 = and i1 %brmerge_i_i1, %sel_tmp10
  %sel_tmp11 = and i1 %tmp5, %icmp8
  %d_assign_1 = fpext float %y_read to double
  %ireg_V_1 = bitcast double %d_assign_1 to i64
  %tmp_82 = trunc i64 %ireg_V_1 to i63
  %isneg_1 = call i1 @_ssdm_op_BitSelect.i1.i64.i32(i64 %ireg_V_1, i32 63)
  %exp_tmp_V_1 = call i11 @_ssdm_op_PartSelect.i11.i64.i32.i32(i64 %ireg_V_1, i32 52, i32 62)
  %tmp_33 = zext i11 %exp_tmp_V_1 to i12
  %exp_V_1 = add i12 -1023, %tmp_33
  %tmp_84 = trunc i64 %ireg_V_1 to i52
  %tmp_34 = call i53 @_ssdm_op_BitConcatenate.i53.i1.i52(i1 true, i52 %tmp_84)
  %p_Result_7 = zext i53 %tmp_34 to i54
  %man_V_3 = sub i54 0, %p_Result_7
  %man_V_6 = select i1 %isneg_1, i54 %man_V_3, i54 %p_Result_7
  %tmp_35 = icmp eq i63 %tmp_82, 0
  %F2_1 = sub i12 1075, %tmp_33
  %QUAN_INC_1 = icmp sgt i12 %F2_1, 30
  %tmp_36 = add i12 -30, %F2_1
  %tmp_37 = sub i12 30, %F2_1
  %sh_amt_1 = select i1 %QUAN_INC_1, i12 %tmp_36, i12 %tmp_37
  %sh_amt_1_cast = sext i12 %sh_amt_1 to i32
  %tmp_38 = icmp eq i12 %F2_1, 30
  %fixed_y_V = trunc i54 %man_V_6 to i32
  %tmp_39 = icmp ult i12 %sh_amt_1, 54
  %tmp_86 = call i7 @_ssdm_op_PartSelect.i7.i12.i32.i32(i12 %sh_amt_1, i32 5, i32 11)
  %icmp1 = icmp eq i7 %tmp_86, 0
  %tmp_40 = shl i32 %fixed_y_V, %sh_amt_1_cast
  %fixed_y_V_1 = select i1 %icmp1, i32 %tmp_40, i32 0
  %tmp_41 = zext i32 %sh_amt_1_cast to i54
  %tmp_43 = ashr i54 %man_V_6, %tmp_41
  %tmp_87 = trunc i54 %tmp_43 to i32
  %p_Val2_0_i_i3 = select i1 %isneg_1, i32 -1, i32 0
  %p_Val2_7 = select i1 %tmp_39, i32 %tmp_87, i32 %p_Val2_0_i_i3
  %tmp_44 = icmp sgt i12 %tmp_36, 54
  %tmp_45 = add i12 -31, %F2_1
  %tmp_60_cast = sext i12 %tmp_45 to i32
  %tmp_88 = call i1 @_ssdm_op_BitSelect.i1.i54.i32(i54 %man_V_6, i32 %tmp_60_cast)
  %qb_1 = select i1 %tmp_44, i1 %isneg_1, i1 %tmp_88
  %tmp_89 = call i7 @_ssdm_op_PartSelect.i7.i12.i32.i32(i12 %F2_1, i32 5, i32 11)
  %icmp2 = icmp sgt i7 %tmp_89, 0
  %tmp_47 = add i12 -32, %F2_1
  %tmp_48 = icmp sgt i12 %tmp_47, 53
  %tmp_90 = trunc i12 %tmp_47 to i6
  %p_op1 = sub i6 -11, %tmp_90
  %tmp_91 = select i1 %tmp_48, i6 0, i6 %p_op1
  %tmp_92 = zext i6 %tmp_91 to i54
  %tmp_93 = lshr i54 -1, %tmp_92
  %p_Result_3 = and i54 %man_V_6, %tmp_93
  %tmp_50 = icmp ne i54 %p_Result_3, 0
  %r_2 = and i1 %icmp2, %tmp_50
  %tmp_95 = call i1 @_ssdm_op_BitSelect.i1.i32.i32(i32 %p_Val2_7, i32 31)
  %p_r_i_i = or i1 %isneg_1, %r_2
  %qb_assign_3 = and i1 %p_r_i_i, %qb_1
  %tmp_51 = zext i1 %qb_assign_3 to i32
  %fixed_y_V_7 = add nsw i32 %p_Val2_7, %tmp_51
  %tmp_96 = call i1 @_ssdm_op_BitSelect.i1.i32.i32(i32 %fixed_y_V_7, i32 31)
  %tmp_53 = xor i1 %tmp_96, true
  %fixed_y_V_3 = select i1 %tmp_38, i32 %fixed_y_V, i32 %fixed_y_V_7
  %sel_tmp12 = xor i1 %tmp_38, true
  %tmp6 = and i1 %tmp_95, %sel_tmp12
  %sel_tmp13 = and i1 %tmp6, %QUAN_INC_1
  %fixed_y_V_4 = select i1 %sel_tmp13, i32 %fixed_y_V_7, i32 %fixed_y_V_3
  %sel_tmp14 = icmp slt i12 %F2_1, 30
  %fixed_y_V_8 = select i1 %sel_tmp14, i32 %fixed_y_V_1, i32 %fixed_y_V_4
  %not_sel_tmp1 = icmp sgt i12 %F2_1, 29
  %tmp7 = and i1 %not_sel_tmp1, %tmp_53
  %carry_1_i = and i1 %tmp7, %sel_tmp13
  %tmp113_cast_cast = select i1 %QUAN_INC_1, i12 2, i12 1
  %tmp_54 = add i12 %tmp113_cast_cast, %exp_V_1
  %tmp_97 = call i11 @_ssdm_op_PartSelect.i11.i12.i32.i32(i12 %tmp_54, i32 1, i32 11)
  %icmp3 = icmp sgt i11 %tmp_97, 0
  %pos1_1 = add i12 2, %F2_1
  %pos1_1_cast = sext i12 %pos1_1 to i32
  %pos2_1 = add i12 3, %F2_1
  %pos2_1_cast = sext i12 %pos2_1 to i32
  %newsignbit_1 = call i1 @_ssdm_op_BitSelect.i1.i32.i32(i32 %fixed_y_V_8, i32 31)
  %tmp_55 = icmp slt i12 %pos1_1, 54
  %tmp_99 = call i1 @_ssdm_op_BitSelect.i1.i12.i32(i12 %pos1_1, i32 11)
  %rev2 = xor i1 %tmp_99, true
  %tmp_56 = zext i32 %pos1_1_cast to i54
  %tmp_57 = ashr i54 %man_V_6, %tmp_56
  %lD_1 = trunc i54 %tmp_57 to i1
  %tmp8 = and i1 %lD_1, %rev2
  %Range1_all_ones_3 = and i1 %tmp8, %tmp_55
  %tmp_101 = call i1 @_ssdm_op_BitSelect.i1.i12.i32(i12 %pos2_1, i32 11)
  %rev3 = xor i1 %tmp_101, true
  %tmp_58 = icmp slt i12 %pos2_1, 54
  %or_cond115_i = and i1 %tmp_58, %rev3
  %tmp_60 = zext i32 %pos2_1_cast to i54
  %Range2_V_3 = lshr i54 %man_V_6, %tmp_60
  %r_V_1 = lshr i54 -1, %tmp_60
  %Range2_all_ones_1 = icmp eq i54 %Range2_V_3, %r_V_1
  %Range2_all_ones_1_i = select i1 %or_cond115_i, i1 %Range2_all_ones_1, i1 %rev3
  %or_cond117_i = and i1 %tmp_58, %rev2
  %Range1_all_ones_2 = and i1 %Range2_all_ones_1_i, %Range1_all_ones_3
  %tmp_61 = icmp eq i54 %Range2_V_3, 0
  %Range1_all_zeros_2 = xor i1 %Range1_all_ones_3, true
  %p_122_i = and i1 %tmp_61, %Range1_all_zeros_2
  %tmp_63 = icmp eq i12 %pos2_1, 54
  %Range1_all_zeros_3 = icmp eq i54 %man_V_6, 0
  %p_119_i = or i1 %Range1_all_zeros_3, %rev2
  %tmp_63_not = xor i1 %tmp_58, true
  %sel_tmp15 = or i1 %tmp_99, %tmp_63_not
  %sel_tmp16 = and i1 %tmp_63, %sel_tmp15
  %sel_tmp17 = select i1 %sel_tmp16, i1 %Range1_all_ones_3, i1 %rev2
  %Range1_all_ones_2_i = select i1 %or_cond117_i, i1 %Range1_all_ones_2, i1 %sel_tmp17
  %sel_tmp18 = select i1 %sel_tmp16, i1 %Range1_all_zeros_2, i1 %p_119_i
  %Range1_all_zeros_2_i = select i1 %or_cond117_i, i1 %p_122_i, i1 %sel_tmp18
  %deleted_zeros_1 = select i1 %carry_1_i, i1 %Range1_all_ones_2_i, i1 %Range1_all_zeros_2_i
  %carry_1_not_i = xor i1 %carry_1_i, true
  %Range2_all_ones_1_not_i = xor i1 %Range2_all_ones_1_i, true
  %brmerge123_i = or i1 %carry_1_not_i, %Range2_all_ones_1_not_i
  %Range1_all_ones_2_mux_i = and i1 %Range1_all_ones_2_i, %carry_1_not_i
  %tmp_102 = call i1 @_ssdm_op_BitSelect.i1.i12.i32(i12 %pos1_1, i32 11)
  %p_120_i = or i1 %tmp_102, %Range1_all_zeros_2
  %deleted_ones_1 = select i1 %brmerge123_i, i1 %Range1_all_ones_2_mux_i, i1 %p_120_i
  %Range1_all_ones_2_i_9 = and i1 %carry_1_i, %Range1_all_ones_2_i
  %tmp_64 = xor i1 %Range1_all_ones_2_i_9, true
  %sel_tmp19 = xor i1 %tmp_55, true
  %deleted_ones_0_i = or i1 %deleted_ones_1, %sel_tmp19
  %tmp9 = and i1 %newsignbit_1, %sel_tmp19
  %sel_tmp20 = and i1 %tmp9, %isneg_1
  %sel_tmp21 = and i1 %tmp_55, %isneg_1
  %p_Repl2_1 = select i1 %sel_tmp21, i1 %tmp_64, i1 %sel_tmp20
  %deleted_zeros_1_not = xor i1 %deleted_zeros_1, true
  %deleted_zeros_0_not_i = and i1 %tmp_55, %deleted_zeros_1_not
  %brmerge_i = or i1 %newsignbit_1, %deleted_zeros_0_not_i
  %tmp_65 = xor i1 %isneg_1, true
  %overflow_1 = and i1 %brmerge_i, %tmp_65
  %brmerge121_demorgan_i = and i1 %newsignbit_1, %deleted_ones_0_i
  %brmerge121_i = xor i1 %brmerge121_demorgan_i, true
  %underflow_1 = and i1 %p_Repl2_1, %brmerge121_i
  %brmerge_i_i = or i1 %underflow_1, %overflow_1
  %p_Result_8 = call i32 @_ssdm_op_BitSet.i32.i32.i32.i1(i32 %fixed_y_V_8, i32 31, i1 %p_Repl2_1)
  %sel_tmp99_demorgan = or i1 %tmp_35, %icmp3
  %sel_tmp22 = xor i1 %tmp_35, true
  %tmp10 = and i1 %brmerge_i_i, %sel_tmp22
  %sel_tmp23 = and i1 %tmp10, %icmp3
  %tmp_30 = call i5 @_ssdm_op_PartSelect.i5.i32.i32.i32(i32 %p_Result_6, i32 27, i32 31)
  %tmp_104 = call i5 @_ssdm_op_PartSelect.i5.i32.i32.i32(i32 %fixed_x_V_8, i32 27, i32 31)
  %tmp_105 = select i1 %tmp_7, i5 0, i5 %tmp_104
  %tmp_66 = select i1 %sel_tmp45_demorgan, i5 %tmp_105, i5 %tmp_104
  %tmp_68 = select i1 %sel_tmp11, i5 %tmp_30, i5 %tmp_66
  %tmp_69 = call i5 @_ssdm_op_PartSelect.i5.i32.i32.i32(i32 %p_Result_8, i32 27, i32 31)
  %tmp_106 = call i5 @_ssdm_op_PartSelect.i5.i32.i32.i32(i32 %fixed_y_V_8, i32 27, i32 31)
  %tmp_107 = select i1 %tmp_35, i5 0, i5 %tmp_106
  %tmp_70 = select i1 %sel_tmp99_demorgan, i5 %tmp_107, i5 %tmp_106
  %tmp_71 = select i1 %sel_tmp23, i5 %tmp_69, i5 %tmp_70
  %p_Result_29_4 = call i10 @_ssdm_op_BitConcatenate.i10.i5.i5(i5 %tmp_68, i5 %tmp_71)
  %tmp_72 = zext i10 %p_Result_29_4 to i64
  %my_LUT_r_addr = getelementptr inbounds [1024 x float]* @my_LUT_r, i64 0, i64 %tmp_72
  %my_LUT_r_load = load volatile float* %my_LUT_r_addr, align 4
  call void @_ssdm_op_Write.ap_auto.floatP(float* %r, float %my_LUT_r_load) nounwind
  %my_LUT_th_addr = getelementptr inbounds [1024 x float]* @my_LUT_th, i64 0, i64 %tmp_72
  %my_LUT_th_load = load volatile float* %my_LUT_th_addr, align 4
  call void @_ssdm_op_Write.ap_auto.floatP(float* %theta, float %my_LUT_th_load) nounwind
  ret void
}

define weak void @_ssdm_op_Write.ap_auto.floatP(float*, float) {
entry:
  store float %1, float* %0
  ret void
}

define weak void @_ssdm_op_SpecTopModule(...) {
entry:
  ret void
}

define weak void @_ssdm_op_SpecBitsMap(...) {
entry:
  ret void
}

define weak float @_ssdm_op_Read.ap_auto.float(float) {
entry:
  ret float %0
}

define weak i7 @_ssdm_op_PartSelect.i7.i12.i32.i32(i12, i32, i32) nounwind readnone {
entry:
  %empty = call i12 @llvm.part.select.i12(i12 %0, i32 %1, i32 %2)
  %empty_10 = trunc i12 %empty to i7
  ret i7 %empty_10
}

declare i63 @_ssdm_op_PartSelect.i63.i64.i32.i32(i64, i32, i32) nounwind readnone

declare i6 @_ssdm_op_PartSelect.i6.i12.i32.i32(i12, i32, i32) nounwind readnone

declare i52 @_ssdm_op_PartSelect.i52.i64.i32.i32(i64, i32, i32) nounwind readnone

define weak i5 @_ssdm_op_PartSelect.i5.i32.i32.i32(i32, i32, i32) nounwind readnone {
entry:
  %empty = call i32 @llvm.part.select.i32(i32 %0, i32 %1, i32 %2)
  %empty_11 = trunc i32 %empty to i5
  ret i5 %empty_11
}

declare i32 @_ssdm_op_PartSelect.i32.i54.i32.i32(i54, i32, i32) nounwind readnone

define weak i11 @_ssdm_op_PartSelect.i11.i64.i32.i32(i64, i32, i32) nounwind readnone {
entry:
  %empty = call i64 @llvm.part.select.i64(i64 %0, i32 %1, i32 %2)
  %empty_12 = trunc i64 %empty to i11
  ret i11 %empty_12
}

define weak i11 @_ssdm_op_PartSelect.i11.i12.i32.i32(i12, i32, i32) nounwind readnone {
entry:
  %empty = call i12 @llvm.part.select.i12(i12 %0, i32 %1, i32 %2)
  %empty_13 = trunc i12 %empty to i11
  ret i11 %empty_13
}

declare i1 @_ssdm_op_PartSelect.i1.i54.i32.i32(i54, i32, i32) nounwind readnone

declare i16 @_ssdm_op_HSub(...)

declare i16 @_ssdm_op_HMul(...)

declare i16 @_ssdm_op_HDiv(...)

declare i16 @_ssdm_op_HAdd(...)

define weak i32 @_ssdm_op_BitSet.i32.i32.i32.i1(i32, i32, i1) nounwind readnone {
entry:
  %empty = icmp ne i1 %2, false
  %empty_14 = zext i1 %empty to i32
  %empty_15 = bitcast i32 %1 to i32
  %empty_16 = shl i32 %empty_14, %empty_15
  %empty_17 = shl i32 1, %empty_15
  %empty_18 = xor i32 %empty_17, -1
  %empty_19 = and i32 %empty_18, %0
  %empty_20 = or i32 %empty_16, %empty_19
  ret i32 %empty_20
}

define weak i1 @_ssdm_op_BitSelect.i1.i64.i32(i64, i32) nounwind readnone {
entry:
  %empty = zext i32 %1 to i64
  %empty_21 = shl i64 1, %empty
  %empty_22 = and i64 %0, %empty_21
  %empty_23 = icmp ne i64 %empty_22, 0
  ret i1 %empty_23
}

define weak i1 @_ssdm_op_BitSelect.i1.i54.i32(i54, i32) nounwind readnone {
entry:
  %empty = zext i32 %1 to i54
  %empty_24 = shl i54 1, %empty
  %empty_25 = and i54 %0, %empty_24
  %empty_26 = icmp ne i54 %empty_25, 0
  ret i1 %empty_26
}

define weak i1 @_ssdm_op_BitSelect.i1.i32.i32(i32, i32) nounwind readnone {
entry:
  %empty = shl i32 1, %1
  %empty_27 = and i32 %0, %empty
  %empty_28 = icmp ne i32 %empty_27, 0
  ret i1 %empty_28
}

define weak i1 @_ssdm_op_BitSelect.i1.i12.i32(i12, i32) nounwind readnone {
entry:
  %empty = trunc i32 %1 to i12
  %empty_29 = shl i12 1, %empty
  %empty_30 = and i12 %0, %empty_29
  %empty_31 = icmp ne i12 %empty_30, 0
  ret i1 %empty_31
}

define weak i53 @_ssdm_op_BitConcatenate.i53.i1.i52(i1, i52) nounwind readnone {
entry:
  %empty = zext i1 %0 to i53
  %empty_32 = zext i52 %1 to i53
  %empty_33 = shl i53 %empty, 52
  %empty_34 = or i53 %empty_33, %empty_32
  ret i53 %empty_34
}

define weak i10 @_ssdm_op_BitConcatenate.i10.i5.i5(i5, i5) nounwind readnone {
entry:
  %empty = zext i5 %0 to i10
  %empty_35 = zext i5 %1 to i10
  %empty_36 = shl i10 %empty, 5
  %empty_37 = or i10 %empty_36, %empty_35
  ret i10 %empty_37
}

declare void @_GLOBAL__I_a() nounwind

!hls.encrypted.func = !{}
!llvm.map.gv = !{!0, !7, !12}

!0 = metadata !{metadata !1, [1024 x float]* @my_LUT_th}
!1 = metadata !{metadata !2}
!2 = metadata !{i32 0, i32 31, metadata !3}
!3 = metadata !{metadata !4}
!4 = metadata !{metadata !"my_LUT_th", metadata !5, metadata !"float", i32 0, i32 31}
!5 = metadata !{metadata !6}
!6 = metadata !{i32 0, i32 1023, i32 1}
!7 = metadata !{metadata !8, [1024 x float]* @my_LUT_r}
!8 = metadata !{metadata !9}
!9 = metadata !{i32 0, i32 31, metadata !10}
!10 = metadata !{metadata !11}
!11 = metadata !{metadata !"my_LUT_r", metadata !5, metadata !"float", i32 0, i32 31}
!12 = metadata !{metadata !13, [1 x i32]* @llvm_global_ctors_0}
!13 = metadata !{metadata !14}
!14 = metadata !{i32 0, i32 31, metadata !15}
!15 = metadata !{metadata !16}
!16 = metadata !{metadata !"llvm.global_ctors.0", metadata !17, metadata !"", i32 0, i32 31}
!17 = metadata !{metadata !18}
!18 = metadata !{i32 0, i32 0, i32 1}
!19 = metadata !{metadata !20}
!20 = metadata !{i32 0, i32 31, metadata !21}
!21 = metadata !{metadata !22}
!22 = metadata !{metadata !"x", metadata !23, metadata !"float", i32 0, i32 31}
!23 = metadata !{metadata !24}
!24 = metadata !{i32 0, i32 0, i32 0}
!25 = metadata !{metadata !26}
!26 = metadata !{i32 0, i32 31, metadata !27}
!27 = metadata !{metadata !28}
!28 = metadata !{metadata !"y", metadata !23, metadata !"float", i32 0, i32 31}
!29 = metadata !{metadata !30}
!30 = metadata !{i32 0, i32 31, metadata !31}
!31 = metadata !{metadata !32}
!32 = metadata !{metadata !"r", metadata !17, metadata !"float", i32 0, i32 31}
!33 = metadata !{metadata !34}
!34 = metadata !{i32 0, i32 31, metadata !35}
!35 = metadata !{metadata !36}
!36 = metadata !{metadata !"theta", metadata !17, metadata !"float", i32 0, i32 31}
